-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: paotui
-- ------------------------------------------------------
-- Server version	5.5.57

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Table structure for table `base_bulletin`
--

DROP TABLE IF EXISTS `base_bulletin`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_bulletin` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `areaid`     INT(11)          DEFAULT NULL,
  `bullFlag`   VARCHAR(255)     DEFAULT NULL,
  `content`    VARCHAR(255)     DEFAULT NULL,
  `title`      VARCHAR(255)     DEFAULT NULL,
  `vorgid`     INT(11) NOT NULL,
  `orgid`      VARCHAR(20)      DEFAULT NULL,
  `userid`     INT(11)          DEFAULT NULL,
  `createTime` DATETIME         DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK316C7F554A0B6039` (`orgid`),
  KEY `FK316C7F5560BEF4F8` (`userid`),
  CONSTRAINT `FK316C7F554A0B6039` FOREIGN KEY (`orgid`) REFERENCES `base_org` (`orgId`),
  CONSTRAINT `FK316C7F5560BEF4F8` FOREIGN KEY (`userid`) REFERENCES `base_user` (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_bulletin_attach`
--

DROP TABLE IF EXISTS `base_bulletin_attach`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_bulletin_attach` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `bybid`      INT(11) NOT NULL,
  `createtime` DATETIME         DEFAULT NULL,
  `fileName`   VARCHAR(255)     DEFAULT NULL,
  `filePath`   VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_org`
--

DROP TABLE IF EXISTS `base_org`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_org` (
  `orgId`   VARCHAR(20) NOT NULL,
  `byOrg`   VARCHAR(20) DEFAULT NULL,
  `orgName` VARCHAR(50) DEFAULT NULL,
  `orgType` INT(11)     NOT NULL,
  PRIMARY KEY (`orgId`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_param`
--

DROP TABLE IF EXISTS `base_param`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_param` (
  `id`        INT(11) NOT NULL AUTO_INCREMENT,
  `mark`      VARCHAR(255)     DEFAULT NULL,
  `paramName` VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 8
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_param_conf`
--

DROP TABLE IF EXISTS `base_param_conf`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_param_conf` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `actionName` VARCHAR(255)     DEFAULT NULL,
  `mark`       VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_param_conf_pk`
--

DROP TABLE IF EXISTS `base_param_conf_pk`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_param_conf_pk` (
  `cid` INT(11) NOT NULL,
  `sid` INT(11) NOT NULL,
  UNIQUE KEY `sid` (`sid`),
  KEY `FK6DD90143962D534` (`sid`),
  KEY `FK6DD9014F8D41968` (`cid`),
  CONSTRAINT `FK6DD90143962D534` FOREIGN KEY (`sid`) REFERENCES `base_param` (`id`),
  CONSTRAINT `FK6DD9014F8D41968` FOREIGN KEY (`cid`) REFERENCES `base_param_conf` (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_param_value`
--

DROP TABLE IF EXISTS `base_param_value`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_param_value` (
  `id`      INT(11) NOT NULL AUTO_INCREMENT,
  `name`    VARCHAR(255)     DEFAULT NULL,
  `sort`    INT(11) NOT NULL,
  `value`   VARCHAR(255)     DEFAULT NULL,
  `paramId` INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKB95E900FA154ECE` (`paramId`),
  CONSTRAINT `FKB95E900FA154ECE` FOREIGN KEY (`paramId`) REFERENCES `base_param` (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 43
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_purview`
--

DROP TABLE IF EXISTS `base_purview`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_purview` (
  `pid`        INT(11) NOT NULL,
  `byid`       INT(11) NOT NULL,
  `createtime` DATETIME     DEFAULT NULL,
  `info`       LONGTEXT,
  `link`       VARCHAR(255) DEFAULT NULL,
  `pname`      VARCHAR(128) DEFAULT NULL,
  `ptype`      INT(11) NOT NULL,
  `sort`       INT(11) NOT NULL,
  `target`     VARCHAR(255) DEFAULT NULL,
  `showType`   INT(11)      DEFAULT '0',
  PRIMARY KEY (`pid`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_purview_copy`
--

DROP TABLE IF EXISTS `base_purview_copy`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_purview_copy` (
  `pid`        INT(11) NOT NULL,
  `byid`       INT(11) NOT NULL,
  `createtime` DATETIME     DEFAULT NULL,
  `info`       LONGTEXT,
  `link`       VARCHAR(255) DEFAULT NULL,
  `pname`      VARCHAR(128) DEFAULT NULL,
  `ptype`      INT(11) NOT NULL,
  `sort`       INT(11) NOT NULL,
  `target`     VARCHAR(255) DEFAULT NULL,
  `showType`   INT(11)      DEFAULT '0',
  PRIMARY KEY (`pid`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_purview_copy1`
--

DROP TABLE IF EXISTS `base_purview_copy1`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_purview_copy1` (
  `pid`        INT(11) NOT NULL,
  `byid`       INT(11) NOT NULL,
  `createtime` DATETIME     DEFAULT NULL,
  `info`       LONGTEXT,
  `link`       VARCHAR(255) DEFAULT NULL,
  `pname`      VARCHAR(128) DEFAULT NULL,
  `ptype`      INT(11) NOT NULL,
  `sort`       INT(11) NOT NULL,
  `target`     VARCHAR(255) DEFAULT NULL,
  `showType`   INT(11)      DEFAULT '0',
  PRIMARY KEY (`pid`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_role`
--

DROP TABLE IF EXISTS `base_role`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_role` (
  `rid`   INT(11) NOT NULL AUTO_INCREMENT,
  `info`  LONGTEXT,
  `rname` VARCHAR(128)     DEFAULT NULL,
  `flag`  INT(11)          DEFAULT NULL,
  `orgId` VARCHAR(20)      DEFAULT NULL,
  PRIMARY KEY (`rid`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 25
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_role_purview`
--

DROP TABLE IF EXISTS `base_role_purview`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_role_purview` (
  `rid` INT(11) NOT NULL,
  `pid` INT(11) NOT NULL,
  PRIMARY KEY (`rid`, `pid`),
  KEY `FKA240081D9294108A` (`rid`),
  KEY `FKA240081D54C34856` (`pid`),
  CONSTRAINT `FKA240081D54C34856` FOREIGN KEY (`pid`) REFERENCES `base_purview` (`pid`),
  CONSTRAINT `FKA240081D9294108A` FOREIGN KEY (`rid`) REFERENCES `base_role` (`rid`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_seting`
--

DROP TABLE IF EXISTS `base_seting`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_seting` (
  `id`              INT(11) NOT NULL AUTO_INCREMENT,
  `createTime`      DATETIME         DEFAULT NULL,
  `globalRoleName`  VARCHAR(200)     DEFAULT NULL,
  `name`            VARCHAR(200)     DEFAULT NULL,
  `rootOrgId`       VARCHAR(30)      DEFAULT NULL,
  `rootOrgName`     VARCHAR(200)     DEFAULT NULL,
  `title`           VARCHAR(200)     DEFAULT NULL,
  `rootPurviewName` VARCHAR(50)      DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 2
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_system_log`
--

DROP TABLE IF EXISTS `base_system_log`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_system_log` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `className`  VARCHAR(100)     DEFAULT NULL,
  `createTime` DATETIME         DEFAULT NULL,
  `methodName` VARCHAR(100)     DEFAULT NULL,
  `params`     LONGTEXT,
  `userName`   VARCHAR(255)     DEFAULT NULL,
  `userId`     INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK956E5A64649D3512` (`className`, `methodName`),
  KEY `FK956E5A6460BEF4F8` (`userId`),
  CONSTRAINT `FK956E5A6460BEF4F8` FOREIGN KEY (`userId`) REFERENCES `base_user` (`id`),
  CONSTRAINT `FK956E5A64649D3512` FOREIGN KEY (`className`, `methodName`) REFERENCES `base_system_log_value` (`className`, `methodName`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_system_log_value`
--

DROP TABLE IF EXISTS `base_system_log_value`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_system_log_value` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `className`  VARCHAR(100)     DEFAULT NULL,
  `methodName` VARCHAR(100)     DEFAULT NULL,
  `moduleName` VARCHAR(255)     DEFAULT NULL,
  `opName`     VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `className` (`className`, `methodName`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 19
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_user`
--

DROP TABLE IF EXISTS `base_user`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_user` (
  `id`         INT(11)     NOT NULL AUTO_INCREMENT,
  `areaid`     VARCHAR(20)          DEFAULT NULL,
  `createtime` DATETIME             DEFAULT NULL,
  `cssStyle`   VARCHAR(255)         DEFAULT NULL,
  `email`      VARCHAR(50)          DEFAULT NULL,
  `mobile`     VARCHAR(20)          DEFAULT NULL,
  `modifyTime` DATETIME             DEFAULT NULL,
  `orgId`      VARCHAR(20) NOT NULL,
  `password`   VARCHAR(255)         DEFAULT NULL,
  `realname`   VARCHAR(20)          DEFAULT NULL,
  `state`      INT(11)     NOT NULL,
  `username`   VARCHAR(20)          DEFAULT NULL,
  `flag`       INT(11)              DEFAULT NULL,
  `cityId`     VARCHAR(255)         DEFAULT NULL,
  `cityName`   VARCHAR(255)         DEFAULT NULL,
  `countyId`   VARCHAR(255)         DEFAULT NULL,
  `countyName` VARCHAR(255)         DEFAULT NULL,
  `agentids`   VARCHAR(255)         DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 22
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_user_purview`
--

DROP TABLE IF EXISTS `base_user_purview`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_user_purview` (
  `userid`  INT(11) NOT NULL,
  `pid`     INT(11) NOT NULL,
  `id`      INT(11)      DEFAULT NULL,
  `areaid`  INT(11)      DEFAULT NULL,
  `objtype` VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (`userid`, `pid`),
  KEY `FKE65F4E7254C34856` (`pid`),
  KEY `FKE65F4E7260BEF4F8` (`userid`),
  CONSTRAINT `FKE65F4E7254C34856` FOREIGN KEY (`pid`) REFERENCES `base_purview` (`pid`),
  CONSTRAINT `FKE65F4E7260BEF4F8` FOREIGN KEY (`userid`) REFERENCES `base_user` (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `base_user_role`
--

DROP TABLE IF EXISTS `base_user_role`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_user_role` (
  `userid` INT(11) NOT NULL,
  `rid`    INT(11) NOT NULL,
  PRIMARY KEY (`userid`, `rid`),
  KEY `FK73C519369294108A` (`rid`),
  KEY `FK73C5193660BEF4F8` (`userid`),
  CONSTRAINT `FK73C5193660BEF4F8` FOREIGN KEY (`userid`) REFERENCES `base_user` (`id`),
  CONSTRAINT `FK73C519369294108A` FOREIGN KEY (`rid`) REFERENCES `base_role` (`rid`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bs_feedback_record`
--

DROP TABLE IF EXISTS `bs_feedback_record`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bs_feedback_record` (
  `id`           INT(11) NOT NULL AUTO_INCREMENT,
  `accounts`     VARCHAR(255)     DEFAULT NULL,
  `businessId`   INT(11)          DEFAULT NULL,
  `businessName` VARCHAR(255)     DEFAULT NULL,
  `content`      LONGTEXT,
  `createTime`   DATETIME         DEFAULT NULL,
  `email`        VARCHAR(255)     DEFAULT NULL,
  `mobile`       VARCHAR(255)     DEFAULT NULL,
  `replyContext` LONGTEXT,
  `replyTime`    DATETIME         DEFAULT NULL,
  `state`        INT(11)          DEFAULT NULL,
  `userId`       INT(11)          DEFAULT NULL,
  `userName`     VARCHAR(255)     DEFAULT NULL,
  `agentId`      INT(11)          DEFAULT NULL,
  `agentName`    VARCHAR(100)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bs_travel_advert`
--

DROP TABLE IF EXISTS `bs_travel_advert`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bs_travel_advert` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `adImages`   VARCHAR(255)     DEFAULT NULL,
  `adRank`     INT(11)          DEFAULT NULL,
  `content`    LONGTEXT,
  `createTime` DATETIME         DEFAULT NULL,
  `linkAddr`   VARCHAR(255)     DEFAULT NULL,
  `location`   INT(11)          DEFAULT NULL,
  `title`      VARCHAR(255)     DEFAULT NULL,
  `used`       INT(11)          DEFAULT NULL,
  `adType`     INT(11)          DEFAULT NULL,
  `agentId`    INT(11)          DEFAULT NULL,
  `agentName`  VARCHAR(100)     DEFAULT NULL,
  `typename`   VARCHAR(255)     DEFAULT NULL,
  `type`       INT(11)          DEFAULT NULL,
  `num`        INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_used_location` (`location`, `used`),
  KEY `idx_used_loc_agentId` (`location`, `used`, `agentId`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 855
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foodmodel1486691996952`
--

DROP TABLE IF EXISTS `foodmodel1486691996952`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foodmodel1486691996952` (
  `field_0`  VARCHAR(200) DEFAULT NULL,
  `field_3`  VARCHAR(200) DEFAULT NULL,
  `field_4`  VARCHAR(200) DEFAULT NULL,
  `field_5`  VARCHAR(200) DEFAULT NULL,
  `field_6`  VARCHAR(200) DEFAULT NULL,
  `field_7`  VARCHAR(200) DEFAULT NULL,
  `field_1`  VARCHAR(200) DEFAULT NULL,
  `field_8`  VARCHAR(200) DEFAULT NULL,
  `field_9`  VARCHAR(200) DEFAULT NULL,
  `field_10` VARCHAR(200) DEFAULT NULL,
  `field_11` VARCHAR(200) DEFAULT NULL,
  `field_12` VARCHAR(200) DEFAULT NULL,
  `field_2`  VARCHAR(200) DEFAULT NULL,
  `field_13` VARCHAR(200) DEFAULT NULL,
  `field_14` VARCHAR(200) DEFAULT NULL,
  `tid`      INT(11) NOT NULL,
  `field_15` VARCHAR(255) DEFAULT NULL,
  `field_16` VARCHAR(255) DEFAULT NULL,
  `field_17` VARCHAR(255) DEFAULT NULL,
  `field_18` VARCHAR(255) DEFAULT NULL,
  `field_19` VARCHAR(255) DEFAULT NULL,
  `field_20` VARCHAR(255) DEFAULT NULL,
  `field_21` VARCHAR(255) DEFAULT NULL,
  `field_22` VARCHAR(255) DEFAULT NULL,
  `field_23` VARCHAR(255) DEFAULT NULL,
  `field_24` VARCHAR(255) DEFAULT NULL,
  `field_25` VARCHAR(255) DEFAULT NULL,
  `field_26` VARCHAR(255) DEFAULT NULL,
  `field_27` VARCHAR(255) DEFAULT NULL,
  `field_28` VARCHAR(255) DEFAULT NULL,
  `field_29` VARCHAR(255) DEFAULT NULL,
  `field_30` VARCHAR(255) DEFAULT NULL,
  `field_31` VARCHAR(255) DEFAULT NULL,
  `field_32` VARCHAR(255) DEFAULT NULL,
  `field_33` VARCHAR(255) DEFAULT NULL,
  `field_34` VARCHAR(255) DEFAULT NULL,
  `field_35` VARCHAR(255) DEFAULT NULL,
  `field_36` VARCHAR(255) DEFAULT NULL,
  `field_37` VARCHAR(255) DEFAULT NULL,
  `field_38` VARCHAR(255) DEFAULT NULL,
  KEY `idx_tid` (`tid`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foodmodel1491552338110`
--

DROP TABLE IF EXISTS `foodmodel1491552338110`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foodmodel1491552338110` (
  `field_0`  VARCHAR(200) DEFAULT NULL,
  `field_7`  VARCHAR(200) DEFAULT NULL,
  `field_1`  VARCHAR(200) DEFAULT NULL,
  `field_8`  VARCHAR(200) DEFAULT NULL,
  `field_2`  VARCHAR(200) DEFAULT NULL,
  `field_9`  VARCHAR(200) DEFAULT NULL,
  `field_3`  VARCHAR(200) DEFAULT NULL,
  `field_10` VARCHAR(200) DEFAULT NULL,
  `field_4`  VARCHAR(200) DEFAULT NULL,
  `field_11` VARCHAR(200) DEFAULT NULL,
  `field_5`  VARCHAR(200) DEFAULT NULL,
  `field_12` VARCHAR(200) DEFAULT NULL,
  `field_6`  VARCHAR(200) DEFAULT NULL,
  `field_13` VARCHAR(200) DEFAULT NULL,
  `tid`      INT(11) NOT NULL,
  KEY `idx_tid` (`tid`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foodmodel1491557537021`
--

DROP TABLE IF EXISTS `foodmodel1491557537021`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foodmodel1491557537021` (
  `field_0` VARCHAR(200) DEFAULT NULL,
  `field_2` VARCHAR(200) DEFAULT NULL,
  `field_1` VARCHAR(200) DEFAULT NULL,
  `field_3` VARCHAR(200) DEFAULT NULL,
  `tid`     INT(11) NOT NULL,
  KEY `idx_tid` (`tid`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `foodmodel1491560575914`
--

DROP TABLE IF EXISTS `foodmodel1491560575914`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `foodmodel1491560575914` (
  `field_0` VARCHAR(200) DEFAULT NULL,
  `field_1` VARCHAR(200) DEFAULT NULL,
  `tid`     INT(11) NOT NULL,
  `field_2` VARCHAR(255) DEFAULT NULL,
  `field_3` VARCHAR(255) DEFAULT NULL,
  `field_4` VARCHAR(255) DEFAULT NULL,
  KEY `dx_tid` (`tid`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_accountrecord`
--

DROP TABLE IF EXISTS `runfast_accountrecord`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_accountrecord` (
  `id`           INT(11) NOT NULL AUTO_INCREMENT,
  `balance`      DECIMAL(19, 2)   DEFAULT NULL,
  `cardnumber`   VARCHAR(255)     DEFAULT NULL,
  `cid`          INT(11)          DEFAULT NULL,
  `minmonety`    DECIMAL(19, 2)   DEFAULT NULL,
  `mobile`       VARCHAR(255)     DEFAULT NULL,
  `monetary`     DECIMAL(19, 2)   DEFAULT NULL,
  `name`         VARCHAR(255)     DEFAULT NULL,
  `type`         INT(11)          DEFAULT NULL,
  `createTime`   DATETIME         DEFAULT NULL,
  `genreType`    INT(11)          DEFAULT NULL,
  `typename`     VARCHAR(255)     DEFAULT NULL,
  `beforemonety` DECIMAL(19, 2)   DEFAULT NULL,
  `showtype`     INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 51357
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_activity`
--

DROP TABLE IF EXISTS `runfast_activity`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_activity` (
  `id`          INT(11) NOT NULL AUTO_INCREMENT,
  `busId`       INT(11)          DEFAULT NULL,
  `busname`     VARCHAR(255)     DEFAULT NULL,
  `createTime`  DATETIME         DEFAULT NULL,
  `discount`    DOUBLE           DEFAULT NULL,
  `endTime`     DATETIME         DEFAULT NULL,
  `fulls`       DOUBLE           DEFAULT NULL,
  `lesss`       DOUBLE           DEFAULT NULL,
  `name`        VARCHAR(255)     DEFAULT NULL,
  `ptype`       INT(11)          DEFAULT NULL,
  `startTime`   DATETIME         DEFAULT NULL,
  `type`        INT(11)          DEFAULT NULL,
  `goodids`     VARCHAR(3000)    DEFAULT NULL,
  `goodsname`   VARCHAR(3000)    DEFAULT NULL,
  `agentId`     INT(11)          DEFAULT NULL,
  `agentName`   VARCHAR(100)     DEFAULT NULL,
  `stops`       INT(11)          DEFAULT NULL,
  `goods`       VARCHAR(3000)    DEFAULT NULL,
  `disprice`    DECIMAL(19, 2)   DEFAULT NULL,
  `stanids`     LONGTEXT,
  `stanidsname` VARCHAR(3000)    DEFAULT NULL,
  `deleted`     INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_busId_startTime_endTime_stops` (`busId`, `endTime`, `startTime`, `stops`),
  KEY `idx_busid_stattime_end_stop_ptype` (`busId`, `endTime`, `ptype`, `startTime`, `stops`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 6079
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_address`
--

DROP TABLE IF EXISTS `runfast_address`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_address` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `area`       VARCHAR(255)     DEFAULT NULL,
  `areaId`     INT(11)          DEFAULT NULL,
  `bossId`     INT(11)          DEFAULT NULL,
  `bossName`   VARCHAR(255)     DEFAULT NULL,
  `city`       VARCHAR(255)     DEFAULT NULL,
  `cityId`     INT(11)          DEFAULT NULL,
  `contract`   VARCHAR(255)     DEFAULT NULL,
  `createTime` DATETIME         DEFAULT NULL,
  `name`       VARCHAR(255)     DEFAULT NULL,
  `phone`      VARCHAR(255)     DEFAULT NULL,
  `province`   VARCHAR(255)     DEFAULT NULL,
  `provinceId` INT(11)          DEFAULT NULL,
  `state`      INT(11)          DEFAULT NULL,
  `twon`       VARCHAR(255)     DEFAULT NULL,
  `twonId`     INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_bossid` (`bossId`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 40
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_agentaccounts`
--

DROP TABLE IF EXISTS `runfast_agentaccounts`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_agentaccounts` (
  `id`              INT(11) NOT NULL AUTO_INCREMENT,
  `accounts`        VARCHAR(100)     DEFAULT NULL,
  `agentId`         INT(11)          DEFAULT NULL,
  `agentName`       VARCHAR(100)     DEFAULT NULL,
  `byRoles`         VARCHAR(255)     DEFAULT NULL,
  `createTime`      DATETIME         DEFAULT NULL,
  `mobile`          VARCHAR(100)     DEFAULT NULL,
  `name`            VARCHAR(255)     DEFAULT NULL,
  `password`        VARCHAR(255)     DEFAULT NULL,
  `bdpushChannelId` VARCHAR(255)     DEFAULT NULL,
  `bdpushUserId`    VARCHAR(255)     DEFAULT NULL,
  `bptype`          INT(11)          DEFAULT NULL,
  `cityId`          VARCHAR(255)     DEFAULT NULL,
  `cityName`        VARCHAR(255)     DEFAULT NULL,
  `countyId`        VARCHAR(255)     DEFAULT NULL,
  `countyName`      VARCHAR(255)     DEFAULT NULL,
  `aid`             INT(11)          DEFAULT NULL,
  `area`            VARCHAR(255)     DEFAULT NULL,
  `areaId`          VARCHAR(255)     DEFAULT NULL,
  `cid`             INT(11)          DEFAULT NULL,
  `pid`             INT(11)          DEFAULT NULL,
  `province`        VARCHAR(255)     DEFAULT NULL,
  `provinceId`      VARCHAR(255)     DEFAULT NULL,
  `tid`             INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 87
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_agentbusiness`
--

DROP TABLE IF EXISTS `runfast_agentbusiness`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_agentbusiness` (
  `id`           INT(11) NOT NULL AUTO_INCREMENT,
  `areas`        VARCHAR(255)     DEFAULT NULL,
  `createTime`   DATETIME         DEFAULT NULL,
  `mobile`       VARCHAR(100)     DEFAULT NULL,
  `name`         VARCHAR(100)     DEFAULT NULL,
  `showAreas`    LONGTEXT,
  `agentId`      INT(11)          DEFAULT NULL,
  `agentName`    VARCHAR(100)     DEFAULT NULL,
  `cityId`       VARCHAR(255)     DEFAULT NULL,
  `cityName`     VARCHAR(255)     DEFAULT NULL,
  `countyId`     VARCHAR(255)     DEFAULT NULL,
  `countyName`   VARCHAR(255)     DEFAULT NULL,
  `coefficient`  DECIMAL(19, 2)   DEFAULT NULL,
  `code`         VARCHAR(255)     DEFAULT NULL,
  `area`         VARCHAR(255)     DEFAULT NULL,
  `areaId`       VARCHAR(255)     DEFAULT NULL,
  `coefficient2` DECIMAL(19, 2)   DEFAULT NULL,
  `managerCode`  VARCHAR(255)     DEFAULT NULL,
  `managerId`    INT(11)          DEFAULT NULL,
  `province`     VARCHAR(255)     DEFAULT NULL,
  `provinceId`   VARCHAR(255)     DEFAULT NULL,
  `aid`          INT(11)          DEFAULT NULL,
  `cid`          INT(11)          DEFAULT NULL,
  `managerName`  VARCHAR(255)     DEFAULT NULL,
  `pid`          INT(11)          DEFAULT NULL,
  `tid`          INT(11)          DEFAULT NULL,
  `ishaveup`     INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 40
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_application`
--

DROP TABLE IF EXISTS `runfast_application`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_application` (
  `id`            INT(11) NOT NULL AUTO_INCREMENT,
  `aid`           INT(11)          DEFAULT NULL,
  `aname`         VARCHAR(255)     DEFAULT NULL,
  `atuDate`       DATETIME         DEFAULT NULL,
  `cardNumber`    VARCHAR(255)     DEFAULT NULL,
  `cid`           INT(11)          DEFAULT NULL,
  `cname`         VARCHAR(255)     DEFAULT NULL,
  `createTime`    DATETIME         DEFAULT NULL,
  `monetary`      DECIMAL(19, 2)   DEFAULT NULL,
  `pic`           VARCHAR(255)     DEFAULT NULL,
  `status`        INT(11)          DEFAULT NULL,
  `type`          INT(11)          DEFAULT NULL,
  `amobile`       VARCHAR(255)     DEFAULT NULL,
  `cmobile`       VARCHAR(255)     DEFAULT NULL,
  `agentId`       INT(11)          DEFAULT NULL,
  `agentName`     VARCHAR(100)     DEFAULT NULL,
  `foodsId`       INT(11)          DEFAULT NULL,
  `foodsName`     VARCHAR(255)     DEFAULT NULL,
  `sid`           INT(11)          DEFAULT NULL,
  `sidname`       VARCHAR(255)     DEFAULT NULL,
  `state`         INT(11)          DEFAULT NULL,
  `supportId`     INT(11)          DEFAULT NULL,
  `supportName`   VARCHAR(255)     DEFAULT NULL,
  `chidId`        INT(11)          DEFAULT NULL,
  `courierNumber` VARCHAR(255)     DEFAULT NULL,
  `num`           INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_cid_sid` (`cid`, `status`, `foodsId`, `sid`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 6
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_area`
--

DROP TABLE IF EXISTS `runfast_area`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_area` (
  `id`           INT(11) NOT NULL AUTO_INCREMENT,
  `byName`       VARCHAR(255)     DEFAULT NULL,
  `byid`         VARCHAR(255)     DEFAULT NULL,
  `code`         VARCHAR(255)     DEFAULT NULL,
  `isShow`       INT(11)          DEFAULT NULL,
  `level`        INT(11)          DEFAULT NULL,
  `name`         VARCHAR(255)     DEFAULT NULL,
  `sort`         INT(11)          DEFAULT NULL,
  `typeImages`   VARCHAR(255)     DEFAULT NULL,
  `agentId`      INT(11)          DEFAULT NULL,
  `agentName`    VARCHAR(100)     DEFAULT NULL,
  `city`         VARCHAR(255)     DEFAULT NULL,
  `cityCode`     VARCHAR(255)     DEFAULT NULL,
  `cityId`       INT(11)          DEFAULT NULL,
  `province`     VARCHAR(255)     DEFAULT NULL,
  `provinceCode` VARCHAR(255)     DEFAULT NULL,
  `provinceId`   INT(11)          DEFAULT NULL,
  `region`       VARCHAR(255)     DEFAULT NULL,
  `regionCode`   VARCHAR(255)     DEFAULT NULL,
  `regionId`     INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 49
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_associator`
--

DROP TABLE IF EXISTS `runfast_associator`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_associator` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `cardNumber` VARCHAR(255)     DEFAULT NULL,
  `cid`        INT(11)          DEFAULT NULL,
  `createTime` DATETIME         DEFAULT NULL,
  `levelId`    INT(11)          DEFAULT NULL,
  `levelName`  VARCHAR(255)     DEFAULT NULL,
  `mobile`     VARCHAR(255)     DEFAULT NULL,
  `name`       VARCHAR(255)     DEFAULT NULL,
  `rank`       INT(11)          DEFAULT NULL,
  `monetary`   DECIMAL(19, 2)   DEFAULT NULL,
  `sort`       INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_car` (`cardNumber`),
  KEY `idx_cid` (`cid`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 447178
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_associatorlevel`
--

DROP TABLE IF EXISTS `runfast_associatorlevel`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_associatorlevel` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `content`    VARCHAR(255)     DEFAULT NULL,
  `createTime` DATETIME         DEFAULT NULL,
  `date`       INT(11)          DEFAULT NULL,
  `discount`   DECIMAL(19, 2)   DEFAULT NULL,
  `endmoney`   DECIMAL(19, 2)   DEFAULT NULL,
  `isdiscount` INT(11)          DEFAULT NULL,
  `name`       VARCHAR(255)     DEFAULT NULL,
  `sort`       INT(11)          DEFAULT NULL,
  `startmoney` DECIMAL(19, 2)   DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_bankaccounts`
--

DROP TABLE IF EXISTS `runfast_bankaccounts`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_bankaccounts` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `account`    VARCHAR(50)      DEFAULT NULL,
  `banktype`   VARCHAR(50)      DEFAULT NULL,
  `createTime` DATETIME         DEFAULT NULL,
  `createbank` VARCHAR(255)     DEFAULT NULL,
  `name`       VARCHAR(50)      DEFAULT NULL,
  `type`       INT(11)          DEFAULT NULL,
  `userId`     INT(11)          DEFAULT NULL,
  `userMobile` VARCHAR(50)      DEFAULT NULL,
  `userName`   VARCHAR(50)      DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 1398
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_brand`
--

DROP TABLE IF EXISTS `runfast_brand`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_brand` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `createTime` DATETIME         DEFAULT NULL,
  `name`       VARCHAR(50)      DEFAULT NULL,
  `logo`       VARCHAR(255)     DEFAULT NULL,
  `sort`       INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 36
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_business`
--

DROP TABLE IF EXISTS `runfast_business`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_business` (
  `id`               INT(11) NOT NULL AUTO_INCREMENT,
  `address`          VARCHAR(255)     DEFAULT NULL,
  `cityId`           VARCHAR(255)     DEFAULT NULL,
  `cityName`         VARCHAR(255)     DEFAULT NULL,
  `content`          VARCHAR(255)     DEFAULT NULL,
  `countyId`         VARCHAR(255)     DEFAULT NULL,
  `countyName`       VARCHAR(255)     DEFAULT NULL,
  `createTime`       DATETIME         DEFAULT NULL,
  `imgPath`          VARCHAR(255)     DEFAULT NULL,
  `latitude`         VARCHAR(255)     DEFAULT NULL,
  `levelId`          INT(11)          DEFAULT NULL,
  `levelName`        VARCHAR(255)     DEFAULT NULL,
  `longitude`        VARCHAR(255)     DEFAULT NULL,
  `mobile`           VARCHAR(255)     DEFAULT NULL,
  `name`             VARCHAR(255)     DEFAULT NULL,
  `saleDayTime`      VARCHAR(255)     DEFAULT NULL,
  `saleRange`        VARCHAR(255)     DEFAULT NULL,
  `saleTime`         VARCHAR(255)     DEFAULT NULL,
  `townId`           VARCHAR(255)     DEFAULT NULL,
  `townName`         VARCHAR(255)     DEFAULT NULL,
  `typeId`           INT(11)          DEFAULT NULL,
  `typeName`         VARCHAR(255)     DEFAULT NULL,
  `salesnum`         INT(11)          DEFAULT NULL,
  `sort`             INT(11)          DEFAULT NULL,
  `startPay`         DECIMAL(19, 2)   DEFAULT NULL,
  `endwork`          DATETIME         DEFAULT NULL,
  `startwork`        DATETIME         DEFAULT NULL,
  `worktoday`        VARCHAR(255)     DEFAULT NULL,
  `packing`          DECIMAL(19, 2)   DEFAULT NULL,
  `isDeliver`        INT(11)          DEFAULT NULL,
  `recommend`        INT(11)          DEFAULT NULL,
  `speed`            INT(11)          DEFAULT NULL,
  `status`           INT(11)          DEFAULT NULL,
  `endTime1`         TIME             DEFAULT NULL,
  `endTime2`         TIME             DEFAULT NULL,
  `startTime1`       TIME             DEFAULT NULL,
  `startTime2`       TIME             DEFAULT NULL,
  `coefficient`      DOUBLE           DEFAULT NULL,
  `minmonety`        DECIMAL(19, 2)   DEFAULT NULL,
  `period`           INT(11)          DEFAULT NULL,
  `account`          VARCHAR(255)     DEFAULT NULL,
  `showps`           DECIMAL(19, 2)   DEFAULT NULL,
  `busshowps`        DECIMAL(19, 2)   DEFAULT NULL,
  `endwork2`         DATETIME         DEFAULT NULL,
  `startwork2`       DATETIME         DEFAULT NULL,
  `statu`            INT(11)          DEFAULT NULL,
  `agentId`          INT(11)          DEFAULT NULL,
  `agentName`        VARCHAR(100)     DEFAULT NULL,
  `statusx`          INT(11)          DEFAULT NULL,
  `distributionTime` INT(11)          DEFAULT NULL,
  `packTime`         INT(11)          DEFAULT NULL,
  `bank`             INT(11)          DEFAULT NULL,
  `establishbank`    VARCHAR(255)     DEFAULT NULL,
  `establishname`    VARCHAR(255)     DEFAULT NULL,
  `mini_imgPath`     VARCHAR(255)     DEFAULT NULL,
  `isopen`           INT(11)          DEFAULT NULL,
  `baseCharge`       DECIMAL(19, 2)   DEFAULT NULL,
  `isCharge`         INT(11)          DEFAULT NULL,
  `code`             VARCHAR(255)     DEFAULT NULL,
  `typestr`          VARCHAR(255)     DEFAULT NULL,
  `teamid`           INT(11)          DEFAULT NULL,
  `teamname`         VARCHAR(255)     DEFAULT NULL,
  `visitnum`         INT(11)          DEFAULT NULL,
  `issubsidy`        INT(11)          DEFAULT NULL,
  `subsidy`          DECIMAL(19, 2)   DEFAULT NULL,
  `autoprint`        INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_lat_lng` (`latitude`, `longitude`),
  KEY `idx_statusx` (`statusx`),
  KEY `idx_statusx_lat_lng` (`latitude`, `longitude`, `statusx`),
  KEY `idx_name_statusx_agentId` (`name`, `agentId`, `statusx`),
  KEY `idx_name_statusx_agentId_typestr` (`name`, `agentId`, `statusx`, `typestr`),
  KEY `idx_agentId` (`agentId`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 4165
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_business_accounts`
--

DROP TABLE IF EXISTS `runfast_business_accounts`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_business_accounts` (
  `id`              INT(11) NOT NULL AUTO_INCREMENT,
  `accounts`        VARCHAR(255)     DEFAULT NULL,
  `businessId`      INT(11)          DEFAULT NULL,
  `businessName`    VARCHAR(255)     DEFAULT NULL,
  `createTime`      DATETIME         DEFAULT NULL,
  `name`            VARCHAR(255)     DEFAULT NULL,
  `password`        VARCHAR(255)     DEFAULT NULL,
  `purview`         INT(11)          DEFAULT NULL,
  `byRoles`         VARCHAR(255)     DEFAULT NULL,
  `mobile`          VARCHAR(255)     DEFAULT NULL,
  `agentId`         INT(11)          DEFAULT NULL,
  `agentName`       VARCHAR(100)     DEFAULT NULL,
  `bdpushChannelId` VARCHAR(255)     DEFAULT NULL,
  `bdpushUserId`    VARCHAR(255)     DEFAULT NULL,
  `bptype`          INT(11)          DEFAULT NULL,
  `ispush`          INT(11)          DEFAULT NULL,
  `otherId`         VARCHAR(255)     DEFAULT NULL,
  `pushType`        INT(11)          DEFAULT NULL,
  `alias`           VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 4796
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_business_busnameauditing`
--

DROP TABLE IF EXISTS `runfast_business_busnameauditing`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_business_busnameauditing` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `audTime`    DATETIME         DEFAULT NULL,
  `auditing`   INT(11)          DEFAULT NULL,
  `busId`      INT(11)          DEFAULT NULL,
  `busaccId`   INT(11)          DEFAULT NULL,
  `createTime` DATETIME         DEFAULT NULL,
  `name`       VARCHAR(255)     DEFAULT NULL,
  `oldname`    VARCHAR(255)     DEFAULT NULL,
  `agentId`    INT(11)          DEFAULT NULL,
  `agentName`  VARCHAR(100)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 637
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_business_comment`
--

DROP TABLE IF EXISTS `runfast_business_comment`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_business_comment` (
  `id`                INT(11) NOT NULL AUTO_INCREMENT,
  `businessId`        INT(11)          DEFAULT NULL,
  `businessName`      VARCHAR(255)     DEFAULT NULL,
  `content`           LONGTEXT,
  `cost`              DOUBLE           DEFAULT NULL,
  `createTime`        DATETIME         DEFAULT NULL,
  `delicerId`         INT(11)          DEFAULT NULL,
  `delicerName`       VARCHAR(255)     DEFAULT NULL,
  `delicerTime`       DATETIME         DEFAULT NULL,
  `orderTime`         DATETIME         DEFAULT NULL,
  `score`             DOUBLE           DEFAULT NULL,
  `userId`            INT(11)          DEFAULT NULL,
  `userName`          VARCHAR(255)     DEFAULT NULL,
  `goodsSellId`       INT(11)          DEFAULT NULL,
  `goodsSellName`     VARCHAR(255)     DEFAULT NULL,
  `orderCode`         VARCHAR(255)     DEFAULT NULL,
  `pic`               VARCHAR(255)     DEFAULT NULL,
  `goodsSellRecordId` INT(11)          DEFAULT NULL,
  `agentId`           INT(11)          DEFAULT NULL,
  `agentName`         VARCHAR(100)     DEFAULT NULL,
  `shangstr`          VARCHAR(255)     DEFAULT NULL,
  `feedback`          LONGTEXT,
  `feedTime`          DATETIME         DEFAULT NULL,
  `recontent`         VARCHAR(255)     DEFAULT NULL,
  `recreateTime`      DATETIME         DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_gid_bid` (`businessId`, `goodsSellId`),
  KEY `idx_gid` (`goodsSellName`),
  KEY `idx_bid` (`businessId`),
  KEY `idx_oid` (`goodsSellRecordId`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 44938
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_business_level`
--

DROP TABLE IF EXISTS `runfast_business_level`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_business_level` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `content`    VARCHAR(255)     DEFAULT NULL,
  `createTime` DATETIME         DEFAULT NULL,
  `enScore`    INT(11)          DEFAULT NULL,
  `name`       VARCHAR(255)     DEFAULT NULL,
  `sort`       INT(11)          DEFAULT NULL,
  `startScore` INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 6
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_business_type`
--

DROP TABLE IF EXISTS `runfast_business_type`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_business_type` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `content`    VARCHAR(255)     DEFAULT NULL,
  `createTime` DATETIME         DEFAULT NULL,
  `name`       VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 26
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_businessalliance`
--

DROP TABLE IF EXISTS `runfast_businessalliance`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_businessalliance` (
  `id`           INT(11) NOT NULL AUTO_INCREMENT,
  `address`      VARCHAR(255)     DEFAULT NULL,
  `businessName` VARCHAR(255)     DEFAULT NULL,
  `contacts`     VARCHAR(255)     DEFAULT NULL,
  `createTime`   DATETIME         DEFAULT NULL,
  `local`        VARCHAR(255)     DEFAULT NULL,
  `moblie`       VARCHAR(255)     DEFAULT NULL,
  `remark`       VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 159
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_changerecord`
--

DROP TABLE IF EXISTS `runfast_changerecord`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_changerecord` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `busId`      INT(11)          DEFAULT NULL,
  `busname`    VARCHAR(255)     DEFAULT NULL,
  `createTime` DATETIME         DEFAULT NULL,
  `endTime`    DATETIME         DEFAULT NULL,
  `goodids`    LONGTEXT,
  `pid`        INT(11)          DEFAULT NULL,
  `pname`      VARCHAR(255)     DEFAULT NULL,
  `stanids`    LONGTEXT,
  `startTime`  DATETIME         DEFAULT NULL,
  `stops`      INT(11)          DEFAULT NULL,
  `type`       INT(11)          DEFAULT NULL,
  `cid`        INT(11)          DEFAULT NULL,
  `cname`      VARCHAR(255)     DEFAULT NULL,
  `ctype`      INT(11)          DEFAULT NULL,
  `status`     INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 65520
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_city`
--

DROP TABLE IF EXISTS `runfast_city`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_city` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `code`       VARCHAR(255)     DEFAULT NULL,
  `name`       VARCHAR(255)     DEFAULT NULL,
  `provinceId` INT(11)          DEFAULT NULL,
  `sort`       INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 371
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_commentpic`
--

DROP TABLE IF EXISTS `runfast_commentpic`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_commentpic` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `createTime` DATETIME         DEFAULT NULL,
  `fid`        INT(11)          DEFAULT NULL,
  `imgUrl`     VARCHAR(255)     DEFAULT NULL,
  `name`       VARCHAR(255)     DEFAULT NULL,
  `pid`        INT(11)          DEFAULT NULL,
  `sid`        INT(11)          DEFAULT NULL,
  `sname`      VARCHAR(255)     DEFAULT NULL,
  `userId`     INT(11)          DEFAULT NULL,
  `userName`   VARCHAR(255)     DEFAULT NULL,
  `type`       INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_fid` (`fid`, `pid`, `sid`, `userId`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_complain`
--

DROP TABLE IF EXISTS `runfast_complain`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_complain` (
  `id`                INT(11) NOT NULL AUTO_INCREMENT,
  `businessId`        INT(11)          DEFAULT NULL,
  `businessName`      VARCHAR(255)     DEFAULT NULL,
  `content`           LONGTEXT CHARACTER SET utf8mb4,
  `createTime`        DATETIME         DEFAULT NULL,
  `goodsSellId`       INT(11)          DEFAULT NULL,
  `goodsSellName`     VARCHAR(255)     DEFAULT NULL,
  `isCheck`           INT(11)          DEFAULT NULL,
  `isReply`           INT(11)          DEFAULT NULL,
  `replyContent`      LONGTEXT,
  `replyTime`         DATETIME         DEFAULT NULL,
  `userEmail`         VARCHAR(255)     DEFAULT NULL,
  `userId`            INT(11)          DEFAULT NULL,
  `userName`          VARCHAR(255)     DEFAULT NULL,
  `goodsSellRecordId` INT(11)          DEFAULT NULL,
  `goodsSellCode`     VARCHAR(255)     DEFAULT NULL,
  `agentId`           INT(11)          DEFAULT NULL,
  `agentName`         VARCHAR(100)     DEFAULT NULL,
  `type`              INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 2768
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_coupon`
--

DROP TABLE IF EXISTS `runfast_coupon`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_coupon` (
  `id`           INT(11) NOT NULL AUTO_INCREMENT,
  `businessName` VARCHAR(255)     DEFAULT NULL,
  `createTime`   DATETIME         DEFAULT NULL,
  `end`          DATETIME         DEFAULT NULL,
  `full`         DECIMAL(19, 2)   DEFAULT NULL,
  `name`         VARCHAR(255)     DEFAULT NULL,
  `price`        DECIMAL(19, 2)   DEFAULT NULL,
  `quantity`     INT(11)          DEFAULT NULL,
  `range1`       INT(11)          DEFAULT NULL,
  `rangeId`      INT(11)          DEFAULT NULL,
  `start`        DATETIME         DEFAULT NULL,
  `limitedNum`   INT(11)          DEFAULT NULL,
  `agentId`      INT(11)          DEFAULT NULL,
  `agentName`    VARCHAR(100)     DEFAULT NULL,
  `enduse`       DATETIME         DEFAULT NULL,
  `islimited`    INT(11)          DEFAULT NULL,
  `startuse`     DATETIME         DEFAULT NULL,
  `type`         INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_rangeId_range1_quantity_end` (`end`, `quantity`, `range1`, `rangeId`, `agentId`),
  KEY `idx_quantity_end` (`end`, `quantity`),
  KEY `idx_rang1_rangeId` (`end`, `quantity`, `range1`, `rangeId`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 40
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_couponrecord`
--

DROP TABLE IF EXISTS `runfast_couponrecord`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_couponrecord` (
  `id`           INT(11) NOT NULL AUTO_INCREMENT,
  `couponId`     INT(11)          DEFAULT NULL,
  `couponName`   VARCHAR(255)     DEFAULT NULL,
  `createTime`   DATETIME         DEFAULT NULL,
  `cuserId`      INT(11)          DEFAULT NULL,
  `cuserName`    VARCHAR(100)     DEFAULT NULL,
  `end`          DATETIME         DEFAULT NULL,
  `full`         DECIMAL(19, 2)   DEFAULT NULL,
  `price`        DECIMAL(19, 2)   DEFAULT NULL,
  `quantity`     INT(11)          DEFAULT NULL,
  `start`        DATETIME         DEFAULT NULL,
  `businessName` VARCHAR(255)     DEFAULT NULL,
  `range1`       INT(11)          DEFAULT NULL,
  `rangeId`      INT(11)          DEFAULT NULL,
  `userd`        INT(11)          DEFAULT NULL,
  `agentId`      INT(11)          DEFAULT NULL,
  `agentName`    VARCHAR(100)     DEFAULT NULL,
  `enduse`       DATETIME         DEFAULT NULL,
  `islimited`    INT(11)          DEFAULT NULL,
  `startuse`     DATETIME         DEFAULT NULL,
  `type`         INT(11)          DEFAULT NULL,
  `yiyuangou`    INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_rangeId_range1_cuserId_end` (`cuserId`, `end`, `range1`, `rangeId`),
  KEY `idx_agentId_cuserId_end_range1` (`cuserId`, `end`, `range1`, `agentId`),
  KEY `idx_agentId_cuserId_end_range1_used` (`cuserId`, `end`, `start`, `range1`, `rangeId`, `userd`, `agentId`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 2175
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_creditlevel`
--

DROP TABLE IF EXISTS `runfast_creditlevel`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_creditlevel` (
  `id`              INT(11) NOT NULL AUTO_INCREMENT,
  `commission`      DECIMAL(19, 2)   DEFAULT NULL,
  `createTime`      DATETIME         DEFAULT NULL,
  `endgrade`        INT(11)          DEFAULT NULL,
  `sort`            INT(11)          DEFAULT NULL,
  `startgrade`      INT(11)          DEFAULT NULL,
  `addprice`        DECIMAL(19, 2)   DEFAULT NULL,
  `commissionDay`   DECIMAL(19, 2)   DEFAULT NULL,
  `commissionNight` DECIMAL(19, 2)   DEFAULT NULL,
  `endTimeDay`      DATETIME         DEFAULT NULL,
  `endTimeNight`    DATETIME         DEFAULT NULL,
  `startTimeDay`    DATETIME         DEFAULT NULL,
  `startTimeNight`  DATETIME         DEFAULT NULL,
  `agentId`         INT(11)          DEFAULT NULL,
  `agentName`       VARCHAR(100)     DEFAULT NULL,
  `distan`          DOUBLE           DEFAULT NULL,
  `price`           DECIMAL(19, 2)   DEFAULT NULL,
  `maxprice`        DECIMAL(19, 2)   DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 42
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_cuser`
--

DROP TABLE IF EXISTS `runfast_cuser`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_cuser` (
  `id`              INT(11) NOT NULL      AUTO_INCREMENT,
  `areaName`        VARCHAR(255)          DEFAULT NULL,
  `byRoles`         VARCHAR(255)          DEFAULT NULL,
  `card`            VARCHAR(255)          DEFAULT NULL,
  `cityId`          VARCHAR(255)          DEFAULT NULL,
  `cityName`        VARCHAR(255)          DEFAULT NULL,
  `code`            VARCHAR(255)          DEFAULT NULL,
  `countyId`        VARCHAR(255)          DEFAULT NULL,
  `countyName`      VARCHAR(255)          DEFAULT NULL,
  `createTime`      DATETIME              DEFAULT NULL,
  `logTime`         DATETIME              DEFAULT NULL,
  `mobile`          VARCHAR(255)          DEFAULT NULL,
  `name`            VARCHAR(255)          DEFAULT NULL,
  `nickname`        VARCHAR(255)
                    CHARACTER SET utf8mb4 DEFAULT NULL,
  `openid`          VARCHAR(255)          DEFAULT NULL,
  `orgType`         VARCHAR(255)          DEFAULT NULL,
  `password`        VARCHAR(255)          DEFAULT NULL,
  `pic`             VARCHAR(255)          DEFAULT NULL,
  `remainder`       DECIMAL(19, 2)        DEFAULT NULL,
  `townId`          VARCHAR(255)          DEFAULT NULL,
  `townName`        VARCHAR(255)          DEFAULT NULL,
  `provinceId`      VARCHAR(255)          DEFAULT NULL,
  `qq`              VARCHAR(255)          DEFAULT NULL,
  `xinl`            VARCHAR(255)          DEFAULT NULL,
  `email`           VARCHAR(255)          DEFAULT NULL,
  `gender`          INT(11)               DEFAULT NULL,
  `score`           DOUBLE(10, 0)         DEFAULT NULL,
  `minmonety`       DECIMAL(19, 2)        DEFAULT NULL,
  `consume`         DECIMAL(19, 2)        DEFAULT NULL,
  `rnum`            INT(11)               DEFAULT NULL,
  `totalremainder`  DECIMAL(19, 2)        DEFAULT NULL,
  `bdpushChannelId` VARCHAR(255)          DEFAULT NULL,
  `bdpushUserId`    VARCHAR(255)          DEFAULT NULL,
  `bptype`          INT(11)               DEFAULT NULL,
  `otherId`         VARCHAR(255)          DEFAULT NULL,
  `pushType`        INT(11)               DEFAULT NULL,
  `alias`           VARCHAR(255)          DEFAULT NULL,
  `thirdLoginId`    VARCHAR(255)          DEFAULT NULL,
  `thirdLoginType`  INT(11)               DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_mobile` (`mobile`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 489003
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_cuseraddress`
--

DROP TABLE IF EXISTS `runfast_cuseraddress`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_cuseraddress` (
  `id`           INT(11) NOT NULL AUTO_INCREMENT,
  `address`      VARCHAR(255)     DEFAULT NULL,
  `cid`          INT(11)          DEFAULT NULL,
  `createTime`   DATETIME         DEFAULT NULL,
  `mobile`       VARCHAR(255)     DEFAULT NULL,
  `name`         VARCHAR(255)     DEFAULT NULL,
  `cname`        VARCHAR(255)     DEFAULT NULL,
  `phone`        VARCHAR(255)     DEFAULT NULL,
  `userAddress`  VARCHAR(255)     DEFAULT NULL,
  `isChoose`     INT(11)          DEFAULT NULL,
  `latitude`     VARCHAR(255)     DEFAULT NULL,
  `longitude`    VARCHAR(255)     DEFAULT NULL,
  `cityName`     VARCHAR(255)     DEFAULT NULL,
  `countyName`   VARCHAR(255)     DEFAULT NULL,
  `provinceName` VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_cid_mob` (`cid`, `mobile`),
  KEY `idx_cid` (`cid`),
  KEY `idx_iscoose` (`cid`, `isChoose`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 145309
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_custom`
--

DROP TABLE IF EXISTS `runfast_custom`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_custom` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `createTime` DATETIME         DEFAULT NULL,
  `mobile`     VARCHAR(255)     DEFAULT NULL,
  `qrcode`     VARCHAR(255)     DEFAULT NULL,
  `agenId`     INT(11)          DEFAULT NULL,
  `agenName`   VARCHAR(255)     DEFAULT NULL,
  `isMaster`   INT(11)          DEFAULT NULL,
  `type`       INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 17
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_deliver_cost`
--

DROP TABLE IF EXISTS `runfast_deliver_cost`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_deliver_cost` (
  `id`              INT(11) NOT NULL AUTO_INCREMENT,
  `charge`          DOUBLE           DEFAULT NULL,
  `createTime`      DATETIME         DEFAULT NULL,
  `noCharge`        DOUBLE           DEFAULT NULL,
  `timeCost1`       DOUBLE           DEFAULT NULL,
  `timeCost2`       DOUBLE           DEFAULT NULL,
  `endTimeDay1`     TIME             DEFAULT NULL,
  `endTimeNight2`   TIME             DEFAULT NULL,
  `startTimeDay1`   TIME             DEFAULT NULL,
  `startTimeNight2` TIME             DEFAULT NULL,
  `charge1`         DECIMAL(19, 2)   DEFAULT NULL,
  `charge2`         DECIMAL(19, 2)   DEFAULT NULL,
  `maxCharge`       DECIMAL(19, 2)   DEFAULT NULL,
  `maxDistance`     DOUBLE           DEFAULT NULL,
  `area`            VARCHAR(255)     DEFAULT NULL,
  `areaName`        VARCHAR(255)     DEFAULT NULL,
  `cityId`          VARCHAR(255)     DEFAULT NULL,
  `cityName`        VARCHAR(255)     DEFAULT NULL,
  `countyId`        VARCHAR(255)     DEFAULT NULL,
  `countyName`      VARCHAR(255)     DEFAULT NULL,
  `isdefault`       INT(11)          DEFAULT NULL,
  `updatehour`      INT(11)          DEFAULT NULL,
  `agentId`         INT(11)          DEFAULT NULL,
  `agentName`       VARCHAR(100)     DEFAULT NULL,
  `speed`           DOUBLE(11, 2)    DEFAULT NULL,
  `distRange`       LONGTEXT,
  PRIMARY KEY (`id`),
  KEY `idx_agentId` (`agentId`),
  KEY `idx_def` (`isdefault`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 37
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_dispatching`
--

DROP TABLE IF EXISTS `runfast_dispatching`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_dispatching` (
  `id`              INT(11) NOT NULL AUTO_INCREMENT,
  `agentId`         INT(11)          DEFAULT NULL,
  `agentName`       VARCHAR(100)     DEFAULT NULL,
  `bigSupportId`    INT(11)          DEFAULT NULL,
  `bigSupportName`  VARCHAR(255)     DEFAULT NULL,
  `buyNum`          INT(11)          DEFAULT NULL,
  `charge`          DECIMAL(19, 2)   DEFAULT NULL,
  `disType`         INT(11)          DEFAULT NULL,
  `firstWeight`     DECIMAL(19, 2)   DEFAULT NULL,
  `firstWeightPay`  DECIMAL(19, 2)   DEFAULT NULL,
  `fixedPay`        DECIMAL(19, 2)   DEFAULT NULL,
  `logisticsId`     INT(11)          DEFAULT NULL,
  `logisticsName`   VARCHAR(255)     DEFAULT NULL,
  `name`            VARCHAR(255)     DEFAULT NULL,
  `pay`             DECIMAL(19, 2)   DEFAULT NULL,
  `promptMsg`       VARCHAR(255)     DEFAULT NULL,
  `renewPay`        DECIMAL(19, 2)   DEFAULT NULL,
  `startArea`       VARCHAR(255)     DEFAULT NULL,
  `startAreaId`     INT(11)          DEFAULT NULL,
  `startCity`       VARCHAR(255)     DEFAULT NULL,
  `startCityId`     INT(11)          DEFAULT NULL,
  `startProvince`   VARCHAR(255)     DEFAULT NULL,
  `startProvinceId` INT(11)          DEFAULT NULL,
  `logisticcode`    VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_agent` (`agentId`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 26
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_dispatching_chargesection`
--

DROP TABLE IF EXISTS `runfast_dispatching_chargesection`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_dispatching_chargesection` (
  `id`            INT(11) NOT NULL AUTO_INCREMENT,
  `disId`         INT(11)          DEFAULT NULL,
  `endSection`    DECIMAL(19, 2)   DEFAULT NULL,
  `level`         INT(11)          DEFAULT NULL,
  `sectionCharge` DECIMAL(19, 2)   DEFAULT NULL,
  `startSection`  DECIMAL(19, 2)   DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_dispatching_end`
--

DROP TABLE IF EXISTS `runfast_dispatching_end`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_dispatching_end` (
  `id`            INT(11) NOT NULL AUTO_INCREMENT,
  `did`           INT(11)          DEFAULT NULL,
  `endArea`       VARCHAR(255)     DEFAULT NULL,
  `endAreaId`     INT(11)          DEFAULT NULL,
  `endCity`       VARCHAR(255)     DEFAULT NULL,
  `endCityId`     INT(11)          DEFAULT NULL,
  `endProvince`   VARCHAR(255)     DEFAULT NULL,
  `endProvinceId` INT(11)          DEFAULT NULL,
  `level`         INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 83
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_distributionassess`
--

DROP TABLE IF EXISTS `runfast_distributionassess`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_distributionassess` (
  `id`                INT(11) NOT NULL AUTO_INCREMENT,
  `assessTime`        DATETIME         DEFAULT NULL,
  `assessman`         VARCHAR(255)     DEFAULT NULL,
  `consume`           DECIMAL(19, 2)   DEFAULT NULL,
  `distributionTime`  DATETIME         DEFAULT NULL,
  `level`             INT(11)          DEFAULT NULL,
  `orderNumber`       VARCHAR(255)     DEFAULT NULL,
  `assessId`          INT(11)          DEFAULT NULL,
  `goodsSellRecordId` INT(11)          DEFAULT NULL,
  `grade`             INT(11)          DEFAULT NULL,
  `orderCode`         VARCHAR(255)     DEFAULT NULL,
  `price`             DOUBLE           DEFAULT NULL,
  `score`             DOUBLE           DEFAULT NULL,
  `shopperId`         INT(11)          DEFAULT NULL,
  `shopperName`       VARCHAR(255)     DEFAULT NULL,
  `shopperTime`       DATETIME         DEFAULT NULL,
  `userId`            INT(11)          DEFAULT NULL,
  `userName`          VARCHAR(255)     DEFAULT NULL,
  `content`           VARCHAR(255)     DEFAULT NULL,
  `agentId`           INT(11)          DEFAULT NULL,
  `agentName`         VARCHAR(100)     DEFAULT NULL,
  `qishoustr`         VARCHAR(255)     DEFAULT NULL,
  `recontent`         VARCHAR(255)     DEFAULT NULL,
  `recreateTime`      DATETIME         DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 44864
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_enshrine`
--

DROP TABLE IF EXISTS `runfast_enshrine`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_enshrine` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `cid`        INT(11)          DEFAULT NULL,
  `createTime` DATETIME         DEFAULT NULL,
  `name`       VARCHAR(255)     DEFAULT NULL,
  `shopId`     INT(11)          DEFAULT NULL,
  `shopname`   VARCHAR(255)     DEFAULT NULL,
  `mobile`     VARCHAR(255)     DEFAULT NULL,
  `openid`     VARCHAR(255)     DEFAULT NULL,
  `type`       INT(11)          DEFAULT NULL,
  `imgPath`    VARCHAR(255)     DEFAULT NULL,
  `startPay`   DECIMAL(19, 2)   DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_shopId_cid_openid_type` (`cid`, `shopId`, `openid`, `type`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 18704
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_entrybill`
--

DROP TABLE IF EXISTS `runfast_entrybill`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_entrybill` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `allowState` VARCHAR(255)     DEFAULT NULL,
  `allowTime`  DATETIME         DEFAULT NULL,
  `allower`    VARCHAR(255)     DEFAULT NULL,
  `area`       VARCHAR(255)     DEFAULT NULL,
  `areaId`     INT(11)          DEFAULT NULL,
  `city`       VARCHAR(255)     DEFAULT NULL,
  `cityId`     INT(11)          DEFAULT NULL,
  `createTime` DATETIME         DEFAULT NULL,
  `entryCode`  VARCHAR(255)     DEFAULT NULL,
  `entryor`    VARCHAR(255)     DEFAULT NULL,
  `nums`       INT(11)          DEFAULT NULL,
  `total`      DECIMAL(19, 2)   DEFAULT NULL,
  `typeId`     INT(11)          DEFAULT NULL,
  `agentId`    INT(11)          DEFAULT NULL,
  `agentName`  VARCHAR(100)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 7
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_entryhistory`
--

DROP TABLE IF EXISTS `runfast_entryhistory`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_entryhistory` (
  `id`          INT(11) NOT NULL AUTO_INCREMENT,
  `area`        VARCHAR(255)     DEFAULT NULL,
  `areaId`      INT(11)          DEFAULT NULL,
  `city`        VARCHAR(255)     DEFAULT NULL,
  `cityId`      INT(11)          DEFAULT NULL,
  `createTime`  DATETIME         DEFAULT NULL,
  `entryCode`   VARCHAR(255)     DEFAULT NULL,
  `entryor`     VARCHAR(255)     DEFAULT NULL,
  `foodId`      INT(11)          DEFAULT NULL,
  `name`        VARCHAR(255)     DEFAULT NULL,
  `nums`        INT(11)          DEFAULT NULL,
  `total`       DECIMAL(19, 2)   DEFAULT NULL,
  `typeId`      INT(11)          DEFAULT NULL,
  `foodstand`   VARCHAR(255)     DEFAULT NULL,
  `foodstandId` INT(11)          DEFAULT NULL,
  `agentId`     INT(11)          DEFAULT NULL,
  `agentName`   VARCHAR(100)     DEFAULT NULL,
  `proCode`     VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 12
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_entryproduct`
--

DROP TABLE IF EXISTS `runfast_entryproduct`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_entryproduct` (
  `id`                  INT(11) NOT NULL AUTO_INCREMENT,
  `countPromotion`      DECIMAL(19, 2)   DEFAULT NULL,
  `countPurchasePricen` DECIMAL(19, 2)   DEFAULT NULL,
  `entryBillId`         INT(11)          DEFAULT NULL,
  `foodId`              INT(11)          DEFAULT NULL,
  `name`                VARCHAR(255)     DEFAULT NULL,
  `nums`                INT(11)          DEFAULT NULL,
  `proCode`             VARCHAR(255)     DEFAULT NULL,
  `standard`            VARCHAR(255)     DEFAULT NULL,
  `standardId`          INT(11)          DEFAULT NULL,
  `supportCode`         VARCHAR(255)     DEFAULT NULL,
  `supportId`           INT(11)          DEFAULT NULL,
  `supportName`         VARCHAR(255)     DEFAULT NULL,
  `total`               DECIMAL(19, 2)   DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 12
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_favorites`
--

DROP TABLE IF EXISTS `runfast_favorites`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_favorites` (
  `id`             INT(11) NOT NULL AUTO_INCREMENT,
  `bossId`         INT(11)          DEFAULT NULL,
  `countPromotion` DECIMAL(19, 2)   DEFAULT NULL,
  `createTime`     DATETIME         DEFAULT NULL,
  `foodId`         INT(11)          DEFAULT NULL,
  `foodName`       VARCHAR(255)     DEFAULT NULL,
  `imglogo`        VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_bossid` (`bossId`, `foodId`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_food`
--

DROP TABLE IF EXISTS `runfast_food`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_food` (
  `id`            INT(11) NOT NULL AUTO_INCREMENT,
  `aid`           INT(11)          DEFAULT NULL,
  `brandId`       INT(11)          DEFAULT NULL,
  `brandName`     VARCHAR(255)     DEFAULT NULL,
  `createTime`    DATETIME         DEFAULT NULL,
  `imglogo`       VARCHAR(255)     DEFAULT NULL,
  `info`          LONGTEXT,
  `ishot`         INT(11)          DEFAULT NULL,
  `lowerPrice`    DECIMAL(19, 2)   DEFAULT NULL,
  `name`          VARCHAR(255)     DEFAULT NULL,
  `saleNums`      INT(11)          DEFAULT NULL,
  `score`         INT(11)          DEFAULT NULL,
  `sort`          INT(11)          DEFAULT NULL,
  `supportCode`   VARCHAR(255)     DEFAULT NULL,
  `supportId`     INT(11)          DEFAULT NULL,
  `supportName`   VARCHAR(255)     DEFAULT NULL,
  `typeId`        INT(11)          DEFAULT NULL,
  `typeId1`       INT(11)          DEFAULT NULL,
  `typeId2`       INT(11)          DEFAULT NULL,
  `typeId3`       INT(11)          DEFAULT NULL,
  `typeName`      VARCHAR(255)     DEFAULT NULL,
  `typeName1`     VARCHAR(255)     DEFAULT NULL,
  `typeName2`     VARCHAR(255)     DEFAULT NULL,
  `typeName3`     VARCHAR(255)     DEFAULT NULL,
  `unitId`        INT(11)          DEFAULT NULL,
  `unitName`      VARCHAR(255)     DEFAULT NULL,
  `upOrDown`      INT(11)          DEFAULT NULL,
  `islimited`     INT(11)          DEFAULT NULL,
  `limiEndTime`   DATETIME         DEFAULT NULL,
  `limiStartTime` DATETIME         DEFAULT NULL,
  `limitNum`      INT(11)          DEFAULT NULL,
  `modelId`       INT(11)          DEFAULT NULL,
  `tableName`     VARCHAR(255)     DEFAULT NULL,
  `lowerRelPrice` DECIMAL(19, 2)   DEFAULT NULL,
  `saleService`   LONGTEXT,
  `minimglogo`    VARCHAR(255)     DEFAULT NULL,
  `isOwn`         INT(11)          DEFAULT NULL,
  `sellchoose`    VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 207
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_food_img`
--

DROP TABLE IF EXISTS `runfast_food_img`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_food_img` (
  `id`       INT(11) NOT NULL AUTO_INCREMENT,
  `foodId`   INT(11)          DEFAULT NULL,
  `foodName` VARCHAR(255)     DEFAULT NULL,
  `imgUrl`   VARCHAR(255)     DEFAULT NULL,
  `title`    VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_foodid` (`foodId`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 3591
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_food_pro`
--

DROP TABLE IF EXISTS `runfast_food_pro`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_food_pro` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `foodId`     INT(11)          DEFAULT NULL,
  `standardId` INT(11)          DEFAULT NULL,
  `tipKey`     VARCHAR(255)     DEFAULT NULL,
  `tipValue`   VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_food` (`foodId`, `standardId`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 4677
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_food_standard`
--

DROP TABLE IF EXISTS `runfast_food_standard`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_food_standard` (
  `id`            INT(11) NOT NULL AUTO_INCREMENT,
  `agentId`       INT(11)          DEFAULT NULL,
  `agentName`     VARCHAR(100)     DEFAULT NULL,
  `barCode`       VARCHAR(255)     DEFAULT NULL,
  `editTime`      DATETIME         DEFAULT NULL,
  `foodId`        INT(11)          DEFAULT NULL,
  `mixBuyNums`    INT(11)          DEFAULT NULL,
  `mixnums`       INT(11)          DEFAULT NULL,
  `presell`       INT(11)          DEFAULT NULL,
  `price`         DECIMAL(19, 2)   DEFAULT NULL,
  `proCode`       VARCHAR(255)     DEFAULT NULL,
  `realPrice`     DECIMAL(19, 2)   DEFAULT NULL,
  `supportId`     INT(11)          DEFAULT NULL,
  `supportName`   VARCHAR(255)     DEFAULT NULL,
  `yunPrice`      DECIMAL(19, 2)   DEFAULT NULL,
  `aceptpay`      INT(11)          DEFAULT NULL,
  `paynum`        DECIMAL(19, 2)   DEFAULT NULL,
  `foodName`      VARCHAR(255)     DEFAULT NULL,
  `islimited`     INT(11)          DEFAULT NULL,
  `limiEndTime`   DATETIME         DEFAULT NULL,
  `limiStartTime` DATETIME         DEFAULT NULL,
  `limitNum`      INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_foodid` (`foodId`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 573
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `runfast_food_view`
--

DROP TABLE IF EXISTS `runfast_food_view`;
/*!50001 DROP VIEW IF EXISTS `runfast_food_view`*/;
SET @saved_cs_client = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `runfast_food_view` AS
  SELECT
    1 AS `id`,
    1 AS `realPrice`,
    1 AS `price`,
    1 AS `proCode`,
    1 AS `foodId`,
    1 AS `supportName`,
    1 AS `supportId`,
    1 AS `yunPrice`,
    1 AS `name`,
    1 AS `unitId`,
    1 AS `unitName`,
    1 AS `typeId`,
    1 AS `typeName`,
    1 AS `upOrDown`,
    1 AS `imglogo`,
    1 AS `info`,
    1 AS `sort`,
    1 AS `saleNums`,
    1 AS `brandId`,
    1 AS `brandName` */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `runfast_foodcomment`
--

DROP TABLE IF EXISTS `runfast_foodcomment`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_foodcomment` (
  `id`           INT(11) NOT NULL AUTO_INCREMENT,
  `agentId`      INT(11)          DEFAULT NULL,
  `agentName`    VARCHAR(100)     DEFAULT NULL,
  `context`      VARCHAR(255)     DEFAULT NULL,
  `createTime`   DATETIME         DEFAULT NULL,
  `feedTime`     DATETIME         DEFAULT NULL,
  `feedback`     LONGTEXT,
  `foodId`       INT(11)          DEFAULT NULL,
  `foodname`     VARCHAR(255)     DEFAULT NULL,
  `orderCode`    VARCHAR(255)     DEFAULT NULL,
  `orderId`      INT(11)          DEFAULT NULL,
  `score`        INT(11)          DEFAULT NULL,
  `standardId`   INT(11)          DEFAULT NULL,
  `standardName` VARCHAR(255)     DEFAULT NULL,
  `supportId`    INT(11)          DEFAULT NULL,
  `supportName`  VARCHAR(255)     DEFAULT NULL,
  `userId`       INT(11)          DEFAULT NULL,
  `userName`     VARCHAR(255)     DEFAULT NULL,
  `kscore`       INT(11)          DEFAULT NULL,
  `pscore`       INT(11)          DEFAULT NULL,
  `corderId`     INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_foodid` (`foodId`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_foodfield`
--

DROP TABLE IF EXISTS `runfast_foodfield`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_foodfield` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `createTime` DATETIME         DEFAULT NULL,
  `fid`        INT(11)          DEFAULT NULL,
  `info`       VARCHAR(255)     DEFAULT NULL,
  `name`       VARCHAR(255)     DEFAULT NULL,
  `sort`       INT(11)          DEFAULT NULL,
  `titleId`    INT(11)          DEFAULT NULL,
  `type`       INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_fid` (`fid`),
  KEY `idx_tid` (`titleId`),
  KEY `idx_fid_tid` (`fid`, `titleId`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 108
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_foodmodel`
--

DROP TABLE IF EXISTS `runfast_foodmodel`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_foodmodel` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `createTime` DATETIME         DEFAULT NULL,
  `name`       VARCHAR(255)     DEFAULT NULL,
  `tableName`  VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 5
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_foodrecomment`
--

DROP TABLE IF EXISTS `runfast_foodrecomment`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_foodrecomment` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `cid`        INT(11)          DEFAULT NULL,
  `cname`      VARCHAR(255)     DEFAULT NULL,
  `content`    VARCHAR(255)     DEFAULT NULL,
  `createTime` DATETIME         DEFAULT NULL,
  `pid`        INT(11)          DEFAULT NULL,
  `type`       INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_tid_pid` (`cid`, `pid`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 196
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_foodunit`
--

DROP TABLE IF EXISTS `runfast_foodunit`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_foodunit` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `createTime` DATETIME         DEFAULT NULL,
  `name`       VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 14
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_goodclass`
--

DROP TABLE IF EXISTS `runfast_goodclass`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_goodclass` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `byid`       INT(11)          DEFAULT NULL,
  `classCode`  VARCHAR(255)     DEFAULT NULL,
  `createTime` DATETIME         DEFAULT NULL,
  `imgpath`    VARCHAR(255)     DEFAULT NULL,
  `name`       VARCHAR(255)     DEFAULT NULL,
  `number`     VARCHAR(255)     DEFAULT NULL,
  `sort`       INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 21
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_goods_sell`
--

DROP TABLE IF EXISTS `runfast_goods_sell`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_goods_sell` (
  `id`            INT(11) NOT NULL AUTO_INCREMENT,
  `businessId`    INT(11)          DEFAULT NULL,
  `businessName`  VARCHAR(255)     DEFAULT NULL,
  `content`       VARCHAR(255)     DEFAULT NULL,
  `createTime`    DATETIME         DEFAULT NULL,
  `discount`      DOUBLE           DEFAULT NULL,
  `imgPath`       VARCHAR(255)     DEFAULT NULL,
  `name`          VARCHAR(255)     DEFAULT NULL,
  `price`         DOUBLE           DEFAULT NULL,
  `sellTypeId`    INT(11)          DEFAULT NULL,
  `sellTypeName`  VARCHAR(255)     DEFAULT NULL,
  `status`        INT(11)          DEFAULT NULL,
  `typeId`        INT(11)          DEFAULT NULL,
  `typeName`      VARCHAR(255)     DEFAULT NULL,
  `salesnum`      INT(11)          DEFAULT NULL,
  `num`           INT(11)          DEFAULT NULL,
  `Star`          INT(11)          DEFAULT NULL,
  `ptype`         INT(11)          DEFAULT NULL,
  `agentId`       INT(11)          DEFAULT NULL,
  `agentName`     VARCHAR(100)     DEFAULT NULL,
  `mini_imgPath`  VARCHAR(255)     DEFAULT NULL,
  `islimited`     INT(11)          DEFAULT NULL,
  `limiEndTime`   DATETIME         DEFAULT NULL,
  `limiStartTime` DATETIME         DEFAULT NULL,
  `limitNum`      INT(11)          DEFAULT NULL,
  `limittype`     INT(11)          DEFAULT NULL,
  `deleted`       BIT(1)           DEFAULT b'0',
  PRIMARY KEY (`id`),
  KEY `idx_businessId` (`businessId`),
  KEY `idx_business_status` (`businessId`, `status`),
  KEY `idx_bbns` (`businessId`, `businessName`, `name`, `status`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 197633
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_goods_sell_children`
--

DROP TABLE IF EXISTS `runfast_goods_sell_children`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_goods_sell_children` (
  `id`                    INT(11) NOT NULL AUTO_INCREMENT,
  `businessId`            INT(11)          DEFAULT NULL,
  `businessName`          VARCHAR(255)     DEFAULT NULL,
  `goodsSellId`           INT(11)          DEFAULT NULL,
  `goodsSellName`         VARCHAR(255)     DEFAULT NULL,
  `num`                   INT(11)          DEFAULT NULL,
  `orderCode`             VARCHAR(255)     DEFAULT NULL,
  `pid`                   INT(11)          DEFAULT NULL,
  `price`                 DOUBLE           DEFAULT NULL,
  `totalprice`            DOUBLE           DEFAULT NULL,
  `goodsSellOptionId`     INT(11)          DEFAULT NULL,
  `goodsSellOptionName`   VARCHAR(255)     DEFAULT NULL,
  `goodsSellStandardId`   INT(11)          DEFAULT NULL,
  `goodsSellStandardName` VARCHAR(255)     DEFAULT NULL,
  `createTime`            DATETIME         DEFAULT NULL,
  `status`                INT(11)          DEFAULT NULL,
  `userId`                INT(11)          DEFAULT NULL,
  `disprice`              DECIMAL(19, 2)   DEFAULT NULL,
  `optionIds`             VARCHAR(255)     DEFAULT NULL,
  `ptype`                 INT(11)          DEFAULT NULL,
  `activity`              INT(11)          DEFAULT NULL,
  `errend`                INT(11)          DEFAULT NULL,
  `activityId`            INT(11)          DEFAULT NULL,
  `activityName`          VARCHAR(255)     DEFAULT NULL,
  `activityType`          INT(11)          DEFAULT NULL,
  `goods`                 VARCHAR(3000)    DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_orderCode` (`orderCode`),
  KEY `idx_cuserid_goodsSellId_createTime` (`goodsSellId`, `createTime`, `status`, `userId`),
  KEY `idx_pid` (`pid`),
  KEY `idx_business_status_goodssellid` (`createTime`, `status`),
  KEY `idx_sum_price` (`goodsSellName`, `num`, `totalprice`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 963366
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_goods_sell_copy`
--

DROP TABLE IF EXISTS `runfast_goods_sell_copy`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_goods_sell_copy` (
  `id`            INT(11) NOT NULL AUTO_INCREMENT,
  `businessId`    INT(11)          DEFAULT NULL,
  `businessName`  VARCHAR(255)     DEFAULT NULL,
  `content`       VARCHAR(255)     DEFAULT NULL,
  `createTime`    DATETIME         DEFAULT NULL,
  `discount`      DOUBLE           DEFAULT NULL,
  `imgPath`       VARCHAR(255)     DEFAULT NULL,
  `name`          VARCHAR(255)     DEFAULT NULL,
  `price`         DOUBLE           DEFAULT NULL,
  `sellTypeId`    INT(11)          DEFAULT NULL,
  `sellTypeName`  VARCHAR(255)     DEFAULT NULL,
  `status`        INT(11)          DEFAULT NULL,
  `typeId`        INT(11)          DEFAULT NULL,
  `typeName`      VARCHAR(255)     DEFAULT NULL,
  `salesnum`      INT(11)          DEFAULT NULL,
  `num`           INT(11)          DEFAULT NULL,
  `Star`          INT(11)          DEFAULT NULL,
  `ptype`         INT(11)          DEFAULT NULL,
  `agentId`       INT(11)          DEFAULT NULL,
  `agentName`     VARCHAR(100)     DEFAULT NULL,
  `mini_imgPath`  VARCHAR(255)     DEFAULT NULL,
  `islimited`     INT(11)          DEFAULT NULL,
  `limiEndTime`   DATETIME         DEFAULT NULL,
  `limiStartTime` DATETIME         DEFAULT NULL,
  `limitNum`      INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_businessId` (`businessId`),
  KEY `idx_business_status` (`businessId`, `status`),
  KEY `idx_bbns` (`businessId`, `businessName`, `name`, `status`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 163350
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_goods_sell_option`
--

DROP TABLE IF EXISTS `runfast_goods_sell_option`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_goods_sell_option` (
  `id`            INT(11) NOT NULL AUTO_INCREMENT,
  `barCode`       VARCHAR(255)     DEFAULT NULL,
  `businessId`    INT(11)          DEFAULT NULL,
  `businessName`  VARCHAR(255)     DEFAULT NULL,
  `goodsSellId`   INT(11)          DEFAULT NULL,
  `goodsSellName` VARCHAR(255)     DEFAULT NULL,
  `name`          VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_goodsSellId` (`goodsSellId`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 79239
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_goods_sell_option_copy`
--

DROP TABLE IF EXISTS `runfast_goods_sell_option_copy`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_goods_sell_option_copy` (
  `id`            INT(11) NOT NULL AUTO_INCREMENT,
  `barCode`       VARCHAR(255)     DEFAULT NULL,
  `businessId`    INT(11)          DEFAULT NULL,
  `businessName`  VARCHAR(255)     DEFAULT NULL,
  `goodsSellId`   INT(11)          DEFAULT NULL,
  `goodsSellName` VARCHAR(255)     DEFAULT NULL,
  `name`          VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_goodsSellId` (`goodsSellId`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 64970
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_goods_sell_out_status`
--

DROP TABLE IF EXISTS `runfast_goods_sell_out_status`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_goods_sell_out_status` (
  `id`                  INT(11) NOT NULL AUTO_INCREMENT,
  `businessId`          INT(11)          DEFAULT NULL,
  `businessName`        VARCHAR(255)     DEFAULT NULL,
  `createTime`          DATETIME         DEFAULT NULL,
  `goodsSellId`         INT(11)          DEFAULT NULL,
  `goodsSellName`       VARCHAR(255)     DEFAULT NULL,
  `goodsSellRecordCode` VARCHAR(255)     DEFAULT NULL,
  `goodsSellRecordId`   INT(11)          DEFAULT NULL,
  `goodsSellRecordName` VARCHAR(255)     DEFAULT NULL,
  `operationId`         INT(11)          DEFAULT NULL,
  `operationName`       VARCHAR(255)     DEFAULT NULL,
  `sort`                INT(11)          DEFAULT NULL,
  `type`                INT(11)          DEFAULT NULL,
  `statStr`             VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_ordercode` (`goodsSellRecordCode`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 3932168
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_goods_sell_record`
--

DROP TABLE IF EXISTS `runfast_goods_sell_record`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_goods_sell_record` (
  `id`                 INT(11) NOT NULL      AUTO_INCREMENT,
  `businessId`         INT(11)               DEFAULT NULL,
  `businessName`       VARCHAR(255)
                       CHARACTER SET utf8    DEFAULT NULL,
  `content`            VARCHAR(255)
                       CHARACTER SET utf8mb4 DEFAULT NULL,
  `createTime`         DATETIME              DEFAULT NULL,
  `goodsSellId`        INT(11)               DEFAULT NULL,
  `goodsSellName`      VARCHAR(255)
                       CHARACTER SET utf8    DEFAULT NULL,
  `orderCode`          VARCHAR(255)
                       CHARACTER SET utf8    DEFAULT NULL,
  `price`              DECIMAL(19, 2)        DEFAULT NULL,
  `status`             INT(11)               DEFAULT NULL,
  `userAddress`        VARCHAR(255)
                       CHARACTER SET utf8    DEFAULT NULL,
  `userId`             INT(11)               DEFAULT NULL,
  `userMobile`         VARCHAR(255)
                       CHARACTER SET utf8    DEFAULT NULL,
  `userName`           VARCHAR(255)
                       CHARACTER SET utf8    DEFAULT NULL,
  `shopper`            VARCHAR(255)
                       CHARACTER SET utf8    DEFAULT NULL,
  `shopperId`          INT(11)               DEFAULT NULL,
  `goodsTotal`         INT(11)               DEFAULT NULL,
  `userAddressId`      INT(11)               DEFAULT NULL,
  `yhprice`            DOUBLE                DEFAULT NULL,
  `packing`            DECIMAL(19, 2)        DEFAULT NULL,
  `showps`             DECIMAL(19, 2)        DEFAULT NULL,
  `statStr`            VARCHAR(255)
                       CHARACTER SET utf8    DEFAULT NULL,
  `endDate`            DATETIME              DEFAULT NULL,
  `rid`                INT(11)               DEFAULT NULL,
  `startDate`          DATETIME              DEFAULT NULL,
  `totalpay`           DECIMAL(19, 2)        DEFAULT NULL,
  `distance`           VARCHAR(255)
                       CHARACTER SET utf8    DEFAULT NULL,
  `iswithdraw`         INT(11)               DEFAULT NULL,
  `address`            VARCHAR(255)
                       CHARACTER SET utf8    DEFAULT NULL,
  `businessDel`        INT(11)               DEFAULT NULL,
  `isPay`              INT(11)               DEFAULT NULL,
  `isRefund`           INT(11)               DEFAULT NULL,
  `userDel`            INT(11)               DEFAULT NULL,
  `isComent`           INT(11)               DEFAULT NULL,
  `isReceive`          INT(11)               DEFAULT NULL,
  `isCancel`           INT(11)               DEFAULT NULL,
  `commisson`          DECIMAL(19, 2)        DEFAULT NULL,
  `shopperMoney`       DECIMAL(19, 2)        DEFAULT NULL,
  `businessAddressLat` VARCHAR(255)
                       CHARACTER SET utf8    DEFAULT NULL,
  `businessAddressLng` VARCHAR(255)
                       CHARACTER SET utf8    DEFAULT NULL,
  `userAddressLat`     VARCHAR(255)
                       CHARACTER SET utf8    DEFAULT NULL,
  `userAddressLng`     VARCHAR(255)
                       CHARACTER SET utf8    DEFAULT NULL,
  `businessAddr`       VARCHAR(255)
                       CHARACTER SET utf8    DEFAULT NULL,
  `businesspay`        DECIMAL(19, 2)        DEFAULT NULL,
  `refund`             DECIMAL(19, 2)        DEFAULT NULL,
  `isDeliver`          INT(11)               DEFAULT NULL,
  `qrcode`             VARCHAR(255)
                       CHARACTER SET utf8    DEFAULT NULL,
  `refundcontext`      VARCHAR(255)
                       CHARACTER SET utf8    DEFAULT NULL,
  `businessget`        DECIMAL(19, 2)        DEFAULT NULL,
  `cityId`             VARCHAR(255)
                       CHARACTER SET utf8    DEFAULT NULL,
  `cityName`           VARCHAR(255)
                       CHARACTER SET utf8    DEFAULT NULL,
  `countyId`           VARCHAR(255)
                       CHARACTER SET utf8    DEFAULT NULL,
  `countyName`         VARCHAR(255)
                       CHARACTER SET utf8    DEFAULT NULL,
  `townId`             VARCHAR(255)
                       CHARACTER SET utf8    DEFAULT NULL,
  `townName`           VARCHAR(255)
                       CHARACTER SET utf8    DEFAULT NULL,
  `agentId`            INT(11)               DEFAULT NULL,
  `agentName`          VARCHAR(100)
                       CHARACTER SET utf8    DEFAULT NULL,
  `businessMobile`     VARCHAR(255)
                       CHARACTER SET utf8    DEFAULT NULL,
  `noCharge`           DOUBLE                DEFAULT NULL,
  `shopperMobile`      VARCHAR(255)
                       CHARACTER SET utf8    DEFAULT NULL,
  `shopperSign`        INT(11)               DEFAULT NULL,
  `activityId`         INT(11)               DEFAULT NULL,
  `activityname`       VARCHAR(255)
                       CHARACTER SET utf8    DEFAULT NULL,
  `activityprice`      DECIMAL(19, 2)        DEFAULT NULL,
  `disprice`           DECIMAL(19, 2)        DEFAULT NULL,
  `distributionTime`   INT(11)               DEFAULT NULL,
  `oldShopper`         VARCHAR(255)
                       CHARACTER SET utf8    DEFAULT NULL,
  `oldShopperId`       INT(11)               DEFAULT NULL,
  `oldShopperMobile`   VARCHAR(255)
                       CHARACTER SET utf8    DEFAULT NULL,
  `agentget`           DECIMAL(19, 2)        DEFAULT NULL,
  `aceptTime`          DATETIME              DEFAULT NULL,
  `payTime`            DATETIME              DEFAULT NULL,
  `ismute`             INT(11)               DEFAULT NULL,
  `isClearing`         INT(11)               DEFAULT NULL,
  `refundTime`         DATETIME              DEFAULT NULL,
  `refundType`         INT(11)               DEFAULT NULL,
  `agentBusget`        DECIMAL(19, 2)        DEFAULT NULL,
  `acoefficient`       DECIMAL(19, 2)        DEFAULT NULL,
  `acoefficient2`      DECIMAL(19, 2)        DEFAULT NULL,
  `agentBusget2`       DECIMAL(19, 2)        DEFAULT NULL,
  `coefficient`        DECIMAL(19, 2)        DEFAULT NULL,
  `isfirst`            INT(11)               DEFAULT NULL,
  `ptype`              INT(11)               DEFAULT NULL,
  `zjzd`               DECIMAL(19, 2)        DEFAULT NULL,
  `range1`             INT(11)               DEFAULT NULL,
  `stype`              INT(11)               DEFAULT NULL,
  `couponname`         VARCHAR(255)
                       CHARACTER SET utf8    DEFAULT NULL,
  `payType`            INT(11)               DEFAULT NULL,
  `orderNumber`        INT(11)               DEFAULT NULL,
  `accptTime`          DATETIME              DEFAULT NULL,
  `isaccpt`            INT(11)               DEFAULT NULL,
  `pushType`           INT(11)               DEFAULT NULL,
  `userPhone`          VARCHAR(255)
                       CHARACTER SET utf8    DEFAULT NULL,
  `appOrwx`            INT(11)               DEFAULT NULL,
  `readyTime`          DATETIME              DEFAULT NULL,
  `disTime`            DATETIME              DEFAULT NULL,
  `errend`             INT(11)               DEFAULT NULL,
  `teamid`             INT(11)               DEFAULT NULL,
  `teamname`           VARCHAR(255)
                       CHARACTER SET utf8    DEFAULT NULL,
  `istimerefund`       INT(10)               DEFAULT NULL,
  `issubsidy`          INT(11)               DEFAULT NULL,
  `lessps`             DECIMAL(19, 2)        DEFAULT NULL,
  `subsidy`            DECIMAL(19, 2)        DEFAULT NULL,
  `content1`           VARCHAR(255)
                       CHARACTER SET utf8    DEFAULT NULL,
  `agree`              VARCHAR(255)
                       CHARACTER SET utf8    DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_orderCode` (`orderCode`),
  KEY `idx_userId` (`userId`),
  KEY `idx_bizId` (`businessId`, `createTime`, `status`),
  KEY `idx_agentId` (`agentId`, `createTime`, `status`),
  KEY `idx_businessid_createTime` (`businessId`, `createTime`),
  KEY `idx_shopper_agentid_status` (`status`, `shopperId`, `isDeliver`, `agentId`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 528570
  DEFAULT CHARSET = utf8
  COLLATE = utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_goods_sell_record_sub`
--

DROP TABLE IF EXISTS `runfast_goods_sell_record_sub`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_goods_sell_record_sub` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `createTime` DATETIME         DEFAULT NULL,
  `goodId`     VARCHAR(255)     DEFAULT NULL,
  `goodName`   VARCHAR(255)     DEFAULT NULL,
  `orderId`    INT(11)          DEFAULT NULL,
  `quantity`   INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_goods_sell_shopper_record`
--

DROP TABLE IF EXISTS `runfast_goods_sell_shopper_record`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_goods_sell_shopper_record` (
  `id`            INT(11) NOT NULL AUTO_INCREMENT,
  `businessId`    INT(11)          DEFAULT NULL,
  `businessName`  VARCHAR(255)     DEFAULT NULL,
  `endTime`       DATETIME         DEFAULT NULL,
  `goodsSellId`   INT(11)          DEFAULT NULL,
  `goodsSellName` VARCHAR(255)     DEFAULT NULL,
  `orderCode`     VARCHAR(255)     DEFAULT NULL,
  `orderTime`     DATETIME         DEFAULT NULL,
  `shopperId`     INT(11)          DEFAULT NULL,
  `shopperName`   INT(11)          DEFAULT NULL,
  `startTime`     DATETIME         DEFAULT NULL,
  `status`        INT(11)          DEFAULT NULL,
  `userId`        INT(11)          DEFAULT NULL,
  `userName`      VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_goods_sell_standard`
--

DROP TABLE IF EXISTS `runfast_goods_sell_standard`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_goods_sell_standard` (
  `id`           INT(11) NOT NULL AUTO_INCREMENT,
  `barCode`      VARCHAR(255)     DEFAULT NULL,
  `discount`     DOUBLE           DEFAULT NULL,
  `goodsSellId`  INT(11)          DEFAULT NULL,
  `name`         VARCHAR(255)     DEFAULT NULL,
  `price`        DOUBLE           DEFAULT NULL,
  `proCode`      VARCHAR(255)     DEFAULT NULL,
  `businessId`   INT(11)          DEFAULT NULL,
  `businessName` VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_goodsSellId` (`goodsSellId`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 380424
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_goods_sell_sub_option`
--

DROP TABLE IF EXISTS `runfast_goods_sell_sub_option`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_goods_sell_sub_option` (
  `id`            INT(11) NOT NULL AUTO_INCREMENT,
  `barCode`       VARCHAR(255)     DEFAULT NULL,
  `del`           INT(11)          DEFAULT NULL,
  `goodsSellId`   INT(11)          DEFAULT NULL,
  `goodsSellName` VARCHAR(255)     DEFAULT NULL,
  `name`          VARCHAR(255)     DEFAULT NULL,
  `optionId`      INT(11)          DEFAULT NULL,
  `optionName`    VARCHAR(255)     DEFAULT NULL,
  `sort`          INT(11)          DEFAULT NULL,
  `businessId`    INT(11)          DEFAULT NULL,
  `businessName`  VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_optionId` (`optionId`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 147685
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_goods_sell_sub_option_copy`
--

DROP TABLE IF EXISTS `runfast_goods_sell_sub_option_copy`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_goods_sell_sub_option_copy` (
  `id`            INT(11) NOT NULL AUTO_INCREMENT,
  `barCode`       VARCHAR(255)     DEFAULT NULL,
  `del`           INT(11)          DEFAULT NULL,
  `goodsSellId`   INT(11)          DEFAULT NULL,
  `goodsSellName` VARCHAR(255)     DEFAULT NULL,
  `name`          VARCHAR(255)     DEFAULT NULL,
  `optionId`      INT(11)          DEFAULT NULL,
  `optionName`    VARCHAR(255)     DEFAULT NULL,
  `sort`          INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_optionId` (`optionId`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 117787
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_goods_sell_type`
--

DROP TABLE IF EXISTS `runfast_goods_sell_type`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_goods_sell_type` (
  `id`           INT(11) NOT NULL AUTO_INCREMENT,
  `businessId`   INT(11)          DEFAULT NULL,
  `businessName` VARCHAR(255)     DEFAULT NULL,
  `content`      VARCHAR(255)     DEFAULT NULL,
  `createTime`   DATETIME         DEFAULT NULL,
  `imgPath`      VARCHAR(255)     DEFAULT NULL,
  `name`         VARCHAR(255)     DEFAULT NULL,
  `agentId`      INT(11)          DEFAULT NULL,
  `agentName`    VARCHAR(100)     DEFAULT NULL,
  `sort`         INT(11)          DEFAULT NULL,
  `deleted`      INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_type` (`businessId`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 24518
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `runfast_goods_sell_view`
--

DROP TABLE IF EXISTS `runfast_goods_sell_view`;
/*!50001 DROP VIEW IF EXISTS `runfast_goods_sell_view`*/;
SET @saved_cs_client = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `runfast_goods_sell_view` AS
  SELECT
    1 AS `id`,
    1 AS `sname`,
    1 AS `price`,
    1 AS `businessName`,
    1 AS `businessId`,
    1 AS `gid`,
    1 AS `name`,
    1 AS `content`,
    1 AS `sellTypeId`,
    1 AS `sellTypeName`,
    1 AS `status`,
    1 AS `star`,
    1 AS `salesnum`,
    1 AS `num`,
    1 AS `agentName`,
    1 AS `agentId`,
    1 AS `createTime` */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `runfast_homemanagement`
--

DROP TABLE IF EXISTS `runfast_homemanagement`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_homemanagement` (
  `id`                        INT(11) NOT NULL AUTO_INCREMENT,
  `apply`                     INT(11)          DEFAULT NULL,
  `backgroundColorCode`       VARCHAR(255)     DEFAULT NULL,
  `lineColorCode`             VARCHAR(255)     DEFAULT NULL,
  `remarks`                   VARCHAR(255)     DEFAULT NULL,
  `title`                     VARCHAR(255)     DEFAULT NULL,
  `type`                      INT(11)          DEFAULT NULL,
  `downbackgroundColorCode`   VARCHAR(255)     DEFAULT NULL,
  `middlebackgroundColorCode` VARCHAR(255)     DEFAULT NULL,
  `topbackgroundColorCode`    VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_homepage`
--

DROP TABLE IF EXISTS `runfast_homepage`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_homepage` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `createTime` DATETIME         DEFAULT NULL,
  `icon`       VARCHAR(255)     DEFAULT NULL,
  `link`       VARCHAR(255)     DEFAULT NULL,
  `name`       VARCHAR(255)     DEFAULT NULL,
  `sort`       INT(11)          DEFAULT NULL,
  `agentId`    INT(11)          DEFAULT NULL,
  `agentName`  VARCHAR(100)     DEFAULT NULL,
  `typelink`   INT(11)          DEFAULT NULL,
  `typename`   VARCHAR(255)     DEFAULT NULL,
  `will`       INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_agentId` (`agentId`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 266
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_killertime`
--

DROP TABLE IF EXISTS `runfast_killertime`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_killertime` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `createTime` DATETIME         DEFAULT NULL,
  `endTime`    DATETIME         DEFAULT NULL,
  `startTime`  DATETIME         DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 6
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_logistics`
--

DROP TABLE IF EXISTS `runfast_logistics`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_logistics` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `code`       VARCHAR(255)     DEFAULT NULL,
  `createTime` DATETIME         DEFAULT NULL,
  `name`       VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 5
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_messge`
--

DROP TABLE IF EXISTS `runfast_messge`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_messge` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `content`    VARCHAR(255)     DEFAULT NULL,
  `createTime` DATETIME         DEFAULT NULL,
  `title`      VARCHAR(255)     DEFAULT NULL,
  `type`       INT(11)          DEFAULT NULL,
  `userId`     INT(11)          DEFAULT NULL,
  `userName`   VARCHAR(255)     DEFAULT NULL,
  `cityId`     VARCHAR(255)     DEFAULT NULL,
  `cityName`   VARCHAR(255)     DEFAULT NULL,
  `countyId`   VARCHAR(255)     DEFAULT NULL,
  `countyName` VARCHAR(255)     DEFAULT NULL,
  `end`        DATETIME         DEFAULT NULL,
  `start`      DATETIME         DEFAULT NULL,
  `townId`     VARCHAR(255)     DEFAULT NULL,
  `townName`   VARCHAR(255)     DEFAULT NULL,
  `agentId`    INT(11)          DEFAULT NULL,
  `agentName`  VARCHAR(100)     DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_userId` (`userId`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 112031
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_notice`
--

DROP TABLE IF EXISTS `runfast_notice`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_notice` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `agentId`    INT(11)          DEFAULT NULL,
  `agentName`  VARCHAR(255)     DEFAULT NULL,
  `context`    VARCHAR(255)     DEFAULT NULL,
  `createTime` DATETIME         DEFAULT NULL,
  `isindex`    INT(11)          DEFAULT NULL,
  `title`      VARCHAR(50)      DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_numbercharge`
--

DROP TABLE IF EXISTS `runfast_numbercharge`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_numbercharge` (
  `id`           INT(11) NOT NULL AUTO_INCREMENT,
  `auditingTime` DATETIME         DEFAULT NULL,
  `createTime`   DATETIME         DEFAULT NULL,
  `mobile`       VARCHAR(255)     DEFAULT NULL,
  `operator`     INT(11)          DEFAULT NULL,
  `price`        DECIMAL(19, 2)   DEFAULT NULL,
  `status`       INT(11)          DEFAULT NULL,
  `userId`       INT(11)          DEFAULT NULL,
  `userName`     VARCHAR(255)     DEFAULT NULL,
  `oid`          INT(11)          DEFAULT NULL,
  `ispay`        INT(11)          DEFAULT NULL,
  `orderCode`    VARCHAR(255)     DEFAULT NULL,
  `payTime`      DATETIME         DEFAULT NULL,
  `payType`      INT(11)          DEFAULT NULL,
  `totalPay`     DECIMAL(19, 2)   DEFAULT NULL,
  `type`         INT(11)          DEFAULT NULL,
  `isflow`       INT(11)          DEFAULT NULL,
  `remarks`      VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `orderCode` (`orderCode`),
  KEY `idx_sta_userid` (`status`, `userId`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 99
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_opengroup`
--

DROP TABLE IF EXISTS `runfast_opengroup`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_opengroup` (
  `id`             INT(11) NOT NULL AUTO_INCREMENT,
  `code`           VARCHAR(255)     DEFAULT NULL,
  `createTime`     DATETIME         DEFAULT NULL,
  `endTime`        DATETIME         DEFAULT NULL,
  `foodId`         INT(11)          DEFAULT NULL,
  `foodclassId`    INT(11)          DEFAULT NULL,
  `foodname`       VARCHAR(255)     DEFAULT NULL,
  `fullTime`       DATETIME         DEFAULT NULL,
  `imgpath`        VARCHAR(255)     DEFAULT NULL,
  `info`           LONGTEXT,
  `integral`       DOUBLE           DEFAULT NULL,
  `iscoupon`       INT(11)          DEFAULT NULL,
  `limitNumber`    INT(11)          DEFAULT NULL,
  `newman`         INT(11)          DEFAULT NULL,
  `price`          DECIMAL(19, 2)   DEFAULT NULL,
  `quantity`       INT(11)          DEFAULT NULL,
  `stats`          INT(11)          DEFAULT NULL,
  `userid`         INT(11)          DEFAULT NULL,
  `username`       VARCHAR(255)     DEFAULT NULL,
  `winningNumbers` INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_foodid` (`foodId`),
  KEY `idx_foodid_stats` (`foodId`, `stats`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 10
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_operationlog`
--

DROP TABLE IF EXISTS `runfast_operationlog`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_operationlog` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `cid`        INT(11)          DEFAULT NULL,
  `cname`      VARCHAR(255)     DEFAULT NULL,
  `ctype`      INT(11)          DEFAULT NULL,
  `otype`      INT(11)          DEFAULT NULL,
  `params`     VARCHAR(255)     DEFAULT NULL,
  `pid`        INT(11)          DEFAULT NULL,
  `type`       INT(11)          DEFAULT NULL,
  `agentId`    INT(11)          DEFAULT NULL,
  `agentName`  VARCHAR(100)     DEFAULT NULL,
  `createTime` DATETIME         DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 218373
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_outbill`
--

DROP TABLE IF EXISTS `runfast_outbill`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_outbill` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `allowState` INT(11)          DEFAULT NULL,
  `allowTime`  DATETIME         DEFAULT NULL,
  `allower`    VARCHAR(255)     DEFAULT NULL,
  `area`       VARCHAR(255)     DEFAULT NULL,
  `areaId`     INT(11)          DEFAULT NULL,
  `city`       VARCHAR(255)     DEFAULT NULL,
  `cityId`     INT(11)          DEFAULT NULL,
  `createTime` DATETIME         DEFAULT NULL,
  `nums`       INT(11)          DEFAULT NULL,
  `outCode`    VARCHAR(255)     DEFAULT NULL,
  `outor`      VARCHAR(255)     DEFAULT NULL,
  `total`      DECIMAL(19, 2)   DEFAULT NULL,
  `typeId`     INT(11)          DEFAULT NULL,
  `agentId`    INT(11)          DEFAULT NULL,
  `agentName`  VARCHAR(100)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_outhistory`
--

DROP TABLE IF EXISTS `runfast_outhistory`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_outhistory` (
  `id`          INT(11) NOT NULL AUTO_INCREMENT,
  `area`        VARCHAR(255)     DEFAULT NULL,
  `areaId`      INT(11)          DEFAULT NULL,
  `city`        VARCHAR(255)     DEFAULT NULL,
  `cityId`      INT(11)          DEFAULT NULL,
  `createTime`  DATETIME         DEFAULT NULL,
  `foodId`      INT(11)          DEFAULT NULL,
  `name`        VARCHAR(255)     DEFAULT NULL,
  `nums`        INT(11)          DEFAULT NULL,
  `outCode`     VARCHAR(255)     DEFAULT NULL,
  `outor`       VARCHAR(255)     DEFAULT NULL,
  `supportCode` VARCHAR(255)     DEFAULT NULL,
  `supportId`   INT(11)          DEFAULT NULL,
  `supportName` VARCHAR(255)     DEFAULT NULL,
  `total`       DECIMAL(19, 2)   DEFAULT NULL,
  `type`        INT(11)          DEFAULT NULL,
  `foodstand`   VARCHAR(255)     DEFAULT NULL,
  `foodstandId` INT(11)          DEFAULT NULL,
  `agentId`     INT(11)          DEFAULT NULL,
  `agentName`   VARCHAR(100)     DEFAULT NULL,
  `proCode`     VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_outproduct`
--

DROP TABLE IF EXISTS `runfast_outproduct`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_outproduct` (
  `id`                  INT(11) NOT NULL AUTO_INCREMENT,
  `countPromotion`      DECIMAL(19, 2)   DEFAULT NULL,
  `countPurchasePricen` DECIMAL(19, 2)   DEFAULT NULL,
  `foodId`              INT(11)          DEFAULT NULL,
  `name`                VARCHAR(255)     DEFAULT NULL,
  `nums`                INT(11)          DEFAULT NULL,
  `outBillId`           INT(11)          DEFAULT NULL,
  `proCode`             VARCHAR(255)     DEFAULT NULL,
  `standard`            VARCHAR(255)     DEFAULT NULL,
  `standardId`          INT(11)          DEFAULT NULL,
  `supportCode`         VARCHAR(255)     DEFAULT NULL,
  `supportId`           INT(11)          DEFAULT NULL,
  `supportName`         VARCHAR(255)     DEFAULT NULL,
  `total`               DECIMAL(19, 2)   DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_fid_sid` (`foodId`, `standardId`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_panicfood`
--

DROP TABLE IF EXISTS `runfast_panicfood`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_panicfood` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `createTime` DATETIME         DEFAULT NULL,
  `endTime`    DATETIME         DEFAULT NULL,
  `enddate`    DATETIME         DEFAULT NULL,
  `imgpath`    VARCHAR(255)     DEFAULT NULL,
  `integral`   DOUBLE           DEFAULT NULL,
  `introduce`  LONGTEXT,
  `isprice`    INT(11)          DEFAULT NULL,
  `maxsum`     INT(11)          DEFAULT NULL,
  `name`       VARCHAR(255)     DEFAULT NULL,
  `offered`    INT(11)          DEFAULT NULL,
  `price`      DECIMAL(19, 2)   DEFAULT NULL,
  `quantity`   INT(11)          DEFAULT NULL,
  `startTime`  DATETIME         DEFAULT NULL,
  `startdate`  DATETIME         DEFAULT NULL,
  `status`     INT(11)          DEFAULT NULL,
  `type`       INT(11)          DEFAULT NULL,
  `typename`   VARCHAR(255)     DEFAULT NULL,
  `isrentrun`  INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 6
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_panicfoodclass`
--

DROP TABLE IF EXISTS `runfast_panicfoodclass`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_panicfoodclass` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `byid`       INT(11)          DEFAULT NULL,
  `createTime` DATETIME         DEFAULT NULL,
  `imgPath`    VARCHAR(255)     DEFAULT NULL,
  `name`       VARCHAR(255)     DEFAULT NULL,
  `sort`       INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 3
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_panicfoodorder`
--

DROP TABLE IF EXISTS `runfast_panicfoodorder`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_panicfoodorder` (
  `id`             INT(11) NOT NULL AUTO_INCREMENT,
  `address`        INT(11)          DEFAULT NULL,
  `createTime`     DATETIME         DEFAULT NULL,
  `foodId`         INT(11)          DEFAULT NULL,
  `foodname`       VARCHAR(255)     DEFAULT NULL,
  `iswin`          INT(11)          DEFAULT NULL,
  `openId`         INT(11)          DEFAULT NULL,
  `pic`            VARCHAR(255)     DEFAULT NULL,
  `price`          DECIMAL(19, 2)   DEFAULT NULL,
  `score`          DOUBLE           DEFAULT NULL,
  `stats`          INT(11)          DEFAULT NULL,
  `type`           INT(11)          DEFAULT NULL,
  `userid`         INT(11)          DEFAULT NULL,
  `username`       VARCHAR(255)     DEFAULT NULL,
  `winningNumbers` INT(11)          DEFAULT NULL,
  `code`           VARCHAR(255)     DEFAULT NULL,
  `recordId`       INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_openid_userid` (`openId`, `userid`, `stats`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 59
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_panicfoodpicture`
--

DROP TABLE IF EXISTS `runfast_panicfoodpicture`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_panicfoodpicture` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `createTime` DATETIME         DEFAULT NULL,
  `foodid`     INT(11)          DEFAULT NULL,
  `path`       VARCHAR(255)     DEFAULT NULL,
  `sort`       INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_foodid` (`foodid`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 48
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_panicrecord`
--

DROP TABLE IF EXISTS `runfast_panicrecord`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_panicrecord` (
  `id`          INT(11) NOT NULL AUTO_INCREMENT,
  `createTime`  DATETIME         DEFAULT NULL,
  `cuername`    VARCHAR(255)     DEFAULT NULL,
  `cuserid`     INT(11)          DEFAULT NULL,
  `openId`      INT(11)          DEFAULT NULL,
  `panicfoodId` INT(11)          DEFAULT NULL,
  `panicname`   VARCHAR(255)     DEFAULT NULL,
  `pic`         VARCHAR(255)     DEFAULT NULL,
  `quantity`    INT(11)          DEFAULT NULL,
  `address`     INT(11)          DEFAULT NULL,
  `code`        VARCHAR(255)     DEFAULT NULL,
  `paytype`     INT(11)          DEFAULT NULL,
  `stats`       INT(11)          DEFAULT NULL,
  `totalprice`  DECIMAL(19, 2)   DEFAULT NULL,
  `score`       DOUBLE           DEFAULT NULL,
  `mobile`      VARCHAR(255)     DEFAULT NULL,
  `periods`     VARCHAR(255)     DEFAULT NULL,
  `typest`      INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_openid` (`openId`),
  KEY `idx_code` (`code`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 116
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_payrecord`
--

DROP TABLE IF EXISTS `runfast_payrecord`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_payrecord` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `createTime` DATETIME         DEFAULT NULL,
  `fromUser`   VARCHAR(50)      DEFAULT NULL,
  `info`       LONGTEXT,
  `modifTime`  DATETIME         DEFAULT NULL,
  `openid`     VARCHAR(200)     DEFAULT NULL,
  `orderId`    INT(11)          DEFAULT NULL,
  `orderNo`    VARCHAR(50)      DEFAULT NULL,
  `payType`    INT(11)          DEFAULT NULL,
  `toUser`     VARCHAR(50)      DEFAULT NULL,
  `totalPrice` DECIMAL(19, 2)   DEFAULT NULL,
  `trade_no`   VARCHAR(50)      DEFAULT NULL,
  `type`       INT(11)          DEFAULT NULL,
  `userId`     INT(11)          DEFAULT NULL,
  `userName`   VARCHAR(255)     DEFAULT NULL,
  `appOrwx`    INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_orderNo` (`orderNo`),
  KEY `idx_userId` (`userId`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 467919
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_province`
--

DROP TABLE IF EXISTS `runfast_province`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_province` (
  `id`   INT(11) NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(255)     DEFAULT NULL,
  `name` VARCHAR(255)     DEFAULT NULL,
  `sort` INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 35
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_redpacket`
--

DROP TABLE IF EXISTS `runfast_redpacket`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_redpacket` (
  `id`           INT(11) NOT NULL AUTO_INCREMENT,
  `businessId`   INT(11)          DEFAULT NULL,
  `businessName` VARCHAR(255)     DEFAULT NULL,
  `code`         VARCHAR(255)     DEFAULT NULL,
  `content`      VARCHAR(255)     DEFAULT NULL,
  `createTime`   DATETIME         DEFAULT NULL,
  `number`       DECIMAL(19, 2)   DEFAULT NULL,
  `status`       INT(11)          DEFAULT NULL,
  `term`         DATETIME         DEFAULT NULL,
  `type`         INT(11)          DEFAULT NULL,
  `userId`       INT(11)          DEFAULT NULL,
  `userMobile`   VARCHAR(255)     DEFAULT NULL,
  `userName`     VARCHAR(255)     DEFAULT NULL,
  `name`         VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_region`
--

DROP TABLE IF EXISTS `runfast_region`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_region` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `city`       VARCHAR(255)     DEFAULT NULL,
  `cityId`     INT(11)          DEFAULT NULL,
  `name`       VARCHAR(255)     DEFAULT NULL,
  `province`   VARCHAR(255)     DEFAULT NULL,
  `provinceId` INT(11)          DEFAULT NULL,
  `code`       VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_city` (`cityId`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 3012
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_safetyrecordimg`
--

DROP TABLE IF EXISTS `runfast_safetyrecordimg`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_safetyrecordimg` (
  `id`     INT(11) NOT NULL AUTO_INCREMENT,
  `busID`  INT(11)          DEFAULT NULL,
  `imgUrl` VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_busid` (`busID`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 10729
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_sale_food`
--

DROP TABLE IF EXISTS `runfast_sale_food`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_sale_food` (
  `id`            INT(11) NOT NULL AUTO_INCREMENT,
  `createTime`    DATETIME         DEFAULT NULL,
  `endDate`       DATETIME         DEFAULT NULL,
  `fid`           INT(11)          DEFAULT NULL,
  `fname`         VARCHAR(255)     DEFAULT NULL,
  `foodId`        INT(11)          DEFAULT NULL,
  `givenums`      INT(11)          DEFAULT NULL,
  `info`          LONGTEXT,
  `killTime`      VARCHAR(255)     DEFAULT NULL,
  `path`          VARCHAR(255)     DEFAULT NULL,
  `pos`           INT(11)          DEFAULT NULL,
  `price`         DECIMAL(19, 2)   DEFAULT NULL,
  `proCode`       VARCHAR(255)     DEFAULT NULL,
  `quantity`      INT(11)          DEFAULT NULL,
  `realPrice`     DECIMAL(19, 2)   DEFAULT NULL,
  `saleNums`      INT(11)          DEFAULT NULL,
  `sort`          INT(11)          DEFAULT NULL,
  `standardPrice` DECIMAL(19, 2)   DEFAULT NULL,
  `startDate`     DATETIME         DEFAULT NULL,
  `title`         VARCHAR(255)     DEFAULT NULL,
  `yunPrice`      DECIMAL(19, 2)   DEFAULT NULL,
  `islimited`     INT(11)          DEFAULT NULL,
  `limiEndTime`   DATETIME         DEFAULT NULL,
  `limiStartTime` DATETIME         DEFAULT NULL,
  `limitNum`      INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 3
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_saleclearlog`
--

DROP TABLE IF EXISTS `runfast_saleclearlog`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_saleclearlog` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `cleardate`  VARCHAR(255)     DEFAULT NULL,
  `createTime` DATETIME         DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_scorerecord`
--

DROP TABLE IF EXISTS `runfast_scorerecord`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_scorerecord` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `cardnumber` VARCHAR(255)     DEFAULT NULL,
  `cid`        INT(11)          DEFAULT NULL,
  `createTime` DATETIME         DEFAULT NULL,
  `mobile`     VARCHAR(255)     DEFAULT NULL,
  `name`       VARCHAR(255)     DEFAULT NULL,
  `score`      INT(11)          DEFAULT NULL,
  `type`       INT(11)          DEFAULT NULL,
  `monetary`   DECIMAL(19, 2)   DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_cid` (`cid`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 543874
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_scroceset`
--

DROP TABLE IF EXISTS `runfast_scroceset`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_scroceset` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `advert`     DOUBLE           DEFAULT NULL,
  `consumexs`  DOUBLE           DEFAULT NULL,
  `createTime` DATETIME         DEFAULT NULL,
  `enshrine`   DOUBLE           DEFAULT NULL,
  `num`        INT(11)          DEFAULT NULL,
  `recommend`  DOUBLE           DEFAULT NULL,
  `register`   DOUBLE           DEFAULT NULL,
  `review`     DOUBLE           DEFAULT NULL,
  `share`      DOUBLE           DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 2
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_sellchose`
--

DROP TABLE IF EXISTS `runfast_sellchose`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_sellchose` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `createTime` DATETIME         DEFAULT NULL,
  `name`       VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_sensitive`
--

DROP TABLE IF EXISTS `runfast_sensitive`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_sensitive` (
  `id`             INT(11) NOT NULL AUTO_INCREMENT,
  `sensitiveWords` VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_shop_order_record`
--

DROP TABLE IF EXISTS `runfast_shop_order_record`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_shop_order_record` (
  `id`             INT(11) NOT NULL AUTO_INCREMENT,
  `activity`       INT(11)          DEFAULT NULL,
  `agentBusget`    DECIMAL(19, 2)   DEFAULT NULL,
  `agentId`        INT(11)          DEFAULT NULL,
  `agentName`      VARCHAR(100)     DEFAULT NULL,
  `agentget`       DECIMAL(19, 2)   DEFAULT NULL,
  `businessId`     INT(11)          DEFAULT NULL,
  `businessMobile` VARCHAR(255)     DEFAULT NULL,
  `businessName`   VARCHAR(255)     DEFAULT NULL,
  `businessget`    DECIMAL(19, 2)   DEFAULT NULL,
  `cargo`          VARCHAR(255)     DEFAULT NULL,
  `cargoMobile`    VARCHAR(255)     DEFAULT NULL,
  `city`           VARCHAR(255)     DEFAULT NULL,
  `cityId`         INT(11)          DEFAULT NULL,
  `coefficient`    DECIMAL(19, 2)   DEFAULT NULL,
  `coefficient2`   DECIMAL(19, 2)   DEFAULT NULL,
  `content`        VARCHAR(255)     DEFAULT NULL,
  `countyId`       INT(11)          DEFAULT NULL,
  `countyName`     VARCHAR(255)     DEFAULT NULL,
  `createTime`     DATETIME         DEFAULT NULL,
  `dealPayOrder`   VARCHAR(255)     DEFAULT NULL,
  `deliveryTime`   DATETIME         DEFAULT NULL,
  `dispathid`      INT(11)          DEFAULT NULL,
  `disprice`       DECIMAL(19, 2)   DEFAULT NULL,
  `foodsId`        INT(11)          DEFAULT NULL,
  `foodsName`      VARCHAR(255)     DEFAULT NULL,
  `foodsTotal`     INT(11)          DEFAULT NULL,
  `freight`        INT(11)          DEFAULT NULL,
  `isfrist`        INT(11)          DEFAULT NULL,
  `kdnumber`       VARCHAR(255)     DEFAULT NULL,
  `lastPayTime`    DATETIME         DEFAULT NULL,
  `liuyan`         VARCHAR(255)     DEFAULT NULL,
  `orderNo`        VARCHAR(255)     DEFAULT NULL,
  `payTime`        DATETIME         DEFAULT NULL,
  `payway`         INT(11)          DEFAULT NULL,
  `quantity`       INT(11)          DEFAULT NULL,
  `status`         INT(11)          DEFAULT NULL,
  `totalPrice`     DECIMAL(19, 2)   DEFAULT NULL,
  `townId`         INT(11)          DEFAULT NULL,
  `townName`       VARCHAR(255)     DEFAULT NULL,
  `userAddress`    VARCHAR(255)     DEFAULT NULL,
  `userId`         INT(11)          DEFAULT NULL,
  `userName`       VARCHAR(255)     DEFAULT NULL,
  `usermobile`     VARCHAR(255)     DEFAULT NULL,
  `waitPay`        DECIMAL(19, 2)   DEFAULT NULL,
  `yunprice`       DECIMAL(19, 2)   DEFAULT NULL,
  `aceptpay`       INT(11)          DEFAULT NULL,
  `paytotal`       DECIMAL(19, 2)   DEFAULT NULL,
  `returnGoods`    DECIMAL(19, 2)   DEFAULT NULL,
  `isallot`        INT(11)          DEFAULT NULL,
  `ispay`          INT(11)          DEFAULT NULL,
  `statuss`        INT(11)          DEFAULT NULL,
  `aceptpaytotal`  DECIMAL(19, 2)   DEFAULT NULL,
  `price`          DECIMAL(19, 2)   DEFAULT NULL,
  `rid`            INT(11)          DEFAULT NULL,
  `yhqprice`       DECIMAL(19, 2)   DEFAULT NULL,
  `kdcode`         VARCHAR(255)     DEFAULT NULL,
  `lid`            INT(11)          DEFAULT NULL,
  `appOrwx`        INT(11)          DEFAULT NULL,
  `readyTime`      DATETIME         DEFAULT NULL,
  `type`           INT(11)          DEFAULT NULL,
  `integral`       DOUBLE           DEFAULT NULL,
  `yitotalprice`   DECIMAL(19, 2)   DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_agentid` (`agentId`),
  KEY `idx_orderno` (`orderNo`),
  KEY `idx_status` (`status`),
  KEY `idx_foodid_userid_actity` (`activity`, `foodsId`, `userId`),
  KEY `idx_agentid_type` (`agentId`, `createTime`, `type`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 68
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_shop_order_record_child`
--

DROP TABLE IF EXISTS `runfast_shop_order_record_child`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_shop_order_record_child` (
  `id`             INT(11) NOT NULL AUTO_INCREMENT,
  `activity`       INT(11)          DEFAULT NULL,
  `agentId`        INT(11)          DEFAULT NULL,
  `agentName`      VARCHAR(100)     DEFAULT NULL,
  `barCode`        VARCHAR(255)     DEFAULT NULL,
  `foodimg`        VARCHAR(255)     DEFAULT NULL,
  `foodprice`      DECIMAL(19, 2)   DEFAULT NULL,
  `foodsId`        INT(11)          DEFAULT NULL,
  `foodsName`      VARCHAR(255)     DEFAULT NULL,
  `foodsTotal`     INT(11)          DEFAULT NULL,
  `idcomment`      INT(11)          DEFAULT NULL,
  `isRepair`       INT(11)          DEFAULT NULL,
  `orderId`        INT(11)          DEFAULT NULL,
  `orderNo`        VARCHAR(255)     DEFAULT NULL,
  `presell`        INT(11)          DEFAULT NULL,
  `proCode`        VARCHAR(255)     DEFAULT NULL,
  `realPrice`      DECIMAL(19, 2)   DEFAULT NULL,
  `repaircontext`  VARCHAR(255)     DEFAULT NULL,
  `saleFoodId`     INT(11)          DEFAULT NULL,
  `shipment`       INT(11)          DEFAULT NULL,
  `sid`            INT(11)          DEFAULT NULL,
  `sidname`        VARCHAR(255)     DEFAULT NULL,
  `status`         INT(11)          DEFAULT NULL,
  `supportId`      INT(11)          DEFAULT NULL,
  `supportName`    VARCHAR(255)     DEFAULT NULL,
  `title`          VARCHAR(255)     DEFAULT NULL,
  `totalPrice`     DECIMAL(19, 2)   DEFAULT NULL,
  `typeId`         INT(11)          DEFAULT NULL,
  `typeName`       VARCHAR(255)     DEFAULT NULL,
  `unitId`         INT(11)          DEFAULT NULL,
  `unitName`       VARCHAR(255)     DEFAULT NULL,
  `userId`         INT(11)          DEFAULT NULL,
  `userName`       VARCHAR(255)     DEFAULT NULL,
  `usermobile`     VARCHAR(255)     DEFAULT NULL,
  `yprice`         DECIMAL(19, 2)   DEFAULT NULL,
  `yunPrice`       DECIMAL(19, 2)   DEFAULT NULL,
  `aceptpay`       INT(11)          DEFAULT NULL,
  `paynum`         DECIMAL(19, 2)   DEFAULT NULL,
  `createTime`     DATETIME         DEFAULT NULL,
  `isRepairNum`    INT(11)          DEFAULT NULL,
  `barternum`      INT(11)          DEFAULT NULL,
  `repairnum`      INT(11)          DEFAULT NULL,
  `returns`        INT(11)          DEFAULT NULL,
  `panicFoodPrice` DECIMAL(19, 2)   DEFAULT NULL,
  `yitype`         INT(11)          DEFAULT NULL,
  `integral`       DOUBLE           DEFAULT NULL,
  `yitotalprice`   DECIMAL(19, 2)   DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_orderno_userid` (`orderNo`, `userId`),
  KEY `idx_stat` (`status`, `userId`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 65
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_shophomepage`
--

DROP TABLE IF EXISTS `runfast_shophomepage`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_shophomepage` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `agentId`    INT(11)          DEFAULT NULL,
  `agentName`  VARCHAR(100)     DEFAULT NULL,
  `createTime` DATETIME         DEFAULT NULL,
  `icon`       VARCHAR(50)      DEFAULT NULL,
  `link`       VARCHAR(255)     DEFAULT NULL,
  `name`       VARCHAR(50)      DEFAULT NULL,
  `sort`       INT(11)          DEFAULT NULL,
  `title`      VARCHAR(255)     DEFAULT NULL,
  `type`       INT(11)          DEFAULT NULL,
  `typeid`     VARCHAR(255)     DEFAULT NULL,
  `typelink`   INT(11)          DEFAULT NULL,
  `typename`   VARCHAR(255)     DEFAULT NULL,
  `icon2`      VARCHAR(255)     DEFAULT NULL,
  `link2`      VARCHAR(255)     DEFAULT NULL,
  `special`    INT(11)          DEFAULT NULL,
  `special2`   INT(11)          DEFAULT NULL,
  `typeid2`    VARCHAR(255)     DEFAULT NULL,
  `typelink2`  INT(11)          DEFAULT NULL,
  `typename2`  VARCHAR(255)     DEFAULT NULL,
  `icon3`      VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 20
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_shophomepagechild`
--

DROP TABLE IF EXISTS `runfast_shophomepagechild`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_shophomepagechild` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `createTime` DATETIME         DEFAULT NULL,
  `icon`       VARCHAR(50)      DEFAULT NULL,
  `link`       VARCHAR(255)     DEFAULT NULL,
  `name`       VARCHAR(255)     DEFAULT NULL,
  `pid`        INT(11)          DEFAULT NULL,
  `pname`      VARCHAR(255)     DEFAULT NULL,
  `sort`       INT(11)          DEFAULT NULL,
  `title`      VARCHAR(255)     DEFAULT NULL,
  `typeid`     VARCHAR(255)     DEFAULT NULL,
  `typelink`   INT(11)          DEFAULT NULL,
  `typename`   VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 52
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_shoporder_status`
--

DROP TABLE IF EXISTS `runfast_shoporder_status`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_shoporder_status` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `cid`        INT(11)          DEFAULT NULL,
  `createTime` DATETIME         DEFAULT NULL,
  `orderNo`    VARCHAR(255)     DEFAULT NULL,
  `sid`        INT(11)          DEFAULT NULL,
  `sort`       INT(11)          DEFAULT NULL,
  `statStr`    VARCHAR(255)     DEFAULT NULL,
  `type`       INT(11)          DEFAULT NULL,
  `userName`   VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_orderno` (`orderNo`),
  KEY `idx_sid` (`sid`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 52
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_shopper`
--

DROP TABLE IF EXISTS `runfast_shopper`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_shopper` (
  `id`               INT(11) NOT NULL AUTO_INCREMENT,
  `createTime`       DATETIME         DEFAULT NULL,
  `creditRating`     INT(11)          DEFAULT NULL,
  `distribution`     VARCHAR(255)     DEFAULT NULL,
  `distributionmun`  INT(11)          DEFAULT NULL,
  `endTime`          DATETIME         DEFAULT NULL,
  `munber`           VARCHAR(255)     DEFAULT NULL,
  `name`             VARCHAR(255)     DEFAULT NULL,
  `password`         VARCHAR(255)     DEFAULT NULL,
  `startTime`        DATETIME         DEFAULT NULL,
  `userName`         VARCHAR(255)     DEFAULT NULL,
  `worktoday`        VARCHAR(255)     DEFAULT NULL,
  `latitude`         VARCHAR(20)      DEFAULT NULL,
  `longitude`        VARCHAR(20)      DEFAULT NULL,
  `interimremainder` DECIMAL(19, 2)   DEFAULT NULL,
  `minmonety`        DECIMAL(19, 2)   DEFAULT NULL,
  `remainder`        DECIMAL(19, 2)   DEFAULT NULL,
  `bdchannelId`      VARCHAR(255)     DEFAULT NULL,
  `bduserId`         VARCHAR(255)     DEFAULT NULL,
  `bptype`           INT(11)          DEFAULT NULL,
  `creditLevelid`    INT(11)          DEFAULT NULL,
  `agentId`          INT(11)          DEFAULT NULL,
  `agentName`        VARCHAR(100)     DEFAULT NULL,
  `working`          INT(11)          DEFAULT NULL,
  `status`           INT(11)          DEFAULT NULL,
  `otherId`          VARCHAR(255)     DEFAULT NULL,
  `pushType`         INT(11)          DEFAULT NULL,
  `teamid`           INT(11)          DEFAULT NULL,
  `teamname`         VARCHAR(255)     DEFAULT NULL,
  `distance`         DOUBLE           DEFAULT NULL,
  `alias`            VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_agentid` (`agentId`),
  KEY `idx_agentid_teamid` (`agentId`, `teamid`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 1169
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_shopper_team`
--

DROP TABLE IF EXISTS `runfast_shopper_team`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_shopper_team` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `agentId`    INT(11)          DEFAULT NULL,
  `agentName`  VARCHAR(255)     DEFAULT NULL,
  `createTime` DATETIME         DEFAULT NULL,
  `name`       VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 23
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_shopping_che`
--

DROP TABLE IF EXISTS `runfast_shopping_che`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_shopping_che` (
  `id`             INT(11) NOT NULL AUTO_INCREMENT,
  `agentId`        INT(11)          DEFAULT NULL,
  `agentName`      VARCHAR(100)     DEFAULT NULL,
  `barCode`        VARCHAR(255)     DEFAULT NULL,
  `businessId`     INT(11)          DEFAULT NULL,
  `businessName`   VARCHAR(255)     DEFAULT NULL,
  `cid`            INT(11)          DEFAULT NULL,
  `cname`          VARCHAR(255)     DEFAULT NULL,
  `disprice`       DECIMAL(19, 2)   DEFAULT NULL,
  `food`           VARCHAR(255)     DEFAULT NULL,
  `foodId`         INT(11)          DEFAULT NULL,
  `foodStandard`   VARCHAR(255)     DEFAULT NULL,
  `foodStandardId` INT(11)          DEFAULT NULL,
  `goodsSellName`  VARCHAR(255)     DEFAULT NULL,
  `num`            INT(11)          DEFAULT NULL,
  `openid`         INT(11)          DEFAULT NULL,
  `price`          DECIMAL(19, 2)   DEFAULT NULL,
  `pricedis`       DECIMAL(19, 2)   DEFAULT NULL,
  `total`          DECIMAL(19, 2)   DEFAULT NULL,
  `aceptpay`       INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_cid` (`cid`),
  KEY `idx_cid_fid` (`cid`, `foodId`, `foodStandardId`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 114
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_shoppingtrolley`
--

DROP TABLE IF EXISTS `runfast_shoppingtrolley`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_shoppingtrolley` (
  `id`                    INT(11) NOT NULL AUTO_INCREMENT,
  `barCode`               VARCHAR(255)     DEFAULT NULL,
  `businessId`            INT(11)          DEFAULT NULL,
  `businessName`          VARCHAR(255)     DEFAULT NULL,
  `cid`                   INT(11)          DEFAULT NULL,
  `cname`                 VARCHAR(255)     DEFAULT NULL,
  `goodsSellId`           INT(11)          DEFAULT NULL,
  `goodsSellName`         VARCHAR(255)     DEFAULT NULL,
  `goodsSellOptionId`     INT(11)          DEFAULT NULL,
  `goodsSellOptionName`   VARCHAR(255)     DEFAULT NULL,
  `goodsSellStandardId`   INT(11)          DEFAULT NULL,
  `goodsSellStandardName` VARCHAR(255)     DEFAULT NULL,
  `num`                   INT(11)          DEFAULT NULL,
  `price`                 DOUBLE           DEFAULT NULL,
  `openid`                VARCHAR(255)     DEFAULT NULL,
  `GoodsSellRecordId`     INT(11)          DEFAULT NULL,
  `snum`                  INT(11)          DEFAULT NULL,
  `packing`               DECIMAL(19, 2)   DEFAULT NULL,
  `ptype`                 INT(11)          DEFAULT NULL,
  `disprice`              DECIMAL(19, 2)   DEFAULT NULL,
  `pricedis`              DECIMAL(19, 2)   DEFAULT NULL,
  `optionIds`             VARCHAR(255)     DEFAULT NULL,
  `islimited`             INT(11)          DEFAULT NULL,
  `limitNum`              INT(11)          DEFAULT NULL,
  `full`                  DECIMAL(19, 2)   DEFAULT NULL,
  `total`                 DECIMAL(19, 2)   DEFAULT NULL,
  `couponid`              INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_cid_openid_businessId` (`cid`, `openid`, `businessId`),
  KEY `idx_openid_busid` (`businessId`, `openid`),
  KEY `idx_cid_busid` (`businessId`, `cid`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 1573677
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_sms_bill`
--

DROP TABLE IF EXISTS `runfast_sms_bill`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_sms_bill` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `createTime` DATETIME         DEFAULT NULL,
  `descx`      VARCHAR(255)     DEFAULT NULL,
  `mession`    VARCHAR(255)     DEFAULT NULL,
  `msg`        VARCHAR(255)     DEFAULT NULL,
  `phone`      VARCHAR(255)     DEFAULT NULL,
  `readSata`   INT(11)          DEFAULT NULL,
  `recordId`   INT(11)          DEFAULT NULL,
  `redate`     DATETIME         DEFAULT NULL,
  `sendState`  INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_sms_report`
--

DROP TABLE IF EXISTS `runfast_sms_report`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_sms_report` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `createTime` DATETIME         DEFAULT NULL,
  `mession`    VARCHAR(20)      DEFAULT NULL,
  `passwd`     VARCHAR(50)      DEFAULT NULL,
  `phone`      VARCHAR(32)      DEFAULT NULL,
  `status`     VARCHAR(20)      DEFAULT NULL,
  `unitid`     VARCHAR(20)      DEFAULT NULL,
  `username`   VARCHAR(50)      DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_sms_sendrecord`
--

DROP TABLE IF EXISTS `runfast_sms_sendrecord`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_sms_sendrecord` (
  `id`       INT(11) NOT NULL AUTO_INCREMENT,
  `ipAddr`   VARCHAR(255)     DEFAULT NULL,
  `mession`  VARCHAR(255)     DEFAULT NULL,
  `msg`      VARCHAR(255)     DEFAULT NULL,
  `phone`    VARCHAR(255)     DEFAULT NULL,
  `sendTime` DATETIME         DEFAULT NULL,
  `smstype`  INT(11)          DEFAULT NULL,
  `xcode`    VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 41494
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_special`
--

DROP TABLE IF EXISTS `runfast_special`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_special` (
  `id`      INT(11) NOT NULL AUTO_INCREMENT,
  `imgpath` VARCHAR(255)     DEFAULT NULL,
  `name`    VARCHAR(255)     DEFAULT NULL,
  `sort`    INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_specialcchild`
--

DROP TABLE IF EXISTS `runfast_specialcchild`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_specialcchild` (
  `id`        INT(11) NOT NULL AUTO_INCREMENT,
  `foodid`    INT(11)          DEFAULT NULL,
  `foodimg`   VARCHAR(255)     DEFAULT NULL,
  `foodname`  VARCHAR(255)     DEFAULT NULL,
  `sid`       INT(11)          DEFAULT NULL,
  `sname`     VARCHAR(255)     DEFAULT NULL,
  `specialid` INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 2
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_stock`
--

DROP TABLE IF EXISTS `runfast_stock`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_stock` (
  `id`             INT(11) NOT NULL AUTO_INCREMENT,
  `agentId`        INT(11)          DEFAULT NULL,
  `agentName`      VARCHAR(100)     DEFAULT NULL,
  `area`           VARCHAR(255)     DEFAULT NULL,
  `areaId`         INT(11)          DEFAULT NULL,
  `city`           VARCHAR(255)     DEFAULT NULL,
  `cityId`         INT(11)          DEFAULT NULL,
  `foodId`         INT(11)          DEFAULT NULL,
  `foodStandard`   VARCHAR(255)     DEFAULT NULL,
  `foodStandardId` INT(11)          DEFAULT NULL,
  `frontNums`      INT(11)          DEFAULT NULL,
  `latestNums`     INT(11)          DEFAULT NULL,
  `name`           VARCHAR(255)     DEFAULT NULL,
  `nums`           INT(11)          DEFAULT NULL,
  `price`          DECIMAL(19, 2)   DEFAULT NULL,
  `proCode`        VARCHAR(255)     DEFAULT NULL,
  `supportCode`    VARCHAR(255)     DEFAULT NULL,
  `supportId`      INT(11)          DEFAULT NULL,
  `supportName`    VARCHAR(255)     DEFAULT NULL,
  `tempNums`       INT(11)          DEFAULT NULL,
  `usefulNums`     INT(11)          DEFAULT NULL,
  `province`       VARCHAR(255)     DEFAULT NULL,
  `provinceId`     INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_angentid_foodid` (`agentId`, `foodId`, `foodStandardId`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 12
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_stock_history`
--

DROP TABLE IF EXISTS `runfast_stock_history`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_stock_history` (
  `id`          INT(11) NOT NULL AUTO_INCREMENT,
  `agentId`     INT(11)          DEFAULT NULL,
  `agentName`   VARCHAR(100)     DEFAULT NULL,
  `area`        VARCHAR(255)     DEFAULT NULL,
  `areaId`      INT(11)          DEFAULT NULL,
  `city`        VARCHAR(255)     DEFAULT NULL,
  `cityId`      INT(11)          DEFAULT NULL,
  `costCode`    VARCHAR(255)     DEFAULT NULL,
  `createTime`  DATETIME         DEFAULT NULL,
  `foodId`      INT(11)          DEFAULT NULL,
  `foodName`    VARCHAR(255)     DEFAULT NULL,
  `frontNums`   INT(11)          DEFAULT NULL,
  `latestNums`  INT(11)          DEFAULT NULL,
  `proCode`     VARCHAR(255)     DEFAULT NULL,
  `state`       INT(11)          DEFAULT NULL,
  `stateId`     INT(11)          DEFAULT NULL,
  `supportCode` VARCHAR(255)     DEFAULT NULL,
  `supportId`   INT(11)          DEFAULT NULL,
  `supportName` VARCHAR(255)     DEFAULT NULL,
  `foodstand`   VARCHAR(255)     DEFAULT NULL,
  `foodstandId` INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 15
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_support`
--

DROP TABLE IF EXISTS `runfast_support`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_support` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `byRoles`    VARCHAR(255)     DEFAULT NULL,
  `code`       VARCHAR(255)     DEFAULT NULL,
  `contract`   VARCHAR(255)     DEFAULT NULL,
  `createTime` DATETIME         DEFAULT NULL,
  `name`       VARCHAR(255)     DEFAULT NULL,
  `password`   VARCHAR(255)     DEFAULT NULL,
  `phone`      VARCHAR(255)     DEFAULT NULL,
  `userName`   VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 7
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_tellcharge`
--

DROP TABLE IF EXISTS `runfast_tellcharge`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_tellcharge` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `createTime` DATETIME         DEFAULT NULL,
  `operator`   INT(11)          DEFAULT NULL,
  `price`      DECIMAL(19, 2)   DEFAULT NULL,
  `totalPay`   DECIMAL(19, 2)   DEFAULT NULL,
  `type`       INT(11)          DEFAULT NULL,
  `quantity`   INT(11)          DEFAULT NULL,
  `isflow`     INT(11)          DEFAULT NULL,
  `locking`    INT(11)          DEFAULT NULL,
  `usable`     INT(11)          DEFAULT NULL,
  `remarks`    LONGTEXT,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_transorder`
--

DROP TABLE IF EXISTS `runfast_transorder`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_transorder` (
  `id`           INT(20)     NOT NULL AUTO_INCREMENT,
  `orderCode`    VARCHAR(20) NOT NULL,
  `orderId`      INT(20)     NOT NULL,
  `askShopperId` INT(20)     NOT NULL,
  `resShopperId` INT(20)     NOT NULL,
  `status`       INT(20)     NOT NULL,
  `takeType`     INT(20)     NOT NULL,
  `comment`      VARCHAR(200)         DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `orderCode` (`orderCode`) USING BTREE,
  UNIQUE KEY `orderCode_2` (`orderCode`),
  UNIQUE KEY `orderCode_3` (`orderCode`),
  UNIQUE KEY `orderCode_4` (`orderCode`),
  UNIQUE KEY `orderCode_5` (`orderCode`),
  UNIQUE KEY `orderCode_6` (`orderCode`),
  UNIQUE KEY `orderCode_7` (`orderCode`),
  UNIQUE KEY `orderCode_8` (`orderCode`),
  UNIQUE KEY `orderCode_9` (`orderCode`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 255
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_twon`
--

DROP TABLE IF EXISTS `runfast_twon`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_twon` (
  `id`           INT(11) NOT NULL AUTO_INCREMENT,
  `agentId`      INT(11)          DEFAULT NULL,
  `agentName`    VARCHAR(100)     DEFAULT NULL,
  `byName`       VARCHAR(255)     DEFAULT NULL,
  `byid`         VARCHAR(255)     DEFAULT NULL,
  `city`         VARCHAR(255)     DEFAULT NULL,
  `cityCode`     VARCHAR(255)     DEFAULT NULL,
  `cityId`       INT(11)          DEFAULT NULL,
  `code`         VARCHAR(255)     DEFAULT NULL,
  `isShow`       INT(11)          DEFAULT NULL,
  `level`        INT(11)          DEFAULT NULL,
  `name`         VARCHAR(255)     DEFAULT NULL,
  `province`     VARCHAR(255)     DEFAULT NULL,
  `provinceCode` VARCHAR(255)     DEFAULT NULL,
  `provinceId`   INT(11)          DEFAULT NULL,
  `region`       VARCHAR(255)     DEFAULT NULL,
  `regionCode`   VARCHAR(255)     DEFAULT NULL,
  `regionId`     INT(11)          DEFAULT NULL,
  `sort`         INT(11)          DEFAULT NULL,
  `typeImages`   VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_regionid` (`regionCode`, `regionId`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 3
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_userbalancecount`
--

DROP TABLE IF EXISTS `runfast_userbalancecount`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_userbalancecount` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `countTime`  DATETIME         DEFAULT NULL,
  `createTime` DATETIME         DEFAULT NULL,
  `price`      DECIMAL(19, 2)   DEFAULT NULL,
  `userId`     INT(11)          DEFAULT NULL,
  `username`   VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 9335130
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_uuemessge`
--

DROP TABLE IF EXISTS `runfast_uuemessge`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_uuemessge` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `context`    VARCHAR(255)     DEFAULT NULL,
  `createTime` DATETIME         DEFAULT NULL,
  `type`       INT(11)          DEFAULT NULL,
  `agentId`    INT(11)          DEFAULT NULL,
  `agentName`  VARCHAR(100)     DEFAULT NULL,
  `end`        DATETIME         DEFAULT NULL,
  `start`      DATETIME         DEFAULT NULL,
  `title`      VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 5
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_visit_record`
--

DROP TABLE IF EXISTS `runfast_visit_record`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_visit_record` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `cid`        INT(11)          DEFAULT NULL,
  `cname`      VARCHAR(255)     DEFAULT NULL,
  `createTime` DATETIME         DEFAULT NULL,
  `pid`        INT(11)          DEFAULT NULL,
  `pname`      VARCHAR(255)     DEFAULT NULL,
  `type`       INT(11)          DEFAULT NULL,
  `visitdate`  VARCHAR(255)     DEFAULT NULL,
  `visitnum`   INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 134944
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_withdraw`
--

DROP TABLE IF EXISTS `runfast_withdraw`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_withdraw` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `account`    VARCHAR(255)     DEFAULT NULL,
  `atuditId`   INT(11)          DEFAULT NULL,
  `atuditName` VARCHAR(255)     DEFAULT NULL,
  `atuditTime` DATETIME         DEFAULT NULL,
  `content`    VARCHAR(255)     DEFAULT NULL,
  `createTime` DATETIME         DEFAULT NULL,
  `monetary`   DECIMAL(19, 2)   DEFAULT NULL,
  `readyTime`  DATETIME         DEFAULT NULL,
  `status`     INT(11)          DEFAULT NULL,
  `type`       INT(11)          DEFAULT NULL,
  `userId`     INT(11)          DEFAULT NULL,
  `userMobile` VARCHAR(255)     DEFAULT NULL,
  `userName`   VARCHAR(255)     DEFAULT NULL,
  `applytype`  INT(11)          DEFAULT NULL,
  `state`      INT(11)          DEFAULT NULL,
  `readrName`  VARCHAR(255)     DEFAULT NULL,
  `rid`        INT(11)          DEFAULT NULL,
  `agentId`    INT(11)          DEFAULT NULL,
  `agentName`  VARCHAR(100)     DEFAULT NULL,
  `bankid`     INT(11)          DEFAULT NULL,
  `banktype`   VARCHAR(50)      DEFAULT NULL,
  `name`       VARCHAR(50)      DEFAULT NULL,
  `wdate`      VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_userId_applytype_status` (`userId`, `applytype`, `state`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 19403
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_withdraworder`
--

DROP TABLE IF EXISTS `runfast_withdraworder`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_withdraworder` (
  `id`           INT(11) NOT NULL AUTO_INCREMENT,
  `businessId`   INT(11)          DEFAULT NULL,
  `businessName` VARCHAR(255)     DEFAULT NULL,
  `createTime`   DATETIME         DEFAULT NULL,
  `oid`          INT(11)          DEFAULT NULL,
  `orderCode`    VARCHAR(255)     DEFAULT NULL,
  `totalpay`     DECIMAL(19, 2)   DEFAULT NULL,
  `wid`          INT(11)          DEFAULT NULL,
  `businesspay`  DECIMAL(19, 2)   DEFAULT NULL,
  `businessget`  DECIMAL(19, 2)   DEFAULT NULL,
  `agentId`      INT(11)          DEFAULT NULL,
  `agentName`    VARCHAR(100)     DEFAULT NULL,
  `agentget`     DECIMAL(19, 2)   DEFAULT NULL,
  `showps`       DECIMAL(19, 2)   DEFAULT NULL,
  `usermunber`   VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_wid` (`wid`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 428977
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `runfast_withdrawrecord`
--

DROP TABLE IF EXISTS `runfast_withdrawrecord`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `runfast_withdrawrecord` (
  `id`             INT(11) NOT NULL AUTO_INCREMENT,
  `acctDate`       DATETIME         DEFAULT NULL,
  `businessId`     INT(11)          DEFAULT NULL,
  `businessName`   VARCHAR(255)     DEFAULT NULL,
  `createTime`     DATETIME         DEFAULT NULL,
  `wid`            INT(11)          DEFAULT NULL,
  `withdrawNumber` DECIMAL(19, 2)   DEFAULT NULL,
  `agentId`        INT(11)          DEFAULT NULL,
  `agentName`      VARCHAR(100)     DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_bid` (`businessId`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 10045
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wp_followers`
--

DROP TABLE IF EXISTS `wp_followers`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_followers` (
  `id`            INT(11) NOT NULL AUTO_INCREMENT,
  `aid`           INT(11)          DEFAULT NULL,
  `city`          VARCHAR(50)      DEFAULT NULL,
  `country`       VARCHAR(50)      DEFAULT NULL,
  `createTime`    DATETIME         DEFAULT NULL,
  `groupId`       INT(11)          DEFAULT NULL,
  `headimgurl`    LONGTEXT,
  `language`      VARCHAR(20)      DEFAULT NULL,
  `modifyTime`    DATETIME         DEFAULT NULL,
  `nickname`      VARCHAR(100)     DEFAULT NULL,
  `openid`        VARCHAR(32)      DEFAULT NULL,
  `province`      VARCHAR(30)      DEFAULT NULL,
  `sex`           INT(11)          DEFAULT NULL,
  `subscribe`     INT(11)          DEFAULT NULL,
  `subscribeTime` DATETIME         DEFAULT NULL,
  `type`          INT(11)          DEFAULT NULL,
  `typeId`        INT(11)          DEFAULT NULL,
  `unionid`       VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_openId` (`openid`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 138136
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wp_group`
--

DROP TABLE IF EXISTS `wp_group`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_group` (
  `id`          INT(11) NOT NULL AUTO_INCREMENT,
  `count`       INT(11)          DEFAULT NULL,
  `createTime`  DATETIME         DEFAULT NULL,
  `name`        VARCHAR(50)      DEFAULT NULL,
  `wxAccountId` INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wp_image`
--

DROP TABLE IF EXISTS `wp_image`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_image` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `cid`        INT(11)          DEFAULT NULL,
  `cname`      VARCHAR(255)     DEFAULT NULL,
  `createTime` DATETIME         DEFAULT NULL,
  `eid`        INT(11)          DEFAULT NULL,
  `fileSize`   BIGINT(20)       DEFAULT NULL,
  `path`       LONGTEXT,
  `pototype`   INT(11)          DEFAULT NULL,
  `sort`       INT(11)          DEFAULT NULL,
  `title`      LONGTEXT,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wp_image_text`
--

DROP TABLE IF EXISTS `wp_image_text`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_image_text` (
  `id`          INT(11) NOT NULL AUTO_INCREMENT,
  `author`      VARCHAR(255)     DEFAULT NULL,
  `browseCount` INT(11)          DEFAULT NULL,
  `cid`         INT(11)          DEFAULT NULL,
  `cname`       VARCHAR(255)     DEFAULT NULL,
  `content`     LONGTEXT,
  `createTime`  DATETIME         DEFAULT NULL,
  `editTime`    DATETIME         DEFAULT NULL,
  `eid`         INT(11)          DEFAULT NULL,
  `flag`        BIT(1)           DEFAULT NULL,
  `link`        VARCHAR(255)     DEFAULT NULL,
  `path`        VARCHAR(255)     DEFAULT NULL,
  `praiseCount` INT(11)          DEFAULT NULL,
  `subtitle`    VARCHAR(255)     DEFAULT NULL,
  `title`       VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wp_image_text_item`
--

DROP TABLE IF EXISTS `wp_image_text_item`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_image_text_item` (
  `id`           INT(11) NOT NULL AUTO_INCREMENT,
  `author`       VARCHAR(255)     DEFAULT NULL,
  `browseCount`  INT(11)          DEFAULT NULL,
  `byid`         INT(11)          DEFAULT NULL,
  `content`      LONGTEXT,
  `createTime`   DATETIME         DEFAULT NULL,
  `eid`          INT(11)          DEFAULT NULL,
  `link`         LONGTEXT,
  `originalLink` VARCHAR(255)     DEFAULT NULL,
  `path`         VARCHAR(255)     DEFAULT NULL,
  `praiseCount`  INT(11)          DEFAULT NULL,
  `subtitle`     VARCHAR(255)     DEFAULT NULL,
  `title`        VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wp_mass_msg`
--

DROP TABLE IF EXISTS `wp_mass_msg`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_mass_msg` (
  `id`          INT(11) NOT NULL AUTO_INCREMENT,
  `content`     LONGTEXT,
  `createTime`  DATETIME         DEFAULT NULL,
  `eId`         INT(11)          DEFAULT NULL,
  `editTime`    DATETIME         DEFAULT NULL,
  `ename`       VARCHAR(255)     DEFAULT NULL,
  `flownum`     INT(11)          DEFAULT NULL,
  `msg_id`      VARCHAR(255)     DEFAULT NULL,
  `sendFalse`   INT(11)          DEFAULT NULL,
  `sendStatus`  VARCHAR(255)     DEFAULT NULL,
  `sendSuccess` INT(11)          DEFAULT NULL,
  `sendTime`    DATETIME         DEFAULT NULL,
  `sended`      INT(11)          DEFAULT NULL,
  `statusDate`  DATETIME         DEFAULT NULL,
  `title`       VARCHAR(255)     DEFAULT NULL,
  `totalCount`  INT(11)          DEFAULT NULL,
  `type`        INT(11)          DEFAULT NULL,
  `typeId`      INT(11)          DEFAULT NULL,
  `url`         VARCHAR(255)     DEFAULT NULL,
  `wxcode`      VARCHAR(255)     DEFAULT NULL,
  `wxid`        INT(11)          DEFAULT NULL,
  `wxname`      VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wp_menu`
--

DROP TABLE IF EXISTS `wp_menu`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_menu` (
  `id`          INT(11) NOT NULL AUTO_INCREMENT,
  `aid`         INT(11)          DEFAULT NULL,
  `byid`        INT(11) NOT NULL,
  `content`     LONGTEXT,
  `createTime`  DATETIME         DEFAULT NULL,
  `keytype`     INT(11)          DEFAULT NULL,
  `mkey`        VARCHAR(100)     DEFAULT NULL,
  `name`        VARCHAR(100)     DEFAULT NULL,
  `type`        VARCHAR(100)     DEFAULT NULL,
  `url`         VARCHAR(255)     DEFAULT NULL,
  `wxAccountId` INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 16
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wp_menu_click_log`
--

DROP TABLE IF EXISTS `wp_menu_click_log`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_menu_click_log` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `createTime` DATETIME         DEFAULT NULL,
  `eid`        INT(11)          DEFAULT NULL,
  `ename`      VARCHAR(255)     DEFAULT NULL,
  `fid`        INT(11)          DEFAULT NULL,
  `mbyid`      INT(11)          DEFAULT NULL,
  `mid`        INT(11)          DEFAULT NULL,
  `mname`      VARCHAR(255)     DEFAULT NULL,
  `openid`     VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 34400
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wp_msg_setting`
--

DROP TABLE IF EXISTS `wp_msg_setting`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_msg_setting` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `createTime` DATETIME         DEFAULT NULL,
  `editTime`   DATETIME         DEFAULT NULL,
  `eid`        INT(11)          DEFAULT NULL,
  `keyword`    VARCHAR(200)     DEFAULT NULL,
  `msgById`    INT(11)          DEFAULT NULL,
  `msgType`    INT(11)          DEFAULT NULL,
  `respType`   INT(11)          DEFAULT NULL,
  `url`        VARCHAR(255)     DEFAULT NULL,
  `welcomeMsg` LONGTEXT,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wp_msg_setting_log`
--

DROP TABLE IF EXISTS `wp_msg_setting_log`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_msg_setting_log` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `content`    LONGTEXT,
  `createTime` DATETIME         DEFAULT NULL,
  `eid`        INT(11)          DEFAULT NULL,
  `fid`        INT(11)          DEFAULT NULL,
  `keyword`    VARCHAR(200)     DEFAULT NULL,
  `mid`        INT(11)          DEFAULT NULL,
  `openid`     VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 9766
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wp_qrcode`
--

DROP TABLE IF EXISTS `wp_qrcode`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_qrcode` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `content`    LONGTEXT,
  `createTime` DATETIME         DEFAULT NULL,
  `editTime`   DATETIME         DEFAULT NULL,
  `eid`        INT(11)          DEFAULT NULL,
  `ename`      VARCHAR(255)     DEFAULT NULL,
  `name`       VARCHAR(255)     DEFAULT NULL,
  `scanCount`  INT(11)          DEFAULT NULL,
  `sceneId`    INT(11)          DEFAULT NULL,
  `stype`      INT(11)          DEFAULT NULL,
  `stypeid`    INT(11)          DEFAULT NULL,
  `token`      VARCHAR(255)     DEFAULT NULL,
  `url`        VARCHAR(255)     DEFAULT NULL,
  `wxcode`     VARCHAR(255)     DEFAULT NULL,
  `wxid`       INT(11)          DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wp_qrcode_record`
--

DROP TABLE IF EXISTS `wp_qrcode_record`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_qrcode_record` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `createTime` DATETIME         DEFAULT NULL,
  `eid`        INT(11)          DEFAULT NULL,
  `ename`      VARCHAR(255)     DEFAULT NULL,
  `fid`        INT(11)          DEFAULT NULL,
  `openId`     VARCHAR(255)     DEFAULT NULL,
  `qrid`       INT(11)          DEFAULT NULL,
  `qrname`     VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wp_video`
--

DROP TABLE IF EXISTS `wp_video`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_video` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `cid`        INT(11)          DEFAULT NULL,
  `createTime` DATETIME         DEFAULT NULL,
  `editTime`   DATETIME         DEFAULT NULL,
  `title`      VARCHAR(100)     DEFAULT NULL,
  `url`        VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wp_voice`
--

DROP TABLE IF EXISTS `wp_voice`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_voice` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `cid`        INT(11)          DEFAULT NULL,
  `cname`      VARCHAR(255)     DEFAULT NULL,
  `createTime` DATETIME         DEFAULT NULL,
  `eid`        INT(11)          DEFAULT NULL,
  `fileSize`   BIGINT(20)       DEFAULT NULL,
  `path`       LONGTEXT,
  `sort`       INT(11)          DEFAULT NULL,
  `title`      LONGTEXT,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wp_wx_setting`
--

DROP TABLE IF EXISTS `wp_wx_setting`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_wx_setting` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `wxurlImage` VARCHAR(255)     DEFAULT NULL,
  `wxurlroot`  VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `wp_wxaccount`
--

DROP TABLE IF EXISTS `wp_wxaccount`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wp_wxaccount` (
  `id`              INT(11) NOT NULL AUTO_INCREMENT,
  `accessToken`     VARCHAR(200)     DEFAULT NULL,
  `account`         VARCHAR(50)      DEFAULT NULL,
  `apiCode`         VARCHAR(20)      DEFAULT NULL,
  `appID`           VARCHAR(50)      DEFAULT NULL,
  `appSecret`       VARCHAR(50)      DEFAULT NULL,
  `applyTime`       DATETIME         DEFAULT NULL,
  `createTime`      DATETIME         DEFAULT NULL,
  `jsapiExpiresIn`  INT(11)          DEFAULT NULL,
  `jsapiTicket`     VARCHAR(200)     DEFAULT NULL,
  `jsapiTicketTime` DATETIME         DEFAULT NULL,
  `modifyTime`      DATETIME         DEFAULT NULL,
  `passwd`          VARCHAR(50)      DEFAULT NULL,
  `status`          INT(11)          DEFAULT NULL,
  `token`           VARCHAR(50)      DEFAULT NULL,
  `tokenTime`       DATETIME         DEFAULT NULL,
  `welcomeMsg`      VARCHAR(200)     DEFAULT NULL,
  `wxName`          VARCHAR(50)      DEFAULT NULL,
  `wxPartnerKey`    VARCHAR(255)     DEFAULT NULL,
  `wxPayMchId`      VARCHAR(255)     DEFAULT NULL,
  `wxNotifyUrl`     VARCHAR(255)     DEFAULT NULL,
  `shopAppID`       VARCHAR(255)     DEFAULT NULL,
  `shopAppSecret`   VARCHAR(255)     DEFAULT NULL,
  `shopwxPayMchId`  VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  AUTO_INCREMENT = 5
  DEFAULT CHARSET = utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Final view structure for view `runfast_food_view`
--

/*!50001 DROP VIEW IF EXISTS `runfast_food_view`*/;
/*!50001 SET @saved_cs_client = @@character_set_client */;
/*!50001 SET @saved_cs_results = @@character_set_results */;
/*!50001 SET @saved_col_connection = @@collation_connection */;
/*!50001 SET character_set_client = utf8 */;
/*!50001 SET character_set_results = utf8 */;
/*!50001 SET collation_connection = utf8_general_ci */;
/*!50001 CREATE ALGORITHM = UNDEFINED */
  /*!50013 DEFINER =`root`@`localhost`
  SQL SECURITY DEFINER */
  /*!50001 VIEW `runfast_food_view` AS
  SELECT
    `a`.`id`          AS `id`,
    `a`.`realPrice`   AS `realPrice`,
    `a`.`price`       AS `price`,
    `a`.`proCode`     AS `proCode`,
    `a`.`foodId`      AS `foodId`,
    `b`.`supportName` AS `supportName`,
    `b`.`supportId`   AS `supportId`,
    `a`.`yunPrice`    AS `yunPrice`,
    `b`.`name`        AS `name`,
    `b`.`unitId`      AS `unitId`,
    `b`.`unitName`    AS `unitName`,
    `b`.`typeId`      AS `typeId`,
    `b`.`typeName`    AS `typeName`,
    `b`.`upOrDown`    AS `upOrDown`,
    `b`.`imglogo`     AS `imglogo`,
    `b`.`info`        AS `info`,
    `b`.`sort`        AS `sort`,
    `b`.`saleNums`    AS `saleNums`,
    `b`.`brandId`     AS `brandId`,
    `b`.`brandName`   AS `brandName`
  FROM (`runfast_food_standard` `a` LEFT JOIN `runfast_food` `b` ON ((`a`.`foodId` = `b`.`id`))) */;
/*!50001 SET character_set_client = @saved_cs_client */;
/*!50001 SET character_set_results = @saved_cs_results */;
/*!50001 SET collation_connection = @saved_col_connection */;

--
-- Final view structure for view `runfast_goods_sell_view`
--

/*!50001 DROP VIEW IF EXISTS `runfast_goods_sell_view`*/;
/*!50001 SET @saved_cs_client = @@character_set_client */;
/*!50001 SET @saved_cs_results = @@character_set_results */;
/*!50001 SET @saved_col_connection = @@collation_connection */;
/*!50001 SET character_set_client = utf8 */;
/*!50001 SET character_set_results = utf8 */;
/*!50001 SET collation_connection = utf8_general_ci */;
/*!50001 CREATE ALGORITHM = UNDEFINED */
  /*!50013 DEFINER =`root`@`localhost`
  SQL SECURITY DEFINER */
  /*!50001 VIEW `runfast_goods_sell_view` AS
  SELECT
    `a`.`id`           AS `id`,
    `a`.`name`         AS `sname`,
    `a`.`price`        AS `price`,
    `b`.`businessName` AS `businessName`,
    `b`.`businessId`   AS `businessId`,
    `b`.`id`           AS `gid`,
    `b`.`name`         AS `name`,
    `b`.`content`      AS `content`,
    `b`.`sellTypeId`   AS `sellTypeId`,
    `b`.`sellTypeName` AS `sellTypeName`,
    `b`.`status`       AS `status`,
    `b`.`Star`         AS `star`,
    `b`.`salesnum`     AS `salesnum`,
    `b`.`num`          AS `num`,
    `b`.`agentName`    AS `agentName`,
    `b`.`agentId`      AS `agentId`,
    `b`.`createTime`   AS `createTime`
  FROM (`runfast_goods_sell_standard` `a` LEFT JOIN `runfast_goods_sell` `b` ON ((`a`.`goodsSellId` = `b`.`id`))) */;
/*!50001 SET character_set_client = @saved_cs_client */;
/*!50001 SET character_set_results = @saved_cs_results */;
/*!50001 SET collation_connection = @saved_col_connection */;
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2018-02-26 18:15:48
