package com.runfast.common.mybatis;

import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.Context;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.internal.DefaultShellCallback;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Administrator on 2018/2/7.
 */
public class Generator {

    static DefaultResourceLoader resourceLoader = new DefaultResourceLoader();

    public static void main(String[] args) throws Exception {
        List<String> warnings = new ArrayList<String>();

        boolean overwrite = true;


        Resource loaderResource = resourceLoader.getResource("classpath:generatorConfig.xml");
        InputStream resource = loaderResource.getInputStream();

        ConfigurationParser cp = new ConfigurationParser(warnings);
        Configuration config = cp.parseConfiguration(resource);
        Context defaultContext = config.getContext("default");

        /*PropertiesLoaderUtils.fillProperties(defaultContext.getProperties(), resourceLoader.getResource("classpath:application.properties"));
        String classpath = Generator.class.getClassLoader().getResource("").toURI().toString();
        classpath += "com.paotui.common.dao";
        defaultContext.addProperty("targetProject", "d:\\java");*/

        DefaultShellCallback callback = new DefaultShellCallback(overwrite);
        MyBatisGenerator myBatisGenerator = new MyBatisGenerator(config, callback, warnings);
        myBatisGenerator.generate(null);
        for (String warning : warnings) {
            System.out.println(warning);
        }



    }
}
