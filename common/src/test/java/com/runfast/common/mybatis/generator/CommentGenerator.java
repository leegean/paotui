package com.runfast.common.mybatis.generator;

import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.java.Field;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.mybatis.generator.internal.DefaultCommentGenerator;
import org.mybatis.generator.internal.util.StringUtility;

public class CommentGenerator extends DefaultCommentGenerator {
    @Override
    public void addFieldComment(Field field, IntrospectedTable introspectedTable, IntrospectedColumn introspectedColumn) {
        super.addFieldComment(field, introspectedTable, introspectedColumn);
        if (introspectedColumn.getRemarks() != null && !"".equals(introspectedColumn.getRemarks())) {
            field.addJavaDocLine("/**");
            field.addJavaDocLine(" * " + introspectedColumn.getRemarks());
//            addJavadocTag(field, false);
            field.addJavaDocLine(" */");
        }
    }

    @Override
    public void addModelClassComment(TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
        super.addModelClassComment(topLevelClass, introspectedTable);
        String remarks = introspectedTable.getRemarks();
        if (!StringUtility.stringHasValue(remarks)) return;

        topLevelClass.addJavaDocLine("/**"); //$NON-NLS-1$


        String[] remarkLines = remarks.split(System.getProperty("line.separator"));  //$NON-NLS-1$
        for (String remarkLine : remarkLines) {
            topLevelClass.addJavaDocLine(" *   " + remarkLine);  //$NON-NLS-1$
        }
        topLevelClass.addJavaDocLine(" *"); //$NON-NLS-1$

//        addJavadocTag(topLevelClass, true);

        topLevelClass.addJavaDocLine(" */"); //$NON-NLS-1$
    }
}
