package com.runfast.common.mybatis.generator;

import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.xml.Attribute;
import org.mybatis.generator.api.dom.xml.TextElement;
import org.mybatis.generator.api.dom.xml.XmlElement;
import org.mybatis.generator.codegen.mybatis3.xmlmapper.elements.AbstractXmlElementGenerator;
import org.mybatis.generator.config.Context;

public class CountByExampleWithPageableElementGenerator extends AbstractXmlElementGenerator {
    public CountByExampleWithPageableElementGenerator(IntrospectedTable introspectedTable, Context context) {
        super();
        this.introspectedTable = introspectedTable;
        this.context = context;
    }

    @Override
    public void addElements(XmlElement parentElement) {
        resetElement(parentElement);

        XmlElement answer = parentElement;
        answer.setName("select"); //$NON-NLS-1$

        String fqjt = introspectedTable.getExampleType();

        answer.addAttribute(new Attribute(
                "id", introspectedTable.getCountByExampleStatementId() + "WithPageable")); //$NON-NLS-1$
        answer.addAttribute(new Attribute("parameterType", "map")); //$NON-NLS-1$
        answer.addAttribute(new Attribute("resultType", "java.lang.Integer")); //$NON-NLS-1$ //$NON-NLS-2$

        context.getCommentGenerator().addComment(answer);

        StringBuilder sb = new StringBuilder();
        sb.append("select count(*) from "); //$NON-NLS-1$
        sb.append(introspectedTable
                .getAliasedFullyQualifiedTableNameAtRuntime());
        answer.addElement(new TextElement(sb.toString()));
        answer.addElement(getUpdateByExampleIncludeElement());

        XmlElement ifElement = new XmlElement("if"); //$NON-NLS-1$
        ifElement.addAttribute(new Attribute("test", "pageable != null")); //$NON-NLS-1$ //$NON-NLS-2$
        ifElement.addElement(new TextElement("limit ${pageable.offset},${pageable.pageSize}")); //$NON-NLS-1$
        answer.addElement(ifElement);

        /*if (context.getPlugins().sqlMapCountByExampleElementGenerated(
                answer, introspectedTable)) {
            parentElement.addElement(answer);
        }*/
    }

    private void resetElement(XmlElement parentElement) {
        parentElement.setName(null);
        parentElement.getAttributes().clear();
        parentElement.getElements().clear();
    }

    protected XmlElement getUpdateByExampleIncludeElement() {
        XmlElement ifElement = new XmlElement("if"); //$NON-NLS-1$
        ifElement.addAttribute(new Attribute("test", "example != null")); //$NON-NLS-1$ //$NON-NLS-2$

        XmlElement includeElement = new XmlElement("include"); //$NON-NLS-1$
        includeElement.addAttribute(new Attribute("refid", //$NON-NLS-1$
                introspectedTable.getMyBatis3UpdateByExampleWhereClauseId()));
        ifElement.addElement(includeElement);

        return ifElement;
    }
}
