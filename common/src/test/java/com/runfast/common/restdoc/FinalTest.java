package com.runfast.common.restdoc;

import com.runfast.paotui.restdocs.CatalogTest;
import com.runfast.paotui.restdocs.OrderTest;
import com.runfast.paotui.restdocs.PayTest;
import org.junit.AfterClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import java.io.*;

@RunWith(Suite.class)
@Suite.SuiteClasses({OrderTest.class,CatalogTest.class,PayTest.class})
public class FinalTest {

    @AfterClass
    public static void after() {
        System.out.println("======" + FinalTest.class.getSimpleName() + "." + "======");


        File items = new File("src/main/asciidoc/items.adoc");

        PrintWriter pw = null;
        try {
            pw = new PrintWriter(items);

            File sourceSnippetDir = new File("target\\generated-snippets");
            String snippetDirPrefix = "include::{snippets}";

            File[] clazzDirs = sourceSnippetDir.listFiles();
            for (File clazzDir : clazzDirs) {
                if (clazzDir.isDirectory()) {
                    File[] autoSections = clazzDir.listFiles(new FilenameFilter() {
                        @Override
                        public boolean accept(File dir, String name) {
                            return "auto-class-section.adoc".equals(name);
                        }
                    });

                    if (autoSections.length > 0) {
                        String path = autoSections[0].getPath();
                        pw.append(snippetDirPrefix + path.replace(sourceSnippetDir.getPath(), "") + "[]");

                        pw.println();

                    }
                }
            }

            pw.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }  finally {
            if (pw != null) pw.close();
        }
    }
}
