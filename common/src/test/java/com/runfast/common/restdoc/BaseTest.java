package com.runfast.common.restdoc;

import capital.scalable.restdocs.AutoDocumentation;
import capital.scalable.restdocs.OperationAttributeHelper;
import capital.scalable.restdocs.SnippetRegistry;
import capital.scalable.restdocs.constraints.ConstraintReader;
import capital.scalable.restdocs.constraints.ConstraintReaderImpl;
import capital.scalable.restdocs.jackson.JacksonResultHandlers;
import capital.scalable.restdocs.javadoc.JavadocReader;
import capital.scalable.restdocs.response.ResponseModifyingPreprocessors;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.runfast.common.restdoc.snippet.ClassSectionSnippet;
import com.runfast.common.restdoc.snippet.DefaultJavadocReader;
import com.runfast.common.restdoc.snippet.FirstGenericJacksonResponseFieldSnippet;
import com.runfast.common.utils.JsonUtils;
import com.runfast.common.web.entity.Result;
import org.apache.shiro.util.ThreadContext;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.ManualRestDocumentation;
import org.springframework.restdocs.RestDocumentationContext;
import org.springframework.restdocs.cli.CliDocumentation;
import org.springframework.restdocs.http.HttpDocumentation;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation;
import org.springframework.restdocs.operation.Operation;
import org.springframework.restdocs.operation.OperationRequest;
import org.springframework.restdocs.operation.preprocess.OperationPreprocessorAdapter;
import org.springframework.restdocs.operation.preprocess.Preprocessors;
import org.springframework.restdocs.test.OperationBuilder;
import org.springframework.test.annotation.Commit;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultHandler;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.Base64Utils;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.method.HandlerMethod;

import javax.servlet.Filter;
import javax.servlet.http.Cookie;
import java.io.File;

import static capital.scalable.restdocs.misc.AuthorizationSnippet.documentAuthorization;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.springframework.restdocs.templates.TemplateFormats.asciidoctor;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@AutoConfigureMockMvc
//@AutoConfigureRestDocs
@Commit
public abstract class BaseTest {
    @Autowired
    private WebApplicationContext context;

    @Rule
    public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation();

    protected static ObjectMapper mapper = JsonUtils.getMapper();
    protected static JavadocReader javadocReader;
    protected static ConstraintReader constraintReader;
    protected static OperationBuilder operationBuilder;

    protected MockMvc mockMvc;


    @Autowired
    private Filter[] springSecurityFilterChain;
    @Before
    public void setUp() {
        System.out.println("========setUp========");
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .addFilters(springSecurityFilterChain)
                .alwaysDo(JacksonResultHandlers.prepareJackson(mapper))
                .alwaysDo(new ResultHandler() {
                    @Override
                    public void handle(MvcResult result) throws Exception {
//                        OperationAttributeHelper.setAuthorization(result.getRequest(),"接口调用权限");
                        OperationAttributeHelper.setJavadocReader(result.getRequest(), new DefaultJavadocReader());
                    }
                })
                .alwaysDo(MockMvcRestDocumentation.document("{ClassName}/{methodName}",
                        Preprocessors.preprocessRequest(new OperationPreprocessorAdapter() {
                            @Override
                            public OperationRequest preprocess(OperationRequest request) {
                                return super.preprocess(request);
                            }
                        }),
                        Preprocessors.preprocessResponse(
                                ResponseModifyingPreprocessors.replaceBinaryContent(),
                                ResponseModifyingPreprocessors.limitJsonArrayLength(mapper),
                                Preprocessors.prettyPrint())))
                .apply(MockMvcRestDocumentation.documentationConfiguration(restDocumentation)
                        .uris()
                        .withScheme("http")
                        .withHost("localhost")
                        .withPort(8280)
                        .and().snippets()
                        .withDefaults(
                                AutoDocumentation.authorization("接口权限"),
                                CliDocumentation.curlRequest(),
                                CliDocumentation.httpieRequest(),
                                HttpDocumentation.httpRequest(),
                                HttpDocumentation.httpResponse(),
                                AutoDocumentation.requestFields(),
                                new FirstGenericJacksonResponseFieldSnippet(),
                                AutoDocumentation.pathParameters(),
                                AutoDocumentation.requestParameters(),
                                AutoDocumentation.description(),
                                AutoDocumentation.methodAndPath(),
                                AutoDocumentation.sectionBuilder()
                                        .snippetNames(
                                                SnippetRegistry.AUTHORIZATION,
                                                SnippetRegistry.PATH_PARAMETERS,

                                                SnippetRegistry.REQUEST_PARAMETERS,
                                                SnippetRegistry.REQUEST_FIELDS,
                                                SnippetRegistry.RESPONSE_FIELDS,
                                                SnippetRegistry.HTTP_REQUEST,
                                                SnippetRegistry.HTTP_RESPONSE)
                                        .skipEmpty(true)
                                        .build()))
                .build();

    }

    /*@TestConfiguration
    static class CustomizationConfiguration implements RestDocsMockMvcConfigurationCustomizer {
        @Override
        public void customize(MockMvcRestDocumentationConfigurer configurer) {
            configurer
                    .uris()
                    .withScheme("http")
                    .withHost("localhost")
                    .withPort(8280)
                    .and()
                    .snippets()
                    .withTemplateFormat(TemplateFormats.asciidoctor())
                    .withDefaults(
                            AutoDocumentation.authorization("接口权限"),
                            CliDocumentation.curlRequest(),
                            CliDocumentation.httpieRequest(),
                            HttpDocumentation.httpRequest(),
                            HttpDocumentation.httpResponse(),
                            AutoDocumentation.requestFields(),
                            new FirstGenericJacksonResponseFieldSnippet(),
                            AutoDocumentation.pathParameters(),
                            AutoDocumentation.requestParameters(),
                            AutoDocumentation.description(),
                            AutoDocumentation.methodAndPath(),
                            AutoDocumentation.sectionBuilder()
                                    .snippetNames(
                                            SnippetRegistry.AUTHORIZATION,
                                            SnippetRegistry.PATH_PARAMETERS,

                                            SnippetRegistry.REQUEST_PARAMETERS,
                                            SnippetRegistry.REQUEST_FIELDS,
                                            SnippetRegistry.RESPONSE_FIELDS,
                                            SnippetRegistry.HTTP_REQUEST,
                                            SnippetRegistry.HTTP_RESPONSE)
                                    .skipEmpty(true)
                                    .build());
        }
    }


    @TestConfiguration
    static class ResultHandlerConfiguration {

        @Bean
        public RestDocumentationResultHandler restDocumentation() {
            return MockMvcRestDocumentation.document("{ClassName}/{methodName}",
                    Preprocessors.preprocessRequest(),
                    Preprocessors.preprocessResponse(
                            ResponseModifyingPreprocessors.replaceBinaryContent(),
                            ResponseModifyingPreprocessors.limitJsonArrayLength(JsonUtils.getMapper()),
                            Preprocessors.prettyPrint()));
        }

    }*/
    protected RequestPostProcessor userToken() {
        return new RequestPostProcessor() {
            @Override
            public MockHttpServletRequest postProcessRequest(MockHttpServletRequest request) {
                // If the tests requires setup logic for users, you can place it here.
                // Authorization headers or cookies for users should be added here as well.
                String accessToken;
                try {
                    accessToken = getAccessToken("15871713751", "e10adc3949ba59abbe56e057f20f883e");
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
                request.addHeader("token", accessToken);
                return documentAuthorization(request, "token是必须的");
            }
        };
    }

    protected String getAccessToken(String username, String password) throws Exception {
        String authorization = "Basic "
                + new String(Base64Utils.encode("app:very_secret".getBytes()));
        String contentType = MediaType.APPLICATION_JSON + ";charset=UTF-8";

        String body = mockMvc
                .perform(
                        post("/api/sys/account/login")
//                                .header("Authorization", authorization)
                                .contentType(
                                        MediaType.APPLICATION_FORM_URLENCODED)
                                .param("username", username)
                                .param("password", password)
                                )
                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.success", Matchers.is("true")))
                .andReturn().getResponse().getContentAsString();
        Result result = JsonUtils.getMapper().readValue(body, Result.class);

        return result.getData().toString();
    }

    @BeforeClass
    public static void setup() {
        mapper.setVisibility(mapper.getSerializationConfig().getDefaultVisibilityChecker()
                .withFieldVisibility(JsonAutoDetect.Visibility.ANY));
        javadocReader = new DefaultJavadocReader();
        constraintReader = ConstraintReaderImpl.create(mapper);

        operationBuilder = new OperationBuilder(asciidoctor());

        operationBuilder.apply(null, new File("target/generated-snippets"), "{ClassName}");
    }


    @Test
    public void afterClass() {

        System.out.println("---------" + BaseTest.class.getSimpleName() + "--------");

        Operation operation = operationBuilder
//                    .attribute(HandlerMethod.class.getName(), createHandlerMethod())
                .attribute(ObjectMapper.class.getName(), mapper)
                .attribute(JavadocReader.class.getName(), javadocReader)
                .attribute(ConstraintReader.class.getName(), constraintReader)
                .build();

        ManualRestDocumentation manualRestDocumentation = new ManualRestDocumentation();
        manualRestDocumentation.beforeTest(this.getClass(), "toString");
        operation.getAttributes().put(RestDocumentationContext.class.getName(), manualRestDocumentation.beforeOperation());


        try {
            operation.getAttributes().put(HandlerMethod.class.getName(), createHandlerMethod("toString"));
            new ClassSectionSnippet().document(operation);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected abstract HandlerMethod createHandlerMethod(String methodName) throws NoSuchMethodException;


}
