package com.runfast.common.restdoc.snippet;

import capital.scalable.restdocs.payload.JacksonResponseFieldSnippet;
import org.springframework.core.MethodParameter;
import org.springframework.web.method.HandlerMethod;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class FirstGenericJacksonResponseFieldSnippet extends JacksonResponseFieldSnippet {
    @Override
    protected Type getType(HandlerMethod method) {
        MethodParameter returnType = method.getReturnType();

        Type genericParameterType = returnType.getGenericParameterType();

        if (genericParameterType instanceof ParameterizedType) {
            return firstGenericType(returnType);
        }
        return Object.class;
    }
}
