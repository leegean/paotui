package com.runfast.paotui.restdocs;

import com.runfast.common.restdoc.BaseTest;
import com.runfast.paotui.dao.model.Order;
import com.runfast.paotui.web.controller.user.OrderController;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.method.HandlerMethod;

import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author: lijin

 */
public class AccountTest extends BaseTest {
    @Override
    protected HandlerMethod createHandlerMethod(String methodName) throws NoSuchMethodException {
        return new HandlerMethod(new OrderController(),methodName);
    }


    @Test
    public void login() throws Exception {
        String body = getAccessToken("15871713751", "e10adc3949ba59abbe56e057f20f883e");
        System.out.println("++++++++"+body+"+++++++");
    }

    @Test
    public void logout() throws Exception {
        String body = mockMvc
                .perform(
                        MockMvcRequestBuilders.post("/api/sys/account/logout")
                                .header("token", "")
                                .contentType(
                                        MediaType.APPLICATION_FORM_URLENCODED)
                )
                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.success", Matchers.is("true")))
                .andReturn().getResponse().getContentAsString();

    }

}
