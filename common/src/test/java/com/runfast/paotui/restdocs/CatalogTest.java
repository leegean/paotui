package com.runfast.paotui.restdocs;

import com.runfast.common.restdoc.BaseTest;
import com.runfast.paotui.dao.model.Order;
import com.runfast.paotui.web.controller.user.CatalogController;
import com.runfast.paotui.web.controller.user.OrderController;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.web.method.HandlerMethod;

import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author: lijin

 */
@Slf4j
public class CatalogTest extends BaseTest {
    @Override
    protected HandlerMethod createHandlerMethod(String methodName) throws NoSuchMethodException {
        return new HandlerMethod(new CatalogController(),methodName);
    }


    @Test
    public void list() throws Exception {
        this.mockMvc.perform(post("/api/user/catalog/list").with(userToken())
                .contentType(MediaType.APPLICATION_JSON_UTF8).param("page","0").param("size","5")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void add() throws Exception {


        String content = this.mockMvc.perform(post("/api/user/catalog/add").with(userToken())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .param("name", "新鲜的水果")
                .param("sugestions", "苹果,香蕉,榴莲")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        System.out.println(content);
    }



}
