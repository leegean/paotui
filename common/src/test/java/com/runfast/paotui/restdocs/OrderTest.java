package com.runfast.paotui.restdocs;

import com.runfast.common.restdoc.BaseTest;
import com.runfast.paotui.dao.model.Order;
import com.runfast.paotui.web.controller.user.OrderController;
import lombok.extern.slf4j.Slf4j;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.method.HandlerMethod;

import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author: lijin
 * @date: 2018年04月24日
 */
@Slf4j
public class OrderTest extends BaseTest {
    @Override
    protected HandlerMethod createHandlerMethod(String methodName) throws NoSuchMethodException {
        return new HandlerMethod(new OrderController(),methodName);
    }


    @Test
    public void list() throws Exception {
        this.mockMvc.perform(post("/api/user/order/list").with(userToken())
                .contentType(MediaType.APPLICATION_JSON_UTF8).param("page","0").param("size","2")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void confirm() throws Exception {

        Order order = new Order();
//        order.withGoodsDescription("买一斤香蕉")
//                .withType(Order.Type.DAIGOU)
//                .withFromType(Order.FromType.NEARBY)
//                .withToId(145307);

        String content = this.mockMvc.perform(post("/api/user/order/confirm").with(userToken())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .param("type", "DAIGOU")
                .param("fromType", "NEARBY")
                .param("toId", "145307")
                .param("goodsDescription", "买一斤香蕉")
                .param("userLng","114.31385")
                .param("userLat","30.545481")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        System.out.println(content);
    }


    @Test
    public void cancel() throws Exception {


        String content = this.mockMvc.perform(post("/api/user/order/cancel").with(userToken())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .param("orderId", "2")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        log.warn("测试响应内容："+content);
    }

    @Test
    public void fillIn() throws Exception {

        Order order = new Order();
//        order.withGoodsDescription("买一斤香蕉")
//                .withType(Order.Type.DAIGOU)
//                .withFromType(Order.FromType.NEARBY)
//                .withToId(145307);

        String content = this.mockMvc.perform(post("/api/user/order/fillIn").with(userToken())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .param("type", "DAIGOU")
                .param("fromType", "NEARBY")
                .param("toId", "145307")
                .param("userLng","114.31385")
                .param("userLat","30.545481")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        log.warn("测试响应内容："+content);
    }

}
