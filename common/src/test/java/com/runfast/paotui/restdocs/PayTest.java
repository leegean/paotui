package com.runfast.paotui.restdocs;

import com.runfast.common.restdoc.BaseTest;
import com.runfast.paotui.web.controller.user.CatalogController;
import com.runfast.paotui.web.controller.user.PayController;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.web.method.HandlerMethod;

import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author: lijin
 */
@Slf4j
public class PayTest extends BaseTest {
    @Override
    protected HandlerMethod createHandlerMethod(String methodName) throws NoSuchMethodException {
        return new HandlerMethod(new PayController(),methodName);
    }



    @Test
    public void prepay() throws Exception {


        String content = this.mockMvc.perform(post("/api/user/pay/prepay").with(userToken())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .param("channel", "WXPAY_APP")
                .param("orderNo", "p20180425000031")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        System.out.println(content);
    }

    @Test
    public void orderQuery() throws Exception {


        String content = this.mockMvc.perform(post("/api/user/pay/orderQuery").with(userToken())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .param("orderNo", "p20180425000031")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        System.out.println(content);
    }



}
