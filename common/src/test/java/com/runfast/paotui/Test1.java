package com.runfast.paotui;

import cn.jpush.api.push.PushResult;
import com.runfast.common.utils.DateUtils;
import com.runfast.paotui.dao.model.Role;
import com.runfast.paotui.utils.JiGuangPushUtil;
import com.runfast.waimai.dao.mapper.*;
import com.runfast.waimai.dao.model.RunfastActivity;
import com.runfast.waimai.entity.CartItem;
import com.runfast.waimai.entity.CartItemKey;
import com.runfast.waimai.entity.DeliveryRange;
import com.runfast.waimai.entity.OptionIdPair;
import com.runfast.waimai.web.dto.BusinessDto;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.core.serializer.DefaultDeserializer;
import org.springframework.core.serializer.DefaultSerializer;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalTime;
import java.util.*;

/**
 * @author: lijin
 * @date: 2018年05月08日
 */
public class Test1 {

    private static SqlSessionFactory sqlSessionFactory;

    @BeforeClass
    public static void beforeClass(){
        String resource = "mybatis-config.xml";
        InputStream inputStream = null;
        try {
            inputStream = Resources.getResourceAsStream(resource);
        } catch (IOException e) {
            e.printStackTrace();
        }
        sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
    }

    @Test
    public void t1() throws IOException {
        String s1 = null;
        String s = (String) s1;
        HashMap<String, Object> map = new HashMap<>();
        map.values();
        DefaultSerializer defaultSerializer = new DefaultSerializer();

        defaultSerializer.serialize(map, System.out);
    }

    /*@Test
    public void t2()  {

        SqlSession sqlSession = sqlSessionFactory.openSession();
        RunfastActivityMapper mapper = sqlSession.getMapper(RunfastActivityMapper.class);
        RunfastActivity runfastActivity = mapper.selectByPrimaryKey(34);
//
        RunfastBusinessMapper businessMapper = sqlSession.getMapper(RunfastBusinessMapper.class);
        RunfastGoodsSellMapper goodsSellMapper = sqlSession.getMapper(RunfastGoodsSellMapper.class);

//        List<BusinessDto> businessDtoList = businessMapper.nearBy(39, null, 114.31385, 30.545481, deliveryStart1, deliveryEnd1, deliveryStart2, deliveryEnd2, null, null, null, PageRequest.of(0, 10));

        BusinessDto detail = businessMapper.detailWithDistance(39, 114.31385, 30.545481);*//*
        List<BusinessDto> search = businessMapper.search("肉片", 39, 114.31385, 30.545481, PageRequest.of(0, 10));
*//*
        List<Integer> activityTypeList = new ArrayList<>();
        activityTypeList.add(1);//满减
        activityTypeList.add(5);//免全部

//        List<RunfastActivity> zoneActivityIn = mapper.getAgentZoneActivityIn(39, activityTypeList);
//        RunfastActivity activityWithTarget = mapper.getActivityWithTarget(4, PageRequest.of(0, 10));
//        businessMapper.getAgentZoneBusiness(39, activityTypeList, 114.31385, 30.545481, PageRequest.of(0, 10));

//        List<Map<String, Object>> offZoneGoods = goodsSellMapper.findAgentOffZoneGoods(39, PageRequest.of(0, 10));

//        List<RunfastGoodsSell> goodsSells = goodsSellMapper.search("肉", 4156, PageRequest.of(0, 10));

        List<Map<String, Object>> agentOffZoneGoods = goodsSellMapper.findAgentOffZoneGoods(39, PageRequest.of(0, 10));

        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(1);
        arrayList.add(5);
        arrayList.add(7);
//        List<RunfastActivity> agentZoneActivityIn = mapper.getAgentZoneActivityIn(39, arrayList);
//        RunfastGoodsSell detailWithDistance = goodsSellMapper.detailWithDistance(197632);
//        RunfastActivity runfastActivity = (RunfastActivity) sqlSession.selectOne("com.runfast.waimai.dao.test.mapper.RunfastActivityMapper.selectByPrimaryKey", 23);
//        System.out.println(runfastActivity);
        System.out.println("===");
    }


    @Test
    public void t3()  {

        SqlSession sqlSession = sqlSessionFactory.openSession();
//        BsTravelAdvertMapper advertMapper = sqlSession.getMapper(BsTravelAdvertMapper.class);

        RunfastGoodsSellOptionMapper optionMapper = sqlSession.getMapper(RunfastGoodsSellOptionMapper.class);
//        List<RunfastGoodsSellOption> optionWithSub = optionMapper.findOptionWithSub(1255);
        System.out.println("===");
    }

    @Test
    public void businessDetail()  {

        SqlSession sqlSession = sqlSessionFactory.openSession();

        RunfastBusinessMapper mapper = sqlSession.getMapper(RunfastBusinessMapper.class);
        BusinessDto detail = mapper.detailWithDistance(4166, 114.31385, 30.545481);
        System.out.println("===");
    }

    @Test
    public void goodsDetail()  {

        SqlSession sqlSession = sqlSessionFactory.openSession();

        RunfastGoodsSellMapper mapper = sqlSession.getMapper(RunfastGoodsSellMapper.class);
//        RunfastGoodsSell detail = mapper.detail(197624);
        System.out.println("===");
    }


    @Test
    public void nearBy()  {

        SqlSession sqlSession = sqlSessionFactory.openSession();

        RunfastBusinessMapper mapper = sqlSession.getMapper(RunfastBusinessMapper.class);
//        List<BusinessDto> detail = mapper.nearBy(39, null, 114.31385, 30.545481, deliveryStart1, deliveryEnd1, deliveryStart2, deliveryEnd2, null, null, null, PageRequest.of(0, 10));
        System.out.println("===");
    }

    @Test
    public void getAgentZoneActivityIn(){
        List<Integer> activityTypeList = new ArrayList<>();
        activityTypeList.add(1);//满减
        activityTypeList.add(5);//免全部

        SqlSession sqlSession = sqlSessionFactory.openSession();
        RunfastActivityMapper mapper = sqlSession.getMapper(RunfastActivityMapper.class);
//        List<RunfastActivity> zoneActivityIn = mapper.getAgentZoneActivityIn(39, activityTypeList);
    }

    @Test
    public void checkGoodsWithOption(){
        SqlSession sqlSession = sqlSessionFactory.openSession();
        RunfastGoodsSellMapper mapper = sqlSession.getMapper(RunfastGoodsSellMapper.class);

        List<CartItem> cartItemList = new ArrayList<>();
        CartItem cartItem = new CartItem();

        CartItemKey cartItemKey = new CartItemKey();
        cartItemKey.setGoodsId(197580);
//        cartItemKey.setStandarId();
        List<OptionIdPair> pairList = new ArrayList<>();
        OptionIdPair optionIdPair = new OptionIdPair();
        optionIdPair.setOptionId(79143);
        optionIdPair.setSubOptionId(147589);
//        pairList.add(optionIdPair);

        cartItemKey.setOptionIdPairList(pairList);
        cartItem.setKey(cartItemKey);

        cartItemList.add(cartItem);
//        List<Map<String, Object>> goodsSellList = mapper.checkGoodsWithOption(0, cartItemList);
    }


    @Test
    public void checkGoodsWithStandar(){
        SqlSession sqlSession = sqlSessionFactory.openSession();
        RunfastGoodsSellMapper mapper = sqlSession.getMapper(RunfastGoodsSellMapper.class);

        List<CartItem> cartItemList = new ArrayList<>();
        CartItem cartItem = new CartItem();

        CartItemKey cartItemKey = new CartItemKey();
        cartItemKey.setGoodsId(197639);
        cartItemKey.setStandarId(380452);

        List<OptionIdPair> pairList = new ArrayList<>();
        OptionIdPair optionIdPair = new OptionIdPair();
        optionIdPair.setOptionId(79143);
        optionIdPair.setSubOptionId(147589);
//        pairList.add(optionIdPair);

        cartItemKey.setOptionIdPairList(pairList);
        cartItem.setKey(cartItemKey);

        cartItemList.add(cartItem);
//        List<Map<String, Object>> goodsSellList = mapper.checkGoodsWithStandar(4170, cartItemList);
    }

*/

    @Test
    public void t2(){
        BigDecimal plateformCoefficient = BigDecimal.valueOf(3);

        BigDecimal parentCoefficient = BigDecimal.valueOf(2);

        BigDecimal leafCoefficient = BigDecimal.valueOf(20);

        if(plateformCoefficient.compareTo(parentCoefficient)==1) parentCoefficient = plateformCoefficient;
        if(parentCoefficient.compareTo(leafCoefficient)==1) leafCoefficient = parentCoefficient;



        BigDecimal baseFee = BigDecimal.valueOf(20);

        BigDecimal totalIncome = baseFee.multiply(leafCoefficient).divide(BigDecimal.valueOf(100),6,RoundingMode.FLOOR);

        BigDecimal plateformIncome = baseFee.multiply(plateformCoefficient).divide(BigDecimal.valueOf(100),6,RoundingMode.FLOOR);

        BigDecimal firstIncome = baseFee.multiply(parentCoefficient.subtract(plateformCoefficient)).divide(BigDecimal.valueOf(100),6,RoundingMode.FLOOR);


        BigDecimal secondIncome = baseFee.multiply(leafCoefficient.subtract(parentCoefficient)).divide(BigDecimal.valueOf(100),6,RoundingMode.FLOOR);



        System.out.println(totalIncome);
        System.out.println(plateformIncome );
        System.out.println(firstIncome );
        System.out.println(secondIncome );
    }


    @Test
    public void t3(){
        BigDecimal plateformCoefficient = BigDecimal.valueOf(3);


        BigDecimal leafCoefficient = BigDecimal.valueOf(20);

        if(plateformCoefficient.compareTo(leafCoefficient)==1) leafCoefficient = plateformCoefficient;


        BigDecimal totalPay = BigDecimal.valueOf(1);

        BigDecimal baseFee = BigDecimal.valueOf(20);

        BigDecimal totalIncome = baseFee.multiply(leafCoefficient).divide(BigDecimal.valueOf(100),6,RoundingMode.FLOOR);

        BigDecimal plateformIncome = baseFee.multiply(plateformCoefficient).divide(BigDecimal.valueOf(100),6,RoundingMode.FLOOR);



        BigDecimal secondIncome = baseFee.multiply(leafCoefficient.subtract(plateformCoefficient)).divide(BigDecimal.valueOf(100),6,RoundingMode.FLOOR);



        System.out.println(totalIncome);
        System.out.println(plateformIncome );
        System.out.println(secondIncome );

        BigDecimal subsidy = BigDecimal.valueOf(10);

        System.out.println("平台"+plateformIncome);
        System.out.println("代理商"+secondIncome.subtract(subsidy));
        System.out.println("商家"+totalPay.subtract(totalIncome).add(subsidy));

    }

    @Test
    public void t4(){

        String weekday = "1,2,3,4";

        Date businessStart1 = new Date(218,7,12,9,1,1);
        Date businessEnd1 = new Date(218,7,12,19,1,1);
        Date businessStart2 = new Date(218,7,12,20,1,1);
        Date businessEnd2 = new Date(218,7,12,22,1,1);

        Date deliveryStart1 = new Date(218,7,12,12,1,1);
        Date deliveryEnd1 = new Date(218,7,12,15,1,1);
        Date deliveryStart2 = new Date(218,7,12,17,1,1);
        Date deliveryEnd2 = new Date(218,7,12,23,1,1);

        Integer deliver = 1;
        boolean isDeliver = deliver != null && deliver == 1;



        Integer status = 0;//后台设置商家营业状态
        Integer statu = 0;//商家设置营业状态
        List<DeliveryRange> deliveryRanges = DateUtils.getDeliveryRange(weekday,
                businessStart1, businessEnd1,
                businessStart2, businessEnd2,
                deliveryStart1, deliveryEnd1,
                deliveryStart2, deliveryEnd2,
                isDeliver, status, statu, BigDecimal.valueOf(2), BigDecimal.valueOf(4));

        Date now = new Date();
        LocalTime time = DateUtils.localTime(now);
        if(deliveryRanges!=null)
        for (DeliveryRange deliveryRange : deliveryRanges) {
            System.out.println(deliveryRange);
        }

    }

    @Test
    public void t5(){

        String orderNo = "9999999999";
        HashMap<String, String> params = new HashMap<>();
        params.put("orderNo", orderNo);
        String title = "外卖订单提醒";
        String alert = "您有新的外卖订单" + orderNo;
        String sound = "neworder.caf";

        List<String> aliasList = new ArrayList<>();
        aliasList.add("1a0018970a87809feb3");
//        aliasList.add("1a0018970a87f26f037");

        PushResult pushResult = JiGuangPushUtil.push(Role.Predefine.CUSTOMER, 600, aliasList, alert,title, params, sound);
//        System.out.println(pushResult);
    }

    @Test
    public void t6(){

        String orderNo = "9999999999";
        HashMap<String, String> params = new HashMap<>();
        params.put("orderNo", orderNo);
        String title = "外卖订单提醒";
        String alert = "您有新的外卖订单" + orderNo;
        String sound = "neworder.caf";

        List<String> aliasList = new ArrayList<>();
        aliasList.add("1a0018970a87809feb3");
//        aliasList.add("1a0018970a87f26f037");

        PushResult pushResult = JiGuangPushUtil.push(Role.Predefine.CUSTOMER, 600, aliasList, alert,title, params, sound);
//        System.out.println(pushResult);
    }

}
