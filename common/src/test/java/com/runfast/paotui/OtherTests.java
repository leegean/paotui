package com.runfast.paotui;

import com.alipay.api.AlipayApiException;
import com.alipay.api.internal.util.json.JSONWriter;
import com.alipay.api.response.AlipayTradeAppPayResponse;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.gxuc.runfast.bean.Cuser;
import com.runfast.common.Constants;
import com.runfast.common.dao.IMapper;
import com.runfast.common.utils.JsonUtils;
import com.runfast.paotui.dao.model.Order;
import com.runfast.common.dao.model.RunfastCuser;
import com.runfast.paotui.entity.BicyclingResponse;
import com.runfast.paotui.entity.Region;
import com.runfast.paotui.utils.AmapUtil;
import com.runfast.pay.service.impl.AlipayServiceImpl;
import com.runfast.waimai.entity.CartItem;
import com.runfast.waimai.entity.CartItemKey;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.crypto.cipher.CryptoCipher;
import org.apache.commons.crypto.cipher.CryptoCipherFactory;
import org.apache.commons.crypto.utils.Utils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.CharUtils;
import org.junit.Test;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.*;
import java.util.Date;

public class OtherTests {
    @Test
    public void t1() {
        Calendar startTime = Calendar.getInstance();
        startTime.set(Calendar.HOUR_OF_DAY, 0);
        startTime.set(Calendar.MINUTE, 0);
        startTime.set(Calendar.SECOND, 0);
        startTime.set(Calendar.MILLISECOND, 0);

//        System.out.println(startTime.getTime());
        Date start = startTime.getTime();
        startTime.add(Calendar.DAY_OF_MONTH, 1);
        Date end = startTime.getTime();
        System.out.println(start + "   " + end);

        for (Method method : IMapper.class.getMethods()) {
            for (Parameter parameter : method.getParameters()) {

                System.out.println(parameter.getName());
            }
            ;
        }
        ;

    }

    @Test
    public void t2() throws IOException, InterruptedException {

        String left = "阿斯（）顿法sdfasdf()国aa";
        String right = "   x2 10.5";
        char paddingChar = '-';
        int maxCharSize = 32;
        StringBuffer line = leftRightAlign(left, right, paddingChar, maxCharSize);

        System.out.println(line.toString());
        System.out.println(line.length());

    }

    private StringBuffer leftRightAlign(String left, String right, char paddingChar, int maxCharSize) {
        StringBuffer line = new StringBuffer();
        int size = 0;
        for (char c : left.toCharArray()) {
            boolean ascii = CharUtils.isAscii(c);
            if (ascii) size += 1;
            else size += 2;
        }
        size += right.length();
        line.append(left);

        int ceil = (int) Math.ceil(size / (float) maxCharSize);

        System.out.println(ceil);
        int paddingNum = maxCharSize * ceil - size;

        for (int i = 0; i < paddingNum; i++) {
            line.append(paddingChar);
        }
        line.append(right);
        return line;
    }

    @Test
    public void t3() throws IOException, InterruptedException {

        JWTCreator.Builder builder = JWT.create();
        String token = null;
        try {

            builder.withIssuer(Constants.ISSUER).withClaim(Constants.USER_ID, 123456);
            token = builder.sign(Algorithm.HMAC256(Constants.SCRETE));

            System.out.println("token: " + token);


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if (token != null) {

            JWTVerifier.Verification require = JWT.require(Algorithm.HMAC256(Constants.SCRETE));
            JWTVerifier build = require.build();
            DecodedJWT verify = build.verify(token);

            JWT decode = JWT.decode(token);
            Claim claim = decode.getClaim(Constants.USER_ID);
            Integer userId = claim.asInt();
            System.out.println(userId);

        }

    }

    @Test
    public void t4() {

        List<String> locaions = AmapUtil.getLocaions("武汉市武铁佳苑东区", false);
        for (String locaion : locaions) {

            System.out.println(locaion);
        }

    }

    @Test
    public void t5() {

        List<Region> regins = AmapUtil.getRegins("114.354849,30.589971|114.354849,30.589971", false);

        for (Region regin : regins) {
            System.out.println(regin.toString());
        }
    }

    @Test
    public void t6() throws JsonProcessingException {

        BicyclingResponse bicycling = AmapUtil.bicycling("114.397308,30.50572", "114.34431926971678,30.662688054431047");
        System.out.println(JsonUtils.getMapper().writeValueAsString(bicycling));
    }

    @Test
    public void t7() throws Exception {
        final SecretKeySpec key = new SecretKeySpec(getUTF8Bytes("1234567890123456"), "AES");
        final IvParameterSpec iv = new IvParameterSpec(getUTF8Bytes("1234567890123456"));
        Properties properties = new Properties();
        properties.setProperty(CryptoCipherFactory.CLASSES_KEY, CryptoCipherFactory.CipherProvider.JCE.getClassName());
        //Creates a CryptoCipher instance with the transformation and properties.
        final String transform = "AES/CBC/PKCS5Padding";
        CryptoCipher encipher = Utils.getCipherInstance(transform, properties);
        System.out.println("Cipher:  " + encipher.getClass().getCanonicalName());
        final String sampleInput = "hello world!";
        System.out.println("input:  " + sampleInput);
        byte[] input = getUTF8Bytes(sampleInput);
        byte[] output = new byte[32];
        //Initializes the cipher with ENCRYPT_MODE, key and iv.
        encipher.init(Cipher.ENCRYPT_MODE, key, iv);
        //Continues a multiple-part encryption/decryption operation for byte array.
        int updateBytes = encipher.update(input, 0, input.length, output, 0);
        updateBytes = 0;
        System.out.println(updateBytes);
        //We must call doFinal at the end of encryption/decryption.
        int finalBytes = encipher.doFinal(input, 0, 0, output, updateBytes);
        System.out.println(finalBytes);
        //Closes the cipher.
        encipher.close();
        System.out.println(Arrays.toString(Arrays.copyOf(output, updateBytes + finalBytes)));
        // Now reverse the process using a different implementation with the same settings
        properties.setProperty(CryptoCipherFactory.CLASSES_KEY, CryptoCipherFactory.CipherProvider.JCE.getClassName());
        CryptoCipher decipher = Utils.getCipherInstance(transform, properties);
        System.out.println("Cipher:  " + encipher.getClass().getCanonicalName());
        decipher.init(Cipher.DECRYPT_MODE, key, iv);
        byte[] decoded = new byte[32];
        int i = decipher.doFinal(output, 0, updateBytes + finalBytes, decoded, 0);
        System.out.println("output: " + new String(decoded, StandardCharsets.UTF_8));
        System.out.println("output: " + new String(Arrays.copyOf(decoded, updateBytes + i), StandardCharsets.UTF_8));
    }

    private static byte[] getUTF8Bytes(String input) {
        return input.getBytes(StandardCharsets.UTF_8);
    }

    @Test
    public void t8() {
        String[] split = ",a,b,c,,,".split(",",-1);
        System.out.println(split.length );
        for (String s : split) {
            System.out.println(s);
        }
    }

    @Test
    public void t9() {
        for (StackTraceElement stackTraceElement : Thread.currentThread().getStackTrace()) {
            System.out.println("文件名：" + stackTraceElement.getFileName() + " 类和方法名：" + stackTraceElement.getClassName() + "." + stackTraceElement.getMethodName() + " 行号：" + stackTraceElement.getLineNumber());
        }
        ;
    }

    @Test
    public void t10() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");

        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/runfast", "root", "yxt-movee-8114");
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT *FROM runfast_cuser LIMIT 10");
        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            System.out.println(resultSet.getObject("remainder").getClass());
        }
    }
    
    @Test
    public void t11() throws InvocationTargetException, IllegalAccessException, NoSuchMethodException, JsonProcessingException {
        Cuser cuser = new Cuser();
        RunfastCuser runfastCuser = new RunfastCuser();
//        BeanUtils.copyProperties(cuser,runfastCuser);
//        BeanUtilsBean2.getInstance().copyProperties(cuser,runfastCuser);

        PropertyUtils.copyProperties(cuser,runfastCuser);
        Order order = new Order();
//        order.withGoodsDescription("买一斤香蕉")
//                .withType(Order.Type.DAIGOU)
//                .withFromType(Order.FromType.NEARBY)
//                .withToId(145307);
        System.out.println(JsonUtils.getMapper().writeValueAsString(order));
    }

    @Test
    public void t12() throws AlipayApiException {
//        AlipayServiceImpl alipayService = new AlipayServiceImpl();
//        String prepay = alipayService.prepay("p20180505014025", 1, "qwerqwer", "QUICK_MSECURITY_PAY", "1d", "http");
//        String content = prepay.substring(0, prepay.indexOf("sign=") - 1);
//        String sign = prepay.substring(prepay.indexOf("sign=") + 5);
//        System.out.println(content+  "   "+ sign);
////        HashMap<String, String> map = new HashMap<>();
////        map.put("name", "leegean");
////        String signContent = AlipaySignature.getSignContent(map);
////        String sign = AlipaySignature.rsaSign(content, AlipayConfig.private_key, AlipayConfig.input_charset, AlipayConfig.sign_type);
////        String content = "name=leegean&sign_type=RSA";
////        String sign = "BeHtnrbpf5yaoHR60fZ6/sgja0XxfxPo/y04lrhMUKduLG37QP/jNcpHJ1wD26Dpg44XXgkWNajJJUSXx9cMO/BpiW6lPdY+GZLv+1iP0x0mRfiqTqZVoFqR+5eEMp9U8TTyrZvS4LU5yxnR/f0ttM7qtKss1dLf/GseQ+kTzzE=";
//        boolean b = AlipaySignature.rsaCheck(content, sign, AlipayConfig.ali_public_key, AlipayConfig.input_charset, AlipayConfig.sign_type);
//        if(b){
//            System.out.println("success");
//        }

    }

    @Test
    public void t13(){
        AlipayServiceImpl alipayService = new AlipayServiceImpl();
        AlipayTradeAppPayResponse alipayTradeAppPayResponse = alipayService.realPay("p20180505014045", 1, "垃圾", "QUICK_MSECURITY_PAY", "1d", "http://www.gxptkc.com");
        System.out.println(new JSONWriter().write(alipayTradeAppPayResponse));
    }

    @Test
    public void t14(){

        CartItemKey cartItemKey = new CartItemKey();
        Map<CartItemKey, Integer> cartMap = new HashMap<>();

        Integer numBuy = cartMap.get(cartItemKey);
        if(numBuy==null){
            numBuy = 1;
            cartMap.put(cartItemKey,numBuy);

        }
        else numBuy++;

        for (Map.Entry<CartItemKey, Integer> entry : cartMap.entrySet()) {
            System.out.println(entry.getKey()+"  "+entry.getValue());
        }
    }
    @Test
    public void t15(){
        int a = 1;
        Double b = 1/3.0;
        double c = 2.56;
        double r = c * 2 +c;
        Double g = null;
        HashMap map = new HashMap();
        CartItem cartItem = new CartItem();
        cartItem.setNum(1);
        map.put("item", cartItem);
        CartItem object1 = (CartItem) map.get("item");


        System.out.println(BigDecimal.valueOf(2).subtract(BigDecimal.valueOf(1.1)));
    }

    @Test
    public void t16(){
        ArrayList<Object> objects = new ArrayList<>();
        while (true){
            objects.add(new Object());
        }

    }
}
