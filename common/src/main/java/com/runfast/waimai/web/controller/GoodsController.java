package com.runfast.waimai.web.controller;

import com.runfast.common.web.entity.Result;
import com.runfast.waimai.dao.model.RunfastGoodsSell;
import com.runfast.waimai.dao.model.RunfastGoodsSellOption;
import com.runfast.waimai.dao.model.RunfastGoodsSellType;
import com.runfast.waimai.service.RunfastGoodsSellOptionService;
import com.runfast.waimai.service.RunfastGoodsSellService;
import com.runfast.waimai.service.RunfastGoodsSellStandardService;
import com.runfast.waimai.service.RunfastGoodsSellTypeService;
import com.runfast.waimai.web.dto.GoodsStandarDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * 商品接口
 */
@RestController("userWmGoodsController")
@RequestMapping(value = "/api/user/wm/goods")
public class GoodsController {


    @Autowired
    private RunfastGoodsSellService goodsSellService;

    @Autowired
    private RunfastGoodsSellStandardService standardService;



    @Autowired
    private RunfastGoodsSellTypeService goodsSellTypeService;


    @PostMapping("/detail")
    public Result<RunfastGoodsSell> detail(@RequestParam int goodsId) {


        RunfastGoodsSell goodsSell = goodsSellService.detail(goodsId);
        return Result.ok("", goodsSell);
    }



    /**
     * 搜索商家店内商品
     *
     * @param businessId 商家id
     * @param pageable
     * @return
     */
    @PostMapping("/search")
    public Result<List<RunfastGoodsSell>> search(@RequestParam int businessId, @RequestParam String name, @PageableDefault Pageable pageable) {


        List<RunfastGoodsSell> search = goodsSellService.search(name, businessId, pageable);

        return Result.ok("", search);
    }


    /**
     * 获取商家可用商品分类及分类下的商品信息
     *
     * @param businessId 商家id
     * @return
     */
    @PostMapping("/catalogs")
    public Result<List<RunfastGoodsSellType>> list(@RequestParam int businessId) {


        List<RunfastGoodsSellType> goodsSellTypeList = goodsSellTypeService.listWithGoods(businessId);


        return Result.ok("", goodsSellTypeList);
    }
}
