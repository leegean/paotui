package com.runfast.waimai.web.dto;

import com.runfast.waimai.dao.model.RunfastBusinessComment;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author: lijin
 * @date: 2018年06月22日
 */

@Data
public class UserBusinessCommentDto implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer id;

    private String name;

    private List<RunfastBusinessComment> commentList;

}
