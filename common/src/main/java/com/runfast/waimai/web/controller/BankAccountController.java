package com.runfast.waimai.web.controller;

import com.runfast.common.dao.model.RunfastCuser;
import com.runfast.common.entity.validate.group.Create;
import com.runfast.common.service.RunfastCuserService;
import com.runfast.common.utils.TokenUtil;
import com.runfast.common.web.entity.Result;
import com.runfast.common.web.entity.ResultCode;
import com.runfast.waimai.dao.model.RunfastBankaccounts;
import com.runfast.waimai.dao.model.RunfastBankaccountsExample;
import com.runfast.waimai.service.RunfastBankaccountsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

/**
 * 用户银行卡账户接口
 */
@RestController("userWmBankAccountController")
@RequestMapping(value= "/api/user/wm/bankAccount", headers = "token")
public class BankAccountController {

    @Autowired
    private RunfastBankaccountsService bankaccountsService;

    @Autowired
    private RunfastCuserService cuserService;
    /**
     * 获取用户银行卡账户列表
     * @param token
     * @return
     */
    @PostMapping("/list")
    public Result<List<RunfastBankaccounts>> list(@RequestHeader String token) {
        Integer userId = TokenUtil.getUserId(token);

        RunfastBankaccountsExample bankaccountsExample = new RunfastBankaccountsExample();
        bankaccountsExample.or().andUserIdEqualTo(userId);
        bankaccountsExample.setOrderByClause("id desc");
        List<RunfastBankaccounts> bankaccountsList = bankaccountsService.selectByExample(bankaccountsExample);

        return Result.ok("",bankaccountsList);
    }

    /**
     * 删除银行卡账户
     * @param id
     * @param token
     * @return
     */
    @PostMapping("/delete")
    public Result delete(@RequestParam int id, @RequestHeader String token) {
        Integer userId = TokenUtil.getUserId(token);

        RunfastBankaccountsExample bankaccountsExample = new RunfastBankaccountsExample();
        bankaccountsExample.or().andUserIdEqualTo(userId).andIdEqualTo(id);
        List<RunfastBankaccounts> bankaccounts = bankaccountsService.selectByExample(bankaccountsExample);
        if(bankaccounts.isEmpty())return Result.fail(ResultCode.FAIL, "当前用户下不存在该银行卡账户");

        bankaccountsService.deleteByPrimaryKey(id);

        return Result.ok("");
    }

    /**
     * 添加银行卡账户
     * @param bankaccounts
     * @param token
     * @return
     */
    @PostMapping("/add")
    public Result add(@Validated(Create.class) @ModelAttribute RunfastBankaccounts bankaccounts, BindingResult bindingResult, @RequestHeader String token) {
        Integer userId = TokenUtil.getUserId(token);
        FieldError fieldError = bindingResult.getFieldError();
        if(fieldError!=null)return Result.fail(ResultCode.PARAMETER_ERROR, fieldError.getField()+" "+ fieldError.getDefaultMessage());

        RunfastCuser cuser = cuserService.selectByPrimaryKey(userId);

        bankaccounts.setId(null);
        bankaccounts.setUserId(userId);
        bankaccounts.setUserName(cuser.getName());
        bankaccounts.setUserMobile(cuser.getMobile());
        bankaccounts.setType(3);
        bankaccounts.setCreateTime(new Date());

        bankaccountsService.insertSelective(bankaccounts);

        return Result.ok("");
    }

}
