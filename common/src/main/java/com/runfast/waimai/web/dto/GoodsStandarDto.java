package com.runfast.waimai.web.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author: lijin
 * @date: 2018年05月09日
 */
@Data
public class GoodsStandarDto implements Serializable {


    private Integer id;

    private String name;

    private Double price;

    private Double packageFee;


    /**
     * 剩余库存
     */
    private Integer num;

    private Integer saleNum;

    private Integer activityId;
    private Integer activityType;

    private Boolean shared;


    private Integer isLimited;

    private Integer limitNum;


    private Integer specialType;

    private String specialName;

    private String specialImg;

    private Integer targetId;

    /**
     * 特价
     */
    private Double disprice;

    private Double discount;
}
