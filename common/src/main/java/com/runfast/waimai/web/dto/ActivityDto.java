package com.runfast.waimai.web.dto;

import com.runfast.waimai.dao.model.RunfastFullLess;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author: lijin
 * @date: 2018年05月07日
 */
@Data
public class ActivityDto{
    private Integer id;

    private Date createTime;


    private String name;

    /**
     * 活动内容 1满减  2打折3赠品4特价5满减免运费6优惠券7免部分配送费8新用户立减活动9首单立减活动10商户红包11下单返红包
     */
    private Integer ptype;

    private Double fulls;

    private Double lesss;

    private String redAmount;

    /**
     * 1：特惠专区；其他：普通活动
     */
    private Integer specialType;

    /**
     * 优选专区活动主题
     */
    private String specialName;

    private Boolean is_limited;

    /**
     * 限购类型（超出后是否允许原价购买0否 1是）
     */
    private Integer limit_type;

    /**
     * 限购数量
     */
    private Integer limit_num;


    private Double disprice;

    private Double discount;

    private List<RunfastFullLess> fullLessList = new ArrayList<>();


}
