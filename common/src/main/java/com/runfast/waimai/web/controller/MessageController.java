package com.runfast.waimai.web.controller;

import com.runfast.common.utils.TokenUtil;
import com.runfast.common.web.entity.Result;
import com.runfast.waimai.dao.model.RunfastMessge;
import com.runfast.waimai.dao.model.RunfastMessgeExample;
import com.runfast.waimai.service.RunfastMessgeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 订单消息接口
 */
@RestController("userWmMessageController")
@RequestMapping(value= "/api/user/wm/message", headers = "token")
public class MessageController {

    @Autowired
    private RunfastMessgeService messgeService;
    /**
     * 获取用户订单消息分页列表
     * @param token
     * @return
     */
    @PostMapping("/list")
    public Result<List<RunfastMessge>> list(@PageableDefault Pageable pageable, @RequestHeader String token) {
        Integer userId = TokenUtil.getUserId(token);


        RunfastMessgeExample messgeExample = new RunfastMessgeExample();
        messgeExample.or().andUserIdEqualTo(userId);
        messgeExample.setOrderByClause("id desc");
        List<RunfastMessge> messgeList = messgeService.selectByExampleWithPageable(messgeExample, pageable);

        return Result.ok("",messgeList);
    }

}
