package com.runfast.waimai.web.controller;

import com.runfast.common.utils.GPSUtil;
import com.runfast.common.utils.TokenUtil;
import com.runfast.common.web.entity.Result;
import com.runfast.common.web.entity.ResultCode;
import com.runfast.waimai.dao.model.RunfastEnshrine;
import com.runfast.waimai.dao.model.RunfastEnshrineExample;
import com.runfast.waimai.service.RunfastBusinessService;
import com.runfast.waimai.service.RunfastEnshrineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 商家活动接口
 */
@RestController("userWmEnshrineController")
@RequestMapping(value= "/api/user/wm/enshrine", headers = "token")
public class EnshrineController {

    @Autowired
    private RunfastEnshrineService enshrineService;

    @Autowired
    private RunfastBusinessService businessService;

    /**
     * 获取用户收藏列表
     * @param token
     * @return
     */
    @PostMapping("/list")
    public Result<List<Object>> list(@RequestParam Double userLng, @RequestParam Double userLat, @PageableDefault Pageable pageable, @RequestHeader String token) {
        /**
         * 高德转百度
         */
        double[] to = GPSUtil.gcj02_To_Bd09(userLat, userLng);
        userLng = to[1];
        userLat = to[0];
        Integer userId = TokenUtil.getUserId(token);

        return enshrineService.list(userId, userLng, userLat,pageable);

    }

    /**
     * 收藏商家
     * @param businessId 商家id
     * @param token
     * @return
     */
    @PostMapping("/add")
    public Result add(@RequestParam int businessId, @RequestHeader String token) {
        Integer userId = TokenUtil.getUserId(token);
        RunfastEnshrineExample enshrineExample = new RunfastEnshrineExample();
        enshrineExample.or().andCidEqualTo(userId).andTypeEqualTo(1).andShopIdEqualTo(businessId);

        List<RunfastEnshrine> enshrineList = enshrineService.selectByExample(enshrineExample);

        if(!enshrineList.isEmpty()) return  Result.ok( "该商家已被收藏");

        RunfastEnshrine enshrine = new RunfastEnshrine();
        enshrine.setCid(userId);
        enshrine.setType(1);
        enshrine.setShopId(businessId);
        enshrine.setCreateTime(new Date());

        enshrineService.insertSelective(enshrine);
        return Result.ok("");
    }


    /**
     * 取消收藏商家
     * @param businessId
     * @param token
     * @return
     */
    @PostMapping("/delete")
    public Result delete(@RequestParam int businessId, @RequestHeader String token) {
        Integer userId = TokenUtil.getUserId(token);

        RunfastEnshrineExample enshrineExample = new RunfastEnshrineExample();
        enshrineExample.or().andCidEqualTo(userId).andTypeEqualTo(1).andShopIdEqualTo(businessId);

        List<RunfastEnshrine> enshrineList = enshrineService.selectByExample(enshrineExample);

        if(enshrineList.isEmpty()) return Result.ok("当前用户下该商家还未收藏");

        RunfastEnshrine enshrine = enshrineList.get(0);

        enshrineService.deleteByPrimaryKey(enshrine.getId());
        return Result.ok("");
    }

}
