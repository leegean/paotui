package com.runfast.waimai.web.dto;

import com.runfast.waimai.dao.model.RunfastBusiness;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author: lijin
 * @date: 2018年05月07日
 */

@Data
public class BusinessDto extends RunfastBusiness implements Serializable {
    private static final long serialVersionUID = 1L;
    private Double distance;


    private List<ActivityDto> activityList = new ArrayList<>();

}
