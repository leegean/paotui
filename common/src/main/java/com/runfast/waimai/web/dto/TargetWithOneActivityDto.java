package com.runfast.waimai.web.dto;

import com.runfast.waimai.dao.model.RunfastActivity;
import com.runfast.waimai.dao.model.RunfastActivityTarget;
import lombok.Data;

/**
 * @author: lijin
 * @date: 2018年05月18日
 */

@Data
public class TargetWithOneActivityDto extends RunfastActivityTarget {

    private RunfastActivity activity;

}
