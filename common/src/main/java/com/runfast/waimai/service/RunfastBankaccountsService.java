package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastBankaccounts;
import com.runfast.waimai.dao.model.RunfastBankaccountsExample;

public interface RunfastBankaccountsService extends IService<RunfastBankaccounts, Integer, RunfastBankaccountsExample> {

    public Integer deleteByPrimaryKey(Integer id);
}