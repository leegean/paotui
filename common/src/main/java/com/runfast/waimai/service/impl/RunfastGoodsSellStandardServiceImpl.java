package com.runfast.waimai.service.impl;

import com.runfast.common.exception.BaseException;
import com.runfast.common.service.BaseService;
import com.runfast.common.web.entity.ResultCode;
import com.runfast.waimai.dao.model.RunfastGoodsSellStandard;
import com.runfast.waimai.dao.model.RunfastGoodsSellStandardExample;
import com.runfast.waimai.service.RunfastGoodsSellStandardService;
import org.springframework.stereotype.Service;

@Service
public class RunfastGoodsSellStandardServiceImpl extends BaseService<RunfastGoodsSellStandard, Integer, RunfastGoodsSellStandardExample> implements RunfastGoodsSellStandardService {
    @Override
    public void updateStock(Integer goodsId, Integer standarId, Integer num) {
        RunfastGoodsSellStandard standard = this.selectByPrimaryKey(standarId);

        Integer standardNum = standard.getNum();
        standardNum = standardNum == null ? 0 : standardNum;

        int i = standardNum + num;
        if(i<0) throw new BaseException(ResultCode.FAIL, "商品库存不足");


        RunfastGoodsSellStandard standardUpdate = new RunfastGoodsSellStandard();
        standardUpdate.setId(standarId);
        int stockNum = standardNum - num;
        standardUpdate.setNum(stockNum<0?0:stockNum);

        this.updateByPrimaryKeySelective(standardUpdate);
    }
}