package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastCreditlevel;
import com.runfast.waimai.dao.model.RunfastCreditlevelExample;

public interface RunfastCreditlevelService extends IService<RunfastCreditlevel, Integer, RunfastCreditlevelExample> {
}