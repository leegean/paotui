package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.BaseUser;
import com.runfast.waimai.dao.model.BaseUserExample;

public interface BaseUserService extends IService<BaseUser, Integer, BaseUserExample> {
}