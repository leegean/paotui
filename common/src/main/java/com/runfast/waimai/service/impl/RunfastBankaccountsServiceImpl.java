package com.runfast.waimai.service.impl;

import com.runfast.common.dao.IMapper;
import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.mapper.RunfastBankaccountsMapper;
import com.runfast.waimai.dao.model.RunfastBankaccounts;
import com.runfast.waimai.dao.model.RunfastBankaccountsExample;
import com.runfast.waimai.service.RunfastBankaccountsService;
import org.springframework.stereotype.Service;

@Service
public class RunfastBankaccountsServiceImpl extends BaseService<RunfastBankaccounts, Integer, RunfastBankaccountsExample> implements RunfastBankaccountsService {
    @Override
    public RunfastBankaccountsMapper getMapper() {
        return (RunfastBankaccountsMapper)super.getMapper();
    }

    @Override
    public Integer deleteByPrimaryKey(Integer id) {
        return getMapper().deleteByPrimaryKey(id);
    }
}