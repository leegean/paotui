package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.RunfastHomepage;
import com.runfast.waimai.dao.model.RunfastHomepageExample;
import com.runfast.waimai.service.RunfastHomepageService;
import org.springframework.stereotype.Service;

@Service
public class RunfastHomepageServiceImpl extends BaseService<RunfastHomepage, Integer, RunfastHomepageExample> implements RunfastHomepageService {
}