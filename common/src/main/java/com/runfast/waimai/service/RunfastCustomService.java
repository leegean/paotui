package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastCustom;
import com.runfast.waimai.dao.model.RunfastCustomExample;

public interface RunfastCustomService extends IService<RunfastCustom, Integer, RunfastCustomExample> {
}