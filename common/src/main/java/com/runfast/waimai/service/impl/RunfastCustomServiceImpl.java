package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.RunfastCustom;
import com.runfast.waimai.dao.model.RunfastCustomExample;
import com.runfast.waimai.service.RunfastCustomService;
import org.springframework.stereotype.Service;

@Service
public class RunfastCustomServiceImpl extends BaseService<RunfastCustom, Integer, RunfastCustomExample> implements RunfastCustomService {
}