package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastAgentMgr;
import com.runfast.waimai.dao.model.RunfastAgentMgrExample;

public interface RunfastAgentMgrService extends IService<RunfastAgentMgr, Integer, RunfastAgentMgrExample> {
}