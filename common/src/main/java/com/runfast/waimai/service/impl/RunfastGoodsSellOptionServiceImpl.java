package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.mapper.RunfastGoodsSellOptionMapper;
import com.runfast.waimai.dao.model.RunfastGoodsSellOption;
import com.runfast.waimai.dao.model.RunfastGoodsSellOptionExample;
import com.runfast.waimai.entity.OptionIdPair;
import com.runfast.waimai.service.RunfastGoodsSellOptionService;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RunfastGoodsSellOptionServiceImpl extends BaseService<RunfastGoodsSellOption, Integer, RunfastGoodsSellOptionExample> implements RunfastGoodsSellOptionService {

    @Override
    public RunfastGoodsSellOptionMapper getMapper() {
        return (RunfastGoodsSellOptionMapper)super.getMapper();
    }

    @Override
    public List<RunfastGoodsSellOption> findOptionWithSub(Integer goodsId) {
        Validate.notNull(goodsId, "goodsId不能为null");
        return getMapper().findOptionWithSub(goodsId);
    }

}