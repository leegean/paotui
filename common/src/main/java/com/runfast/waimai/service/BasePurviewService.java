package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.BasePurview;
import com.runfast.waimai.dao.model.BasePurviewExample;

public interface BasePurviewService extends IService<BasePurview, Integer, BasePurviewExample> {
}