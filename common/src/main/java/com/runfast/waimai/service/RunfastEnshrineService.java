package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.common.web.entity.Result;
import com.runfast.waimai.dao.model.RunfastEnshrine;
import com.runfast.waimai.dao.model.RunfastEnshrineExample;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface RunfastEnshrineService extends IService<RunfastEnshrine, Integer, RunfastEnshrineExample> {
    Result<List<Object>> list(Integer userId, Double userLng, Double userLat, Pageable pageable);

    public Integer deleteByPrimaryKey(Integer id);
}