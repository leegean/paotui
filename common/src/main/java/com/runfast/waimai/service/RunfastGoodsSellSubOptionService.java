package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastGoodsSellSubOption;
import com.runfast.waimai.dao.model.RunfastGoodsSellSubOptionExample;

import java.util.List;

public interface RunfastGoodsSellSubOptionService extends IService<RunfastGoodsSellSubOption, Integer, RunfastGoodsSellSubOptionExample> {
}