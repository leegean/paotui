package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.mapper.RunfastGoodsSellMapper;
import com.runfast.waimai.dao.model.RunfastGoodsSell;
import com.runfast.waimai.dao.model.RunfastGoodsSellExample;
import com.runfast.waimai.dao.model.RunfastGoodsSellOption;
import com.runfast.waimai.entity.GoodsOptionPair;
import com.runfast.waimai.entity.GoodsStandarPair;
import com.runfast.waimai.service.RunfastGoodsSellOptionService;
import com.runfast.waimai.service.RunfastGoodsSellService;
import com.runfast.waimai.web.dto.GoodsStandarDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class RunfastGoodsSellServiceImpl extends BaseService<RunfastGoodsSell, Integer, RunfastGoodsSellExample> implements RunfastGoodsSellService {
    @Autowired
    private RunfastGoodsSellOptionService optionService;

    @Override
    public RunfastGoodsSellMapper getMapper() {
        return (RunfastGoodsSellMapper) super.getMapper();
    }

    @Override
    public List<RunfastGoodsSell> search(String name, Integer businessId, Pageable pageable) {
        List<RunfastGoodsSell> goodsSellList = getMapper().search(name, businessId, pageable);

        for (RunfastGoodsSell goodsSell : goodsSellList) {
            populateGoods(goodsSell);
        }
        return goodsSellList;
    }


    @Override
    public List<Map<String, Object>> findAgentOffZoneGoods(Integer agentId, double userLng, double userLat, Date deliveryStart1, Date deliveryEnd1, Date deliveryStart2, Date deliveryEnd2, Pageable pageable) {

        RunfastGoodsSellMapper mapper = getMapper();
        return mapper.findAgentOffZoneGoods(agentId, userLng, userLat, deliveryStart1,deliveryEnd1,deliveryStart2,deliveryEnd2,pageable);
    }


    @Override
    public List<Map<String, Object>> checkGoodsWithStandar(Integer businessId, Set<GoodsStandarPair> goodsStandarPairSet) {
        return getMapper().checkGoodsWithStandar(businessId, goodsStandarPairSet);
    }

    @Override
    public List<Map<String, Object>> checkGoodsWithOption(Integer businessId, Set<GoodsOptionPair> goodsOptionPairSet) {
        return getMapper().checkGoodsWithOption(businessId, goodsOptionPairSet);
    }

    @Override
    public RunfastGoodsSell detail(int goodsId) {
        RunfastGoodsSell goodsSell = getMapper().detail(goodsId);
        populateGoods(goodsSell);
        return goodsSell;
    }

    @Override
    public void populateGoods(RunfastGoodsSell goodsSell) {
        List<RunfastGoodsSellOption> goodsSellOptions = optionService.findOptionWithSub(goodsSell.getId());

        goodsSell.setGoodsSellOptionList(goodsSellOptions);
        Integer goodsSellNum = goodsSell.getNum();


        Integer goodsSellSalesnum = goodsSell.getSalesnum();


        goodsSellNum = goodsSellNum == null ? 0 : goodsSellNum;
        goodsSellSalesnum = goodsSellSalesnum == null ? 0 : goodsSellSalesnum;


        Integer numCount = 0;
        Integer saleNumCount = 0;
        List<GoodsStandarDto> standardList = goodsSell.getGoodsSellStandardList();
        for (GoodsStandarDto goodsStandarDto : standardList) {
            Integer num = goodsStandarDto.getNum();
            num = num == null ? 0 : num;

            Integer saleNum = goodsStandarDto.getSaleNum();
            saleNum = saleNum == null ? 0 : saleNum;


            goodsSellNum += num;
            goodsSellSalesnum += saleNum;


        }

        goodsSell.setNum(goodsSellNum);
        goodsSell.setSalesnum(goodsSellSalesnum);

       /* if(goodsSellNum==null||goodsSellSalesnum==null){ //同步规格和商品销量库存

        RunfastGoodsSell goodsSell1Update = new RunfastGoodsSell();

        goodsSell1Update.setId(goodsSell.getId());
        goodsSell1Update.setNum(goodsSellNum);
        goodsSell1Update.setSalesnum(goodsSellSalesnum);

        this.updateByPrimaryKeySelective(goodsSell1Update);
        }*/


    }

    @Override
    public Long countOfRequired(Integer businessId) {
        return getMapper().countOfRequired(businessId);
    }
}