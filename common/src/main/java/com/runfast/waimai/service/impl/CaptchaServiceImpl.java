package com.runfast.waimai.service.impl;

import com.runfast.common.security.captcha.NECaptchaVerifier;
import com.runfast.common.security.captcha.NESecretPair;
import com.runfast.waimai.service.CaptchaService;
import org.springframework.stereotype.Service;

/**
 * @author: lijin
 * @date: 2018��03��26��
 */

@Service
public class CaptchaServiceImpl implements CaptchaService {
    private static final String captchaId = "2a05ddcc43e648fd9ad48a08de7dcb11"; // ��֤��id
    private static final String secretId = "e70e74242a102cdce8de346572b1942d"; // ��Կ��id
    private static final String secretKey = "ac5fb24167c6bd060902ea41760322b7"; // ��Կ��key

    private final NECaptchaVerifier verifier = new NECaptchaVerifier(captchaId, new NESecretPair(secretId, secretKey));
    @Override
    public boolean verify(String validate) {
        boolean isValid = verifier.verify(validate, null); // �������У��
        return isValid;
    }
}
