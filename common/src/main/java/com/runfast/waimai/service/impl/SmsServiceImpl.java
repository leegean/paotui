package com.runfast.waimai.service.impl;

import com.runfast.common.dao.model.RunfastCuser;
import com.runfast.common.dao.model.RunfastCuserExample;
import com.runfast.common.exception.BaseException;
import com.runfast.common.security.captcha.NECaptchaVerifier;
import com.runfast.common.service.RunfastCuserService;
import com.runfast.common.utils.SingleSendSms;
import com.runfast.common.web.entity.Result;
import com.runfast.common.web.entity.ResultCode;
import com.runfast.waimai.dao.model.RunfastSmsSendrecord;
import com.runfast.waimai.dao.model.RunfastSmsSendrecordExample;
import com.runfast.waimai.service.CaptchaService;
import com.runfast.waimai.service.RunfastSmsSendrecordService;
import com.runfast.waimai.service.SmsService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.*;

/**
 * @author: lijin
 * @date: 2018年06月07日
 */
@Service
public class SmsServiceImpl implements SmsService {

    @Autowired
    private CaptchaService captchaService;

    @Autowired
    private RunfastSmsSendrecordService smsSendrecordService;

    @Autowired
    private RunfastCuserService cuserService;

    @Override
    public Result sendSms(String validate, String mobile, RunfastSmsSendrecord.SmsType smsType, String remoteAddr) {
        if(StringUtils.isNotBlank(validate)){
            boolean isValid = captchaService.verify(validate);
            if (!isValid) return Result.fail(ResultCode.FAIL, "验证码错误");
        }/*else{
            return Result.fail(ResultCode.FAIL, "请输入验证码");
        }*/

        RunfastSmsSendrecordExample smsSendrecordExample = new RunfastSmsSendrecordExample();
        smsSendrecordExample.or().andPhoneEqualTo(mobile).andSmstypeEqualTo(smsType.getCode());
        smsSendrecordExample.setOrderByClause("id desc");
        List<RunfastSmsSendrecord> smsSendrecordList = smsSendrecordService.selectByExample(smsSendrecordExample);

        /*if(!smsSendrecordList.isEmpty()){
            RunfastSmsSendrecord smsSendrecord = smsSendrecordList.get(0);
            Date sendTime = smsSendrecord.getSendTime();
            if(System.currentTimeMillis() - sendTime.getTime() <= 60000)return  Result.fail(ResultCode.FAIL, "不能频繁发送短信");
        }*/

        Random random = new Random();
        int x = random.nextInt(899999);
        x = x + 100000;


        RunfastSmsSendrecord record = new RunfastSmsSendrecord();
        record.setSmstype(smsType.getCode());
        record.setPhone(mobile);
        record.setIpAddr(remoteAddr);
        record.setMsg("验证码" + x);
        record.setXcode(x + "");
        record.setSendTime(new Date());
        smsSendrecordService.insertSelective(record);

        Result result = SingleSendSms.sendMsg(mobile, "{\"code\":\"" + x + "\"}", "SMS_17020084");

        if(result.isSuccess()){
            RunfastCuserExample cuserExample = new RunfastCuserExample();
            cuserExample.or().andMobileEqualTo(mobile);
            long count = cuserService.countByExample(cuserExample);
            Map<String, Object> map = new HashMap<>();
            map.put("mobileExist", count>0);
            return Result.ok("短信验证码发送成功，请查收",map);
        }else{
            String errorMsg = result.getErrorMsg();
            switch (errorMsg){
                case "13,mobile wrong":
                    errorMsg = "手机号码错误";
                    break;
            }


            throw new BaseException(ResultCode.FAIL, errorMsg); //回滚事务
        }


    }
}
