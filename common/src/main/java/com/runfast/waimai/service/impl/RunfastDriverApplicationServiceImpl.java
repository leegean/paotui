package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.RunfastDriverApplication;
import com.runfast.waimai.dao.model.RunfastDriverApplicationExample;
import com.runfast.waimai.service.RunfastDriverApplicationService;
import org.springframework.stereotype.Service;

@Service
public class RunfastDriverApplicationServiceImpl extends BaseService<RunfastDriverApplication, Integer, RunfastDriverApplicationExample> implements RunfastDriverApplicationService {
}