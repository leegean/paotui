package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.RunfastBusinessBusnameauditing;
import com.runfast.waimai.dao.model.RunfastBusinessBusnameauditingExample;
import com.runfast.waimai.service.RunfastBusinessBusnameauditingService;
import org.springframework.stereotype.Service;

@Service
public class RunfastBusinessBusnameauditingServiceImpl extends BaseService<RunfastBusinessBusnameauditing, Integer, RunfastBusinessBusnameauditingExample> implements RunfastBusinessBusnameauditingService {
}