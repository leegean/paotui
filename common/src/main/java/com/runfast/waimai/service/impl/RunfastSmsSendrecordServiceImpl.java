package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.RunfastSmsSendrecord;
import com.runfast.waimai.dao.model.RunfastSmsSendrecordExample;
import com.runfast.waimai.service.RunfastSmsSendrecordService;
import org.springframework.stereotype.Service;

@Service
public class RunfastSmsSendrecordServiceImpl extends BaseService<RunfastSmsSendrecord, Integer, RunfastSmsSendrecordExample> implements RunfastSmsSendrecordService {
}