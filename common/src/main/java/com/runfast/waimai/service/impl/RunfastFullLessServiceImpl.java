package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.RunfastFullLess;
import com.runfast.waimai.dao.model.RunfastFullLessExample;
import com.runfast.waimai.service.RunfastFullLessService;
import org.springframework.stereotype.Service;

@Service
public class RunfastFullLessServiceImpl extends BaseService<RunfastFullLess, Integer, RunfastFullLessExample> implements RunfastFullLessService {
}