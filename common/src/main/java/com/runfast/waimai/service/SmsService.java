package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.common.web.entity.Result;
import com.runfast.waimai.dao.model.BaseOrg;
import com.runfast.waimai.dao.model.BaseOrgExample;
import com.runfast.waimai.dao.model.RunfastSmsSendrecord;

public interface SmsService{
    Result sendSms(String validate, String mobile, RunfastSmsSendrecord.SmsType smsType, String remoteAddr);
}