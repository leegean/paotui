package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastWithdraworder;
import com.runfast.waimai.dao.model.RunfastWithdraworderExample;

public interface RunfastWithdraworderService extends IService<RunfastWithdraworder, Integer, RunfastWithdraworderExample> {
}