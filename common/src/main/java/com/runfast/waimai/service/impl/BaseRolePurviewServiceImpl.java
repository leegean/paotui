package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.BaseRolePurview;
import com.runfast.waimai.dao.model.BaseRolePurviewExample;
import com.runfast.waimai.service.BaseRolePurviewService;
import org.springframework.stereotype.Service;

@Service
public class BaseRolePurviewServiceImpl extends BaseService<BaseRolePurview, Integer, BaseRolePurviewExample> implements BaseRolePurviewService {
}