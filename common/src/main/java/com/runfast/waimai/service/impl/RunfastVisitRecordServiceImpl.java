package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.RunfastVisitRecord;
import com.runfast.waimai.dao.model.RunfastVisitRecordExample;
import com.runfast.waimai.service.RunfastVisitRecordService;
import org.springframework.stereotype.Service;

@Service
public class RunfastVisitRecordServiceImpl extends BaseService<RunfastVisitRecord, Integer, RunfastVisitRecordExample> implements RunfastVisitRecordService {
}