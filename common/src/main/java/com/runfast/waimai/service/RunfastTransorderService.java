package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastTransorder;
import com.runfast.waimai.dao.model.RunfastTransorderExample;

public interface RunfastTransorderService extends IService<RunfastTransorder, Integer, RunfastTransorderExample> {
}