package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.RunfastGoodsSellOutStatus;
import com.runfast.waimai.dao.model.RunfastGoodsSellOutStatusExample;
import com.runfast.waimai.service.RunfastGoodsSellOutStatusService;
import org.springframework.stereotype.Service;

@Service
public class RunfastGoodsSellOutStatusServiceImpl extends BaseService<RunfastGoodsSellOutStatus, Integer, RunfastGoodsSellOutStatusExample> implements RunfastGoodsSellOutStatusService {
}