package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastScorerecord;
import com.runfast.waimai.dao.model.RunfastScorerecordExample;

public interface RunfastScorerecordService extends IService<RunfastScorerecord, Integer, RunfastScorerecordExample> {
}