package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.RunfastBusinessalliance;
import com.runfast.waimai.dao.model.RunfastBusinessallianceExample;
import com.runfast.waimai.service.RunfastBusinessallianceService;
import org.springframework.stereotype.Service;

@Service
public class RunfastBusinessallianceServiceImpl extends BaseService<RunfastBusinessalliance, Integer, RunfastBusinessallianceExample> implements RunfastBusinessallianceService {
}