package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastDistributionassess;
import com.runfast.waimai.dao.model.RunfastDistributionassessExample;

public interface RunfastDistributionassessService extends IService<RunfastDistributionassess, Integer, RunfastDistributionassessExample> {
}