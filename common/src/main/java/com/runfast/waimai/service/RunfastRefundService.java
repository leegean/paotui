package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastRefund;
import com.runfast.waimai.dao.model.RunfastRefundExample;

public interface RunfastRefundService extends IService<RunfastRefund, Integer, RunfastRefundExample> {
}