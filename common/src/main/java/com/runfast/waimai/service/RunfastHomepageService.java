package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastHomepage;
import com.runfast.waimai.dao.model.RunfastHomepageExample;

public interface RunfastHomepageService extends IService<RunfastHomepage, Integer, RunfastHomepageExample> {
}