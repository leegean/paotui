package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.RunfastDistributionassess;
import com.runfast.waimai.dao.model.RunfastDistributionassessExample;
import com.runfast.waimai.service.RunfastDistributionassessService;
import org.springframework.stereotype.Service;

@Service
public class RunfastDistributionassessServiceImpl extends BaseService<RunfastDistributionassess, Integer, RunfastDistributionassessExample> implements RunfastDistributionassessService {
}