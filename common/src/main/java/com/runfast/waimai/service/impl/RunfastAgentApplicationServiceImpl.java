package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.RunfastAgentApplication;
import com.runfast.waimai.dao.model.RunfastAgentApplicationExample;
import com.runfast.waimai.service.RunfastAgentApplicationService;
import org.springframework.stereotype.Service;

@Service
public class RunfastAgentApplicationServiceImpl extends BaseService<RunfastAgentApplication, Integer, RunfastAgentApplicationExample> implements RunfastAgentApplicationService {
}