package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastAgentMgrHasRunfastAgentbusiness;
import com.runfast.waimai.dao.model.RunfastAgentMgrHasRunfastAgentbusinessExample;

public interface RunfastAgentMgrHasRunfastAgentbusinessService extends IService<RunfastAgentMgrHasRunfastAgentbusiness, Integer, RunfastAgentMgrHasRunfastAgentbusinessExample> {
}