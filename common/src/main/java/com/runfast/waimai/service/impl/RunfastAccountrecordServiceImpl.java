package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.RunfastAccountrecord;
import com.runfast.waimai.dao.model.RunfastAccountrecordExample;
import com.runfast.waimai.service.RunfastAccountrecordService;
import org.springframework.stereotype.Service;

@Service
public class RunfastAccountrecordServiceImpl extends BaseService<RunfastAccountrecord, Integer, RunfastAccountrecordExample> implements RunfastAccountrecordService {
}