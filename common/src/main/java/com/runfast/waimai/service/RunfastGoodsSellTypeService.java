package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastGoodsSellType;
import com.runfast.waimai.dao.model.RunfastGoodsSellTypeExample;

import java.util.List;

public interface RunfastGoodsSellTypeService extends IService<RunfastGoodsSellType, Integer, RunfastGoodsSellTypeExample> {
    List<RunfastGoodsSellType> listWithGoods(int businessId);
}