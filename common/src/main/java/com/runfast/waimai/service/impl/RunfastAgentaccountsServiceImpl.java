package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.RunfastAgentaccounts;
import com.runfast.waimai.dao.model.RunfastAgentaccountsExample;
import com.runfast.waimai.service.RunfastAgentaccountsService;
import org.springframework.stereotype.Service;

@Service
public class RunfastAgentaccountsServiceImpl extends BaseService<RunfastAgentaccounts, Integer, RunfastAgentaccountsExample> implements RunfastAgentaccountsService {
}