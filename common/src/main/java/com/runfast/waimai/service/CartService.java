package com.runfast.waimai.service;

import com.runfast.common.web.entity.Result;
import com.runfast.waimai.entity.Cart;
import com.runfast.waimai.entity.CartItem;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author: lijin
 * @date: 2018年05月17日
 */
public interface CartService {


    Map<String, List<Map<String, Object>>> checkCart(Integer businessId, List<CartItem> cart, boolean check);

    Result add(Integer businessId, List<CartItem> cartItems, Integer userId, String deviceId);

    Result delete(Integer businessId, List<CartItem> cart, Integer userId, String deviceId);


    Result fillInDiy(double userLng, double userLat, Integer businessId, Cart redisCart, Integer toAddressId, Date selfTime, String selfMobile, Boolean eatInBusiness, Integer userRedId, Integer userCouponId, Date bookTime, Integer userId);

    Result list(int businessId, Integer userId);

    List<Cart> list(Integer userId);

    Result recur(Integer userId, Integer orderId);

    Result chooseCartItem(Integer businessId, List<CartItem> cartItems, Integer userId);
}
