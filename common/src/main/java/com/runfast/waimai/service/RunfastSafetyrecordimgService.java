package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastSafetyrecordimg;
import com.runfast.waimai.dao.model.RunfastSafetyrecordimgExample;

public interface RunfastSafetyrecordimgService extends IService<RunfastSafetyrecordimg, Integer, RunfastSafetyrecordimgExample> {
}