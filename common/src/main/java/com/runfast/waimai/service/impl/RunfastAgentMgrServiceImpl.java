package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.RunfastAgentMgr;
import com.runfast.waimai.dao.model.RunfastAgentMgrExample;
import com.runfast.waimai.service.RunfastAgentMgrService;
import org.springframework.stereotype.Service;

@Service
public class RunfastAgentMgrServiceImpl extends BaseService<RunfastAgentMgr, Integer, RunfastAgentMgrExample> implements RunfastAgentMgrService {
}