package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.RunfastAgentMgrHasRunfastAgentbusiness;
import com.runfast.waimai.dao.model.RunfastAgentMgrHasRunfastAgentbusinessExample;
import com.runfast.waimai.service.RunfastAgentMgrHasRunfastAgentbusinessService;
import org.springframework.stereotype.Service;

@Service
public class RunfastAgentMgrHasRunfastAgentbusinessServiceImpl extends BaseService<RunfastAgentMgrHasRunfastAgentbusiness, Integer, RunfastAgentMgrHasRunfastAgentbusinessExample> implements RunfastAgentMgrHasRunfastAgentbusinessService {
}