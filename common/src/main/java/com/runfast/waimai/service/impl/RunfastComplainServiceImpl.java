package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.RunfastComplain;
import com.runfast.waimai.dao.model.RunfastComplainExample;
import com.runfast.waimai.service.RunfastComplainService;
import org.springframework.stereotype.Service;

@Service
public class RunfastComplainServiceImpl extends BaseService<RunfastComplain, Integer, RunfastComplainExample> implements RunfastComplainService {
}