package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.RunfastCreditlevel;
import com.runfast.waimai.dao.model.RunfastCreditlevelExample;
import com.runfast.waimai.service.RunfastCreditlevelService;
import org.springframework.stereotype.Service;

@Service
public class RunfastCreditlevelServiceImpl extends BaseService<RunfastCreditlevel, Integer, RunfastCreditlevelExample> implements RunfastCreditlevelService {
}