package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastShoppingtrolley;
import com.runfast.waimai.dao.model.RunfastShoppingtrolleyExample;

public interface RunfastShoppingtrolleyService extends IService<RunfastShoppingtrolley, Integer, RunfastShoppingtrolleyExample> {
}