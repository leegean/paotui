package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastWithdraw;
import com.runfast.waimai.dao.model.RunfastWithdrawExample;

public interface RunfastWithdrawService extends IService<RunfastWithdraw, Integer, RunfastWithdrawExample> {
}