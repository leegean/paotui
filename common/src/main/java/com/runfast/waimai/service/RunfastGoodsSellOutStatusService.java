package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastGoodsSellOutStatus;
import com.runfast.waimai.dao.model.RunfastGoodsSellOutStatusExample;

public interface RunfastGoodsSellOutStatusService extends IService<RunfastGoodsSellOutStatus, Integer, RunfastGoodsSellOutStatusExample> {
}