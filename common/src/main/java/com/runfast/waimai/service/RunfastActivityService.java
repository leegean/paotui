package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.common.web.entity.Result;
import com.runfast.waimai.dao.model.RunfastActivity;
import com.runfast.waimai.dao.model.RunfastActivityExample;
import com.runfast.waimai.web.dto.ActivityDto;
import com.runfast.waimai.web.dto.TargetWithOneActivityDto;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface RunfastActivityService extends IService<RunfastActivity, Integer, RunfastActivityExample> {


    List<RunfastActivity> getAgentZoneActivityIn(int agentId, List<Integer> activityTypeList);

    RunfastActivity getActivityWithTarget(Integer activityId, Pageable pageable);

    List<TargetWithOneActivityDto> findBusinessActivity(Integer businessId, List<Integer> activityTypeList);



    Result pick(Integer redId, Integer userId);


    List<ActivityDto> findBusinessActivityDto(Integer businessId);


    Result<List<RunfastActivity>> redActivityForPick(Integer agentId, Integer userId, Boolean random);

    Result<List<RunfastActivity>> businessRedActivityForPick(Integer businessId, Integer userId);

    Result returnActivityForPick(int orderId, Integer userId);
}