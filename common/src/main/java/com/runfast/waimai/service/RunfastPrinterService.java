package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.common.web.entity.Result;
import com.runfast.waimai.dao.model.RunfastBusiness;
import com.runfast.waimai.dao.model.RunfastGoodsSellRecord;
import com.runfast.waimai.dao.model.RunfastPrinter;
import com.runfast.waimai.dao.model.RunfastPrinterExample;

public interface RunfastPrinterService extends IService<RunfastPrinter, Integer, RunfastPrinterExample> {
    Result print(RunfastGoodsSellRecord gr, RunfastBusiness business);
}