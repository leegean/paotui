package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastGoodsSell;
import com.runfast.waimai.dao.model.RunfastGoodsSellExample;
import com.runfast.waimai.entity.GoodsOptionPair;
import com.runfast.waimai.entity.GoodsStandarPair;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

public interface RunfastGoodsSellService extends IService<RunfastGoodsSell, Integer, RunfastGoodsSellExample> {
    List<RunfastGoodsSell> search(String name, Integer businessId, Pageable pageable);


    List<Map<String, Object>> findAgentOffZoneGoods(Integer agentId, double userLng, double userLat, Date deliveryStart1, Date deliveryEnd1, Date deliveryStart2, Date deliveryEnd2, Pageable pageable);


    List<Map<String, Object>> checkGoodsWithStandar(Integer businessId, Set<GoodsStandarPair> goodsStandarPairSet);

    List<Map<String, Object>> checkGoodsWithOption(Integer businessId, Set<GoodsOptionPair> goodsOptionPairSet);

    RunfastGoodsSell detail(int goodsId);

    void populateGoods(RunfastGoodsSell goodsSell);

    Long countOfRequired(Integer businessId);
}