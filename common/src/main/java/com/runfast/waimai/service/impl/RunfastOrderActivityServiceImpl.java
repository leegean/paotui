package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.RunfastOrderActivity;
import com.runfast.waimai.dao.model.RunfastOrderActivityExample;
import com.runfast.waimai.service.RunfastOrderActivityService;
import org.springframework.stereotype.Service;

@Service
public class RunfastOrderActivityServiceImpl extends BaseService<RunfastOrderActivity, Integer, RunfastOrderActivityExample> implements RunfastOrderActivityService {
}