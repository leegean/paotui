package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.RunfastTransorder;
import com.runfast.waimai.dao.model.RunfastTransorderExample;
import com.runfast.waimai.service.RunfastTransorderService;
import org.springframework.stereotype.Service;

@Service
public class RunfastTransorderServiceImpl extends BaseService<RunfastTransorder, Integer, RunfastTransorderExample> implements RunfastTransorderService {
}