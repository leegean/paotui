package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastBusinessBusnameauditing;
import com.runfast.waimai.dao.model.RunfastBusinessBusnameauditingExample;

public interface RunfastBusinessBusnameauditingService extends IService<RunfastBusinessBusnameauditing, Integer, RunfastBusinessBusnameauditingExample> {
}