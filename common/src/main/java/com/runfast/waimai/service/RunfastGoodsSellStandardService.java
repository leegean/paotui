package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastGoodsSellStandard;
import com.runfast.waimai.dao.model.RunfastGoodsSellStandardExample;

public interface RunfastGoodsSellStandardService extends IService<RunfastGoodsSellStandard, Integer, RunfastGoodsSellStandardExample> {

    public void updateStock(Integer goodsId, Integer standarId, Integer num);
}