package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastWithdrawrecord;
import com.runfast.waimai.dao.model.RunfastWithdrawrecordExample;

public interface RunfastWithdrawrecordService extends IService<RunfastWithdrawrecord, Integer, RunfastWithdrawrecordExample> {
}