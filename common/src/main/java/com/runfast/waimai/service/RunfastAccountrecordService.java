package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastAccountrecord;
import com.runfast.waimai.dao.model.RunfastAccountrecordExample;

public interface RunfastAccountrecordService extends IService<RunfastAccountrecord, Integer, RunfastAccountrecordExample> {
}