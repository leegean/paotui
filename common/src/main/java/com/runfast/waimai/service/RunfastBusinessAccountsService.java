package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastBusinessAccounts;
import com.runfast.waimai.dao.model.RunfastBusinessAccountsExample;

public interface RunfastBusinessAccountsService extends IService<RunfastBusinessAccounts, Integer, RunfastBusinessAccountsExample> {
}