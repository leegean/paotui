package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.SysUserToken;
import com.runfast.waimai.dao.model.SysUserTokenExample;
import com.runfast.waimai.service.SysUserTokenService;
import org.springframework.stereotype.Service;

@Service
public class SysUserTokenServiceImpl extends BaseService<SysUserToken, Integer, SysUserTokenExample> implements SysUserTokenService {
}