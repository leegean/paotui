package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastVisitRecord;
import com.runfast.waimai.dao.model.RunfastVisitRecordExample;

public interface RunfastVisitRecordService extends IService<RunfastVisitRecord, Integer, RunfastVisitRecordExample> {
}