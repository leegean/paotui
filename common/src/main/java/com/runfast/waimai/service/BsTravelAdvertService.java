package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.BsTravelAdvert;
import com.runfast.waimai.dao.model.BsTravelAdvertExample;

public interface BsTravelAdvertService extends IService<BsTravelAdvert, Integer, BsTravelAdvertExample> {
}