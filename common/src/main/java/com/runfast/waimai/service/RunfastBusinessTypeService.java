package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastBusinessType;
import com.runfast.waimai.dao.model.RunfastBusinessTypeExample;

import java.util.List;

public interface RunfastBusinessTypeService extends IService<RunfastBusinessType, Integer, RunfastBusinessTypeExample> {
    List<RunfastBusinessType> findAllBusinessType();


}