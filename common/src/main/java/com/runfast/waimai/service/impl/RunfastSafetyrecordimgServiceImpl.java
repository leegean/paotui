package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.RunfastSafetyrecordimg;
import com.runfast.waimai.dao.model.RunfastSafetyrecordimgExample;
import com.runfast.waimai.service.RunfastSafetyrecordimgService;
import org.springframework.stereotype.Service;

@Service
public class RunfastSafetyrecordimgServiceImpl extends BaseService<RunfastSafetyrecordimg, Integer, RunfastSafetyrecordimgExample> implements RunfastSafetyrecordimgService {
}