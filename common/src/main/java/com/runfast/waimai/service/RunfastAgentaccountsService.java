package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastAgentaccounts;
import com.runfast.waimai.dao.model.RunfastAgentaccountsExample;

public interface RunfastAgentaccountsService extends IService<RunfastAgentaccounts, Integer, RunfastAgentaccountsExample> {
}