package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.RunfastGoodsSellSubOption;
import com.runfast.waimai.dao.model.RunfastGoodsSellSubOptionExample;
import com.runfast.waimai.service.RunfastGoodsSellSubOptionService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RunfastGoodsSellSubOptionServiceImpl extends BaseService<RunfastGoodsSellSubOption, Integer, RunfastGoodsSellSubOptionExample> implements RunfastGoodsSellSubOptionService {

}