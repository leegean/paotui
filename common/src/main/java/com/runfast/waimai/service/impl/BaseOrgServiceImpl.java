package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.BaseOrg;
import com.runfast.waimai.dao.model.BaseOrgExample;
import com.runfast.waimai.service.BaseOrgService;
import org.springframework.stereotype.Service;

@Service
public class BaseOrgServiceImpl extends BaseService<BaseOrg, Integer, BaseOrgExample> implements BaseOrgService {
}