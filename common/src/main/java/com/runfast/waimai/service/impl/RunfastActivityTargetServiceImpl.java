package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.RunfastActivityTarget;
import com.runfast.waimai.dao.model.RunfastActivityTargetExample;
import com.runfast.waimai.service.RunfastActivityTargetService;
import org.springframework.stereotype.Service;

@Service
public class RunfastActivityTargetServiceImpl extends BaseService<RunfastActivityTarget, Integer, RunfastActivityTargetExample> implements RunfastActivityTargetService {
}