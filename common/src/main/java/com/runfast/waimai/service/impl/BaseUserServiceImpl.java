package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.BaseUser;
import com.runfast.waimai.dao.model.BaseUserExample;
import com.runfast.waimai.service.BaseUserService;
import org.springframework.stereotype.Service;

@Service
public class BaseUserServiceImpl extends BaseService<BaseUser, Integer, BaseUserExample> implements BaseUserService {
}