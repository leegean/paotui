package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastAgentApplication;
import com.runfast.waimai.dao.model.RunfastAgentApplicationExample;

public interface RunfastAgentApplicationService extends IService<RunfastAgentApplication, Integer, RunfastAgentApplicationExample> {
}