package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastSmsSendrecord;
import com.runfast.waimai.dao.model.RunfastSmsSendrecordExample;

public interface RunfastSmsSendrecordService extends IService<RunfastSmsSendrecord, Integer, RunfastSmsSendrecordExample> {
}