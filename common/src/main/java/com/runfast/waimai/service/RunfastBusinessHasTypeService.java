package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastBusinessHasType;
import com.runfast.waimai.dao.model.RunfastBusinessHasTypeExample;

public interface RunfastBusinessHasTypeService extends IService<RunfastBusinessHasType, Integer, RunfastBusinessHasTypeExample> {
}