package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastActivityTarget;
import com.runfast.waimai.dao.model.RunfastActivityTargetExample;

public interface RunfastActivityTargetService extends IService<RunfastActivityTarget, Integer, RunfastActivityTargetExample> {
}