package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.BaseRole;
import com.runfast.waimai.dao.model.BaseRoleExample;
import com.runfast.waimai.service.BaseRoleService;
import org.springframework.stereotype.Service;

@Service
public class BaseRoleServiceImpl extends BaseService<BaseRole, Integer, BaseRoleExample> implements BaseRoleService {
}