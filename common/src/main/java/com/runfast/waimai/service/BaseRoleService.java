package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.BaseRole;
import com.runfast.waimai.dao.model.BaseRoleExample;

public interface BaseRoleService extends IService<BaseRole, Integer, BaseRoleExample> {
}