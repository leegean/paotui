package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.mapper.RunfastRedPacketRecordMapper;
import com.runfast.waimai.dao.model.*;
import com.runfast.waimai.service.RunfastGoodsSellRecordService;
import com.runfast.waimai.service.RunfastRedPacketRecordService;
import com.runfast.waimai.web.dto.RedPacketRecordWithOneRedActivityDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class RunfastRedPacketRecordServiceImpl extends BaseService<RunfastRedPacketRecord, Integer, RunfastRedPacketRecordExample> implements RunfastRedPacketRecordService {


    @Autowired
    private RunfastGoodsSellRecordService orderService;
    @Override
    public RunfastRedPacketRecordMapper getMapper() {
        return (RunfastRedPacketRecordMapper)super.getMapper();
    }

    @Override
    public RedPacketRecordWithOneRedActivityDto getRedPacketRecordWithOneRedActivity(Integer userRedId) {
        return getMapper().getRedPacketRecordWithOneRedActivity(userRedId);
    }

    @Override
    public List<Map<String, Object>> findRedRecord(Integer userId, List<Integer> activityTypeList, Pageable pageable) {
        return getMapper().findRedRecord(userId,activityTypeList,pageable);
    }

    @Override
    public List<Map<String, Object>> findValidRedRecord(Integer userId, Integer businessId, double full, List<Integer> activityTypeList) {
        return getMapper().findValidRedRecord(userId,businessId, full,activityTypeList);
    }


}