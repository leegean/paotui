package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.BaseUserRole;
import com.runfast.waimai.dao.model.BaseUserRoleExample;

public interface BaseUserRoleService extends IService<BaseUserRole, Integer, BaseUserRoleExample> {
}