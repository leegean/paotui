package com.runfast.waimai.service.impl;

import com.runfast.common.dao.IMapper;
import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.mapper.RunfastBusinessCommentMapper;
import com.runfast.waimai.dao.model.RunfastBusinessComment;
import com.runfast.waimai.dao.model.RunfastBusinessCommentExample;
import com.runfast.waimai.service.RunfastBusinessCommentService;
import com.runfast.waimai.web.dto.UserBusinessCommentDto;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class RunfastBusinessCommentServiceImpl extends BaseService<RunfastBusinessComment, Integer, RunfastBusinessCommentExample> implements RunfastBusinessCommentService {
    @Override
    public RunfastBusinessCommentMapper getMapper() {
        return (RunfastBusinessCommentMapper)super.getMapper();
    }

    @Override
    public Map<String, Object> businessCommentStat(Integer businessId, Integer userId) {
        return getMapper().businessCommentStat(businessId,userId);
    }

    @Override
    public List<RunfastBusinessComment> find(Integer businessId,Integer userId, Pageable pageable) {
        return getMapper().find(businessId, userId,pageable);
    }

    @Override
    public List<UserBusinessCommentDto> findUserComment(Integer userId) {
        return getMapper().findUserComment(userId);
    }
}