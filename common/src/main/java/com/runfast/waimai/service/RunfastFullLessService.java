package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastFullLess;
import com.runfast.waimai.dao.model.RunfastFullLessExample;

public interface RunfastFullLessService extends IService<RunfastFullLess, Integer, RunfastFullLessExample> {
}