package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastComplain;
import com.runfast.waimai.dao.model.RunfastComplainExample;

public interface RunfastComplainService extends IService<RunfastComplain, Integer, RunfastComplainExample> {
}