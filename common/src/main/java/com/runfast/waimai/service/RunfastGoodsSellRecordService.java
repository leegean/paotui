package com.runfast.waimai.service;

import com.runfast.common.dao.model.RunfastCuser;
import com.runfast.common.service.IService;
import com.runfast.common.web.entity.Result;
import com.runfast.pay.RefundType;
import com.runfast.waimai.dao.model.RunfastGoodsSellRecord;
import com.runfast.waimai.dao.model.RunfastGoodsSellRecordExample;
import com.runfast.waimai.entity.Cart;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface RunfastGoodsSellRecordService extends IService<RunfastGoodsSellRecord, Integer, RunfastGoodsSellRecordExample> {
    Result confirm(Integer businessId, Integer userId, String remark);

    Result list(Integer userId, Pageable pageable);

    Cart orderDetail(Integer userId, RunfastGoodsSellRecord order);

    Result applyCancel(Integer userId, Integer orderId);

    void autoCancel(Integer userId, Integer orderId);


    void autoComplete(Integer userId, Integer orderId);


    Result autoCancel(RunfastCuser cuser, RunfastGoodsSellRecord order, String description);

    Result applyRefund( Integer orderId,String reason, RefundType refundType, Integer refundAmount);


    Map<String,Object> findLastestOrder(Integer agentId, Integer userId);


    List<Map<String,Integer>> findCompletableOrder(Date time);

    List<Map<String,Integer>> findConfirmRefundOrder(Date time);

    List<Map<String,Integer>> findConfirmCancelOrder(Date time);

    void confirmCancel(Integer userId, Integer orderId);

    void confirmRefund(Integer userId, Integer orderId);

    Result walletPay(Integer orderId, String password);



    Result applyRefund(Integer orderId, List<Integer> orderItemId, String reason);

    List<Map<String,Integer>> findCancelNewOrder(Date time);

    List<Map<String,Integer>> findCancelPaidOrder(Date time);

    Integer resetIsComent(int orderId);

    Result applyRefundInfo(Integer orderId, List orderItemId);

    Result businessTake(RunfastGoodsSellRecord order);

    Result complete(Integer orderId);
}