package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.RunfastShoppingtrolley;
import com.runfast.waimai.dao.model.RunfastShoppingtrolleyExample;
import com.runfast.waimai.service.RunfastShoppingtrolleyService;
import org.springframework.stereotype.Service;

@Service
public class RunfastShoppingtrolleyServiceImpl extends BaseService<RunfastShoppingtrolley, Integer, RunfastShoppingtrolleyExample> implements RunfastShoppingtrolleyService {
}