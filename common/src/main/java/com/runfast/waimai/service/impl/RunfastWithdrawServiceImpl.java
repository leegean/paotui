package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.RunfastWithdraw;
import com.runfast.waimai.dao.model.RunfastWithdrawExample;
import com.runfast.waimai.service.RunfastWithdrawService;
import org.springframework.stereotype.Service;

@Service
public class RunfastWithdrawServiceImpl extends BaseService<RunfastWithdraw, Integer, RunfastWithdrawExample> implements RunfastWithdrawService {
}