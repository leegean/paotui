package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.BaseRolePurview;
import com.runfast.waimai.dao.model.BaseRolePurviewExample;

public interface BaseRolePurviewService extends IService<BaseRolePurview, Integer, BaseRolePurviewExample> {
}