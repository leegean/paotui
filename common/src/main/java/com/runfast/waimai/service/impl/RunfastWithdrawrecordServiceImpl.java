package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.RunfastWithdrawrecord;
import com.runfast.waimai.dao.model.RunfastWithdrawrecordExample;
import com.runfast.waimai.service.RunfastWithdrawrecordService;
import org.springframework.stereotype.Service;

@Service
public class RunfastWithdrawrecordServiceImpl extends BaseService<RunfastWithdrawrecord, Integer, RunfastWithdrawrecordExample> implements RunfastWithdrawrecordService {
}