package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.BsTravelAdvert;
import com.runfast.waimai.dao.model.BsTravelAdvertExample;
import com.runfast.waimai.service.BsTravelAdvertService;
import org.springframework.stereotype.Service;

@Service
public class BsTravelAdvertServiceImpl extends BaseService<BsTravelAdvert, Integer, BsTravelAdvertExample> implements BsTravelAdvertService {
}