package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.RunfastMessge;
import com.runfast.waimai.dao.model.RunfastMessgeExample;
import com.runfast.waimai.service.RunfastMessgeService;
import org.springframework.stereotype.Service;

@Service
public class RunfastMessgeServiceImpl extends BaseService<RunfastMessge, Integer, RunfastMessgeExample> implements RunfastMessgeService {
}