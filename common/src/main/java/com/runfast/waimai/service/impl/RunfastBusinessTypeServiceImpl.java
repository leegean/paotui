package com.runfast.waimai.service.impl;

import com.runfast.common.dao.IMapper;
import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.mapper.RunfastBusinessTypeMapper;
import com.runfast.waimai.dao.model.RunfastBusinessType;
import com.runfast.waimai.dao.model.RunfastBusinessTypeExample;
import com.runfast.waimai.service.RunfastBusinessTypeService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RunfastBusinessTypeServiceImpl extends BaseService<RunfastBusinessType, Integer, RunfastBusinessTypeExample> implements RunfastBusinessTypeService {

    @Override
    public RunfastBusinessTypeMapper getMapper() {
        return (RunfastBusinessTypeMapper)super.getMapper();
    }

    @Override
    public List<RunfastBusinessType> findAllBusinessType() {
        return getMapper().findAllBusinessType();
    }
}