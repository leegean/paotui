package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastMessge;
import com.runfast.waimai.dao.model.RunfastMessgeExample;

public interface RunfastMessgeService extends IService<RunfastMessge, Integer, RunfastMessgeExample> {
}