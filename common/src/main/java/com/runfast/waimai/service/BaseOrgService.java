package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.BaseOrg;
import com.runfast.waimai.dao.model.BaseOrgExample;

public interface BaseOrgService extends IService<BaseOrg, Integer, BaseOrgExample> {
}