package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.common.dao.model.RunfastDeliverCost;
import com.runfast.common.web.entity.Result;
import com.runfast.waimai.dao.model.RunfastBusiness;
import com.runfast.waimai.dao.model.RunfastBusinessExample;
import com.runfast.waimai.web.dto.BusinessDto;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface RunfastBusinessService extends IService<RunfastBusiness, Integer, RunfastBusinessExample> {
    List<Map<String, Object>> nearBy(Integer agentId, Integer range, Double longitude, Double latitude, Integer sorting, List<Integer> activityType, List<Integer> catalogId, Integer businessFeature, Pageable pageable);

    List<Map<String, Object>> offZoneMoreBusiness(Integer agentId, Integer range, Double userLng, Double userLat, List<Integer> activityType, Pageable pageable);

    Result detail(Integer businessId, Double userLng, Double userLat);


    Map<String, Object> detail(RunfastDeliverCost defaultDeliveryTemplate, BusinessDto businessDto);

    Map<String, Object> getDeliveryInfo(BusinessDto businessDto);

    List<Map<String, Object>> search(String name, Integer agentId, Double userLng, Double userLat, Pageable pageable);



    Result getAgentZoneBusiness(Integer activityId, Double userLng, Double userLat, Pageable pageable);


    BusinessDto detailWithDistance(Integer businessId, Double userLng, Double userLat);

    BusinessDto detailWithIsOpen(Integer businessId, Double userLng, Double userLat, Date deliveryStart1, Date deliveryEnd1, Date deliveryStart2, Date deliveryEnd2);

    Map<String,Object> findNameById(Integer businessId);

    Map<String,Object> findImgById(Integer businessId);
}