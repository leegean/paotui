package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.mapper.RunfastGoodsSellTypeMapper;
import com.runfast.waimai.dao.model.RunfastGoodsSell;
import com.runfast.waimai.dao.model.RunfastGoodsSellType;
import com.runfast.waimai.dao.model.RunfastGoodsSellTypeExample;
import com.runfast.waimai.service.RunfastActivityService;
import com.runfast.waimai.service.RunfastGoodsSellService;
import com.runfast.waimai.service.RunfastGoodsSellStandardService;
import com.runfast.waimai.service.RunfastGoodsSellTypeService;
import com.runfast.waimai.web.dto.GoodsStandarDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RunfastGoodsSellTypeServiceImpl extends BaseService<RunfastGoodsSellType, Integer, RunfastGoodsSellTypeExample> implements RunfastGoodsSellTypeService {

    @Autowired
    private RunfastGoodsSellService goodsSellService;

    @Autowired
    private RunfastGoodsSellStandardService goodsSellStandardService;

    @Autowired
    private RunfastActivityService activityService;

    @Override
    public RunfastGoodsSellTypeMapper getMapper() {
        return (RunfastGoodsSellTypeMapper)super.getMapper();
    }

    @Override
    public List<RunfastGoodsSellType> listWithGoods(int businessId) {

        /*RunfastGoodsSellTypeExample goodsSellTypeExample = new RunfastGoodsSellTypeExample();
        goodsSellTypeExample.or().andBusinessidEqualTo(businessId).andDeletedEqualTo(0);
        goodsSellTypeExample.or().andBusinessidEqualTo(businessId).andDeletedIsNull();
        goodsSellTypeExample.setOrderByClause("sort desc");

        List<RunfastGoodsSellType> goodsSellTypes = this.selectByExample(goodsSellTypeExample);
        for (RunfastGoodsSellType goodsSellType : goodsSellTypes) {
        }*/


        /*RunfastGoodsSellTypeMapper mapper = getMapper();
        List<RunfastGoodsSellType> goodsSellTypeList = mapper.listWithGoods(businessId);



        for (RunfastGoodsSellType goodsSellType : goodsSellTypeList) {
            List<RunfastGoodsSell> goodsSellList = goodsSellType.getGoodsSellList();
            for (RunfastGoodsSell goodsSell : goodsSellList) {

                RunfastGoodsSellStandardExample goodsSellStandardExample = new RunfastGoodsSellStandardExample();
                goodsSellStandardExample.or().andGoodssellidEqualTo(goodsSell.getId());
                goodsSellStandardExample.setOrderByClause("sort desc");
                List<RunfastGoodsSellStandard> goodsSellStandardList = goodsSellStandardService.selectByExample(goodsSellStandardExample);


            }
        }

        ArrayList<Integer> typeList = new ArrayList<>();
        typeList.add(2);
        typeList.add(4);
        List<RunfastActivity> activityList = activityService.getValidActivities(businessId, typeList);
        for (RunfastActivity activity : activityList) {
            String stanids = activity.getStanids();
            if(StringUtils.isNotBlank(stanids)) {
                String[] stand = stanids.split(",", -1);

            }
        }*/

        RunfastGoodsSellTypeMapper mapper = getMapper();
        List<RunfastGoodsSellType> goodsSellTypeList = mapper.listWithGoods(businessId);


        RunfastGoodsSellType offType = new RunfastGoodsSellType();
        offType.setName("折扣");
        List<RunfastGoodsSell> offGoodsList = new ArrayList<>();
        offType.setGoodsSellList(offGoodsList);
        for (RunfastGoodsSellType goodsSellType : goodsSellTypeList) {
            List<RunfastGoodsSell> goodsSellList = goodsSellType.getGoodsSellList();
            if (goodsSellList != null) {
                for (RunfastGoodsSell goodsSell : goodsSellList) {

                    goodsSellService.populateGoods(goodsSell);


                    /**
                     * 选取打折分类商品
                     */
                    List<GoodsStandarDto> standardList = goodsSell.getGoodsSellStandardList();
                    for (GoodsStandarDto standarDto : standardList) {
                        Integer activityId = standarDto.getActivityId();
                        if (activityId != null) { //不为null，即有打折活动

                            offGoodsList.add(goodsSell);
                            break;
                        }
                    }
                }
            }
        }

        if(!offGoodsList.isEmpty()) goodsSellTypeList.add(0, offType);
        return goodsSellTypeList;
    }
}