package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.BaseUserRole;
import com.runfast.waimai.dao.model.BaseUserRoleExample;
import com.runfast.waimai.service.BaseUserRoleService;
import org.springframework.stereotype.Service;

@Service
public class BaseUserRoleServiceImpl extends BaseService<BaseUserRole, Integer, BaseUserRoleExample> implements BaseUserRoleService {
}