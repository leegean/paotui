package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.RunfastRefund;
import com.runfast.waimai.dao.model.RunfastRefundExample;
import com.runfast.waimai.service.RunfastRefundService;
import org.springframework.stereotype.Service;

@Service
public class RunfastRefundServiceImpl extends BaseService<RunfastRefund, Integer, RunfastRefundExample> implements RunfastRefundService {
}