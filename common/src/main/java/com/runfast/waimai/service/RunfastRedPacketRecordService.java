package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastRedPacketRecord;
import com.runfast.waimai.dao.model.RunfastRedPacketRecordExample;
import com.runfast.waimai.web.dto.RedPacketRecordWithOneRedActivityDto;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface RunfastRedPacketRecordService extends IService<RunfastRedPacketRecord, Integer, RunfastRedPacketRecordExample> {
    RedPacketRecordWithOneRedActivityDto getRedPacketRecordWithOneRedActivity(Integer userRedId);


    List<Map<String, Object>> findRedRecord(Integer userId, List<Integer> activityTypeList, Pageable pageable);

    List<Map<String, Object>> findValidRedRecord(Integer userId, Integer businessId, double v, List<Integer> activityTypeList);
}