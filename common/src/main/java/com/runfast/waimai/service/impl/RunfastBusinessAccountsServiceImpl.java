package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.RunfastBusinessAccounts;
import com.runfast.waimai.dao.model.RunfastBusinessAccountsExample;
import com.runfast.waimai.service.RunfastBusinessAccountsService;
import org.springframework.stereotype.Service;

@Service
public class RunfastBusinessAccountsServiceImpl extends BaseService<RunfastBusinessAccounts, Integer, RunfastBusinessAccountsExample> implements RunfastBusinessAccountsService {
}