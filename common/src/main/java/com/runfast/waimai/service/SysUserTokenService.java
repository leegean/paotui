package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.SysUserToken;
import com.runfast.waimai.dao.model.SysUserTokenExample;

public interface SysUserTokenService extends IService<SysUserToken, Integer, SysUserTokenExample> {
}