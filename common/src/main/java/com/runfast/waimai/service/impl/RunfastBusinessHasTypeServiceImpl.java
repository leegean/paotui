package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.RunfastBusinessHasType;
import com.runfast.waimai.dao.model.RunfastBusinessHasTypeExample;
import com.runfast.waimai.service.RunfastBusinessHasTypeService;
import org.springframework.stereotype.Service;

@Service
public class RunfastBusinessHasTypeServiceImpl extends BaseService<RunfastBusinessHasType, Integer, RunfastBusinessHasTypeExample> implements RunfastBusinessHasTypeService {
}