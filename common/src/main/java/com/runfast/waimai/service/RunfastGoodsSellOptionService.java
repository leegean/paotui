package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastGoodsSell;
import com.runfast.waimai.dao.model.RunfastGoodsSellOption;
import com.runfast.waimai.dao.model.RunfastGoodsSellOptionExample;
import com.runfast.waimai.entity.CartItem;
import com.runfast.waimai.entity.OptionIdPair;

import java.util.List;

public interface RunfastGoodsSellOptionService extends IService<RunfastGoodsSellOption, Integer, RunfastGoodsSellOptionExample> {
    List<RunfastGoodsSellOption> findOptionWithSub(Integer goodsId);

}