package com.runfast.waimai.service.impl;

import com.runfast.common.dao.model.RunfastDeliverCost;
import com.runfast.common.service.BaseService;
import com.runfast.common.service.RunfastDeliverCostService;
import com.runfast.common.web.entity.Result;
import com.runfast.waimai.dao.mapper.RunfastBusinessMapper;
import com.runfast.waimai.dao.mapper.RunfastEnshrineMapper;
import com.runfast.waimai.dao.model.RunfastEnshrine;
import com.runfast.waimai.dao.model.RunfastEnshrineExample;
import com.runfast.waimai.service.RunfastBusinessService;
import com.runfast.waimai.service.RunfastEnshrineService;
import com.runfast.waimai.web.dto.BusinessDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class RunfastEnshrineServiceImpl extends BaseService<RunfastEnshrine, Integer, RunfastEnshrineExample> implements RunfastEnshrineService {

    @Autowired
    private RunfastBusinessService businessService;

    @Autowired
    private RunfastDeliverCostService deliverCostService;

    @Autowired
    private RunfastBusinessMapper businessMapper;

    @Override
    public RunfastEnshrineMapper getMapper() {
        return (RunfastEnshrineMapper)super.getMapper();
    }

    @Override
    public Result<List<Object>> list(Integer userId, Double userLng, Double userLat, Pageable pageable) {
        RunfastEnshrineExample enshrineExample = new RunfastEnshrineExample();
        enshrineExample.or().andCidEqualTo(userId);
        List<RunfastEnshrine> enshrineList = this.selectByExampleWithPageable(enshrineExample,pageable);

        List<Object> enshrineObjectList = new ArrayList<>();

        List<Integer> businessIdList = new ArrayList<>();
        for (RunfastEnshrine enshrine : enshrineList) {
            Integer type = enshrine.getType();
            if(type==1){//收藏商家
                businessIdList.add(enshrine.getShopId());
            }
        }

        if(!businessIdList.isEmpty()){
            List<BusinessDto> businessDtoList = businessMapper.listBusiness(businessIdList, userLng, userLat);

            List<Object> businessList = new ArrayList<>();
            for (BusinessDto businessDto : businessDtoList) {
                RunfastDeliverCost defaultDeliveryTemplate = deliverCostService.getDefaultDeliveryTemplate(businessDto.getAgentId());
                Date deliveryStart1 = defaultDeliveryTemplate.getStartTimeDay1();
                Date deliveryEnd1 = defaultDeliveryTemplate.getEndTimeDay1();
                Date deliveryStart2 = defaultDeliveryTemplate.getStartTimeNight2();
                Date deliveryEnd2 = defaultDeliveryTemplate.getEndTimeNight2();
                BusinessDto dto = businessService.detailWithIsOpen(businessDto.getId(), userLng, userLat, deliveryStart1, deliveryEnd1, deliveryStart2, deliveryEnd2);

                businessList.add(businessService.detail(null, dto));
            }
            enshrineObjectList.addAll(businessList);
        }
        return Result.ok("", enshrineObjectList);
    }

    @Override
    public Integer deleteByPrimaryKey(Integer id) {
        return getMapper().deleteByPrimaryKey(id);
    }
}