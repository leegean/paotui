package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.RunfastScorerecord;
import com.runfast.waimai.dao.model.RunfastScorerecordExample;
import com.runfast.waimai.service.RunfastScorerecordService;
import org.springframework.stereotype.Service;

@Service
public class RunfastScorerecordServiceImpl extends BaseService<RunfastScorerecord, Integer, RunfastScorerecordExample> implements RunfastScorerecordService {
}