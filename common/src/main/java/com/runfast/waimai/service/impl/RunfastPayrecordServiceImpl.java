package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.RunfastPayrecord;
import com.runfast.waimai.dao.model.RunfastPayrecordExample;
import com.runfast.waimai.service.RunfastPayrecordService;
import org.springframework.stereotype.Service;

@Service
public class RunfastPayrecordServiceImpl extends BaseService<RunfastPayrecord, Integer, RunfastPayrecordExample> implements RunfastPayrecordService {
}