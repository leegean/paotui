package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastOrderActivity;
import com.runfast.waimai.dao.model.RunfastOrderActivityExample;

public interface RunfastOrderActivityService extends IService<RunfastOrderActivity, Integer, RunfastOrderActivityExample> {
}