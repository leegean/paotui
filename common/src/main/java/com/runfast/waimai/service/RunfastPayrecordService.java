package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastPayrecord;
import com.runfast.waimai.dao.model.RunfastPayrecordExample;

public interface RunfastPayrecordService extends IService<RunfastPayrecord, Integer, RunfastPayrecordExample> {
}