package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.RunfastWithdraworder;
import com.runfast.waimai.dao.model.RunfastWithdraworderExample;
import com.runfast.waimai.service.RunfastWithdraworderService;
import org.springframework.stereotype.Service;

@Service
public class RunfastWithdraworderServiceImpl extends BaseService<RunfastWithdraworder, Integer, RunfastWithdraworderExample> implements RunfastWithdraworderService {
}