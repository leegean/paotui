package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastDriverApplication;
import com.runfast.waimai.dao.model.RunfastDriverApplicationExample;

public interface RunfastDriverApplicationService extends IService<RunfastDriverApplication, Integer, RunfastDriverApplicationExample> {
}