package com.runfast.waimai.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.model.BasePurview;
import com.runfast.waimai.dao.model.BasePurviewExample;
import com.runfast.waimai.service.BasePurviewService;
import org.springframework.stereotype.Service;

@Service
public class BasePurviewServiceImpl extends BaseService<BasePurview, Integer, BasePurviewExample> implements BasePurviewService {
}