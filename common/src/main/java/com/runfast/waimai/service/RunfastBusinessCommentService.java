package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastBusinessComment;
import com.runfast.waimai.dao.model.RunfastBusinessCommentExample;
import com.runfast.waimai.web.dto.UserBusinessCommentDto;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

public interface RunfastBusinessCommentService extends IService<RunfastBusinessComment, Integer, RunfastBusinessCommentExample> {
    Map<String,Object> businessCommentStat(Integer businessId, Integer userId);

    List<RunfastBusinessComment> find(Integer businessId, Integer userId,Pageable pageable);

    List<UserBusinessCommentDto> findUserComment(Integer userId);
}