package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastGoodsSellChildren;
import com.runfast.waimai.dao.model.RunfastGoodsSellChildrenExample;

import java.util.List;

public interface RunfastGoodsSellChildrenService extends IService<RunfastGoodsSellChildren, Integer, RunfastGoodsSellChildrenExample> {
    List<RunfastGoodsSellChildren> findGroupByOrderId(RunfastGoodsSellChildrenExample orderItemExample);
}