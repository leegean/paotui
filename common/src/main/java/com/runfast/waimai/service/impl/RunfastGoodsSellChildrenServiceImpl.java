package com.runfast.waimai.service.impl;

import com.runfast.common.dao.IMapper;
import com.runfast.common.service.BaseService;
import com.runfast.waimai.dao.mapper.RunfastGoodsSellChildrenMapper;
import com.runfast.waimai.dao.model.RunfastGoodsSellChildren;
import com.runfast.waimai.dao.model.RunfastGoodsSellChildrenExample;
import com.runfast.waimai.service.RunfastGoodsSellChildrenService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RunfastGoodsSellChildrenServiceImpl extends BaseService<RunfastGoodsSellChildren, Integer, RunfastGoodsSellChildrenExample> implements RunfastGoodsSellChildrenService {
    @Override
    public RunfastGoodsSellChildrenMapper getMapper() {
        return (RunfastGoodsSellChildrenMapper)super.getMapper();
    }

    @Override
    public List<RunfastGoodsSellChildren> findGroupByOrderId(RunfastGoodsSellChildrenExample orderItemExample) {
        return getMapper().findGroupByOrderId(orderItemExample);
    }
}