package com.runfast.waimai.service;

import com.runfast.common.service.IService;
import com.runfast.waimai.dao.model.RunfastBusinessalliance;
import com.runfast.waimai.dao.model.RunfastBusinessallianceExample;

public interface RunfastBusinessallianceService extends IService<RunfastBusinessalliance, Integer, RunfastBusinessallianceExample> {
}