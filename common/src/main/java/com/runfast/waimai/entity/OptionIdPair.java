package com.runfast.waimai.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author: lijin
 * @date: 2018年05月12日
 */
@Data
public class OptionIdPair implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer optionId;

    private Integer subOptionId;
}
