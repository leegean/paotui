package com.runfast.waimai.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author: lijin
 * @date: 2018年05月12日
 */

@Data
public class CartItemKey implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer goodsId;
    private Integer standarId;

    private List<OptionIdPair> optionIdPairList = new ArrayList<>();


}
