package com.runfast.waimai.entity;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author: lijin
 * @date: 2018年05月14日
 */

@Data
public class CartItem implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;
    private CartItemKey key;

    /**
     * 购买数量
     */
    private Integer num;

    /**
     * 商品名称
     */
    private String goodsName;
    /**
     * 商品图片
     */
    private String goodsImg;

    /**
     * 规格名称
     */
    private String standarName;

    /**
     * 商品规格名称+商品子选项名称(可能有多个子选项)
     */
    private String standarOptionName;

    /**
     * 原单价
     */
    private BigDecimal price;

    /**
     * 原总价
     */
    private BigDecimal totalPrice;


    /**
     * 打折特价活动id
     */
    private Integer activityId;

    private String activityName;

    private Integer activityType;

    /**
     * 优惠单价
     */
    private BigDecimal disprice;

    /**
     * 优惠总价
     */
    private BigDecimal totalDisprice;

    /**
     * 购物车条目添加时间
     */
    private Date createTime;

    private Boolean checked;


}
