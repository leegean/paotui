package com.runfast.waimai.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: lijin
 * @date: 2018年05月29日
 */

@Data
@AllArgsConstructor
public class GoodsOptionPair {

    private Integer goodsId;

    private OptionIdPair optionIdPair;
}
