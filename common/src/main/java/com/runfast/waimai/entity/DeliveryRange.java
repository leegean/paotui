package com.runfast.waimai.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalTime;

/**
 * @author: lijin
 * @date: 2018年07月12日
 */

@Data
@AllArgsConstructor
public class DeliveryRange {

    private LocalTime start;
    private LocalTime end;
    private BigDecimal deliveryFee;
}
