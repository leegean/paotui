package com.runfast.waimai.entity;

import com.runfast.waimai.dao.model.RunfastOrderActivity;
import lombok.Data;
import org.apache.commons.lang3.ObjectUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author: lijin
 * @date: 2018年05月19日
 */
@Data
public class Cart implements Serializable{

    private static final long serialVersionUID = 1L;

    private Integer orderId;
    private String businessImg;
    private Integer isDeliver;
    private Integer businessId;
    private String businessName;
    private String businessAddr;//商家地址
    private String businessAddressLat;// 商家纬度
    private String businessAddressLng;// 商家经度
    private String businessMobile;//商家电话

    private Integer deliveryDuration;
    private List<RunfastOrderActivity> validActivityList;

    private Integer userId;//客户id（用户）
    private String userName;//客户名称（用户）
    private String userMobile;//用户电话
    private String userPhone;//下单用户电话
    private String userAddress;//送货地址
    private String address;//门牌号
    private Integer userAddressId;//用户地址ID
    private String userAddressLat;// 配送纬度
    private String userAddressLng;// 配送经度
    private Integer userAddressTag; //用户地址标签
    private Integer userAddressGender;//用户地址性别

    private BigDecimal deliveryFee;//原配送费
    private BigDecimal finalDeliveryFee;//最终配送费，去除免配送费活动后的金额

    private String  oldShopper;//原配送员姓名
    private Integer oldShopperId; //原配送员ID
    private String  oldShopperMobile; //原配送员电话
    private Integer agree;//是否同意0发送请求1同意2不同意

    private String shopper;//配送员姓名
    private Integer shopperId; //配送员ID
    private String shopperMobile; //配送员电话

    private Date createTime;//下订单时间

    private Integer status;//订单状态编码-3:商家拒单-1：订单取消  0：客户下单，1：客户已付款  2：商家接单  3：骑手接单   4：商品打包 ，5：商品配送 6：商品送达，7:确认收货 ，8：订单完成

    private String statStr;//订单状态编码 -1：订单取消  0：客户下单，1：客户已付款  2：商家接单  3：骑手接单   4：商品打包 ，5：商品配送 6：商品送达，7:确认收货 ，8：订单完成

    private Integer isReceive;//是否收货； 1：已收货， 其他未收货
    private Integer isPay;//是否支付； 1：已支付， 其他未支付
    private Integer isRefund;//是否退款； 1：全额退款，2部分退款 ， 其他未退款

    private Integer isComent;//是否评价；null：未评价 1：已评价商家， 2：全部评价
    private Integer isCancel;//用户提出取消订单 1:用户提出 2:商家同意取消订单3:不同意取消订单
    private BigDecimal refund;//退款金额
    private String refundcontext;//拒单原因

    private Boolean booked; //是否是预订单
    private Date bookTime;//预定时间
    private Date disTime;//大约送达时间
    private Date payTime;//支付时间
    private Integer payType;// 支付类型:0支付宝;1微信;2钱包

    private String orderNo;
    private Integer orderNumber;//商家订单序号
    /**
     * 用户应付金额
     */
    private BigDecimal totalPay;
    private BigDecimal totalPackageFee;
    private BigDecimal cartPrice;

    /**
     * 购物车优惠后的金额,考虑打折特价活动，其他活动不考虑
     */
    private BigDecimal cartDisprice;

    private BigDecimal offAmount;
    private String cartTips;
    private String limitTips;
    private String redTips;
    private String shareTips;

    private List<CartItem> cartItems;

    private Integer distance;

    private Integer totalNum;

    /**
     * 备注
     */
    private String remark;

    /**
     * 用户通用红包id（代理商红包）
     */
    private Integer userRedId;

    /**
     * 用户代金券（商家红包）
     */
    private Integer userCouponId;

    /**
     * 自取时间
     */
    private Date selfTime;

    /**
     * 自取电话
     */
    private String selfMobile;

    /**
     * 商家是否自取
     */
    private Boolean suportSelf;


    /**
     * 用户选择的取餐方式
     */
    private Boolean userSuportSelf;
}
