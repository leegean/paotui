package com.runfast.waimai.common;

/**
 * @author: lijin
 * @date: 2018年05月14日
 */
public class Constants {
    /**
     * 
     */
    public static final String REDIS_NAMESPACE_CART = "cart:";//购物车
    public static final String REDIS_NAMESPACE_PREORDER = "preorder:";//预订单

    public final static String DEVICE_ID_PREFIX = "deviceId-"; //设备唯一标识前缀
}
