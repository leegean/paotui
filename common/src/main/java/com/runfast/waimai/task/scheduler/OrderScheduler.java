package com.runfast.waimai.task.scheduler;

import com.runfast.waimai.dao.model.RunfastGoodsSellRecordExample;
import com.runfast.waimai.service.RunfastGoodsSellRecordService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

/**
 * @author: lijin
 * @date: 2018年04月16日
 */
@Component("wmOrderScheduler")
@Slf4j
public class OrderScheduler {

    @Autowired
    private RunfastGoodsSellRecordService orderService;



    /**
     * 定时处理订单
     */
    @Scheduled(fixedDelay = 60 * 1000)
    public void handle() {
        log.info("开始外卖订单定时任务");
        // 超时取消新创建的订单
        asyncCancelNewOrder();

        // 超时取消用户已付款的订单
        asyncCancelPaidOrder();


        //超时完成订单（商家已接单，没有申请取消）
        asyncCompleteOrder();


        //超时确认退款申请
        asyncConfirmRefund();


        //超时确认取消申请
        asyncConfirmCancel();

    }

    @Async
    void asyncCancelNewOrder() {
        log.debug("系统取消超过10分钟未付款的新订单");
        Calendar instance = Calendar.getInstance();
        instance.add(Calendar.MINUTE, -10);


        List<Map<String, Integer>> maps = orderService.findCancelNewOrder(instance.getTime());
        for (Map<String, Integer> map : maps) {
            Integer id = map.get("id");
            Integer userId = map.get("userId");


            try{
                orderService.autoCancel(userId, id);
            }catch (Exception e){
                log.error("用户id：{} , 订单id：{}",userId,id, e);

            }
        }

    }


    @Async
    void asyncCancelPaidOrder() {
        log.debug("系统取消超过10分钟未接单的已付款订单");
        Calendar instance = Calendar.getInstance();
        instance.add(Calendar.MINUTE, -10);


        List<Map<String, Integer>> maps = orderService.findCancelPaidOrder(instance.getTime());
        for (Map<String, Integer> map : maps) {
            Integer id = map.get("id");
            Integer userId = map.get("userId");


            try {
                orderService.autoCancel(userId, id);
            }catch (Exception e){
                log.error("用户id：{} , 订单id：{}",userId,id, e);
            }
        }

    }

    @Async
    void asyncCompleteOrder() {
        log.debug("系统完成超过5小时未完成的已接订单");
        /**
         * 超过5小时，自动完成
         */
        Calendar instance = Calendar.getInstance();
        instance.add(Calendar.HOUR, -5);


        List<Map<String, Integer>> maps = orderService.findCompletableOrder(instance.getTime());
        for (Map<String, Integer> map : maps) {
            Integer id = map.get("id");
            Integer userId = map.get("userId");


            try {
                orderService.autoComplete(userId, id);
            }catch (Exception e){
                log.error("用户id：{} , 订单id：{}",userId,id, e);
            }
        }

    }

    @Async
    void asyncConfirmRefund() {
        log.debug("系统确认超过2小时未处理的退款申请订单");
        /**
         * 超过2小时，自动确认退款
         */
        Calendar instance = Calendar.getInstance();
        instance.add(Calendar.HOUR, -2);


        List<Map<String, Integer>> maps = orderService.findConfirmRefundOrder(instance.getTime());
        for (Map<String, Integer> map : maps) {
            Integer id = map.get("id");
            Integer userId = map.get("userId");


            try {
                orderService.confirmRefund(userId, id);
            }catch (Exception e){
                log.error("用户id：{} , 订单id：{}",userId,id, e);
            }
        }

    }


    @Async
    void asyncConfirmCancel() {
        log.debug("系统确认超过2小时未处理的取消申请订单");
        /**
         * 超过2小时，自动确认取消申请
         */
        Calendar instance = Calendar.getInstance();
        instance.add(Calendar.HOUR, -2);


        List<Map<String, Integer>> maps = orderService.findConfirmCancelOrder(instance.getTime());
        for (Map<String, Integer> map : maps) {
            Integer id = map.get("id");
            Integer userId = map.get("userId");


            try {
                orderService.confirmCancel(userId, id);
            }catch (Exception e){
                log.error("用户id：{} , 订单id：{}",userId,id, e);
            }
        }

    }

}
