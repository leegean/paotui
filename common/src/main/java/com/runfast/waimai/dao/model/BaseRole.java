package com.runfast.waimai.dao.model;

import java.io.Serializable;

public class BaseRole implements Serializable {
    private Integer rid;

    private String rname;

    private Integer flag;

    private String orgId;

    private String info;

    private static final long serialVersionUID = 1L;

    public Integer getRid() {
        return rid;
    }

    public BaseRole withRid(Integer rid) {
        this.setRid(rid);
        return this;
    }

    public void setRid(Integer rid) {
        this.rid = rid;
    }

    public String getRname() {
        return rname;
    }

    public BaseRole withRname(String rname) {
        this.setRname(rname);
        return this;
    }

    public void setRname(String rname) {
        this.rname = rname == null ? null : rname.trim();
    }

    public Integer getFlag() {
        return flag;
    }

    public BaseRole withFlag(Integer flag) {
        this.setFlag(flag);
        return this;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public String getOrgId() {
        return orgId;
    }

    public BaseRole withOrgId(String orgId) {
        this.setOrgId(orgId);
        return this;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId == null ? null : orgId.trim();
    }

    public String getInfo() {
        return info;
    }

    public BaseRole withInfo(String info) {
        this.setInfo(info);
        return this;
    }

    public void setInfo(String info) {
        this.info = info == null ? null : info.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", rid=").append(rid);
        sb.append(", rname=").append(rname);
        sb.append(", flag=").append(flag);
        sb.append(", orgId=").append(orgId);
        sb.append(", info=").append(info);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}