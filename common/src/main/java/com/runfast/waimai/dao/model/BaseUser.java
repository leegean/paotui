package com.runfast.waimai.dao.model;

import java.io.Serializable;
import java.util.Date;

public class BaseUser implements Serializable {
    private Integer id;

    private String areaid;

    private Date createtime;

    private String cssStyle;

    private String email;

    private String mobile;

    private Date modifyTime;

    private String orgId;

    private String password;

    private String realname;

    private Integer state;

    private String username;

    private Integer flag;

    private String cityId;

    private String cityName;

    private String countyId;

    private String countyName;

    private String agentids;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public BaseUser withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAreaid() {
        return areaid;
    }

    public BaseUser withAreaid(String areaid) {
        this.setAreaid(areaid);
        return this;
    }

    public void setAreaid(String areaid) {
        this.areaid = areaid == null ? null : areaid.trim();
    }

    public Date getCreatetime() {
        return createtime;
    }

    public BaseUser withCreatetime(Date createtime) {
        this.setCreatetime(createtime);
        return this;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public String getCssStyle() {
        return cssStyle;
    }

    public BaseUser withCssStyle(String cssStyle) {
        this.setCssStyle(cssStyle);
        return this;
    }

    public void setCssStyle(String cssStyle) {
        this.cssStyle = cssStyle == null ? null : cssStyle.trim();
    }

    public String getEmail() {
        return email;
    }

    public BaseUser withEmail(String email) {
        this.setEmail(email);
        return this;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getMobile() {
        return mobile;
    }

    public BaseUser withMobile(String mobile) {
        this.setMobile(mobile);
        return this;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public BaseUser withModifyTime(Date modifyTime) {
        this.setModifyTime(modifyTime);
        return this;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getOrgId() {
        return orgId;
    }

    public BaseUser withOrgId(String orgId) {
        this.setOrgId(orgId);
        return this;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId == null ? null : orgId.trim();
    }

    public String getPassword() {
        return password;
    }

    public BaseUser withPassword(String password) {
        this.setPassword(password);
        return this;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getRealname() {
        return realname;
    }

    public BaseUser withRealname(String realname) {
        this.setRealname(realname);
        return this;
    }

    public void setRealname(String realname) {
        this.realname = realname == null ? null : realname.trim();
    }

    public Integer getState() {
        return state;
    }

    public BaseUser withState(Integer state) {
        this.setState(state);
        return this;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getUsername() {
        return username;
    }

    public BaseUser withUsername(String username) {
        this.setUsername(username);
        return this;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public Integer getFlag() {
        return flag;
    }

    public BaseUser withFlag(Integer flag) {
        this.setFlag(flag);
        return this;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public String getCityId() {
        return cityId;
    }

    public BaseUser withCityId(String cityId) {
        this.setCityId(cityId);
        return this;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId == null ? null : cityId.trim();
    }

    public String getCityName() {
        return cityName;
    }

    public BaseUser withCityName(String cityName) {
        this.setCityName(cityName);
        return this;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName == null ? null : cityName.trim();
    }

    public String getCountyId() {
        return countyId;
    }

    public BaseUser withCountyId(String countyId) {
        this.setCountyId(countyId);
        return this;
    }

    public void setCountyId(String countyId) {
        this.countyId = countyId == null ? null : countyId.trim();
    }

    public String getCountyName() {
        return countyName;
    }

    public BaseUser withCountyName(String countyName) {
        this.setCountyName(countyName);
        return this;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName == null ? null : countyName.trim();
    }

    public String getAgentids() {
        return agentids;
    }

    public BaseUser withAgentids(String agentids) {
        this.setAgentids(agentids);
        return this;
    }

    public void setAgentids(String agentids) {
        this.agentids = agentids == null ? null : agentids.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", areaid=").append(areaid);
        sb.append(", createtime=").append(createtime);
        sb.append(", cssStyle=").append(cssStyle);
        sb.append(", email=").append(email);
        sb.append(", mobile=").append(mobile);
        sb.append(", modifyTime=").append(modifyTime);
        sb.append(", orgId=").append(orgId);
        sb.append(", password=").append(password);
        sb.append(", realname=").append(realname);
        sb.append(", state=").append(state);
        sb.append(", username=").append(username);
        sb.append(", flag=").append(flag);
        sb.append(", cityId=").append(cityId);
        sb.append(", cityName=").append(cityName);
        sb.append(", countyId=").append(countyId);
        sb.append(", countyName=").append(countyName);
        sb.append(", agentids=").append(agentids);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}