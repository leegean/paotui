package com.runfast.waimai.dao.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
public class RunfastGoodsSellType implements Serializable {
    private Integer id;

    private Integer businessId;

    private String businessName;

    private String content;

    private Date createTime;

    private String imgPath;

    private String name;

    private Integer agentId;

    private String agentName;

    private Integer sort;

    private Integer deleted;

    private List<RunfastGoodsSell> goodsSellList;

    private static final long serialVersionUID = 1L;
}