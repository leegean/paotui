package com.runfast.waimai.dao.model;

import com.runfast.waimai.web.dto.GoodsStandarDto;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class RunfastGoodsSell implements Serializable {
    private Integer id;

    private Integer businessId;

    private String businessName;

    private String content;

    private Date createTime;

    private Double discount;

    /**
     * mini_num
     */
    private String imgPath;

    private String name;

    private Double price;

    private Integer sellTypeId;

    private String sellTypeName;

    private Integer status;

    private Integer typeId;

    private String typeName;

    private Integer salesnum;

    private Integer num;

    private Integer star;

    private Integer ptype;

    private Integer agentId;

    private String agentName;

    private String mini_imgPath;

    private Integer islimited;

    private Date limiEndTime;

    private Date limiStartTime;

    private Integer limitNum;

    private Integer limittype;

    private Integer deleted;

    private String tags;

    private String unit;

    /**
     * 子标题
     */
    private String subTitle;

    /**
     * 是否必选商品
     */
    private Boolean required;

    /**
     * 最低购买数量
     */
    private Integer min_num;

    private List<GoodsStandarDto> goodsSellStandardList = new ArrayList<>();

    private List<RunfastGoodsSellOption> goodsSellOptionList= new ArrayList<>();

    private static final long serialVersionUID = 1L;
}