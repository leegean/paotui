package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastAgentMgrHasRunfastAgentbusiness;
import com.runfast.waimai.dao.model.RunfastAgentMgrHasRunfastAgentbusinessExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastAgentMgrHasRunfastAgentbusinessMapper extends IMapper<RunfastAgentMgrHasRunfastAgentbusiness, Integer, RunfastAgentMgrHasRunfastAgentbusinessExample> {
}