package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastGoodsSellChildren;
import com.runfast.waimai.dao.model.RunfastGoodsSellChildrenExample;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface RunfastGoodsSellChildrenMapper extends IMapper<RunfastGoodsSellChildren, Integer, RunfastGoodsSellChildrenExample> {
    List<RunfastGoodsSellChildren> findGroupByOrderId(RunfastGoodsSellChildrenExample orderItemExample);
}