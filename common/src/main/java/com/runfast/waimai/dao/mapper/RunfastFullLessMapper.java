package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastFullLess;
import com.runfast.waimai.dao.model.RunfastFullLessExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastFullLessMapper extends IMapper<RunfastFullLess, Integer, RunfastFullLessExample> {
}