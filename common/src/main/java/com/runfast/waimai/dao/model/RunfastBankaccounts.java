package com.runfast.waimai.dao.model;

import com.runfast.common.entity.validate.group.Create;
import com.runfast.common.entity.validate.group.Update;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

public class RunfastBankaccounts implements Serializable {
    @NotBlank(groups = {Update.class})
    private Integer id;

    @NotBlank(groups = {Create.class, Update.class})
    private String account;

    @NotBlank(groups = {Create.class, Update.class})
    private String banktype;

    private Date createTime;

    private String createbank;
    @NotBlank(groups = {Create.class, Update.class})
    private String name;

    private Integer type;

    private Integer userId;

    private String userMobile;

    private String userName;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public RunfastBankaccounts withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public RunfastBankaccounts withAccount(String account) {
        this.setAccount(account);
        return this;
    }

    public void setAccount(String account) {
        this.account = account == null ? null : account.trim();
    }

    public String getBanktype() {
        return banktype;
    }

    public RunfastBankaccounts withBanktype(String banktype) {
        this.setBanktype(banktype);
        return this;
    }

    public void setBanktype(String banktype) {
        this.banktype = banktype == null ? null : banktype.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public RunfastBankaccounts withCreateTime(Date createTime) {
        this.setCreateTime(createTime);
        return this;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreatebank() {
        return createbank;
    }

    public RunfastBankaccounts withCreatebank(String createbank) {
        this.setCreatebank(createbank);
        return this;
    }

    public void setCreatebank(String createbank) {
        this.createbank = createbank == null ? null : createbank.trim();
    }

    public String getName() {
        return name;
    }

    public RunfastBankaccounts withName(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getType() {
        return type;
    }

    public RunfastBankaccounts withType(Integer type) {
        this.setType(type);
        return this;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getUserId() {
        return userId;
    }

    public RunfastBankaccounts withUserId(Integer userId) {
        this.setUserId(userId);
        return this;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserMobile() {
        return userMobile;
    }

    public RunfastBankaccounts withUserMobile(String userMobile) {
        this.setUserMobile(userMobile);
        return this;
    }

    public void setUserMobile(String userMobile) {
        this.userMobile = userMobile == null ? null : userMobile.trim();
    }

    public String getUserName() {
        return userName;
    }

    public RunfastBankaccounts withUserName(String userName) {
        this.setUserName(userName);
        return this;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", account=").append(account);
        sb.append(", banktype=").append(banktype);
        sb.append(", createTime=").append(createTime);
        sb.append(", createbank=").append(createbank);
        sb.append(", name=").append(name);
        sb.append(", type=").append(type);
        sb.append(", userId=").append(userId);
        sb.append(", userMobile=").append(userMobile);
        sb.append(", userName=").append(userName);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}