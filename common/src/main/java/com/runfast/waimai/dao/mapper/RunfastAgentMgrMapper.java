package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastAgentMgr;
import com.runfast.waimai.dao.model.RunfastAgentMgrExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastAgentMgrMapper extends IMapper<RunfastAgentMgr, Integer, RunfastAgentMgrExample> {
}