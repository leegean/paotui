package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastGoodsSellOption;
import com.runfast.waimai.dao.model.RunfastGoodsSellOptionExample;
import com.runfast.waimai.entity.OptionIdPair;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface RunfastGoodsSellOptionMapper extends IMapper<RunfastGoodsSellOption, Integer, RunfastGoodsSellOptionExample> {

    List<RunfastGoodsSellOption> findOptionWithSub(Integer goodsId);

}