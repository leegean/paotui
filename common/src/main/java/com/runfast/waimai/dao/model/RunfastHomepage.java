package com.runfast.waimai.dao.model;

import java.io.Serializable;
import java.util.Date;

public class RunfastHomepage implements Serializable {
    private Integer id;

    private Date createTime;

    private String icon;

    private String link;

    private String name;

    private Integer sort;

    private Integer agentId;

    private String agentName;

    private Integer typelink;

    private String typename;

    private Integer will;

    /**
     * 广告分类里对应具体商家，商家分类等id
     */
    private Integer commonId;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public RunfastHomepage withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public RunfastHomepage withCreateTime(Date createTime) {
        this.setCreateTime(createTime);
        return this;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getIcon() {
        return icon;
    }

    public RunfastHomepage withIcon(String icon) {
        this.setIcon(icon);
        return this;
    }

    public void setIcon(String icon) {
        this.icon = icon == null ? null : icon.trim();
    }

    public String getLink() {
        return link;
    }

    public RunfastHomepage withLink(String link) {
        this.setLink(link);
        return this;
    }

    public void setLink(String link) {
        this.link = link == null ? null : link.trim();
    }

    public String getName() {
        return name;
    }

    public RunfastHomepage withName(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getSort() {
        return sort;
    }

    public RunfastHomepage withSort(Integer sort) {
        this.setSort(sort);
        return this;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public RunfastHomepage withAgentId(Integer agentId) {
        this.setAgentId(agentId);
        return this;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public String getAgentName() {
        return agentName;
    }

    public RunfastHomepage withAgentName(String agentName) {
        this.setAgentName(agentName);
        return this;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName == null ? null : agentName.trim();
    }

    public Integer getTypelink() {
        return typelink;
    }

    public RunfastHomepage withTypelink(Integer typelink) {
        this.setTypelink(typelink);
        return this;
    }

    public void setTypelink(Integer typelink) {
        this.typelink = typelink;
    }

    public String getTypename() {
        return typename;
    }

    public RunfastHomepage withTypename(String typename) {
        this.setTypename(typename);
        return this;
    }

    public void setTypename(String typename) {
        this.typename = typename == null ? null : typename.trim();
    }

    public Integer getWill() {
        return will;
    }

    public RunfastHomepage withWill(Integer will) {
        this.setWill(will);
        return this;
    }

    public void setWill(Integer will) {
        this.will = will;
    }

    public Integer getCommonId() {
        return commonId;
    }

    public RunfastHomepage withCommonId(Integer commonId) {
        this.setCommonId(commonId);
        return this;
    }

    public void setCommonId(Integer commonId) {
        this.commonId = commonId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", createTime=").append(createTime);
        sb.append(", icon=").append(icon);
        sb.append(", link=").append(link);
        sb.append(", name=").append(name);
        sb.append(", sort=").append(sort);
        sb.append(", agentId=").append(agentId);
        sb.append(", agentName=").append(agentName);
        sb.append(", typelink=").append(typelink);
        sb.append(", typename=").append(typename);
        sb.append(", will=").append(will);
        sb.append(", commonId=").append(commonId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}