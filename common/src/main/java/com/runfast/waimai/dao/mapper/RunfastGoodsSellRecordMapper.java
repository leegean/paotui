package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastGoodsSellRecord;
import com.runfast.waimai.dao.model.RunfastGoodsSellRecordExample;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Mapper
public interface RunfastGoodsSellRecordMapper extends IMapper<RunfastGoodsSellRecord, Integer, RunfastGoodsSellRecordExample> {
    Map<String,Object> findLastestOrder(Integer agentId, Integer userId);


    List<Map<String,Integer>> findCompletableOrder(Date time);

    List<Map<String,Integer>> findConfirmRefundOrder(Date time);

    List<Map<String,Integer>> findConfirmCancelOrder(Date time);

    List<Map<String,Integer>> findCancelNewOrder(Date time);
    List<Map<String,Integer>> findCancelPaidOrder(Date time);

    Integer resetIsComent(int orderId);
}