package com.runfast.waimai.dao.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class RunfastDistributionassess implements Serializable {
    private Integer id;

    private Date assessTime;

    private String assessman;

    private BigDecimal consume;

    private Date distributionTime;

    private Integer level;

    private String orderNumber;

    private Integer assessId;

    private Integer goodsSellRecordId;

    private Integer grade;

    private String orderCode;

    private Double price;

    private Double score;

    private Integer shopperId;

    private String shopperName;

    private Date shopperTime;

    private Integer userId;

    private String userName;

    private String content;

    private Integer agentId;

    private String agentName;

    private String qishoustr;

    private String recontent;

    private Date recreateTime;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public RunfastDistributionassess withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getAssessTime() {
        return assessTime;
    }

    public RunfastDistributionassess withAssessTime(Date assessTime) {
        this.setAssessTime(assessTime);
        return this;
    }

    public void setAssessTime(Date assessTime) {
        this.assessTime = assessTime;
    }

    public String getAssessman() {
        return assessman;
    }

    public RunfastDistributionassess withAssessman(String assessman) {
        this.setAssessman(assessman);
        return this;
    }

    public void setAssessman(String assessman) {
        this.assessman = assessman == null ? null : assessman.trim();
    }

    public BigDecimal getConsume() {
        return consume;
    }

    public RunfastDistributionassess withConsume(BigDecimal consume) {
        this.setConsume(consume);
        return this;
    }

    public void setConsume(BigDecimal consume) {
        this.consume = consume;
    }

    public Date getDistributionTime() {
        return distributionTime;
    }

    public RunfastDistributionassess withDistributionTime(Date distributionTime) {
        this.setDistributionTime(distributionTime);
        return this;
    }

    public void setDistributionTime(Date distributionTime) {
        this.distributionTime = distributionTime;
    }

    public Integer getLevel() {
        return level;
    }

    public RunfastDistributionassess withLevel(Integer level) {
        this.setLevel(level);
        return this;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public RunfastDistributionassess withOrderNumber(String orderNumber) {
        this.setOrderNumber(orderNumber);
        return this;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber == null ? null : orderNumber.trim();
    }

    public Integer getAssessId() {
        return assessId;
    }

    public RunfastDistributionassess withAssessId(Integer assessId) {
        this.setAssessId(assessId);
        return this;
    }

    public void setAssessId(Integer assessId) {
        this.assessId = assessId;
    }

    public Integer getGoodsSellRecordId() {
        return goodsSellRecordId;
    }

    public RunfastDistributionassess withGoodsSellRecordId(Integer goodsSellRecordId) {
        this.setGoodsSellRecordId(goodsSellRecordId);
        return this;
    }

    public void setGoodsSellRecordId(Integer goodsSellRecordId) {
        this.goodsSellRecordId = goodsSellRecordId;
    }

    public Integer getGrade() {
        return grade;
    }

    public RunfastDistributionassess withGrade(Integer grade) {
        this.setGrade(grade);
        return this;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public RunfastDistributionassess withOrderCode(String orderCode) {
        this.setOrderCode(orderCode);
        return this;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode == null ? null : orderCode.trim();
    }

    public Double getPrice() {
        return price;
    }

    public RunfastDistributionassess withPrice(Double price) {
        this.setPrice(price);
        return this;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getScore() {
        return score;
    }

    public RunfastDistributionassess withScore(Double score) {
        this.setScore(score);
        return this;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public Integer getShopperId() {
        return shopperId;
    }

    public RunfastDistributionassess withShopperId(Integer shopperId) {
        this.setShopperId(shopperId);
        return this;
    }

    public void setShopperId(Integer shopperId) {
        this.shopperId = shopperId;
    }

    public String getShopperName() {
        return shopperName;
    }

    public RunfastDistributionassess withShopperName(String shopperName) {
        this.setShopperName(shopperName);
        return this;
    }

    public void setShopperName(String shopperName) {
        this.shopperName = shopperName == null ? null : shopperName.trim();
    }

    public Date getShopperTime() {
        return shopperTime;
    }

    public RunfastDistributionassess withShopperTime(Date shopperTime) {
        this.setShopperTime(shopperTime);
        return this;
    }

    public void setShopperTime(Date shopperTime) {
        this.shopperTime = shopperTime;
    }

    public Integer getUserId() {
        return userId;
    }

    public RunfastDistributionassess withUserId(Integer userId) {
        this.setUserId(userId);
        return this;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public RunfastDistributionassess withUserName(String userName) {
        this.setUserName(userName);
        return this;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getContent() {
        return content;
    }

    public RunfastDistributionassess withContent(String content) {
        this.setContent(content);
        return this;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public Integer getAgentId() {
        return agentId;
    }

    public RunfastDistributionassess withAgentId(Integer agentId) {
        this.setAgentId(agentId);
        return this;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public String getAgentName() {
        return agentName;
    }

    public RunfastDistributionassess withAgentName(String agentName) {
        this.setAgentName(agentName);
        return this;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName == null ? null : agentName.trim();
    }

    public String getQishoustr() {
        return qishoustr;
    }

    public RunfastDistributionassess withQishoustr(String qishoustr) {
        this.setQishoustr(qishoustr);
        return this;
    }

    public void setQishoustr(String qishoustr) {
        this.qishoustr = qishoustr == null ? null : qishoustr.trim();
    }

    public String getRecontent() {
        return recontent;
    }

    public RunfastDistributionassess withRecontent(String recontent) {
        this.setRecontent(recontent);
        return this;
    }

    public void setRecontent(String recontent) {
        this.recontent = recontent == null ? null : recontent.trim();
    }

    public Date getRecreateTime() {
        return recreateTime;
    }

    public RunfastDistributionassess withRecreateTime(Date recreateTime) {
        this.setRecreateTime(recreateTime);
        return this;
    }

    public void setRecreateTime(Date recreateTime) {
        this.recreateTime = recreateTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", assessTime=").append(assessTime);
        sb.append(", assessman=").append(assessman);
        sb.append(", consume=").append(consume);
        sb.append(", distributionTime=").append(distributionTime);
        sb.append(", level=").append(level);
        sb.append(", orderNumber=").append(orderNumber);
        sb.append(", assessId=").append(assessId);
        sb.append(", goodsSellRecordId=").append(goodsSellRecordId);
        sb.append(", grade=").append(grade);
        sb.append(", orderCode=").append(orderCode);
        sb.append(", price=").append(price);
        sb.append(", score=").append(score);
        sb.append(", shopperId=").append(shopperId);
        sb.append(", shopperName=").append(shopperName);
        sb.append(", shopperTime=").append(shopperTime);
        sb.append(", userId=").append(userId);
        sb.append(", userName=").append(userName);
        sb.append(", content=").append(content);
        sb.append(", agentId=").append(agentId);
        sb.append(", agentName=").append(agentName);
        sb.append(", qishoustr=").append(qishoustr);
        sb.append(", recontent=").append(recontent);
        sb.append(", recreateTime=").append(recreateTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}