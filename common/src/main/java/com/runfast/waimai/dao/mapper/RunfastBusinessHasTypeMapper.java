package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastBusinessHasType;
import com.runfast.waimai.dao.model.RunfastBusinessHasTypeExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastBusinessHasTypeMapper extends IMapper<RunfastBusinessHasType, Integer, RunfastBusinessHasTypeExample> {
}