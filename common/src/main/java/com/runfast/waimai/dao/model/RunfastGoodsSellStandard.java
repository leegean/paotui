package com.runfast.waimai.dao.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class RunfastGoodsSellStandard implements Serializable {
    private Integer id;

    private String barCode;

    private Double discount;

    private Integer goodsSellId;

    private String name;

    private Double price;

    private String proCode;

    private Integer businessId;

    private String businessName;

    /**
     * 剩余库存
     */
    private Integer num;

    /**
     * 月销售数量
     */
    private Integer sale_num;

    /**
     * 规格排序
     */
    private Integer sort;

    /**
     * 餐盒费
     */
    private Double package_fee;

    /**
     * 特价
     */
    private Double disprice;

    /**
     * 是否删除
     */
    private Boolean deleted;

    private static final long serialVersionUID = 1L;

}