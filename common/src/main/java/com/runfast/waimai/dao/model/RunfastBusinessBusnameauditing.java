package com.runfast.waimai.dao.model;

import java.io.Serializable;
import java.util.Date;

public class RunfastBusinessBusnameauditing implements Serializable {
    private Integer id;

    private Date audTime;

    private Integer auditing;

    private Integer busId;

    private Integer busaccId;

    private Date createTime;

    private String name;

    private String oldname;

    private Integer agentId;

    private String agentName;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public RunfastBusinessBusnameauditing withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getAudTime() {
        return audTime;
    }

    public RunfastBusinessBusnameauditing withAudTime(Date audTime) {
        this.setAudTime(audTime);
        return this;
    }

    public void setAudTime(Date audTime) {
        this.audTime = audTime;
    }

    public Integer getAuditing() {
        return auditing;
    }

    public RunfastBusinessBusnameauditing withAuditing(Integer auditing) {
        this.setAuditing(auditing);
        return this;
    }

    public void setAuditing(Integer auditing) {
        this.auditing = auditing;
    }

    public Integer getBusId() {
        return busId;
    }

    public RunfastBusinessBusnameauditing withBusId(Integer busId) {
        this.setBusId(busId);
        return this;
    }

    public void setBusId(Integer busId) {
        this.busId = busId;
    }

    public Integer getBusaccId() {
        return busaccId;
    }

    public RunfastBusinessBusnameauditing withBusaccId(Integer busaccId) {
        this.setBusaccId(busaccId);
        return this;
    }

    public void setBusaccId(Integer busaccId) {
        this.busaccId = busaccId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public RunfastBusinessBusnameauditing withCreateTime(Date createTime) {
        this.setCreateTime(createTime);
        return this;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getName() {
        return name;
    }

    public RunfastBusinessBusnameauditing withName(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getOldname() {
        return oldname;
    }

    public RunfastBusinessBusnameauditing withOldname(String oldname) {
        this.setOldname(oldname);
        return this;
    }

    public void setOldname(String oldname) {
        this.oldname = oldname == null ? null : oldname.trim();
    }

    public Integer getAgentId() {
        return agentId;
    }

    public RunfastBusinessBusnameauditing withAgentId(Integer agentId) {
        this.setAgentId(agentId);
        return this;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public String getAgentName() {
        return agentName;
    }

    public RunfastBusinessBusnameauditing withAgentName(String agentName) {
        this.setAgentName(agentName);
        return this;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName == null ? null : agentName.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", audTime=").append(audTime);
        sb.append(", auditing=").append(auditing);
        sb.append(", busId=").append(busId);
        sb.append(", busaccId=").append(busaccId);
        sb.append(", createTime=").append(createTime);
        sb.append(", name=").append(name);
        sb.append(", oldname=").append(oldname);
        sb.append(", agentId=").append(agentId);
        sb.append(", agentName=").append(agentName);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}