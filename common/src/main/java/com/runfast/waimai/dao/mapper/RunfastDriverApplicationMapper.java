package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastDriverApplication;
import com.runfast.waimai.dao.model.RunfastDriverApplicationExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastDriverApplicationMapper extends IMapper<RunfastDriverApplication, Integer, RunfastDriverApplicationExample> {
}