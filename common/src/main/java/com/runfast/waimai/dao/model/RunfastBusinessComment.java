package com.runfast.waimai.dao.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;


@Data
public class RunfastBusinessComment implements Serializable {
    private Integer id;

    private Integer businessId;

    private String businessName;

    private Double cost;

    private Date createTime;

    private Integer delicerId;

    private String delicerName;

    private Date delicerTime;

    private Date orderTime;

    private Double score;

    private Integer userId;

    private String userName;

    private Integer goodsSellId;

    private String goodsSellName;

    private String orderCode;

    private String pic;

    private Integer goodsSellRecordId;

    private Integer agentId;

    private String agentName;

    private String shangstr;

    private Date feedTime;

    private String recontent;

    private Date recreateTime;

    /**
     * 是否删除
     */
    private Boolean deleted;

    /**
     * 骑手得分
     */
    private Double delicerScore;

    private String content;

    private String feedback;

    /**
     * 口味得分
     */
    private Integer tasteScore;

    /**
     * 包装得分
     */
    private Integer packagesScore;

    /**
     * 匿名评价
     */
    private Boolean anonymous;

    /**
     * 是否对骑手满意
     */
    private Boolean driverSatisfy;

    private static final long serialVersionUID = 1L;

}