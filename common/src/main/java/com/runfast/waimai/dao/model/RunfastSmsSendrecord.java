package com.runfast.waimai.dao.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class RunfastSmsSendrecord implements Serializable {
    private Integer id;

    private String ipAddr;

    private String mession;

    private String msg;

    private String phone;

    private Date sendTime;

    private Integer smstype;

    private String xcode;

    private static final long serialVersionUID = 1L;


    public enum SmsType {
        sms_register(1),sms_pwd_update(2),sms_pwd_send(3),sms_login(4),sms_bind(5),sms_join(6);
        private int code;

        SmsType(int code) {
            this.code = code;
        }

        public int getCode() {
            return code;
        }
    }
}