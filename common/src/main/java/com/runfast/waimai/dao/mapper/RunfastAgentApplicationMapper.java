package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastAgentApplication;
import com.runfast.waimai.dao.model.RunfastAgentApplicationExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastAgentApplicationMapper extends IMapper<RunfastAgentApplication, Integer, RunfastAgentApplicationExample> {
}