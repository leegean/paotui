package com.runfast.waimai.dao.model;

import com.runfast.common.entity.validate.group.Create;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

public class RunfastAgentMgr implements Serializable {
    private Integer id;

    private String username;

    private String password;

    /**
     * 员工姓名
     */
    @NotBlank(groups = {Create.class})
    private String name;

    @NotBlank(groups = {Create.class})
    private String mobile;

    private Integer roleId;

    private String roleName;

    private Integer createBy;

    private Date createTime;

    private Integer updateBy;

    private Date updateTime;

    private Integer status;

    /**
     * 公司名称
     */
    @NotBlank(groups = {Create.class})
    private String company;

    /**
     * 公司成立时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotBlank(groups = {Create.class})
    private Date setupTime;

    /**
     * 公司地址
     */
    @NotBlank(groups = {Create.class})
    private String address;

    /**
     * 公司规模
     */
    @NotBlank(groups = {Create.class})
    private String scale;

    /**
     * 可投入资金
     */
    @NotBlank(groups = {Create.class})
    private String investAmount;

    /**
     * 主营业务
     */
    @NotBlank(groups = {Create.class})
    private String mainBusiness;

    /**
     * 相关行业经验
     */
    @NotBlank(groups = {Create.class})
    private String experience;

    /**
     * 了解当地餐饮市场
     */
    @NotBlank(groups = {Create.class})
    private String knowFoodMarket;

    /**
     * 市场资源
     */
    @NotBlank(groups = {Create.class})
    private String marketResource;
    @NotBlank(groups = {Create.class})
    private String email;

    /**
     * 负责人身份证号
     */
    @NotBlank(groups = {Create.class})
    private String idNumber;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public RunfastAgentMgr withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public RunfastAgentMgr withUsername(String username) {
        this.setUsername(username);
        return this;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public String getPassword() {
        return password;
    }

    public RunfastAgentMgr withPassword(String password) {
        this.setPassword(password);
        return this;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getName() {
        return name;
    }

    public RunfastAgentMgr withName(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getMobile() {
        return mobile;
    }

    public RunfastAgentMgr withMobile(String mobile) {
        this.setMobile(mobile);
        return this;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public Integer getRoleId() {
        return roleId;
    }

    public RunfastAgentMgr withRoleId(Integer roleId) {
        this.setRoleId(roleId);
        return this;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public RunfastAgentMgr withRoleName(String roleName) {
        this.setRoleName(roleName);
        return this;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName == null ? null : roleName.trim();
    }

    public Integer getCreateBy() {
        return createBy;
    }

    public RunfastAgentMgr withCreateBy(Integer createBy) {
        this.setCreateBy(createBy);
        return this;
    }

    public void setCreateBy(Integer createBy) {
        this.createBy = createBy;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public RunfastAgentMgr withCreateTime(Date createTime) {
        this.setCreateTime(createTime);
        return this;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getUpdateBy() {
        return updateBy;
    }

    public RunfastAgentMgr withUpdateBy(Integer updateBy) {
        this.setUpdateBy(updateBy);
        return this;
    }

    public void setUpdateBy(Integer updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public RunfastAgentMgr withUpdateTime(Date updateTime) {
        this.setUpdateTime(updateTime);
        return this;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getStatus() {
        return status;
    }

    public RunfastAgentMgr withStatus(Integer status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCompany() {
        return company;
    }

    public RunfastAgentMgr withCompany(String company) {
        this.setCompany(company);
        return this;
    }

    public void setCompany(String company) {
        this.company = company == null ? null : company.trim();
    }

    public Date getSetupTime() {
        return setupTime;
    }

    public RunfastAgentMgr withSetupTime(Date setupTime) {
        this.setSetupTime(setupTime);
        return this;
    }

    public void setSetupTime(Date setupTime) {
        this.setupTime = setupTime;
    }

    public String getAddress() {
        return address;
    }

    public RunfastAgentMgr withAddress(String address) {
        this.setAddress(address);
        return this;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getScale() {
        return scale;
    }

    public RunfastAgentMgr withScale(String scale) {
        this.setScale(scale);
        return this;
    }

    public void setScale(String scale) {
        this.scale = scale == null ? null : scale.trim();
    }

    public String getInvestAmount() {
        return investAmount;
    }

    public RunfastAgentMgr withInvestAmount(String investAmount) {
        this.setInvestAmount(investAmount);
        return this;
    }

    public void setInvestAmount(String investAmount) {
        this.investAmount = investAmount == null ? null : investAmount.trim();
    }

    public String getMainBusiness() {
        return mainBusiness;
    }

    public RunfastAgentMgr withMainBusiness(String mainBusiness) {
        this.setMainBusiness(mainBusiness);
        return this;
    }

    public void setMainBusiness(String mainBusiness) {
        this.mainBusiness = mainBusiness == null ? null : mainBusiness.trim();
    }

    public String getExperience() {
        return experience;
    }

    public RunfastAgentMgr withExperience(String experience) {
        this.setExperience(experience);
        return this;
    }

    public void setExperience(String experience) {
        this.experience = experience == null ? null : experience.trim();
    }

    public String getKnowFoodMarket() {
        return knowFoodMarket;
    }

    public RunfastAgentMgr withKnowFoodMarket(String knowFoodMarket) {
        this.setKnowFoodMarket(knowFoodMarket);
        return this;
    }

    public void setKnowFoodMarket(String knowFoodMarket) {
        this.knowFoodMarket = knowFoodMarket == null ? null : knowFoodMarket.trim();
    }

    public String getMarketResource() {
        return marketResource;
    }

    public RunfastAgentMgr withMarketResource(String marketResource) {
        this.setMarketResource(marketResource);
        return this;
    }

    public void setMarketResource(String marketResource) {
        this.marketResource = marketResource == null ? null : marketResource.trim();
    }

    public String getEmail() {
        return email;
    }

    public RunfastAgentMgr withEmail(String email) {
        this.setEmail(email);
        return this;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getIdNumber() {
        return idNumber;
    }

    public RunfastAgentMgr withIdNumber(String idNumber) {
        this.setIdNumber(idNumber);
        return this;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber == null ? null : idNumber.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", username=").append(username);
        sb.append(", password=").append(password);
        sb.append(", name=").append(name);
        sb.append(", mobile=").append(mobile);
        sb.append(", roleId=").append(roleId);
        sb.append(", roleName=").append(roleName);
        sb.append(", createBy=").append(createBy);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateBy=").append(updateBy);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", status=").append(status);
        sb.append(", company=").append(company);
        sb.append(", setupTime=").append(setupTime);
        sb.append(", address=").append(address);
        sb.append(", scale=").append(scale);
        sb.append(", investAmount=").append(investAmount);
        sb.append(", mainBusiness=").append(mainBusiness);
        sb.append(", experience=").append(experience);
        sb.append(", knowFoodMarket=").append(knowFoodMarket);
        sb.append(", marketResource=").append(marketResource);
        sb.append(", email=").append(email);
        sb.append(", idNumber=").append(idNumber);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}