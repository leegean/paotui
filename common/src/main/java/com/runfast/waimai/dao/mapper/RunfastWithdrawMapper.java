package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastWithdraw;
import com.runfast.waimai.dao.model.RunfastWithdrawExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastWithdrawMapper extends IMapper<RunfastWithdraw, Integer, RunfastWithdrawExample> {
}