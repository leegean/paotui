package com.runfast.waimai.dao.model;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class RunfastGoodsSellOption implements Serializable {
    private Integer id;

    private String barCode;

    private Integer businessId;

    private String businessName;

    private Integer goodsSellId;

    private String goodsSellName;

    private String name;

    /**
     * 是否删除
     */
    private Boolean deleted;

    private List<RunfastGoodsSellSubOption> subOptionList = new ArrayList<>();

    private static final long serialVersionUID = 1L;

}