package com.runfast.waimai.dao.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class RunfastOrderActivity implements Serializable {
    private Integer id;

    private Integer activityId;

    private Integer activityType;

    private Boolean shared;

    private Double less;

    private Integer agentId;

    private Integer businessId;

    private Integer goodsId;

    private Integer standarId;

    private Integer orderId;

    private static final long serialVersionUID = 1L;

    private Double subsidy;
}