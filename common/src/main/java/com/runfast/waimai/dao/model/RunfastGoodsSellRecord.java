package com.runfast.waimai.dao.model;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class RunfastGoodsSellRecord implements Serializable {
    public static enum Status{
//        订单状态编码-3:商家拒单-1：订单取消  0：客户下单，1：客户已付款  2：商家接单  3：骑手接单   4：商品打包 ，5：商品配送 6：商品送达，7:确认收货 ，8：订单完成
        canceled(-1),
        created(0),
        paid(1),
        business_taken(2),
        driver_taken(3),
        packaged(4),
        on_the_way(5),
        arrived(6),
        confirmed(7),
        completed(8),
        exception(9);




        private int code;
        Status(int code) {
            this.code = code;
        }

        public int getCode() {
            return code;
        }
    }
    private Integer id;

    private Integer businessId;

    private String businessName;

    private String content;

    private Date createTime;

    private Integer goodsSellId;

    private String goodsSellName;

    private String orderCode;

    private BigDecimal price;

    private Integer status;

    private String userAddress;

    private Integer userId;

    private String userMobile;

    private String userName;

    private String shopper;

    private Integer shopperId;

    private Integer goodsTotal;

    private Integer userAddressId;

    private Double yhprice;

    private BigDecimal packing;

    private BigDecimal showps;

    private String statStr;

    private Date endDate;

    private Integer rid;

    private Date startDate;

    private BigDecimal totalpay;

    private String distance;

    private Integer iswithdraw;

    private String address;

    private Integer businessDel;

    private Integer isPay;

    /**
     * 1:用户提出 2:商家同意3:不同意；4：系统超时确认
     */
    private Integer isRefund;

    private Integer userDel;

    private Integer isComent;

    private Integer isReceive;

    /**
     * 1:用户提出 2:商家同意取消订单3:不同意取消订单；4：系统超时确认
     */
    private Integer isCancel;

    private BigDecimal commisson;

    private BigDecimal shopperMoney;

    private String businessAddressLat;

    private String businessAddressLng;

    private String userAddressLat;

    private String userAddressLng;

    private String businessAddr;

    private BigDecimal businesspay;

    private BigDecimal refund;

    private Integer isDeliver;

    private String qrcode;

    private String refundcontext;

    private BigDecimal businessget;

    private String cityId;

    private String cityName;

    private String countyId;

    private String countyName;

    private String townId;

    private String townName;

    private Integer agentId;

    private String agentName;

    private String businessMobile;

    private Double noCharge;

    private String shopperMobile;

    private Integer shopperSign;

    private Integer activityId;

    private String activityname;

    private BigDecimal activityprice;

    private BigDecimal disprice;

    private Integer distributionTime;

    private String oldShopper;

    private Integer oldShopperId;

    private String oldShopperMobile;

    private BigDecimal agentget;

    private Date aceptTime;

    private Date payTime;

    private Integer ismute;

    private Integer isClearing;

    /**
     * 申请退款时间
     */
    private Date refundTime;

    private Integer refundType;

    private BigDecimal agentBusget;

    private BigDecimal acoefficient;

    private BigDecimal acoefficient2;

    private BigDecimal agentBusget2;

    private BigDecimal coefficient;

    private Integer isfirst;

    private Integer ptype;

    private BigDecimal zjzd;

    private Integer range1;

    private Integer stype;

    private String couponname;

    private Integer payType;

    private Integer orderNumber;

    private Date accptTime;

    private Integer isaccpt;

    private Integer pushType;

    private String userPhone;

    private Integer appOrwx;

    private Date readyTime;

    private Date disTime;

    private Integer errend;

    private Integer teamid;

    private String teamname;

    private Integer istimerefund;

    private Integer issubsidy;

    private BigDecimal lessps;

    private BigDecimal subsidy;

    private Integer agree;

    private String content1;

    /**
     * 原配送费
     */
    private BigDecimal deliveryFee;

    /**
     * 自取时间
     */
    private Date selfTime;

    /**
     * 自取电话
     */
    private String selfMobile;

    /**
     * 用户选择的自取方式
     */
    private Boolean suportSelf;

    /**
     * 堂食（0：否；1：是）
     */
    private Boolean eatInBusiness;

    /**
     * 申请取消时间
     */
    private Date cancelTime;

    private Integer userAddressTag;


    /**
     * 是否是预订单（1：是）
     */
    private Boolean booked;

    /**
     * 预定时间
     */
    private Date bookTime;

    /**
     * 用户地址性别
     */
    private Integer userAddressGender;

    private static final long serialVersionUID = 1L;


    private List<RunfastOrderActivity> validActivityList = new ArrayList<>();

}