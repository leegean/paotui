package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.BaseOrg;
import com.runfast.waimai.dao.model.BaseOrgExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BaseOrgMapper extends IMapper<BaseOrg, Integer, BaseOrgExample> {
}