package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastAgentaccounts;
import com.runfast.waimai.dao.model.RunfastAgentaccountsExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastAgentaccountsMapper extends IMapper<RunfastAgentaccounts, Integer, RunfastAgentaccountsExample> {
}