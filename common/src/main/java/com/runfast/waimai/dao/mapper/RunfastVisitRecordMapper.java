package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastVisitRecord;
import com.runfast.waimai.dao.model.RunfastVisitRecordExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastVisitRecordMapper extends IMapper<RunfastVisitRecord, Integer, RunfastVisitRecordExample> {
}