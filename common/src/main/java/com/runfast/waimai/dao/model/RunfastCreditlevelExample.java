package com.runfast.waimai.dao.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RunfastCreditlevelExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public RunfastCreditlevelExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andCommissionIsNull() {
            addCriterion("commission is null");
            return (Criteria) this;
        }

        public Criteria andCommissionIsNotNull() {
            addCriterion("commission is not null");
            return (Criteria) this;
        }

        public Criteria andCommissionEqualTo(BigDecimal value) {
            addCriterion("commission =", value, "commission");
            return (Criteria) this;
        }

        public Criteria andCommissionNotEqualTo(BigDecimal value) {
            addCriterion("commission <>", value, "commission");
            return (Criteria) this;
        }

        public Criteria andCommissionGreaterThan(BigDecimal value) {
            addCriterion("commission >", value, "commission");
            return (Criteria) this;
        }

        public Criteria andCommissionGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("commission >=", value, "commission");
            return (Criteria) this;
        }

        public Criteria andCommissionLessThan(BigDecimal value) {
            addCriterion("commission <", value, "commission");
            return (Criteria) this;
        }

        public Criteria andCommissionLessThanOrEqualTo(BigDecimal value) {
            addCriterion("commission <=", value, "commission");
            return (Criteria) this;
        }

        public Criteria andCommissionIn(List<BigDecimal> values) {
            addCriterion("commission in", values, "commission");
            return (Criteria) this;
        }

        public Criteria andCommissionNotIn(List<BigDecimal> values) {
            addCriterion("commission not in", values, "commission");
            return (Criteria) this;
        }

        public Criteria andCommissionBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("commission between", value1, value2, "commission");
            return (Criteria) this;
        }

        public Criteria andCommissionNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("commission not between", value1, value2, "commission");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("createTime is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("createTime is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("createTime =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("createTime <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("createTime >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("createTime >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("createTime <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("createTime <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("createTime in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("createTime not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("createTime between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("createTime not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andEndgradeIsNull() {
            addCriterion("endgrade is null");
            return (Criteria) this;
        }

        public Criteria andEndgradeIsNotNull() {
            addCriterion("endgrade is not null");
            return (Criteria) this;
        }

        public Criteria andEndgradeEqualTo(Integer value) {
            addCriterion("endgrade =", value, "endgrade");
            return (Criteria) this;
        }

        public Criteria andEndgradeNotEqualTo(Integer value) {
            addCriterion("endgrade <>", value, "endgrade");
            return (Criteria) this;
        }

        public Criteria andEndgradeGreaterThan(Integer value) {
            addCriterion("endgrade >", value, "endgrade");
            return (Criteria) this;
        }

        public Criteria andEndgradeGreaterThanOrEqualTo(Integer value) {
            addCriterion("endgrade >=", value, "endgrade");
            return (Criteria) this;
        }

        public Criteria andEndgradeLessThan(Integer value) {
            addCriterion("endgrade <", value, "endgrade");
            return (Criteria) this;
        }

        public Criteria andEndgradeLessThanOrEqualTo(Integer value) {
            addCriterion("endgrade <=", value, "endgrade");
            return (Criteria) this;
        }

        public Criteria andEndgradeIn(List<Integer> values) {
            addCriterion("endgrade in", values, "endgrade");
            return (Criteria) this;
        }

        public Criteria andEndgradeNotIn(List<Integer> values) {
            addCriterion("endgrade not in", values, "endgrade");
            return (Criteria) this;
        }

        public Criteria andEndgradeBetween(Integer value1, Integer value2) {
            addCriterion("endgrade between", value1, value2, "endgrade");
            return (Criteria) this;
        }

        public Criteria andEndgradeNotBetween(Integer value1, Integer value2) {
            addCriterion("endgrade not between", value1, value2, "endgrade");
            return (Criteria) this;
        }

        public Criteria andSortIsNull() {
            addCriterion("sort is null");
            return (Criteria) this;
        }

        public Criteria andSortIsNotNull() {
            addCriterion("sort is not null");
            return (Criteria) this;
        }

        public Criteria andSortEqualTo(Integer value) {
            addCriterion("sort =", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotEqualTo(Integer value) {
            addCriterion("sort <>", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThan(Integer value) {
            addCriterion("sort >", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThanOrEqualTo(Integer value) {
            addCriterion("sort >=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThan(Integer value) {
            addCriterion("sort <", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThanOrEqualTo(Integer value) {
            addCriterion("sort <=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortIn(List<Integer> values) {
            addCriterion("sort in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotIn(List<Integer> values) {
            addCriterion("sort not in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortBetween(Integer value1, Integer value2) {
            addCriterion("sort between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotBetween(Integer value1, Integer value2) {
            addCriterion("sort not between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andStartgradeIsNull() {
            addCriterion("startgrade is null");
            return (Criteria) this;
        }

        public Criteria andStartgradeIsNotNull() {
            addCriterion("startgrade is not null");
            return (Criteria) this;
        }

        public Criteria andStartgradeEqualTo(Integer value) {
            addCriterion("startgrade =", value, "startgrade");
            return (Criteria) this;
        }

        public Criteria andStartgradeNotEqualTo(Integer value) {
            addCriterion("startgrade <>", value, "startgrade");
            return (Criteria) this;
        }

        public Criteria andStartgradeGreaterThan(Integer value) {
            addCriterion("startgrade >", value, "startgrade");
            return (Criteria) this;
        }

        public Criteria andStartgradeGreaterThanOrEqualTo(Integer value) {
            addCriterion("startgrade >=", value, "startgrade");
            return (Criteria) this;
        }

        public Criteria andStartgradeLessThan(Integer value) {
            addCriterion("startgrade <", value, "startgrade");
            return (Criteria) this;
        }

        public Criteria andStartgradeLessThanOrEqualTo(Integer value) {
            addCriterion("startgrade <=", value, "startgrade");
            return (Criteria) this;
        }

        public Criteria andStartgradeIn(List<Integer> values) {
            addCriterion("startgrade in", values, "startgrade");
            return (Criteria) this;
        }

        public Criteria andStartgradeNotIn(List<Integer> values) {
            addCriterion("startgrade not in", values, "startgrade");
            return (Criteria) this;
        }

        public Criteria andStartgradeBetween(Integer value1, Integer value2) {
            addCriterion("startgrade between", value1, value2, "startgrade");
            return (Criteria) this;
        }

        public Criteria andStartgradeNotBetween(Integer value1, Integer value2) {
            addCriterion("startgrade not between", value1, value2, "startgrade");
            return (Criteria) this;
        }

        public Criteria andAddpriceIsNull() {
            addCriterion("addprice is null");
            return (Criteria) this;
        }

        public Criteria andAddpriceIsNotNull() {
            addCriterion("addprice is not null");
            return (Criteria) this;
        }

        public Criteria andAddpriceEqualTo(BigDecimal value) {
            addCriterion("addprice =", value, "addprice");
            return (Criteria) this;
        }

        public Criteria andAddpriceNotEqualTo(BigDecimal value) {
            addCriterion("addprice <>", value, "addprice");
            return (Criteria) this;
        }

        public Criteria andAddpriceGreaterThan(BigDecimal value) {
            addCriterion("addprice >", value, "addprice");
            return (Criteria) this;
        }

        public Criteria andAddpriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("addprice >=", value, "addprice");
            return (Criteria) this;
        }

        public Criteria andAddpriceLessThan(BigDecimal value) {
            addCriterion("addprice <", value, "addprice");
            return (Criteria) this;
        }

        public Criteria andAddpriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("addprice <=", value, "addprice");
            return (Criteria) this;
        }

        public Criteria andAddpriceIn(List<BigDecimal> values) {
            addCriterion("addprice in", values, "addprice");
            return (Criteria) this;
        }

        public Criteria andAddpriceNotIn(List<BigDecimal> values) {
            addCriterion("addprice not in", values, "addprice");
            return (Criteria) this;
        }

        public Criteria andAddpriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("addprice between", value1, value2, "addprice");
            return (Criteria) this;
        }

        public Criteria andAddpriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("addprice not between", value1, value2, "addprice");
            return (Criteria) this;
        }

        public Criteria andCommissionDayIsNull() {
            addCriterion("commissionDay is null");
            return (Criteria) this;
        }

        public Criteria andCommissionDayIsNotNull() {
            addCriterion("commissionDay is not null");
            return (Criteria) this;
        }

        public Criteria andCommissionDayEqualTo(BigDecimal value) {
            addCriterion("commissionDay =", value, "commissionDay");
            return (Criteria) this;
        }

        public Criteria andCommissionDayNotEqualTo(BigDecimal value) {
            addCriterion("commissionDay <>", value, "commissionDay");
            return (Criteria) this;
        }

        public Criteria andCommissionDayGreaterThan(BigDecimal value) {
            addCriterion("commissionDay >", value, "commissionDay");
            return (Criteria) this;
        }

        public Criteria andCommissionDayGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("commissionDay >=", value, "commissionDay");
            return (Criteria) this;
        }

        public Criteria andCommissionDayLessThan(BigDecimal value) {
            addCriterion("commissionDay <", value, "commissionDay");
            return (Criteria) this;
        }

        public Criteria andCommissionDayLessThanOrEqualTo(BigDecimal value) {
            addCriterion("commissionDay <=", value, "commissionDay");
            return (Criteria) this;
        }

        public Criteria andCommissionDayIn(List<BigDecimal> values) {
            addCriterion("commissionDay in", values, "commissionDay");
            return (Criteria) this;
        }

        public Criteria andCommissionDayNotIn(List<BigDecimal> values) {
            addCriterion("commissionDay not in", values, "commissionDay");
            return (Criteria) this;
        }

        public Criteria andCommissionDayBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("commissionDay between", value1, value2, "commissionDay");
            return (Criteria) this;
        }

        public Criteria andCommissionDayNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("commissionDay not between", value1, value2, "commissionDay");
            return (Criteria) this;
        }

        public Criteria andCommissionNightIsNull() {
            addCriterion("commissionNight is null");
            return (Criteria) this;
        }

        public Criteria andCommissionNightIsNotNull() {
            addCriterion("commissionNight is not null");
            return (Criteria) this;
        }

        public Criteria andCommissionNightEqualTo(BigDecimal value) {
            addCriterion("commissionNight =", value, "commissionNight");
            return (Criteria) this;
        }

        public Criteria andCommissionNightNotEqualTo(BigDecimal value) {
            addCriterion("commissionNight <>", value, "commissionNight");
            return (Criteria) this;
        }

        public Criteria andCommissionNightGreaterThan(BigDecimal value) {
            addCriterion("commissionNight >", value, "commissionNight");
            return (Criteria) this;
        }

        public Criteria andCommissionNightGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("commissionNight >=", value, "commissionNight");
            return (Criteria) this;
        }

        public Criteria andCommissionNightLessThan(BigDecimal value) {
            addCriterion("commissionNight <", value, "commissionNight");
            return (Criteria) this;
        }

        public Criteria andCommissionNightLessThanOrEqualTo(BigDecimal value) {
            addCriterion("commissionNight <=", value, "commissionNight");
            return (Criteria) this;
        }

        public Criteria andCommissionNightIn(List<BigDecimal> values) {
            addCriterion("commissionNight in", values, "commissionNight");
            return (Criteria) this;
        }

        public Criteria andCommissionNightNotIn(List<BigDecimal> values) {
            addCriterion("commissionNight not in", values, "commissionNight");
            return (Criteria) this;
        }

        public Criteria andCommissionNightBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("commissionNight between", value1, value2, "commissionNight");
            return (Criteria) this;
        }

        public Criteria andCommissionNightNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("commissionNight not between", value1, value2, "commissionNight");
            return (Criteria) this;
        }

        public Criteria andEndTimeDayIsNull() {
            addCriterion("endTimeDay is null");
            return (Criteria) this;
        }

        public Criteria andEndTimeDayIsNotNull() {
            addCriterion("endTimeDay is not null");
            return (Criteria) this;
        }

        public Criteria andEndTimeDayEqualTo(Date value) {
            addCriterion("endTimeDay =", value, "endTimeDay");
            return (Criteria) this;
        }

        public Criteria andEndTimeDayNotEqualTo(Date value) {
            addCriterion("endTimeDay <>", value, "endTimeDay");
            return (Criteria) this;
        }

        public Criteria andEndTimeDayGreaterThan(Date value) {
            addCriterion("endTimeDay >", value, "endTimeDay");
            return (Criteria) this;
        }

        public Criteria andEndTimeDayGreaterThanOrEqualTo(Date value) {
            addCriterion("endTimeDay >=", value, "endTimeDay");
            return (Criteria) this;
        }

        public Criteria andEndTimeDayLessThan(Date value) {
            addCriterion("endTimeDay <", value, "endTimeDay");
            return (Criteria) this;
        }

        public Criteria andEndTimeDayLessThanOrEqualTo(Date value) {
            addCriterion("endTimeDay <=", value, "endTimeDay");
            return (Criteria) this;
        }

        public Criteria andEndTimeDayIn(List<Date> values) {
            addCriterion("endTimeDay in", values, "endTimeDay");
            return (Criteria) this;
        }

        public Criteria andEndTimeDayNotIn(List<Date> values) {
            addCriterion("endTimeDay not in", values, "endTimeDay");
            return (Criteria) this;
        }

        public Criteria andEndTimeDayBetween(Date value1, Date value2) {
            addCriterion("endTimeDay between", value1, value2, "endTimeDay");
            return (Criteria) this;
        }

        public Criteria andEndTimeDayNotBetween(Date value1, Date value2) {
            addCriterion("endTimeDay not between", value1, value2, "endTimeDay");
            return (Criteria) this;
        }

        public Criteria andEndTimeNightIsNull() {
            addCriterion("endTimeNight is null");
            return (Criteria) this;
        }

        public Criteria andEndTimeNightIsNotNull() {
            addCriterion("endTimeNight is not null");
            return (Criteria) this;
        }

        public Criteria andEndTimeNightEqualTo(Date value) {
            addCriterion("endTimeNight =", value, "endTimeNight");
            return (Criteria) this;
        }

        public Criteria andEndTimeNightNotEqualTo(Date value) {
            addCriterion("endTimeNight <>", value, "endTimeNight");
            return (Criteria) this;
        }

        public Criteria andEndTimeNightGreaterThan(Date value) {
            addCriterion("endTimeNight >", value, "endTimeNight");
            return (Criteria) this;
        }

        public Criteria andEndTimeNightGreaterThanOrEqualTo(Date value) {
            addCriterion("endTimeNight >=", value, "endTimeNight");
            return (Criteria) this;
        }

        public Criteria andEndTimeNightLessThan(Date value) {
            addCriterion("endTimeNight <", value, "endTimeNight");
            return (Criteria) this;
        }

        public Criteria andEndTimeNightLessThanOrEqualTo(Date value) {
            addCriterion("endTimeNight <=", value, "endTimeNight");
            return (Criteria) this;
        }

        public Criteria andEndTimeNightIn(List<Date> values) {
            addCriterion("endTimeNight in", values, "endTimeNight");
            return (Criteria) this;
        }

        public Criteria andEndTimeNightNotIn(List<Date> values) {
            addCriterion("endTimeNight not in", values, "endTimeNight");
            return (Criteria) this;
        }

        public Criteria andEndTimeNightBetween(Date value1, Date value2) {
            addCriterion("endTimeNight between", value1, value2, "endTimeNight");
            return (Criteria) this;
        }

        public Criteria andEndTimeNightNotBetween(Date value1, Date value2) {
            addCriterion("endTimeNight not between", value1, value2, "endTimeNight");
            return (Criteria) this;
        }

        public Criteria andStartTimeDayIsNull() {
            addCriterion("startTimeDay is null");
            return (Criteria) this;
        }

        public Criteria andStartTimeDayIsNotNull() {
            addCriterion("startTimeDay is not null");
            return (Criteria) this;
        }

        public Criteria andStartTimeDayEqualTo(Date value) {
            addCriterion("startTimeDay =", value, "startTimeDay");
            return (Criteria) this;
        }

        public Criteria andStartTimeDayNotEqualTo(Date value) {
            addCriterion("startTimeDay <>", value, "startTimeDay");
            return (Criteria) this;
        }

        public Criteria andStartTimeDayGreaterThan(Date value) {
            addCriterion("startTimeDay >", value, "startTimeDay");
            return (Criteria) this;
        }

        public Criteria andStartTimeDayGreaterThanOrEqualTo(Date value) {
            addCriterion("startTimeDay >=", value, "startTimeDay");
            return (Criteria) this;
        }

        public Criteria andStartTimeDayLessThan(Date value) {
            addCriterion("startTimeDay <", value, "startTimeDay");
            return (Criteria) this;
        }

        public Criteria andStartTimeDayLessThanOrEqualTo(Date value) {
            addCriterion("startTimeDay <=", value, "startTimeDay");
            return (Criteria) this;
        }

        public Criteria andStartTimeDayIn(List<Date> values) {
            addCriterion("startTimeDay in", values, "startTimeDay");
            return (Criteria) this;
        }

        public Criteria andStartTimeDayNotIn(List<Date> values) {
            addCriterion("startTimeDay not in", values, "startTimeDay");
            return (Criteria) this;
        }

        public Criteria andStartTimeDayBetween(Date value1, Date value2) {
            addCriterion("startTimeDay between", value1, value2, "startTimeDay");
            return (Criteria) this;
        }

        public Criteria andStartTimeDayNotBetween(Date value1, Date value2) {
            addCriterion("startTimeDay not between", value1, value2, "startTimeDay");
            return (Criteria) this;
        }

        public Criteria andStartTimeNightIsNull() {
            addCriterion("startTimeNight is null");
            return (Criteria) this;
        }

        public Criteria andStartTimeNightIsNotNull() {
            addCriterion("startTimeNight is not null");
            return (Criteria) this;
        }

        public Criteria andStartTimeNightEqualTo(Date value) {
            addCriterion("startTimeNight =", value, "startTimeNight");
            return (Criteria) this;
        }

        public Criteria andStartTimeNightNotEqualTo(Date value) {
            addCriterion("startTimeNight <>", value, "startTimeNight");
            return (Criteria) this;
        }

        public Criteria andStartTimeNightGreaterThan(Date value) {
            addCriterion("startTimeNight >", value, "startTimeNight");
            return (Criteria) this;
        }

        public Criteria andStartTimeNightGreaterThanOrEqualTo(Date value) {
            addCriterion("startTimeNight >=", value, "startTimeNight");
            return (Criteria) this;
        }

        public Criteria andStartTimeNightLessThan(Date value) {
            addCriterion("startTimeNight <", value, "startTimeNight");
            return (Criteria) this;
        }

        public Criteria andStartTimeNightLessThanOrEqualTo(Date value) {
            addCriterion("startTimeNight <=", value, "startTimeNight");
            return (Criteria) this;
        }

        public Criteria andStartTimeNightIn(List<Date> values) {
            addCriterion("startTimeNight in", values, "startTimeNight");
            return (Criteria) this;
        }

        public Criteria andStartTimeNightNotIn(List<Date> values) {
            addCriterion("startTimeNight not in", values, "startTimeNight");
            return (Criteria) this;
        }

        public Criteria andStartTimeNightBetween(Date value1, Date value2) {
            addCriterion("startTimeNight between", value1, value2, "startTimeNight");
            return (Criteria) this;
        }

        public Criteria andStartTimeNightNotBetween(Date value1, Date value2) {
            addCriterion("startTimeNight not between", value1, value2, "startTimeNight");
            return (Criteria) this;
        }

        public Criteria andAgentIdIsNull() {
            addCriterion("agentId is null");
            return (Criteria) this;
        }

        public Criteria andAgentIdIsNotNull() {
            addCriterion("agentId is not null");
            return (Criteria) this;
        }

        public Criteria andAgentIdEqualTo(Integer value) {
            addCriterion("agentId =", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotEqualTo(Integer value) {
            addCriterion("agentId <>", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdGreaterThan(Integer value) {
            addCriterion("agentId >", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("agentId >=", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdLessThan(Integer value) {
            addCriterion("agentId <", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdLessThanOrEqualTo(Integer value) {
            addCriterion("agentId <=", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdIn(List<Integer> values) {
            addCriterion("agentId in", values, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotIn(List<Integer> values) {
            addCriterion("agentId not in", values, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdBetween(Integer value1, Integer value2) {
            addCriterion("agentId between", value1, value2, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotBetween(Integer value1, Integer value2) {
            addCriterion("agentId not between", value1, value2, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentNameIsNull() {
            addCriterion("agentName is null");
            return (Criteria) this;
        }

        public Criteria andAgentNameIsNotNull() {
            addCriterion("agentName is not null");
            return (Criteria) this;
        }

        public Criteria andAgentNameEqualTo(String value) {
            addCriterion("agentName =", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotEqualTo(String value) {
            addCriterion("agentName <>", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameGreaterThan(String value) {
            addCriterion("agentName >", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameGreaterThanOrEqualTo(String value) {
            addCriterion("agentName >=", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameLessThan(String value) {
            addCriterion("agentName <", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameLessThanOrEqualTo(String value) {
            addCriterion("agentName <=", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameLike(String value) {
            addCriterion("agentName like", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotLike(String value) {
            addCriterion("agentName not like", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameIn(List<String> values) {
            addCriterion("agentName in", values, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotIn(List<String> values) {
            addCriterion("agentName not in", values, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameBetween(String value1, String value2) {
            addCriterion("agentName between", value1, value2, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotBetween(String value1, String value2) {
            addCriterion("agentName not between", value1, value2, "agentName");
            return (Criteria) this;
        }

        public Criteria andDistanIsNull() {
            addCriterion("distan is null");
            return (Criteria) this;
        }

        public Criteria andDistanIsNotNull() {
            addCriterion("distan is not null");
            return (Criteria) this;
        }

        public Criteria andDistanEqualTo(Double value) {
            addCriterion("distan =", value, "distan");
            return (Criteria) this;
        }

        public Criteria andDistanNotEqualTo(Double value) {
            addCriterion("distan <>", value, "distan");
            return (Criteria) this;
        }

        public Criteria andDistanGreaterThan(Double value) {
            addCriterion("distan >", value, "distan");
            return (Criteria) this;
        }

        public Criteria andDistanGreaterThanOrEqualTo(Double value) {
            addCriterion("distan >=", value, "distan");
            return (Criteria) this;
        }

        public Criteria andDistanLessThan(Double value) {
            addCriterion("distan <", value, "distan");
            return (Criteria) this;
        }

        public Criteria andDistanLessThanOrEqualTo(Double value) {
            addCriterion("distan <=", value, "distan");
            return (Criteria) this;
        }

        public Criteria andDistanIn(List<Double> values) {
            addCriterion("distan in", values, "distan");
            return (Criteria) this;
        }

        public Criteria andDistanNotIn(List<Double> values) {
            addCriterion("distan not in", values, "distan");
            return (Criteria) this;
        }

        public Criteria andDistanBetween(Double value1, Double value2) {
            addCriterion("distan between", value1, value2, "distan");
            return (Criteria) this;
        }

        public Criteria andDistanNotBetween(Double value1, Double value2) {
            addCriterion("distan not between", value1, value2, "distan");
            return (Criteria) this;
        }

        public Criteria andPriceIsNull() {
            addCriterion("price is null");
            return (Criteria) this;
        }

        public Criteria andPriceIsNotNull() {
            addCriterion("price is not null");
            return (Criteria) this;
        }

        public Criteria andPriceEqualTo(BigDecimal value) {
            addCriterion("price =", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotEqualTo(BigDecimal value) {
            addCriterion("price <>", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceGreaterThan(BigDecimal value) {
            addCriterion("price >", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("price >=", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceLessThan(BigDecimal value) {
            addCriterion("price <", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("price <=", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceIn(List<BigDecimal> values) {
            addCriterion("price in", values, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotIn(List<BigDecimal> values) {
            addCriterion("price not in", values, "price");
            return (Criteria) this;
        }

        public Criteria andPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("price between", value1, value2, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("price not between", value1, value2, "price");
            return (Criteria) this;
        }

        public Criteria andMaxpriceIsNull() {
            addCriterion("maxprice is null");
            return (Criteria) this;
        }

        public Criteria andMaxpriceIsNotNull() {
            addCriterion("maxprice is not null");
            return (Criteria) this;
        }

        public Criteria andMaxpriceEqualTo(BigDecimal value) {
            addCriterion("maxprice =", value, "maxprice");
            return (Criteria) this;
        }

        public Criteria andMaxpriceNotEqualTo(BigDecimal value) {
            addCriterion("maxprice <>", value, "maxprice");
            return (Criteria) this;
        }

        public Criteria andMaxpriceGreaterThan(BigDecimal value) {
            addCriterion("maxprice >", value, "maxprice");
            return (Criteria) this;
        }

        public Criteria andMaxpriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("maxprice >=", value, "maxprice");
            return (Criteria) this;
        }

        public Criteria andMaxpriceLessThan(BigDecimal value) {
            addCriterion("maxprice <", value, "maxprice");
            return (Criteria) this;
        }

        public Criteria andMaxpriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("maxprice <=", value, "maxprice");
            return (Criteria) this;
        }

        public Criteria andMaxpriceIn(List<BigDecimal> values) {
            addCriterion("maxprice in", values, "maxprice");
            return (Criteria) this;
        }

        public Criteria andMaxpriceNotIn(List<BigDecimal> values) {
            addCriterion("maxprice not in", values, "maxprice");
            return (Criteria) this;
        }

        public Criteria andMaxpriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("maxprice between", value1, value2, "maxprice");
            return (Criteria) this;
        }

        public Criteria andMaxpriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("maxprice not between", value1, value2, "maxprice");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}