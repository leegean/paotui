package com.runfast.waimai.dao.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class RunfastWithdrawrecord implements Serializable {
    private Integer id;

    private Date acctDate;

    private Integer businessId;

    private String businessName;

    private Date createTime;

    private Integer wid;

    private BigDecimal withdrawNumber;

    private Integer agentId;

    private String agentName;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public RunfastWithdrawrecord withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getAcctDate() {
        return acctDate;
    }

    public RunfastWithdrawrecord withAcctDate(Date acctDate) {
        this.setAcctDate(acctDate);
        return this;
    }

    public void setAcctDate(Date acctDate) {
        this.acctDate = acctDate;
    }

    public Integer getBusinessId() {
        return businessId;
    }

    public RunfastWithdrawrecord withBusinessId(Integer businessId) {
        this.setBusinessId(businessId);
        return this;
    }

    public void setBusinessId(Integer businessId) {
        this.businessId = businessId;
    }

    public String getBusinessName() {
        return businessName;
    }

    public RunfastWithdrawrecord withBusinessName(String businessName) {
        this.setBusinessName(businessName);
        return this;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName == null ? null : businessName.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public RunfastWithdrawrecord withCreateTime(Date createTime) {
        this.setCreateTime(createTime);
        return this;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getWid() {
        return wid;
    }

    public RunfastWithdrawrecord withWid(Integer wid) {
        this.setWid(wid);
        return this;
    }

    public void setWid(Integer wid) {
        this.wid = wid;
    }

    public BigDecimal getWithdrawNumber() {
        return withdrawNumber;
    }

    public RunfastWithdrawrecord withWithdrawNumber(BigDecimal withdrawNumber) {
        this.setWithdrawNumber(withdrawNumber);
        return this;
    }

    public void setWithdrawNumber(BigDecimal withdrawNumber) {
        this.withdrawNumber = withdrawNumber;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public RunfastWithdrawrecord withAgentId(Integer agentId) {
        this.setAgentId(agentId);
        return this;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public String getAgentName() {
        return agentName;
    }

    public RunfastWithdrawrecord withAgentName(String agentName) {
        this.setAgentName(agentName);
        return this;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName == null ? null : agentName.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", acctDate=").append(acctDate);
        sb.append(", businessId=").append(businessId);
        sb.append(", businessName=").append(businessName);
        sb.append(", createTime=").append(createTime);
        sb.append(", wid=").append(wid);
        sb.append(", withdrawNumber=").append(withdrawNumber);
        sb.append(", agentId=").append(agentId);
        sb.append(", agentName=").append(agentName);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}