package com.runfast.waimai.dao.model;

import java.io.Serializable;
import java.util.Date;

public class SysUserToken implements Serializable {
    private Long user_id;

    private String token;

    private Date expire_time;

    private Date update_time;

    private Integer user_type;

    private static final long serialVersionUID = 1L;

    public Long getUser_id() {
        return user_id;
    }

    public SysUserToken withUser_id(Long user_id) {
        this.setUser_id(user_id);
        return this;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public String getToken() {
        return token;
    }

    public SysUserToken withToken(String token) {
        this.setToken(token);
        return this;
    }

    public void setToken(String token) {
        this.token = token == null ? null : token.trim();
    }

    public Date getExpire_time() {
        return expire_time;
    }

    public SysUserToken withExpire_time(Date expire_time) {
        this.setExpire_time(expire_time);
        return this;
    }

    public void setExpire_time(Date expire_time) {
        this.expire_time = expire_time;
    }

    public Date getUpdate_time() {
        return update_time;
    }

    public SysUserToken withUpdate_time(Date update_time) {
        this.setUpdate_time(update_time);
        return this;
    }

    public void setUpdate_time(Date update_time) {
        this.update_time = update_time;
    }

    public Integer getUser_type() {
        return user_type;
    }

    public SysUserToken withUser_type(Integer user_type) {
        this.setUser_type(user_type);
        return this;
    }

    public void setUser_type(Integer user_type) {
        this.user_type = user_type;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", user_id=").append(user_id);
        sb.append(", token=").append(token);
        sb.append(", expire_time=").append(expire_time);
        sb.append(", update_time=").append(update_time);
        sb.append(", user_type=").append(user_type);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}