package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastBusinessAccounts;
import com.runfast.waimai.dao.model.RunfastBusinessAccountsExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastBusinessAccountsMapper extends IMapper<RunfastBusinessAccounts, Integer, RunfastBusinessAccountsExample> {
}