package com.runfast.waimai.dao.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RunfastGoodsSellChildrenExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public RunfastGoodsSellChildrenExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andBusinessIdIsNull() {
            addCriterion("businessId is null");
            return (Criteria) this;
        }

        public Criteria andBusinessIdIsNotNull() {
            addCriterion("businessId is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessIdEqualTo(Integer value) {
            addCriterion("businessId =", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdNotEqualTo(Integer value) {
            addCriterion("businessId <>", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdGreaterThan(Integer value) {
            addCriterion("businessId >", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("businessId >=", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdLessThan(Integer value) {
            addCriterion("businessId <", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdLessThanOrEqualTo(Integer value) {
            addCriterion("businessId <=", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdIn(List<Integer> values) {
            addCriterion("businessId in", values, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdNotIn(List<Integer> values) {
            addCriterion("businessId not in", values, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdBetween(Integer value1, Integer value2) {
            addCriterion("businessId between", value1, value2, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdNotBetween(Integer value1, Integer value2) {
            addCriterion("businessId not between", value1, value2, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessNameIsNull() {
            addCriterion("businessName is null");
            return (Criteria) this;
        }

        public Criteria andBusinessNameIsNotNull() {
            addCriterion("businessName is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessNameEqualTo(String value) {
            addCriterion("businessName =", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameNotEqualTo(String value) {
            addCriterion("businessName <>", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameGreaterThan(String value) {
            addCriterion("businessName >", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameGreaterThanOrEqualTo(String value) {
            addCriterion("businessName >=", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameLessThan(String value) {
            addCriterion("businessName <", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameLessThanOrEqualTo(String value) {
            addCriterion("businessName <=", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameLike(String value) {
            addCriterion("businessName like", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameNotLike(String value) {
            addCriterion("businessName not like", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameIn(List<String> values) {
            addCriterion("businessName in", values, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameNotIn(List<String> values) {
            addCriterion("businessName not in", values, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameBetween(String value1, String value2) {
            addCriterion("businessName between", value1, value2, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameNotBetween(String value1, String value2) {
            addCriterion("businessName not between", value1, value2, "businessName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdIsNull() {
            addCriterion("goodsSellId is null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdIsNotNull() {
            addCriterion("goodsSellId is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdEqualTo(Integer value) {
            addCriterion("goodsSellId =", value, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdNotEqualTo(Integer value) {
            addCriterion("goodsSellId <>", value, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdGreaterThan(Integer value) {
            addCriterion("goodsSellId >", value, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("goodsSellId >=", value, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdLessThan(Integer value) {
            addCriterion("goodsSellId <", value, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdLessThanOrEqualTo(Integer value) {
            addCriterion("goodsSellId <=", value, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdIn(List<Integer> values) {
            addCriterion("goodsSellId in", values, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdNotIn(List<Integer> values) {
            addCriterion("goodsSellId not in", values, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdBetween(Integer value1, Integer value2) {
            addCriterion("goodsSellId between", value1, value2, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdNotBetween(Integer value1, Integer value2) {
            addCriterion("goodsSellId not between", value1, value2, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameIsNull() {
            addCriterion("goodsSellName is null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameIsNotNull() {
            addCriterion("goodsSellName is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameEqualTo(String value) {
            addCriterion("goodsSellName =", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameNotEqualTo(String value) {
            addCriterion("goodsSellName <>", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameGreaterThan(String value) {
            addCriterion("goodsSellName >", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameGreaterThanOrEqualTo(String value) {
            addCriterion("goodsSellName >=", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameLessThan(String value) {
            addCriterion("goodsSellName <", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameLessThanOrEqualTo(String value) {
            addCriterion("goodsSellName <=", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameLike(String value) {
            addCriterion("goodsSellName like", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameNotLike(String value) {
            addCriterion("goodsSellName not like", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameIn(List<String> values) {
            addCriterion("goodsSellName in", values, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameNotIn(List<String> values) {
            addCriterion("goodsSellName not in", values, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameBetween(String value1, String value2) {
            addCriterion("goodsSellName between", value1, value2, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameNotBetween(String value1, String value2) {
            addCriterion("goodsSellName not between", value1, value2, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andNumIsNull() {
            addCriterion("num is null");
            return (Criteria) this;
        }

        public Criteria andNumIsNotNull() {
            addCriterion("num is not null");
            return (Criteria) this;
        }

        public Criteria andNumEqualTo(Integer value) {
            addCriterion("num =", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumNotEqualTo(Integer value) {
            addCriterion("num <>", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumGreaterThan(Integer value) {
            addCriterion("num >", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("num >=", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumLessThan(Integer value) {
            addCriterion("num <", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumLessThanOrEqualTo(Integer value) {
            addCriterion("num <=", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumIn(List<Integer> values) {
            addCriterion("num in", values, "num");
            return (Criteria) this;
        }

        public Criteria andNumNotIn(List<Integer> values) {
            addCriterion("num not in", values, "num");
            return (Criteria) this;
        }

        public Criteria andNumBetween(Integer value1, Integer value2) {
            addCriterion("num between", value1, value2, "num");
            return (Criteria) this;
        }

        public Criteria andNumNotBetween(Integer value1, Integer value2) {
            addCriterion("num not between", value1, value2, "num");
            return (Criteria) this;
        }

        public Criteria andOrderCodeIsNull() {
            addCriterion("orderCode is null");
            return (Criteria) this;
        }

        public Criteria andOrderCodeIsNotNull() {
            addCriterion("orderCode is not null");
            return (Criteria) this;
        }

        public Criteria andOrderCodeEqualTo(String value) {
            addCriterion("orderCode =", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeNotEqualTo(String value) {
            addCriterion("orderCode <>", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeGreaterThan(String value) {
            addCriterion("orderCode >", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeGreaterThanOrEqualTo(String value) {
            addCriterion("orderCode >=", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeLessThan(String value) {
            addCriterion("orderCode <", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeLessThanOrEqualTo(String value) {
            addCriterion("orderCode <=", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeLike(String value) {
            addCriterion("orderCode like", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeNotLike(String value) {
            addCriterion("orderCode not like", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeIn(List<String> values) {
            addCriterion("orderCode in", values, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeNotIn(List<String> values) {
            addCriterion("orderCode not in", values, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeBetween(String value1, String value2) {
            addCriterion("orderCode between", value1, value2, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeNotBetween(String value1, String value2) {
            addCriterion("orderCode not between", value1, value2, "orderCode");
            return (Criteria) this;
        }

        public Criteria andPidIsNull() {
            addCriterion("pid is null");
            return (Criteria) this;
        }

        public Criteria andPidIsNotNull() {
            addCriterion("pid is not null");
            return (Criteria) this;
        }

        public Criteria andPidEqualTo(Integer value) {
            addCriterion("pid =", value, "pid");
            return (Criteria) this;
        }

        public Criteria andPidNotEqualTo(Integer value) {
            addCriterion("pid <>", value, "pid");
            return (Criteria) this;
        }

        public Criteria andPidGreaterThan(Integer value) {
            addCriterion("pid >", value, "pid");
            return (Criteria) this;
        }

        public Criteria andPidGreaterThanOrEqualTo(Integer value) {
            addCriterion("pid >=", value, "pid");
            return (Criteria) this;
        }

        public Criteria andPidLessThan(Integer value) {
            addCriterion("pid <", value, "pid");
            return (Criteria) this;
        }

        public Criteria andPidLessThanOrEqualTo(Integer value) {
            addCriterion("pid <=", value, "pid");
            return (Criteria) this;
        }

        public Criteria andPidIn(List<Integer> values) {
            addCriterion("pid in", values, "pid");
            return (Criteria) this;
        }

        public Criteria andPidNotIn(List<Integer> values) {
            addCriterion("pid not in", values, "pid");
            return (Criteria) this;
        }

        public Criteria andPidBetween(Integer value1, Integer value2) {
            addCriterion("pid between", value1, value2, "pid");
            return (Criteria) this;
        }

        public Criteria andPidNotBetween(Integer value1, Integer value2) {
            addCriterion("pid not between", value1, value2, "pid");
            return (Criteria) this;
        }

        public Criteria andPriceIsNull() {
            addCriterion("price is null");
            return (Criteria) this;
        }

        public Criteria andPriceIsNotNull() {
            addCriterion("price is not null");
            return (Criteria) this;
        }

        public Criteria andPriceEqualTo(Double value) {
            addCriterion("price =", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotEqualTo(Double value) {
            addCriterion("price <>", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceGreaterThan(Double value) {
            addCriterion("price >", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceGreaterThanOrEqualTo(Double value) {
            addCriterion("price >=", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceLessThan(Double value) {
            addCriterion("price <", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceLessThanOrEqualTo(Double value) {
            addCriterion("price <=", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceIn(List<Double> values) {
            addCriterion("price in", values, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotIn(List<Double> values) {
            addCriterion("price not in", values, "price");
            return (Criteria) this;
        }

        public Criteria andPriceBetween(Double value1, Double value2) {
            addCriterion("price between", value1, value2, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotBetween(Double value1, Double value2) {
            addCriterion("price not between", value1, value2, "price");
            return (Criteria) this;
        }

        public Criteria andTotalpriceIsNull() {
            addCriterion("totalprice is null");
            return (Criteria) this;
        }

        public Criteria andTotalpriceIsNotNull() {
            addCriterion("totalprice is not null");
            return (Criteria) this;
        }

        public Criteria andTotalpriceEqualTo(Double value) {
            addCriterion("totalprice =", value, "totalprice");
            return (Criteria) this;
        }

        public Criteria andTotalpriceNotEqualTo(Double value) {
            addCriterion("totalprice <>", value, "totalprice");
            return (Criteria) this;
        }

        public Criteria andTotalpriceGreaterThan(Double value) {
            addCriterion("totalprice >", value, "totalprice");
            return (Criteria) this;
        }

        public Criteria andTotalpriceGreaterThanOrEqualTo(Double value) {
            addCriterion("totalprice >=", value, "totalprice");
            return (Criteria) this;
        }

        public Criteria andTotalpriceLessThan(Double value) {
            addCriterion("totalprice <", value, "totalprice");
            return (Criteria) this;
        }

        public Criteria andTotalpriceLessThanOrEqualTo(Double value) {
            addCriterion("totalprice <=", value, "totalprice");
            return (Criteria) this;
        }

        public Criteria andTotalpriceIn(List<Double> values) {
            addCriterion("totalprice in", values, "totalprice");
            return (Criteria) this;
        }

        public Criteria andTotalpriceNotIn(List<Double> values) {
            addCriterion("totalprice not in", values, "totalprice");
            return (Criteria) this;
        }

        public Criteria andTotalpriceBetween(Double value1, Double value2) {
            addCriterion("totalprice between", value1, value2, "totalprice");
            return (Criteria) this;
        }

        public Criteria andTotalpriceNotBetween(Double value1, Double value2) {
            addCriterion("totalprice not between", value1, value2, "totalprice");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionIdIsNull() {
            addCriterion("goodsSellOptionId is null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionIdIsNotNull() {
            addCriterion("goodsSellOptionId is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionIdEqualTo(Integer value) {
            addCriterion("goodsSellOptionId =", value, "goodsSellOptionId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionIdNotEqualTo(Integer value) {
            addCriterion("goodsSellOptionId <>", value, "goodsSellOptionId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionIdGreaterThan(Integer value) {
            addCriterion("goodsSellOptionId >", value, "goodsSellOptionId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("goodsSellOptionId >=", value, "goodsSellOptionId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionIdLessThan(Integer value) {
            addCriterion("goodsSellOptionId <", value, "goodsSellOptionId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionIdLessThanOrEqualTo(Integer value) {
            addCriterion("goodsSellOptionId <=", value, "goodsSellOptionId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionIdIn(List<Integer> values) {
            addCriterion("goodsSellOptionId in", values, "goodsSellOptionId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionIdNotIn(List<Integer> values) {
            addCriterion("goodsSellOptionId not in", values, "goodsSellOptionId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionIdBetween(Integer value1, Integer value2) {
            addCriterion("goodsSellOptionId between", value1, value2, "goodsSellOptionId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionIdNotBetween(Integer value1, Integer value2) {
            addCriterion("goodsSellOptionId not between", value1, value2, "goodsSellOptionId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionNameIsNull() {
            addCriterion("goodsSellOptionName is null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionNameIsNotNull() {
            addCriterion("goodsSellOptionName is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionNameEqualTo(String value) {
            addCriterion("goodsSellOptionName =", value, "goodsSellOptionName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionNameNotEqualTo(String value) {
            addCriterion("goodsSellOptionName <>", value, "goodsSellOptionName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionNameGreaterThan(String value) {
            addCriterion("goodsSellOptionName >", value, "goodsSellOptionName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionNameGreaterThanOrEqualTo(String value) {
            addCriterion("goodsSellOptionName >=", value, "goodsSellOptionName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionNameLessThan(String value) {
            addCriterion("goodsSellOptionName <", value, "goodsSellOptionName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionNameLessThanOrEqualTo(String value) {
            addCriterion("goodsSellOptionName <=", value, "goodsSellOptionName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionNameLike(String value) {
            addCriterion("goodsSellOptionName like", value, "goodsSellOptionName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionNameNotLike(String value) {
            addCriterion("goodsSellOptionName not like", value, "goodsSellOptionName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionNameIn(List<String> values) {
            addCriterion("goodsSellOptionName in", values, "goodsSellOptionName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionNameNotIn(List<String> values) {
            addCriterion("goodsSellOptionName not in", values, "goodsSellOptionName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionNameBetween(String value1, String value2) {
            addCriterion("goodsSellOptionName between", value1, value2, "goodsSellOptionName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionNameNotBetween(String value1, String value2) {
            addCriterion("goodsSellOptionName not between", value1, value2, "goodsSellOptionName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardIdIsNull() {
            addCriterion("goodsSellStandardId is null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardIdIsNotNull() {
            addCriterion("goodsSellStandardId is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardIdEqualTo(Integer value) {
            addCriterion("goodsSellStandardId =", value, "goodsSellStandardId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardIdNotEqualTo(Integer value) {
            addCriterion("goodsSellStandardId <>", value, "goodsSellStandardId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardIdGreaterThan(Integer value) {
            addCriterion("goodsSellStandardId >", value, "goodsSellStandardId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("goodsSellStandardId >=", value, "goodsSellStandardId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardIdLessThan(Integer value) {
            addCriterion("goodsSellStandardId <", value, "goodsSellStandardId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardIdLessThanOrEqualTo(Integer value) {
            addCriterion("goodsSellStandardId <=", value, "goodsSellStandardId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardIdIn(List<Integer> values) {
            addCriterion("goodsSellStandardId in", values, "goodsSellStandardId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardIdNotIn(List<Integer> values) {
            addCriterion("goodsSellStandardId not in", values, "goodsSellStandardId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardIdBetween(Integer value1, Integer value2) {
            addCriterion("goodsSellStandardId between", value1, value2, "goodsSellStandardId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardIdNotBetween(Integer value1, Integer value2) {
            addCriterion("goodsSellStandardId not between", value1, value2, "goodsSellStandardId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardNameIsNull() {
            addCriterion("goodsSellStandardName is null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardNameIsNotNull() {
            addCriterion("goodsSellStandardName is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardNameEqualTo(String value) {
            addCriterion("goodsSellStandardName =", value, "goodsSellStandardName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardNameNotEqualTo(String value) {
            addCriterion("goodsSellStandardName <>", value, "goodsSellStandardName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardNameGreaterThan(String value) {
            addCriterion("goodsSellStandardName >", value, "goodsSellStandardName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardNameGreaterThanOrEqualTo(String value) {
            addCriterion("goodsSellStandardName >=", value, "goodsSellStandardName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardNameLessThan(String value) {
            addCriterion("goodsSellStandardName <", value, "goodsSellStandardName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardNameLessThanOrEqualTo(String value) {
            addCriterion("goodsSellStandardName <=", value, "goodsSellStandardName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardNameLike(String value) {
            addCriterion("goodsSellStandardName like", value, "goodsSellStandardName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardNameNotLike(String value) {
            addCriterion("goodsSellStandardName not like", value, "goodsSellStandardName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardNameIn(List<String> values) {
            addCriterion("goodsSellStandardName in", values, "goodsSellStandardName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardNameNotIn(List<String> values) {
            addCriterion("goodsSellStandardName not in", values, "goodsSellStandardName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardNameBetween(String value1, String value2) {
            addCriterion("goodsSellStandardName between", value1, value2, "goodsSellStandardName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardNameNotBetween(String value1, String value2) {
            addCriterion("goodsSellStandardName not between", value1, value2, "goodsSellStandardName");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("createTime is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("createTime is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("createTime =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("createTime <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("createTime >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("createTime >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("createTime <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("createTime <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("createTime in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("createTime not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("createTime between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("createTime not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNull() {
            addCriterion("userId is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("userId is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(Integer value) {
            addCriterion("userId =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(Integer value) {
            addCriterion("userId <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(Integer value) {
            addCriterion("userId >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("userId >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(Integer value) {
            addCriterion("userId <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(Integer value) {
            addCriterion("userId <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<Integer> values) {
            addCriterion("userId in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<Integer> values) {
            addCriterion("userId not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(Integer value1, Integer value2) {
            addCriterion("userId between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(Integer value1, Integer value2) {
            addCriterion("userId not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andDispriceIsNull() {
            addCriterion("disprice is null");
            return (Criteria) this;
        }

        public Criteria andDispriceIsNotNull() {
            addCriterion("disprice is not null");
            return (Criteria) this;
        }

        public Criteria andDispriceEqualTo(BigDecimal value) {
            addCriterion("disprice =", value, "disprice");
            return (Criteria) this;
        }

        public Criteria andDispriceNotEqualTo(BigDecimal value) {
            addCriterion("disprice <>", value, "disprice");
            return (Criteria) this;
        }

        public Criteria andDispriceGreaterThan(BigDecimal value) {
            addCriterion("disprice >", value, "disprice");
            return (Criteria) this;
        }

        public Criteria andDispriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("disprice >=", value, "disprice");
            return (Criteria) this;
        }

        public Criteria andDispriceLessThan(BigDecimal value) {
            addCriterion("disprice <", value, "disprice");
            return (Criteria) this;
        }

        public Criteria andDispriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("disprice <=", value, "disprice");
            return (Criteria) this;
        }

        public Criteria andDispriceIn(List<BigDecimal> values) {
            addCriterion("disprice in", values, "disprice");
            return (Criteria) this;
        }

        public Criteria andDispriceNotIn(List<BigDecimal> values) {
            addCriterion("disprice not in", values, "disprice");
            return (Criteria) this;
        }

        public Criteria andDispriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("disprice between", value1, value2, "disprice");
            return (Criteria) this;
        }

        public Criteria andDispriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("disprice not between", value1, value2, "disprice");
            return (Criteria) this;
        }

        public Criteria andOptionIdsIsNull() {
            addCriterion("optionIds is null");
            return (Criteria) this;
        }

        public Criteria andOptionIdsIsNotNull() {
            addCriterion("optionIds is not null");
            return (Criteria) this;
        }

        public Criteria andOptionIdsEqualTo(String value) {
            addCriterion("optionIds =", value, "optionIds");
            return (Criteria) this;
        }

        public Criteria andOptionIdsNotEqualTo(String value) {
            addCriterion("optionIds <>", value, "optionIds");
            return (Criteria) this;
        }

        public Criteria andOptionIdsGreaterThan(String value) {
            addCriterion("optionIds >", value, "optionIds");
            return (Criteria) this;
        }

        public Criteria andOptionIdsGreaterThanOrEqualTo(String value) {
            addCriterion("optionIds >=", value, "optionIds");
            return (Criteria) this;
        }

        public Criteria andOptionIdsLessThan(String value) {
            addCriterion("optionIds <", value, "optionIds");
            return (Criteria) this;
        }

        public Criteria andOptionIdsLessThanOrEqualTo(String value) {
            addCriterion("optionIds <=", value, "optionIds");
            return (Criteria) this;
        }

        public Criteria andOptionIdsLike(String value) {
            addCriterion("optionIds like", value, "optionIds");
            return (Criteria) this;
        }

        public Criteria andOptionIdsNotLike(String value) {
            addCriterion("optionIds not like", value, "optionIds");
            return (Criteria) this;
        }

        public Criteria andOptionIdsIn(List<String> values) {
            addCriterion("optionIds in", values, "optionIds");
            return (Criteria) this;
        }

        public Criteria andOptionIdsNotIn(List<String> values) {
            addCriterion("optionIds not in", values, "optionIds");
            return (Criteria) this;
        }

        public Criteria andOptionIdsBetween(String value1, String value2) {
            addCriterion("optionIds between", value1, value2, "optionIds");
            return (Criteria) this;
        }

        public Criteria andOptionIdsNotBetween(String value1, String value2) {
            addCriterion("optionIds not between", value1, value2, "optionIds");
            return (Criteria) this;
        }

        public Criteria andPtypeIsNull() {
            addCriterion("ptype is null");
            return (Criteria) this;
        }

        public Criteria andPtypeIsNotNull() {
            addCriterion("ptype is not null");
            return (Criteria) this;
        }

        public Criteria andPtypeEqualTo(Integer value) {
            addCriterion("ptype =", value, "ptype");
            return (Criteria) this;
        }

        public Criteria andPtypeNotEqualTo(Integer value) {
            addCriterion("ptype <>", value, "ptype");
            return (Criteria) this;
        }

        public Criteria andPtypeGreaterThan(Integer value) {
            addCriterion("ptype >", value, "ptype");
            return (Criteria) this;
        }

        public Criteria andPtypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("ptype >=", value, "ptype");
            return (Criteria) this;
        }

        public Criteria andPtypeLessThan(Integer value) {
            addCriterion("ptype <", value, "ptype");
            return (Criteria) this;
        }

        public Criteria andPtypeLessThanOrEqualTo(Integer value) {
            addCriterion("ptype <=", value, "ptype");
            return (Criteria) this;
        }

        public Criteria andPtypeIn(List<Integer> values) {
            addCriterion("ptype in", values, "ptype");
            return (Criteria) this;
        }

        public Criteria andPtypeNotIn(List<Integer> values) {
            addCriterion("ptype not in", values, "ptype");
            return (Criteria) this;
        }

        public Criteria andPtypeBetween(Integer value1, Integer value2) {
            addCriterion("ptype between", value1, value2, "ptype");
            return (Criteria) this;
        }

        public Criteria andPtypeNotBetween(Integer value1, Integer value2) {
            addCriterion("ptype not between", value1, value2, "ptype");
            return (Criteria) this;
        }

        public Criteria andActivityIsNull() {
            addCriterion("activity is null");
            return (Criteria) this;
        }

        public Criteria andActivityIsNotNull() {
            addCriterion("activity is not null");
            return (Criteria) this;
        }

        public Criteria andActivityEqualTo(Integer value) {
            addCriterion("activity =", value, "activity");
            return (Criteria) this;
        }

        public Criteria andActivityNotEqualTo(Integer value) {
            addCriterion("activity <>", value, "activity");
            return (Criteria) this;
        }

        public Criteria andActivityGreaterThan(Integer value) {
            addCriterion("activity >", value, "activity");
            return (Criteria) this;
        }

        public Criteria andActivityGreaterThanOrEqualTo(Integer value) {
            addCriterion("activity >=", value, "activity");
            return (Criteria) this;
        }

        public Criteria andActivityLessThan(Integer value) {
            addCriterion("activity <", value, "activity");
            return (Criteria) this;
        }

        public Criteria andActivityLessThanOrEqualTo(Integer value) {
            addCriterion("activity <=", value, "activity");
            return (Criteria) this;
        }

        public Criteria andActivityIn(List<Integer> values) {
            addCriterion("activity in", values, "activity");
            return (Criteria) this;
        }

        public Criteria andActivityNotIn(List<Integer> values) {
            addCriterion("activity not in", values, "activity");
            return (Criteria) this;
        }

        public Criteria andActivityBetween(Integer value1, Integer value2) {
            addCriterion("activity between", value1, value2, "activity");
            return (Criteria) this;
        }

        public Criteria andActivityNotBetween(Integer value1, Integer value2) {
            addCriterion("activity not between", value1, value2, "activity");
            return (Criteria) this;
        }

        public Criteria andErrendIsNull() {
            addCriterion("errend is null");
            return (Criteria) this;
        }

        public Criteria andErrendIsNotNull() {
            addCriterion("errend is not null");
            return (Criteria) this;
        }

        public Criteria andErrendEqualTo(Integer value) {
            addCriterion("errend =", value, "errend");
            return (Criteria) this;
        }

        public Criteria andErrendNotEqualTo(Integer value) {
            addCriterion("errend <>", value, "errend");
            return (Criteria) this;
        }

        public Criteria andErrendGreaterThan(Integer value) {
            addCriterion("errend >", value, "errend");
            return (Criteria) this;
        }

        public Criteria andErrendGreaterThanOrEqualTo(Integer value) {
            addCriterion("errend >=", value, "errend");
            return (Criteria) this;
        }

        public Criteria andErrendLessThan(Integer value) {
            addCriterion("errend <", value, "errend");
            return (Criteria) this;
        }

        public Criteria andErrendLessThanOrEqualTo(Integer value) {
            addCriterion("errend <=", value, "errend");
            return (Criteria) this;
        }

        public Criteria andErrendIn(List<Integer> values) {
            addCriterion("errend in", values, "errend");
            return (Criteria) this;
        }

        public Criteria andErrendNotIn(List<Integer> values) {
            addCriterion("errend not in", values, "errend");
            return (Criteria) this;
        }

        public Criteria andErrendBetween(Integer value1, Integer value2) {
            addCriterion("errend between", value1, value2, "errend");
            return (Criteria) this;
        }

        public Criteria andErrendNotBetween(Integer value1, Integer value2) {
            addCriterion("errend not between", value1, value2, "errend");
            return (Criteria) this;
        }

        public Criteria andActivityIdIsNull() {
            addCriterion("activityId is null");
            return (Criteria) this;
        }

        public Criteria andActivityIdIsNotNull() {
            addCriterion("activityId is not null");
            return (Criteria) this;
        }

        public Criteria andActivityIdEqualTo(Integer value) {
            addCriterion("activityId =", value, "activityId");
            return (Criteria) this;
        }

        public Criteria andActivityIdNotEqualTo(Integer value) {
            addCriterion("activityId <>", value, "activityId");
            return (Criteria) this;
        }

        public Criteria andActivityIdGreaterThan(Integer value) {
            addCriterion("activityId >", value, "activityId");
            return (Criteria) this;
        }

        public Criteria andActivityIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("activityId >=", value, "activityId");
            return (Criteria) this;
        }

        public Criteria andActivityIdLessThan(Integer value) {
            addCriterion("activityId <", value, "activityId");
            return (Criteria) this;
        }

        public Criteria andActivityIdLessThanOrEqualTo(Integer value) {
            addCriterion("activityId <=", value, "activityId");
            return (Criteria) this;
        }

        public Criteria andActivityIdIn(List<Integer> values) {
            addCriterion("activityId in", values, "activityId");
            return (Criteria) this;
        }

        public Criteria andActivityIdNotIn(List<Integer> values) {
            addCriterion("activityId not in", values, "activityId");
            return (Criteria) this;
        }

        public Criteria andActivityIdBetween(Integer value1, Integer value2) {
            addCriterion("activityId between", value1, value2, "activityId");
            return (Criteria) this;
        }

        public Criteria andActivityIdNotBetween(Integer value1, Integer value2) {
            addCriterion("activityId not between", value1, value2, "activityId");
            return (Criteria) this;
        }

        public Criteria andActivityNameIsNull() {
            addCriterion("activityName is null");
            return (Criteria) this;
        }

        public Criteria andActivityNameIsNotNull() {
            addCriterion("activityName is not null");
            return (Criteria) this;
        }

        public Criteria andActivityNameEqualTo(String value) {
            addCriterion("activityName =", value, "activityName");
            return (Criteria) this;
        }

        public Criteria andActivityNameNotEqualTo(String value) {
            addCriterion("activityName <>", value, "activityName");
            return (Criteria) this;
        }

        public Criteria andActivityNameGreaterThan(String value) {
            addCriterion("activityName >", value, "activityName");
            return (Criteria) this;
        }

        public Criteria andActivityNameGreaterThanOrEqualTo(String value) {
            addCriterion("activityName >=", value, "activityName");
            return (Criteria) this;
        }

        public Criteria andActivityNameLessThan(String value) {
            addCriterion("activityName <", value, "activityName");
            return (Criteria) this;
        }

        public Criteria andActivityNameLessThanOrEqualTo(String value) {
            addCriterion("activityName <=", value, "activityName");
            return (Criteria) this;
        }

        public Criteria andActivityNameLike(String value) {
            addCriterion("activityName like", value, "activityName");
            return (Criteria) this;
        }

        public Criteria andActivityNameNotLike(String value) {
            addCriterion("activityName not like", value, "activityName");
            return (Criteria) this;
        }

        public Criteria andActivityNameIn(List<String> values) {
            addCriterion("activityName in", values, "activityName");
            return (Criteria) this;
        }

        public Criteria andActivityNameNotIn(List<String> values) {
            addCriterion("activityName not in", values, "activityName");
            return (Criteria) this;
        }

        public Criteria andActivityNameBetween(String value1, String value2) {
            addCriterion("activityName between", value1, value2, "activityName");
            return (Criteria) this;
        }

        public Criteria andActivityNameNotBetween(String value1, String value2) {
            addCriterion("activityName not between", value1, value2, "activityName");
            return (Criteria) this;
        }

        public Criteria andActivityTypeIsNull() {
            addCriterion("activityType is null");
            return (Criteria) this;
        }

        public Criteria andActivityTypeIsNotNull() {
            addCriterion("activityType is not null");
            return (Criteria) this;
        }

        public Criteria andActivityTypeEqualTo(Integer value) {
            addCriterion("activityType =", value, "activityType");
            return (Criteria) this;
        }

        public Criteria andActivityTypeNotEqualTo(Integer value) {
            addCriterion("activityType <>", value, "activityType");
            return (Criteria) this;
        }

        public Criteria andActivityTypeGreaterThan(Integer value) {
            addCriterion("activityType >", value, "activityType");
            return (Criteria) this;
        }

        public Criteria andActivityTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("activityType >=", value, "activityType");
            return (Criteria) this;
        }

        public Criteria andActivityTypeLessThan(Integer value) {
            addCriterion("activityType <", value, "activityType");
            return (Criteria) this;
        }

        public Criteria andActivityTypeLessThanOrEqualTo(Integer value) {
            addCriterion("activityType <=", value, "activityType");
            return (Criteria) this;
        }

        public Criteria andActivityTypeIn(List<Integer> values) {
            addCriterion("activityType in", values, "activityType");
            return (Criteria) this;
        }

        public Criteria andActivityTypeNotIn(List<Integer> values) {
            addCriterion("activityType not in", values, "activityType");
            return (Criteria) this;
        }

        public Criteria andActivityTypeBetween(Integer value1, Integer value2) {
            addCriterion("activityType between", value1, value2, "activityType");
            return (Criteria) this;
        }

        public Criteria andActivityTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("activityType not between", value1, value2, "activityType");
            return (Criteria) this;
        }

        public Criteria andGoodsIsNull() {
            addCriterion("goods is null");
            return (Criteria) this;
        }

        public Criteria andGoodsIsNotNull() {
            addCriterion("goods is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsEqualTo(String value) {
            addCriterion("goods =", value, "goods");
            return (Criteria) this;
        }

        public Criteria andGoodsNotEqualTo(String value) {
            addCriterion("goods <>", value, "goods");
            return (Criteria) this;
        }

        public Criteria andGoodsGreaterThan(String value) {
            addCriterion("goods >", value, "goods");
            return (Criteria) this;
        }

        public Criteria andGoodsGreaterThanOrEqualTo(String value) {
            addCriterion("goods >=", value, "goods");
            return (Criteria) this;
        }

        public Criteria andGoodsLessThan(String value) {
            addCriterion("goods <", value, "goods");
            return (Criteria) this;
        }

        public Criteria andGoodsLessThanOrEqualTo(String value) {
            addCriterion("goods <=", value, "goods");
            return (Criteria) this;
        }

        public Criteria andGoodsLike(String value) {
            addCriterion("goods like", value, "goods");
            return (Criteria) this;
        }

        public Criteria andGoodsNotLike(String value) {
            addCriterion("goods not like", value, "goods");
            return (Criteria) this;
        }

        public Criteria andGoodsIn(List<String> values) {
            addCriterion("goods in", values, "goods");
            return (Criteria) this;
        }

        public Criteria andGoodsNotIn(List<String> values) {
            addCriterion("goods not in", values, "goods");
            return (Criteria) this;
        }

        public Criteria andGoodsBetween(String value1, String value2) {
            addCriterion("goods between", value1, value2, "goods");
            return (Criteria) this;
        }

        public Criteria andGoodsNotBetween(String value1, String value2) {
            addCriterion("goods not between", value1, value2, "goods");
            return (Criteria) this;
        }

        public Criteria andTotalDispriceIsNull() {
            addCriterion("totalDisprice is null");
            return (Criteria) this;
        }

        public Criteria andTotalDispriceIsNotNull() {
            addCriterion("totalDisprice is not null");
            return (Criteria) this;
        }

        public Criteria andTotalDispriceEqualTo(Double value) {
            addCriterion("totalDisprice =", value, "totalDisprice");
            return (Criteria) this;
        }

        public Criteria andTotalDispriceNotEqualTo(Double value) {
            addCriterion("totalDisprice <>", value, "totalDisprice");
            return (Criteria) this;
        }

        public Criteria andTotalDispriceGreaterThan(Double value) {
            addCriterion("totalDisprice >", value, "totalDisprice");
            return (Criteria) this;
        }

        public Criteria andTotalDispriceGreaterThanOrEqualTo(Double value) {
            addCriterion("totalDisprice >=", value, "totalDisprice");
            return (Criteria) this;
        }

        public Criteria andTotalDispriceLessThan(Double value) {
            addCriterion("totalDisprice <", value, "totalDisprice");
            return (Criteria) this;
        }

        public Criteria andTotalDispriceLessThanOrEqualTo(Double value) {
            addCriterion("totalDisprice <=", value, "totalDisprice");
            return (Criteria) this;
        }

        public Criteria andTotalDispriceIn(List<Double> values) {
            addCriterion("totalDisprice in", values, "totalDisprice");
            return (Criteria) this;
        }

        public Criteria andTotalDispriceNotIn(List<Double> values) {
            addCriterion("totalDisprice not in", values, "totalDisprice");
            return (Criteria) this;
        }

        public Criteria andTotalDispriceBetween(Double value1, Double value2) {
            addCriterion("totalDisprice between", value1, value2, "totalDisprice");
            return (Criteria) this;
        }

        public Criteria andTotalDispriceNotBetween(Double value1, Double value2) {
            addCriterion("totalDisprice not between", value1, value2, "totalDisprice");
            return (Criteria) this;
        }

        public Criteria andStandarOptionNameIsNull() {
            addCriterion("standarOptionName is null");
            return (Criteria) this;
        }

        public Criteria andStandarOptionNameIsNotNull() {
            addCriterion("standarOptionName is not null");
            return (Criteria) this;
        }

        public Criteria andStandarOptionNameEqualTo(String value) {
            addCriterion("standarOptionName =", value, "standarOptionName");
            return (Criteria) this;
        }

        public Criteria andStandarOptionNameNotEqualTo(String value) {
            addCriterion("standarOptionName <>", value, "standarOptionName");
            return (Criteria) this;
        }

        public Criteria andStandarOptionNameGreaterThan(String value) {
            addCriterion("standarOptionName >", value, "standarOptionName");
            return (Criteria) this;
        }

        public Criteria andStandarOptionNameGreaterThanOrEqualTo(String value) {
            addCriterion("standarOptionName >=", value, "standarOptionName");
            return (Criteria) this;
        }

        public Criteria andStandarOptionNameLessThan(String value) {
            addCriterion("standarOptionName <", value, "standarOptionName");
            return (Criteria) this;
        }

        public Criteria andStandarOptionNameLessThanOrEqualTo(String value) {
            addCriterion("standarOptionName <=", value, "standarOptionName");
            return (Criteria) this;
        }

        public Criteria andStandarOptionNameLike(String value) {
            addCriterion("standarOptionName like", value, "standarOptionName");
            return (Criteria) this;
        }

        public Criteria andStandarOptionNameNotLike(String value) {
            addCriterion("standarOptionName not like", value, "standarOptionName");
            return (Criteria) this;
        }

        public Criteria andStandarOptionNameIn(List<String> values) {
            addCriterion("standarOptionName in", values, "standarOptionName");
            return (Criteria) this;
        }

        public Criteria andStandarOptionNameNotIn(List<String> values) {
            addCriterion("standarOptionName not in", values, "standarOptionName");
            return (Criteria) this;
        }

        public Criteria andStandarOptionNameBetween(String value1, String value2) {
            addCriterion("standarOptionName between", value1, value2, "standarOptionName");
            return (Criteria) this;
        }

        public Criteria andStandarOptionNameNotBetween(String value1, String value2) {
            addCriterion("standarOptionName not between", value1, value2, "standarOptionName");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}