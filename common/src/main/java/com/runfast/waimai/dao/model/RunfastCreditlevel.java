package com.runfast.waimai.dao.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class RunfastCreditlevel implements Serializable {
    private Integer id;

    private BigDecimal commission;

    private Date createTime;

    private Integer endgrade;

    private Integer sort;

    private Integer startgrade;

    private BigDecimal addprice;

    private BigDecimal commissionDay;

    private BigDecimal commissionNight;

    private Date endTimeDay;

    private Date endTimeNight;

    private Date startTimeDay;

    private Date startTimeNight;

    private Integer agentId;

    private String agentName;

    private Double distan;

    private BigDecimal price;

    private BigDecimal maxprice;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public RunfastCreditlevel withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getCommission() {
        return commission;
    }

    public RunfastCreditlevel withCommission(BigDecimal commission) {
        this.setCommission(commission);
        return this;
    }

    public void setCommission(BigDecimal commission) {
        this.commission = commission;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public RunfastCreditlevel withCreateTime(Date createTime) {
        this.setCreateTime(createTime);
        return this;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getEndgrade() {
        return endgrade;
    }

    public RunfastCreditlevel withEndgrade(Integer endgrade) {
        this.setEndgrade(endgrade);
        return this;
    }

    public void setEndgrade(Integer endgrade) {
        this.endgrade = endgrade;
    }

    public Integer getSort() {
        return sort;
    }

    public RunfastCreditlevel withSort(Integer sort) {
        this.setSort(sort);
        return this;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getStartgrade() {
        return startgrade;
    }

    public RunfastCreditlevel withStartgrade(Integer startgrade) {
        this.setStartgrade(startgrade);
        return this;
    }

    public void setStartgrade(Integer startgrade) {
        this.startgrade = startgrade;
    }

    public BigDecimal getAddprice() {
        return addprice;
    }

    public RunfastCreditlevel withAddprice(BigDecimal addprice) {
        this.setAddprice(addprice);
        return this;
    }

    public void setAddprice(BigDecimal addprice) {
        this.addprice = addprice;
    }

    public BigDecimal getCommissionDay() {
        return commissionDay;
    }

    public RunfastCreditlevel withCommissionDay(BigDecimal commissionDay) {
        this.setCommissionDay(commissionDay);
        return this;
    }

    public void setCommissionDay(BigDecimal commissionDay) {
        this.commissionDay = commissionDay;
    }

    public BigDecimal getCommissionNight() {
        return commissionNight;
    }

    public RunfastCreditlevel withCommissionNight(BigDecimal commissionNight) {
        this.setCommissionNight(commissionNight);
        return this;
    }

    public void setCommissionNight(BigDecimal commissionNight) {
        this.commissionNight = commissionNight;
    }

    public Date getEndTimeDay() {
        return endTimeDay;
    }

    public RunfastCreditlevel withEndTimeDay(Date endTimeDay) {
        this.setEndTimeDay(endTimeDay);
        return this;
    }

    public void setEndTimeDay(Date endTimeDay) {
        this.endTimeDay = endTimeDay;
    }

    public Date getEndTimeNight() {
        return endTimeNight;
    }

    public RunfastCreditlevel withEndTimeNight(Date endTimeNight) {
        this.setEndTimeNight(endTimeNight);
        return this;
    }

    public void setEndTimeNight(Date endTimeNight) {
        this.endTimeNight = endTimeNight;
    }

    public Date getStartTimeDay() {
        return startTimeDay;
    }

    public RunfastCreditlevel withStartTimeDay(Date startTimeDay) {
        this.setStartTimeDay(startTimeDay);
        return this;
    }

    public void setStartTimeDay(Date startTimeDay) {
        this.startTimeDay = startTimeDay;
    }

    public Date getStartTimeNight() {
        return startTimeNight;
    }

    public RunfastCreditlevel withStartTimeNight(Date startTimeNight) {
        this.setStartTimeNight(startTimeNight);
        return this;
    }

    public void setStartTimeNight(Date startTimeNight) {
        this.startTimeNight = startTimeNight;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public RunfastCreditlevel withAgentId(Integer agentId) {
        this.setAgentId(agentId);
        return this;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public String getAgentName() {
        return agentName;
    }

    public RunfastCreditlevel withAgentName(String agentName) {
        this.setAgentName(agentName);
        return this;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName == null ? null : agentName.trim();
    }

    public Double getDistan() {
        return distan;
    }

    public RunfastCreditlevel withDistan(Double distan) {
        this.setDistan(distan);
        return this;
    }

    public void setDistan(Double distan) {
        this.distan = distan;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public RunfastCreditlevel withPrice(BigDecimal price) {
        this.setPrice(price);
        return this;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getMaxprice() {
        return maxprice;
    }

    public RunfastCreditlevel withMaxprice(BigDecimal maxprice) {
        this.setMaxprice(maxprice);
        return this;
    }

    public void setMaxprice(BigDecimal maxprice) {
        this.maxprice = maxprice;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", commission=").append(commission);
        sb.append(", createTime=").append(createTime);
        sb.append(", endgrade=").append(endgrade);
        sb.append(", sort=").append(sort);
        sb.append(", startgrade=").append(startgrade);
        sb.append(", addprice=").append(addprice);
        sb.append(", commissionDay=").append(commissionDay);
        sb.append(", commissionNight=").append(commissionNight);
        sb.append(", endTimeDay=").append(endTimeDay);
        sb.append(", endTimeNight=").append(endTimeNight);
        sb.append(", startTimeDay=").append(startTimeDay);
        sb.append(", startTimeNight=").append(startTimeNight);
        sb.append(", agentId=").append(agentId);
        sb.append(", agentName=").append(agentName);
        sb.append(", distan=").append(distan);
        sb.append(", price=").append(price);
        sb.append(", maxprice=").append(maxprice);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}