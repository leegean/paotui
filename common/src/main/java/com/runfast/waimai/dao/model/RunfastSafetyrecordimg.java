package com.runfast.waimai.dao.model;

import java.io.Serializable;

public class RunfastSafetyrecordimg implements Serializable {
    private Integer id;

    private Integer busID;

    private String imgUrl;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public RunfastSafetyrecordimg withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBusID() {
        return busID;
    }

    public RunfastSafetyrecordimg withBusID(Integer busID) {
        this.setBusID(busID);
        return this;
    }

    public void setBusID(Integer busID) {
        this.busID = busID;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public RunfastSafetyrecordimg withImgUrl(String imgUrl) {
        this.setImgUrl(imgUrl);
        return this;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl == null ? null : imgUrl.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", busID=").append(busID);
        sb.append(", imgUrl=").append(imgUrl);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}