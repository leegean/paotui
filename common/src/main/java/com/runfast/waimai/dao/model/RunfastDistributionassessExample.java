package com.runfast.waimai.dao.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RunfastDistributionassessExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public RunfastDistributionassessExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andAssessTimeIsNull() {
            addCriterion("assessTime is null");
            return (Criteria) this;
        }

        public Criteria andAssessTimeIsNotNull() {
            addCriterion("assessTime is not null");
            return (Criteria) this;
        }

        public Criteria andAssessTimeEqualTo(Date value) {
            addCriterion("assessTime =", value, "assessTime");
            return (Criteria) this;
        }

        public Criteria andAssessTimeNotEqualTo(Date value) {
            addCriterion("assessTime <>", value, "assessTime");
            return (Criteria) this;
        }

        public Criteria andAssessTimeGreaterThan(Date value) {
            addCriterion("assessTime >", value, "assessTime");
            return (Criteria) this;
        }

        public Criteria andAssessTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("assessTime >=", value, "assessTime");
            return (Criteria) this;
        }

        public Criteria andAssessTimeLessThan(Date value) {
            addCriterion("assessTime <", value, "assessTime");
            return (Criteria) this;
        }

        public Criteria andAssessTimeLessThanOrEqualTo(Date value) {
            addCriterion("assessTime <=", value, "assessTime");
            return (Criteria) this;
        }

        public Criteria andAssessTimeIn(List<Date> values) {
            addCriterion("assessTime in", values, "assessTime");
            return (Criteria) this;
        }

        public Criteria andAssessTimeNotIn(List<Date> values) {
            addCriterion("assessTime not in", values, "assessTime");
            return (Criteria) this;
        }

        public Criteria andAssessTimeBetween(Date value1, Date value2) {
            addCriterion("assessTime between", value1, value2, "assessTime");
            return (Criteria) this;
        }

        public Criteria andAssessTimeNotBetween(Date value1, Date value2) {
            addCriterion("assessTime not between", value1, value2, "assessTime");
            return (Criteria) this;
        }

        public Criteria andAssessmanIsNull() {
            addCriterion("assessman is null");
            return (Criteria) this;
        }

        public Criteria andAssessmanIsNotNull() {
            addCriterion("assessman is not null");
            return (Criteria) this;
        }

        public Criteria andAssessmanEqualTo(String value) {
            addCriterion("assessman =", value, "assessman");
            return (Criteria) this;
        }

        public Criteria andAssessmanNotEqualTo(String value) {
            addCriterion("assessman <>", value, "assessman");
            return (Criteria) this;
        }

        public Criteria andAssessmanGreaterThan(String value) {
            addCriterion("assessman >", value, "assessman");
            return (Criteria) this;
        }

        public Criteria andAssessmanGreaterThanOrEqualTo(String value) {
            addCriterion("assessman >=", value, "assessman");
            return (Criteria) this;
        }

        public Criteria andAssessmanLessThan(String value) {
            addCriterion("assessman <", value, "assessman");
            return (Criteria) this;
        }

        public Criteria andAssessmanLessThanOrEqualTo(String value) {
            addCriterion("assessman <=", value, "assessman");
            return (Criteria) this;
        }

        public Criteria andAssessmanLike(String value) {
            addCriterion("assessman like", value, "assessman");
            return (Criteria) this;
        }

        public Criteria andAssessmanNotLike(String value) {
            addCriterion("assessman not like", value, "assessman");
            return (Criteria) this;
        }

        public Criteria andAssessmanIn(List<String> values) {
            addCriterion("assessman in", values, "assessman");
            return (Criteria) this;
        }

        public Criteria andAssessmanNotIn(List<String> values) {
            addCriterion("assessman not in", values, "assessman");
            return (Criteria) this;
        }

        public Criteria andAssessmanBetween(String value1, String value2) {
            addCriterion("assessman between", value1, value2, "assessman");
            return (Criteria) this;
        }

        public Criteria andAssessmanNotBetween(String value1, String value2) {
            addCriterion("assessman not between", value1, value2, "assessman");
            return (Criteria) this;
        }

        public Criteria andConsumeIsNull() {
            addCriterion("consume is null");
            return (Criteria) this;
        }

        public Criteria andConsumeIsNotNull() {
            addCriterion("consume is not null");
            return (Criteria) this;
        }

        public Criteria andConsumeEqualTo(BigDecimal value) {
            addCriterion("consume =", value, "consume");
            return (Criteria) this;
        }

        public Criteria andConsumeNotEqualTo(BigDecimal value) {
            addCriterion("consume <>", value, "consume");
            return (Criteria) this;
        }

        public Criteria andConsumeGreaterThan(BigDecimal value) {
            addCriterion("consume >", value, "consume");
            return (Criteria) this;
        }

        public Criteria andConsumeGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("consume >=", value, "consume");
            return (Criteria) this;
        }

        public Criteria andConsumeLessThan(BigDecimal value) {
            addCriterion("consume <", value, "consume");
            return (Criteria) this;
        }

        public Criteria andConsumeLessThanOrEqualTo(BigDecimal value) {
            addCriterion("consume <=", value, "consume");
            return (Criteria) this;
        }

        public Criteria andConsumeIn(List<BigDecimal> values) {
            addCriterion("consume in", values, "consume");
            return (Criteria) this;
        }

        public Criteria andConsumeNotIn(List<BigDecimal> values) {
            addCriterion("consume not in", values, "consume");
            return (Criteria) this;
        }

        public Criteria andConsumeBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("consume between", value1, value2, "consume");
            return (Criteria) this;
        }

        public Criteria andConsumeNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("consume not between", value1, value2, "consume");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeIsNull() {
            addCriterion("distributionTime is null");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeIsNotNull() {
            addCriterion("distributionTime is not null");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeEqualTo(Date value) {
            addCriterion("distributionTime =", value, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeNotEqualTo(Date value) {
            addCriterion("distributionTime <>", value, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeGreaterThan(Date value) {
            addCriterion("distributionTime >", value, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("distributionTime >=", value, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeLessThan(Date value) {
            addCriterion("distributionTime <", value, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeLessThanOrEqualTo(Date value) {
            addCriterion("distributionTime <=", value, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeIn(List<Date> values) {
            addCriterion("distributionTime in", values, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeNotIn(List<Date> values) {
            addCriterion("distributionTime not in", values, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeBetween(Date value1, Date value2) {
            addCriterion("distributionTime between", value1, value2, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeNotBetween(Date value1, Date value2) {
            addCriterion("distributionTime not between", value1, value2, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andLevelIsNull() {
            addCriterion("level is null");
            return (Criteria) this;
        }

        public Criteria andLevelIsNotNull() {
            addCriterion("level is not null");
            return (Criteria) this;
        }

        public Criteria andLevelEqualTo(Integer value) {
            addCriterion("level =", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelNotEqualTo(Integer value) {
            addCriterion("level <>", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelGreaterThan(Integer value) {
            addCriterion("level >", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelGreaterThanOrEqualTo(Integer value) {
            addCriterion("level >=", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelLessThan(Integer value) {
            addCriterion("level <", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelLessThanOrEqualTo(Integer value) {
            addCriterion("level <=", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelIn(List<Integer> values) {
            addCriterion("level in", values, "level");
            return (Criteria) this;
        }

        public Criteria andLevelNotIn(List<Integer> values) {
            addCriterion("level not in", values, "level");
            return (Criteria) this;
        }

        public Criteria andLevelBetween(Integer value1, Integer value2) {
            addCriterion("level between", value1, value2, "level");
            return (Criteria) this;
        }

        public Criteria andLevelNotBetween(Integer value1, Integer value2) {
            addCriterion("level not between", value1, value2, "level");
            return (Criteria) this;
        }

        public Criteria andOrderNumberIsNull() {
            addCriterion("orderNumber is null");
            return (Criteria) this;
        }

        public Criteria andOrderNumberIsNotNull() {
            addCriterion("orderNumber is not null");
            return (Criteria) this;
        }

        public Criteria andOrderNumberEqualTo(String value) {
            addCriterion("orderNumber =", value, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberNotEqualTo(String value) {
            addCriterion("orderNumber <>", value, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberGreaterThan(String value) {
            addCriterion("orderNumber >", value, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberGreaterThanOrEqualTo(String value) {
            addCriterion("orderNumber >=", value, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberLessThan(String value) {
            addCriterion("orderNumber <", value, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberLessThanOrEqualTo(String value) {
            addCriterion("orderNumber <=", value, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberLike(String value) {
            addCriterion("orderNumber like", value, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberNotLike(String value) {
            addCriterion("orderNumber not like", value, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberIn(List<String> values) {
            addCriterion("orderNumber in", values, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberNotIn(List<String> values) {
            addCriterion("orderNumber not in", values, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberBetween(String value1, String value2) {
            addCriterion("orderNumber between", value1, value2, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberNotBetween(String value1, String value2) {
            addCriterion("orderNumber not between", value1, value2, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andAssessIdIsNull() {
            addCriterion("assessId is null");
            return (Criteria) this;
        }

        public Criteria andAssessIdIsNotNull() {
            addCriterion("assessId is not null");
            return (Criteria) this;
        }

        public Criteria andAssessIdEqualTo(Integer value) {
            addCriterion("assessId =", value, "assessId");
            return (Criteria) this;
        }

        public Criteria andAssessIdNotEqualTo(Integer value) {
            addCriterion("assessId <>", value, "assessId");
            return (Criteria) this;
        }

        public Criteria andAssessIdGreaterThan(Integer value) {
            addCriterion("assessId >", value, "assessId");
            return (Criteria) this;
        }

        public Criteria andAssessIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("assessId >=", value, "assessId");
            return (Criteria) this;
        }

        public Criteria andAssessIdLessThan(Integer value) {
            addCriterion("assessId <", value, "assessId");
            return (Criteria) this;
        }

        public Criteria andAssessIdLessThanOrEqualTo(Integer value) {
            addCriterion("assessId <=", value, "assessId");
            return (Criteria) this;
        }

        public Criteria andAssessIdIn(List<Integer> values) {
            addCriterion("assessId in", values, "assessId");
            return (Criteria) this;
        }

        public Criteria andAssessIdNotIn(List<Integer> values) {
            addCriterion("assessId not in", values, "assessId");
            return (Criteria) this;
        }

        public Criteria andAssessIdBetween(Integer value1, Integer value2) {
            addCriterion("assessId between", value1, value2, "assessId");
            return (Criteria) this;
        }

        public Criteria andAssessIdNotBetween(Integer value1, Integer value2) {
            addCriterion("assessId not between", value1, value2, "assessId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdIsNull() {
            addCriterion("goodsSellRecordId is null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdIsNotNull() {
            addCriterion("goodsSellRecordId is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdEqualTo(Integer value) {
            addCriterion("goodsSellRecordId =", value, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdNotEqualTo(Integer value) {
            addCriterion("goodsSellRecordId <>", value, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdGreaterThan(Integer value) {
            addCriterion("goodsSellRecordId >", value, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("goodsSellRecordId >=", value, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdLessThan(Integer value) {
            addCriterion("goodsSellRecordId <", value, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdLessThanOrEqualTo(Integer value) {
            addCriterion("goodsSellRecordId <=", value, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdIn(List<Integer> values) {
            addCriterion("goodsSellRecordId in", values, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdNotIn(List<Integer> values) {
            addCriterion("goodsSellRecordId not in", values, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdBetween(Integer value1, Integer value2) {
            addCriterion("goodsSellRecordId between", value1, value2, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdNotBetween(Integer value1, Integer value2) {
            addCriterion("goodsSellRecordId not between", value1, value2, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGradeIsNull() {
            addCriterion("grade is null");
            return (Criteria) this;
        }

        public Criteria andGradeIsNotNull() {
            addCriterion("grade is not null");
            return (Criteria) this;
        }

        public Criteria andGradeEqualTo(Integer value) {
            addCriterion("grade =", value, "grade");
            return (Criteria) this;
        }

        public Criteria andGradeNotEqualTo(Integer value) {
            addCriterion("grade <>", value, "grade");
            return (Criteria) this;
        }

        public Criteria andGradeGreaterThan(Integer value) {
            addCriterion("grade >", value, "grade");
            return (Criteria) this;
        }

        public Criteria andGradeGreaterThanOrEqualTo(Integer value) {
            addCriterion("grade >=", value, "grade");
            return (Criteria) this;
        }

        public Criteria andGradeLessThan(Integer value) {
            addCriterion("grade <", value, "grade");
            return (Criteria) this;
        }

        public Criteria andGradeLessThanOrEqualTo(Integer value) {
            addCriterion("grade <=", value, "grade");
            return (Criteria) this;
        }

        public Criteria andGradeIn(List<Integer> values) {
            addCriterion("grade in", values, "grade");
            return (Criteria) this;
        }

        public Criteria andGradeNotIn(List<Integer> values) {
            addCriterion("grade not in", values, "grade");
            return (Criteria) this;
        }

        public Criteria andGradeBetween(Integer value1, Integer value2) {
            addCriterion("grade between", value1, value2, "grade");
            return (Criteria) this;
        }

        public Criteria andGradeNotBetween(Integer value1, Integer value2) {
            addCriterion("grade not between", value1, value2, "grade");
            return (Criteria) this;
        }

        public Criteria andOrderCodeIsNull() {
            addCriterion("orderCode is null");
            return (Criteria) this;
        }

        public Criteria andOrderCodeIsNotNull() {
            addCriterion("orderCode is not null");
            return (Criteria) this;
        }

        public Criteria andOrderCodeEqualTo(String value) {
            addCriterion("orderCode =", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeNotEqualTo(String value) {
            addCriterion("orderCode <>", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeGreaterThan(String value) {
            addCriterion("orderCode >", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeGreaterThanOrEqualTo(String value) {
            addCriterion("orderCode >=", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeLessThan(String value) {
            addCriterion("orderCode <", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeLessThanOrEqualTo(String value) {
            addCriterion("orderCode <=", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeLike(String value) {
            addCriterion("orderCode like", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeNotLike(String value) {
            addCriterion("orderCode not like", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeIn(List<String> values) {
            addCriterion("orderCode in", values, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeNotIn(List<String> values) {
            addCriterion("orderCode not in", values, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeBetween(String value1, String value2) {
            addCriterion("orderCode between", value1, value2, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeNotBetween(String value1, String value2) {
            addCriterion("orderCode not between", value1, value2, "orderCode");
            return (Criteria) this;
        }

        public Criteria andPriceIsNull() {
            addCriterion("price is null");
            return (Criteria) this;
        }

        public Criteria andPriceIsNotNull() {
            addCriterion("price is not null");
            return (Criteria) this;
        }

        public Criteria andPriceEqualTo(Double value) {
            addCriterion("price =", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotEqualTo(Double value) {
            addCriterion("price <>", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceGreaterThan(Double value) {
            addCriterion("price >", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceGreaterThanOrEqualTo(Double value) {
            addCriterion("price >=", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceLessThan(Double value) {
            addCriterion("price <", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceLessThanOrEqualTo(Double value) {
            addCriterion("price <=", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceIn(List<Double> values) {
            addCriterion("price in", values, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotIn(List<Double> values) {
            addCriterion("price not in", values, "price");
            return (Criteria) this;
        }

        public Criteria andPriceBetween(Double value1, Double value2) {
            addCriterion("price between", value1, value2, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotBetween(Double value1, Double value2) {
            addCriterion("price not between", value1, value2, "price");
            return (Criteria) this;
        }

        public Criteria andScoreIsNull() {
            addCriterion("score is null");
            return (Criteria) this;
        }

        public Criteria andScoreIsNotNull() {
            addCriterion("score is not null");
            return (Criteria) this;
        }

        public Criteria andScoreEqualTo(Double value) {
            addCriterion("score =", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotEqualTo(Double value) {
            addCriterion("score <>", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreGreaterThan(Double value) {
            addCriterion("score >", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreGreaterThanOrEqualTo(Double value) {
            addCriterion("score >=", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreLessThan(Double value) {
            addCriterion("score <", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreLessThanOrEqualTo(Double value) {
            addCriterion("score <=", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreIn(List<Double> values) {
            addCriterion("score in", values, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotIn(List<Double> values) {
            addCriterion("score not in", values, "score");
            return (Criteria) this;
        }

        public Criteria andScoreBetween(Double value1, Double value2) {
            addCriterion("score between", value1, value2, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotBetween(Double value1, Double value2) {
            addCriterion("score not between", value1, value2, "score");
            return (Criteria) this;
        }

        public Criteria andShopperIdIsNull() {
            addCriterion("shopperId is null");
            return (Criteria) this;
        }

        public Criteria andShopperIdIsNotNull() {
            addCriterion("shopperId is not null");
            return (Criteria) this;
        }

        public Criteria andShopperIdEqualTo(Integer value) {
            addCriterion("shopperId =", value, "shopperId");
            return (Criteria) this;
        }

        public Criteria andShopperIdNotEqualTo(Integer value) {
            addCriterion("shopperId <>", value, "shopperId");
            return (Criteria) this;
        }

        public Criteria andShopperIdGreaterThan(Integer value) {
            addCriterion("shopperId >", value, "shopperId");
            return (Criteria) this;
        }

        public Criteria andShopperIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("shopperId >=", value, "shopperId");
            return (Criteria) this;
        }

        public Criteria andShopperIdLessThan(Integer value) {
            addCriterion("shopperId <", value, "shopperId");
            return (Criteria) this;
        }

        public Criteria andShopperIdLessThanOrEqualTo(Integer value) {
            addCriterion("shopperId <=", value, "shopperId");
            return (Criteria) this;
        }

        public Criteria andShopperIdIn(List<Integer> values) {
            addCriterion("shopperId in", values, "shopperId");
            return (Criteria) this;
        }

        public Criteria andShopperIdNotIn(List<Integer> values) {
            addCriterion("shopperId not in", values, "shopperId");
            return (Criteria) this;
        }

        public Criteria andShopperIdBetween(Integer value1, Integer value2) {
            addCriterion("shopperId between", value1, value2, "shopperId");
            return (Criteria) this;
        }

        public Criteria andShopperIdNotBetween(Integer value1, Integer value2) {
            addCriterion("shopperId not between", value1, value2, "shopperId");
            return (Criteria) this;
        }

        public Criteria andShopperNameIsNull() {
            addCriterion("shopperName is null");
            return (Criteria) this;
        }

        public Criteria andShopperNameIsNotNull() {
            addCriterion("shopperName is not null");
            return (Criteria) this;
        }

        public Criteria andShopperNameEqualTo(String value) {
            addCriterion("shopperName =", value, "shopperName");
            return (Criteria) this;
        }

        public Criteria andShopperNameNotEqualTo(String value) {
            addCriterion("shopperName <>", value, "shopperName");
            return (Criteria) this;
        }

        public Criteria andShopperNameGreaterThan(String value) {
            addCriterion("shopperName >", value, "shopperName");
            return (Criteria) this;
        }

        public Criteria andShopperNameGreaterThanOrEqualTo(String value) {
            addCriterion("shopperName >=", value, "shopperName");
            return (Criteria) this;
        }

        public Criteria andShopperNameLessThan(String value) {
            addCriterion("shopperName <", value, "shopperName");
            return (Criteria) this;
        }

        public Criteria andShopperNameLessThanOrEqualTo(String value) {
            addCriterion("shopperName <=", value, "shopperName");
            return (Criteria) this;
        }

        public Criteria andShopperNameLike(String value) {
            addCriterion("shopperName like", value, "shopperName");
            return (Criteria) this;
        }

        public Criteria andShopperNameNotLike(String value) {
            addCriterion("shopperName not like", value, "shopperName");
            return (Criteria) this;
        }

        public Criteria andShopperNameIn(List<String> values) {
            addCriterion("shopperName in", values, "shopperName");
            return (Criteria) this;
        }

        public Criteria andShopperNameNotIn(List<String> values) {
            addCriterion("shopperName not in", values, "shopperName");
            return (Criteria) this;
        }

        public Criteria andShopperNameBetween(String value1, String value2) {
            addCriterion("shopperName between", value1, value2, "shopperName");
            return (Criteria) this;
        }

        public Criteria andShopperNameNotBetween(String value1, String value2) {
            addCriterion("shopperName not between", value1, value2, "shopperName");
            return (Criteria) this;
        }

        public Criteria andShopperTimeIsNull() {
            addCriterion("shopperTime is null");
            return (Criteria) this;
        }

        public Criteria andShopperTimeIsNotNull() {
            addCriterion("shopperTime is not null");
            return (Criteria) this;
        }

        public Criteria andShopperTimeEqualTo(Date value) {
            addCriterion("shopperTime =", value, "shopperTime");
            return (Criteria) this;
        }

        public Criteria andShopperTimeNotEqualTo(Date value) {
            addCriterion("shopperTime <>", value, "shopperTime");
            return (Criteria) this;
        }

        public Criteria andShopperTimeGreaterThan(Date value) {
            addCriterion("shopperTime >", value, "shopperTime");
            return (Criteria) this;
        }

        public Criteria andShopperTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("shopperTime >=", value, "shopperTime");
            return (Criteria) this;
        }

        public Criteria andShopperTimeLessThan(Date value) {
            addCriterion("shopperTime <", value, "shopperTime");
            return (Criteria) this;
        }

        public Criteria andShopperTimeLessThanOrEqualTo(Date value) {
            addCriterion("shopperTime <=", value, "shopperTime");
            return (Criteria) this;
        }

        public Criteria andShopperTimeIn(List<Date> values) {
            addCriterion("shopperTime in", values, "shopperTime");
            return (Criteria) this;
        }

        public Criteria andShopperTimeNotIn(List<Date> values) {
            addCriterion("shopperTime not in", values, "shopperTime");
            return (Criteria) this;
        }

        public Criteria andShopperTimeBetween(Date value1, Date value2) {
            addCriterion("shopperTime between", value1, value2, "shopperTime");
            return (Criteria) this;
        }

        public Criteria andShopperTimeNotBetween(Date value1, Date value2) {
            addCriterion("shopperTime not between", value1, value2, "shopperTime");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNull() {
            addCriterion("userId is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("userId is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(Integer value) {
            addCriterion("userId =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(Integer value) {
            addCriterion("userId <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(Integer value) {
            addCriterion("userId >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("userId >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(Integer value) {
            addCriterion("userId <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(Integer value) {
            addCriterion("userId <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<Integer> values) {
            addCriterion("userId in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<Integer> values) {
            addCriterion("userId not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(Integer value1, Integer value2) {
            addCriterion("userId between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(Integer value1, Integer value2) {
            addCriterion("userId not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserNameIsNull() {
            addCriterion("userName is null");
            return (Criteria) this;
        }

        public Criteria andUserNameIsNotNull() {
            addCriterion("userName is not null");
            return (Criteria) this;
        }

        public Criteria andUserNameEqualTo(String value) {
            addCriterion("userName =", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotEqualTo(String value) {
            addCriterion("userName <>", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameGreaterThan(String value) {
            addCriterion("userName >", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameGreaterThanOrEqualTo(String value) {
            addCriterion("userName >=", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLessThan(String value) {
            addCriterion("userName <", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLessThanOrEqualTo(String value) {
            addCriterion("userName <=", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLike(String value) {
            addCriterion("userName like", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotLike(String value) {
            addCriterion("userName not like", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameIn(List<String> values) {
            addCriterion("userName in", values, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotIn(List<String> values) {
            addCriterion("userName not in", values, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameBetween(String value1, String value2) {
            addCriterion("userName between", value1, value2, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotBetween(String value1, String value2) {
            addCriterion("userName not between", value1, value2, "userName");
            return (Criteria) this;
        }

        public Criteria andContentIsNull() {
            addCriterion("content is null");
            return (Criteria) this;
        }

        public Criteria andContentIsNotNull() {
            addCriterion("content is not null");
            return (Criteria) this;
        }

        public Criteria andContentEqualTo(String value) {
            addCriterion("content =", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotEqualTo(String value) {
            addCriterion("content <>", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentGreaterThan(String value) {
            addCriterion("content >", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentGreaterThanOrEqualTo(String value) {
            addCriterion("content >=", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLessThan(String value) {
            addCriterion("content <", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLessThanOrEqualTo(String value) {
            addCriterion("content <=", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLike(String value) {
            addCriterion("content like", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotLike(String value) {
            addCriterion("content not like", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentIn(List<String> values) {
            addCriterion("content in", values, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotIn(List<String> values) {
            addCriterion("content not in", values, "content");
            return (Criteria) this;
        }

        public Criteria andContentBetween(String value1, String value2) {
            addCriterion("content between", value1, value2, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotBetween(String value1, String value2) {
            addCriterion("content not between", value1, value2, "content");
            return (Criteria) this;
        }

        public Criteria andAgentIdIsNull() {
            addCriterion("agentId is null");
            return (Criteria) this;
        }

        public Criteria andAgentIdIsNotNull() {
            addCriterion("agentId is not null");
            return (Criteria) this;
        }

        public Criteria andAgentIdEqualTo(Integer value) {
            addCriterion("agentId =", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotEqualTo(Integer value) {
            addCriterion("agentId <>", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdGreaterThan(Integer value) {
            addCriterion("agentId >", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("agentId >=", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdLessThan(Integer value) {
            addCriterion("agentId <", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdLessThanOrEqualTo(Integer value) {
            addCriterion("agentId <=", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdIn(List<Integer> values) {
            addCriterion("agentId in", values, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotIn(List<Integer> values) {
            addCriterion("agentId not in", values, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdBetween(Integer value1, Integer value2) {
            addCriterion("agentId between", value1, value2, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotBetween(Integer value1, Integer value2) {
            addCriterion("agentId not between", value1, value2, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentNameIsNull() {
            addCriterion("agentName is null");
            return (Criteria) this;
        }

        public Criteria andAgentNameIsNotNull() {
            addCriterion("agentName is not null");
            return (Criteria) this;
        }

        public Criteria andAgentNameEqualTo(String value) {
            addCriterion("agentName =", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotEqualTo(String value) {
            addCriterion("agentName <>", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameGreaterThan(String value) {
            addCriterion("agentName >", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameGreaterThanOrEqualTo(String value) {
            addCriterion("agentName >=", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameLessThan(String value) {
            addCriterion("agentName <", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameLessThanOrEqualTo(String value) {
            addCriterion("agentName <=", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameLike(String value) {
            addCriterion("agentName like", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotLike(String value) {
            addCriterion("agentName not like", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameIn(List<String> values) {
            addCriterion("agentName in", values, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotIn(List<String> values) {
            addCriterion("agentName not in", values, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameBetween(String value1, String value2) {
            addCriterion("agentName between", value1, value2, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotBetween(String value1, String value2) {
            addCriterion("agentName not between", value1, value2, "agentName");
            return (Criteria) this;
        }

        public Criteria andQishoustrIsNull() {
            addCriterion("qishoustr is null");
            return (Criteria) this;
        }

        public Criteria andQishoustrIsNotNull() {
            addCriterion("qishoustr is not null");
            return (Criteria) this;
        }

        public Criteria andQishoustrEqualTo(String value) {
            addCriterion("qishoustr =", value, "qishoustr");
            return (Criteria) this;
        }

        public Criteria andQishoustrNotEqualTo(String value) {
            addCriterion("qishoustr <>", value, "qishoustr");
            return (Criteria) this;
        }

        public Criteria andQishoustrGreaterThan(String value) {
            addCriterion("qishoustr >", value, "qishoustr");
            return (Criteria) this;
        }

        public Criteria andQishoustrGreaterThanOrEqualTo(String value) {
            addCriterion("qishoustr >=", value, "qishoustr");
            return (Criteria) this;
        }

        public Criteria andQishoustrLessThan(String value) {
            addCriterion("qishoustr <", value, "qishoustr");
            return (Criteria) this;
        }

        public Criteria andQishoustrLessThanOrEqualTo(String value) {
            addCriterion("qishoustr <=", value, "qishoustr");
            return (Criteria) this;
        }

        public Criteria andQishoustrLike(String value) {
            addCriterion("qishoustr like", value, "qishoustr");
            return (Criteria) this;
        }

        public Criteria andQishoustrNotLike(String value) {
            addCriterion("qishoustr not like", value, "qishoustr");
            return (Criteria) this;
        }

        public Criteria andQishoustrIn(List<String> values) {
            addCriterion("qishoustr in", values, "qishoustr");
            return (Criteria) this;
        }

        public Criteria andQishoustrNotIn(List<String> values) {
            addCriterion("qishoustr not in", values, "qishoustr");
            return (Criteria) this;
        }

        public Criteria andQishoustrBetween(String value1, String value2) {
            addCriterion("qishoustr between", value1, value2, "qishoustr");
            return (Criteria) this;
        }

        public Criteria andQishoustrNotBetween(String value1, String value2) {
            addCriterion("qishoustr not between", value1, value2, "qishoustr");
            return (Criteria) this;
        }

        public Criteria andRecontentIsNull() {
            addCriterion("recontent is null");
            return (Criteria) this;
        }

        public Criteria andRecontentIsNotNull() {
            addCriterion("recontent is not null");
            return (Criteria) this;
        }

        public Criteria andRecontentEqualTo(String value) {
            addCriterion("recontent =", value, "recontent");
            return (Criteria) this;
        }

        public Criteria andRecontentNotEqualTo(String value) {
            addCriterion("recontent <>", value, "recontent");
            return (Criteria) this;
        }

        public Criteria andRecontentGreaterThan(String value) {
            addCriterion("recontent >", value, "recontent");
            return (Criteria) this;
        }

        public Criteria andRecontentGreaterThanOrEqualTo(String value) {
            addCriterion("recontent >=", value, "recontent");
            return (Criteria) this;
        }

        public Criteria andRecontentLessThan(String value) {
            addCriterion("recontent <", value, "recontent");
            return (Criteria) this;
        }

        public Criteria andRecontentLessThanOrEqualTo(String value) {
            addCriterion("recontent <=", value, "recontent");
            return (Criteria) this;
        }

        public Criteria andRecontentLike(String value) {
            addCriterion("recontent like", value, "recontent");
            return (Criteria) this;
        }

        public Criteria andRecontentNotLike(String value) {
            addCriterion("recontent not like", value, "recontent");
            return (Criteria) this;
        }

        public Criteria andRecontentIn(List<String> values) {
            addCriterion("recontent in", values, "recontent");
            return (Criteria) this;
        }

        public Criteria andRecontentNotIn(List<String> values) {
            addCriterion("recontent not in", values, "recontent");
            return (Criteria) this;
        }

        public Criteria andRecontentBetween(String value1, String value2) {
            addCriterion("recontent between", value1, value2, "recontent");
            return (Criteria) this;
        }

        public Criteria andRecontentNotBetween(String value1, String value2) {
            addCriterion("recontent not between", value1, value2, "recontent");
            return (Criteria) this;
        }

        public Criteria andRecreateTimeIsNull() {
            addCriterion("recreateTime is null");
            return (Criteria) this;
        }

        public Criteria andRecreateTimeIsNotNull() {
            addCriterion("recreateTime is not null");
            return (Criteria) this;
        }

        public Criteria andRecreateTimeEqualTo(Date value) {
            addCriterion("recreateTime =", value, "recreateTime");
            return (Criteria) this;
        }

        public Criteria andRecreateTimeNotEqualTo(Date value) {
            addCriterion("recreateTime <>", value, "recreateTime");
            return (Criteria) this;
        }

        public Criteria andRecreateTimeGreaterThan(Date value) {
            addCriterion("recreateTime >", value, "recreateTime");
            return (Criteria) this;
        }

        public Criteria andRecreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("recreateTime >=", value, "recreateTime");
            return (Criteria) this;
        }

        public Criteria andRecreateTimeLessThan(Date value) {
            addCriterion("recreateTime <", value, "recreateTime");
            return (Criteria) this;
        }

        public Criteria andRecreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("recreateTime <=", value, "recreateTime");
            return (Criteria) this;
        }

        public Criteria andRecreateTimeIn(List<Date> values) {
            addCriterion("recreateTime in", values, "recreateTime");
            return (Criteria) this;
        }

        public Criteria andRecreateTimeNotIn(List<Date> values) {
            addCriterion("recreateTime not in", values, "recreateTime");
            return (Criteria) this;
        }

        public Criteria andRecreateTimeBetween(Date value1, Date value2) {
            addCriterion("recreateTime between", value1, value2, "recreateTime");
            return (Criteria) this;
        }

        public Criteria andRecreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("recreateTime not between", value1, value2, "recreateTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}