package com.runfast.waimai.dao.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class RunfastActivityTarget implements Serializable {
    private Integer id;

    /**
     * 代理商id
     */
    private Integer agentId;

    /**
     * 商家id
     */
    private Integer businessId;

    /**
     * 商品id
     */
    private Integer goodsId;

    /**
     * 规格id
     */
    private Integer standarId;

    /**
     * 活动id
     */
    private Integer activityId;

    /**
     * 打折价格
     */
    private Double disprice;

    /**
     * 折扣
     */
    private Double discount;

    /**
     * 活动库存
     */
    private Integer num;

    /**
     * 1暂停 0 正常
     */
    private Integer stops;

    private Boolean deleted;
    private static final long serialVersionUID = 1L;

}