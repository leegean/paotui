package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastWithdrawrecord;
import com.runfast.waimai.dao.model.RunfastWithdrawrecordExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastWithdrawrecordMapper extends IMapper<RunfastWithdrawrecord, Integer, RunfastWithdrawrecordExample> {
}