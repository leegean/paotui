package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastGoodsSellOutStatus;
import com.runfast.waimai.dao.model.RunfastGoodsSellOutStatusExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastGoodsSellOutStatusMapper extends IMapper<RunfastGoodsSellOutStatus, Integer, RunfastGoodsSellOutStatusExample> {
}