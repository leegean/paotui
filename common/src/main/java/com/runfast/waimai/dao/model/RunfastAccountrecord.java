package com.runfast.waimai.dao.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class RunfastAccountrecord implements Serializable {
    private Integer id;

    private BigDecimal balance;

    private String cardnumber;

    private Integer cid;

    private BigDecimal minmonety;

    private String mobile;

    private BigDecimal monetary;

    private String name;

    private Integer type;

    private Date createTime;

    private Integer genreType;

    private String typename;

    private BigDecimal beforemonety;

    private Integer showtype;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public RunfastAccountrecord withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public RunfastAccountrecord withBalance(BigDecimal balance) {
        this.setBalance(balance);
        return this;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getCardnumber() {
        return cardnumber;
    }

    public RunfastAccountrecord withCardnumber(String cardnumber) {
        this.setCardnumber(cardnumber);
        return this;
    }

    public void setCardnumber(String cardnumber) {
        this.cardnumber = cardnumber == null ? null : cardnumber.trim();
    }

    public Integer getCid() {
        return cid;
    }

    public RunfastAccountrecord withCid(Integer cid) {
        this.setCid(cid);
        return this;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public BigDecimal getMinmonety() {
        return minmonety;
    }

    public RunfastAccountrecord withMinmonety(BigDecimal minmonety) {
        this.setMinmonety(minmonety);
        return this;
    }

    public void setMinmonety(BigDecimal minmonety) {
        this.minmonety = minmonety;
    }

    public String getMobile() {
        return mobile;
    }

    public RunfastAccountrecord withMobile(String mobile) {
        this.setMobile(mobile);
        return this;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public BigDecimal getMonetary() {
        return monetary;
    }

    public RunfastAccountrecord withMonetary(BigDecimal monetary) {
        this.setMonetary(monetary);
        return this;
    }

    public void setMonetary(BigDecimal monetary) {
        this.monetary = monetary;
    }

    public String getName() {
        return name;
    }

    public RunfastAccountrecord withName(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getType() {
        return type;
    }

    public RunfastAccountrecord withType(Integer type) {
        this.setType(type);
        return this;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public RunfastAccountrecord withCreateTime(Date createTime) {
        this.setCreateTime(createTime);
        return this;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getGenreType() {
        return genreType;
    }

    public RunfastAccountrecord withGenreType(Integer genreType) {
        this.setGenreType(genreType);
        return this;
    }

    public void setGenreType(Integer genreType) {
        this.genreType = genreType;
    }

    public String getTypename() {
        return typename;
    }

    public RunfastAccountrecord withTypename(String typename) {
        this.setTypename(typename);
        return this;
    }

    public void setTypename(String typename) {
        this.typename = typename == null ? null : typename.trim();
    }

    public BigDecimal getBeforemonety() {
        return beforemonety;
    }

    public RunfastAccountrecord withBeforemonety(BigDecimal beforemonety) {
        this.setBeforemonety(beforemonety);
        return this;
    }

    public void setBeforemonety(BigDecimal beforemonety) {
        this.beforemonety = beforemonety;
    }

    public Integer getShowtype() {
        return showtype;
    }

    public RunfastAccountrecord withShowtype(Integer showtype) {
        this.setShowtype(showtype);
        return this;
    }

    public void setShowtype(Integer showtype) {
        this.showtype = showtype;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", balance=").append(balance);
        sb.append(", cardnumber=").append(cardnumber);
        sb.append(", cid=").append(cid);
        sb.append(", minmonety=").append(minmonety);
        sb.append(", mobile=").append(mobile);
        sb.append(", monetary=").append(monetary);
        sb.append(", name=").append(name);
        sb.append(", type=").append(type);
        sb.append(", createTime=").append(createTime);
        sb.append(", genreType=").append(genreType);
        sb.append(", typename=").append(typename);
        sb.append(", beforemonety=").append(beforemonety);
        sb.append(", showtype=").append(showtype);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}