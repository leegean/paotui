package com.runfast.waimai.dao.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RunfastSmsSendrecordExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public RunfastSmsSendrecordExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIpAddrIsNull() {
            addCriterion("ipAddr is null");
            return (Criteria) this;
        }

        public Criteria andIpAddrIsNotNull() {
            addCriterion("ipAddr is not null");
            return (Criteria) this;
        }

        public Criteria andIpAddrEqualTo(String value) {
            addCriterion("ipAddr =", value, "ipAddr");
            return (Criteria) this;
        }

        public Criteria andIpAddrNotEqualTo(String value) {
            addCriterion("ipAddr <>", value, "ipAddr");
            return (Criteria) this;
        }

        public Criteria andIpAddrGreaterThan(String value) {
            addCriterion("ipAddr >", value, "ipAddr");
            return (Criteria) this;
        }

        public Criteria andIpAddrGreaterThanOrEqualTo(String value) {
            addCriterion("ipAddr >=", value, "ipAddr");
            return (Criteria) this;
        }

        public Criteria andIpAddrLessThan(String value) {
            addCriterion("ipAddr <", value, "ipAddr");
            return (Criteria) this;
        }

        public Criteria andIpAddrLessThanOrEqualTo(String value) {
            addCriterion("ipAddr <=", value, "ipAddr");
            return (Criteria) this;
        }

        public Criteria andIpAddrLike(String value) {
            addCriterion("ipAddr like", value, "ipAddr");
            return (Criteria) this;
        }

        public Criteria andIpAddrNotLike(String value) {
            addCriterion("ipAddr not like", value, "ipAddr");
            return (Criteria) this;
        }

        public Criteria andIpAddrIn(List<String> values) {
            addCriterion("ipAddr in", values, "ipAddr");
            return (Criteria) this;
        }

        public Criteria andIpAddrNotIn(List<String> values) {
            addCriterion("ipAddr not in", values, "ipAddr");
            return (Criteria) this;
        }

        public Criteria andIpAddrBetween(String value1, String value2) {
            addCriterion("ipAddr between", value1, value2, "ipAddr");
            return (Criteria) this;
        }

        public Criteria andIpAddrNotBetween(String value1, String value2) {
            addCriterion("ipAddr not between", value1, value2, "ipAddr");
            return (Criteria) this;
        }

        public Criteria andMessionIsNull() {
            addCriterion("mession is null");
            return (Criteria) this;
        }

        public Criteria andMessionIsNotNull() {
            addCriterion("mession is not null");
            return (Criteria) this;
        }

        public Criteria andMessionEqualTo(String value) {
            addCriterion("mession =", value, "mession");
            return (Criteria) this;
        }

        public Criteria andMessionNotEqualTo(String value) {
            addCriterion("mession <>", value, "mession");
            return (Criteria) this;
        }

        public Criteria andMessionGreaterThan(String value) {
            addCriterion("mession >", value, "mession");
            return (Criteria) this;
        }

        public Criteria andMessionGreaterThanOrEqualTo(String value) {
            addCriterion("mession >=", value, "mession");
            return (Criteria) this;
        }

        public Criteria andMessionLessThan(String value) {
            addCriterion("mession <", value, "mession");
            return (Criteria) this;
        }

        public Criteria andMessionLessThanOrEqualTo(String value) {
            addCriterion("mession <=", value, "mession");
            return (Criteria) this;
        }

        public Criteria andMessionLike(String value) {
            addCriterion("mession like", value, "mession");
            return (Criteria) this;
        }

        public Criteria andMessionNotLike(String value) {
            addCriterion("mession not like", value, "mession");
            return (Criteria) this;
        }

        public Criteria andMessionIn(List<String> values) {
            addCriterion("mession in", values, "mession");
            return (Criteria) this;
        }

        public Criteria andMessionNotIn(List<String> values) {
            addCriterion("mession not in", values, "mession");
            return (Criteria) this;
        }

        public Criteria andMessionBetween(String value1, String value2) {
            addCriterion("mession between", value1, value2, "mession");
            return (Criteria) this;
        }

        public Criteria andMessionNotBetween(String value1, String value2) {
            addCriterion("mession not between", value1, value2, "mession");
            return (Criteria) this;
        }

        public Criteria andMsgIsNull() {
            addCriterion("msg is null");
            return (Criteria) this;
        }

        public Criteria andMsgIsNotNull() {
            addCriterion("msg is not null");
            return (Criteria) this;
        }

        public Criteria andMsgEqualTo(String value) {
            addCriterion("msg =", value, "msg");
            return (Criteria) this;
        }

        public Criteria andMsgNotEqualTo(String value) {
            addCriterion("msg <>", value, "msg");
            return (Criteria) this;
        }

        public Criteria andMsgGreaterThan(String value) {
            addCriterion("msg >", value, "msg");
            return (Criteria) this;
        }

        public Criteria andMsgGreaterThanOrEqualTo(String value) {
            addCriterion("msg >=", value, "msg");
            return (Criteria) this;
        }

        public Criteria andMsgLessThan(String value) {
            addCriterion("msg <", value, "msg");
            return (Criteria) this;
        }

        public Criteria andMsgLessThanOrEqualTo(String value) {
            addCriterion("msg <=", value, "msg");
            return (Criteria) this;
        }

        public Criteria andMsgLike(String value) {
            addCriterion("msg like", value, "msg");
            return (Criteria) this;
        }

        public Criteria andMsgNotLike(String value) {
            addCriterion("msg not like", value, "msg");
            return (Criteria) this;
        }

        public Criteria andMsgIn(List<String> values) {
            addCriterion("msg in", values, "msg");
            return (Criteria) this;
        }

        public Criteria andMsgNotIn(List<String> values) {
            addCriterion("msg not in", values, "msg");
            return (Criteria) this;
        }

        public Criteria andMsgBetween(String value1, String value2) {
            addCriterion("msg between", value1, value2, "msg");
            return (Criteria) this;
        }

        public Criteria andMsgNotBetween(String value1, String value2) {
            addCriterion("msg not between", value1, value2, "msg");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNull() {
            addCriterion("phone is null");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNotNull() {
            addCriterion("phone is not null");
            return (Criteria) this;
        }

        public Criteria andPhoneEqualTo(String value) {
            addCriterion("phone =", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotEqualTo(String value) {
            addCriterion("phone <>", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThan(String value) {
            addCriterion("phone >", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("phone >=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThan(String value) {
            addCriterion("phone <", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThanOrEqualTo(String value) {
            addCriterion("phone <=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLike(String value) {
            addCriterion("phone like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotLike(String value) {
            addCriterion("phone not like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneIn(List<String> values) {
            addCriterion("phone in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotIn(List<String> values) {
            addCriterion("phone not in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneBetween(String value1, String value2) {
            addCriterion("phone between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotBetween(String value1, String value2) {
            addCriterion("phone not between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andSendTimeIsNull() {
            addCriterion("sendTime is null");
            return (Criteria) this;
        }

        public Criteria andSendTimeIsNotNull() {
            addCriterion("sendTime is not null");
            return (Criteria) this;
        }

        public Criteria andSendTimeEqualTo(Date value) {
            addCriterion("sendTime =", value, "sendTime");
            return (Criteria) this;
        }

        public Criteria andSendTimeNotEqualTo(Date value) {
            addCriterion("sendTime <>", value, "sendTime");
            return (Criteria) this;
        }

        public Criteria andSendTimeGreaterThan(Date value) {
            addCriterion("sendTime >", value, "sendTime");
            return (Criteria) this;
        }

        public Criteria andSendTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("sendTime >=", value, "sendTime");
            return (Criteria) this;
        }

        public Criteria andSendTimeLessThan(Date value) {
            addCriterion("sendTime <", value, "sendTime");
            return (Criteria) this;
        }

        public Criteria andSendTimeLessThanOrEqualTo(Date value) {
            addCriterion("sendTime <=", value, "sendTime");
            return (Criteria) this;
        }

        public Criteria andSendTimeIn(List<Date> values) {
            addCriterion("sendTime in", values, "sendTime");
            return (Criteria) this;
        }

        public Criteria andSendTimeNotIn(List<Date> values) {
            addCriterion("sendTime not in", values, "sendTime");
            return (Criteria) this;
        }

        public Criteria andSendTimeBetween(Date value1, Date value2) {
            addCriterion("sendTime between", value1, value2, "sendTime");
            return (Criteria) this;
        }

        public Criteria andSendTimeNotBetween(Date value1, Date value2) {
            addCriterion("sendTime not between", value1, value2, "sendTime");
            return (Criteria) this;
        }

        public Criteria andSmstypeIsNull() {
            addCriterion("smstype is null");
            return (Criteria) this;
        }

        public Criteria andSmstypeIsNotNull() {
            addCriterion("smstype is not null");
            return (Criteria) this;
        }

        public Criteria andSmstypeEqualTo(Integer value) {
            addCriterion("smstype =", value, "smstype");
            return (Criteria) this;
        }

        public Criteria andSmstypeNotEqualTo(Integer value) {
            addCriterion("smstype <>", value, "smstype");
            return (Criteria) this;
        }

        public Criteria andSmstypeGreaterThan(Integer value) {
            addCriterion("smstype >", value, "smstype");
            return (Criteria) this;
        }

        public Criteria andSmstypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("smstype >=", value, "smstype");
            return (Criteria) this;
        }

        public Criteria andSmstypeLessThan(Integer value) {
            addCriterion("smstype <", value, "smstype");
            return (Criteria) this;
        }

        public Criteria andSmstypeLessThanOrEqualTo(Integer value) {
            addCriterion("smstype <=", value, "smstype");
            return (Criteria) this;
        }

        public Criteria andSmstypeIn(List<Integer> values) {
            addCriterion("smstype in", values, "smstype");
            return (Criteria) this;
        }

        public Criteria andSmstypeNotIn(List<Integer> values) {
            addCriterion("smstype not in", values, "smstype");
            return (Criteria) this;
        }

        public Criteria andSmstypeBetween(Integer value1, Integer value2) {
            addCriterion("smstype between", value1, value2, "smstype");
            return (Criteria) this;
        }

        public Criteria andSmstypeNotBetween(Integer value1, Integer value2) {
            addCriterion("smstype not between", value1, value2, "smstype");
            return (Criteria) this;
        }

        public Criteria andXcodeIsNull() {
            addCriterion("xcode is null");
            return (Criteria) this;
        }

        public Criteria andXcodeIsNotNull() {
            addCriterion("xcode is not null");
            return (Criteria) this;
        }

        public Criteria andXcodeEqualTo(String value) {
            addCriterion("xcode =", value, "xcode");
            return (Criteria) this;
        }

        public Criteria andXcodeNotEqualTo(String value) {
            addCriterion("xcode <>", value, "xcode");
            return (Criteria) this;
        }

        public Criteria andXcodeGreaterThan(String value) {
            addCriterion("xcode >", value, "xcode");
            return (Criteria) this;
        }

        public Criteria andXcodeGreaterThanOrEqualTo(String value) {
            addCriterion("xcode >=", value, "xcode");
            return (Criteria) this;
        }

        public Criteria andXcodeLessThan(String value) {
            addCriterion("xcode <", value, "xcode");
            return (Criteria) this;
        }

        public Criteria andXcodeLessThanOrEqualTo(String value) {
            addCriterion("xcode <=", value, "xcode");
            return (Criteria) this;
        }

        public Criteria andXcodeLike(String value) {
            addCriterion("xcode like", value, "xcode");
            return (Criteria) this;
        }

        public Criteria andXcodeNotLike(String value) {
            addCriterion("xcode not like", value, "xcode");
            return (Criteria) this;
        }

        public Criteria andXcodeIn(List<String> values) {
            addCriterion("xcode in", values, "xcode");
            return (Criteria) this;
        }

        public Criteria andXcodeNotIn(List<String> values) {
            addCriterion("xcode not in", values, "xcode");
            return (Criteria) this;
        }

        public Criteria andXcodeBetween(String value1, String value2) {
            addCriterion("xcode between", value1, value2, "xcode");
            return (Criteria) this;
        }

        public Criteria andXcodeNotBetween(String value1, String value2) {
            addCriterion("xcode not between", value1, value2, "xcode");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}