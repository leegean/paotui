package com.runfast.waimai.dao.model;

import java.io.Serializable;
import java.util.Date;

public class RunfastAgentaccounts implements Serializable {
    private Integer id;

    private String accounts;

    private Integer agentId;

    private String agentName;

    private String byRoles;

    private Date createTime;

    private String mobile;

    private String name;

    private String password;

    private String bdpushChannelId;

    private String bdpushUserId;

    private Integer bptype;

    private String cityId;

    private String cityName;

    private String countyId;

    private String countyName;

    private Integer aid;

    private String area;

    private String areaId;

    private Integer cid;

    private Integer pid;

    private String province;

    private String provinceId;

    private Integer tid;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public RunfastAgentaccounts withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccounts() {
        return accounts;
    }

    public RunfastAgentaccounts withAccounts(String accounts) {
        this.setAccounts(accounts);
        return this;
    }

    public void setAccounts(String accounts) {
        this.accounts = accounts == null ? null : accounts.trim();
    }

    public Integer getAgentId() {
        return agentId;
    }

    public RunfastAgentaccounts withAgentId(Integer agentId) {
        this.setAgentId(agentId);
        return this;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public String getAgentName() {
        return agentName;
    }

    public RunfastAgentaccounts withAgentName(String agentName) {
        this.setAgentName(agentName);
        return this;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName == null ? null : agentName.trim();
    }

    public String getByRoles() {
        return byRoles;
    }

    public RunfastAgentaccounts withByRoles(String byRoles) {
        this.setByRoles(byRoles);
        return this;
    }

    public void setByRoles(String byRoles) {
        this.byRoles = byRoles == null ? null : byRoles.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public RunfastAgentaccounts withCreateTime(Date createTime) {
        this.setCreateTime(createTime);
        return this;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getMobile() {
        return mobile;
    }

    public RunfastAgentaccounts withMobile(String mobile) {
        this.setMobile(mobile);
        return this;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public String getName() {
        return name;
    }

    public RunfastAgentaccounts withName(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getPassword() {
        return password;
    }

    public RunfastAgentaccounts withPassword(String password) {
        this.setPassword(password);
        return this;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getBdpushChannelId() {
        return bdpushChannelId;
    }

    public RunfastAgentaccounts withBdpushChannelId(String bdpushChannelId) {
        this.setBdpushChannelId(bdpushChannelId);
        return this;
    }

    public void setBdpushChannelId(String bdpushChannelId) {
        this.bdpushChannelId = bdpushChannelId == null ? null : bdpushChannelId.trim();
    }

    public String getBdpushUserId() {
        return bdpushUserId;
    }

    public RunfastAgentaccounts withBdpushUserId(String bdpushUserId) {
        this.setBdpushUserId(bdpushUserId);
        return this;
    }

    public void setBdpushUserId(String bdpushUserId) {
        this.bdpushUserId = bdpushUserId == null ? null : bdpushUserId.trim();
    }

    public Integer getBptype() {
        return bptype;
    }

    public RunfastAgentaccounts withBptype(Integer bptype) {
        this.setBptype(bptype);
        return this;
    }

    public void setBptype(Integer bptype) {
        this.bptype = bptype;
    }

    public String getCityId() {
        return cityId;
    }

    public RunfastAgentaccounts withCityId(String cityId) {
        this.setCityId(cityId);
        return this;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId == null ? null : cityId.trim();
    }

    public String getCityName() {
        return cityName;
    }

    public RunfastAgentaccounts withCityName(String cityName) {
        this.setCityName(cityName);
        return this;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName == null ? null : cityName.trim();
    }

    public String getCountyId() {
        return countyId;
    }

    public RunfastAgentaccounts withCountyId(String countyId) {
        this.setCountyId(countyId);
        return this;
    }

    public void setCountyId(String countyId) {
        this.countyId = countyId == null ? null : countyId.trim();
    }

    public String getCountyName() {
        return countyName;
    }

    public RunfastAgentaccounts withCountyName(String countyName) {
        this.setCountyName(countyName);
        return this;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName == null ? null : countyName.trim();
    }

    public Integer getAid() {
        return aid;
    }

    public RunfastAgentaccounts withAid(Integer aid) {
        this.setAid(aid);
        return this;
    }

    public void setAid(Integer aid) {
        this.aid = aid;
    }

    public String getArea() {
        return area;
    }

    public RunfastAgentaccounts withArea(String area) {
        this.setArea(area);
        return this;
    }

    public void setArea(String area) {
        this.area = area == null ? null : area.trim();
    }

    public String getAreaId() {
        return areaId;
    }

    public RunfastAgentaccounts withAreaId(String areaId) {
        this.setAreaId(areaId);
        return this;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId == null ? null : areaId.trim();
    }

    public Integer getCid() {
        return cid;
    }

    public RunfastAgentaccounts withCid(Integer cid) {
        this.setCid(cid);
        return this;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public Integer getPid() {
        return pid;
    }

    public RunfastAgentaccounts withPid(Integer pid) {
        this.setPid(pid);
        return this;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getProvince() {
        return province;
    }

    public RunfastAgentaccounts withProvince(String province) {
        this.setProvince(province);
        return this;
    }

    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    public String getProvinceId() {
        return provinceId;
    }

    public RunfastAgentaccounts withProvinceId(String provinceId) {
        this.setProvinceId(provinceId);
        return this;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId == null ? null : provinceId.trim();
    }

    public Integer getTid() {
        return tid;
    }

    public RunfastAgentaccounts withTid(Integer tid) {
        this.setTid(tid);
        return this;
    }

    public void setTid(Integer tid) {
        this.tid = tid;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", accounts=").append(accounts);
        sb.append(", agentId=").append(agentId);
        sb.append(", agentName=").append(agentName);
        sb.append(", byRoles=").append(byRoles);
        sb.append(", createTime=").append(createTime);
        sb.append(", mobile=").append(mobile);
        sb.append(", name=").append(name);
        sb.append(", password=").append(password);
        sb.append(", bdpushChannelId=").append(bdpushChannelId);
        sb.append(", bdpushUserId=").append(bdpushUserId);
        sb.append(", bptype=").append(bptype);
        sb.append(", cityId=").append(cityId);
        sb.append(", cityName=").append(cityName);
        sb.append(", countyId=").append(countyId);
        sb.append(", countyName=").append(countyName);
        sb.append(", aid=").append(aid);
        sb.append(", area=").append(area);
        sb.append(", areaId=").append(areaId);
        sb.append(", cid=").append(cid);
        sb.append(", pid=").append(pid);
        sb.append(", province=").append(province);
        sb.append(", provinceId=").append(provinceId);
        sb.append(", tid=").append(tid);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}