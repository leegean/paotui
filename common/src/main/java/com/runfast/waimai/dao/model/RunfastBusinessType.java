package com.runfast.waimai.dao.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
public class RunfastBusinessType implements Serializable {
    private Integer id;

    private String content;

    private Date createTime;

    private String name;

    /**
     * 上级分类
     */
    private Integer parent_id;

    private Boolean deleted;

    private static final long serialVersionUID = 1L;

    private List<RunfastBusinessType> children;

}