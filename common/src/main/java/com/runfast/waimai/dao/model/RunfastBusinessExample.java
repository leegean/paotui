package com.runfast.waimai.dao.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class RunfastBusinessExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public RunfastBusinessExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCTime(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Time(value.getTime()), property);
        }

        protected void addCriterionForJDBCTime(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Time> timeList = new ArrayList<java.sql.Time>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                timeList.add(new java.sql.Time(iter.next().getTime()));
            }
            addCriterion(condition, timeList, property);
        }

        protected void addCriterionForJDBCTime(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Time(value1.getTime()), new java.sql.Time(value2.getTime()), property);
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andAddressIsNull() {
            addCriterion("address is null");
            return (Criteria) this;
        }

        public Criteria andAddressIsNotNull() {
            addCriterion("address is not null");
            return (Criteria) this;
        }

        public Criteria andAddressEqualTo(String value) {
            addCriterion("address =", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotEqualTo(String value) {
            addCriterion("address <>", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressGreaterThan(String value) {
            addCriterion("address >", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressGreaterThanOrEqualTo(String value) {
            addCriterion("address >=", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLessThan(String value) {
            addCriterion("address <", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLessThanOrEqualTo(String value) {
            addCriterion("address <=", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLike(String value) {
            addCriterion("address like", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotLike(String value) {
            addCriterion("address not like", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressIn(List<String> values) {
            addCriterion("address in", values, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotIn(List<String> values) {
            addCriterion("address not in", values, "address");
            return (Criteria) this;
        }

        public Criteria andAddressBetween(String value1, String value2) {
            addCriterion("address between", value1, value2, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotBetween(String value1, String value2) {
            addCriterion("address not between", value1, value2, "address");
            return (Criteria) this;
        }

        public Criteria andCityIdIsNull() {
            addCriterion("cityId is null");
            return (Criteria) this;
        }

        public Criteria andCityIdIsNotNull() {
            addCriterion("cityId is not null");
            return (Criteria) this;
        }

        public Criteria andCityIdEqualTo(String value) {
            addCriterion("cityId =", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdNotEqualTo(String value) {
            addCriterion("cityId <>", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdGreaterThan(String value) {
            addCriterion("cityId >", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdGreaterThanOrEqualTo(String value) {
            addCriterion("cityId >=", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdLessThan(String value) {
            addCriterion("cityId <", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdLessThanOrEqualTo(String value) {
            addCriterion("cityId <=", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdLike(String value) {
            addCriterion("cityId like", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdNotLike(String value) {
            addCriterion("cityId not like", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdIn(List<String> values) {
            addCriterion("cityId in", values, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdNotIn(List<String> values) {
            addCriterion("cityId not in", values, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdBetween(String value1, String value2) {
            addCriterion("cityId between", value1, value2, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdNotBetween(String value1, String value2) {
            addCriterion("cityId not between", value1, value2, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityNameIsNull() {
            addCriterion("cityName is null");
            return (Criteria) this;
        }

        public Criteria andCityNameIsNotNull() {
            addCriterion("cityName is not null");
            return (Criteria) this;
        }

        public Criteria andCityNameEqualTo(String value) {
            addCriterion("cityName =", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameNotEqualTo(String value) {
            addCriterion("cityName <>", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameGreaterThan(String value) {
            addCriterion("cityName >", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameGreaterThanOrEqualTo(String value) {
            addCriterion("cityName >=", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameLessThan(String value) {
            addCriterion("cityName <", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameLessThanOrEqualTo(String value) {
            addCriterion("cityName <=", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameLike(String value) {
            addCriterion("cityName like", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameNotLike(String value) {
            addCriterion("cityName not like", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameIn(List<String> values) {
            addCriterion("cityName in", values, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameNotIn(List<String> values) {
            addCriterion("cityName not in", values, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameBetween(String value1, String value2) {
            addCriterion("cityName between", value1, value2, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameNotBetween(String value1, String value2) {
            addCriterion("cityName not between", value1, value2, "cityName");
            return (Criteria) this;
        }

        public Criteria andContentIsNull() {
            addCriterion("content is null");
            return (Criteria) this;
        }

        public Criteria andContentIsNotNull() {
            addCriterion("content is not null");
            return (Criteria) this;
        }

        public Criteria andContentEqualTo(String value) {
            addCriterion("content =", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotEqualTo(String value) {
            addCriterion("content <>", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentGreaterThan(String value) {
            addCriterion("content >", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentGreaterThanOrEqualTo(String value) {
            addCriterion("content >=", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLessThan(String value) {
            addCriterion("content <", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLessThanOrEqualTo(String value) {
            addCriterion("content <=", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLike(String value) {
            addCriterion("content like", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotLike(String value) {
            addCriterion("content not like", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentIn(List<String> values) {
            addCriterion("content in", values, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotIn(List<String> values) {
            addCriterion("content not in", values, "content");
            return (Criteria) this;
        }

        public Criteria andContentBetween(String value1, String value2) {
            addCriterion("content between", value1, value2, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotBetween(String value1, String value2) {
            addCriterion("content not between", value1, value2, "content");
            return (Criteria) this;
        }

        public Criteria andCountyIdIsNull() {
            addCriterion("countyId is null");
            return (Criteria) this;
        }

        public Criteria andCountyIdIsNotNull() {
            addCriterion("countyId is not null");
            return (Criteria) this;
        }

        public Criteria andCountyIdEqualTo(String value) {
            addCriterion("countyId =", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdNotEqualTo(String value) {
            addCriterion("countyId <>", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdGreaterThan(String value) {
            addCriterion("countyId >", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdGreaterThanOrEqualTo(String value) {
            addCriterion("countyId >=", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdLessThan(String value) {
            addCriterion("countyId <", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdLessThanOrEqualTo(String value) {
            addCriterion("countyId <=", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdLike(String value) {
            addCriterion("countyId like", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdNotLike(String value) {
            addCriterion("countyId not like", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdIn(List<String> values) {
            addCriterion("countyId in", values, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdNotIn(List<String> values) {
            addCriterion("countyId not in", values, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdBetween(String value1, String value2) {
            addCriterion("countyId between", value1, value2, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdNotBetween(String value1, String value2) {
            addCriterion("countyId not between", value1, value2, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyNameIsNull() {
            addCriterion("countyName is null");
            return (Criteria) this;
        }

        public Criteria andCountyNameIsNotNull() {
            addCriterion("countyName is not null");
            return (Criteria) this;
        }

        public Criteria andCountyNameEqualTo(String value) {
            addCriterion("countyName =", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameNotEqualTo(String value) {
            addCriterion("countyName <>", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameGreaterThan(String value) {
            addCriterion("countyName >", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameGreaterThanOrEqualTo(String value) {
            addCriterion("countyName >=", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameLessThan(String value) {
            addCriterion("countyName <", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameLessThanOrEqualTo(String value) {
            addCriterion("countyName <=", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameLike(String value) {
            addCriterion("countyName like", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameNotLike(String value) {
            addCriterion("countyName not like", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameIn(List<String> values) {
            addCriterion("countyName in", values, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameNotIn(List<String> values) {
            addCriterion("countyName not in", values, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameBetween(String value1, String value2) {
            addCriterion("countyName between", value1, value2, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameNotBetween(String value1, String value2) {
            addCriterion("countyName not between", value1, value2, "countyName");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("createTime is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("createTime is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("createTime =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("createTime <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("createTime >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("createTime >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("createTime <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("createTime <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("createTime in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("createTime not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("createTime between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("createTime not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andImgPathIsNull() {
            addCriterion("imgPath is null");
            return (Criteria) this;
        }

        public Criteria andImgPathIsNotNull() {
            addCriterion("imgPath is not null");
            return (Criteria) this;
        }

        public Criteria andImgPathEqualTo(String value) {
            addCriterion("imgPath =", value, "imgPath");
            return (Criteria) this;
        }

        public Criteria andImgPathNotEqualTo(String value) {
            addCriterion("imgPath <>", value, "imgPath");
            return (Criteria) this;
        }

        public Criteria andImgPathGreaterThan(String value) {
            addCriterion("imgPath >", value, "imgPath");
            return (Criteria) this;
        }

        public Criteria andImgPathGreaterThanOrEqualTo(String value) {
            addCriterion("imgPath >=", value, "imgPath");
            return (Criteria) this;
        }

        public Criteria andImgPathLessThan(String value) {
            addCriterion("imgPath <", value, "imgPath");
            return (Criteria) this;
        }

        public Criteria andImgPathLessThanOrEqualTo(String value) {
            addCriterion("imgPath <=", value, "imgPath");
            return (Criteria) this;
        }

        public Criteria andImgPathLike(String value) {
            addCriterion("imgPath like", value, "imgPath");
            return (Criteria) this;
        }

        public Criteria andImgPathNotLike(String value) {
            addCriterion("imgPath not like", value, "imgPath");
            return (Criteria) this;
        }

        public Criteria andImgPathIn(List<String> values) {
            addCriterion("imgPath in", values, "imgPath");
            return (Criteria) this;
        }

        public Criteria andImgPathNotIn(List<String> values) {
            addCriterion("imgPath not in", values, "imgPath");
            return (Criteria) this;
        }

        public Criteria andImgPathBetween(String value1, String value2) {
            addCriterion("imgPath between", value1, value2, "imgPath");
            return (Criteria) this;
        }

        public Criteria andImgPathNotBetween(String value1, String value2) {
            addCriterion("imgPath not between", value1, value2, "imgPath");
            return (Criteria) this;
        }

        public Criteria andLatitudeIsNull() {
            addCriterion("latitude is null");
            return (Criteria) this;
        }

        public Criteria andLatitudeIsNotNull() {
            addCriterion("latitude is not null");
            return (Criteria) this;
        }

        public Criteria andLatitudeEqualTo(String value) {
            addCriterion("latitude =", value, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeNotEqualTo(String value) {
            addCriterion("latitude <>", value, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeGreaterThan(String value) {
            addCriterion("latitude >", value, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeGreaterThanOrEqualTo(String value) {
            addCriterion("latitude >=", value, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeLessThan(String value) {
            addCriterion("latitude <", value, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeLessThanOrEqualTo(String value) {
            addCriterion("latitude <=", value, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeLike(String value) {
            addCriterion("latitude like", value, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeNotLike(String value) {
            addCriterion("latitude not like", value, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeIn(List<String> values) {
            addCriterion("latitude in", values, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeNotIn(List<String> values) {
            addCriterion("latitude not in", values, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeBetween(String value1, String value2) {
            addCriterion("latitude between", value1, value2, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeNotBetween(String value1, String value2) {
            addCriterion("latitude not between", value1, value2, "latitude");
            return (Criteria) this;
        }

        public Criteria andLevelIdIsNull() {
            addCriterion("levelId is null");
            return (Criteria) this;
        }

        public Criteria andLevelIdIsNotNull() {
            addCriterion("levelId is not null");
            return (Criteria) this;
        }

        public Criteria andLevelIdEqualTo(Integer value) {
            addCriterion("levelId =", value, "levelId");
            return (Criteria) this;
        }

        public Criteria andLevelIdNotEqualTo(Integer value) {
            addCriterion("levelId <>", value, "levelId");
            return (Criteria) this;
        }

        public Criteria andLevelIdGreaterThan(Integer value) {
            addCriterion("levelId >", value, "levelId");
            return (Criteria) this;
        }

        public Criteria andLevelIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("levelId >=", value, "levelId");
            return (Criteria) this;
        }

        public Criteria andLevelIdLessThan(Integer value) {
            addCriterion("levelId <", value, "levelId");
            return (Criteria) this;
        }

        public Criteria andLevelIdLessThanOrEqualTo(Integer value) {
            addCriterion("levelId <=", value, "levelId");
            return (Criteria) this;
        }

        public Criteria andLevelIdIn(List<Integer> values) {
            addCriterion("levelId in", values, "levelId");
            return (Criteria) this;
        }

        public Criteria andLevelIdNotIn(List<Integer> values) {
            addCriterion("levelId not in", values, "levelId");
            return (Criteria) this;
        }

        public Criteria andLevelIdBetween(Integer value1, Integer value2) {
            addCriterion("levelId between", value1, value2, "levelId");
            return (Criteria) this;
        }

        public Criteria andLevelIdNotBetween(Integer value1, Integer value2) {
            addCriterion("levelId not between", value1, value2, "levelId");
            return (Criteria) this;
        }

        public Criteria andLevelNameIsNull() {
            addCriterion("levelName is null");
            return (Criteria) this;
        }

        public Criteria andLevelNameIsNotNull() {
            addCriterion("levelName is not null");
            return (Criteria) this;
        }

        public Criteria andLevelNameEqualTo(String value) {
            addCriterion("levelName =", value, "levelName");
            return (Criteria) this;
        }

        public Criteria andLevelNameNotEqualTo(String value) {
            addCriterion("levelName <>", value, "levelName");
            return (Criteria) this;
        }

        public Criteria andLevelNameGreaterThan(String value) {
            addCriterion("levelName >", value, "levelName");
            return (Criteria) this;
        }

        public Criteria andLevelNameGreaterThanOrEqualTo(String value) {
            addCriterion("levelName >=", value, "levelName");
            return (Criteria) this;
        }

        public Criteria andLevelNameLessThan(String value) {
            addCriterion("levelName <", value, "levelName");
            return (Criteria) this;
        }

        public Criteria andLevelNameLessThanOrEqualTo(String value) {
            addCriterion("levelName <=", value, "levelName");
            return (Criteria) this;
        }

        public Criteria andLevelNameLike(String value) {
            addCriterion("levelName like", value, "levelName");
            return (Criteria) this;
        }

        public Criteria andLevelNameNotLike(String value) {
            addCriterion("levelName not like", value, "levelName");
            return (Criteria) this;
        }

        public Criteria andLevelNameIn(List<String> values) {
            addCriterion("levelName in", values, "levelName");
            return (Criteria) this;
        }

        public Criteria andLevelNameNotIn(List<String> values) {
            addCriterion("levelName not in", values, "levelName");
            return (Criteria) this;
        }

        public Criteria andLevelNameBetween(String value1, String value2) {
            addCriterion("levelName between", value1, value2, "levelName");
            return (Criteria) this;
        }

        public Criteria andLevelNameNotBetween(String value1, String value2) {
            addCriterion("levelName not between", value1, value2, "levelName");
            return (Criteria) this;
        }

        public Criteria andLongitudeIsNull() {
            addCriterion("longitude is null");
            return (Criteria) this;
        }

        public Criteria andLongitudeIsNotNull() {
            addCriterion("longitude is not null");
            return (Criteria) this;
        }

        public Criteria andLongitudeEqualTo(String value) {
            addCriterion("longitude =", value, "longitude");
            return (Criteria) this;
        }

        public Criteria andLongitudeNotEqualTo(String value) {
            addCriterion("longitude <>", value, "longitude");
            return (Criteria) this;
        }

        public Criteria andLongitudeGreaterThan(String value) {
            addCriterion("longitude >", value, "longitude");
            return (Criteria) this;
        }

        public Criteria andLongitudeGreaterThanOrEqualTo(String value) {
            addCriterion("longitude >=", value, "longitude");
            return (Criteria) this;
        }

        public Criteria andLongitudeLessThan(String value) {
            addCriterion("longitude <", value, "longitude");
            return (Criteria) this;
        }

        public Criteria andLongitudeLessThanOrEqualTo(String value) {
            addCriterion("longitude <=", value, "longitude");
            return (Criteria) this;
        }

        public Criteria andLongitudeLike(String value) {
            addCriterion("longitude like", value, "longitude");
            return (Criteria) this;
        }

        public Criteria andLongitudeNotLike(String value) {
            addCriterion("longitude not like", value, "longitude");
            return (Criteria) this;
        }

        public Criteria andLongitudeIn(List<String> values) {
            addCriterion("longitude in", values, "longitude");
            return (Criteria) this;
        }

        public Criteria andLongitudeNotIn(List<String> values) {
            addCriterion("longitude not in", values, "longitude");
            return (Criteria) this;
        }

        public Criteria andLongitudeBetween(String value1, String value2) {
            addCriterion("longitude between", value1, value2, "longitude");
            return (Criteria) this;
        }

        public Criteria andLongitudeNotBetween(String value1, String value2) {
            addCriterion("longitude not between", value1, value2, "longitude");
            return (Criteria) this;
        }

        public Criteria andMobileIsNull() {
            addCriterion("mobile is null");
            return (Criteria) this;
        }

        public Criteria andMobileIsNotNull() {
            addCriterion("mobile is not null");
            return (Criteria) this;
        }

        public Criteria andMobileEqualTo(String value) {
            addCriterion("mobile =", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotEqualTo(String value) {
            addCriterion("mobile <>", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileGreaterThan(String value) {
            addCriterion("mobile >", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileGreaterThanOrEqualTo(String value) {
            addCriterion("mobile >=", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLessThan(String value) {
            addCriterion("mobile <", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLessThanOrEqualTo(String value) {
            addCriterion("mobile <=", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLike(String value) {
            addCriterion("mobile like", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotLike(String value) {
            addCriterion("mobile not like", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileIn(List<String> values) {
            addCriterion("mobile in", values, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotIn(List<String> values) {
            addCriterion("mobile not in", values, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileBetween(String value1, String value2) {
            addCriterion("mobile between", value1, value2, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotBetween(String value1, String value2) {
            addCriterion("mobile not between", value1, value2, "mobile");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andSaleDayTimeIsNull() {
            addCriterion("saleDayTime is null");
            return (Criteria) this;
        }

        public Criteria andSaleDayTimeIsNotNull() {
            addCriterion("saleDayTime is not null");
            return (Criteria) this;
        }

        public Criteria andSaleDayTimeEqualTo(String value) {
            addCriterion("saleDayTime =", value, "saleDayTime");
            return (Criteria) this;
        }

        public Criteria andSaleDayTimeNotEqualTo(String value) {
            addCriterion("saleDayTime <>", value, "saleDayTime");
            return (Criteria) this;
        }

        public Criteria andSaleDayTimeGreaterThan(String value) {
            addCriterion("saleDayTime >", value, "saleDayTime");
            return (Criteria) this;
        }

        public Criteria andSaleDayTimeGreaterThanOrEqualTo(String value) {
            addCriterion("saleDayTime >=", value, "saleDayTime");
            return (Criteria) this;
        }

        public Criteria andSaleDayTimeLessThan(String value) {
            addCriterion("saleDayTime <", value, "saleDayTime");
            return (Criteria) this;
        }

        public Criteria andSaleDayTimeLessThanOrEqualTo(String value) {
            addCriterion("saleDayTime <=", value, "saleDayTime");
            return (Criteria) this;
        }

        public Criteria andSaleDayTimeLike(String value) {
            addCriterion("saleDayTime like", value, "saleDayTime");
            return (Criteria) this;
        }

        public Criteria andSaleDayTimeNotLike(String value) {
            addCriterion("saleDayTime not like", value, "saleDayTime");
            return (Criteria) this;
        }

        public Criteria andSaleDayTimeIn(List<String> values) {
            addCriterion("saleDayTime in", values, "saleDayTime");
            return (Criteria) this;
        }

        public Criteria andSaleDayTimeNotIn(List<String> values) {
            addCriterion("saleDayTime not in", values, "saleDayTime");
            return (Criteria) this;
        }

        public Criteria andSaleDayTimeBetween(String value1, String value2) {
            addCriterion("saleDayTime between", value1, value2, "saleDayTime");
            return (Criteria) this;
        }

        public Criteria andSaleDayTimeNotBetween(String value1, String value2) {
            addCriterion("saleDayTime not between", value1, value2, "saleDayTime");
            return (Criteria) this;
        }

        public Criteria andSaleRangeIsNull() {
            addCriterion("saleRange is null");
            return (Criteria) this;
        }

        public Criteria andSaleRangeIsNotNull() {
            addCriterion("saleRange is not null");
            return (Criteria) this;
        }

        public Criteria andSaleRangeEqualTo(String value) {
            addCriterion("saleRange =", value, "saleRange");
            return (Criteria) this;
        }

        public Criteria andSaleRangeNotEqualTo(String value) {
            addCriterion("saleRange <>", value, "saleRange");
            return (Criteria) this;
        }

        public Criteria andSaleRangeGreaterThan(String value) {
            addCriterion("saleRange >", value, "saleRange");
            return (Criteria) this;
        }

        public Criteria andSaleRangeGreaterThanOrEqualTo(String value) {
            addCriterion("saleRange >=", value, "saleRange");
            return (Criteria) this;
        }

        public Criteria andSaleRangeLessThan(String value) {
            addCriterion("saleRange <", value, "saleRange");
            return (Criteria) this;
        }

        public Criteria andSaleRangeLessThanOrEqualTo(String value) {
            addCriterion("saleRange <=", value, "saleRange");
            return (Criteria) this;
        }

        public Criteria andSaleRangeLike(String value) {
            addCriterion("saleRange like", value, "saleRange");
            return (Criteria) this;
        }

        public Criteria andSaleRangeNotLike(String value) {
            addCriterion("saleRange not like", value, "saleRange");
            return (Criteria) this;
        }

        public Criteria andSaleRangeIn(List<String> values) {
            addCriterion("saleRange in", values, "saleRange");
            return (Criteria) this;
        }

        public Criteria andSaleRangeNotIn(List<String> values) {
            addCriterion("saleRange not in", values, "saleRange");
            return (Criteria) this;
        }

        public Criteria andSaleRangeBetween(String value1, String value2) {
            addCriterion("saleRange between", value1, value2, "saleRange");
            return (Criteria) this;
        }

        public Criteria andSaleRangeNotBetween(String value1, String value2) {
            addCriterion("saleRange not between", value1, value2, "saleRange");
            return (Criteria) this;
        }

        public Criteria andSaleTimeIsNull() {
            addCriterion("saleTime is null");
            return (Criteria) this;
        }

        public Criteria andSaleTimeIsNotNull() {
            addCriterion("saleTime is not null");
            return (Criteria) this;
        }

        public Criteria andSaleTimeEqualTo(String value) {
            addCriterion("saleTime =", value, "saleTime");
            return (Criteria) this;
        }

        public Criteria andSaleTimeNotEqualTo(String value) {
            addCriterion("saleTime <>", value, "saleTime");
            return (Criteria) this;
        }

        public Criteria andSaleTimeGreaterThan(String value) {
            addCriterion("saleTime >", value, "saleTime");
            return (Criteria) this;
        }

        public Criteria andSaleTimeGreaterThanOrEqualTo(String value) {
            addCriterion("saleTime >=", value, "saleTime");
            return (Criteria) this;
        }

        public Criteria andSaleTimeLessThan(String value) {
            addCriterion("saleTime <", value, "saleTime");
            return (Criteria) this;
        }

        public Criteria andSaleTimeLessThanOrEqualTo(String value) {
            addCriterion("saleTime <=", value, "saleTime");
            return (Criteria) this;
        }

        public Criteria andSaleTimeLike(String value) {
            addCriterion("saleTime like", value, "saleTime");
            return (Criteria) this;
        }

        public Criteria andSaleTimeNotLike(String value) {
            addCriterion("saleTime not like", value, "saleTime");
            return (Criteria) this;
        }

        public Criteria andSaleTimeIn(List<String> values) {
            addCriterion("saleTime in", values, "saleTime");
            return (Criteria) this;
        }

        public Criteria andSaleTimeNotIn(List<String> values) {
            addCriterion("saleTime not in", values, "saleTime");
            return (Criteria) this;
        }

        public Criteria andSaleTimeBetween(String value1, String value2) {
            addCriterion("saleTime between", value1, value2, "saleTime");
            return (Criteria) this;
        }

        public Criteria andSaleTimeNotBetween(String value1, String value2) {
            addCriterion("saleTime not between", value1, value2, "saleTime");
            return (Criteria) this;
        }

        public Criteria andTownIdIsNull() {
            addCriterion("townId is null");
            return (Criteria) this;
        }

        public Criteria andTownIdIsNotNull() {
            addCriterion("townId is not null");
            return (Criteria) this;
        }

        public Criteria andTownIdEqualTo(String value) {
            addCriterion("townId =", value, "townId");
            return (Criteria) this;
        }

        public Criteria andTownIdNotEqualTo(String value) {
            addCriterion("townId <>", value, "townId");
            return (Criteria) this;
        }

        public Criteria andTownIdGreaterThan(String value) {
            addCriterion("townId >", value, "townId");
            return (Criteria) this;
        }

        public Criteria andTownIdGreaterThanOrEqualTo(String value) {
            addCriterion("townId >=", value, "townId");
            return (Criteria) this;
        }

        public Criteria andTownIdLessThan(String value) {
            addCriterion("townId <", value, "townId");
            return (Criteria) this;
        }

        public Criteria andTownIdLessThanOrEqualTo(String value) {
            addCriterion("townId <=", value, "townId");
            return (Criteria) this;
        }

        public Criteria andTownIdLike(String value) {
            addCriterion("townId like", value, "townId");
            return (Criteria) this;
        }

        public Criteria andTownIdNotLike(String value) {
            addCriterion("townId not like", value, "townId");
            return (Criteria) this;
        }

        public Criteria andTownIdIn(List<String> values) {
            addCriterion("townId in", values, "townId");
            return (Criteria) this;
        }

        public Criteria andTownIdNotIn(List<String> values) {
            addCriterion("townId not in", values, "townId");
            return (Criteria) this;
        }

        public Criteria andTownIdBetween(String value1, String value2) {
            addCriterion("townId between", value1, value2, "townId");
            return (Criteria) this;
        }

        public Criteria andTownIdNotBetween(String value1, String value2) {
            addCriterion("townId not between", value1, value2, "townId");
            return (Criteria) this;
        }

        public Criteria andTownNameIsNull() {
            addCriterion("townName is null");
            return (Criteria) this;
        }

        public Criteria andTownNameIsNotNull() {
            addCriterion("townName is not null");
            return (Criteria) this;
        }

        public Criteria andTownNameEqualTo(String value) {
            addCriterion("townName =", value, "townName");
            return (Criteria) this;
        }

        public Criteria andTownNameNotEqualTo(String value) {
            addCriterion("townName <>", value, "townName");
            return (Criteria) this;
        }

        public Criteria andTownNameGreaterThan(String value) {
            addCriterion("townName >", value, "townName");
            return (Criteria) this;
        }

        public Criteria andTownNameGreaterThanOrEqualTo(String value) {
            addCriterion("townName >=", value, "townName");
            return (Criteria) this;
        }

        public Criteria andTownNameLessThan(String value) {
            addCriterion("townName <", value, "townName");
            return (Criteria) this;
        }

        public Criteria andTownNameLessThanOrEqualTo(String value) {
            addCriterion("townName <=", value, "townName");
            return (Criteria) this;
        }

        public Criteria andTownNameLike(String value) {
            addCriterion("townName like", value, "townName");
            return (Criteria) this;
        }

        public Criteria andTownNameNotLike(String value) {
            addCriterion("townName not like", value, "townName");
            return (Criteria) this;
        }

        public Criteria andTownNameIn(List<String> values) {
            addCriterion("townName in", values, "townName");
            return (Criteria) this;
        }

        public Criteria andTownNameNotIn(List<String> values) {
            addCriterion("townName not in", values, "townName");
            return (Criteria) this;
        }

        public Criteria andTownNameBetween(String value1, String value2) {
            addCriterion("townName between", value1, value2, "townName");
            return (Criteria) this;
        }

        public Criteria andTownNameNotBetween(String value1, String value2) {
            addCriterion("townName not between", value1, value2, "townName");
            return (Criteria) this;
        }

        public Criteria andTypeIdIsNull() {
            addCriterion("typeId is null");
            return (Criteria) this;
        }

        public Criteria andTypeIdIsNotNull() {
            addCriterion("typeId is not null");
            return (Criteria) this;
        }

        public Criteria andTypeIdEqualTo(Integer value) {
            addCriterion("typeId =", value, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdNotEqualTo(Integer value) {
            addCriterion("typeId <>", value, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdGreaterThan(Integer value) {
            addCriterion("typeId >", value, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("typeId >=", value, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdLessThan(Integer value) {
            addCriterion("typeId <", value, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdLessThanOrEqualTo(Integer value) {
            addCriterion("typeId <=", value, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdIn(List<Integer> values) {
            addCriterion("typeId in", values, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdNotIn(List<Integer> values) {
            addCriterion("typeId not in", values, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdBetween(Integer value1, Integer value2) {
            addCriterion("typeId between", value1, value2, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdNotBetween(Integer value1, Integer value2) {
            addCriterion("typeId not between", value1, value2, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeNameIsNull() {
            addCriterion("typeName is null");
            return (Criteria) this;
        }

        public Criteria andTypeNameIsNotNull() {
            addCriterion("typeName is not null");
            return (Criteria) this;
        }

        public Criteria andTypeNameEqualTo(String value) {
            addCriterion("typeName =", value, "typeName");
            return (Criteria) this;
        }

        public Criteria andTypeNameNotEqualTo(String value) {
            addCriterion("typeName <>", value, "typeName");
            return (Criteria) this;
        }

        public Criteria andTypeNameGreaterThan(String value) {
            addCriterion("typeName >", value, "typeName");
            return (Criteria) this;
        }

        public Criteria andTypeNameGreaterThanOrEqualTo(String value) {
            addCriterion("typeName >=", value, "typeName");
            return (Criteria) this;
        }

        public Criteria andTypeNameLessThan(String value) {
            addCriterion("typeName <", value, "typeName");
            return (Criteria) this;
        }

        public Criteria andTypeNameLessThanOrEqualTo(String value) {
            addCriterion("typeName <=", value, "typeName");
            return (Criteria) this;
        }

        public Criteria andTypeNameLike(String value) {
            addCriterion("typeName like", value, "typeName");
            return (Criteria) this;
        }

        public Criteria andTypeNameNotLike(String value) {
            addCriterion("typeName not like", value, "typeName");
            return (Criteria) this;
        }

        public Criteria andTypeNameIn(List<String> values) {
            addCriterion("typeName in", values, "typeName");
            return (Criteria) this;
        }

        public Criteria andTypeNameNotIn(List<String> values) {
            addCriterion("typeName not in", values, "typeName");
            return (Criteria) this;
        }

        public Criteria andTypeNameBetween(String value1, String value2) {
            addCriterion("typeName between", value1, value2, "typeName");
            return (Criteria) this;
        }

        public Criteria andTypeNameNotBetween(String value1, String value2) {
            addCriterion("typeName not between", value1, value2, "typeName");
            return (Criteria) this;
        }

        public Criteria andSalesnumIsNull() {
            addCriterion("salesnum is null");
            return (Criteria) this;
        }

        public Criteria andSalesnumIsNotNull() {
            addCriterion("salesnum is not null");
            return (Criteria) this;
        }

        public Criteria andSalesnumEqualTo(Integer value) {
            addCriterion("salesnum =", value, "salesnum");
            return (Criteria) this;
        }

        public Criteria andSalesnumNotEqualTo(Integer value) {
            addCriterion("salesnum <>", value, "salesnum");
            return (Criteria) this;
        }

        public Criteria andSalesnumGreaterThan(Integer value) {
            addCriterion("salesnum >", value, "salesnum");
            return (Criteria) this;
        }

        public Criteria andSalesnumGreaterThanOrEqualTo(Integer value) {
            addCriterion("salesnum >=", value, "salesnum");
            return (Criteria) this;
        }

        public Criteria andSalesnumLessThan(Integer value) {
            addCriterion("salesnum <", value, "salesnum");
            return (Criteria) this;
        }

        public Criteria andSalesnumLessThanOrEqualTo(Integer value) {
            addCriterion("salesnum <=", value, "salesnum");
            return (Criteria) this;
        }

        public Criteria andSalesnumIn(List<Integer> values) {
            addCriterion("salesnum in", values, "salesnum");
            return (Criteria) this;
        }

        public Criteria andSalesnumNotIn(List<Integer> values) {
            addCriterion("salesnum not in", values, "salesnum");
            return (Criteria) this;
        }

        public Criteria andSalesnumBetween(Integer value1, Integer value2) {
            addCriterion("salesnum between", value1, value2, "salesnum");
            return (Criteria) this;
        }

        public Criteria andSalesnumNotBetween(Integer value1, Integer value2) {
            addCriterion("salesnum not between", value1, value2, "salesnum");
            return (Criteria) this;
        }

        public Criteria andSortIsNull() {
            addCriterion("sort is null");
            return (Criteria) this;
        }

        public Criteria andSortIsNotNull() {
            addCriterion("sort is not null");
            return (Criteria) this;
        }

        public Criteria andSortEqualTo(Integer value) {
            addCriterion("sort =", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotEqualTo(Integer value) {
            addCriterion("sort <>", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThan(Integer value) {
            addCriterion("sort >", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThanOrEqualTo(Integer value) {
            addCriterion("sort >=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThan(Integer value) {
            addCriterion("sort <", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThanOrEqualTo(Integer value) {
            addCriterion("sort <=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortIn(List<Integer> values) {
            addCriterion("sort in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotIn(List<Integer> values) {
            addCriterion("sort not in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortBetween(Integer value1, Integer value2) {
            addCriterion("sort between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotBetween(Integer value1, Integer value2) {
            addCriterion("sort not between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andStartPayIsNull() {
            addCriterion("startPay is null");
            return (Criteria) this;
        }

        public Criteria andStartPayIsNotNull() {
            addCriterion("startPay is not null");
            return (Criteria) this;
        }

        public Criteria andStartPayEqualTo(BigDecimal value) {
            addCriterion("startPay =", value, "startPay");
            return (Criteria) this;
        }

        public Criteria andStartPayNotEqualTo(BigDecimal value) {
            addCriterion("startPay <>", value, "startPay");
            return (Criteria) this;
        }

        public Criteria andStartPayGreaterThan(BigDecimal value) {
            addCriterion("startPay >", value, "startPay");
            return (Criteria) this;
        }

        public Criteria andStartPayGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("startPay >=", value, "startPay");
            return (Criteria) this;
        }

        public Criteria andStartPayLessThan(BigDecimal value) {
            addCriterion("startPay <", value, "startPay");
            return (Criteria) this;
        }

        public Criteria andStartPayLessThanOrEqualTo(BigDecimal value) {
            addCriterion("startPay <=", value, "startPay");
            return (Criteria) this;
        }

        public Criteria andStartPayIn(List<BigDecimal> values) {
            addCriterion("startPay in", values, "startPay");
            return (Criteria) this;
        }

        public Criteria andStartPayNotIn(List<BigDecimal> values) {
            addCriterion("startPay not in", values, "startPay");
            return (Criteria) this;
        }

        public Criteria andStartPayBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("startPay between", value1, value2, "startPay");
            return (Criteria) this;
        }

        public Criteria andStartPayNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("startPay not between", value1, value2, "startPay");
            return (Criteria) this;
        }

        public Criteria andEndworkIsNull() {
            addCriterion("endwork is null");
            return (Criteria) this;
        }

        public Criteria andEndworkIsNotNull() {
            addCriterion("endwork is not null");
            return (Criteria) this;
        }

        public Criteria andEndworkEqualTo(Date value) {
            addCriterion("endwork =", value, "endwork");
            return (Criteria) this;
        }

        public Criteria andEndworkNotEqualTo(Date value) {
            addCriterion("endwork <>", value, "endwork");
            return (Criteria) this;
        }

        public Criteria andEndworkGreaterThan(Date value) {
            addCriterion("endwork >", value, "endwork");
            return (Criteria) this;
        }

        public Criteria andEndworkGreaterThanOrEqualTo(Date value) {
            addCriterion("endwork >=", value, "endwork");
            return (Criteria) this;
        }

        public Criteria andEndworkLessThan(Date value) {
            addCriterion("endwork <", value, "endwork");
            return (Criteria) this;
        }

        public Criteria andEndworkLessThanOrEqualTo(Date value) {
            addCriterion("endwork <=", value, "endwork");
            return (Criteria) this;
        }

        public Criteria andEndworkIn(List<Date> values) {
            addCriterion("endwork in", values, "endwork");
            return (Criteria) this;
        }

        public Criteria andEndworkNotIn(List<Date> values) {
            addCriterion("endwork not in", values, "endwork");
            return (Criteria) this;
        }

        public Criteria andEndworkBetween(Date value1, Date value2) {
            addCriterion("endwork between", value1, value2, "endwork");
            return (Criteria) this;
        }

        public Criteria andEndworkNotBetween(Date value1, Date value2) {
            addCriterion("endwork not between", value1, value2, "endwork");
            return (Criteria) this;
        }

        public Criteria andStartworkIsNull() {
            addCriterion("startwork is null");
            return (Criteria) this;
        }

        public Criteria andStartworkIsNotNull() {
            addCriterion("startwork is not null");
            return (Criteria) this;
        }

        public Criteria andStartworkEqualTo(Date value) {
            addCriterion("startwork =", value, "startwork");
            return (Criteria) this;
        }

        public Criteria andStartworkNotEqualTo(Date value) {
            addCriterion("startwork <>", value, "startwork");
            return (Criteria) this;
        }

        public Criteria andStartworkGreaterThan(Date value) {
            addCriterion("startwork >", value, "startwork");
            return (Criteria) this;
        }

        public Criteria andStartworkGreaterThanOrEqualTo(Date value) {
            addCriterion("startwork >=", value, "startwork");
            return (Criteria) this;
        }

        public Criteria andStartworkLessThan(Date value) {
            addCriterion("startwork <", value, "startwork");
            return (Criteria) this;
        }

        public Criteria andStartworkLessThanOrEqualTo(Date value) {
            addCriterion("startwork <=", value, "startwork");
            return (Criteria) this;
        }

        public Criteria andStartworkIn(List<Date> values) {
            addCriterion("startwork in", values, "startwork");
            return (Criteria) this;
        }

        public Criteria andStartworkNotIn(List<Date> values) {
            addCriterion("startwork not in", values, "startwork");
            return (Criteria) this;
        }

        public Criteria andStartworkBetween(Date value1, Date value2) {
            addCriterion("startwork between", value1, value2, "startwork");
            return (Criteria) this;
        }

        public Criteria andStartworkNotBetween(Date value1, Date value2) {
            addCriterion("startwork not between", value1, value2, "startwork");
            return (Criteria) this;
        }

        public Criteria andWorktodayIsNull() {
            addCriterion("worktoday is null");
            return (Criteria) this;
        }

        public Criteria andWorktodayIsNotNull() {
            addCriterion("worktoday is not null");
            return (Criteria) this;
        }

        public Criteria andWorktodayEqualTo(String value) {
            addCriterion("worktoday =", value, "worktoday");
            return (Criteria) this;
        }

        public Criteria andWorktodayNotEqualTo(String value) {
            addCriterion("worktoday <>", value, "worktoday");
            return (Criteria) this;
        }

        public Criteria andWorktodayGreaterThan(String value) {
            addCriterion("worktoday >", value, "worktoday");
            return (Criteria) this;
        }

        public Criteria andWorktodayGreaterThanOrEqualTo(String value) {
            addCriterion("worktoday >=", value, "worktoday");
            return (Criteria) this;
        }

        public Criteria andWorktodayLessThan(String value) {
            addCriterion("worktoday <", value, "worktoday");
            return (Criteria) this;
        }

        public Criteria andWorktodayLessThanOrEqualTo(String value) {
            addCriterion("worktoday <=", value, "worktoday");
            return (Criteria) this;
        }

        public Criteria andWorktodayLike(String value) {
            addCriterion("worktoday like", value, "worktoday");
            return (Criteria) this;
        }

        public Criteria andWorktodayNotLike(String value) {
            addCriterion("worktoday not like", value, "worktoday");
            return (Criteria) this;
        }

        public Criteria andWorktodayIn(List<String> values) {
            addCriterion("worktoday in", values, "worktoday");
            return (Criteria) this;
        }

        public Criteria andWorktodayNotIn(List<String> values) {
            addCriterion("worktoday not in", values, "worktoday");
            return (Criteria) this;
        }

        public Criteria andWorktodayBetween(String value1, String value2) {
            addCriterion("worktoday between", value1, value2, "worktoday");
            return (Criteria) this;
        }

        public Criteria andWorktodayNotBetween(String value1, String value2) {
            addCriterion("worktoday not between", value1, value2, "worktoday");
            return (Criteria) this;
        }

        public Criteria andPackingIsNull() {
            addCriterion("packing is null");
            return (Criteria) this;
        }

        public Criteria andPackingIsNotNull() {
            addCriterion("packing is not null");
            return (Criteria) this;
        }

        public Criteria andPackingEqualTo(BigDecimal value) {
            addCriterion("packing =", value, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingNotEqualTo(BigDecimal value) {
            addCriterion("packing <>", value, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingGreaterThan(BigDecimal value) {
            addCriterion("packing >", value, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("packing >=", value, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingLessThan(BigDecimal value) {
            addCriterion("packing <", value, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingLessThanOrEqualTo(BigDecimal value) {
            addCriterion("packing <=", value, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingIn(List<BigDecimal> values) {
            addCriterion("packing in", values, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingNotIn(List<BigDecimal> values) {
            addCriterion("packing not in", values, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("packing between", value1, value2, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("packing not between", value1, value2, "packing");
            return (Criteria) this;
        }

        public Criteria andIsDeliverIsNull() {
            addCriterion("isDeliver is null");
            return (Criteria) this;
        }

        public Criteria andIsDeliverIsNotNull() {
            addCriterion("isDeliver is not null");
            return (Criteria) this;
        }

        public Criteria andIsDeliverEqualTo(Integer value) {
            addCriterion("isDeliver =", value, "isDeliver");
            return (Criteria) this;
        }

        public Criteria andIsDeliverNotEqualTo(Integer value) {
            addCriterion("isDeliver <>", value, "isDeliver");
            return (Criteria) this;
        }

        public Criteria andIsDeliverGreaterThan(Integer value) {
            addCriterion("isDeliver >", value, "isDeliver");
            return (Criteria) this;
        }

        public Criteria andIsDeliverGreaterThanOrEqualTo(Integer value) {
            addCriterion("isDeliver >=", value, "isDeliver");
            return (Criteria) this;
        }

        public Criteria andIsDeliverLessThan(Integer value) {
            addCriterion("isDeliver <", value, "isDeliver");
            return (Criteria) this;
        }

        public Criteria andIsDeliverLessThanOrEqualTo(Integer value) {
            addCriterion("isDeliver <=", value, "isDeliver");
            return (Criteria) this;
        }

        public Criteria andIsDeliverIn(List<Integer> values) {
            addCriterion("isDeliver in", values, "isDeliver");
            return (Criteria) this;
        }

        public Criteria andIsDeliverNotIn(List<Integer> values) {
            addCriterion("isDeliver not in", values, "isDeliver");
            return (Criteria) this;
        }

        public Criteria andIsDeliverBetween(Integer value1, Integer value2) {
            addCriterion("isDeliver between", value1, value2, "isDeliver");
            return (Criteria) this;
        }

        public Criteria andIsDeliverNotBetween(Integer value1, Integer value2) {
            addCriterion("isDeliver not between", value1, value2, "isDeliver");
            return (Criteria) this;
        }

        public Criteria andRecommendIsNull() {
            addCriterion("recommend is null");
            return (Criteria) this;
        }

        public Criteria andRecommendIsNotNull() {
            addCriterion("recommend is not null");
            return (Criteria) this;
        }

        public Criteria andRecommendEqualTo(Integer value) {
            addCriterion("recommend =", value, "recommend");
            return (Criteria) this;
        }

        public Criteria andRecommendNotEqualTo(Integer value) {
            addCriterion("recommend <>", value, "recommend");
            return (Criteria) this;
        }

        public Criteria andRecommendGreaterThan(Integer value) {
            addCriterion("recommend >", value, "recommend");
            return (Criteria) this;
        }

        public Criteria andRecommendGreaterThanOrEqualTo(Integer value) {
            addCriterion("recommend >=", value, "recommend");
            return (Criteria) this;
        }

        public Criteria andRecommendLessThan(Integer value) {
            addCriterion("recommend <", value, "recommend");
            return (Criteria) this;
        }

        public Criteria andRecommendLessThanOrEqualTo(Integer value) {
            addCriterion("recommend <=", value, "recommend");
            return (Criteria) this;
        }

        public Criteria andRecommendIn(List<Integer> values) {
            addCriterion("recommend in", values, "recommend");
            return (Criteria) this;
        }

        public Criteria andRecommendNotIn(List<Integer> values) {
            addCriterion("recommend not in", values, "recommend");
            return (Criteria) this;
        }

        public Criteria andRecommendBetween(Integer value1, Integer value2) {
            addCriterion("recommend between", value1, value2, "recommend");
            return (Criteria) this;
        }

        public Criteria andRecommendNotBetween(Integer value1, Integer value2) {
            addCriterion("recommend not between", value1, value2, "recommend");
            return (Criteria) this;
        }

        public Criteria andSpeedIsNull() {
            addCriterion("speed is null");
            return (Criteria) this;
        }

        public Criteria andSpeedIsNotNull() {
            addCriterion("speed is not null");
            return (Criteria) this;
        }

        public Criteria andSpeedEqualTo(Integer value) {
            addCriterion("speed =", value, "speed");
            return (Criteria) this;
        }

        public Criteria andSpeedNotEqualTo(Integer value) {
            addCriterion("speed <>", value, "speed");
            return (Criteria) this;
        }

        public Criteria andSpeedGreaterThan(Integer value) {
            addCriterion("speed >", value, "speed");
            return (Criteria) this;
        }

        public Criteria andSpeedGreaterThanOrEqualTo(Integer value) {
            addCriterion("speed >=", value, "speed");
            return (Criteria) this;
        }

        public Criteria andSpeedLessThan(Integer value) {
            addCriterion("speed <", value, "speed");
            return (Criteria) this;
        }

        public Criteria andSpeedLessThanOrEqualTo(Integer value) {
            addCriterion("speed <=", value, "speed");
            return (Criteria) this;
        }

        public Criteria andSpeedIn(List<Integer> values) {
            addCriterion("speed in", values, "speed");
            return (Criteria) this;
        }

        public Criteria andSpeedNotIn(List<Integer> values) {
            addCriterion("speed not in", values, "speed");
            return (Criteria) this;
        }

        public Criteria andSpeedBetween(Integer value1, Integer value2) {
            addCriterion("speed between", value1, value2, "speed");
            return (Criteria) this;
        }

        public Criteria andSpeedNotBetween(Integer value1, Integer value2) {
            addCriterion("speed not between", value1, value2, "speed");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andEndTime1IsNull() {
            addCriterion("endTime1 is null");
            return (Criteria) this;
        }

        public Criteria andEndTime1IsNotNull() {
            addCriterion("endTime1 is not null");
            return (Criteria) this;
        }

        public Criteria andEndTime1EqualTo(Date value) {
            addCriterionForJDBCTime("endTime1 =", value, "endTime1");
            return (Criteria) this;
        }

        public Criteria andEndTime1NotEqualTo(Date value) {
            addCriterionForJDBCTime("endTime1 <>", value, "endTime1");
            return (Criteria) this;
        }

        public Criteria andEndTime1GreaterThan(Date value) {
            addCriterionForJDBCTime("endTime1 >", value, "endTime1");
            return (Criteria) this;
        }

        public Criteria andEndTime1GreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("endTime1 >=", value, "endTime1");
            return (Criteria) this;
        }

        public Criteria andEndTime1LessThan(Date value) {
            addCriterionForJDBCTime("endTime1 <", value, "endTime1");
            return (Criteria) this;
        }

        public Criteria andEndTime1LessThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("endTime1 <=", value, "endTime1");
            return (Criteria) this;
        }

        public Criteria andEndTime1In(List<Date> values) {
            addCriterionForJDBCTime("endTime1 in", values, "endTime1");
            return (Criteria) this;
        }

        public Criteria andEndTime1NotIn(List<Date> values) {
            addCriterionForJDBCTime("endTime1 not in", values, "endTime1");
            return (Criteria) this;
        }

        public Criteria andEndTime1Between(Date value1, Date value2) {
            addCriterionForJDBCTime("endTime1 between", value1, value2, "endTime1");
            return (Criteria) this;
        }

        public Criteria andEndTime1NotBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("endTime1 not between", value1, value2, "endTime1");
            return (Criteria) this;
        }

        public Criteria andEndTime2IsNull() {
            addCriterion("endTime2 is null");
            return (Criteria) this;
        }

        public Criteria andEndTime2IsNotNull() {
            addCriterion("endTime2 is not null");
            return (Criteria) this;
        }

        public Criteria andEndTime2EqualTo(Date value) {
            addCriterionForJDBCTime("endTime2 =", value, "endTime2");
            return (Criteria) this;
        }

        public Criteria andEndTime2NotEqualTo(Date value) {
            addCriterionForJDBCTime("endTime2 <>", value, "endTime2");
            return (Criteria) this;
        }

        public Criteria andEndTime2GreaterThan(Date value) {
            addCriterionForJDBCTime("endTime2 >", value, "endTime2");
            return (Criteria) this;
        }

        public Criteria andEndTime2GreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("endTime2 >=", value, "endTime2");
            return (Criteria) this;
        }

        public Criteria andEndTime2LessThan(Date value) {
            addCriterionForJDBCTime("endTime2 <", value, "endTime2");
            return (Criteria) this;
        }

        public Criteria andEndTime2LessThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("endTime2 <=", value, "endTime2");
            return (Criteria) this;
        }

        public Criteria andEndTime2In(List<Date> values) {
            addCriterionForJDBCTime("endTime2 in", values, "endTime2");
            return (Criteria) this;
        }

        public Criteria andEndTime2NotIn(List<Date> values) {
            addCriterionForJDBCTime("endTime2 not in", values, "endTime2");
            return (Criteria) this;
        }

        public Criteria andEndTime2Between(Date value1, Date value2) {
            addCriterionForJDBCTime("endTime2 between", value1, value2, "endTime2");
            return (Criteria) this;
        }

        public Criteria andEndTime2NotBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("endTime2 not between", value1, value2, "endTime2");
            return (Criteria) this;
        }

        public Criteria andStartTime1IsNull() {
            addCriterion("startTime1 is null");
            return (Criteria) this;
        }

        public Criteria andStartTime1IsNotNull() {
            addCriterion("startTime1 is not null");
            return (Criteria) this;
        }

        public Criteria andStartTime1EqualTo(Date value) {
            addCriterionForJDBCTime("startTime1 =", value, "startTime1");
            return (Criteria) this;
        }

        public Criteria andStartTime1NotEqualTo(Date value) {
            addCriterionForJDBCTime("startTime1 <>", value, "startTime1");
            return (Criteria) this;
        }

        public Criteria andStartTime1GreaterThan(Date value) {
            addCriterionForJDBCTime("startTime1 >", value, "startTime1");
            return (Criteria) this;
        }

        public Criteria andStartTime1GreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("startTime1 >=", value, "startTime1");
            return (Criteria) this;
        }

        public Criteria andStartTime1LessThan(Date value) {
            addCriterionForJDBCTime("startTime1 <", value, "startTime1");
            return (Criteria) this;
        }

        public Criteria andStartTime1LessThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("startTime1 <=", value, "startTime1");
            return (Criteria) this;
        }

        public Criteria andStartTime1In(List<Date> values) {
            addCriterionForJDBCTime("startTime1 in", values, "startTime1");
            return (Criteria) this;
        }

        public Criteria andStartTime1NotIn(List<Date> values) {
            addCriterionForJDBCTime("startTime1 not in", values, "startTime1");
            return (Criteria) this;
        }

        public Criteria andStartTime1Between(Date value1, Date value2) {
            addCriterionForJDBCTime("startTime1 between", value1, value2, "startTime1");
            return (Criteria) this;
        }

        public Criteria andStartTime1NotBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("startTime1 not between", value1, value2, "startTime1");
            return (Criteria) this;
        }

        public Criteria andStartTime2IsNull() {
            addCriterion("startTime2 is null");
            return (Criteria) this;
        }

        public Criteria andStartTime2IsNotNull() {
            addCriterion("startTime2 is not null");
            return (Criteria) this;
        }

        public Criteria andStartTime2EqualTo(Date value) {
            addCriterionForJDBCTime("startTime2 =", value, "startTime2");
            return (Criteria) this;
        }

        public Criteria andStartTime2NotEqualTo(Date value) {
            addCriterionForJDBCTime("startTime2 <>", value, "startTime2");
            return (Criteria) this;
        }

        public Criteria andStartTime2GreaterThan(Date value) {
            addCriterionForJDBCTime("startTime2 >", value, "startTime2");
            return (Criteria) this;
        }

        public Criteria andStartTime2GreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("startTime2 >=", value, "startTime2");
            return (Criteria) this;
        }

        public Criteria andStartTime2LessThan(Date value) {
            addCriterionForJDBCTime("startTime2 <", value, "startTime2");
            return (Criteria) this;
        }

        public Criteria andStartTime2LessThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("startTime2 <=", value, "startTime2");
            return (Criteria) this;
        }

        public Criteria andStartTime2In(List<Date> values) {
            addCriterionForJDBCTime("startTime2 in", values, "startTime2");
            return (Criteria) this;
        }

        public Criteria andStartTime2NotIn(List<Date> values) {
            addCriterionForJDBCTime("startTime2 not in", values, "startTime2");
            return (Criteria) this;
        }

        public Criteria andStartTime2Between(Date value1, Date value2) {
            addCriterionForJDBCTime("startTime2 between", value1, value2, "startTime2");
            return (Criteria) this;
        }

        public Criteria andStartTime2NotBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("startTime2 not between", value1, value2, "startTime2");
            return (Criteria) this;
        }

        public Criteria andCoefficientIsNull() {
            addCriterion("coefficient is null");
            return (Criteria) this;
        }

        public Criteria andCoefficientIsNotNull() {
            addCriterion("coefficient is not null");
            return (Criteria) this;
        }

        public Criteria andCoefficientEqualTo(Double value) {
            addCriterion("coefficient =", value, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientNotEqualTo(Double value) {
            addCriterion("coefficient <>", value, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientGreaterThan(Double value) {
            addCriterion("coefficient >", value, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientGreaterThanOrEqualTo(Double value) {
            addCriterion("coefficient >=", value, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientLessThan(Double value) {
            addCriterion("coefficient <", value, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientLessThanOrEqualTo(Double value) {
            addCriterion("coefficient <=", value, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientIn(List<Double> values) {
            addCriterion("coefficient in", values, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientNotIn(List<Double> values) {
            addCriterion("coefficient not in", values, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientBetween(Double value1, Double value2) {
            addCriterion("coefficient between", value1, value2, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientNotBetween(Double value1, Double value2) {
            addCriterion("coefficient not between", value1, value2, "coefficient");
            return (Criteria) this;
        }

        public Criteria andMinmonetyIsNull() {
            addCriterion("minmonety is null");
            return (Criteria) this;
        }

        public Criteria andMinmonetyIsNotNull() {
            addCriterion("minmonety is not null");
            return (Criteria) this;
        }

        public Criteria andMinmonetyEqualTo(BigDecimal value) {
            addCriterion("minmonety =", value, "minmonety");
            return (Criteria) this;
        }

        public Criteria andMinmonetyNotEqualTo(BigDecimal value) {
            addCriterion("minmonety <>", value, "minmonety");
            return (Criteria) this;
        }

        public Criteria andMinmonetyGreaterThan(BigDecimal value) {
            addCriterion("minmonety >", value, "minmonety");
            return (Criteria) this;
        }

        public Criteria andMinmonetyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("minmonety >=", value, "minmonety");
            return (Criteria) this;
        }

        public Criteria andMinmonetyLessThan(BigDecimal value) {
            addCriterion("minmonety <", value, "minmonety");
            return (Criteria) this;
        }

        public Criteria andMinmonetyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("minmonety <=", value, "minmonety");
            return (Criteria) this;
        }

        public Criteria andMinmonetyIn(List<BigDecimal> values) {
            addCriterion("minmonety in", values, "minmonety");
            return (Criteria) this;
        }

        public Criteria andMinmonetyNotIn(List<BigDecimal> values) {
            addCriterion("minmonety not in", values, "minmonety");
            return (Criteria) this;
        }

        public Criteria andMinmonetyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("minmonety between", value1, value2, "minmonety");
            return (Criteria) this;
        }

        public Criteria andMinmonetyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("minmonety not between", value1, value2, "minmonety");
            return (Criteria) this;
        }

        public Criteria andPeriodIsNull() {
            addCriterion("period is null");
            return (Criteria) this;
        }

        public Criteria andPeriodIsNotNull() {
            addCriterion("period is not null");
            return (Criteria) this;
        }

        public Criteria andPeriodEqualTo(Integer value) {
            addCriterion("period =", value, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodNotEqualTo(Integer value) {
            addCriterion("period <>", value, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodGreaterThan(Integer value) {
            addCriterion("period >", value, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodGreaterThanOrEqualTo(Integer value) {
            addCriterion("period >=", value, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodLessThan(Integer value) {
            addCriterion("period <", value, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodLessThanOrEqualTo(Integer value) {
            addCriterion("period <=", value, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodIn(List<Integer> values) {
            addCriterion("period in", values, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodNotIn(List<Integer> values) {
            addCriterion("period not in", values, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodBetween(Integer value1, Integer value2) {
            addCriterion("period between", value1, value2, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodNotBetween(Integer value1, Integer value2) {
            addCriterion("period not between", value1, value2, "period");
            return (Criteria) this;
        }

        public Criteria andAccountIsNull() {
            addCriterion("account is null");
            return (Criteria) this;
        }

        public Criteria andAccountIsNotNull() {
            addCriterion("account is not null");
            return (Criteria) this;
        }

        public Criteria andAccountEqualTo(String value) {
            addCriterion("account =", value, "account");
            return (Criteria) this;
        }

        public Criteria andAccountNotEqualTo(String value) {
            addCriterion("account <>", value, "account");
            return (Criteria) this;
        }

        public Criteria andAccountGreaterThan(String value) {
            addCriterion("account >", value, "account");
            return (Criteria) this;
        }

        public Criteria andAccountGreaterThanOrEqualTo(String value) {
            addCriterion("account >=", value, "account");
            return (Criteria) this;
        }

        public Criteria andAccountLessThan(String value) {
            addCriterion("account <", value, "account");
            return (Criteria) this;
        }

        public Criteria andAccountLessThanOrEqualTo(String value) {
            addCriterion("account <=", value, "account");
            return (Criteria) this;
        }

        public Criteria andAccountLike(String value) {
            addCriterion("account like", value, "account");
            return (Criteria) this;
        }

        public Criteria andAccountNotLike(String value) {
            addCriterion("account not like", value, "account");
            return (Criteria) this;
        }

        public Criteria andAccountIn(List<String> values) {
            addCriterion("account in", values, "account");
            return (Criteria) this;
        }

        public Criteria andAccountNotIn(List<String> values) {
            addCriterion("account not in", values, "account");
            return (Criteria) this;
        }

        public Criteria andAccountBetween(String value1, String value2) {
            addCriterion("account between", value1, value2, "account");
            return (Criteria) this;
        }

        public Criteria andAccountNotBetween(String value1, String value2) {
            addCriterion("account not between", value1, value2, "account");
            return (Criteria) this;
        }

        public Criteria andShowpsIsNull() {
            addCriterion("showps is null");
            return (Criteria) this;
        }

        public Criteria andShowpsIsNotNull() {
            addCriterion("showps is not null");
            return (Criteria) this;
        }

        public Criteria andShowpsEqualTo(BigDecimal value) {
            addCriterion("showps =", value, "showps");
            return (Criteria) this;
        }

        public Criteria andShowpsNotEqualTo(BigDecimal value) {
            addCriterion("showps <>", value, "showps");
            return (Criteria) this;
        }

        public Criteria andShowpsGreaterThan(BigDecimal value) {
            addCriterion("showps >", value, "showps");
            return (Criteria) this;
        }

        public Criteria andShowpsGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("showps >=", value, "showps");
            return (Criteria) this;
        }

        public Criteria andShowpsLessThan(BigDecimal value) {
            addCriterion("showps <", value, "showps");
            return (Criteria) this;
        }

        public Criteria andShowpsLessThanOrEqualTo(BigDecimal value) {
            addCriterion("showps <=", value, "showps");
            return (Criteria) this;
        }

        public Criteria andShowpsIn(List<BigDecimal> values) {
            addCriterion("showps in", values, "showps");
            return (Criteria) this;
        }

        public Criteria andShowpsNotIn(List<BigDecimal> values) {
            addCriterion("showps not in", values, "showps");
            return (Criteria) this;
        }

        public Criteria andShowpsBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("showps between", value1, value2, "showps");
            return (Criteria) this;
        }

        public Criteria andShowpsNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("showps not between", value1, value2, "showps");
            return (Criteria) this;
        }

        public Criteria andBusshowpsIsNull() {
            addCriterion("busshowps is null");
            return (Criteria) this;
        }

        public Criteria andBusshowpsIsNotNull() {
            addCriterion("busshowps is not null");
            return (Criteria) this;
        }

        public Criteria andBusshowpsEqualTo(BigDecimal value) {
            addCriterion("busshowps =", value, "busshowps");
            return (Criteria) this;
        }

        public Criteria andBusshowpsNotEqualTo(BigDecimal value) {
            addCriterion("busshowps <>", value, "busshowps");
            return (Criteria) this;
        }

        public Criteria andBusshowpsGreaterThan(BigDecimal value) {
            addCriterion("busshowps >", value, "busshowps");
            return (Criteria) this;
        }

        public Criteria andBusshowpsGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("busshowps >=", value, "busshowps");
            return (Criteria) this;
        }

        public Criteria andBusshowpsLessThan(BigDecimal value) {
            addCriterion("busshowps <", value, "busshowps");
            return (Criteria) this;
        }

        public Criteria andBusshowpsLessThanOrEqualTo(BigDecimal value) {
            addCriterion("busshowps <=", value, "busshowps");
            return (Criteria) this;
        }

        public Criteria andBusshowpsIn(List<BigDecimal> values) {
            addCriterion("busshowps in", values, "busshowps");
            return (Criteria) this;
        }

        public Criteria andBusshowpsNotIn(List<BigDecimal> values) {
            addCriterion("busshowps not in", values, "busshowps");
            return (Criteria) this;
        }

        public Criteria andBusshowpsBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("busshowps between", value1, value2, "busshowps");
            return (Criteria) this;
        }

        public Criteria andBusshowpsNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("busshowps not between", value1, value2, "busshowps");
            return (Criteria) this;
        }

        public Criteria andEndwork2IsNull() {
            addCriterion("endwork2 is null");
            return (Criteria) this;
        }

        public Criteria andEndwork2IsNotNull() {
            addCriterion("endwork2 is not null");
            return (Criteria) this;
        }

        public Criteria andEndwork2EqualTo(Date value) {
            addCriterion("endwork2 =", value, "endwork2");
            return (Criteria) this;
        }

        public Criteria andEndwork2NotEqualTo(Date value) {
            addCriterion("endwork2 <>", value, "endwork2");
            return (Criteria) this;
        }

        public Criteria andEndwork2GreaterThan(Date value) {
            addCriterion("endwork2 >", value, "endwork2");
            return (Criteria) this;
        }

        public Criteria andEndwork2GreaterThanOrEqualTo(Date value) {
            addCriterion("endwork2 >=", value, "endwork2");
            return (Criteria) this;
        }

        public Criteria andEndwork2LessThan(Date value) {
            addCriterion("endwork2 <", value, "endwork2");
            return (Criteria) this;
        }

        public Criteria andEndwork2LessThanOrEqualTo(Date value) {
            addCriterion("endwork2 <=", value, "endwork2");
            return (Criteria) this;
        }

        public Criteria andEndwork2In(List<Date> values) {
            addCriterion("endwork2 in", values, "endwork2");
            return (Criteria) this;
        }

        public Criteria andEndwork2NotIn(List<Date> values) {
            addCriterion("endwork2 not in", values, "endwork2");
            return (Criteria) this;
        }

        public Criteria andEndwork2Between(Date value1, Date value2) {
            addCriterion("endwork2 between", value1, value2, "endwork2");
            return (Criteria) this;
        }

        public Criteria andEndwork2NotBetween(Date value1, Date value2) {
            addCriterion("endwork2 not between", value1, value2, "endwork2");
            return (Criteria) this;
        }

        public Criteria andStartwork2IsNull() {
            addCriterion("startwork2 is null");
            return (Criteria) this;
        }

        public Criteria andStartwork2IsNotNull() {
            addCriterion("startwork2 is not null");
            return (Criteria) this;
        }

        public Criteria andStartwork2EqualTo(Date value) {
            addCriterion("startwork2 =", value, "startwork2");
            return (Criteria) this;
        }

        public Criteria andStartwork2NotEqualTo(Date value) {
            addCriterion("startwork2 <>", value, "startwork2");
            return (Criteria) this;
        }

        public Criteria andStartwork2GreaterThan(Date value) {
            addCriterion("startwork2 >", value, "startwork2");
            return (Criteria) this;
        }

        public Criteria andStartwork2GreaterThanOrEqualTo(Date value) {
            addCriterion("startwork2 >=", value, "startwork2");
            return (Criteria) this;
        }

        public Criteria andStartwork2LessThan(Date value) {
            addCriterion("startwork2 <", value, "startwork2");
            return (Criteria) this;
        }

        public Criteria andStartwork2LessThanOrEqualTo(Date value) {
            addCriterion("startwork2 <=", value, "startwork2");
            return (Criteria) this;
        }

        public Criteria andStartwork2In(List<Date> values) {
            addCriterion("startwork2 in", values, "startwork2");
            return (Criteria) this;
        }

        public Criteria andStartwork2NotIn(List<Date> values) {
            addCriterion("startwork2 not in", values, "startwork2");
            return (Criteria) this;
        }

        public Criteria andStartwork2Between(Date value1, Date value2) {
            addCriterion("startwork2 between", value1, value2, "startwork2");
            return (Criteria) this;
        }

        public Criteria andStartwork2NotBetween(Date value1, Date value2) {
            addCriterion("startwork2 not between", value1, value2, "startwork2");
            return (Criteria) this;
        }

        public Criteria andStatuIsNull() {
            addCriterion("statu is null");
            return (Criteria) this;
        }

        public Criteria andStatuIsNotNull() {
            addCriterion("statu is not null");
            return (Criteria) this;
        }

        public Criteria andStatuEqualTo(Integer value) {
            addCriterion("statu =", value, "statu");
            return (Criteria) this;
        }

        public Criteria andStatuNotEqualTo(Integer value) {
            addCriterion("statu <>", value, "statu");
            return (Criteria) this;
        }

        public Criteria andStatuGreaterThan(Integer value) {
            addCriterion("statu >", value, "statu");
            return (Criteria) this;
        }

        public Criteria andStatuGreaterThanOrEqualTo(Integer value) {
            addCriterion("statu >=", value, "statu");
            return (Criteria) this;
        }

        public Criteria andStatuLessThan(Integer value) {
            addCriterion("statu <", value, "statu");
            return (Criteria) this;
        }

        public Criteria andStatuLessThanOrEqualTo(Integer value) {
            addCriterion("statu <=", value, "statu");
            return (Criteria) this;
        }

        public Criteria andStatuIn(List<Integer> values) {
            addCriterion("statu in", values, "statu");
            return (Criteria) this;
        }

        public Criteria andStatuNotIn(List<Integer> values) {
            addCriterion("statu not in", values, "statu");
            return (Criteria) this;
        }

        public Criteria andStatuBetween(Integer value1, Integer value2) {
            addCriterion("statu between", value1, value2, "statu");
            return (Criteria) this;
        }

        public Criteria andStatuNotBetween(Integer value1, Integer value2) {
            addCriterion("statu not between", value1, value2, "statu");
            return (Criteria) this;
        }

        public Criteria andAgentIdIsNull() {
            addCriterion("agentId is null");
            return (Criteria) this;
        }

        public Criteria andAgentIdIsNotNull() {
            addCriterion("agentId is not null");
            return (Criteria) this;
        }

        public Criteria andAgentIdEqualTo(Integer value) {
            addCriterion("agentId =", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotEqualTo(Integer value) {
            addCriterion("agentId <>", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdGreaterThan(Integer value) {
            addCriterion("agentId >", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("agentId >=", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdLessThan(Integer value) {
            addCriterion("agentId <", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdLessThanOrEqualTo(Integer value) {
            addCriterion("agentId <=", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdIn(List<Integer> values) {
            addCriterion("agentId in", values, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotIn(List<Integer> values) {
            addCriterion("agentId not in", values, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdBetween(Integer value1, Integer value2) {
            addCriterion("agentId between", value1, value2, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotBetween(Integer value1, Integer value2) {
            addCriterion("agentId not between", value1, value2, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentNameIsNull() {
            addCriterion("agentName is null");
            return (Criteria) this;
        }

        public Criteria andAgentNameIsNotNull() {
            addCriterion("agentName is not null");
            return (Criteria) this;
        }

        public Criteria andAgentNameEqualTo(String value) {
            addCriterion("agentName =", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotEqualTo(String value) {
            addCriterion("agentName <>", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameGreaterThan(String value) {
            addCriterion("agentName >", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameGreaterThanOrEqualTo(String value) {
            addCriterion("agentName >=", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameLessThan(String value) {
            addCriterion("agentName <", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameLessThanOrEqualTo(String value) {
            addCriterion("agentName <=", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameLike(String value) {
            addCriterion("agentName like", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotLike(String value) {
            addCriterion("agentName not like", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameIn(List<String> values) {
            addCriterion("agentName in", values, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotIn(List<String> values) {
            addCriterion("agentName not in", values, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameBetween(String value1, String value2) {
            addCriterion("agentName between", value1, value2, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotBetween(String value1, String value2) {
            addCriterion("agentName not between", value1, value2, "agentName");
            return (Criteria) this;
        }

        public Criteria andStatusxIsNull() {
            addCriterion("statusx is null");
            return (Criteria) this;
        }

        public Criteria andStatusxIsNotNull() {
            addCriterion("statusx is not null");
            return (Criteria) this;
        }

        public Criteria andStatusxEqualTo(Integer value) {
            addCriterion("statusx =", value, "statusx");
            return (Criteria) this;
        }

        public Criteria andStatusxNotEqualTo(Integer value) {
            addCriterion("statusx <>", value, "statusx");
            return (Criteria) this;
        }

        public Criteria andStatusxGreaterThan(Integer value) {
            addCriterion("statusx >", value, "statusx");
            return (Criteria) this;
        }

        public Criteria andStatusxGreaterThanOrEqualTo(Integer value) {
            addCriterion("statusx >=", value, "statusx");
            return (Criteria) this;
        }

        public Criteria andStatusxLessThan(Integer value) {
            addCriterion("statusx <", value, "statusx");
            return (Criteria) this;
        }

        public Criteria andStatusxLessThanOrEqualTo(Integer value) {
            addCriterion("statusx <=", value, "statusx");
            return (Criteria) this;
        }

        public Criteria andStatusxIn(List<Integer> values) {
            addCriterion("statusx in", values, "statusx");
            return (Criteria) this;
        }

        public Criteria andStatusxNotIn(List<Integer> values) {
            addCriterion("statusx not in", values, "statusx");
            return (Criteria) this;
        }

        public Criteria andStatusxBetween(Integer value1, Integer value2) {
            addCriterion("statusx between", value1, value2, "statusx");
            return (Criteria) this;
        }

        public Criteria andStatusxNotBetween(Integer value1, Integer value2) {
            addCriterion("statusx not between", value1, value2, "statusx");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeIsNull() {
            addCriterion("distributionTime is null");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeIsNotNull() {
            addCriterion("distributionTime is not null");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeEqualTo(Integer value) {
            addCriterion("distributionTime =", value, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeNotEqualTo(Integer value) {
            addCriterion("distributionTime <>", value, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeGreaterThan(Integer value) {
            addCriterion("distributionTime >", value, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeGreaterThanOrEqualTo(Integer value) {
            addCriterion("distributionTime >=", value, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeLessThan(Integer value) {
            addCriterion("distributionTime <", value, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeLessThanOrEqualTo(Integer value) {
            addCriterion("distributionTime <=", value, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeIn(List<Integer> values) {
            addCriterion("distributionTime in", values, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeNotIn(List<Integer> values) {
            addCriterion("distributionTime not in", values, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeBetween(Integer value1, Integer value2) {
            addCriterion("distributionTime between", value1, value2, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeNotBetween(Integer value1, Integer value2) {
            addCriterion("distributionTime not between", value1, value2, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andPackTimeIsNull() {
            addCriterion("packTime is null");
            return (Criteria) this;
        }

        public Criteria andPackTimeIsNotNull() {
            addCriterion("packTime is not null");
            return (Criteria) this;
        }

        public Criteria andPackTimeEqualTo(Integer value) {
            addCriterion("packTime =", value, "packTime");
            return (Criteria) this;
        }

        public Criteria andPackTimeNotEqualTo(Integer value) {
            addCriterion("packTime <>", value, "packTime");
            return (Criteria) this;
        }

        public Criteria andPackTimeGreaterThan(Integer value) {
            addCriterion("packTime >", value, "packTime");
            return (Criteria) this;
        }

        public Criteria andPackTimeGreaterThanOrEqualTo(Integer value) {
            addCriterion("packTime >=", value, "packTime");
            return (Criteria) this;
        }

        public Criteria andPackTimeLessThan(Integer value) {
            addCriterion("packTime <", value, "packTime");
            return (Criteria) this;
        }

        public Criteria andPackTimeLessThanOrEqualTo(Integer value) {
            addCriterion("packTime <=", value, "packTime");
            return (Criteria) this;
        }

        public Criteria andPackTimeIn(List<Integer> values) {
            addCriterion("packTime in", values, "packTime");
            return (Criteria) this;
        }

        public Criteria andPackTimeNotIn(List<Integer> values) {
            addCriterion("packTime not in", values, "packTime");
            return (Criteria) this;
        }

        public Criteria andPackTimeBetween(Integer value1, Integer value2) {
            addCriterion("packTime between", value1, value2, "packTime");
            return (Criteria) this;
        }

        public Criteria andPackTimeNotBetween(Integer value1, Integer value2) {
            addCriterion("packTime not between", value1, value2, "packTime");
            return (Criteria) this;
        }

        public Criteria andBankIsNull() {
            addCriterion("bank is null");
            return (Criteria) this;
        }

        public Criteria andBankIsNotNull() {
            addCriterion("bank is not null");
            return (Criteria) this;
        }

        public Criteria andBankEqualTo(Integer value) {
            addCriterion("bank =", value, "bank");
            return (Criteria) this;
        }

        public Criteria andBankNotEqualTo(Integer value) {
            addCriterion("bank <>", value, "bank");
            return (Criteria) this;
        }

        public Criteria andBankGreaterThan(Integer value) {
            addCriterion("bank >", value, "bank");
            return (Criteria) this;
        }

        public Criteria andBankGreaterThanOrEqualTo(Integer value) {
            addCriterion("bank >=", value, "bank");
            return (Criteria) this;
        }

        public Criteria andBankLessThan(Integer value) {
            addCriterion("bank <", value, "bank");
            return (Criteria) this;
        }

        public Criteria andBankLessThanOrEqualTo(Integer value) {
            addCriterion("bank <=", value, "bank");
            return (Criteria) this;
        }

        public Criteria andBankIn(List<Integer> values) {
            addCriterion("bank in", values, "bank");
            return (Criteria) this;
        }

        public Criteria andBankNotIn(List<Integer> values) {
            addCriterion("bank not in", values, "bank");
            return (Criteria) this;
        }

        public Criteria andBankBetween(Integer value1, Integer value2) {
            addCriterion("bank between", value1, value2, "bank");
            return (Criteria) this;
        }

        public Criteria andBankNotBetween(Integer value1, Integer value2) {
            addCriterion("bank not between", value1, value2, "bank");
            return (Criteria) this;
        }

        public Criteria andEstablishbankIsNull() {
            addCriterion("establishbank is null");
            return (Criteria) this;
        }

        public Criteria andEstablishbankIsNotNull() {
            addCriterion("establishbank is not null");
            return (Criteria) this;
        }

        public Criteria andEstablishbankEqualTo(String value) {
            addCriterion("establishbank =", value, "establishbank");
            return (Criteria) this;
        }

        public Criteria andEstablishbankNotEqualTo(String value) {
            addCriterion("establishbank <>", value, "establishbank");
            return (Criteria) this;
        }

        public Criteria andEstablishbankGreaterThan(String value) {
            addCriterion("establishbank >", value, "establishbank");
            return (Criteria) this;
        }

        public Criteria andEstablishbankGreaterThanOrEqualTo(String value) {
            addCriterion("establishbank >=", value, "establishbank");
            return (Criteria) this;
        }

        public Criteria andEstablishbankLessThan(String value) {
            addCriterion("establishbank <", value, "establishbank");
            return (Criteria) this;
        }

        public Criteria andEstablishbankLessThanOrEqualTo(String value) {
            addCriterion("establishbank <=", value, "establishbank");
            return (Criteria) this;
        }

        public Criteria andEstablishbankLike(String value) {
            addCriterion("establishbank like", value, "establishbank");
            return (Criteria) this;
        }

        public Criteria andEstablishbankNotLike(String value) {
            addCriterion("establishbank not like", value, "establishbank");
            return (Criteria) this;
        }

        public Criteria andEstablishbankIn(List<String> values) {
            addCriterion("establishbank in", values, "establishbank");
            return (Criteria) this;
        }

        public Criteria andEstablishbankNotIn(List<String> values) {
            addCriterion("establishbank not in", values, "establishbank");
            return (Criteria) this;
        }

        public Criteria andEstablishbankBetween(String value1, String value2) {
            addCriterion("establishbank between", value1, value2, "establishbank");
            return (Criteria) this;
        }

        public Criteria andEstablishbankNotBetween(String value1, String value2) {
            addCriterion("establishbank not between", value1, value2, "establishbank");
            return (Criteria) this;
        }

        public Criteria andEstablishnameIsNull() {
            addCriterion("establishname is null");
            return (Criteria) this;
        }

        public Criteria andEstablishnameIsNotNull() {
            addCriterion("establishname is not null");
            return (Criteria) this;
        }

        public Criteria andEstablishnameEqualTo(String value) {
            addCriterion("establishname =", value, "establishname");
            return (Criteria) this;
        }

        public Criteria andEstablishnameNotEqualTo(String value) {
            addCriterion("establishname <>", value, "establishname");
            return (Criteria) this;
        }

        public Criteria andEstablishnameGreaterThan(String value) {
            addCriterion("establishname >", value, "establishname");
            return (Criteria) this;
        }

        public Criteria andEstablishnameGreaterThanOrEqualTo(String value) {
            addCriterion("establishname >=", value, "establishname");
            return (Criteria) this;
        }

        public Criteria andEstablishnameLessThan(String value) {
            addCriterion("establishname <", value, "establishname");
            return (Criteria) this;
        }

        public Criteria andEstablishnameLessThanOrEqualTo(String value) {
            addCriterion("establishname <=", value, "establishname");
            return (Criteria) this;
        }

        public Criteria andEstablishnameLike(String value) {
            addCriterion("establishname like", value, "establishname");
            return (Criteria) this;
        }

        public Criteria andEstablishnameNotLike(String value) {
            addCriterion("establishname not like", value, "establishname");
            return (Criteria) this;
        }

        public Criteria andEstablishnameIn(List<String> values) {
            addCriterion("establishname in", values, "establishname");
            return (Criteria) this;
        }

        public Criteria andEstablishnameNotIn(List<String> values) {
            addCriterion("establishname not in", values, "establishname");
            return (Criteria) this;
        }

        public Criteria andEstablishnameBetween(String value1, String value2) {
            addCriterion("establishname between", value1, value2, "establishname");
            return (Criteria) this;
        }

        public Criteria andEstablishnameNotBetween(String value1, String value2) {
            addCriterion("establishname not between", value1, value2, "establishname");
            return (Criteria) this;
        }

        public Criteria andMini_imgPathIsNull() {
            addCriterion("mini_imgPath is null");
            return (Criteria) this;
        }

        public Criteria andMini_imgPathIsNotNull() {
            addCriterion("mini_imgPath is not null");
            return (Criteria) this;
        }

        public Criteria andMini_imgPathEqualTo(String value) {
            addCriterion("mini_imgPath =", value, "mini_imgPath");
            return (Criteria) this;
        }

        public Criteria andMini_imgPathNotEqualTo(String value) {
            addCriterion("mini_imgPath <>", value, "mini_imgPath");
            return (Criteria) this;
        }

        public Criteria andMini_imgPathGreaterThan(String value) {
            addCriterion("mini_imgPath >", value, "mini_imgPath");
            return (Criteria) this;
        }

        public Criteria andMini_imgPathGreaterThanOrEqualTo(String value) {
            addCriterion("mini_imgPath >=", value, "mini_imgPath");
            return (Criteria) this;
        }

        public Criteria andMini_imgPathLessThan(String value) {
            addCriterion("mini_imgPath <", value, "mini_imgPath");
            return (Criteria) this;
        }

        public Criteria andMini_imgPathLessThanOrEqualTo(String value) {
            addCriterion("mini_imgPath <=", value, "mini_imgPath");
            return (Criteria) this;
        }

        public Criteria andMini_imgPathLike(String value) {
            addCriterion("mini_imgPath like", value, "mini_imgPath");
            return (Criteria) this;
        }

        public Criteria andMini_imgPathNotLike(String value) {
            addCriterion("mini_imgPath not like", value, "mini_imgPath");
            return (Criteria) this;
        }

        public Criteria andMini_imgPathIn(List<String> values) {
            addCriterion("mini_imgPath in", values, "mini_imgPath");
            return (Criteria) this;
        }

        public Criteria andMini_imgPathNotIn(List<String> values) {
            addCriterion("mini_imgPath not in", values, "mini_imgPath");
            return (Criteria) this;
        }

        public Criteria andMini_imgPathBetween(String value1, String value2) {
            addCriterion("mini_imgPath between", value1, value2, "mini_imgPath");
            return (Criteria) this;
        }

        public Criteria andMini_imgPathNotBetween(String value1, String value2) {
            addCriterion("mini_imgPath not between", value1, value2, "mini_imgPath");
            return (Criteria) this;
        }

        public Criteria andIsopenIsNull() {
            addCriterion("isopen is null");
            return (Criteria) this;
        }

        public Criteria andIsopenIsNotNull() {
            addCriterion("isopen is not null");
            return (Criteria) this;
        }

        public Criteria andIsopenEqualTo(Integer value) {
            addCriterion("isopen =", value, "isopen");
            return (Criteria) this;
        }

        public Criteria andIsopenNotEqualTo(Integer value) {
            addCriterion("isopen <>", value, "isopen");
            return (Criteria) this;
        }

        public Criteria andIsopenGreaterThan(Integer value) {
            addCriterion("isopen >", value, "isopen");
            return (Criteria) this;
        }

        public Criteria andIsopenGreaterThanOrEqualTo(Integer value) {
            addCriterion("isopen >=", value, "isopen");
            return (Criteria) this;
        }

        public Criteria andIsopenLessThan(Integer value) {
            addCriterion("isopen <", value, "isopen");
            return (Criteria) this;
        }

        public Criteria andIsopenLessThanOrEqualTo(Integer value) {
            addCriterion("isopen <=", value, "isopen");
            return (Criteria) this;
        }

        public Criteria andIsopenIn(List<Integer> values) {
            addCriterion("isopen in", values, "isopen");
            return (Criteria) this;
        }

        public Criteria andIsopenNotIn(List<Integer> values) {
            addCriterion("isopen not in", values, "isopen");
            return (Criteria) this;
        }

        public Criteria andIsopenBetween(Integer value1, Integer value2) {
            addCriterion("isopen between", value1, value2, "isopen");
            return (Criteria) this;
        }

        public Criteria andIsopenNotBetween(Integer value1, Integer value2) {
            addCriterion("isopen not between", value1, value2, "isopen");
            return (Criteria) this;
        }

        public Criteria andBaseChargeIsNull() {
            addCriterion("baseCharge is null");
            return (Criteria) this;
        }

        public Criteria andBaseChargeIsNotNull() {
            addCriterion("baseCharge is not null");
            return (Criteria) this;
        }

        public Criteria andBaseChargeEqualTo(BigDecimal value) {
            addCriterion("baseCharge =", value, "baseCharge");
            return (Criteria) this;
        }

        public Criteria andBaseChargeNotEqualTo(BigDecimal value) {
            addCriterion("baseCharge <>", value, "baseCharge");
            return (Criteria) this;
        }

        public Criteria andBaseChargeGreaterThan(BigDecimal value) {
            addCriterion("baseCharge >", value, "baseCharge");
            return (Criteria) this;
        }

        public Criteria andBaseChargeGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("baseCharge >=", value, "baseCharge");
            return (Criteria) this;
        }

        public Criteria andBaseChargeLessThan(BigDecimal value) {
            addCriterion("baseCharge <", value, "baseCharge");
            return (Criteria) this;
        }

        public Criteria andBaseChargeLessThanOrEqualTo(BigDecimal value) {
            addCriterion("baseCharge <=", value, "baseCharge");
            return (Criteria) this;
        }

        public Criteria andBaseChargeIn(List<BigDecimal> values) {
            addCriterion("baseCharge in", values, "baseCharge");
            return (Criteria) this;
        }

        public Criteria andBaseChargeNotIn(List<BigDecimal> values) {
            addCriterion("baseCharge not in", values, "baseCharge");
            return (Criteria) this;
        }

        public Criteria andBaseChargeBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("baseCharge between", value1, value2, "baseCharge");
            return (Criteria) this;
        }

        public Criteria andBaseChargeNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("baseCharge not between", value1, value2, "baseCharge");
            return (Criteria) this;
        }

        public Criteria andIsChargeIsNull() {
            addCriterion("isCharge is null");
            return (Criteria) this;
        }

        public Criteria andIsChargeIsNotNull() {
            addCriterion("isCharge is not null");
            return (Criteria) this;
        }

        public Criteria andIsChargeEqualTo(Integer value) {
            addCriterion("isCharge =", value, "isCharge");
            return (Criteria) this;
        }

        public Criteria andIsChargeNotEqualTo(Integer value) {
            addCriterion("isCharge <>", value, "isCharge");
            return (Criteria) this;
        }

        public Criteria andIsChargeGreaterThan(Integer value) {
            addCriterion("isCharge >", value, "isCharge");
            return (Criteria) this;
        }

        public Criteria andIsChargeGreaterThanOrEqualTo(Integer value) {
            addCriterion("isCharge >=", value, "isCharge");
            return (Criteria) this;
        }

        public Criteria andIsChargeLessThan(Integer value) {
            addCriterion("isCharge <", value, "isCharge");
            return (Criteria) this;
        }

        public Criteria andIsChargeLessThanOrEqualTo(Integer value) {
            addCriterion("isCharge <=", value, "isCharge");
            return (Criteria) this;
        }

        public Criteria andIsChargeIn(List<Integer> values) {
            addCriterion("isCharge in", values, "isCharge");
            return (Criteria) this;
        }

        public Criteria andIsChargeNotIn(List<Integer> values) {
            addCriterion("isCharge not in", values, "isCharge");
            return (Criteria) this;
        }

        public Criteria andIsChargeBetween(Integer value1, Integer value2) {
            addCriterion("isCharge between", value1, value2, "isCharge");
            return (Criteria) this;
        }

        public Criteria andIsChargeNotBetween(Integer value1, Integer value2) {
            addCriterion("isCharge not between", value1, value2, "isCharge");
            return (Criteria) this;
        }

        public Criteria andCodeIsNull() {
            addCriterion("code is null");
            return (Criteria) this;
        }

        public Criteria andCodeIsNotNull() {
            addCriterion("code is not null");
            return (Criteria) this;
        }

        public Criteria andCodeEqualTo(String value) {
            addCriterion("code =", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeNotEqualTo(String value) {
            addCriterion("code <>", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeGreaterThan(String value) {
            addCriterion("code >", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeGreaterThanOrEqualTo(String value) {
            addCriterion("code >=", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeLessThan(String value) {
            addCriterion("code <", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeLessThanOrEqualTo(String value) {
            addCriterion("code <=", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeLike(String value) {
            addCriterion("code like", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeNotLike(String value) {
            addCriterion("code not like", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeIn(List<String> values) {
            addCriterion("code in", values, "code");
            return (Criteria) this;
        }

        public Criteria andCodeNotIn(List<String> values) {
            addCriterion("code not in", values, "code");
            return (Criteria) this;
        }

        public Criteria andCodeBetween(String value1, String value2) {
            addCriterion("code between", value1, value2, "code");
            return (Criteria) this;
        }

        public Criteria andCodeNotBetween(String value1, String value2) {
            addCriterion("code not between", value1, value2, "code");
            return (Criteria) this;
        }

        public Criteria andTypestrIsNull() {
            addCriterion("typestr is null");
            return (Criteria) this;
        }

        public Criteria andTypestrIsNotNull() {
            addCriterion("typestr is not null");
            return (Criteria) this;
        }

        public Criteria andTypestrEqualTo(String value) {
            addCriterion("typestr =", value, "typestr");
            return (Criteria) this;
        }

        public Criteria andTypestrNotEqualTo(String value) {
            addCriterion("typestr <>", value, "typestr");
            return (Criteria) this;
        }

        public Criteria andTypestrGreaterThan(String value) {
            addCriterion("typestr >", value, "typestr");
            return (Criteria) this;
        }

        public Criteria andTypestrGreaterThanOrEqualTo(String value) {
            addCriterion("typestr >=", value, "typestr");
            return (Criteria) this;
        }

        public Criteria andTypestrLessThan(String value) {
            addCriterion("typestr <", value, "typestr");
            return (Criteria) this;
        }

        public Criteria andTypestrLessThanOrEqualTo(String value) {
            addCriterion("typestr <=", value, "typestr");
            return (Criteria) this;
        }

        public Criteria andTypestrLike(String value) {
            addCriterion("typestr like", value, "typestr");
            return (Criteria) this;
        }

        public Criteria andTypestrNotLike(String value) {
            addCriterion("typestr not like", value, "typestr");
            return (Criteria) this;
        }

        public Criteria andTypestrIn(List<String> values) {
            addCriterion("typestr in", values, "typestr");
            return (Criteria) this;
        }

        public Criteria andTypestrNotIn(List<String> values) {
            addCriterion("typestr not in", values, "typestr");
            return (Criteria) this;
        }

        public Criteria andTypestrBetween(String value1, String value2) {
            addCriterion("typestr between", value1, value2, "typestr");
            return (Criteria) this;
        }

        public Criteria andTypestrNotBetween(String value1, String value2) {
            addCriterion("typestr not between", value1, value2, "typestr");
            return (Criteria) this;
        }

        public Criteria andTeamidIsNull() {
            addCriterion("teamid is null");
            return (Criteria) this;
        }

        public Criteria andTeamidIsNotNull() {
            addCriterion("teamid is not null");
            return (Criteria) this;
        }

        public Criteria andTeamidEqualTo(Integer value) {
            addCriterion("teamid =", value, "teamid");
            return (Criteria) this;
        }

        public Criteria andTeamidNotEqualTo(Integer value) {
            addCriterion("teamid <>", value, "teamid");
            return (Criteria) this;
        }

        public Criteria andTeamidGreaterThan(Integer value) {
            addCriterion("teamid >", value, "teamid");
            return (Criteria) this;
        }

        public Criteria andTeamidGreaterThanOrEqualTo(Integer value) {
            addCriterion("teamid >=", value, "teamid");
            return (Criteria) this;
        }

        public Criteria andTeamidLessThan(Integer value) {
            addCriterion("teamid <", value, "teamid");
            return (Criteria) this;
        }

        public Criteria andTeamidLessThanOrEqualTo(Integer value) {
            addCriterion("teamid <=", value, "teamid");
            return (Criteria) this;
        }

        public Criteria andTeamidIn(List<Integer> values) {
            addCriterion("teamid in", values, "teamid");
            return (Criteria) this;
        }

        public Criteria andTeamidNotIn(List<Integer> values) {
            addCriterion("teamid not in", values, "teamid");
            return (Criteria) this;
        }

        public Criteria andTeamidBetween(Integer value1, Integer value2) {
            addCriterion("teamid between", value1, value2, "teamid");
            return (Criteria) this;
        }

        public Criteria andTeamidNotBetween(Integer value1, Integer value2) {
            addCriterion("teamid not between", value1, value2, "teamid");
            return (Criteria) this;
        }

        public Criteria andTeamnameIsNull() {
            addCriterion("teamname is null");
            return (Criteria) this;
        }

        public Criteria andTeamnameIsNotNull() {
            addCriterion("teamname is not null");
            return (Criteria) this;
        }

        public Criteria andTeamnameEqualTo(String value) {
            addCriterion("teamname =", value, "teamname");
            return (Criteria) this;
        }

        public Criteria andTeamnameNotEqualTo(String value) {
            addCriterion("teamname <>", value, "teamname");
            return (Criteria) this;
        }

        public Criteria andTeamnameGreaterThan(String value) {
            addCriterion("teamname >", value, "teamname");
            return (Criteria) this;
        }

        public Criteria andTeamnameGreaterThanOrEqualTo(String value) {
            addCriterion("teamname >=", value, "teamname");
            return (Criteria) this;
        }

        public Criteria andTeamnameLessThan(String value) {
            addCriterion("teamname <", value, "teamname");
            return (Criteria) this;
        }

        public Criteria andTeamnameLessThanOrEqualTo(String value) {
            addCriterion("teamname <=", value, "teamname");
            return (Criteria) this;
        }

        public Criteria andTeamnameLike(String value) {
            addCriterion("teamname like", value, "teamname");
            return (Criteria) this;
        }

        public Criteria andTeamnameNotLike(String value) {
            addCriterion("teamname not like", value, "teamname");
            return (Criteria) this;
        }

        public Criteria andTeamnameIn(List<String> values) {
            addCriterion("teamname in", values, "teamname");
            return (Criteria) this;
        }

        public Criteria andTeamnameNotIn(List<String> values) {
            addCriterion("teamname not in", values, "teamname");
            return (Criteria) this;
        }

        public Criteria andTeamnameBetween(String value1, String value2) {
            addCriterion("teamname between", value1, value2, "teamname");
            return (Criteria) this;
        }

        public Criteria andTeamnameNotBetween(String value1, String value2) {
            addCriterion("teamname not between", value1, value2, "teamname");
            return (Criteria) this;
        }

        public Criteria andVisitnumIsNull() {
            addCriterion("visitnum is null");
            return (Criteria) this;
        }

        public Criteria andVisitnumIsNotNull() {
            addCriterion("visitnum is not null");
            return (Criteria) this;
        }

        public Criteria andVisitnumEqualTo(Integer value) {
            addCriterion("visitnum =", value, "visitnum");
            return (Criteria) this;
        }

        public Criteria andVisitnumNotEqualTo(Integer value) {
            addCriterion("visitnum <>", value, "visitnum");
            return (Criteria) this;
        }

        public Criteria andVisitnumGreaterThan(Integer value) {
            addCriterion("visitnum >", value, "visitnum");
            return (Criteria) this;
        }

        public Criteria andVisitnumGreaterThanOrEqualTo(Integer value) {
            addCriterion("visitnum >=", value, "visitnum");
            return (Criteria) this;
        }

        public Criteria andVisitnumLessThan(Integer value) {
            addCriterion("visitnum <", value, "visitnum");
            return (Criteria) this;
        }

        public Criteria andVisitnumLessThanOrEqualTo(Integer value) {
            addCriterion("visitnum <=", value, "visitnum");
            return (Criteria) this;
        }

        public Criteria andVisitnumIn(List<Integer> values) {
            addCriterion("visitnum in", values, "visitnum");
            return (Criteria) this;
        }

        public Criteria andVisitnumNotIn(List<Integer> values) {
            addCriterion("visitnum not in", values, "visitnum");
            return (Criteria) this;
        }

        public Criteria andVisitnumBetween(Integer value1, Integer value2) {
            addCriterion("visitnum between", value1, value2, "visitnum");
            return (Criteria) this;
        }

        public Criteria andVisitnumNotBetween(Integer value1, Integer value2) {
            addCriterion("visitnum not between", value1, value2, "visitnum");
            return (Criteria) this;
        }

        public Criteria andIssubsidyIsNull() {
            addCriterion("issubsidy is null");
            return (Criteria) this;
        }

        public Criteria andIssubsidyIsNotNull() {
            addCriterion("issubsidy is not null");
            return (Criteria) this;
        }

        public Criteria andIssubsidyEqualTo(Integer value) {
            addCriterion("issubsidy =", value, "issubsidy");
            return (Criteria) this;
        }

        public Criteria andIssubsidyNotEqualTo(Integer value) {
            addCriterion("issubsidy <>", value, "issubsidy");
            return (Criteria) this;
        }

        public Criteria andIssubsidyGreaterThan(Integer value) {
            addCriterion("issubsidy >", value, "issubsidy");
            return (Criteria) this;
        }

        public Criteria andIssubsidyGreaterThanOrEqualTo(Integer value) {
            addCriterion("issubsidy >=", value, "issubsidy");
            return (Criteria) this;
        }

        public Criteria andIssubsidyLessThan(Integer value) {
            addCriterion("issubsidy <", value, "issubsidy");
            return (Criteria) this;
        }

        public Criteria andIssubsidyLessThanOrEqualTo(Integer value) {
            addCriterion("issubsidy <=", value, "issubsidy");
            return (Criteria) this;
        }

        public Criteria andIssubsidyIn(List<Integer> values) {
            addCriterion("issubsidy in", values, "issubsidy");
            return (Criteria) this;
        }

        public Criteria andIssubsidyNotIn(List<Integer> values) {
            addCriterion("issubsidy not in", values, "issubsidy");
            return (Criteria) this;
        }

        public Criteria andIssubsidyBetween(Integer value1, Integer value2) {
            addCriterion("issubsidy between", value1, value2, "issubsidy");
            return (Criteria) this;
        }

        public Criteria andIssubsidyNotBetween(Integer value1, Integer value2) {
            addCriterion("issubsidy not between", value1, value2, "issubsidy");
            return (Criteria) this;
        }

        public Criteria andSubsidyIsNull() {
            addCriterion("subsidy is null");
            return (Criteria) this;
        }

        public Criteria andSubsidyIsNotNull() {
            addCriterion("subsidy is not null");
            return (Criteria) this;
        }

        public Criteria andSubsidyEqualTo(BigDecimal value) {
            addCriterion("subsidy =", value, "subsidy");
            return (Criteria) this;
        }

        public Criteria andSubsidyNotEqualTo(BigDecimal value) {
            addCriterion("subsidy <>", value, "subsidy");
            return (Criteria) this;
        }

        public Criteria andSubsidyGreaterThan(BigDecimal value) {
            addCriterion("subsidy >", value, "subsidy");
            return (Criteria) this;
        }

        public Criteria andSubsidyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("subsidy >=", value, "subsidy");
            return (Criteria) this;
        }

        public Criteria andSubsidyLessThan(BigDecimal value) {
            addCriterion("subsidy <", value, "subsidy");
            return (Criteria) this;
        }

        public Criteria andSubsidyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("subsidy <=", value, "subsidy");
            return (Criteria) this;
        }

        public Criteria andSubsidyIn(List<BigDecimal> values) {
            addCriterion("subsidy in", values, "subsidy");
            return (Criteria) this;
        }

        public Criteria andSubsidyNotIn(List<BigDecimal> values) {
            addCriterion("subsidy not in", values, "subsidy");
            return (Criteria) this;
        }

        public Criteria andSubsidyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("subsidy between", value1, value2, "subsidy");
            return (Criteria) this;
        }

        public Criteria andSubsidyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("subsidy not between", value1, value2, "subsidy");
            return (Criteria) this;
        }

        public Criteria andGoldBusinessIsNull() {
            addCriterion("goldBusiness is null");
            return (Criteria) this;
        }

        public Criteria andGoldBusinessIsNotNull() {
            addCriterion("goldBusiness is not null");
            return (Criteria) this;
        }

        public Criteria andGoldBusinessEqualTo(Boolean value) {
            addCriterion("goldBusiness =", value, "goldBusiness");
            return (Criteria) this;
        }

        public Criteria andGoldBusinessNotEqualTo(Boolean value) {
            addCriterion("goldBusiness <>", value, "goldBusiness");
            return (Criteria) this;
        }

        public Criteria andGoldBusinessGreaterThan(Boolean value) {
            addCriterion("goldBusiness >", value, "goldBusiness");
            return (Criteria) this;
        }

        public Criteria andGoldBusinessGreaterThanOrEqualTo(Boolean value) {
            addCriterion("goldBusiness >=", value, "goldBusiness");
            return (Criteria) this;
        }

        public Criteria andGoldBusinessLessThan(Boolean value) {
            addCriterion("goldBusiness <", value, "goldBusiness");
            return (Criteria) this;
        }

        public Criteria andGoldBusinessLessThanOrEqualTo(Boolean value) {
            addCriterion("goldBusiness <=", value, "goldBusiness");
            return (Criteria) this;
        }

        public Criteria andGoldBusinessIn(List<Boolean> values) {
            addCriterion("goldBusiness in", values, "goldBusiness");
            return (Criteria) this;
        }

        public Criteria andGoldBusinessNotIn(List<Boolean> values) {
            addCriterion("goldBusiness not in", values, "goldBusiness");
            return (Criteria) this;
        }

        public Criteria andGoldBusinessBetween(Boolean value1, Boolean value2) {
            addCriterion("goldBusiness between", value1, value2, "goldBusiness");
            return (Criteria) this;
        }

        public Criteria andGoldBusinessNotBetween(Boolean value1, Boolean value2) {
            addCriterion("goldBusiness not between", value1, value2, "goldBusiness");
            return (Criteria) this;
        }

        public Criteria andAutoPrintIsNull() {
            addCriterion("autoPrint is null");
            return (Criteria) this;
        }

        public Criteria andAutoPrintIsNotNull() {
            addCriterion("autoPrint is not null");
            return (Criteria) this;
        }

        public Criteria andAutoPrintEqualTo(Boolean value) {
            addCriterion("autoPrint =", value, "autoPrint");
            return (Criteria) this;
        }

        public Criteria andAutoPrintNotEqualTo(Boolean value) {
            addCriterion("autoPrint <>", value, "autoPrint");
            return (Criteria) this;
        }

        public Criteria andAutoPrintGreaterThan(Boolean value) {
            addCriterion("autoPrint >", value, "autoPrint");
            return (Criteria) this;
        }

        public Criteria andAutoPrintGreaterThanOrEqualTo(Boolean value) {
            addCriterion("autoPrint >=", value, "autoPrint");
            return (Criteria) this;
        }

        public Criteria andAutoPrintLessThan(Boolean value) {
            addCriterion("autoPrint <", value, "autoPrint");
            return (Criteria) this;
        }

        public Criteria andAutoPrintLessThanOrEqualTo(Boolean value) {
            addCriterion("autoPrint <=", value, "autoPrint");
            return (Criteria) this;
        }

        public Criteria andAutoPrintIn(List<Boolean> values) {
            addCriterion("autoPrint in", values, "autoPrint");
            return (Criteria) this;
        }

        public Criteria andAutoPrintNotIn(List<Boolean> values) {
            addCriterion("autoPrint not in", values, "autoPrint");
            return (Criteria) this;
        }

        public Criteria andAutoPrintBetween(Boolean value1, Boolean value2) {
            addCriterion("autoPrint between", value1, value2, "autoPrint");
            return (Criteria) this;
        }

        public Criteria andAutoPrintNotBetween(Boolean value1, Boolean value2) {
            addCriterion("autoPrint not between", value1, value2, "autoPrint");
            return (Criteria) this;
        }

        public Criteria andAutoTakingIsNull() {
            addCriterion("autoTaking is null");
            return (Criteria) this;
        }

        public Criteria andAutoTakingIsNotNull() {
            addCriterion("autoTaking is not null");
            return (Criteria) this;
        }

        public Criteria andAutoTakingEqualTo(Boolean value) {
            addCriterion("autoTaking =", value, "autoTaking");
            return (Criteria) this;
        }

        public Criteria andAutoTakingNotEqualTo(Boolean value) {
            addCriterion("autoTaking <>", value, "autoTaking");
            return (Criteria) this;
        }

        public Criteria andAutoTakingGreaterThan(Boolean value) {
            addCriterion("autoTaking >", value, "autoTaking");
            return (Criteria) this;
        }

        public Criteria andAutoTakingGreaterThanOrEqualTo(Boolean value) {
            addCriterion("autoTaking >=", value, "autoTaking");
            return (Criteria) this;
        }

        public Criteria andAutoTakingLessThan(Boolean value) {
            addCriterion("autoTaking <", value, "autoTaking");
            return (Criteria) this;
        }

        public Criteria andAutoTakingLessThanOrEqualTo(Boolean value) {
            addCriterion("autoTaking <=", value, "autoTaking");
            return (Criteria) this;
        }

        public Criteria andAutoTakingIn(List<Boolean> values) {
            addCriterion("autoTaking in", values, "autoTaking");
            return (Criteria) this;
        }

        public Criteria andAutoTakingNotIn(List<Boolean> values) {
            addCriterion("autoTaking not in", values, "autoTaking");
            return (Criteria) this;
        }

        public Criteria andAutoTakingBetween(Boolean value1, Boolean value2) {
            addCriterion("autoTaking between", value1, value2, "autoTaking");
            return (Criteria) this;
        }

        public Criteria andAutoTakingNotBetween(Boolean value1, Boolean value2) {
            addCriterion("autoTaking not between", value1, value2, "autoTaking");
            return (Criteria) this;
        }

        public Criteria andPriorityIsNull() {
            addCriterion("priority is null");
            return (Criteria) this;
        }

        public Criteria andPriorityIsNotNull() {
            addCriterion("priority is not null");
            return (Criteria) this;
        }

        public Criteria andPriorityEqualTo(Boolean value) {
            addCriterion("priority =", value, "priority");
            return (Criteria) this;
        }

        public Criteria andPriorityNotEqualTo(Boolean value) {
            addCriterion("priority <>", value, "priority");
            return (Criteria) this;
        }

        public Criteria andPriorityGreaterThan(Boolean value) {
            addCriterion("priority >", value, "priority");
            return (Criteria) this;
        }

        public Criteria andPriorityGreaterThanOrEqualTo(Boolean value) {
            addCriterion("priority >=", value, "priority");
            return (Criteria) this;
        }

        public Criteria andPriorityLessThan(Boolean value) {
            addCriterion("priority <", value, "priority");
            return (Criteria) this;
        }

        public Criteria andPriorityLessThanOrEqualTo(Boolean value) {
            addCriterion("priority <=", value, "priority");
            return (Criteria) this;
        }

        public Criteria andPriorityIn(List<Boolean> values) {
            addCriterion("priority in", values, "priority");
            return (Criteria) this;
        }

        public Criteria andPriorityNotIn(List<Boolean> values) {
            addCriterion("priority not in", values, "priority");
            return (Criteria) this;
        }

        public Criteria andPriorityBetween(Boolean value1, Boolean value2) {
            addCriterion("priority between", value1, value2, "priority");
            return (Criteria) this;
        }

        public Criteria andPriorityNotBetween(Boolean value1, Boolean value2) {
            addCriterion("priority not between", value1, value2, "priority");
            return (Criteria) this;
        }

        public Criteria andWm_mobileIsNull() {
            addCriterion("wm_mobile is null");
            return (Criteria) this;
        }

        public Criteria andWm_mobileIsNotNull() {
            addCriterion("wm_mobile is not null");
            return (Criteria) this;
        }

        public Criteria andWm_mobileEqualTo(String value) {
            addCriterion("wm_mobile =", value, "wm_mobile");
            return (Criteria) this;
        }

        public Criteria andWm_mobileNotEqualTo(String value) {
            addCriterion("wm_mobile <>", value, "wm_mobile");
            return (Criteria) this;
        }

        public Criteria andWm_mobileGreaterThan(String value) {
            addCriterion("wm_mobile >", value, "wm_mobile");
            return (Criteria) this;
        }

        public Criteria andWm_mobileGreaterThanOrEqualTo(String value) {
            addCriterion("wm_mobile >=", value, "wm_mobile");
            return (Criteria) this;
        }

        public Criteria andWm_mobileLessThan(String value) {
            addCriterion("wm_mobile <", value, "wm_mobile");
            return (Criteria) this;
        }

        public Criteria andWm_mobileLessThanOrEqualTo(String value) {
            addCriterion("wm_mobile <=", value, "wm_mobile");
            return (Criteria) this;
        }

        public Criteria andWm_mobileLike(String value) {
            addCriterion("wm_mobile like", value, "wm_mobile");
            return (Criteria) this;
        }

        public Criteria andWm_mobileNotLike(String value) {
            addCriterion("wm_mobile not like", value, "wm_mobile");
            return (Criteria) this;
        }

        public Criteria andWm_mobileIn(List<String> values) {
            addCriterion("wm_mobile in", values, "wm_mobile");
            return (Criteria) this;
        }

        public Criteria andWm_mobileNotIn(List<String> values) {
            addCriterion("wm_mobile not in", values, "wm_mobile");
            return (Criteria) this;
        }

        public Criteria andWm_mobileBetween(String value1, String value2) {
            addCriterion("wm_mobile between", value1, value2, "wm_mobile");
            return (Criteria) this;
        }

        public Criteria andWm_mobileNotBetween(String value1, String value2) {
            addCriterion("wm_mobile not between", value1, value2, "wm_mobile");
            return (Criteria) this;
        }

        public Criteria andFace_imageIsNull() {
            addCriterion("face_image is null");
            return (Criteria) this;
        }

        public Criteria andFace_imageIsNotNull() {
            addCriterion("face_image is not null");
            return (Criteria) this;
        }

        public Criteria andFace_imageEqualTo(String value) {
            addCriterion("face_image =", value, "face_image");
            return (Criteria) this;
        }

        public Criteria andFace_imageNotEqualTo(String value) {
            addCriterion("face_image <>", value, "face_image");
            return (Criteria) this;
        }

        public Criteria andFace_imageGreaterThan(String value) {
            addCriterion("face_image >", value, "face_image");
            return (Criteria) this;
        }

        public Criteria andFace_imageGreaterThanOrEqualTo(String value) {
            addCriterion("face_image >=", value, "face_image");
            return (Criteria) this;
        }

        public Criteria andFace_imageLessThan(String value) {
            addCriterion("face_image <", value, "face_image");
            return (Criteria) this;
        }

        public Criteria andFace_imageLessThanOrEqualTo(String value) {
            addCriterion("face_image <=", value, "face_image");
            return (Criteria) this;
        }

        public Criteria andFace_imageLike(String value) {
            addCriterion("face_image like", value, "face_image");
            return (Criteria) this;
        }

        public Criteria andFace_imageNotLike(String value) {
            addCriterion("face_image not like", value, "face_image");
            return (Criteria) this;
        }

        public Criteria andFace_imageIn(List<String> values) {
            addCriterion("face_image in", values, "face_image");
            return (Criteria) this;
        }

        public Criteria andFace_imageNotIn(List<String> values) {
            addCriterion("face_image not in", values, "face_image");
            return (Criteria) this;
        }

        public Criteria andFace_imageBetween(String value1, String value2) {
            addCriterion("face_image between", value1, value2, "face_image");
            return (Criteria) this;
        }

        public Criteria andFace_imageNotBetween(String value1, String value2) {
            addCriterion("face_image not between", value1, value2, "face_image");
            return (Criteria) this;
        }

        public Criteria andInner_imageIsNull() {
            addCriterion("inner_image is null");
            return (Criteria) this;
        }

        public Criteria andInner_imageIsNotNull() {
            addCriterion("inner_image is not null");
            return (Criteria) this;
        }

        public Criteria andInner_imageEqualTo(String value) {
            addCriterion("inner_image =", value, "inner_image");
            return (Criteria) this;
        }

        public Criteria andInner_imageNotEqualTo(String value) {
            addCriterion("inner_image <>", value, "inner_image");
            return (Criteria) this;
        }

        public Criteria andInner_imageGreaterThan(String value) {
            addCriterion("inner_image >", value, "inner_image");
            return (Criteria) this;
        }

        public Criteria andInner_imageGreaterThanOrEqualTo(String value) {
            addCriterion("inner_image >=", value, "inner_image");
            return (Criteria) this;
        }

        public Criteria andInner_imageLessThan(String value) {
            addCriterion("inner_image <", value, "inner_image");
            return (Criteria) this;
        }

        public Criteria andInner_imageLessThanOrEqualTo(String value) {
            addCriterion("inner_image <=", value, "inner_image");
            return (Criteria) this;
        }

        public Criteria andInner_imageLike(String value) {
            addCriterion("inner_image like", value, "inner_image");
            return (Criteria) this;
        }

        public Criteria andInner_imageNotLike(String value) {
            addCriterion("inner_image not like", value, "inner_image");
            return (Criteria) this;
        }

        public Criteria andInner_imageIn(List<String> values) {
            addCriterion("inner_image in", values, "inner_image");
            return (Criteria) this;
        }

        public Criteria andInner_imageNotIn(List<String> values) {
            addCriterion("inner_image not in", values, "inner_image");
            return (Criteria) this;
        }

        public Criteria andInner_imageBetween(String value1, String value2) {
            addCriterion("inner_image between", value1, value2, "inner_image");
            return (Criteria) this;
        }

        public Criteria andInner_imageNotBetween(String value1, String value2) {
            addCriterion("inner_image not between", value1, value2, "inner_image");
            return (Criteria) this;
        }

        public Criteria andScoreIsNull() {
            addCriterion("score is null");
            return (Criteria) this;
        }

        public Criteria andScoreIsNotNull() {
            addCriterion("score is not null");
            return (Criteria) this;
        }

        public Criteria andScoreEqualTo(Integer value) {
            addCriterion("score =", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotEqualTo(Integer value) {
            addCriterion("score <>", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreGreaterThan(Integer value) {
            addCriterion("score >", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreGreaterThanOrEqualTo(Integer value) {
            addCriterion("score >=", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreLessThan(Integer value) {
            addCriterion("score <", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreLessThanOrEqualTo(Integer value) {
            addCriterion("score <=", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreIn(List<Integer> values) {
            addCriterion("score in", values, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotIn(List<Integer> values) {
            addCriterion("score not in", values, "score");
            return (Criteria) this;
        }

        public Criteria andScoreBetween(Integer value1, Integer value2) {
            addCriterion("score between", value1, value2, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotBetween(Integer value1, Integer value2) {
            addCriterion("score not between", value1, value2, "score");
            return (Criteria) this;
        }

        public Criteria andMainCertIsNull() {
            addCriterion("mainCert is null");
            return (Criteria) this;
        }

        public Criteria andMainCertIsNotNull() {
            addCriterion("mainCert is not null");
            return (Criteria) this;
        }

        public Criteria andMainCertEqualTo(Integer value) {
            addCriterion("mainCert =", value, "mainCert");
            return (Criteria) this;
        }

        public Criteria andMainCertNotEqualTo(Integer value) {
            addCriterion("mainCert <>", value, "mainCert");
            return (Criteria) this;
        }

        public Criteria andMainCertGreaterThan(Integer value) {
            addCriterion("mainCert >", value, "mainCert");
            return (Criteria) this;
        }

        public Criteria andMainCertGreaterThanOrEqualTo(Integer value) {
            addCriterion("mainCert >=", value, "mainCert");
            return (Criteria) this;
        }

        public Criteria andMainCertLessThan(Integer value) {
            addCriterion("mainCert <", value, "mainCert");
            return (Criteria) this;
        }

        public Criteria andMainCertLessThanOrEqualTo(Integer value) {
            addCriterion("mainCert <=", value, "mainCert");
            return (Criteria) this;
        }

        public Criteria andMainCertIn(List<Integer> values) {
            addCriterion("mainCert in", values, "mainCert");
            return (Criteria) this;
        }

        public Criteria andMainCertNotIn(List<Integer> values) {
            addCriterion("mainCert not in", values, "mainCert");
            return (Criteria) this;
        }

        public Criteria andMainCertBetween(Integer value1, Integer value2) {
            addCriterion("mainCert between", value1, value2, "mainCert");
            return (Criteria) this;
        }

        public Criteria andMainCertNotBetween(Integer value1, Integer value2) {
            addCriterion("mainCert not between", value1, value2, "mainCert");
            return (Criteria) this;
        }

        public Criteria andMainImgIsNull() {
            addCriterion("mainImg is null");
            return (Criteria) this;
        }

        public Criteria andMainImgIsNotNull() {
            addCriterion("mainImg is not null");
            return (Criteria) this;
        }

        public Criteria andMainImgEqualTo(String value) {
            addCriterion("mainImg =", value, "mainImg");
            return (Criteria) this;
        }

        public Criteria andMainImgNotEqualTo(String value) {
            addCriterion("mainImg <>", value, "mainImg");
            return (Criteria) this;
        }

        public Criteria andMainImgGreaterThan(String value) {
            addCriterion("mainImg >", value, "mainImg");
            return (Criteria) this;
        }

        public Criteria andMainImgGreaterThanOrEqualTo(String value) {
            addCriterion("mainImg >=", value, "mainImg");
            return (Criteria) this;
        }

        public Criteria andMainImgLessThan(String value) {
            addCriterion("mainImg <", value, "mainImg");
            return (Criteria) this;
        }

        public Criteria andMainImgLessThanOrEqualTo(String value) {
            addCriterion("mainImg <=", value, "mainImg");
            return (Criteria) this;
        }

        public Criteria andMainImgLike(String value) {
            addCriterion("mainImg like", value, "mainImg");
            return (Criteria) this;
        }

        public Criteria andMainImgNotLike(String value) {
            addCriterion("mainImg not like", value, "mainImg");
            return (Criteria) this;
        }

        public Criteria andMainImgIn(List<String> values) {
            addCriterion("mainImg in", values, "mainImg");
            return (Criteria) this;
        }

        public Criteria andMainImgNotIn(List<String> values) {
            addCriterion("mainImg not in", values, "mainImg");
            return (Criteria) this;
        }

        public Criteria andMainImgBetween(String value1, String value2) {
            addCriterion("mainImg between", value1, value2, "mainImg");
            return (Criteria) this;
        }

        public Criteria andMainImgNotBetween(String value1, String value2) {
            addCriterion("mainImg not between", value1, value2, "mainImg");
            return (Criteria) this;
        }

        public Criteria andMainOrgNameIsNull() {
            addCriterion("mainOrgName is null");
            return (Criteria) this;
        }

        public Criteria andMainOrgNameIsNotNull() {
            addCriterion("mainOrgName is not null");
            return (Criteria) this;
        }

        public Criteria andMainOrgNameEqualTo(String value) {
            addCriterion("mainOrgName =", value, "mainOrgName");
            return (Criteria) this;
        }

        public Criteria andMainOrgNameNotEqualTo(String value) {
            addCriterion("mainOrgName <>", value, "mainOrgName");
            return (Criteria) this;
        }

        public Criteria andMainOrgNameGreaterThan(String value) {
            addCriterion("mainOrgName >", value, "mainOrgName");
            return (Criteria) this;
        }

        public Criteria andMainOrgNameGreaterThanOrEqualTo(String value) {
            addCriterion("mainOrgName >=", value, "mainOrgName");
            return (Criteria) this;
        }

        public Criteria andMainOrgNameLessThan(String value) {
            addCriterion("mainOrgName <", value, "mainOrgName");
            return (Criteria) this;
        }

        public Criteria andMainOrgNameLessThanOrEqualTo(String value) {
            addCriterion("mainOrgName <=", value, "mainOrgName");
            return (Criteria) this;
        }

        public Criteria andMainOrgNameLike(String value) {
            addCriterion("mainOrgName like", value, "mainOrgName");
            return (Criteria) this;
        }

        public Criteria andMainOrgNameNotLike(String value) {
            addCriterion("mainOrgName not like", value, "mainOrgName");
            return (Criteria) this;
        }

        public Criteria andMainOrgNameIn(List<String> values) {
            addCriterion("mainOrgName in", values, "mainOrgName");
            return (Criteria) this;
        }

        public Criteria andMainOrgNameNotIn(List<String> values) {
            addCriterion("mainOrgName not in", values, "mainOrgName");
            return (Criteria) this;
        }

        public Criteria andMainOrgNameBetween(String value1, String value2) {
            addCriterion("mainOrgName between", value1, value2, "mainOrgName");
            return (Criteria) this;
        }

        public Criteria andMainOrgNameNotBetween(String value1, String value2) {
            addCriterion("mainOrgName not between", value1, value2, "mainOrgName");
            return (Criteria) this;
        }

        public Criteria andMainLeaderIsNull() {
            addCriterion("mainLeader is null");
            return (Criteria) this;
        }

        public Criteria andMainLeaderIsNotNull() {
            addCriterion("mainLeader is not null");
            return (Criteria) this;
        }

        public Criteria andMainLeaderEqualTo(String value) {
            addCriterion("mainLeader =", value, "mainLeader");
            return (Criteria) this;
        }

        public Criteria andMainLeaderNotEqualTo(String value) {
            addCriterion("mainLeader <>", value, "mainLeader");
            return (Criteria) this;
        }

        public Criteria andMainLeaderGreaterThan(String value) {
            addCriterion("mainLeader >", value, "mainLeader");
            return (Criteria) this;
        }

        public Criteria andMainLeaderGreaterThanOrEqualTo(String value) {
            addCriterion("mainLeader >=", value, "mainLeader");
            return (Criteria) this;
        }

        public Criteria andMainLeaderLessThan(String value) {
            addCriterion("mainLeader <", value, "mainLeader");
            return (Criteria) this;
        }

        public Criteria andMainLeaderLessThanOrEqualTo(String value) {
            addCriterion("mainLeader <=", value, "mainLeader");
            return (Criteria) this;
        }

        public Criteria andMainLeaderLike(String value) {
            addCriterion("mainLeader like", value, "mainLeader");
            return (Criteria) this;
        }

        public Criteria andMainLeaderNotLike(String value) {
            addCriterion("mainLeader not like", value, "mainLeader");
            return (Criteria) this;
        }

        public Criteria andMainLeaderIn(List<String> values) {
            addCriterion("mainLeader in", values, "mainLeader");
            return (Criteria) this;
        }

        public Criteria andMainLeaderNotIn(List<String> values) {
            addCriterion("mainLeader not in", values, "mainLeader");
            return (Criteria) this;
        }

        public Criteria andMainLeaderBetween(String value1, String value2) {
            addCriterion("mainLeader between", value1, value2, "mainLeader");
            return (Criteria) this;
        }

        public Criteria andMainLeaderNotBetween(String value1, String value2) {
            addCriterion("mainLeader not between", value1, value2, "mainLeader");
            return (Criteria) this;
        }

        public Criteria andRegisterNumIsNull() {
            addCriterion("registerNum is null");
            return (Criteria) this;
        }

        public Criteria andRegisterNumIsNotNull() {
            addCriterion("registerNum is not null");
            return (Criteria) this;
        }

        public Criteria andRegisterNumEqualTo(String value) {
            addCriterion("registerNum =", value, "registerNum");
            return (Criteria) this;
        }

        public Criteria andRegisterNumNotEqualTo(String value) {
            addCriterion("registerNum <>", value, "registerNum");
            return (Criteria) this;
        }

        public Criteria andRegisterNumGreaterThan(String value) {
            addCriterion("registerNum >", value, "registerNum");
            return (Criteria) this;
        }

        public Criteria andRegisterNumGreaterThanOrEqualTo(String value) {
            addCriterion("registerNum >=", value, "registerNum");
            return (Criteria) this;
        }

        public Criteria andRegisterNumLessThan(String value) {
            addCriterion("registerNum <", value, "registerNum");
            return (Criteria) this;
        }

        public Criteria andRegisterNumLessThanOrEqualTo(String value) {
            addCriterion("registerNum <=", value, "registerNum");
            return (Criteria) this;
        }

        public Criteria andRegisterNumLike(String value) {
            addCriterion("registerNum like", value, "registerNum");
            return (Criteria) this;
        }

        public Criteria andRegisterNumNotLike(String value) {
            addCriterion("registerNum not like", value, "registerNum");
            return (Criteria) this;
        }

        public Criteria andRegisterNumIn(List<String> values) {
            addCriterion("registerNum in", values, "registerNum");
            return (Criteria) this;
        }

        public Criteria andRegisterNumNotIn(List<String> values) {
            addCriterion("registerNum not in", values, "registerNum");
            return (Criteria) this;
        }

        public Criteria andRegisterNumBetween(String value1, String value2) {
            addCriterion("registerNum between", value1, value2, "registerNum");
            return (Criteria) this;
        }

        public Criteria andRegisterNumNotBetween(String value1, String value2) {
            addCriterion("registerNum not between", value1, value2, "registerNum");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressIsNull() {
            addCriterion("registerAddress is null");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressIsNotNull() {
            addCriterion("registerAddress is not null");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressEqualTo(String value) {
            addCriterion("registerAddress =", value, "registerAddress");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressNotEqualTo(String value) {
            addCriterion("registerAddress <>", value, "registerAddress");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressGreaterThan(String value) {
            addCriterion("registerAddress >", value, "registerAddress");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressGreaterThanOrEqualTo(String value) {
            addCriterion("registerAddress >=", value, "registerAddress");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressLessThan(String value) {
            addCriterion("registerAddress <", value, "registerAddress");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressLessThanOrEqualTo(String value) {
            addCriterion("registerAddress <=", value, "registerAddress");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressLike(String value) {
            addCriterion("registerAddress like", value, "registerAddress");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressNotLike(String value) {
            addCriterion("registerAddress not like", value, "registerAddress");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressIn(List<String> values) {
            addCriterion("registerAddress in", values, "registerAddress");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressNotIn(List<String> values) {
            addCriterion("registerAddress not in", values, "registerAddress");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressBetween(String value1, String value2) {
            addCriterion("registerAddress between", value1, value2, "registerAddress");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressNotBetween(String value1, String value2) {
            addCriterion("registerAddress not between", value1, value2, "registerAddress");
            return (Criteria) this;
        }

        public Criteria andMainExpirationDateIsNull() {
            addCriterion("mainExpirationDate is null");
            return (Criteria) this;
        }

        public Criteria andMainExpirationDateIsNotNull() {
            addCriterion("mainExpirationDate is not null");
            return (Criteria) this;
        }

        public Criteria andMainExpirationDateEqualTo(Date value) {
            addCriterion("mainExpirationDate =", value, "mainExpirationDate");
            return (Criteria) this;
        }

        public Criteria andMainExpirationDateNotEqualTo(Date value) {
            addCriterion("mainExpirationDate <>", value, "mainExpirationDate");
            return (Criteria) this;
        }

        public Criteria andMainExpirationDateGreaterThan(Date value) {
            addCriterion("mainExpirationDate >", value, "mainExpirationDate");
            return (Criteria) this;
        }

        public Criteria andMainExpirationDateGreaterThanOrEqualTo(Date value) {
            addCriterion("mainExpirationDate >=", value, "mainExpirationDate");
            return (Criteria) this;
        }

        public Criteria andMainExpirationDateLessThan(Date value) {
            addCriterion("mainExpirationDate <", value, "mainExpirationDate");
            return (Criteria) this;
        }

        public Criteria andMainExpirationDateLessThanOrEqualTo(Date value) {
            addCriterion("mainExpirationDate <=", value, "mainExpirationDate");
            return (Criteria) this;
        }

        public Criteria andMainExpirationDateIn(List<Date> values) {
            addCriterion("mainExpirationDate in", values, "mainExpirationDate");
            return (Criteria) this;
        }

        public Criteria andMainExpirationDateNotIn(List<Date> values) {
            addCriterion("mainExpirationDate not in", values, "mainExpirationDate");
            return (Criteria) this;
        }

        public Criteria andMainExpirationDateBetween(Date value1, Date value2) {
            addCriterion("mainExpirationDate between", value1, value2, "mainExpirationDate");
            return (Criteria) this;
        }

        public Criteria andMainExpirationDateNotBetween(Date value1, Date value2) {
            addCriterion("mainExpirationDate not between", value1, value2, "mainExpirationDate");
            return (Criteria) this;
        }

        public Criteria andTradeCertIsNull() {
            addCriterion("tradeCert is null");
            return (Criteria) this;
        }

        public Criteria andTradeCertIsNotNull() {
            addCriterion("tradeCert is not null");
            return (Criteria) this;
        }

        public Criteria andTradeCertEqualTo(Integer value) {
            addCriterion("tradeCert =", value, "tradeCert");
            return (Criteria) this;
        }

        public Criteria andTradeCertNotEqualTo(Integer value) {
            addCriterion("tradeCert <>", value, "tradeCert");
            return (Criteria) this;
        }

        public Criteria andTradeCertGreaterThan(Integer value) {
            addCriterion("tradeCert >", value, "tradeCert");
            return (Criteria) this;
        }

        public Criteria andTradeCertGreaterThanOrEqualTo(Integer value) {
            addCriterion("tradeCert >=", value, "tradeCert");
            return (Criteria) this;
        }

        public Criteria andTradeCertLessThan(Integer value) {
            addCriterion("tradeCert <", value, "tradeCert");
            return (Criteria) this;
        }

        public Criteria andTradeCertLessThanOrEqualTo(Integer value) {
            addCriterion("tradeCert <=", value, "tradeCert");
            return (Criteria) this;
        }

        public Criteria andTradeCertIn(List<Integer> values) {
            addCriterion("tradeCert in", values, "tradeCert");
            return (Criteria) this;
        }

        public Criteria andTradeCertNotIn(List<Integer> values) {
            addCriterion("tradeCert not in", values, "tradeCert");
            return (Criteria) this;
        }

        public Criteria andTradeCertBetween(Integer value1, Integer value2) {
            addCriterion("tradeCert between", value1, value2, "tradeCert");
            return (Criteria) this;
        }

        public Criteria andTradeCertNotBetween(Integer value1, Integer value2) {
            addCriterion("tradeCert not between", value1, value2, "tradeCert");
            return (Criteria) this;
        }

        public Criteria andTradeImgIsNull() {
            addCriterion("tradeImg is null");
            return (Criteria) this;
        }

        public Criteria andTradeImgIsNotNull() {
            addCriterion("tradeImg is not null");
            return (Criteria) this;
        }

        public Criteria andTradeImgEqualTo(String value) {
            addCriterion("tradeImg =", value, "tradeImg");
            return (Criteria) this;
        }

        public Criteria andTradeImgNotEqualTo(String value) {
            addCriterion("tradeImg <>", value, "tradeImg");
            return (Criteria) this;
        }

        public Criteria andTradeImgGreaterThan(String value) {
            addCriterion("tradeImg >", value, "tradeImg");
            return (Criteria) this;
        }

        public Criteria andTradeImgGreaterThanOrEqualTo(String value) {
            addCriterion("tradeImg >=", value, "tradeImg");
            return (Criteria) this;
        }

        public Criteria andTradeImgLessThan(String value) {
            addCriterion("tradeImg <", value, "tradeImg");
            return (Criteria) this;
        }

        public Criteria andTradeImgLessThanOrEqualTo(String value) {
            addCriterion("tradeImg <=", value, "tradeImg");
            return (Criteria) this;
        }

        public Criteria andTradeImgLike(String value) {
            addCriterion("tradeImg like", value, "tradeImg");
            return (Criteria) this;
        }

        public Criteria andTradeImgNotLike(String value) {
            addCriterion("tradeImg not like", value, "tradeImg");
            return (Criteria) this;
        }

        public Criteria andTradeImgIn(List<String> values) {
            addCriterion("tradeImg in", values, "tradeImg");
            return (Criteria) this;
        }

        public Criteria andTradeImgNotIn(List<String> values) {
            addCriterion("tradeImg not in", values, "tradeImg");
            return (Criteria) this;
        }

        public Criteria andTradeImgBetween(String value1, String value2) {
            addCriterion("tradeImg between", value1, value2, "tradeImg");
            return (Criteria) this;
        }

        public Criteria andTradeImgNotBetween(String value1, String value2) {
            addCriterion("tradeImg not between", value1, value2, "tradeImg");
            return (Criteria) this;
        }

        public Criteria andTradeOrgNameIsNull() {
            addCriterion("tradeOrgName is null");
            return (Criteria) this;
        }

        public Criteria andTradeOrgNameIsNotNull() {
            addCriterion("tradeOrgName is not null");
            return (Criteria) this;
        }

        public Criteria andTradeOrgNameEqualTo(String value) {
            addCriterion("tradeOrgName =", value, "tradeOrgName");
            return (Criteria) this;
        }

        public Criteria andTradeOrgNameNotEqualTo(String value) {
            addCriterion("tradeOrgName <>", value, "tradeOrgName");
            return (Criteria) this;
        }

        public Criteria andTradeOrgNameGreaterThan(String value) {
            addCriterion("tradeOrgName >", value, "tradeOrgName");
            return (Criteria) this;
        }

        public Criteria andTradeOrgNameGreaterThanOrEqualTo(String value) {
            addCriterion("tradeOrgName >=", value, "tradeOrgName");
            return (Criteria) this;
        }

        public Criteria andTradeOrgNameLessThan(String value) {
            addCriterion("tradeOrgName <", value, "tradeOrgName");
            return (Criteria) this;
        }

        public Criteria andTradeOrgNameLessThanOrEqualTo(String value) {
            addCriterion("tradeOrgName <=", value, "tradeOrgName");
            return (Criteria) this;
        }

        public Criteria andTradeOrgNameLike(String value) {
            addCriterion("tradeOrgName like", value, "tradeOrgName");
            return (Criteria) this;
        }

        public Criteria andTradeOrgNameNotLike(String value) {
            addCriterion("tradeOrgName not like", value, "tradeOrgName");
            return (Criteria) this;
        }

        public Criteria andTradeOrgNameIn(List<String> values) {
            addCriterion("tradeOrgName in", values, "tradeOrgName");
            return (Criteria) this;
        }

        public Criteria andTradeOrgNameNotIn(List<String> values) {
            addCriterion("tradeOrgName not in", values, "tradeOrgName");
            return (Criteria) this;
        }

        public Criteria andTradeOrgNameBetween(String value1, String value2) {
            addCriterion("tradeOrgName between", value1, value2, "tradeOrgName");
            return (Criteria) this;
        }

        public Criteria andTradeOrgNameNotBetween(String value1, String value2) {
            addCriterion("tradeOrgName not between", value1, value2, "tradeOrgName");
            return (Criteria) this;
        }

        public Criteria andTradeLeaderIsNull() {
            addCriterion("tradeLeader is null");
            return (Criteria) this;
        }

        public Criteria andTradeLeaderIsNotNull() {
            addCriterion("tradeLeader is not null");
            return (Criteria) this;
        }

        public Criteria andTradeLeaderEqualTo(String value) {
            addCriterion("tradeLeader =", value, "tradeLeader");
            return (Criteria) this;
        }

        public Criteria andTradeLeaderNotEqualTo(String value) {
            addCriterion("tradeLeader <>", value, "tradeLeader");
            return (Criteria) this;
        }

        public Criteria andTradeLeaderGreaterThan(String value) {
            addCriterion("tradeLeader >", value, "tradeLeader");
            return (Criteria) this;
        }

        public Criteria andTradeLeaderGreaterThanOrEqualTo(String value) {
            addCriterion("tradeLeader >=", value, "tradeLeader");
            return (Criteria) this;
        }

        public Criteria andTradeLeaderLessThan(String value) {
            addCriterion("tradeLeader <", value, "tradeLeader");
            return (Criteria) this;
        }

        public Criteria andTradeLeaderLessThanOrEqualTo(String value) {
            addCriterion("tradeLeader <=", value, "tradeLeader");
            return (Criteria) this;
        }

        public Criteria andTradeLeaderLike(String value) {
            addCriterion("tradeLeader like", value, "tradeLeader");
            return (Criteria) this;
        }

        public Criteria andTradeLeaderNotLike(String value) {
            addCriterion("tradeLeader not like", value, "tradeLeader");
            return (Criteria) this;
        }

        public Criteria andTradeLeaderIn(List<String> values) {
            addCriterion("tradeLeader in", values, "tradeLeader");
            return (Criteria) this;
        }

        public Criteria andTradeLeaderNotIn(List<String> values) {
            addCriterion("tradeLeader not in", values, "tradeLeader");
            return (Criteria) this;
        }

        public Criteria andTradeLeaderBetween(String value1, String value2) {
            addCriterion("tradeLeader between", value1, value2, "tradeLeader");
            return (Criteria) this;
        }

        public Criteria andTradeLeaderNotBetween(String value1, String value2) {
            addCriterion("tradeLeader not between", value1, value2, "tradeLeader");
            return (Criteria) this;
        }

        public Criteria andLicenseNumIsNull() {
            addCriterion("licenseNum is null");
            return (Criteria) this;
        }

        public Criteria andLicenseNumIsNotNull() {
            addCriterion("licenseNum is not null");
            return (Criteria) this;
        }

        public Criteria andLicenseNumEqualTo(String value) {
            addCriterion("licenseNum =", value, "licenseNum");
            return (Criteria) this;
        }

        public Criteria andLicenseNumNotEqualTo(String value) {
            addCriterion("licenseNum <>", value, "licenseNum");
            return (Criteria) this;
        }

        public Criteria andLicenseNumGreaterThan(String value) {
            addCriterion("licenseNum >", value, "licenseNum");
            return (Criteria) this;
        }

        public Criteria andLicenseNumGreaterThanOrEqualTo(String value) {
            addCriterion("licenseNum >=", value, "licenseNum");
            return (Criteria) this;
        }

        public Criteria andLicenseNumLessThan(String value) {
            addCriterion("licenseNum <", value, "licenseNum");
            return (Criteria) this;
        }

        public Criteria andLicenseNumLessThanOrEqualTo(String value) {
            addCriterion("licenseNum <=", value, "licenseNum");
            return (Criteria) this;
        }

        public Criteria andLicenseNumLike(String value) {
            addCriterion("licenseNum like", value, "licenseNum");
            return (Criteria) this;
        }

        public Criteria andLicenseNumNotLike(String value) {
            addCriterion("licenseNum not like", value, "licenseNum");
            return (Criteria) this;
        }

        public Criteria andLicenseNumIn(List<String> values) {
            addCriterion("licenseNum in", values, "licenseNum");
            return (Criteria) this;
        }

        public Criteria andLicenseNumNotIn(List<String> values) {
            addCriterion("licenseNum not in", values, "licenseNum");
            return (Criteria) this;
        }

        public Criteria andLicenseNumBetween(String value1, String value2) {
            addCriterion("licenseNum between", value1, value2, "licenseNum");
            return (Criteria) this;
        }

        public Criteria andLicenseNumNotBetween(String value1, String value2) {
            addCriterion("licenseNum not between", value1, value2, "licenseNum");
            return (Criteria) this;
        }

        public Criteria andLicenseAddressIsNull() {
            addCriterion("licenseAddress is null");
            return (Criteria) this;
        }

        public Criteria andLicenseAddressIsNotNull() {
            addCriterion("licenseAddress is not null");
            return (Criteria) this;
        }

        public Criteria andLicenseAddressEqualTo(String value) {
            addCriterion("licenseAddress =", value, "licenseAddress");
            return (Criteria) this;
        }

        public Criteria andLicenseAddressNotEqualTo(String value) {
            addCriterion("licenseAddress <>", value, "licenseAddress");
            return (Criteria) this;
        }

        public Criteria andLicenseAddressGreaterThan(String value) {
            addCriterion("licenseAddress >", value, "licenseAddress");
            return (Criteria) this;
        }

        public Criteria andLicenseAddressGreaterThanOrEqualTo(String value) {
            addCriterion("licenseAddress >=", value, "licenseAddress");
            return (Criteria) this;
        }

        public Criteria andLicenseAddressLessThan(String value) {
            addCriterion("licenseAddress <", value, "licenseAddress");
            return (Criteria) this;
        }

        public Criteria andLicenseAddressLessThanOrEqualTo(String value) {
            addCriterion("licenseAddress <=", value, "licenseAddress");
            return (Criteria) this;
        }

        public Criteria andLicenseAddressLike(String value) {
            addCriterion("licenseAddress like", value, "licenseAddress");
            return (Criteria) this;
        }

        public Criteria andLicenseAddressNotLike(String value) {
            addCriterion("licenseAddress not like", value, "licenseAddress");
            return (Criteria) this;
        }

        public Criteria andLicenseAddressIn(List<String> values) {
            addCriterion("licenseAddress in", values, "licenseAddress");
            return (Criteria) this;
        }

        public Criteria andLicenseAddressNotIn(List<String> values) {
            addCriterion("licenseAddress not in", values, "licenseAddress");
            return (Criteria) this;
        }

        public Criteria andLicenseAddressBetween(String value1, String value2) {
            addCriterion("licenseAddress between", value1, value2, "licenseAddress");
            return (Criteria) this;
        }

        public Criteria andLicenseAddressNotBetween(String value1, String value2) {
            addCriterion("licenseAddress not between", value1, value2, "licenseAddress");
            return (Criteria) this;
        }

        public Criteria andTradeExpirationDateIsNull() {
            addCriterion("tradeExpirationDate is null");
            return (Criteria) this;
        }

        public Criteria andTradeExpirationDateIsNotNull() {
            addCriterion("tradeExpirationDate is not null");
            return (Criteria) this;
        }

        public Criteria andTradeExpirationDateEqualTo(Date value) {
            addCriterion("tradeExpirationDate =", value, "tradeExpirationDate");
            return (Criteria) this;
        }

        public Criteria andTradeExpirationDateNotEqualTo(Date value) {
            addCriterion("tradeExpirationDate <>", value, "tradeExpirationDate");
            return (Criteria) this;
        }

        public Criteria andTradeExpirationDateGreaterThan(Date value) {
            addCriterion("tradeExpirationDate >", value, "tradeExpirationDate");
            return (Criteria) this;
        }

        public Criteria andTradeExpirationDateGreaterThanOrEqualTo(Date value) {
            addCriterion("tradeExpirationDate >=", value, "tradeExpirationDate");
            return (Criteria) this;
        }

        public Criteria andTradeExpirationDateLessThan(Date value) {
            addCriterion("tradeExpirationDate <", value, "tradeExpirationDate");
            return (Criteria) this;
        }

        public Criteria andTradeExpirationDateLessThanOrEqualTo(Date value) {
            addCriterion("tradeExpirationDate <=", value, "tradeExpirationDate");
            return (Criteria) this;
        }

        public Criteria andTradeExpirationDateIn(List<Date> values) {
            addCriterion("tradeExpirationDate in", values, "tradeExpirationDate");
            return (Criteria) this;
        }

        public Criteria andTradeExpirationDateNotIn(List<Date> values) {
            addCriterion("tradeExpirationDate not in", values, "tradeExpirationDate");
            return (Criteria) this;
        }

        public Criteria andTradeExpirationDateBetween(Date value1, Date value2) {
            addCriterion("tradeExpirationDate between", value1, value2, "tradeExpirationDate");
            return (Criteria) this;
        }

        public Criteria andTradeExpirationDateNotBetween(Date value1, Date value2) {
            addCriterion("tradeExpirationDate not between", value1, value2, "tradeExpirationDate");
            return (Criteria) this;
        }

        public Criteria andSuportSelfIsNull() {
            addCriterion("suportSelf is null");
            return (Criteria) this;
        }

        public Criteria andSuportSelfIsNotNull() {
            addCriterion("suportSelf is not null");
            return (Criteria) this;
        }

        public Criteria andSuportSelfEqualTo(Boolean value) {
            addCriterion("suportSelf =", value, "suportSelf");
            return (Criteria) this;
        }

        public Criteria andSuportSelfNotEqualTo(Boolean value) {
            addCriterion("suportSelf <>", value, "suportSelf");
            return (Criteria) this;
        }

        public Criteria andSuportSelfGreaterThan(Boolean value) {
            addCriterion("suportSelf >", value, "suportSelf");
            return (Criteria) this;
        }

        public Criteria andSuportSelfGreaterThanOrEqualTo(Boolean value) {
            addCriterion("suportSelf >=", value, "suportSelf");
            return (Criteria) this;
        }

        public Criteria andSuportSelfLessThan(Boolean value) {
            addCriterion("suportSelf <", value, "suportSelf");
            return (Criteria) this;
        }

        public Criteria andSuportSelfLessThanOrEqualTo(Boolean value) {
            addCriterion("suportSelf <=", value, "suportSelf");
            return (Criteria) this;
        }

        public Criteria andSuportSelfIn(List<Boolean> values) {
            addCriterion("suportSelf in", values, "suportSelf");
            return (Criteria) this;
        }

        public Criteria andSuportSelfNotIn(List<Boolean> values) {
            addCriterion("suportSelf not in", values, "suportSelf");
            return (Criteria) this;
        }

        public Criteria andSuportSelfBetween(Boolean value1, Boolean value2) {
            addCriterion("suportSelf between", value1, value2, "suportSelf");
            return (Criteria) this;
        }

        public Criteria andSuportSelfNotBetween(Boolean value1, Boolean value2) {
            addCriterion("suportSelf not between", value1, value2, "suportSelf");
            return (Criteria) this;
        }

        public Criteria andBoss_nameIsNull() {
            addCriterion("boss_name is null");
            return (Criteria) this;
        }

        public Criteria andBoss_nameIsNotNull() {
            addCriterion("boss_name is not null");
            return (Criteria) this;
        }

        public Criteria andBoss_nameEqualTo(String value) {
            addCriterion("boss_name =", value, "boss_name");
            return (Criteria) this;
        }

        public Criteria andBoss_nameNotEqualTo(String value) {
            addCriterion("boss_name <>", value, "boss_name");
            return (Criteria) this;
        }

        public Criteria andBoss_nameGreaterThan(String value) {
            addCriterion("boss_name >", value, "boss_name");
            return (Criteria) this;
        }

        public Criteria andBoss_nameGreaterThanOrEqualTo(String value) {
            addCriterion("boss_name >=", value, "boss_name");
            return (Criteria) this;
        }

        public Criteria andBoss_nameLessThan(String value) {
            addCriterion("boss_name <", value, "boss_name");
            return (Criteria) this;
        }

        public Criteria andBoss_nameLessThanOrEqualTo(String value) {
            addCriterion("boss_name <=", value, "boss_name");
            return (Criteria) this;
        }

        public Criteria andBoss_nameLike(String value) {
            addCriterion("boss_name like", value, "boss_name");
            return (Criteria) this;
        }

        public Criteria andBoss_nameNotLike(String value) {
            addCriterion("boss_name not like", value, "boss_name");
            return (Criteria) this;
        }

        public Criteria andBoss_nameIn(List<String> values) {
            addCriterion("boss_name in", values, "boss_name");
            return (Criteria) this;
        }

        public Criteria andBoss_nameNotIn(List<String> values) {
            addCriterion("boss_name not in", values, "boss_name");
            return (Criteria) this;
        }

        public Criteria andBoss_nameBetween(String value1, String value2) {
            addCriterion("boss_name between", value1, value2, "boss_name");
            return (Criteria) this;
        }

        public Criteria andBoss_nameNotBetween(String value1, String value2) {
            addCriterion("boss_name not between", value1, value2, "boss_name");
            return (Criteria) this;
        }

        public Criteria andUndoneIsNull() {
            addCriterion("undone is null");
            return (Criteria) this;
        }

        public Criteria andUndoneIsNotNull() {
            addCriterion("undone is not null");
            return (Criteria) this;
        }

        public Criteria andUndoneEqualTo(Boolean value) {
            addCriterion("undone =", value, "undone");
            return (Criteria) this;
        }

        public Criteria andUndoneNotEqualTo(Boolean value) {
            addCriterion("undone <>", value, "undone");
            return (Criteria) this;
        }

        public Criteria andUndoneGreaterThan(Boolean value) {
            addCriterion("undone >", value, "undone");
            return (Criteria) this;
        }

        public Criteria andUndoneGreaterThanOrEqualTo(Boolean value) {
            addCriterion("undone >=", value, "undone");
            return (Criteria) this;
        }

        public Criteria andUndoneLessThan(Boolean value) {
            addCriterion("undone <", value, "undone");
            return (Criteria) this;
        }

        public Criteria andUndoneLessThanOrEqualTo(Boolean value) {
            addCriterion("undone <=", value, "undone");
            return (Criteria) this;
        }

        public Criteria andUndoneIn(List<Boolean> values) {
            addCriterion("undone in", values, "undone");
            return (Criteria) this;
        }

        public Criteria andUndoneNotIn(List<Boolean> values) {
            addCriterion("undone not in", values, "undone");
            return (Criteria) this;
        }

        public Criteria andUndoneBetween(Boolean value1, Boolean value2) {
            addCriterion("undone between", value1, value2, "undone");
            return (Criteria) this;
        }

        public Criteria andUndoneNotBetween(Boolean value1, Boolean value2) {
            addCriterion("undone not between", value1, value2, "undone");
            return (Criteria) this;
        }

        public Criteria andEndwork3IsNull() {
            addCriterion("endwork3 is null");
            return (Criteria) this;
        }

        public Criteria andEndwork3IsNotNull() {
            addCriterion("endwork3 is not null");
            return (Criteria) this;
        }

        public Criteria andEndwork3EqualTo(Date value) {
            addCriterion("endwork3 =", value, "endwork3");
            return (Criteria) this;
        }

        public Criteria andEndwork3NotEqualTo(Date value) {
            addCriterion("endwork3 <>", value, "endwork3");
            return (Criteria) this;
        }

        public Criteria andEndwork3GreaterThan(Date value) {
            addCriterion("endwork3 >", value, "endwork3");
            return (Criteria) this;
        }

        public Criteria andEndwork3GreaterThanOrEqualTo(Date value) {
            addCriterion("endwork3 >=", value, "endwork3");
            return (Criteria) this;
        }

        public Criteria andEndwork3LessThan(Date value) {
            addCriterion("endwork3 <", value, "endwork3");
            return (Criteria) this;
        }

        public Criteria andEndwork3LessThanOrEqualTo(Date value) {
            addCriterion("endwork3 <=", value, "endwork3");
            return (Criteria) this;
        }

        public Criteria andEndwork3In(List<Date> values) {
            addCriterion("endwork3 in", values, "endwork3");
            return (Criteria) this;
        }

        public Criteria andEndwork3NotIn(List<Date> values) {
            addCriterion("endwork3 not in", values, "endwork3");
            return (Criteria) this;
        }

        public Criteria andEndwork3Between(Date value1, Date value2) {
            addCriterion("endwork3 between", value1, value2, "endwork3");
            return (Criteria) this;
        }

        public Criteria andEndwork3NotBetween(Date value1, Date value2) {
            addCriterion("endwork3 not between", value1, value2, "endwork3");
            return (Criteria) this;
        }

        public Criteria andStartwork3IsNull() {
            addCriterion("startwork3 is null");
            return (Criteria) this;
        }

        public Criteria andStartwork3IsNotNull() {
            addCriterion("startwork3 is not null");
            return (Criteria) this;
        }

        public Criteria andStartwork3EqualTo(Date value) {
            addCriterion("startwork3 =", value, "startwork3");
            return (Criteria) this;
        }

        public Criteria andStartwork3NotEqualTo(Date value) {
            addCriterion("startwork3 <>", value, "startwork3");
            return (Criteria) this;
        }

        public Criteria andStartwork3GreaterThan(Date value) {
            addCriterion("startwork3 >", value, "startwork3");
            return (Criteria) this;
        }

        public Criteria andStartwork3GreaterThanOrEqualTo(Date value) {
            addCriterion("startwork3 >=", value, "startwork3");
            return (Criteria) this;
        }

        public Criteria andStartwork3LessThan(Date value) {
            addCriterion("startwork3 <", value, "startwork3");
            return (Criteria) this;
        }

        public Criteria andStartwork3LessThanOrEqualTo(Date value) {
            addCriterion("startwork3 <=", value, "startwork3");
            return (Criteria) this;
        }

        public Criteria andStartwork3In(List<Date> values) {
            addCriterion("startwork3 in", values, "startwork3");
            return (Criteria) this;
        }

        public Criteria andStartwork3NotIn(List<Date> values) {
            addCriterion("startwork3 not in", values, "startwork3");
            return (Criteria) this;
        }

        public Criteria andStartwork3Between(Date value1, Date value2) {
            addCriterion("startwork3 between", value1, value2, "startwork3");
            return (Criteria) this;
        }

        public Criteria andStartwork3NotBetween(Date value1, Date value2) {
            addCriterion("startwork3 not between", value1, value2, "startwork3");
            return (Criteria) this;
        }

        public Criteria andBinding_bankIsNull() {
            addCriterion("binding_bank is null");
            return (Criteria) this;
        }

        public Criteria andBinding_bankIsNotNull() {
            addCriterion("binding_bank is not null");
            return (Criteria) this;
        }

        public Criteria andBinding_bankEqualTo(Boolean value) {
            addCriterion("binding_bank =", value, "binding_bank");
            return (Criteria) this;
        }

        public Criteria andBinding_bankNotEqualTo(Boolean value) {
            addCriterion("binding_bank <>", value, "binding_bank");
            return (Criteria) this;
        }

        public Criteria andBinding_bankGreaterThan(Boolean value) {
            addCriterion("binding_bank >", value, "binding_bank");
            return (Criteria) this;
        }

        public Criteria andBinding_bankGreaterThanOrEqualTo(Boolean value) {
            addCriterion("binding_bank >=", value, "binding_bank");
            return (Criteria) this;
        }

        public Criteria andBinding_bankLessThan(Boolean value) {
            addCriterion("binding_bank <", value, "binding_bank");
            return (Criteria) this;
        }

        public Criteria andBinding_bankLessThanOrEqualTo(Boolean value) {
            addCriterion("binding_bank <=", value, "binding_bank");
            return (Criteria) this;
        }

        public Criteria andBinding_bankIn(List<Boolean> values) {
            addCriterion("binding_bank in", values, "binding_bank");
            return (Criteria) this;
        }

        public Criteria andBinding_bankNotIn(List<Boolean> values) {
            addCriterion("binding_bank not in", values, "binding_bank");
            return (Criteria) this;
        }

        public Criteria andBinding_bankBetween(Boolean value1, Boolean value2) {
            addCriterion("binding_bank between", value1, value2, "binding_bank");
            return (Criteria) this;
        }

        public Criteria andBinding_bankNotBetween(Boolean value1, Boolean value2) {
            addCriterion("binding_bank not between", value1, value2, "binding_bank");
            return (Criteria) this;
        }

        public Criteria andExpire_timeIsNull() {
            addCriterion("expire_time is null");
            return (Criteria) this;
        }

        public Criteria andExpire_timeIsNotNull() {
            addCriterion("expire_time is not null");
            return (Criteria) this;
        }

        public Criteria andExpire_timeEqualTo(Date value) {
            addCriterion("expire_time =", value, "expire_time");
            return (Criteria) this;
        }

        public Criteria andExpire_timeNotEqualTo(Date value) {
            addCriterion("expire_time <>", value, "expire_time");
            return (Criteria) this;
        }

        public Criteria andExpire_timeGreaterThan(Date value) {
            addCriterion("expire_time >", value, "expire_time");
            return (Criteria) this;
        }

        public Criteria andExpire_timeGreaterThanOrEqualTo(Date value) {
            addCriterion("expire_time >=", value, "expire_time");
            return (Criteria) this;
        }

        public Criteria andExpire_timeLessThan(Date value) {
            addCriterion("expire_time <", value, "expire_time");
            return (Criteria) this;
        }

        public Criteria andExpire_timeLessThanOrEqualTo(Date value) {
            addCriterion("expire_time <=", value, "expire_time");
            return (Criteria) this;
        }

        public Criteria andExpire_timeIn(List<Date> values) {
            addCriterion("expire_time in", values, "expire_time");
            return (Criteria) this;
        }

        public Criteria andExpire_timeNotIn(List<Date> values) {
            addCriterion("expire_time not in", values, "expire_time");
            return (Criteria) this;
        }

        public Criteria andExpire_timeBetween(Date value1, Date value2) {
            addCriterion("expire_time between", value1, value2, "expire_time");
            return (Criteria) this;
        }

        public Criteria andExpire_timeNotBetween(Date value1, Date value2) {
            addCriterion("expire_time not between", value1, value2, "expire_time");
            return (Criteria) this;
        }

        public Criteria andProvince_idIsNull() {
            addCriterion("province_id is null");
            return (Criteria) this;
        }

        public Criteria andProvince_idIsNotNull() {
            addCriterion("province_id is not null");
            return (Criteria) this;
        }

        public Criteria andProvince_idEqualTo(String value) {
            addCriterion("province_id =", value, "province_id");
            return (Criteria) this;
        }

        public Criteria andProvince_idNotEqualTo(String value) {
            addCriterion("province_id <>", value, "province_id");
            return (Criteria) this;
        }

        public Criteria andProvince_idGreaterThan(String value) {
            addCriterion("province_id >", value, "province_id");
            return (Criteria) this;
        }

        public Criteria andProvince_idGreaterThanOrEqualTo(String value) {
            addCriterion("province_id >=", value, "province_id");
            return (Criteria) this;
        }

        public Criteria andProvince_idLessThan(String value) {
            addCriterion("province_id <", value, "province_id");
            return (Criteria) this;
        }

        public Criteria andProvince_idLessThanOrEqualTo(String value) {
            addCriterion("province_id <=", value, "province_id");
            return (Criteria) this;
        }

        public Criteria andProvince_idLike(String value) {
            addCriterion("province_id like", value, "province_id");
            return (Criteria) this;
        }

        public Criteria andProvince_idNotLike(String value) {
            addCriterion("province_id not like", value, "province_id");
            return (Criteria) this;
        }

        public Criteria andProvince_idIn(List<String> values) {
            addCriterion("province_id in", values, "province_id");
            return (Criteria) this;
        }

        public Criteria andProvince_idNotIn(List<String> values) {
            addCriterion("province_id not in", values, "province_id");
            return (Criteria) this;
        }

        public Criteria andProvince_idBetween(String value1, String value2) {
            addCriterion("province_id between", value1, value2, "province_id");
            return (Criteria) this;
        }

        public Criteria andProvince_idNotBetween(String value1, String value2) {
            addCriterion("province_id not between", value1, value2, "province_id");
            return (Criteria) this;
        }

        public Criteria andProvince_nameIsNull() {
            addCriterion("province_name is null");
            return (Criteria) this;
        }

        public Criteria andProvince_nameIsNotNull() {
            addCriterion("province_name is not null");
            return (Criteria) this;
        }

        public Criteria andProvince_nameEqualTo(String value) {
            addCriterion("province_name =", value, "province_name");
            return (Criteria) this;
        }

        public Criteria andProvince_nameNotEqualTo(String value) {
            addCriterion("province_name <>", value, "province_name");
            return (Criteria) this;
        }

        public Criteria andProvince_nameGreaterThan(String value) {
            addCriterion("province_name >", value, "province_name");
            return (Criteria) this;
        }

        public Criteria andProvince_nameGreaterThanOrEqualTo(String value) {
            addCriterion("province_name >=", value, "province_name");
            return (Criteria) this;
        }

        public Criteria andProvince_nameLessThan(String value) {
            addCriterion("province_name <", value, "province_name");
            return (Criteria) this;
        }

        public Criteria andProvince_nameLessThanOrEqualTo(String value) {
            addCriterion("province_name <=", value, "province_name");
            return (Criteria) this;
        }

        public Criteria andProvince_nameLike(String value) {
            addCriterion("province_name like", value, "province_name");
            return (Criteria) this;
        }

        public Criteria andProvince_nameNotLike(String value) {
            addCriterion("province_name not like", value, "province_name");
            return (Criteria) this;
        }

        public Criteria andProvince_nameIn(List<String> values) {
            addCriterion("province_name in", values, "province_name");
            return (Criteria) this;
        }

        public Criteria andProvince_nameNotIn(List<String> values) {
            addCriterion("province_name not in", values, "province_name");
            return (Criteria) this;
        }

        public Criteria andProvince_nameBetween(String value1, String value2) {
            addCriterion("province_name between", value1, value2, "province_name");
            return (Criteria) this;
        }

        public Criteria andProvince_nameNotBetween(String value1, String value2) {
            addCriterion("province_name not between", value1, value2, "province_name");
            return (Criteria) this;
        }

        public Criteria andMain_class_idIsNull() {
            addCriterion("main_class_id is null");
            return (Criteria) this;
        }

        public Criteria andMain_class_idIsNotNull() {
            addCriterion("main_class_id is not null");
            return (Criteria) this;
        }

        public Criteria andMain_class_idEqualTo(Integer value) {
            addCriterion("main_class_id =", value, "main_class_id");
            return (Criteria) this;
        }

        public Criteria andMain_class_idNotEqualTo(Integer value) {
            addCriterion("main_class_id <>", value, "main_class_id");
            return (Criteria) this;
        }

        public Criteria andMain_class_idGreaterThan(Integer value) {
            addCriterion("main_class_id >", value, "main_class_id");
            return (Criteria) this;
        }

        public Criteria andMain_class_idGreaterThanOrEqualTo(Integer value) {
            addCriterion("main_class_id >=", value, "main_class_id");
            return (Criteria) this;
        }

        public Criteria andMain_class_idLessThan(Integer value) {
            addCriterion("main_class_id <", value, "main_class_id");
            return (Criteria) this;
        }

        public Criteria andMain_class_idLessThanOrEqualTo(Integer value) {
            addCriterion("main_class_id <=", value, "main_class_id");
            return (Criteria) this;
        }

        public Criteria andMain_class_idIn(List<Integer> values) {
            addCriterion("main_class_id in", values, "main_class_id");
            return (Criteria) this;
        }

        public Criteria andMain_class_idNotIn(List<Integer> values) {
            addCriterion("main_class_id not in", values, "main_class_id");
            return (Criteria) this;
        }

        public Criteria andMain_class_idBetween(Integer value1, Integer value2) {
            addCriterion("main_class_id between", value1, value2, "main_class_id");
            return (Criteria) this;
        }

        public Criteria andMain_class_idNotBetween(Integer value1, Integer value2) {
            addCriterion("main_class_id not between", value1, value2, "main_class_id");
            return (Criteria) this;
        }

        public Criteria andSub_class_id1IsNull() {
            addCriterion("sub_class_id1 is null");
            return (Criteria) this;
        }

        public Criteria andSub_class_id1IsNotNull() {
            addCriterion("sub_class_id1 is not null");
            return (Criteria) this;
        }

        public Criteria andSub_class_id1EqualTo(Integer value) {
            addCriterion("sub_class_id1 =", value, "sub_class_id1");
            return (Criteria) this;
        }

        public Criteria andSub_class_id1NotEqualTo(Integer value) {
            addCriterion("sub_class_id1 <>", value, "sub_class_id1");
            return (Criteria) this;
        }

        public Criteria andSub_class_id1GreaterThan(Integer value) {
            addCriterion("sub_class_id1 >", value, "sub_class_id1");
            return (Criteria) this;
        }

        public Criteria andSub_class_id1GreaterThanOrEqualTo(Integer value) {
            addCriterion("sub_class_id1 >=", value, "sub_class_id1");
            return (Criteria) this;
        }

        public Criteria andSub_class_id1LessThan(Integer value) {
            addCriterion("sub_class_id1 <", value, "sub_class_id1");
            return (Criteria) this;
        }

        public Criteria andSub_class_id1LessThanOrEqualTo(Integer value) {
            addCriterion("sub_class_id1 <=", value, "sub_class_id1");
            return (Criteria) this;
        }

        public Criteria andSub_class_id1In(List<Integer> values) {
            addCriterion("sub_class_id1 in", values, "sub_class_id1");
            return (Criteria) this;
        }

        public Criteria andSub_class_id1NotIn(List<Integer> values) {
            addCriterion("sub_class_id1 not in", values, "sub_class_id1");
            return (Criteria) this;
        }

        public Criteria andSub_class_id1Between(Integer value1, Integer value2) {
            addCriterion("sub_class_id1 between", value1, value2, "sub_class_id1");
            return (Criteria) this;
        }

        public Criteria andSub_class_id1NotBetween(Integer value1, Integer value2) {
            addCriterion("sub_class_id1 not between", value1, value2, "sub_class_id1");
            return (Criteria) this;
        }

        public Criteria andSub_class_id2IsNull() {
            addCriterion("sub_class_id2 is null");
            return (Criteria) this;
        }

        public Criteria andSub_class_id2IsNotNull() {
            addCriterion("sub_class_id2 is not null");
            return (Criteria) this;
        }

        public Criteria andSub_class_id2EqualTo(Integer value) {
            addCriterion("sub_class_id2 =", value, "sub_class_id2");
            return (Criteria) this;
        }

        public Criteria andSub_class_id2NotEqualTo(Integer value) {
            addCriterion("sub_class_id2 <>", value, "sub_class_id2");
            return (Criteria) this;
        }

        public Criteria andSub_class_id2GreaterThan(Integer value) {
            addCriterion("sub_class_id2 >", value, "sub_class_id2");
            return (Criteria) this;
        }

        public Criteria andSub_class_id2GreaterThanOrEqualTo(Integer value) {
            addCriterion("sub_class_id2 >=", value, "sub_class_id2");
            return (Criteria) this;
        }

        public Criteria andSub_class_id2LessThan(Integer value) {
            addCriterion("sub_class_id2 <", value, "sub_class_id2");
            return (Criteria) this;
        }

        public Criteria andSub_class_id2LessThanOrEqualTo(Integer value) {
            addCriterion("sub_class_id2 <=", value, "sub_class_id2");
            return (Criteria) this;
        }

        public Criteria andSub_class_id2In(List<Integer> values) {
            addCriterion("sub_class_id2 in", values, "sub_class_id2");
            return (Criteria) this;
        }

        public Criteria andSub_class_id2NotIn(List<Integer> values) {
            addCriterion("sub_class_id2 not in", values, "sub_class_id2");
            return (Criteria) this;
        }

        public Criteria andSub_class_id2Between(Integer value1, Integer value2) {
            addCriterion("sub_class_id2 between", value1, value2, "sub_class_id2");
            return (Criteria) this;
        }

        public Criteria andSub_class_id2NotBetween(Integer value1, Integer value2) {
            addCriterion("sub_class_id2 not between", value1, value2, "sub_class_id2");
            return (Criteria) this;
        }

        public Criteria andBookableIsNull() {
            addCriterion("bookable is null");
            return (Criteria) this;
        }

        public Criteria andBookableIsNotNull() {
            addCriterion("bookable is not null");
            return (Criteria) this;
        }

        public Criteria andBookableEqualTo(Boolean value) {
            addCriterion("bookable =", value, "bookable");
            return (Criteria) this;
        }

        public Criteria andBookableNotEqualTo(Boolean value) {
            addCriterion("bookable <>", value, "bookable");
            return (Criteria) this;
        }

        public Criteria andBookableGreaterThan(Boolean value) {
            addCriterion("bookable >", value, "bookable");
            return (Criteria) this;
        }

        public Criteria andBookableGreaterThanOrEqualTo(Boolean value) {
            addCriterion("bookable >=", value, "bookable");
            return (Criteria) this;
        }

        public Criteria andBookableLessThan(Boolean value) {
            addCriterion("bookable <", value, "bookable");
            return (Criteria) this;
        }

        public Criteria andBookableLessThanOrEqualTo(Boolean value) {
            addCriterion("bookable <=", value, "bookable");
            return (Criteria) this;
        }

        public Criteria andBookableIn(List<Boolean> values) {
            addCriterion("bookable in", values, "bookable");
            return (Criteria) this;
        }

        public Criteria andBookableNotIn(List<Boolean> values) {
            addCriterion("bookable not in", values, "bookable");
            return (Criteria) this;
        }

        public Criteria andBookableBetween(Boolean value1, Boolean value2) {
            addCriterion("bookable between", value1, value2, "bookable");
            return (Criteria) this;
        }

        public Criteria andBookableNotBetween(Boolean value1, Boolean value2) {
            addCriterion("bookable not between", value1, value2, "bookable");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}