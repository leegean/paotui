package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastRedPacketRecord;
import com.runfast.waimai.dao.model.RunfastRedPacketRecordExample;
import com.runfast.waimai.web.dto.RedPacketRecordWithOneRedActivityDto;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

@Mapper
public interface RunfastRedPacketRecordMapper extends IMapper<RunfastRedPacketRecord, Integer, RunfastRedPacketRecordExample> {
    RedPacketRecordWithOneRedActivityDto getRedPacketRecordWithOneRedActivity(Integer userRedId);


    List<Map<String, Object>> findRedRecord(Integer userId, List<Integer> activityTypeList, Pageable pageable);

    List<Map<String, Object>>  findValidRedRecord(Integer userId, Integer businessId, double full, List<Integer> activityTypeList);
}