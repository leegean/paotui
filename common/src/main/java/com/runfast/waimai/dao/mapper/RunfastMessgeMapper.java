package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastMessge;
import com.runfast.waimai.dao.model.RunfastMessgeExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastMessgeMapper extends IMapper<RunfastMessge, Integer, RunfastMessgeExample> {
}