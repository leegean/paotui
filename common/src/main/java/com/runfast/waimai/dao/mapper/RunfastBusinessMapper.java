package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastBusiness;
import com.runfast.waimai.dao.model.RunfastBusinessExample;
import com.runfast.waimai.web.dto.BusinessDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Mapper
public interface RunfastBusinessMapper extends IMapper<RunfastBusiness, Integer, RunfastBusinessExample> {
    List<BusinessDto> nearBy(@Param("agentId") Integer agentId, @Param("distance") Integer distance, @Param("longitude") Double longitude, @Param("latitude") Double latitude, Date deliveryStart1, Date deliveryEnd1, Date deliveryStart2, Date deliveryEnd2, @Param("sorting") Integer sorting, List<Integer> activityType, Integer specialType, List<Integer> catalogId, Integer businessFeature, @Param("pageable") Pageable pageable);

    BusinessDto detailWithDistance(Integer businessId, Double userLng, Double userLat);

    List<BusinessDto> search(String name, Integer agentId, Double userLng, Double userLat, Date deliveryStart1, Date deliveryEnd1, Date deliveryStart2, Date deliveryEnd2, Pageable pageable);

    List<Map<String, Object>> getAgentZoneBusiness(Integer agentId, List<Integer> activityTypeList, Pageable pageable);

    List<BusinessDto> listBusiness(List<Integer> businessIdList, Double userLng, Double userLat);

    List<BusinessDto> listBusinessIsOpen(List<Integer> businessIdList, Double userLng, Double userLat,Date deliveryStart1, Date deliveryEnd1, Date deliveryStart2, Date deliveryEnd2);

    Map<String,Object> findNameById(Integer id);

    Map<String,Object> findImgById(Integer id);

    BusinessDto detailWithIsOpen(Integer businessId, Double userLng, Double userLat, Date deliveryStart1, Date deliveryEnd1, Date deliveryStart2, Date deliveryEnd2);
}