package com.runfast.waimai.dao.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RunfastGoodsSellOutStatusExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public RunfastGoodsSellOutStatusExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andBusinessIdIsNull() {
            addCriterion("businessId is null");
            return (Criteria) this;
        }

        public Criteria andBusinessIdIsNotNull() {
            addCriterion("businessId is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessIdEqualTo(Integer value) {
            addCriterion("businessId =", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdNotEqualTo(Integer value) {
            addCriterion("businessId <>", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdGreaterThan(Integer value) {
            addCriterion("businessId >", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("businessId >=", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdLessThan(Integer value) {
            addCriterion("businessId <", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdLessThanOrEqualTo(Integer value) {
            addCriterion("businessId <=", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdIn(List<Integer> values) {
            addCriterion("businessId in", values, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdNotIn(List<Integer> values) {
            addCriterion("businessId not in", values, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdBetween(Integer value1, Integer value2) {
            addCriterion("businessId between", value1, value2, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdNotBetween(Integer value1, Integer value2) {
            addCriterion("businessId not between", value1, value2, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessNameIsNull() {
            addCriterion("businessName is null");
            return (Criteria) this;
        }

        public Criteria andBusinessNameIsNotNull() {
            addCriterion("businessName is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessNameEqualTo(String value) {
            addCriterion("businessName =", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameNotEqualTo(String value) {
            addCriterion("businessName <>", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameGreaterThan(String value) {
            addCriterion("businessName >", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameGreaterThanOrEqualTo(String value) {
            addCriterion("businessName >=", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameLessThan(String value) {
            addCriterion("businessName <", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameLessThanOrEqualTo(String value) {
            addCriterion("businessName <=", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameLike(String value) {
            addCriterion("businessName like", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameNotLike(String value) {
            addCriterion("businessName not like", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameIn(List<String> values) {
            addCriterion("businessName in", values, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameNotIn(List<String> values) {
            addCriterion("businessName not in", values, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameBetween(String value1, String value2) {
            addCriterion("businessName between", value1, value2, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameNotBetween(String value1, String value2) {
            addCriterion("businessName not between", value1, value2, "businessName");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("createTime is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("createTime is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("createTime =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("createTime <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("createTime >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("createTime >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("createTime <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("createTime <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("createTime in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("createTime not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("createTime between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("createTime not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdIsNull() {
            addCriterion("goodsSellId is null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdIsNotNull() {
            addCriterion("goodsSellId is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdEqualTo(Integer value) {
            addCriterion("goodsSellId =", value, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdNotEqualTo(Integer value) {
            addCriterion("goodsSellId <>", value, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdGreaterThan(Integer value) {
            addCriterion("goodsSellId >", value, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("goodsSellId >=", value, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdLessThan(Integer value) {
            addCriterion("goodsSellId <", value, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdLessThanOrEqualTo(Integer value) {
            addCriterion("goodsSellId <=", value, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdIn(List<Integer> values) {
            addCriterion("goodsSellId in", values, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdNotIn(List<Integer> values) {
            addCriterion("goodsSellId not in", values, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdBetween(Integer value1, Integer value2) {
            addCriterion("goodsSellId between", value1, value2, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdNotBetween(Integer value1, Integer value2) {
            addCriterion("goodsSellId not between", value1, value2, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameIsNull() {
            addCriterion("goodsSellName is null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameIsNotNull() {
            addCriterion("goodsSellName is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameEqualTo(String value) {
            addCriterion("goodsSellName =", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameNotEqualTo(String value) {
            addCriterion("goodsSellName <>", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameGreaterThan(String value) {
            addCriterion("goodsSellName >", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameGreaterThanOrEqualTo(String value) {
            addCriterion("goodsSellName >=", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameLessThan(String value) {
            addCriterion("goodsSellName <", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameLessThanOrEqualTo(String value) {
            addCriterion("goodsSellName <=", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameLike(String value) {
            addCriterion("goodsSellName like", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameNotLike(String value) {
            addCriterion("goodsSellName not like", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameIn(List<String> values) {
            addCriterion("goodsSellName in", values, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameNotIn(List<String> values) {
            addCriterion("goodsSellName not in", values, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameBetween(String value1, String value2) {
            addCriterion("goodsSellName between", value1, value2, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameNotBetween(String value1, String value2) {
            addCriterion("goodsSellName not between", value1, value2, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordCodeIsNull() {
            addCriterion("goodsSellRecordCode is null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordCodeIsNotNull() {
            addCriterion("goodsSellRecordCode is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordCodeEqualTo(String value) {
            addCriterion("goodsSellRecordCode =", value, "goodsSellRecordCode");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordCodeNotEqualTo(String value) {
            addCriterion("goodsSellRecordCode <>", value, "goodsSellRecordCode");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordCodeGreaterThan(String value) {
            addCriterion("goodsSellRecordCode >", value, "goodsSellRecordCode");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordCodeGreaterThanOrEqualTo(String value) {
            addCriterion("goodsSellRecordCode >=", value, "goodsSellRecordCode");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordCodeLessThan(String value) {
            addCriterion("goodsSellRecordCode <", value, "goodsSellRecordCode");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordCodeLessThanOrEqualTo(String value) {
            addCriterion("goodsSellRecordCode <=", value, "goodsSellRecordCode");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordCodeLike(String value) {
            addCriterion("goodsSellRecordCode like", value, "goodsSellRecordCode");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordCodeNotLike(String value) {
            addCriterion("goodsSellRecordCode not like", value, "goodsSellRecordCode");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordCodeIn(List<String> values) {
            addCriterion("goodsSellRecordCode in", values, "goodsSellRecordCode");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordCodeNotIn(List<String> values) {
            addCriterion("goodsSellRecordCode not in", values, "goodsSellRecordCode");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordCodeBetween(String value1, String value2) {
            addCriterion("goodsSellRecordCode between", value1, value2, "goodsSellRecordCode");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordCodeNotBetween(String value1, String value2) {
            addCriterion("goodsSellRecordCode not between", value1, value2, "goodsSellRecordCode");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdIsNull() {
            addCriterion("goodsSellRecordId is null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdIsNotNull() {
            addCriterion("goodsSellRecordId is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdEqualTo(Integer value) {
            addCriterion("goodsSellRecordId =", value, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdNotEqualTo(Integer value) {
            addCriterion("goodsSellRecordId <>", value, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdGreaterThan(Integer value) {
            addCriterion("goodsSellRecordId >", value, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("goodsSellRecordId >=", value, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdLessThan(Integer value) {
            addCriterion("goodsSellRecordId <", value, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdLessThanOrEqualTo(Integer value) {
            addCriterion("goodsSellRecordId <=", value, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdIn(List<Integer> values) {
            addCriterion("goodsSellRecordId in", values, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdNotIn(List<Integer> values) {
            addCriterion("goodsSellRecordId not in", values, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdBetween(Integer value1, Integer value2) {
            addCriterion("goodsSellRecordId between", value1, value2, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdNotBetween(Integer value1, Integer value2) {
            addCriterion("goodsSellRecordId not between", value1, value2, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordNameIsNull() {
            addCriterion("goodsSellRecordName is null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordNameIsNotNull() {
            addCriterion("goodsSellRecordName is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordNameEqualTo(String value) {
            addCriterion("goodsSellRecordName =", value, "goodsSellRecordName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordNameNotEqualTo(String value) {
            addCriterion("goodsSellRecordName <>", value, "goodsSellRecordName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordNameGreaterThan(String value) {
            addCriterion("goodsSellRecordName >", value, "goodsSellRecordName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordNameGreaterThanOrEqualTo(String value) {
            addCriterion("goodsSellRecordName >=", value, "goodsSellRecordName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordNameLessThan(String value) {
            addCriterion("goodsSellRecordName <", value, "goodsSellRecordName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordNameLessThanOrEqualTo(String value) {
            addCriterion("goodsSellRecordName <=", value, "goodsSellRecordName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordNameLike(String value) {
            addCriterion("goodsSellRecordName like", value, "goodsSellRecordName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordNameNotLike(String value) {
            addCriterion("goodsSellRecordName not like", value, "goodsSellRecordName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordNameIn(List<String> values) {
            addCriterion("goodsSellRecordName in", values, "goodsSellRecordName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordNameNotIn(List<String> values) {
            addCriterion("goodsSellRecordName not in", values, "goodsSellRecordName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordNameBetween(String value1, String value2) {
            addCriterion("goodsSellRecordName between", value1, value2, "goodsSellRecordName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordNameNotBetween(String value1, String value2) {
            addCriterion("goodsSellRecordName not between", value1, value2, "goodsSellRecordName");
            return (Criteria) this;
        }

        public Criteria andOperationIdIsNull() {
            addCriterion("operationId is null");
            return (Criteria) this;
        }

        public Criteria andOperationIdIsNotNull() {
            addCriterion("operationId is not null");
            return (Criteria) this;
        }

        public Criteria andOperationIdEqualTo(Integer value) {
            addCriterion("operationId =", value, "operationId");
            return (Criteria) this;
        }

        public Criteria andOperationIdNotEqualTo(Integer value) {
            addCriterion("operationId <>", value, "operationId");
            return (Criteria) this;
        }

        public Criteria andOperationIdGreaterThan(Integer value) {
            addCriterion("operationId >", value, "operationId");
            return (Criteria) this;
        }

        public Criteria andOperationIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("operationId >=", value, "operationId");
            return (Criteria) this;
        }

        public Criteria andOperationIdLessThan(Integer value) {
            addCriterion("operationId <", value, "operationId");
            return (Criteria) this;
        }

        public Criteria andOperationIdLessThanOrEqualTo(Integer value) {
            addCriterion("operationId <=", value, "operationId");
            return (Criteria) this;
        }

        public Criteria andOperationIdIn(List<Integer> values) {
            addCriterion("operationId in", values, "operationId");
            return (Criteria) this;
        }

        public Criteria andOperationIdNotIn(List<Integer> values) {
            addCriterion("operationId not in", values, "operationId");
            return (Criteria) this;
        }

        public Criteria andOperationIdBetween(Integer value1, Integer value2) {
            addCriterion("operationId between", value1, value2, "operationId");
            return (Criteria) this;
        }

        public Criteria andOperationIdNotBetween(Integer value1, Integer value2) {
            addCriterion("operationId not between", value1, value2, "operationId");
            return (Criteria) this;
        }

        public Criteria andOperationNameIsNull() {
            addCriterion("operationName is null");
            return (Criteria) this;
        }

        public Criteria andOperationNameIsNotNull() {
            addCriterion("operationName is not null");
            return (Criteria) this;
        }

        public Criteria andOperationNameEqualTo(String value) {
            addCriterion("operationName =", value, "operationName");
            return (Criteria) this;
        }

        public Criteria andOperationNameNotEqualTo(String value) {
            addCriterion("operationName <>", value, "operationName");
            return (Criteria) this;
        }

        public Criteria andOperationNameGreaterThan(String value) {
            addCriterion("operationName >", value, "operationName");
            return (Criteria) this;
        }

        public Criteria andOperationNameGreaterThanOrEqualTo(String value) {
            addCriterion("operationName >=", value, "operationName");
            return (Criteria) this;
        }

        public Criteria andOperationNameLessThan(String value) {
            addCriterion("operationName <", value, "operationName");
            return (Criteria) this;
        }

        public Criteria andOperationNameLessThanOrEqualTo(String value) {
            addCriterion("operationName <=", value, "operationName");
            return (Criteria) this;
        }

        public Criteria andOperationNameLike(String value) {
            addCriterion("operationName like", value, "operationName");
            return (Criteria) this;
        }

        public Criteria andOperationNameNotLike(String value) {
            addCriterion("operationName not like", value, "operationName");
            return (Criteria) this;
        }

        public Criteria andOperationNameIn(List<String> values) {
            addCriterion("operationName in", values, "operationName");
            return (Criteria) this;
        }

        public Criteria andOperationNameNotIn(List<String> values) {
            addCriterion("operationName not in", values, "operationName");
            return (Criteria) this;
        }

        public Criteria andOperationNameBetween(String value1, String value2) {
            addCriterion("operationName between", value1, value2, "operationName");
            return (Criteria) this;
        }

        public Criteria andOperationNameNotBetween(String value1, String value2) {
            addCriterion("operationName not between", value1, value2, "operationName");
            return (Criteria) this;
        }

        public Criteria andSortIsNull() {
            addCriterion("sort is null");
            return (Criteria) this;
        }

        public Criteria andSortIsNotNull() {
            addCriterion("sort is not null");
            return (Criteria) this;
        }

        public Criteria andSortEqualTo(Integer value) {
            addCriterion("sort =", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotEqualTo(Integer value) {
            addCriterion("sort <>", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThan(Integer value) {
            addCriterion("sort >", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortGreaterThanOrEqualTo(Integer value) {
            addCriterion("sort >=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThan(Integer value) {
            addCriterion("sort <", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortLessThanOrEqualTo(Integer value) {
            addCriterion("sort <=", value, "sort");
            return (Criteria) this;
        }

        public Criteria andSortIn(List<Integer> values) {
            addCriterion("sort in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotIn(List<Integer> values) {
            addCriterion("sort not in", values, "sort");
            return (Criteria) this;
        }

        public Criteria andSortBetween(Integer value1, Integer value2) {
            addCriterion("sort between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andSortNotBetween(Integer value1, Integer value2) {
            addCriterion("sort not between", value1, value2, "sort");
            return (Criteria) this;
        }

        public Criteria andTypeIsNull() {
            addCriterion("type is null");
            return (Criteria) this;
        }

        public Criteria andTypeIsNotNull() {
            addCriterion("type is not null");
            return (Criteria) this;
        }

        public Criteria andTypeEqualTo(Integer value) {
            addCriterion("type =", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotEqualTo(Integer value) {
            addCriterion("type <>", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThan(Integer value) {
            addCriterion("type >", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("type >=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThan(Integer value) {
            addCriterion("type <", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThanOrEqualTo(Integer value) {
            addCriterion("type <=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeIn(List<Integer> values) {
            addCriterion("type in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotIn(List<Integer> values) {
            addCriterion("type not in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeBetween(Integer value1, Integer value2) {
            addCriterion("type between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("type not between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andStatStrIsNull() {
            addCriterion("statStr is null");
            return (Criteria) this;
        }

        public Criteria andStatStrIsNotNull() {
            addCriterion("statStr is not null");
            return (Criteria) this;
        }

        public Criteria andStatStrEqualTo(String value) {
            addCriterion("statStr =", value, "statStr");
            return (Criteria) this;
        }

        public Criteria andStatStrNotEqualTo(String value) {
            addCriterion("statStr <>", value, "statStr");
            return (Criteria) this;
        }

        public Criteria andStatStrGreaterThan(String value) {
            addCriterion("statStr >", value, "statStr");
            return (Criteria) this;
        }

        public Criteria andStatStrGreaterThanOrEqualTo(String value) {
            addCriterion("statStr >=", value, "statStr");
            return (Criteria) this;
        }

        public Criteria andStatStrLessThan(String value) {
            addCriterion("statStr <", value, "statStr");
            return (Criteria) this;
        }

        public Criteria andStatStrLessThanOrEqualTo(String value) {
            addCriterion("statStr <=", value, "statStr");
            return (Criteria) this;
        }

        public Criteria andStatStrLike(String value) {
            addCriterion("statStr like", value, "statStr");
            return (Criteria) this;
        }

        public Criteria andStatStrNotLike(String value) {
            addCriterion("statStr not like", value, "statStr");
            return (Criteria) this;
        }

        public Criteria andStatStrIn(List<String> values) {
            addCriterion("statStr in", values, "statStr");
            return (Criteria) this;
        }

        public Criteria andStatStrNotIn(List<String> values) {
            addCriterion("statStr not in", values, "statStr");
            return (Criteria) this;
        }

        public Criteria andStatStrBetween(String value1, String value2) {
            addCriterion("statStr between", value1, value2, "statStr");
            return (Criteria) this;
        }

        public Criteria andStatStrNotBetween(String value1, String value2) {
            addCriterion("statStr not between", value1, value2, "statStr");
            return (Criteria) this;
        }

        public Criteria andDescriptionIsNull() {
            addCriterion("description is null");
            return (Criteria) this;
        }

        public Criteria andDescriptionIsNotNull() {
            addCriterion("description is not null");
            return (Criteria) this;
        }

        public Criteria andDescriptionEqualTo(String value) {
            addCriterion("description =", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotEqualTo(String value) {
            addCriterion("description <>", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionGreaterThan(String value) {
            addCriterion("description >", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionGreaterThanOrEqualTo(String value) {
            addCriterion("description >=", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLessThan(String value) {
            addCriterion("description <", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLessThanOrEqualTo(String value) {
            addCriterion("description <=", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLike(String value) {
            addCriterion("description like", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotLike(String value) {
            addCriterion("description not like", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionIn(List<String> values) {
            addCriterion("description in", values, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotIn(List<String> values) {
            addCriterion("description not in", values, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionBetween(String value1, String value2) {
            addCriterion("description between", value1, value2, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotBetween(String value1, String value2) {
            addCriterion("description not between", value1, value2, "description");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}