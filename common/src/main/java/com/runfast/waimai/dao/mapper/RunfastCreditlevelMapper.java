package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastCreditlevel;
import com.runfast.waimai.dao.model.RunfastCreditlevelExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastCreditlevelMapper extends IMapper<RunfastCreditlevel, Integer, RunfastCreditlevelExample> {
}