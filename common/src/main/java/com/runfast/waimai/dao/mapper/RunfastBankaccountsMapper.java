package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastBankaccounts;
import com.runfast.waimai.dao.model.RunfastBankaccountsExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastBankaccountsMapper extends IMapper<RunfastBankaccounts, Integer, RunfastBankaccountsExample> {
    public Integer deleteByPrimaryKey(Integer id);
}