package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.BaseRolePurview;
import com.runfast.waimai.dao.model.BaseRolePurviewExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BaseRolePurviewMapper extends IMapper<BaseRolePurview, Integer, BaseRolePurviewExample> {
}