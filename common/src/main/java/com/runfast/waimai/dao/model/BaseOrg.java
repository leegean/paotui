package com.runfast.waimai.dao.model;

import java.io.Serializable;

public class BaseOrg implements Serializable {
    private String orgId;

    private String byOrg;

    private String orgName;

    private Integer orgType;

    private static final long serialVersionUID = 1L;

    public String getOrgId() {
        return orgId;
    }

    public BaseOrg withOrgId(String orgId) {
        this.setOrgId(orgId);
        return this;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId == null ? null : orgId.trim();
    }

    public String getByOrg() {
        return byOrg;
    }

    public BaseOrg withByOrg(String byOrg) {
        this.setByOrg(byOrg);
        return this;
    }

    public void setByOrg(String byOrg) {
        this.byOrg = byOrg == null ? null : byOrg.trim();
    }

    public String getOrgName() {
        return orgName;
    }

    public BaseOrg withOrgName(String orgName) {
        this.setOrgName(orgName);
        return this;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName == null ? null : orgName.trim();
    }

    public Integer getOrgType() {
        return orgType;
    }

    public BaseOrg withOrgType(Integer orgType) {
        this.setOrgType(orgType);
        return this;
    }

    public void setOrgType(Integer orgType) {
        this.orgType = orgType;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", orgId=").append(orgId);
        sb.append(", byOrg=").append(byOrg);
        sb.append(", orgName=").append(orgName);
        sb.append(", orgType=").append(orgType);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}