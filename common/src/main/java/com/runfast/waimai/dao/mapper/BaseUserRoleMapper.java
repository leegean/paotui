package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.BaseUserRole;
import com.runfast.waimai.dao.model.BaseUserRoleExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BaseUserRoleMapper extends IMapper<BaseUserRole, Integer, BaseUserRoleExample> {
}