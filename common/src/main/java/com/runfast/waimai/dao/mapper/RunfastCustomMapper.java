package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastCustom;
import com.runfast.waimai.dao.model.RunfastCustomExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastCustomMapper extends IMapper<RunfastCustom, Integer, RunfastCustomExample> {
}