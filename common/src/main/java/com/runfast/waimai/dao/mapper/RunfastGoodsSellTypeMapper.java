package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastGoodsSellType;
import com.runfast.waimai.dao.model.RunfastGoodsSellTypeExample;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface RunfastGoodsSellTypeMapper extends IMapper<RunfastGoodsSellType, Integer, RunfastGoodsSellTypeExample> {
    /**
     * 获取商家商品分类，同一商品的不同规格默认规格为优惠力度最大的，前端也只显示该规格
     * @param businessId
     * @return
     */
    List<RunfastGoodsSellType> listWithGoods(int businessId);
}