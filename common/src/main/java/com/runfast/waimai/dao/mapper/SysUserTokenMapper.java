package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.SysUserToken;
import com.runfast.waimai.dao.model.SysUserTokenExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysUserTokenMapper extends IMapper<SysUserToken, Integer, SysUserTokenExample> {
}