package com.runfast.waimai.dao.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RunfastActivityExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public RunfastActivityExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andBusIdIsNull() {
            addCriterion("busId is null");
            return (Criteria) this;
        }

        public Criteria andBusIdIsNotNull() {
            addCriterion("busId is not null");
            return (Criteria) this;
        }

        public Criteria andBusIdEqualTo(Integer value) {
            addCriterion("busId =", value, "busId");
            return (Criteria) this;
        }

        public Criteria andBusIdNotEqualTo(Integer value) {
            addCriterion("busId <>", value, "busId");
            return (Criteria) this;
        }

        public Criteria andBusIdGreaterThan(Integer value) {
            addCriterion("busId >", value, "busId");
            return (Criteria) this;
        }

        public Criteria andBusIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("busId >=", value, "busId");
            return (Criteria) this;
        }

        public Criteria andBusIdLessThan(Integer value) {
            addCriterion("busId <", value, "busId");
            return (Criteria) this;
        }

        public Criteria andBusIdLessThanOrEqualTo(Integer value) {
            addCriterion("busId <=", value, "busId");
            return (Criteria) this;
        }

        public Criteria andBusIdIn(List<Integer> values) {
            addCriterion("busId in", values, "busId");
            return (Criteria) this;
        }

        public Criteria andBusIdNotIn(List<Integer> values) {
            addCriterion("busId not in", values, "busId");
            return (Criteria) this;
        }

        public Criteria andBusIdBetween(Integer value1, Integer value2) {
            addCriterion("busId between", value1, value2, "busId");
            return (Criteria) this;
        }

        public Criteria andBusIdNotBetween(Integer value1, Integer value2) {
            addCriterion("busId not between", value1, value2, "busId");
            return (Criteria) this;
        }

        public Criteria andBusnameIsNull() {
            addCriterion("busname is null");
            return (Criteria) this;
        }

        public Criteria andBusnameIsNotNull() {
            addCriterion("busname is not null");
            return (Criteria) this;
        }

        public Criteria andBusnameEqualTo(String value) {
            addCriterion("busname =", value, "busname");
            return (Criteria) this;
        }

        public Criteria andBusnameNotEqualTo(String value) {
            addCriterion("busname <>", value, "busname");
            return (Criteria) this;
        }

        public Criteria andBusnameGreaterThan(String value) {
            addCriterion("busname >", value, "busname");
            return (Criteria) this;
        }

        public Criteria andBusnameGreaterThanOrEqualTo(String value) {
            addCriterion("busname >=", value, "busname");
            return (Criteria) this;
        }

        public Criteria andBusnameLessThan(String value) {
            addCriterion("busname <", value, "busname");
            return (Criteria) this;
        }

        public Criteria andBusnameLessThanOrEqualTo(String value) {
            addCriterion("busname <=", value, "busname");
            return (Criteria) this;
        }

        public Criteria andBusnameLike(String value) {
            addCriterion("busname like", value, "busname");
            return (Criteria) this;
        }

        public Criteria andBusnameNotLike(String value) {
            addCriterion("busname not like", value, "busname");
            return (Criteria) this;
        }

        public Criteria andBusnameIn(List<String> values) {
            addCriterion("busname in", values, "busname");
            return (Criteria) this;
        }

        public Criteria andBusnameNotIn(List<String> values) {
            addCriterion("busname not in", values, "busname");
            return (Criteria) this;
        }

        public Criteria andBusnameBetween(String value1, String value2) {
            addCriterion("busname between", value1, value2, "busname");
            return (Criteria) this;
        }

        public Criteria andBusnameNotBetween(String value1, String value2) {
            addCriterion("busname not between", value1, value2, "busname");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("createTime is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("createTime is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("createTime =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("createTime <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("createTime >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("createTime >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("createTime <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("createTime <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("createTime in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("createTime not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("createTime between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("createTime not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andDiscountIsNull() {
            addCriterion("discount is null");
            return (Criteria) this;
        }

        public Criteria andDiscountIsNotNull() {
            addCriterion("discount is not null");
            return (Criteria) this;
        }

        public Criteria andDiscountEqualTo(Double value) {
            addCriterion("discount =", value, "discount");
            return (Criteria) this;
        }

        public Criteria andDiscountNotEqualTo(Double value) {
            addCriterion("discount <>", value, "discount");
            return (Criteria) this;
        }

        public Criteria andDiscountGreaterThan(Double value) {
            addCriterion("discount >", value, "discount");
            return (Criteria) this;
        }

        public Criteria andDiscountGreaterThanOrEqualTo(Double value) {
            addCriterion("discount >=", value, "discount");
            return (Criteria) this;
        }

        public Criteria andDiscountLessThan(Double value) {
            addCriterion("discount <", value, "discount");
            return (Criteria) this;
        }

        public Criteria andDiscountLessThanOrEqualTo(Double value) {
            addCriterion("discount <=", value, "discount");
            return (Criteria) this;
        }

        public Criteria andDiscountIn(List<Double> values) {
            addCriterion("discount in", values, "discount");
            return (Criteria) this;
        }

        public Criteria andDiscountNotIn(List<Double> values) {
            addCriterion("discount not in", values, "discount");
            return (Criteria) this;
        }

        public Criteria andDiscountBetween(Double value1, Double value2) {
            addCriterion("discount between", value1, value2, "discount");
            return (Criteria) this;
        }

        public Criteria andDiscountNotBetween(Double value1, Double value2) {
            addCriterion("discount not between", value1, value2, "discount");
            return (Criteria) this;
        }

        public Criteria andEndTimeIsNull() {
            addCriterion("endTime is null");
            return (Criteria) this;
        }

        public Criteria andEndTimeIsNotNull() {
            addCriterion("endTime is not null");
            return (Criteria) this;
        }

        public Criteria andEndTimeEqualTo(Date value) {
            addCriterion("endTime =", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotEqualTo(Date value) {
            addCriterion("endTime <>", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeGreaterThan(Date value) {
            addCriterion("endTime >", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("endTime >=", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeLessThan(Date value) {
            addCriterion("endTime <", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeLessThanOrEqualTo(Date value) {
            addCriterion("endTime <=", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeIn(List<Date> values) {
            addCriterion("endTime in", values, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotIn(List<Date> values) {
            addCriterion("endTime not in", values, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeBetween(Date value1, Date value2) {
            addCriterion("endTime between", value1, value2, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotBetween(Date value1, Date value2) {
            addCriterion("endTime not between", value1, value2, "endTime");
            return (Criteria) this;
        }

        public Criteria andFullsIsNull() {
            addCriterion("fulls is null");
            return (Criteria) this;
        }

        public Criteria andFullsIsNotNull() {
            addCriterion("fulls is not null");
            return (Criteria) this;
        }

        public Criteria andFullsEqualTo(Double value) {
            addCriterion("fulls =", value, "fulls");
            return (Criteria) this;
        }

        public Criteria andFullsNotEqualTo(Double value) {
            addCriterion("fulls <>", value, "fulls");
            return (Criteria) this;
        }

        public Criteria andFullsGreaterThan(Double value) {
            addCriterion("fulls >", value, "fulls");
            return (Criteria) this;
        }

        public Criteria andFullsGreaterThanOrEqualTo(Double value) {
            addCriterion("fulls >=", value, "fulls");
            return (Criteria) this;
        }

        public Criteria andFullsLessThan(Double value) {
            addCriterion("fulls <", value, "fulls");
            return (Criteria) this;
        }

        public Criteria andFullsLessThanOrEqualTo(Double value) {
            addCriterion("fulls <=", value, "fulls");
            return (Criteria) this;
        }

        public Criteria andFullsIn(List<Double> values) {
            addCriterion("fulls in", values, "fulls");
            return (Criteria) this;
        }

        public Criteria andFullsNotIn(List<Double> values) {
            addCriterion("fulls not in", values, "fulls");
            return (Criteria) this;
        }

        public Criteria andFullsBetween(Double value1, Double value2) {
            addCriterion("fulls between", value1, value2, "fulls");
            return (Criteria) this;
        }

        public Criteria andFullsNotBetween(Double value1, Double value2) {
            addCriterion("fulls not between", value1, value2, "fulls");
            return (Criteria) this;
        }

        public Criteria andLesssIsNull() {
            addCriterion("lesss is null");
            return (Criteria) this;
        }

        public Criteria andLesssIsNotNull() {
            addCriterion("lesss is not null");
            return (Criteria) this;
        }

        public Criteria andLesssEqualTo(Double value) {
            addCriterion("lesss =", value, "lesss");
            return (Criteria) this;
        }

        public Criteria andLesssNotEqualTo(Double value) {
            addCriterion("lesss <>", value, "lesss");
            return (Criteria) this;
        }

        public Criteria andLesssGreaterThan(Double value) {
            addCriterion("lesss >", value, "lesss");
            return (Criteria) this;
        }

        public Criteria andLesssGreaterThanOrEqualTo(Double value) {
            addCriterion("lesss >=", value, "lesss");
            return (Criteria) this;
        }

        public Criteria andLesssLessThan(Double value) {
            addCriterion("lesss <", value, "lesss");
            return (Criteria) this;
        }

        public Criteria andLesssLessThanOrEqualTo(Double value) {
            addCriterion("lesss <=", value, "lesss");
            return (Criteria) this;
        }

        public Criteria andLesssIn(List<Double> values) {
            addCriterion("lesss in", values, "lesss");
            return (Criteria) this;
        }

        public Criteria andLesssNotIn(List<Double> values) {
            addCriterion("lesss not in", values, "lesss");
            return (Criteria) this;
        }

        public Criteria andLesssBetween(Double value1, Double value2) {
            addCriterion("lesss between", value1, value2, "lesss");
            return (Criteria) this;
        }

        public Criteria andLesssNotBetween(Double value1, Double value2) {
            addCriterion("lesss not between", value1, value2, "lesss");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andPtypeIsNull() {
            addCriterion("ptype is null");
            return (Criteria) this;
        }

        public Criteria andPtypeIsNotNull() {
            addCriterion("ptype is not null");
            return (Criteria) this;
        }

        public Criteria andPtypeEqualTo(Integer value) {
            addCriterion("ptype =", value, "ptype");
            return (Criteria) this;
        }

        public Criteria andPtypeNotEqualTo(Integer value) {
            addCriterion("ptype <>", value, "ptype");
            return (Criteria) this;
        }

        public Criteria andPtypeGreaterThan(Integer value) {
            addCriterion("ptype >", value, "ptype");
            return (Criteria) this;
        }

        public Criteria andPtypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("ptype >=", value, "ptype");
            return (Criteria) this;
        }

        public Criteria andPtypeLessThan(Integer value) {
            addCriterion("ptype <", value, "ptype");
            return (Criteria) this;
        }

        public Criteria andPtypeLessThanOrEqualTo(Integer value) {
            addCriterion("ptype <=", value, "ptype");
            return (Criteria) this;
        }

        public Criteria andPtypeIn(List<Integer> values) {
            addCriterion("ptype in", values, "ptype");
            return (Criteria) this;
        }

        public Criteria andPtypeNotIn(List<Integer> values) {
            addCriterion("ptype not in", values, "ptype");
            return (Criteria) this;
        }

        public Criteria andPtypeBetween(Integer value1, Integer value2) {
            addCriterion("ptype between", value1, value2, "ptype");
            return (Criteria) this;
        }

        public Criteria andPtypeNotBetween(Integer value1, Integer value2) {
            addCriterion("ptype not between", value1, value2, "ptype");
            return (Criteria) this;
        }

        public Criteria andStartTimeIsNull() {
            addCriterion("startTime is null");
            return (Criteria) this;
        }

        public Criteria andStartTimeIsNotNull() {
            addCriterion("startTime is not null");
            return (Criteria) this;
        }

        public Criteria andStartTimeEqualTo(Date value) {
            addCriterion("startTime =", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotEqualTo(Date value) {
            addCriterion("startTime <>", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeGreaterThan(Date value) {
            addCriterion("startTime >", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("startTime >=", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeLessThan(Date value) {
            addCriterion("startTime <", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeLessThanOrEqualTo(Date value) {
            addCriterion("startTime <=", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeIn(List<Date> values) {
            addCriterion("startTime in", values, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotIn(List<Date> values) {
            addCriterion("startTime not in", values, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeBetween(Date value1, Date value2) {
            addCriterion("startTime between", value1, value2, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotBetween(Date value1, Date value2) {
            addCriterion("startTime not between", value1, value2, "startTime");
            return (Criteria) this;
        }

        public Criteria andTypeIsNull() {
            addCriterion("type is null");
            return (Criteria) this;
        }

        public Criteria andTypeIsNotNull() {
            addCriterion("type is not null");
            return (Criteria) this;
        }

        public Criteria andTypeEqualTo(Integer value) {
            addCriterion("type =", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotEqualTo(Integer value) {
            addCriterion("type <>", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThan(Integer value) {
            addCriterion("type >", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("type >=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThan(Integer value) {
            addCriterion("type <", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThanOrEqualTo(Integer value) {
            addCriterion("type <=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeIn(List<Integer> values) {
            addCriterion("type in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotIn(List<Integer> values) {
            addCriterion("type not in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeBetween(Integer value1, Integer value2) {
            addCriterion("type between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("type not between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andGoodidsIsNull() {
            addCriterion("goodids is null");
            return (Criteria) this;
        }

        public Criteria andGoodidsIsNotNull() {
            addCriterion("goodids is not null");
            return (Criteria) this;
        }

        public Criteria andGoodidsEqualTo(String value) {
            addCriterion("goodids =", value, "goodids");
            return (Criteria) this;
        }

        public Criteria andGoodidsNotEqualTo(String value) {
            addCriterion("goodids <>", value, "goodids");
            return (Criteria) this;
        }

        public Criteria andGoodidsGreaterThan(String value) {
            addCriterion("goodids >", value, "goodids");
            return (Criteria) this;
        }

        public Criteria andGoodidsGreaterThanOrEqualTo(String value) {
            addCriterion("goodids >=", value, "goodids");
            return (Criteria) this;
        }

        public Criteria andGoodidsLessThan(String value) {
            addCriterion("goodids <", value, "goodids");
            return (Criteria) this;
        }

        public Criteria andGoodidsLessThanOrEqualTo(String value) {
            addCriterion("goodids <=", value, "goodids");
            return (Criteria) this;
        }

        public Criteria andGoodidsLike(String value) {
            addCriterion("goodids like", value, "goodids");
            return (Criteria) this;
        }

        public Criteria andGoodidsNotLike(String value) {
            addCriterion("goodids not like", value, "goodids");
            return (Criteria) this;
        }

        public Criteria andGoodidsIn(List<String> values) {
            addCriterion("goodids in", values, "goodids");
            return (Criteria) this;
        }

        public Criteria andGoodidsNotIn(List<String> values) {
            addCriterion("goodids not in", values, "goodids");
            return (Criteria) this;
        }

        public Criteria andGoodidsBetween(String value1, String value2) {
            addCriterion("goodids between", value1, value2, "goodids");
            return (Criteria) this;
        }

        public Criteria andGoodidsNotBetween(String value1, String value2) {
            addCriterion("goodids not between", value1, value2, "goodids");
            return (Criteria) this;
        }

        public Criteria andGoodsnameIsNull() {
            addCriterion("goodsname is null");
            return (Criteria) this;
        }

        public Criteria andGoodsnameIsNotNull() {
            addCriterion("goodsname is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsnameEqualTo(String value) {
            addCriterion("goodsname =", value, "goodsname");
            return (Criteria) this;
        }

        public Criteria andGoodsnameNotEqualTo(String value) {
            addCriterion("goodsname <>", value, "goodsname");
            return (Criteria) this;
        }

        public Criteria andGoodsnameGreaterThan(String value) {
            addCriterion("goodsname >", value, "goodsname");
            return (Criteria) this;
        }

        public Criteria andGoodsnameGreaterThanOrEqualTo(String value) {
            addCriterion("goodsname >=", value, "goodsname");
            return (Criteria) this;
        }

        public Criteria andGoodsnameLessThan(String value) {
            addCriterion("goodsname <", value, "goodsname");
            return (Criteria) this;
        }

        public Criteria andGoodsnameLessThanOrEqualTo(String value) {
            addCriterion("goodsname <=", value, "goodsname");
            return (Criteria) this;
        }

        public Criteria andGoodsnameLike(String value) {
            addCriterion("goodsname like", value, "goodsname");
            return (Criteria) this;
        }

        public Criteria andGoodsnameNotLike(String value) {
            addCriterion("goodsname not like", value, "goodsname");
            return (Criteria) this;
        }

        public Criteria andGoodsnameIn(List<String> values) {
            addCriterion("goodsname in", values, "goodsname");
            return (Criteria) this;
        }

        public Criteria andGoodsnameNotIn(List<String> values) {
            addCriterion("goodsname not in", values, "goodsname");
            return (Criteria) this;
        }

        public Criteria andGoodsnameBetween(String value1, String value2) {
            addCriterion("goodsname between", value1, value2, "goodsname");
            return (Criteria) this;
        }

        public Criteria andGoodsnameNotBetween(String value1, String value2) {
            addCriterion("goodsname not between", value1, value2, "goodsname");
            return (Criteria) this;
        }

        public Criteria andAgentIdIsNull() {
            addCriterion("agentId is null");
            return (Criteria) this;
        }

        public Criteria andAgentIdIsNotNull() {
            addCriterion("agentId is not null");
            return (Criteria) this;
        }

        public Criteria andAgentIdEqualTo(Integer value) {
            addCriterion("agentId =", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotEqualTo(Integer value) {
            addCriterion("agentId <>", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdGreaterThan(Integer value) {
            addCriterion("agentId >", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("agentId >=", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdLessThan(Integer value) {
            addCriterion("agentId <", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdLessThanOrEqualTo(Integer value) {
            addCriterion("agentId <=", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdIn(List<Integer> values) {
            addCriterion("agentId in", values, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotIn(List<Integer> values) {
            addCriterion("agentId not in", values, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdBetween(Integer value1, Integer value2) {
            addCriterion("agentId between", value1, value2, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotBetween(Integer value1, Integer value2) {
            addCriterion("agentId not between", value1, value2, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentNameIsNull() {
            addCriterion("agentName is null");
            return (Criteria) this;
        }

        public Criteria andAgentNameIsNotNull() {
            addCriterion("agentName is not null");
            return (Criteria) this;
        }

        public Criteria andAgentNameEqualTo(String value) {
            addCriterion("agentName =", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotEqualTo(String value) {
            addCriterion("agentName <>", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameGreaterThan(String value) {
            addCriterion("agentName >", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameGreaterThanOrEqualTo(String value) {
            addCriterion("agentName >=", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameLessThan(String value) {
            addCriterion("agentName <", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameLessThanOrEqualTo(String value) {
            addCriterion("agentName <=", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameLike(String value) {
            addCriterion("agentName like", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotLike(String value) {
            addCriterion("agentName not like", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameIn(List<String> values) {
            addCriterion("agentName in", values, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotIn(List<String> values) {
            addCriterion("agentName not in", values, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameBetween(String value1, String value2) {
            addCriterion("agentName between", value1, value2, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotBetween(String value1, String value2) {
            addCriterion("agentName not between", value1, value2, "agentName");
            return (Criteria) this;
        }

        public Criteria andStopsIsNull() {
            addCriterion("stops is null");
            return (Criteria) this;
        }

        public Criteria andStopsIsNotNull() {
            addCriterion("stops is not null");
            return (Criteria) this;
        }

        public Criteria andStopsEqualTo(Integer value) {
            addCriterion("stops =", value, "stops");
            return (Criteria) this;
        }

        public Criteria andStopsNotEqualTo(Integer value) {
            addCriterion("stops <>", value, "stops");
            return (Criteria) this;
        }

        public Criteria andStopsGreaterThan(Integer value) {
            addCriterion("stops >", value, "stops");
            return (Criteria) this;
        }

        public Criteria andStopsGreaterThanOrEqualTo(Integer value) {
            addCriterion("stops >=", value, "stops");
            return (Criteria) this;
        }

        public Criteria andStopsLessThan(Integer value) {
            addCriterion("stops <", value, "stops");
            return (Criteria) this;
        }

        public Criteria andStopsLessThanOrEqualTo(Integer value) {
            addCriterion("stops <=", value, "stops");
            return (Criteria) this;
        }

        public Criteria andStopsIn(List<Integer> values) {
            addCriterion("stops in", values, "stops");
            return (Criteria) this;
        }

        public Criteria andStopsNotIn(List<Integer> values) {
            addCriterion("stops not in", values, "stops");
            return (Criteria) this;
        }

        public Criteria andStopsBetween(Integer value1, Integer value2) {
            addCriterion("stops between", value1, value2, "stops");
            return (Criteria) this;
        }

        public Criteria andStopsNotBetween(Integer value1, Integer value2) {
            addCriterion("stops not between", value1, value2, "stops");
            return (Criteria) this;
        }

        public Criteria andGoodsIsNull() {
            addCriterion("goods is null");
            return (Criteria) this;
        }

        public Criteria andGoodsIsNotNull() {
            addCriterion("goods is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsEqualTo(String value) {
            addCriterion("goods =", value, "goods");
            return (Criteria) this;
        }

        public Criteria andGoodsNotEqualTo(String value) {
            addCriterion("goods <>", value, "goods");
            return (Criteria) this;
        }

        public Criteria andGoodsGreaterThan(String value) {
            addCriterion("goods >", value, "goods");
            return (Criteria) this;
        }

        public Criteria andGoodsGreaterThanOrEqualTo(String value) {
            addCriterion("goods >=", value, "goods");
            return (Criteria) this;
        }

        public Criteria andGoodsLessThan(String value) {
            addCriterion("goods <", value, "goods");
            return (Criteria) this;
        }

        public Criteria andGoodsLessThanOrEqualTo(String value) {
            addCriterion("goods <=", value, "goods");
            return (Criteria) this;
        }

        public Criteria andGoodsLike(String value) {
            addCriterion("goods like", value, "goods");
            return (Criteria) this;
        }

        public Criteria andGoodsNotLike(String value) {
            addCriterion("goods not like", value, "goods");
            return (Criteria) this;
        }

        public Criteria andGoodsIn(List<String> values) {
            addCriterion("goods in", values, "goods");
            return (Criteria) this;
        }

        public Criteria andGoodsNotIn(List<String> values) {
            addCriterion("goods not in", values, "goods");
            return (Criteria) this;
        }

        public Criteria andGoodsBetween(String value1, String value2) {
            addCriterion("goods between", value1, value2, "goods");
            return (Criteria) this;
        }

        public Criteria andGoodsNotBetween(String value1, String value2) {
            addCriterion("goods not between", value1, value2, "goods");
            return (Criteria) this;
        }

        public Criteria andDispriceIsNull() {
            addCriterion("disprice is null");
            return (Criteria) this;
        }

        public Criteria andDispriceIsNotNull() {
            addCriterion("disprice is not null");
            return (Criteria) this;
        }

        public Criteria andDispriceEqualTo(BigDecimal value) {
            addCriterion("disprice =", value, "disprice");
            return (Criteria) this;
        }

        public Criteria andDispriceNotEqualTo(BigDecimal value) {
            addCriterion("disprice <>", value, "disprice");
            return (Criteria) this;
        }

        public Criteria andDispriceGreaterThan(BigDecimal value) {
            addCriterion("disprice >", value, "disprice");
            return (Criteria) this;
        }

        public Criteria andDispriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("disprice >=", value, "disprice");
            return (Criteria) this;
        }

        public Criteria andDispriceLessThan(BigDecimal value) {
            addCriterion("disprice <", value, "disprice");
            return (Criteria) this;
        }

        public Criteria andDispriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("disprice <=", value, "disprice");
            return (Criteria) this;
        }

        public Criteria andDispriceIn(List<BigDecimal> values) {
            addCriterion("disprice in", values, "disprice");
            return (Criteria) this;
        }

        public Criteria andDispriceNotIn(List<BigDecimal> values) {
            addCriterion("disprice not in", values, "disprice");
            return (Criteria) this;
        }

        public Criteria andDispriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("disprice between", value1, value2, "disprice");
            return (Criteria) this;
        }

        public Criteria andDispriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("disprice not between", value1, value2, "disprice");
            return (Criteria) this;
        }

        public Criteria andStanidsnameIsNull() {
            addCriterion("stanidsname is null");
            return (Criteria) this;
        }

        public Criteria andStanidsnameIsNotNull() {
            addCriterion("stanidsname is not null");
            return (Criteria) this;
        }

        public Criteria andStanidsnameEqualTo(String value) {
            addCriterion("stanidsname =", value, "stanidsname");
            return (Criteria) this;
        }

        public Criteria andStanidsnameNotEqualTo(String value) {
            addCriterion("stanidsname <>", value, "stanidsname");
            return (Criteria) this;
        }

        public Criteria andStanidsnameGreaterThan(String value) {
            addCriterion("stanidsname >", value, "stanidsname");
            return (Criteria) this;
        }

        public Criteria andStanidsnameGreaterThanOrEqualTo(String value) {
            addCriterion("stanidsname >=", value, "stanidsname");
            return (Criteria) this;
        }

        public Criteria andStanidsnameLessThan(String value) {
            addCriterion("stanidsname <", value, "stanidsname");
            return (Criteria) this;
        }

        public Criteria andStanidsnameLessThanOrEqualTo(String value) {
            addCriterion("stanidsname <=", value, "stanidsname");
            return (Criteria) this;
        }

        public Criteria andStanidsnameLike(String value) {
            addCriterion("stanidsname like", value, "stanidsname");
            return (Criteria) this;
        }

        public Criteria andStanidsnameNotLike(String value) {
            addCriterion("stanidsname not like", value, "stanidsname");
            return (Criteria) this;
        }

        public Criteria andStanidsnameIn(List<String> values) {
            addCriterion("stanidsname in", values, "stanidsname");
            return (Criteria) this;
        }

        public Criteria andStanidsnameNotIn(List<String> values) {
            addCriterion("stanidsname not in", values, "stanidsname");
            return (Criteria) this;
        }

        public Criteria andStanidsnameBetween(String value1, String value2) {
            addCriterion("stanidsname between", value1, value2, "stanidsname");
            return (Criteria) this;
        }

        public Criteria andStanidsnameNotBetween(String value1, String value2) {
            addCriterion("stanidsname not between", value1, value2, "stanidsname");
            return (Criteria) this;
        }

        public Criteria andDeletedIsNull() {
            addCriterion("deleted is null");
            return (Criteria) this;
        }

        public Criteria andDeletedIsNotNull() {
            addCriterion("deleted is not null");
            return (Criteria) this;
        }

        public Criteria andDeletedEqualTo(Integer value) {
            addCriterion("deleted =", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedNotEqualTo(Integer value) {
            addCriterion("deleted <>", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedGreaterThan(Integer value) {
            addCriterion("deleted >", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedGreaterThanOrEqualTo(Integer value) {
            addCriterion("deleted >=", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedLessThan(Integer value) {
            addCriterion("deleted <", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedLessThanOrEqualTo(Integer value) {
            addCriterion("deleted <=", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedIn(List<Integer> values) {
            addCriterion("deleted in", values, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedNotIn(List<Integer> values) {
            addCriterion("deleted not in", values, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedBetween(Integer value1, Integer value2) {
            addCriterion("deleted between", value1, value2, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedNotBetween(Integer value1, Integer value2) {
            addCriterion("deleted not between", value1, value2, "deleted");
            return (Criteria) this;
        }

        public Criteria andSharedIsNull() {
            addCriterion("shared is null");
            return (Criteria) this;
        }

        public Criteria andSharedIsNotNull() {
            addCriterion("shared is not null");
            return (Criteria) this;
        }

        public Criteria andSharedEqualTo(Boolean value) {
            addCriterion("shared =", value, "shared");
            return (Criteria) this;
        }

        public Criteria andSharedNotEqualTo(Boolean value) {
            addCriterion("shared <>", value, "shared");
            return (Criteria) this;
        }

        public Criteria andSharedGreaterThan(Boolean value) {
            addCriterion("shared >", value, "shared");
            return (Criteria) this;
        }

        public Criteria andSharedGreaterThanOrEqualTo(Boolean value) {
            addCriterion("shared >=", value, "shared");
            return (Criteria) this;
        }

        public Criteria andSharedLessThan(Boolean value) {
            addCriterion("shared <", value, "shared");
            return (Criteria) this;
        }

        public Criteria andSharedLessThanOrEqualTo(Boolean value) {
            addCriterion("shared <=", value, "shared");
            return (Criteria) this;
        }

        public Criteria andSharedIn(List<Boolean> values) {
            addCriterion("shared in", values, "shared");
            return (Criteria) this;
        }

        public Criteria andSharedNotIn(List<Boolean> values) {
            addCriterion("shared not in", values, "shared");
            return (Criteria) this;
        }

        public Criteria andSharedBetween(Boolean value1, Boolean value2) {
            addCriterion("shared between", value1, value2, "shared");
            return (Criteria) this;
        }

        public Criteria andSharedNotBetween(Boolean value1, Boolean value2) {
            addCriterion("shared not between", value1, value2, "shared");
            return (Criteria) this;
        }

        public Criteria andWeekIsNull() {
            addCriterion("week is null");
            return (Criteria) this;
        }

        public Criteria andWeekIsNotNull() {
            addCriterion("week is not null");
            return (Criteria) this;
        }

        public Criteria andWeekEqualTo(String value) {
            addCriterion("week =", value, "week");
            return (Criteria) this;
        }

        public Criteria andWeekNotEqualTo(String value) {
            addCriterion("week <>", value, "week");
            return (Criteria) this;
        }

        public Criteria andWeekGreaterThan(String value) {
            addCriterion("week >", value, "week");
            return (Criteria) this;
        }

        public Criteria andWeekGreaterThanOrEqualTo(String value) {
            addCriterion("week >=", value, "week");
            return (Criteria) this;
        }

        public Criteria andWeekLessThan(String value) {
            addCriterion("week <", value, "week");
            return (Criteria) this;
        }

        public Criteria andWeekLessThanOrEqualTo(String value) {
            addCriterion("week <=", value, "week");
            return (Criteria) this;
        }

        public Criteria andWeekLike(String value) {
            addCriterion("week like", value, "week");
            return (Criteria) this;
        }

        public Criteria andWeekNotLike(String value) {
            addCriterion("week not like", value, "week");
            return (Criteria) this;
        }

        public Criteria andWeekIn(List<String> values) {
            addCriterion("week in", values, "week");
            return (Criteria) this;
        }

        public Criteria andWeekNotIn(List<String> values) {
            addCriterion("week not in", values, "week");
            return (Criteria) this;
        }

        public Criteria andWeekBetween(String value1, String value2) {
            addCriterion("week between", value1, value2, "week");
            return (Criteria) this;
        }

        public Criteria andWeekNotBetween(String value1, String value2) {
            addCriterion("week not between", value1, value2, "week");
            return (Criteria) this;
        }

        public Criteria andIs_limitedIsNull() {
            addCriterion("is_limited is null");
            return (Criteria) this;
        }

        public Criteria andIs_limitedIsNotNull() {
            addCriterion("is_limited is not null");
            return (Criteria) this;
        }

        public Criteria andIs_limitedEqualTo(Boolean value) {
            addCriterion("is_limited =", value, "is_limited");
            return (Criteria) this;
        }

        public Criteria andIs_limitedNotEqualTo(Boolean value) {
            addCriterion("is_limited <>", value, "is_limited");
            return (Criteria) this;
        }

        public Criteria andIs_limitedGreaterThan(Boolean value) {
            addCriterion("is_limited >", value, "is_limited");
            return (Criteria) this;
        }

        public Criteria andIs_limitedGreaterThanOrEqualTo(Boolean value) {
            addCriterion("is_limited >=", value, "is_limited");
            return (Criteria) this;
        }

        public Criteria andIs_limitedLessThan(Boolean value) {
            addCriterion("is_limited <", value, "is_limited");
            return (Criteria) this;
        }

        public Criteria andIs_limitedLessThanOrEqualTo(Boolean value) {
            addCriterion("is_limited <=", value, "is_limited");
            return (Criteria) this;
        }

        public Criteria andIs_limitedIn(List<Boolean> values) {
            addCriterion("is_limited in", values, "is_limited");
            return (Criteria) this;
        }

        public Criteria andIs_limitedNotIn(List<Boolean> values) {
            addCriterion("is_limited not in", values, "is_limited");
            return (Criteria) this;
        }

        public Criteria andIs_limitedBetween(Boolean value1, Boolean value2) {
            addCriterion("is_limited between", value1, value2, "is_limited");
            return (Criteria) this;
        }

        public Criteria andIs_limitedNotBetween(Boolean value1, Boolean value2) {
            addCriterion("is_limited not between", value1, value2, "is_limited");
            return (Criteria) this;
        }

        public Criteria andLimit_typeIsNull() {
            addCriterion("limit_type is null");
            return (Criteria) this;
        }

        public Criteria andLimit_typeIsNotNull() {
            addCriterion("limit_type is not null");
            return (Criteria) this;
        }

        public Criteria andLimit_typeEqualTo(Integer value) {
            addCriterion("limit_type =", value, "limit_type");
            return (Criteria) this;
        }

        public Criteria andLimit_typeNotEqualTo(Integer value) {
            addCriterion("limit_type <>", value, "limit_type");
            return (Criteria) this;
        }

        public Criteria andLimit_typeGreaterThan(Integer value) {
            addCriterion("limit_type >", value, "limit_type");
            return (Criteria) this;
        }

        public Criteria andLimit_typeGreaterThanOrEqualTo(Integer value) {
            addCriterion("limit_type >=", value, "limit_type");
            return (Criteria) this;
        }

        public Criteria andLimit_typeLessThan(Integer value) {
            addCriterion("limit_type <", value, "limit_type");
            return (Criteria) this;
        }

        public Criteria andLimit_typeLessThanOrEqualTo(Integer value) {
            addCriterion("limit_type <=", value, "limit_type");
            return (Criteria) this;
        }

        public Criteria andLimit_typeIn(List<Integer> values) {
            addCriterion("limit_type in", values, "limit_type");
            return (Criteria) this;
        }

        public Criteria andLimit_typeNotIn(List<Integer> values) {
            addCriterion("limit_type not in", values, "limit_type");
            return (Criteria) this;
        }

        public Criteria andLimit_typeBetween(Integer value1, Integer value2) {
            addCriterion("limit_type between", value1, value2, "limit_type");
            return (Criteria) this;
        }

        public Criteria andLimit_typeNotBetween(Integer value1, Integer value2) {
            addCriterion("limit_type not between", value1, value2, "limit_type");
            return (Criteria) this;
        }

        public Criteria andLimit_numIsNull() {
            addCriterion("limit_num is null");
            return (Criteria) this;
        }

        public Criteria andLimit_numIsNotNull() {
            addCriterion("limit_num is not null");
            return (Criteria) this;
        }

        public Criteria andLimit_numEqualTo(Integer value) {
            addCriterion("limit_num =", value, "limit_num");
            return (Criteria) this;
        }

        public Criteria andLimit_numNotEqualTo(Integer value) {
            addCriterion("limit_num <>", value, "limit_num");
            return (Criteria) this;
        }

        public Criteria andLimit_numGreaterThan(Integer value) {
            addCriterion("limit_num >", value, "limit_num");
            return (Criteria) this;
        }

        public Criteria andLimit_numGreaterThanOrEqualTo(Integer value) {
            addCriterion("limit_num >=", value, "limit_num");
            return (Criteria) this;
        }

        public Criteria andLimit_numLessThan(Integer value) {
            addCriterion("limit_num <", value, "limit_num");
            return (Criteria) this;
        }

        public Criteria andLimit_numLessThanOrEqualTo(Integer value) {
            addCriterion("limit_num <=", value, "limit_num");
            return (Criteria) this;
        }

        public Criteria andLimit_numIn(List<Integer> values) {
            addCriterion("limit_num in", values, "limit_num");
            return (Criteria) this;
        }

        public Criteria andLimit_numNotIn(List<Integer> values) {
            addCriterion("limit_num not in", values, "limit_num");
            return (Criteria) this;
        }

        public Criteria andLimit_numBetween(Integer value1, Integer value2) {
            addCriterion("limit_num between", value1, value2, "limit_num");
            return (Criteria) this;
        }

        public Criteria andLimit_numNotBetween(Integer value1, Integer value2) {
            addCriterion("limit_num not between", value1, value2, "limit_num");
            return (Criteria) this;
        }

        public Criteria andAgent_subsidyIsNull() {
            addCriterion("agent_subsidy is null");
            return (Criteria) this;
        }

        public Criteria andAgent_subsidyIsNotNull() {
            addCriterion("agent_subsidy is not null");
            return (Criteria) this;
        }

        public Criteria andAgent_subsidyEqualTo(Double value) {
            addCriterion("agent_subsidy =", value, "agent_subsidy");
            return (Criteria) this;
        }

        public Criteria andAgent_subsidyNotEqualTo(Double value) {
            addCriterion("agent_subsidy <>", value, "agent_subsidy");
            return (Criteria) this;
        }

        public Criteria andAgent_subsidyGreaterThan(Double value) {
            addCriterion("agent_subsidy >", value, "agent_subsidy");
            return (Criteria) this;
        }

        public Criteria andAgent_subsidyGreaterThanOrEqualTo(Double value) {
            addCriterion("agent_subsidy >=", value, "agent_subsidy");
            return (Criteria) this;
        }

        public Criteria andAgent_subsidyLessThan(Double value) {
            addCriterion("agent_subsidy <", value, "agent_subsidy");
            return (Criteria) this;
        }

        public Criteria andAgent_subsidyLessThanOrEqualTo(Double value) {
            addCriterion("agent_subsidy <=", value, "agent_subsidy");
            return (Criteria) this;
        }

        public Criteria andAgent_subsidyIn(List<Double> values) {
            addCriterion("agent_subsidy in", values, "agent_subsidy");
            return (Criteria) this;
        }

        public Criteria andAgent_subsidyNotIn(List<Double> values) {
            addCriterion("agent_subsidy not in", values, "agent_subsidy");
            return (Criteria) this;
        }

        public Criteria andAgent_subsidyBetween(Double value1, Double value2) {
            addCriterion("agent_subsidy between", value1, value2, "agent_subsidy");
            return (Criteria) this;
        }

        public Criteria andAgent_subsidyNotBetween(Double value1, Double value2) {
            addCriterion("agent_subsidy not between", value1, value2, "agent_subsidy");
            return (Criteria) this;
        }

        public Criteria andSpecial_typeIsNull() {
            addCriterion("special_type is null");
            return (Criteria) this;
        }

        public Criteria andSpecial_typeIsNotNull() {
            addCriterion("special_type is not null");
            return (Criteria) this;
        }

        public Criteria andSpecial_typeEqualTo(Integer value) {
            addCriterion("special_type =", value, "special_type");
            return (Criteria) this;
        }

        public Criteria andSpecial_typeNotEqualTo(Integer value) {
            addCriterion("special_type <>", value, "special_type");
            return (Criteria) this;
        }

        public Criteria andSpecial_typeGreaterThan(Integer value) {
            addCriterion("special_type >", value, "special_type");
            return (Criteria) this;
        }

        public Criteria andSpecial_typeGreaterThanOrEqualTo(Integer value) {
            addCriterion("special_type >=", value, "special_type");
            return (Criteria) this;
        }

        public Criteria andSpecial_typeLessThan(Integer value) {
            addCriterion("special_type <", value, "special_type");
            return (Criteria) this;
        }

        public Criteria andSpecial_typeLessThanOrEqualTo(Integer value) {
            addCriterion("special_type <=", value, "special_type");
            return (Criteria) this;
        }

        public Criteria andSpecial_typeIn(List<Integer> values) {
            addCriterion("special_type in", values, "special_type");
            return (Criteria) this;
        }

        public Criteria andSpecial_typeNotIn(List<Integer> values) {
            addCriterion("special_type not in", values, "special_type");
            return (Criteria) this;
        }

        public Criteria andSpecial_typeBetween(Integer value1, Integer value2) {
            addCriterion("special_type between", value1, value2, "special_type");
            return (Criteria) this;
        }

        public Criteria andSpecial_typeNotBetween(Integer value1, Integer value2) {
            addCriterion("special_type not between", value1, value2, "special_type");
            return (Criteria) this;
        }

        public Criteria andSpecial_nameIsNull() {
            addCriterion("special_name is null");
            return (Criteria) this;
        }

        public Criteria andSpecial_nameIsNotNull() {
            addCriterion("special_name is not null");
            return (Criteria) this;
        }

        public Criteria andSpecial_nameEqualTo(String value) {
            addCriterion("special_name =", value, "special_name");
            return (Criteria) this;
        }

        public Criteria andSpecial_nameNotEqualTo(String value) {
            addCriterion("special_name <>", value, "special_name");
            return (Criteria) this;
        }

        public Criteria andSpecial_nameGreaterThan(String value) {
            addCriterion("special_name >", value, "special_name");
            return (Criteria) this;
        }

        public Criteria andSpecial_nameGreaterThanOrEqualTo(String value) {
            addCriterion("special_name >=", value, "special_name");
            return (Criteria) this;
        }

        public Criteria andSpecial_nameLessThan(String value) {
            addCriterion("special_name <", value, "special_name");
            return (Criteria) this;
        }

        public Criteria andSpecial_nameLessThanOrEqualTo(String value) {
            addCriterion("special_name <=", value, "special_name");
            return (Criteria) this;
        }

        public Criteria andSpecial_nameLike(String value) {
            addCriterion("special_name like", value, "special_name");
            return (Criteria) this;
        }

        public Criteria andSpecial_nameNotLike(String value) {
            addCriterion("special_name not like", value, "special_name");
            return (Criteria) this;
        }

        public Criteria andSpecial_nameIn(List<String> values) {
            addCriterion("special_name in", values, "special_name");
            return (Criteria) this;
        }

        public Criteria andSpecial_nameNotIn(List<String> values) {
            addCriterion("special_name not in", values, "special_name");
            return (Criteria) this;
        }

        public Criteria andSpecial_nameBetween(String value1, String value2) {
            addCriterion("special_name between", value1, value2, "special_name");
            return (Criteria) this;
        }

        public Criteria andSpecial_nameNotBetween(String value1, String value2) {
            addCriterion("special_name not between", value1, value2, "special_name");
            return (Criteria) this;
        }

        public Criteria andStart1IsNull() {
            addCriterion("start1 is null");
            return (Criteria) this;
        }

        public Criteria andStart1IsNotNull() {
            addCriterion("start1 is not null");
            return (Criteria) this;
        }

        public Criteria andStart1EqualTo(Date value) {
            addCriterion("start1 =", value, "start1");
            return (Criteria) this;
        }

        public Criteria andStart1NotEqualTo(Date value) {
            addCriterion("start1 <>", value, "start1");
            return (Criteria) this;
        }

        public Criteria andStart1GreaterThan(Date value) {
            addCriterion("start1 >", value, "start1");
            return (Criteria) this;
        }

        public Criteria andStart1GreaterThanOrEqualTo(Date value) {
            addCriterion("start1 >=", value, "start1");
            return (Criteria) this;
        }

        public Criteria andStart1LessThan(Date value) {
            addCriterion("start1 <", value, "start1");
            return (Criteria) this;
        }

        public Criteria andStart1LessThanOrEqualTo(Date value) {
            addCriterion("start1 <=", value, "start1");
            return (Criteria) this;
        }

        public Criteria andStart1In(List<Date> values) {
            addCriterion("start1 in", values, "start1");
            return (Criteria) this;
        }

        public Criteria andStart1NotIn(List<Date> values) {
            addCriterion("start1 not in", values, "start1");
            return (Criteria) this;
        }

        public Criteria andStart1Between(Date value1, Date value2) {
            addCriterion("start1 between", value1, value2, "start1");
            return (Criteria) this;
        }

        public Criteria andStart1NotBetween(Date value1, Date value2) {
            addCriterion("start1 not between", value1, value2, "start1");
            return (Criteria) this;
        }

        public Criteria andStart2IsNull() {
            addCriterion("start2 is null");
            return (Criteria) this;
        }

        public Criteria andStart2IsNotNull() {
            addCriterion("start2 is not null");
            return (Criteria) this;
        }

        public Criteria andStart2EqualTo(Date value) {
            addCriterion("start2 =", value, "start2");
            return (Criteria) this;
        }

        public Criteria andStart2NotEqualTo(Date value) {
            addCriterion("start2 <>", value, "start2");
            return (Criteria) this;
        }

        public Criteria andStart2GreaterThan(Date value) {
            addCriterion("start2 >", value, "start2");
            return (Criteria) this;
        }

        public Criteria andStart2GreaterThanOrEqualTo(Date value) {
            addCriterion("start2 >=", value, "start2");
            return (Criteria) this;
        }

        public Criteria andStart2LessThan(Date value) {
            addCriterion("start2 <", value, "start2");
            return (Criteria) this;
        }

        public Criteria andStart2LessThanOrEqualTo(Date value) {
            addCriterion("start2 <=", value, "start2");
            return (Criteria) this;
        }

        public Criteria andStart2In(List<Date> values) {
            addCriterion("start2 in", values, "start2");
            return (Criteria) this;
        }

        public Criteria andStart2NotIn(List<Date> values) {
            addCriterion("start2 not in", values, "start2");
            return (Criteria) this;
        }

        public Criteria andStart2Between(Date value1, Date value2) {
            addCriterion("start2 between", value1, value2, "start2");
            return (Criteria) this;
        }

        public Criteria andStart2NotBetween(Date value1, Date value2) {
            addCriterion("start2 not between", value1, value2, "start2");
            return (Criteria) this;
        }

        public Criteria andEnd1IsNull() {
            addCriterion("end1 is null");
            return (Criteria) this;
        }

        public Criteria andEnd1IsNotNull() {
            addCriterion("end1 is not null");
            return (Criteria) this;
        }

        public Criteria andEnd1EqualTo(Date value) {
            addCriterion("end1 =", value, "end1");
            return (Criteria) this;
        }

        public Criteria andEnd1NotEqualTo(Date value) {
            addCriterion("end1 <>", value, "end1");
            return (Criteria) this;
        }

        public Criteria andEnd1GreaterThan(Date value) {
            addCriterion("end1 >", value, "end1");
            return (Criteria) this;
        }

        public Criteria andEnd1GreaterThanOrEqualTo(Date value) {
            addCriterion("end1 >=", value, "end1");
            return (Criteria) this;
        }

        public Criteria andEnd1LessThan(Date value) {
            addCriterion("end1 <", value, "end1");
            return (Criteria) this;
        }

        public Criteria andEnd1LessThanOrEqualTo(Date value) {
            addCriterion("end1 <=", value, "end1");
            return (Criteria) this;
        }

        public Criteria andEnd1In(List<Date> values) {
            addCriterion("end1 in", values, "end1");
            return (Criteria) this;
        }

        public Criteria andEnd1NotIn(List<Date> values) {
            addCriterion("end1 not in", values, "end1");
            return (Criteria) this;
        }

        public Criteria andEnd1Between(Date value1, Date value2) {
            addCriterion("end1 between", value1, value2, "end1");
            return (Criteria) this;
        }

        public Criteria andEnd1NotBetween(Date value1, Date value2) {
            addCriterion("end1 not between", value1, value2, "end1");
            return (Criteria) this;
        }

        public Criteria andEnd2IsNull() {
            addCriterion("end2 is null");
            return (Criteria) this;
        }

        public Criteria andEnd2IsNotNull() {
            addCriterion("end2 is not null");
            return (Criteria) this;
        }

        public Criteria andEnd2EqualTo(Date value) {
            addCriterion("end2 =", value, "end2");
            return (Criteria) this;
        }

        public Criteria andEnd2NotEqualTo(Date value) {
            addCriterion("end2 <>", value, "end2");
            return (Criteria) this;
        }

        public Criteria andEnd2GreaterThan(Date value) {
            addCriterion("end2 >", value, "end2");
            return (Criteria) this;
        }

        public Criteria andEnd2GreaterThanOrEqualTo(Date value) {
            addCriterion("end2 >=", value, "end2");
            return (Criteria) this;
        }

        public Criteria andEnd2LessThan(Date value) {
            addCriterion("end2 <", value, "end2");
            return (Criteria) this;
        }

        public Criteria andEnd2LessThanOrEqualTo(Date value) {
            addCriterion("end2 <=", value, "end2");
            return (Criteria) this;
        }

        public Criteria andEnd2In(List<Date> values) {
            addCriterion("end2 in", values, "end2");
            return (Criteria) this;
        }

        public Criteria andEnd2NotIn(List<Date> values) {
            addCriterion("end2 not in", values, "end2");
            return (Criteria) this;
        }

        public Criteria andEnd2Between(Date value1, Date value2) {
            addCriterion("end2 between", value1, value2, "end2");
            return (Criteria) this;
        }

        public Criteria andEnd2NotBetween(Date value1, Date value2) {
            addCriterion("end2 not between", value1, value2, "end2");
            return (Criteria) this;
        }

        public Criteria andStart3IsNull() {
            addCriterion("start3 is null");
            return (Criteria) this;
        }

        public Criteria andStart3IsNotNull() {
            addCriterion("start3 is not null");
            return (Criteria) this;
        }

        public Criteria andStart3EqualTo(Date value) {
            addCriterion("start3 =", value, "start3");
            return (Criteria) this;
        }

        public Criteria andStart3NotEqualTo(Date value) {
            addCriterion("start3 <>", value, "start3");
            return (Criteria) this;
        }

        public Criteria andStart3GreaterThan(Date value) {
            addCriterion("start3 >", value, "start3");
            return (Criteria) this;
        }

        public Criteria andStart3GreaterThanOrEqualTo(Date value) {
            addCriterion("start3 >=", value, "start3");
            return (Criteria) this;
        }

        public Criteria andStart3LessThan(Date value) {
            addCriterion("start3 <", value, "start3");
            return (Criteria) this;
        }

        public Criteria andStart3LessThanOrEqualTo(Date value) {
            addCriterion("start3 <=", value, "start3");
            return (Criteria) this;
        }

        public Criteria andStart3In(List<Date> values) {
            addCriterion("start3 in", values, "start3");
            return (Criteria) this;
        }

        public Criteria andStart3NotIn(List<Date> values) {
            addCriterion("start3 not in", values, "start3");
            return (Criteria) this;
        }

        public Criteria andStart3Between(Date value1, Date value2) {
            addCriterion("start3 between", value1, value2, "start3");
            return (Criteria) this;
        }

        public Criteria andStart3NotBetween(Date value1, Date value2) {
            addCriterion("start3 not between", value1, value2, "start3");
            return (Criteria) this;
        }

        public Criteria andEnd3IsNull() {
            addCriterion("end3 is null");
            return (Criteria) this;
        }

        public Criteria andEnd3IsNotNull() {
            addCriterion("end3 is not null");
            return (Criteria) this;
        }

        public Criteria andEnd3EqualTo(Date value) {
            addCriterion("end3 =", value, "end3");
            return (Criteria) this;
        }

        public Criteria andEnd3NotEqualTo(Date value) {
            addCriterion("end3 <>", value, "end3");
            return (Criteria) this;
        }

        public Criteria andEnd3GreaterThan(Date value) {
            addCriterion("end3 >", value, "end3");
            return (Criteria) this;
        }

        public Criteria andEnd3GreaterThanOrEqualTo(Date value) {
            addCriterion("end3 >=", value, "end3");
            return (Criteria) this;
        }

        public Criteria andEnd3LessThan(Date value) {
            addCriterion("end3 <", value, "end3");
            return (Criteria) this;
        }

        public Criteria andEnd3LessThanOrEqualTo(Date value) {
            addCriterion("end3 <=", value, "end3");
            return (Criteria) this;
        }

        public Criteria andEnd3In(List<Date> values) {
            addCriterion("end3 in", values, "end3");
            return (Criteria) this;
        }

        public Criteria andEnd3NotIn(List<Date> values) {
            addCriterion("end3 not in", values, "end3");
            return (Criteria) this;
        }

        public Criteria andEnd3Between(Date value1, Date value2) {
            addCriterion("end3 between", value1, value2, "end3");
            return (Criteria) this;
        }

        public Criteria andEnd3NotBetween(Date value1, Date value2) {
            addCriterion("end3 not between", value1, value2, "end3");
            return (Criteria) this;
        }

        public Criteria andSpecial_imgIsNull() {
            addCriterion("special_img is null");
            return (Criteria) this;
        }

        public Criteria andSpecial_imgIsNotNull() {
            addCriterion("special_img is not null");
            return (Criteria) this;
        }

        public Criteria andSpecial_imgEqualTo(String value) {
            addCriterion("special_img =", value, "special_img");
            return (Criteria) this;
        }

        public Criteria andSpecial_imgNotEqualTo(String value) {
            addCriterion("special_img <>", value, "special_img");
            return (Criteria) this;
        }

        public Criteria andSpecial_imgGreaterThan(String value) {
            addCriterion("special_img >", value, "special_img");
            return (Criteria) this;
        }

        public Criteria andSpecial_imgGreaterThanOrEqualTo(String value) {
            addCriterion("special_img >=", value, "special_img");
            return (Criteria) this;
        }

        public Criteria andSpecial_imgLessThan(String value) {
            addCriterion("special_img <", value, "special_img");
            return (Criteria) this;
        }

        public Criteria andSpecial_imgLessThanOrEqualTo(String value) {
            addCriterion("special_img <=", value, "special_img");
            return (Criteria) this;
        }

        public Criteria andSpecial_imgLike(String value) {
            addCriterion("special_img like", value, "special_img");
            return (Criteria) this;
        }

        public Criteria andSpecial_imgNotLike(String value) {
            addCriterion("special_img not like", value, "special_img");
            return (Criteria) this;
        }

        public Criteria andSpecial_imgIn(List<String> values) {
            addCriterion("special_img in", values, "special_img");
            return (Criteria) this;
        }

        public Criteria andSpecial_imgNotIn(List<String> values) {
            addCriterion("special_img not in", values, "special_img");
            return (Criteria) this;
        }

        public Criteria andSpecial_imgBetween(String value1, String value2) {
            addCriterion("special_img between", value1, value2, "special_img");
            return (Criteria) this;
        }

        public Criteria andSpecial_imgNotBetween(String value1, String value2) {
            addCriterion("special_img not between", value1, value2, "special_img");
            return (Criteria) this;
        }

        public Criteria andDiscardIsNull() {
            addCriterion("discard is null");
            return (Criteria) this;
        }

        public Criteria andDiscardIsNotNull() {
            addCriterion("discard is not null");
            return (Criteria) this;
        }

        public Criteria andDiscardEqualTo(Boolean value) {
            addCriterion("discard =", value, "discard");
            return (Criteria) this;
        }

        public Criteria andDiscardNotEqualTo(Boolean value) {
            addCriterion("discard <>", value, "discard");
            return (Criteria) this;
        }

        public Criteria andDiscardGreaterThan(Boolean value) {
            addCriterion("discard >", value, "discard");
            return (Criteria) this;
        }

        public Criteria andDiscardGreaterThanOrEqualTo(Boolean value) {
            addCriterion("discard >=", value, "discard");
            return (Criteria) this;
        }

        public Criteria andDiscardLessThan(Boolean value) {
            addCriterion("discard <", value, "discard");
            return (Criteria) this;
        }

        public Criteria andDiscardLessThanOrEqualTo(Boolean value) {
            addCriterion("discard <=", value, "discard");
            return (Criteria) this;
        }

        public Criteria andDiscardIn(List<Boolean> values) {
            addCriterion("discard in", values, "discard");
            return (Criteria) this;
        }

        public Criteria andDiscardNotIn(List<Boolean> values) {
            addCriterion("discard not in", values, "discard");
            return (Criteria) this;
        }

        public Criteria andDiscardBetween(Boolean value1, Boolean value2) {
            addCriterion("discard between", value1, value2, "discard");
            return (Criteria) this;
        }

        public Criteria andDiscardNotBetween(Boolean value1, Boolean value2) {
            addCriterion("discard not between", value1, value2, "discard");
            return (Criteria) this;
        }

        public Criteria andDiscardTimeIsNull() {
            addCriterion("discardTime is null");
            return (Criteria) this;
        }

        public Criteria andDiscardTimeIsNotNull() {
            addCriterion("discardTime is not null");
            return (Criteria) this;
        }

        public Criteria andDiscardTimeEqualTo(Date value) {
            addCriterion("discardTime =", value, "discardTime");
            return (Criteria) this;
        }

        public Criteria andDiscardTimeNotEqualTo(Date value) {
            addCriterion("discardTime <>", value, "discardTime");
            return (Criteria) this;
        }

        public Criteria andDiscardTimeGreaterThan(Date value) {
            addCriterion("discardTime >", value, "discardTime");
            return (Criteria) this;
        }

        public Criteria andDiscardTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("discardTime >=", value, "discardTime");
            return (Criteria) this;
        }

        public Criteria andDiscardTimeLessThan(Date value) {
            addCriterion("discardTime <", value, "discardTime");
            return (Criteria) this;
        }

        public Criteria andDiscardTimeLessThanOrEqualTo(Date value) {
            addCriterion("discardTime <=", value, "discardTime");
            return (Criteria) this;
        }

        public Criteria andDiscardTimeIn(List<Date> values) {
            addCriterion("discardTime in", values, "discardTime");
            return (Criteria) this;
        }

        public Criteria andDiscardTimeNotIn(List<Date> values) {
            addCriterion("discardTime not in", values, "discardTime");
            return (Criteria) this;
        }

        public Criteria andDiscardTimeBetween(Date value1, Date value2) {
            addCriterion("discardTime between", value1, value2, "discardTime");
            return (Criteria) this;
        }

        public Criteria andDiscardTimeNotBetween(Date value1, Date value2) {
            addCriterion("discardTime not between", value1, value2, "discardTime");
            return (Criteria) this;
        }

        public Criteria andCreateByIsNull() {
            addCriterion("createBy is null");
            return (Criteria) this;
        }

        public Criteria andCreateByIsNotNull() {
            addCriterion("createBy is not null");
            return (Criteria) this;
        }

        public Criteria andCreateByEqualTo(Integer value) {
            addCriterion("createBy =", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByNotEqualTo(Integer value) {
            addCriterion("createBy <>", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByGreaterThan(Integer value) {
            addCriterion("createBy >", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByGreaterThanOrEqualTo(Integer value) {
            addCriterion("createBy >=", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByLessThan(Integer value) {
            addCriterion("createBy <", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByLessThanOrEqualTo(Integer value) {
            addCriterion("createBy <=", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByIn(List<Integer> values) {
            addCriterion("createBy in", values, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByNotIn(List<Integer> values) {
            addCriterion("createBy not in", values, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByBetween(Integer value1, Integer value2) {
            addCriterion("createBy between", value1, value2, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByNotBetween(Integer value1, Integer value2) {
            addCriterion("createBy not between", value1, value2, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateTypeIsNull() {
            addCriterion("createType is null");
            return (Criteria) this;
        }

        public Criteria andCreateTypeIsNotNull() {
            addCriterion("createType is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTypeEqualTo(Integer value) {
            addCriterion("createType =", value, "createType");
            return (Criteria) this;
        }

        public Criteria andCreateTypeNotEqualTo(Integer value) {
            addCriterion("createType <>", value, "createType");
            return (Criteria) this;
        }

        public Criteria andCreateTypeGreaterThan(Integer value) {
            addCriterion("createType >", value, "createType");
            return (Criteria) this;
        }

        public Criteria andCreateTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("createType >=", value, "createType");
            return (Criteria) this;
        }

        public Criteria andCreateTypeLessThan(Integer value) {
            addCriterion("createType <", value, "createType");
            return (Criteria) this;
        }

        public Criteria andCreateTypeLessThanOrEqualTo(Integer value) {
            addCriterion("createType <=", value, "createType");
            return (Criteria) this;
        }

        public Criteria andCreateTypeIn(List<Integer> values) {
            addCriterion("createType in", values, "createType");
            return (Criteria) this;
        }

        public Criteria andCreateTypeNotIn(List<Integer> values) {
            addCriterion("createType not in", values, "createType");
            return (Criteria) this;
        }

        public Criteria andCreateTypeBetween(Integer value1, Integer value2) {
            addCriterion("createType between", value1, value2, "createType");
            return (Criteria) this;
        }

        public Criteria andCreateTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("createType not between", value1, value2, "createType");
            return (Criteria) this;
        }

        public Criteria andRedUserTypeIsNull() {
            addCriterion("redUserType is null");
            return (Criteria) this;
        }

        public Criteria andRedUserTypeIsNotNull() {
            addCriterion("redUserType is not null");
            return (Criteria) this;
        }

        public Criteria andRedUserTypeEqualTo(Integer value) {
            addCriterion("redUserType =", value, "redUserType");
            return (Criteria) this;
        }

        public Criteria andRedUserTypeNotEqualTo(Integer value) {
            addCriterion("redUserType <>", value, "redUserType");
            return (Criteria) this;
        }

        public Criteria andRedUserTypeGreaterThan(Integer value) {
            addCriterion("redUserType >", value, "redUserType");
            return (Criteria) this;
        }

        public Criteria andRedUserTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("redUserType >=", value, "redUserType");
            return (Criteria) this;
        }

        public Criteria andRedUserTypeLessThan(Integer value) {
            addCriterion("redUserType <", value, "redUserType");
            return (Criteria) this;
        }

        public Criteria andRedUserTypeLessThanOrEqualTo(Integer value) {
            addCriterion("redUserType <=", value, "redUserType");
            return (Criteria) this;
        }

        public Criteria andRedUserTypeIn(List<Integer> values) {
            addCriterion("redUserType in", values, "redUserType");
            return (Criteria) this;
        }

        public Criteria andRedUserTypeNotIn(List<Integer> values) {
            addCriterion("redUserType not in", values, "redUserType");
            return (Criteria) this;
        }

        public Criteria andRedUserTypeBetween(Integer value1, Integer value2) {
            addCriterion("redUserType between", value1, value2, "redUserType");
            return (Criteria) this;
        }

        public Criteria andRedUserTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("redUserType not between", value1, value2, "redUserType");
            return (Criteria) this;
        }

        public Criteria andRedDayIsNull() {
            addCriterion("redDay is null");
            return (Criteria) this;
        }

        public Criteria andRedDayIsNotNull() {
            addCriterion("redDay is not null");
            return (Criteria) this;
        }

        public Criteria andRedDayEqualTo(Integer value) {
            addCriterion("redDay =", value, "redDay");
            return (Criteria) this;
        }

        public Criteria andRedDayNotEqualTo(Integer value) {
            addCriterion("redDay <>", value, "redDay");
            return (Criteria) this;
        }

        public Criteria andRedDayGreaterThan(Integer value) {
            addCriterion("redDay >", value, "redDay");
            return (Criteria) this;
        }

        public Criteria andRedDayGreaterThanOrEqualTo(Integer value) {
            addCriterion("redDay >=", value, "redDay");
            return (Criteria) this;
        }

        public Criteria andRedDayLessThan(Integer value) {
            addCriterion("redDay <", value, "redDay");
            return (Criteria) this;
        }

        public Criteria andRedDayLessThanOrEqualTo(Integer value) {
            addCriterion("redDay <=", value, "redDay");
            return (Criteria) this;
        }

        public Criteria andRedDayIn(List<Integer> values) {
            addCriterion("redDay in", values, "redDay");
            return (Criteria) this;
        }

        public Criteria andRedDayNotIn(List<Integer> values) {
            addCriterion("redDay not in", values, "redDay");
            return (Criteria) this;
        }

        public Criteria andRedDayBetween(Integer value1, Integer value2) {
            addCriterion("redDay between", value1, value2, "redDay");
            return (Criteria) this;
        }

        public Criteria andRedDayNotBetween(Integer value1, Integer value2) {
            addCriterion("redDay not between", value1, value2, "redDay");
            return (Criteria) this;
        }

        public Criteria andRedAmountIsNull() {
            addCriterion("redAmount is null");
            return (Criteria) this;
        }

        public Criteria andRedAmountIsNotNull() {
            addCriterion("redAmount is not null");
            return (Criteria) this;
        }

        public Criteria andRedAmountEqualTo(String value) {
            addCriterion("redAmount =", value, "redAmount");
            return (Criteria) this;
        }

        public Criteria andRedAmountNotEqualTo(String value) {
            addCriterion("redAmount <>", value, "redAmount");
            return (Criteria) this;
        }

        public Criteria andRedAmountGreaterThan(String value) {
            addCriterion("redAmount >", value, "redAmount");
            return (Criteria) this;
        }

        public Criteria andRedAmountGreaterThanOrEqualTo(String value) {
            addCriterion("redAmount >=", value, "redAmount");
            return (Criteria) this;
        }

        public Criteria andRedAmountLessThan(String value) {
            addCriterion("redAmount <", value, "redAmount");
            return (Criteria) this;
        }

        public Criteria andRedAmountLessThanOrEqualTo(String value) {
            addCriterion("redAmount <=", value, "redAmount");
            return (Criteria) this;
        }

        public Criteria andRedAmountLike(String value) {
            addCriterion("redAmount like", value, "redAmount");
            return (Criteria) this;
        }

        public Criteria andRedAmountNotLike(String value) {
            addCriterion("redAmount not like", value, "redAmount");
            return (Criteria) this;
        }

        public Criteria andRedAmountIn(List<String> values) {
            addCriterion("redAmount in", values, "redAmount");
            return (Criteria) this;
        }

        public Criteria andRedAmountNotIn(List<String> values) {
            addCriterion("redAmount not in", values, "redAmount");
            return (Criteria) this;
        }

        public Criteria andRedAmountBetween(String value1, String value2) {
            addCriterion("redAmount between", value1, value2, "redAmount");
            return (Criteria) this;
        }

        public Criteria andRedAmountNotBetween(String value1, String value2) {
            addCriterion("redAmount not between", value1, value2, "redAmount");
            return (Criteria) this;
        }

        public Criteria andRedLimitTypeIsNull() {
            addCriterion("redLimitType is null");
            return (Criteria) this;
        }

        public Criteria andRedLimitTypeIsNotNull() {
            addCriterion("redLimitType is not null");
            return (Criteria) this;
        }

        public Criteria andRedLimitTypeEqualTo(Integer value) {
            addCriterion("redLimitType =", value, "redLimitType");
            return (Criteria) this;
        }

        public Criteria andRedLimitTypeNotEqualTo(Integer value) {
            addCriterion("redLimitType <>", value, "redLimitType");
            return (Criteria) this;
        }

        public Criteria andRedLimitTypeGreaterThan(Integer value) {
            addCriterion("redLimitType >", value, "redLimitType");
            return (Criteria) this;
        }

        public Criteria andRedLimitTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("redLimitType >=", value, "redLimitType");
            return (Criteria) this;
        }

        public Criteria andRedLimitTypeLessThan(Integer value) {
            addCriterion("redLimitType <", value, "redLimitType");
            return (Criteria) this;
        }

        public Criteria andRedLimitTypeLessThanOrEqualTo(Integer value) {
            addCriterion("redLimitType <=", value, "redLimitType");
            return (Criteria) this;
        }

        public Criteria andRedLimitTypeIn(List<Integer> values) {
            addCriterion("redLimitType in", values, "redLimitType");
            return (Criteria) this;
        }

        public Criteria andRedLimitTypeNotIn(List<Integer> values) {
            addCriterion("redLimitType not in", values, "redLimitType");
            return (Criteria) this;
        }

        public Criteria andRedLimitTypeBetween(Integer value1, Integer value2) {
            addCriterion("redLimitType between", value1, value2, "redLimitType");
            return (Criteria) this;
        }

        public Criteria andRedLimitTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("redLimitType not between", value1, value2, "redLimitType");
            return (Criteria) this;
        }

        public Criteria andRedNumIsNull() {
            addCriterion("redNum is null");
            return (Criteria) this;
        }

        public Criteria andRedNumIsNotNull() {
            addCriterion("redNum is not null");
            return (Criteria) this;
        }

        public Criteria andRedNumEqualTo(Integer value) {
            addCriterion("redNum =", value, "redNum");
            return (Criteria) this;
        }

        public Criteria andRedNumNotEqualTo(Integer value) {
            addCriterion("redNum <>", value, "redNum");
            return (Criteria) this;
        }

        public Criteria andRedNumGreaterThan(Integer value) {
            addCriterion("redNum >", value, "redNum");
            return (Criteria) this;
        }

        public Criteria andRedNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("redNum >=", value, "redNum");
            return (Criteria) this;
        }

        public Criteria andRedNumLessThan(Integer value) {
            addCriterion("redNum <", value, "redNum");
            return (Criteria) this;
        }

        public Criteria andRedNumLessThanOrEqualTo(Integer value) {
            addCriterion("redNum <=", value, "redNum");
            return (Criteria) this;
        }

        public Criteria andRedNumIn(List<Integer> values) {
            addCriterion("redNum in", values, "redNum");
            return (Criteria) this;
        }

        public Criteria andRedNumNotIn(List<Integer> values) {
            addCriterion("redNum not in", values, "redNum");
            return (Criteria) this;
        }

        public Criteria andRedNumBetween(Integer value1, Integer value2) {
            addCriterion("redNum between", value1, value2, "redNum");
            return (Criteria) this;
        }

        public Criteria andRedNumNotBetween(Integer value1, Integer value2) {
            addCriterion("redNum not between", value1, value2, "redNum");
            return (Criteria) this;
        }

        public Criteria andRedPersonLimitTypeIsNull() {
            addCriterion("redPersonLimitType is null");
            return (Criteria) this;
        }

        public Criteria andRedPersonLimitTypeIsNotNull() {
            addCriterion("redPersonLimitType is not null");
            return (Criteria) this;
        }

        public Criteria andRedPersonLimitTypeEqualTo(Integer value) {
            addCriterion("redPersonLimitType =", value, "redPersonLimitType");
            return (Criteria) this;
        }

        public Criteria andRedPersonLimitTypeNotEqualTo(Integer value) {
            addCriterion("redPersonLimitType <>", value, "redPersonLimitType");
            return (Criteria) this;
        }

        public Criteria andRedPersonLimitTypeGreaterThan(Integer value) {
            addCriterion("redPersonLimitType >", value, "redPersonLimitType");
            return (Criteria) this;
        }

        public Criteria andRedPersonLimitTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("redPersonLimitType >=", value, "redPersonLimitType");
            return (Criteria) this;
        }

        public Criteria andRedPersonLimitTypeLessThan(Integer value) {
            addCriterion("redPersonLimitType <", value, "redPersonLimitType");
            return (Criteria) this;
        }

        public Criteria andRedPersonLimitTypeLessThanOrEqualTo(Integer value) {
            addCriterion("redPersonLimitType <=", value, "redPersonLimitType");
            return (Criteria) this;
        }

        public Criteria andRedPersonLimitTypeIn(List<Integer> values) {
            addCriterion("redPersonLimitType in", values, "redPersonLimitType");
            return (Criteria) this;
        }

        public Criteria andRedPersonLimitTypeNotIn(List<Integer> values) {
            addCriterion("redPersonLimitType not in", values, "redPersonLimitType");
            return (Criteria) this;
        }

        public Criteria andRedPersonLimitTypeBetween(Integer value1, Integer value2) {
            addCriterion("redPersonLimitType between", value1, value2, "redPersonLimitType");
            return (Criteria) this;
        }

        public Criteria andRedPersonLimitTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("redPersonLimitType not between", value1, value2, "redPersonLimitType");
            return (Criteria) this;
        }

        public Criteria andRedGetNumIsNull() {
            addCriterion("redGetNum is null");
            return (Criteria) this;
        }

        public Criteria andRedGetNumIsNotNull() {
            addCriterion("redGetNum is not null");
            return (Criteria) this;
        }

        public Criteria andRedGetNumEqualTo(Integer value) {
            addCriterion("redGetNum =", value, "redGetNum");
            return (Criteria) this;
        }

        public Criteria andRedGetNumNotEqualTo(Integer value) {
            addCriterion("redGetNum <>", value, "redGetNum");
            return (Criteria) this;
        }

        public Criteria andRedGetNumGreaterThan(Integer value) {
            addCriterion("redGetNum >", value, "redGetNum");
            return (Criteria) this;
        }

        public Criteria andRedGetNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("redGetNum >=", value, "redGetNum");
            return (Criteria) this;
        }

        public Criteria andRedGetNumLessThan(Integer value) {
            addCriterion("redGetNum <", value, "redGetNum");
            return (Criteria) this;
        }

        public Criteria andRedGetNumLessThanOrEqualTo(Integer value) {
            addCriterion("redGetNum <=", value, "redGetNum");
            return (Criteria) this;
        }

        public Criteria andRedGetNumIn(List<Integer> values) {
            addCriterion("redGetNum in", values, "redGetNum");
            return (Criteria) this;
        }

        public Criteria andRedGetNumNotIn(List<Integer> values) {
            addCriterion("redGetNum not in", values, "redGetNum");
            return (Criteria) this;
        }

        public Criteria andRedGetNumBetween(Integer value1, Integer value2) {
            addCriterion("redGetNum between", value1, value2, "redGetNum");
            return (Criteria) this;
        }

        public Criteria andRedGetNumNotBetween(Integer value1, Integer value2) {
            addCriterion("redGetNum not between", value1, value2, "redGetNum");
            return (Criteria) this;
        }

        public Criteria andRedDayGetNumIsNull() {
            addCriterion("redDayGetNum is null");
            return (Criteria) this;
        }

        public Criteria andRedDayGetNumIsNotNull() {
            addCriterion("redDayGetNum is not null");
            return (Criteria) this;
        }

        public Criteria andRedDayGetNumEqualTo(Integer value) {
            addCriterion("redDayGetNum =", value, "redDayGetNum");
            return (Criteria) this;
        }

        public Criteria andRedDayGetNumNotEqualTo(Integer value) {
            addCriterion("redDayGetNum <>", value, "redDayGetNum");
            return (Criteria) this;
        }

        public Criteria andRedDayGetNumGreaterThan(Integer value) {
            addCriterion("redDayGetNum >", value, "redDayGetNum");
            return (Criteria) this;
        }

        public Criteria andRedDayGetNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("redDayGetNum >=", value, "redDayGetNum");
            return (Criteria) this;
        }

        public Criteria andRedDayGetNumLessThan(Integer value) {
            addCriterion("redDayGetNum <", value, "redDayGetNum");
            return (Criteria) this;
        }

        public Criteria andRedDayGetNumLessThanOrEqualTo(Integer value) {
            addCriterion("redDayGetNum <=", value, "redDayGetNum");
            return (Criteria) this;
        }

        public Criteria andRedDayGetNumIn(List<Integer> values) {
            addCriterion("redDayGetNum in", values, "redDayGetNum");
            return (Criteria) this;
        }

        public Criteria andRedDayGetNumNotIn(List<Integer> values) {
            addCriterion("redDayGetNum not in", values, "redDayGetNum");
            return (Criteria) this;
        }

        public Criteria andRedDayGetNumBetween(Integer value1, Integer value2) {
            addCriterion("redDayGetNum between", value1, value2, "redDayGetNum");
            return (Criteria) this;
        }

        public Criteria andRedDayGetNumNotBetween(Integer value1, Integer value2) {
            addCriterion("redDayGetNum not between", value1, value2, "redDayGetNum");
            return (Criteria) this;
        }

        public Criteria andRedDayGetTimeIsNull() {
            addCriterion("redDayGetTime is null");
            return (Criteria) this;
        }

        public Criteria andRedDayGetTimeIsNotNull() {
            addCriterion("redDayGetTime is not null");
            return (Criteria) this;
        }

        public Criteria andRedDayGetTimeEqualTo(Date value) {
            addCriterion("redDayGetTime =", value, "redDayGetTime");
            return (Criteria) this;
        }

        public Criteria andRedDayGetTimeNotEqualTo(Date value) {
            addCriterion("redDayGetTime <>", value, "redDayGetTime");
            return (Criteria) this;
        }

        public Criteria andRedDayGetTimeGreaterThan(Date value) {
            addCriterion("redDayGetTime >", value, "redDayGetTime");
            return (Criteria) this;
        }

        public Criteria andRedDayGetTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("redDayGetTime >=", value, "redDayGetTime");
            return (Criteria) this;
        }

        public Criteria andRedDayGetTimeLessThan(Date value) {
            addCriterion("redDayGetTime <", value, "redDayGetTime");
            return (Criteria) this;
        }

        public Criteria andRedDayGetTimeLessThanOrEqualTo(Date value) {
            addCriterion("redDayGetTime <=", value, "redDayGetTime");
            return (Criteria) this;
        }

        public Criteria andRedDayGetTimeIn(List<Date> values) {
            addCriterion("redDayGetTime in", values, "redDayGetTime");
            return (Criteria) this;
        }

        public Criteria andRedDayGetTimeNotIn(List<Date> values) {
            addCriterion("redDayGetTime not in", values, "redDayGetTime");
            return (Criteria) this;
        }

        public Criteria andRedDayGetTimeBetween(Date value1, Date value2) {
            addCriterion("redDayGetTime between", value1, value2, "redDayGetTime");
            return (Criteria) this;
        }

        public Criteria andRedDayGetTimeNotBetween(Date value1, Date value2) {
            addCriterion("redDayGetTime not between", value1, value2, "redDayGetTime");
            return (Criteria) this;
        }

        public Criteria andRedValidDayIsNull() {
            addCriterion("redValidDay is null");
            return (Criteria) this;
        }

        public Criteria andRedValidDayIsNotNull() {
            addCriterion("redValidDay is not null");
            return (Criteria) this;
        }

        public Criteria andRedValidDayEqualTo(Integer value) {
            addCriterion("redValidDay =", value, "redValidDay");
            return (Criteria) this;
        }

        public Criteria andRedValidDayNotEqualTo(Integer value) {
            addCriterion("redValidDay <>", value, "redValidDay");
            return (Criteria) this;
        }

        public Criteria andRedValidDayGreaterThan(Integer value) {
            addCriterion("redValidDay >", value, "redValidDay");
            return (Criteria) this;
        }

        public Criteria andRedValidDayGreaterThanOrEqualTo(Integer value) {
            addCriterion("redValidDay >=", value, "redValidDay");
            return (Criteria) this;
        }

        public Criteria andRedValidDayLessThan(Integer value) {
            addCriterion("redValidDay <", value, "redValidDay");
            return (Criteria) this;
        }

        public Criteria andRedValidDayLessThanOrEqualTo(Integer value) {
            addCriterion("redValidDay <=", value, "redValidDay");
            return (Criteria) this;
        }

        public Criteria andRedValidDayIn(List<Integer> values) {
            addCriterion("redValidDay in", values, "redValidDay");
            return (Criteria) this;
        }

        public Criteria andRedValidDayNotIn(List<Integer> values) {
            addCriterion("redValidDay not in", values, "redValidDay");
            return (Criteria) this;
        }

        public Criteria andRedValidDayBetween(Integer value1, Integer value2) {
            addCriterion("redValidDay between", value1, value2, "redValidDay");
            return (Criteria) this;
        }

        public Criteria andRedValidDayNotBetween(Integer value1, Integer value2) {
            addCriterion("redValidDay not between", value1, value2, "redValidDay");
            return (Criteria) this;
        }

        public Criteria andFullReturnIsNull() {
            addCriterion("fullReturn is null");
            return (Criteria) this;
        }

        public Criteria andFullReturnIsNotNull() {
            addCriterion("fullReturn is not null");
            return (Criteria) this;
        }

        public Criteria andFullReturnEqualTo(Double value) {
            addCriterion("fullReturn =", value, "fullReturn");
            return (Criteria) this;
        }

        public Criteria andFullReturnNotEqualTo(Double value) {
            addCriterion("fullReturn <>", value, "fullReturn");
            return (Criteria) this;
        }

        public Criteria andFullReturnGreaterThan(Double value) {
            addCriterion("fullReturn >", value, "fullReturn");
            return (Criteria) this;
        }

        public Criteria andFullReturnGreaterThanOrEqualTo(Double value) {
            addCriterion("fullReturn >=", value, "fullReturn");
            return (Criteria) this;
        }

        public Criteria andFullReturnLessThan(Double value) {
            addCriterion("fullReturn <", value, "fullReturn");
            return (Criteria) this;
        }

        public Criteria andFullReturnLessThanOrEqualTo(Double value) {
            addCriterion("fullReturn <=", value, "fullReturn");
            return (Criteria) this;
        }

        public Criteria andFullReturnIn(List<Double> values) {
            addCriterion("fullReturn in", values, "fullReturn");
            return (Criteria) this;
        }

        public Criteria andFullReturnNotIn(List<Double> values) {
            addCriterion("fullReturn not in", values, "fullReturn");
            return (Criteria) this;
        }

        public Criteria andFullReturnBetween(Double value1, Double value2) {
            addCriterion("fullReturn between", value1, value2, "fullReturn");
            return (Criteria) this;
        }

        public Criteria andFullReturnNotBetween(Double value1, Double value2) {
            addCriterion("fullReturn not between", value1, value2, "fullReturn");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}