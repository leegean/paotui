package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastRefund;
import com.runfast.waimai.dao.model.RunfastRefundExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastRefundMapper extends IMapper<RunfastRefund, Integer, RunfastRefundExample> {
}