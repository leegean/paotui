package com.runfast.waimai.dao.model;

import com.runfast.common.entity.validate.group.Create;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

public class RunfastDriverApplication implements Serializable {
    private Integer id;

    /**
     * 姓名
     */
    @NotBlank(groups = {Create.class})
    private String name;

    /**
     * 性别（1：男，0：女）
     */
    @NotNull(groups = {Create.class})
    private Integer gender;

    /**
     * 身份证
     */
    @NotBlank(groups = {Create.class})
    private String idNumber;

    /**
     * 年龄
     */
    @NotNull(groups = {Create.class})
    private Integer age;

    /**
     * 手机号
     */
    @NotBlank(groups = {Create.class})
    private String mobile;

    /**
     * 省份
     */
    @NotBlank(groups = {Create.class})
    private String province;

    /**
     * 城市
     */
    @NotBlank(groups = {Create.class})
    private String city;

    /**
     * 是否有物流或外卖送餐经验（1：是，0：否）
     */
    @NotNull(groups = {Create.class})
    private Integer experienced;

    /**
     * 工作意向（0：全职，1：兼职）
     */
    @NotNull(groups = {Create.class})
    private Integer intention;

    private Date createTime;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public RunfastDriverApplication withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public RunfastDriverApplication withName(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getGender() {
        return gender;
    }

    public RunfastDriverApplication withGender(Integer gender) {
        this.setGender(gender);
        return this;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public RunfastDriverApplication withIdNumber(String idNumber) {
        this.setIdNumber(idNumber);
        return this;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber == null ? null : idNumber.trim();
    }

    public Integer getAge() {
        return age;
    }

    public RunfastDriverApplication withAge(Integer age) {
        this.setAge(age);
        return this;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getMobile() {
        return mobile;
    }

    public RunfastDriverApplication withMobile(String mobile) {
        this.setMobile(mobile);
        return this;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public String getProvince() {
        return province;
    }

    public RunfastDriverApplication withProvince(String province) {
        this.setProvince(province);
        return this;
    }

    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    public String getCity() {
        return city;
    }

    public RunfastDriverApplication withCity(String city) {
        this.setCity(city);
        return this;
    }

    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    public Integer getExperienced() {
        return experienced;
    }

    public RunfastDriverApplication withExperienced(Integer experienced) {
        this.setExperienced(experienced);
        return this;
    }

    public void setExperienced(Integer experienced) {
        this.experienced = experienced;
    }

    public Integer getIntention() {
        return intention;
    }

    public RunfastDriverApplication withIntention(Integer intention) {
        this.setIntention(intention);
        return this;
    }

    public void setIntention(Integer intention) {
        this.intention = intention;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public RunfastDriverApplication withCreateTime(Date createTime) {
        this.setCreateTime(createTime);
        return this;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", gender=").append(gender);
        sb.append(", idNumber=").append(idNumber);
        sb.append(", age=").append(age);
        sb.append(", mobile=").append(mobile);
        sb.append(", province=").append(province);
        sb.append(", city=").append(city);
        sb.append(", experienced=").append(experienced);
        sb.append(", intention=").append(intention);
        sb.append(", createTime=").append(createTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}