package com.runfast.waimai.dao.model;

import java.io.Serializable;
import java.util.Date;

public class RunfastGoodsSellOutStatus implements Serializable {
    private Integer id;

    private Integer businessId;

    private String businessName;

    private Date createTime;

    private Integer goodsSellId;

    private String goodsSellName;

    private String goodsSellRecordCode;

    private Integer goodsSellRecordId;

    private String goodsSellRecordName;

    private Integer operationId;

    private String operationName;

    private Integer sort;

    private Integer type;

    private String statStr;

    /**
     * 状态描述，比如异常状态原因
     */
    private String description;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public RunfastGoodsSellOutStatus withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBusinessId() {
        return businessId;
    }

    public RunfastGoodsSellOutStatus withBusinessId(Integer businessId) {
        this.setBusinessId(businessId);
        return this;
    }

    public void setBusinessId(Integer businessId) {
        this.businessId = businessId;
    }

    public String getBusinessName() {
        return businessName;
    }

    public RunfastGoodsSellOutStatus withBusinessName(String businessName) {
        this.setBusinessName(businessName);
        return this;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName == null ? null : businessName.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public RunfastGoodsSellOutStatus withCreateTime(Date createTime) {
        this.setCreateTime(createTime);
        return this;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getGoodsSellId() {
        return goodsSellId;
    }

    public RunfastGoodsSellOutStatus withGoodsSellId(Integer goodsSellId) {
        this.setGoodsSellId(goodsSellId);
        return this;
    }

    public void setGoodsSellId(Integer goodsSellId) {
        this.goodsSellId = goodsSellId;
    }

    public String getGoodsSellName() {
        return goodsSellName;
    }

    public RunfastGoodsSellOutStatus withGoodsSellName(String goodsSellName) {
        this.setGoodsSellName(goodsSellName);
        return this;
    }

    public void setGoodsSellName(String goodsSellName) {
        this.goodsSellName = goodsSellName == null ? null : goodsSellName.trim();
    }

    public String getGoodsSellRecordCode() {
        return goodsSellRecordCode;
    }

    public RunfastGoodsSellOutStatus withGoodsSellRecordCode(String goodsSellRecordCode) {
        this.setGoodsSellRecordCode(goodsSellRecordCode);
        return this;
    }

    public void setGoodsSellRecordCode(String goodsSellRecordCode) {
        this.goodsSellRecordCode = goodsSellRecordCode == null ? null : goodsSellRecordCode.trim();
    }

    public Integer getGoodsSellRecordId() {
        return goodsSellRecordId;
    }

    public RunfastGoodsSellOutStatus withGoodsSellRecordId(Integer goodsSellRecordId) {
        this.setGoodsSellRecordId(goodsSellRecordId);
        return this;
    }

    public void setGoodsSellRecordId(Integer goodsSellRecordId) {
        this.goodsSellRecordId = goodsSellRecordId;
    }

    public String getGoodsSellRecordName() {
        return goodsSellRecordName;
    }

    public RunfastGoodsSellOutStatus withGoodsSellRecordName(String goodsSellRecordName) {
        this.setGoodsSellRecordName(goodsSellRecordName);
        return this;
    }

    public void setGoodsSellRecordName(String goodsSellRecordName) {
        this.goodsSellRecordName = goodsSellRecordName == null ? null : goodsSellRecordName.trim();
    }

    public Integer getOperationId() {
        return operationId;
    }

    public RunfastGoodsSellOutStatus withOperationId(Integer operationId) {
        this.setOperationId(operationId);
        return this;
    }

    public void setOperationId(Integer operationId) {
        this.operationId = operationId;
    }

    public String getOperationName() {
        return operationName;
    }

    public RunfastGoodsSellOutStatus withOperationName(String operationName) {
        this.setOperationName(operationName);
        return this;
    }

    public void setOperationName(String operationName) {
        this.operationName = operationName == null ? null : operationName.trim();
    }

    public Integer getSort() {
        return sort;
    }

    public RunfastGoodsSellOutStatus withSort(Integer sort) {
        this.setSort(sort);
        return this;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getType() {
        return type;
    }

    public RunfastGoodsSellOutStatus withType(Integer type) {
        this.setType(type);
        return this;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getStatStr() {
        return statStr;
    }

    public RunfastGoodsSellOutStatus withStatStr(String statStr) {
        this.setStatStr(statStr);
        return this;
    }

    public void setStatStr(String statStr) {
        this.statStr = statStr == null ? null : statStr.trim();
    }

    public String getDescription() {
        return description;
    }

    public RunfastGoodsSellOutStatus withDescription(String description) {
        this.setDescription(description);
        return this;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", businessId=").append(businessId);
        sb.append(", businessName=").append(businessName);
        sb.append(", createTime=").append(createTime);
        sb.append(", goodsSellId=").append(goodsSellId);
        sb.append(", goodsSellName=").append(goodsSellName);
        sb.append(", goodsSellRecordCode=").append(goodsSellRecordCode);
        sb.append(", goodsSellRecordId=").append(goodsSellRecordId);
        sb.append(", goodsSellRecordName=").append(goodsSellRecordName);
        sb.append(", operationId=").append(operationId);
        sb.append(", operationName=").append(operationName);
        sb.append(", sort=").append(sort);
        sb.append(", type=").append(type);
        sb.append(", statStr=").append(statStr);
        sb.append(", description=").append(description);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}