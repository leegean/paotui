package com.runfast.waimai.dao.model;

import java.io.Serializable;

public class RunfastFullLess implements Serializable {
    private Integer id;

    private Double full;

    private Double less;

    private Double subsidy;

    private Integer activity_id;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public RunfastFullLess withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getFull() {
        return full;
    }

    public RunfastFullLess withFull(Double full) {
        this.setFull(full);
        return this;
    }

    public void setFull(Double full) {
        this.full = full;
    }

    public Double getLess() {
        return less;
    }

    public RunfastFullLess withLess(Double less) {
        this.setLess(less);
        return this;
    }

    public void setLess(Double less) {
        this.less = less;
    }

    public Double getSubsidy() {
        return subsidy;
    }

    public RunfastFullLess withSubsidy(Double subsidy) {
        this.setSubsidy(subsidy);
        return this;
    }

    public void setSubsidy(Double subsidy) {
        this.subsidy = subsidy;
    }

    public Integer getActivity_id() {
        return activity_id;
    }

    public RunfastFullLess withActivity_id(Integer activity_id) {
        this.setActivity_id(activity_id);
        return this;
    }

    public void setActivity_id(Integer activity_id) {
        this.activity_id = activity_id;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", full=").append(full);
        sb.append(", less=").append(less);
        sb.append(", subsidy=").append(subsidy);
        sb.append(", activity_id=").append(activity_id);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}