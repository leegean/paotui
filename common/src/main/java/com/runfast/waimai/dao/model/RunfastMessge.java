package com.runfast.waimai.dao.model;

import java.io.Serializable;
import java.util.Date;

public class RunfastMessge implements Serializable {
    private Integer id;

    private String content;

    private Date createTime;

    private String title;

    private Integer type;

    private Integer userId;

    private String userName;

    private String cityId;

    private String cityName;

    private String countyId;

    private String countyName;

    private Date end;

    private Date start;

    private String townId;

    private String townName;

    private Integer agentId;

    private String agentName;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public RunfastMessge withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public RunfastMessge withContent(String content) {
        this.setContent(content);
        return this;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public RunfastMessge withCreateTime(Date createTime) {
        this.setCreateTime(createTime);
        return this;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getTitle() {
        return title;
    }

    public RunfastMessge withTitle(String title) {
        this.setTitle(title);
        return this;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public Integer getType() {
        return type;
    }

    public RunfastMessge withType(Integer type) {
        this.setType(type);
        return this;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getUserId() {
        return userId;
    }

    public RunfastMessge withUserId(Integer userId) {
        this.setUserId(userId);
        return this;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public RunfastMessge withUserName(String userName) {
        this.setUserName(userName);
        return this;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getCityId() {
        return cityId;
    }

    public RunfastMessge withCityId(String cityId) {
        this.setCityId(cityId);
        return this;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId == null ? null : cityId.trim();
    }

    public String getCityName() {
        return cityName;
    }

    public RunfastMessge withCityName(String cityName) {
        this.setCityName(cityName);
        return this;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName == null ? null : cityName.trim();
    }

    public String getCountyId() {
        return countyId;
    }

    public RunfastMessge withCountyId(String countyId) {
        this.setCountyId(countyId);
        return this;
    }

    public void setCountyId(String countyId) {
        this.countyId = countyId == null ? null : countyId.trim();
    }

    public String getCountyName() {
        return countyName;
    }

    public RunfastMessge withCountyName(String countyName) {
        this.setCountyName(countyName);
        return this;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName == null ? null : countyName.trim();
    }

    public Date getEnd() {
        return end;
    }

    public RunfastMessge withEnd(Date end) {
        this.setEnd(end);
        return this;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Date getStart() {
        return start;
    }

    public RunfastMessge withStart(Date start) {
        this.setStart(start);
        return this;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public String getTownId() {
        return townId;
    }

    public RunfastMessge withTownId(String townId) {
        this.setTownId(townId);
        return this;
    }

    public void setTownId(String townId) {
        this.townId = townId == null ? null : townId.trim();
    }

    public String getTownName() {
        return townName;
    }

    public RunfastMessge withTownName(String townName) {
        this.setTownName(townName);
        return this;
    }

    public void setTownName(String townName) {
        this.townName = townName == null ? null : townName.trim();
    }

    public Integer getAgentId() {
        return agentId;
    }

    public RunfastMessge withAgentId(Integer agentId) {
        this.setAgentId(agentId);
        return this;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public String getAgentName() {
        return agentName;
    }

    public RunfastMessge withAgentName(String agentName) {
        this.setAgentName(agentName);
        return this;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName == null ? null : agentName.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", content=").append(content);
        sb.append(", createTime=").append(createTime);
        sb.append(", title=").append(title);
        sb.append(", type=").append(type);
        sb.append(", userId=").append(userId);
        sb.append(", userName=").append(userName);
        sb.append(", cityId=").append(cityId);
        sb.append(", cityName=").append(cityName);
        sb.append(", countyId=").append(countyId);
        sb.append(", countyName=").append(countyName);
        sb.append(", end=").append(end);
        sb.append(", start=").append(start);
        sb.append(", townId=").append(townId);
        sb.append(", townName=").append(townName);
        sb.append(", agentId=").append(agentId);
        sb.append(", agentName=").append(agentName);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}