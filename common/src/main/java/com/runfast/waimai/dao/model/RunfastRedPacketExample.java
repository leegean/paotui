package com.runfast.waimai.dao.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RunfastRedPacketExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public RunfastRedPacketExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andBusinessIdIsNull() {
            addCriterion("businessId is null");
            return (Criteria) this;
        }

        public Criteria andBusinessIdIsNotNull() {
            addCriterion("businessId is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessIdEqualTo(Integer value) {
            addCriterion("businessId =", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdNotEqualTo(Integer value) {
            addCriterion("businessId <>", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdGreaterThan(Integer value) {
            addCriterion("businessId >", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("businessId >=", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdLessThan(Integer value) {
            addCriterion("businessId <", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdLessThanOrEqualTo(Integer value) {
            addCriterion("businessId <=", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdIn(List<Integer> values) {
            addCriterion("businessId in", values, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdNotIn(List<Integer> values) {
            addCriterion("businessId not in", values, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdBetween(Integer value1, Integer value2) {
            addCriterion("businessId between", value1, value2, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdNotBetween(Integer value1, Integer value2) {
            addCriterion("businessId not between", value1, value2, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessNameIsNull() {
            addCriterion("businessName is null");
            return (Criteria) this;
        }

        public Criteria andBusinessNameIsNotNull() {
            addCriterion("businessName is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessNameEqualTo(String value) {
            addCriterion("businessName =", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameNotEqualTo(String value) {
            addCriterion("businessName <>", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameGreaterThan(String value) {
            addCriterion("businessName >", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameGreaterThanOrEqualTo(String value) {
            addCriterion("businessName >=", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameLessThan(String value) {
            addCriterion("businessName <", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameLessThanOrEqualTo(String value) {
            addCriterion("businessName <=", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameLike(String value) {
            addCriterion("businessName like", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameNotLike(String value) {
            addCriterion("businessName not like", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameIn(List<String> values) {
            addCriterion("businessName in", values, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameNotIn(List<String> values) {
            addCriterion("businessName not in", values, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameBetween(String value1, String value2) {
            addCriterion("businessName between", value1, value2, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameNotBetween(String value1, String value2) {
            addCriterion("businessName not between", value1, value2, "businessName");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("createTime is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("createTime is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("createTime =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("createTime <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("createTime >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("createTime >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("createTime <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("createTime <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("createTime in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("createTime not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("createTime between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("createTime not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeIsNull() {
            addCriterion("startTime is null");
            return (Criteria) this;
        }

        public Criteria andStartTimeIsNotNull() {
            addCriterion("startTime is not null");
            return (Criteria) this;
        }

        public Criteria andStartTimeEqualTo(Date value) {
            addCriterion("startTime =", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotEqualTo(Date value) {
            addCriterion("startTime <>", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeGreaterThan(Date value) {
            addCriterion("startTime >", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("startTime >=", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeLessThan(Date value) {
            addCriterion("startTime <", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeLessThanOrEqualTo(Date value) {
            addCriterion("startTime <=", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeIn(List<Date> values) {
            addCriterion("startTime in", values, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotIn(List<Date> values) {
            addCriterion("startTime not in", values, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeBetween(Date value1, Date value2) {
            addCriterion("startTime between", value1, value2, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotBetween(Date value1, Date value2) {
            addCriterion("startTime not between", value1, value2, "startTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeIsNull() {
            addCriterion("endTime is null");
            return (Criteria) this;
        }

        public Criteria andEndTimeIsNotNull() {
            addCriterion("endTime is not null");
            return (Criteria) this;
        }

        public Criteria andEndTimeEqualTo(Date value) {
            addCriterion("endTime =", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotEqualTo(Date value) {
            addCriterion("endTime <>", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeGreaterThan(Date value) {
            addCriterion("endTime >", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("endTime >=", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeLessThan(Date value) {
            addCriterion("endTime <", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeLessThanOrEqualTo(Date value) {
            addCriterion("endTime <=", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeIn(List<Date> values) {
            addCriterion("endTime in", values, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotIn(List<Date> values) {
            addCriterion("endTime not in", values, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeBetween(Date value1, Date value2) {
            addCriterion("endTime between", value1, value2, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotBetween(Date value1, Date value2) {
            addCriterion("endTime not between", value1, value2, "endTime");
            return (Criteria) this;
        }

        public Criteria andFullIsNull() {
            addCriterion("full is null");
            return (Criteria) this;
        }

        public Criteria andFullIsNotNull() {
            addCriterion("full is not null");
            return (Criteria) this;
        }

        public Criteria andFullEqualTo(Double value) {
            addCriterion("full =", value, "full");
            return (Criteria) this;
        }

        public Criteria andFullNotEqualTo(Double value) {
            addCriterion("full <>", value, "full");
            return (Criteria) this;
        }

        public Criteria andFullGreaterThan(Double value) {
            addCriterion("full >", value, "full");
            return (Criteria) this;
        }

        public Criteria andFullGreaterThanOrEqualTo(Double value) {
            addCriterion("full >=", value, "full");
            return (Criteria) this;
        }

        public Criteria andFullLessThan(Double value) {
            addCriterion("full <", value, "full");
            return (Criteria) this;
        }

        public Criteria andFullLessThanOrEqualTo(Double value) {
            addCriterion("full <=", value, "full");
            return (Criteria) this;
        }

        public Criteria andFullIn(List<Double> values) {
            addCriterion("full in", values, "full");
            return (Criteria) this;
        }

        public Criteria andFullNotIn(List<Double> values) {
            addCriterion("full not in", values, "full");
            return (Criteria) this;
        }

        public Criteria andFullBetween(Double value1, Double value2) {
            addCriterion("full between", value1, value2, "full");
            return (Criteria) this;
        }

        public Criteria andFullNotBetween(Double value1, Double value2) {
            addCriterion("full not between", value1, value2, "full");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andTypeIsNull() {
            addCriterion("type is null");
            return (Criteria) this;
        }

        public Criteria andTypeIsNotNull() {
            addCriterion("type is not null");
            return (Criteria) this;
        }

        public Criteria andTypeEqualTo(Integer value) {
            addCriterion("type =", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotEqualTo(Integer value) {
            addCriterion("type <>", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThan(Integer value) {
            addCriterion("type >", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("type >=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThan(Integer value) {
            addCriterion("type <", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThanOrEqualTo(Integer value) {
            addCriterion("type <=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeIn(List<Integer> values) {
            addCriterion("type in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotIn(List<Integer> values) {
            addCriterion("type not in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeBetween(Integer value1, Integer value2) {
            addCriterion("type between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("type not between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andStopsIsNull() {
            addCriterion("stops is null");
            return (Criteria) this;
        }

        public Criteria andStopsIsNotNull() {
            addCriterion("stops is not null");
            return (Criteria) this;
        }

        public Criteria andStopsEqualTo(Integer value) {
            addCriterion("stops =", value, "stops");
            return (Criteria) this;
        }

        public Criteria andStopsNotEqualTo(Integer value) {
            addCriterion("stops <>", value, "stops");
            return (Criteria) this;
        }

        public Criteria andStopsGreaterThan(Integer value) {
            addCriterion("stops >", value, "stops");
            return (Criteria) this;
        }

        public Criteria andStopsGreaterThanOrEqualTo(Integer value) {
            addCriterion("stops >=", value, "stops");
            return (Criteria) this;
        }

        public Criteria andStopsLessThan(Integer value) {
            addCriterion("stops <", value, "stops");
            return (Criteria) this;
        }

        public Criteria andStopsLessThanOrEqualTo(Integer value) {
            addCriterion("stops <=", value, "stops");
            return (Criteria) this;
        }

        public Criteria andStopsIn(List<Integer> values) {
            addCriterion("stops in", values, "stops");
            return (Criteria) this;
        }

        public Criteria andStopsNotIn(List<Integer> values) {
            addCriterion("stops not in", values, "stops");
            return (Criteria) this;
        }

        public Criteria andStopsBetween(Integer value1, Integer value2) {
            addCriterion("stops between", value1, value2, "stops");
            return (Criteria) this;
        }

        public Criteria andStopsNotBetween(Integer value1, Integer value2) {
            addCriterion("stops not between", value1, value2, "stops");
            return (Criteria) this;
        }

        public Criteria andStart1IsNull() {
            addCriterion("start1 is null");
            return (Criteria) this;
        }

        public Criteria andStart1IsNotNull() {
            addCriterion("start1 is not null");
            return (Criteria) this;
        }

        public Criteria andStart1EqualTo(Date value) {
            addCriterion("start1 =", value, "start1");
            return (Criteria) this;
        }

        public Criteria andStart1NotEqualTo(Date value) {
            addCriterion("start1 <>", value, "start1");
            return (Criteria) this;
        }

        public Criteria andStart1GreaterThan(Date value) {
            addCriterion("start1 >", value, "start1");
            return (Criteria) this;
        }

        public Criteria andStart1GreaterThanOrEqualTo(Date value) {
            addCriterion("start1 >=", value, "start1");
            return (Criteria) this;
        }

        public Criteria andStart1LessThan(Date value) {
            addCriterion("start1 <", value, "start1");
            return (Criteria) this;
        }

        public Criteria andStart1LessThanOrEqualTo(Date value) {
            addCriterion("start1 <=", value, "start1");
            return (Criteria) this;
        }

        public Criteria andStart1In(List<Date> values) {
            addCriterion("start1 in", values, "start1");
            return (Criteria) this;
        }

        public Criteria andStart1NotIn(List<Date> values) {
            addCriterion("start1 not in", values, "start1");
            return (Criteria) this;
        }

        public Criteria andStart1Between(Date value1, Date value2) {
            addCriterion("start1 between", value1, value2, "start1");
            return (Criteria) this;
        }

        public Criteria andStart1NotBetween(Date value1, Date value2) {
            addCriterion("start1 not between", value1, value2, "start1");
            return (Criteria) this;
        }

        public Criteria andStart2IsNull() {
            addCriterion("start2 is null");
            return (Criteria) this;
        }

        public Criteria andStart2IsNotNull() {
            addCriterion("start2 is not null");
            return (Criteria) this;
        }

        public Criteria andStart2EqualTo(Date value) {
            addCriterion("start2 =", value, "start2");
            return (Criteria) this;
        }

        public Criteria andStart2NotEqualTo(Date value) {
            addCriterion("start2 <>", value, "start2");
            return (Criteria) this;
        }

        public Criteria andStart2GreaterThan(Date value) {
            addCriterion("start2 >", value, "start2");
            return (Criteria) this;
        }

        public Criteria andStart2GreaterThanOrEqualTo(Date value) {
            addCriterion("start2 >=", value, "start2");
            return (Criteria) this;
        }

        public Criteria andStart2LessThan(Date value) {
            addCriterion("start2 <", value, "start2");
            return (Criteria) this;
        }

        public Criteria andStart2LessThanOrEqualTo(Date value) {
            addCriterion("start2 <=", value, "start2");
            return (Criteria) this;
        }

        public Criteria andStart2In(List<Date> values) {
            addCriterion("start2 in", values, "start2");
            return (Criteria) this;
        }

        public Criteria andStart2NotIn(List<Date> values) {
            addCriterion("start2 not in", values, "start2");
            return (Criteria) this;
        }

        public Criteria andStart2Between(Date value1, Date value2) {
            addCriterion("start2 between", value1, value2, "start2");
            return (Criteria) this;
        }

        public Criteria andStart2NotBetween(Date value1, Date value2) {
            addCriterion("start2 not between", value1, value2, "start2");
            return (Criteria) this;
        }

        public Criteria andEnd1IsNull() {
            addCriterion("end1 is null");
            return (Criteria) this;
        }

        public Criteria andEnd1IsNotNull() {
            addCriterion("end1 is not null");
            return (Criteria) this;
        }

        public Criteria andEnd1EqualTo(Date value) {
            addCriterion("end1 =", value, "end1");
            return (Criteria) this;
        }

        public Criteria andEnd1NotEqualTo(Date value) {
            addCriterion("end1 <>", value, "end1");
            return (Criteria) this;
        }

        public Criteria andEnd1GreaterThan(Date value) {
            addCriterion("end1 >", value, "end1");
            return (Criteria) this;
        }

        public Criteria andEnd1GreaterThanOrEqualTo(Date value) {
            addCriterion("end1 >=", value, "end1");
            return (Criteria) this;
        }

        public Criteria andEnd1LessThan(Date value) {
            addCriterion("end1 <", value, "end1");
            return (Criteria) this;
        }

        public Criteria andEnd1LessThanOrEqualTo(Date value) {
            addCriterion("end1 <=", value, "end1");
            return (Criteria) this;
        }

        public Criteria andEnd1In(List<Date> values) {
            addCriterion("end1 in", values, "end1");
            return (Criteria) this;
        }

        public Criteria andEnd1NotIn(List<Date> values) {
            addCriterion("end1 not in", values, "end1");
            return (Criteria) this;
        }

        public Criteria andEnd1Between(Date value1, Date value2) {
            addCriterion("end1 between", value1, value2, "end1");
            return (Criteria) this;
        }

        public Criteria andEnd1NotBetween(Date value1, Date value2) {
            addCriterion("end1 not between", value1, value2, "end1");
            return (Criteria) this;
        }

        public Criteria andEnd2IsNull() {
            addCriterion("end2 is null");
            return (Criteria) this;
        }

        public Criteria andEnd2IsNotNull() {
            addCriterion("end2 is not null");
            return (Criteria) this;
        }

        public Criteria andEnd2EqualTo(Date value) {
            addCriterion("end2 =", value, "end2");
            return (Criteria) this;
        }

        public Criteria andEnd2NotEqualTo(Date value) {
            addCriterion("end2 <>", value, "end2");
            return (Criteria) this;
        }

        public Criteria andEnd2GreaterThan(Date value) {
            addCriterion("end2 >", value, "end2");
            return (Criteria) this;
        }

        public Criteria andEnd2GreaterThanOrEqualTo(Date value) {
            addCriterion("end2 >=", value, "end2");
            return (Criteria) this;
        }

        public Criteria andEnd2LessThan(Date value) {
            addCriterion("end2 <", value, "end2");
            return (Criteria) this;
        }

        public Criteria andEnd2LessThanOrEqualTo(Date value) {
            addCriterion("end2 <=", value, "end2");
            return (Criteria) this;
        }

        public Criteria andEnd2In(List<Date> values) {
            addCriterion("end2 in", values, "end2");
            return (Criteria) this;
        }

        public Criteria andEnd2NotIn(List<Date> values) {
            addCriterion("end2 not in", values, "end2");
            return (Criteria) this;
        }

        public Criteria andEnd2Between(Date value1, Date value2) {
            addCriterion("end2 between", value1, value2, "end2");
            return (Criteria) this;
        }

        public Criteria andEnd2NotBetween(Date value1, Date value2) {
            addCriterion("end2 not between", value1, value2, "end2");
            return (Criteria) this;
        }

        public Criteria andStart3IsNull() {
            addCriterion("start3 is null");
            return (Criteria) this;
        }

        public Criteria andStart3IsNotNull() {
            addCriterion("start3 is not null");
            return (Criteria) this;
        }

        public Criteria andStart3EqualTo(Date value) {
            addCriterion("start3 =", value, "start3");
            return (Criteria) this;
        }

        public Criteria andStart3NotEqualTo(Date value) {
            addCriterion("start3 <>", value, "start3");
            return (Criteria) this;
        }

        public Criteria andStart3GreaterThan(Date value) {
            addCriterion("start3 >", value, "start3");
            return (Criteria) this;
        }

        public Criteria andStart3GreaterThanOrEqualTo(Date value) {
            addCriterion("start3 >=", value, "start3");
            return (Criteria) this;
        }

        public Criteria andStart3LessThan(Date value) {
            addCriterion("start3 <", value, "start3");
            return (Criteria) this;
        }

        public Criteria andStart3LessThanOrEqualTo(Date value) {
            addCriterion("start3 <=", value, "start3");
            return (Criteria) this;
        }

        public Criteria andStart3In(List<Date> values) {
            addCriterion("start3 in", values, "start3");
            return (Criteria) this;
        }

        public Criteria andStart3NotIn(List<Date> values) {
            addCriterion("start3 not in", values, "start3");
            return (Criteria) this;
        }

        public Criteria andStart3Between(Date value1, Date value2) {
            addCriterion("start3 between", value1, value2, "start3");
            return (Criteria) this;
        }

        public Criteria andStart3NotBetween(Date value1, Date value2) {
            addCriterion("start3 not between", value1, value2, "start3");
            return (Criteria) this;
        }

        public Criteria andEnd3IsNull() {
            addCriterion("end3 is null");
            return (Criteria) this;
        }

        public Criteria andEnd3IsNotNull() {
            addCriterion("end3 is not null");
            return (Criteria) this;
        }

        public Criteria andEnd3EqualTo(Date value) {
            addCriterion("end3 =", value, "end3");
            return (Criteria) this;
        }

        public Criteria andEnd3NotEqualTo(Date value) {
            addCriterion("end3 <>", value, "end3");
            return (Criteria) this;
        }

        public Criteria andEnd3GreaterThan(Date value) {
            addCriterion("end3 >", value, "end3");
            return (Criteria) this;
        }

        public Criteria andEnd3GreaterThanOrEqualTo(Date value) {
            addCriterion("end3 >=", value, "end3");
            return (Criteria) this;
        }

        public Criteria andEnd3LessThan(Date value) {
            addCriterion("end3 <", value, "end3");
            return (Criteria) this;
        }

        public Criteria andEnd3LessThanOrEqualTo(Date value) {
            addCriterion("end3 <=", value, "end3");
            return (Criteria) this;
        }

        public Criteria andEnd3In(List<Date> values) {
            addCriterion("end3 in", values, "end3");
            return (Criteria) this;
        }

        public Criteria andEnd3NotIn(List<Date> values) {
            addCriterion("end3 not in", values, "end3");
            return (Criteria) this;
        }

        public Criteria andEnd3Between(Date value1, Date value2) {
            addCriterion("end3 between", value1, value2, "end3");
            return (Criteria) this;
        }

        public Criteria andEnd3NotBetween(Date value1, Date value2) {
            addCriterion("end3 not between", value1, value2, "end3");
            return (Criteria) this;
        }

        public Criteria andDeletedIsNull() {
            addCriterion("deleted is null");
            return (Criteria) this;
        }

        public Criteria andDeletedIsNotNull() {
            addCriterion("deleted is not null");
            return (Criteria) this;
        }

        public Criteria andDeletedEqualTo(Integer value) {
            addCriterion("deleted =", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedNotEqualTo(Integer value) {
            addCriterion("deleted <>", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedGreaterThan(Integer value) {
            addCriterion("deleted >", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedGreaterThanOrEqualTo(Integer value) {
            addCriterion("deleted >=", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedLessThan(Integer value) {
            addCriterion("deleted <", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedLessThanOrEqualTo(Integer value) {
            addCriterion("deleted <=", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedIn(List<Integer> values) {
            addCriterion("deleted in", values, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedNotIn(List<Integer> values) {
            addCriterion("deleted not in", values, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedBetween(Integer value1, Integer value2) {
            addCriterion("deleted between", value1, value2, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedNotBetween(Integer value1, Integer value2) {
            addCriterion("deleted not between", value1, value2, "deleted");
            return (Criteria) this;
        }

        public Criteria andSharedIsNull() {
            addCriterion("shared is null");
            return (Criteria) this;
        }

        public Criteria andSharedIsNotNull() {
            addCriterion("shared is not null");
            return (Criteria) this;
        }

        public Criteria andSharedEqualTo(Boolean value) {
            addCriterion("shared =", value, "shared");
            return (Criteria) this;
        }

        public Criteria andSharedNotEqualTo(Boolean value) {
            addCriterion("shared <>", value, "shared");
            return (Criteria) this;
        }

        public Criteria andSharedGreaterThan(Boolean value) {
            addCriterion("shared >", value, "shared");
            return (Criteria) this;
        }

        public Criteria andSharedGreaterThanOrEqualTo(Boolean value) {
            addCriterion("shared >=", value, "shared");
            return (Criteria) this;
        }

        public Criteria andSharedLessThan(Boolean value) {
            addCriterion("shared <", value, "shared");
            return (Criteria) this;
        }

        public Criteria andSharedLessThanOrEqualTo(Boolean value) {
            addCriterion("shared <=", value, "shared");
            return (Criteria) this;
        }

        public Criteria andSharedIn(List<Boolean> values) {
            addCriterion("shared in", values, "shared");
            return (Criteria) this;
        }

        public Criteria andSharedNotIn(List<Boolean> values) {
            addCriterion("shared not in", values, "shared");
            return (Criteria) this;
        }

        public Criteria andSharedBetween(Boolean value1, Boolean value2) {
            addCriterion("shared between", value1, value2, "shared");
            return (Criteria) this;
        }

        public Criteria andSharedNotBetween(Boolean value1, Boolean value2) {
            addCriterion("shared not between", value1, value2, "shared");
            return (Criteria) this;
        }

        public Criteria andWeekIsNull() {
            addCriterion("week is null");
            return (Criteria) this;
        }

        public Criteria andWeekIsNotNull() {
            addCriterion("week is not null");
            return (Criteria) this;
        }

        public Criteria andWeekEqualTo(String value) {
            addCriterion("week =", value, "week");
            return (Criteria) this;
        }

        public Criteria andWeekNotEqualTo(String value) {
            addCriterion("week <>", value, "week");
            return (Criteria) this;
        }

        public Criteria andWeekGreaterThan(String value) {
            addCriterion("week >", value, "week");
            return (Criteria) this;
        }

        public Criteria andWeekGreaterThanOrEqualTo(String value) {
            addCriterion("week >=", value, "week");
            return (Criteria) this;
        }

        public Criteria andWeekLessThan(String value) {
            addCriterion("week <", value, "week");
            return (Criteria) this;
        }

        public Criteria andWeekLessThanOrEqualTo(String value) {
            addCriterion("week <=", value, "week");
            return (Criteria) this;
        }

        public Criteria andWeekLike(String value) {
            addCriterion("week like", value, "week");
            return (Criteria) this;
        }

        public Criteria andWeekNotLike(String value) {
            addCriterion("week not like", value, "week");
            return (Criteria) this;
        }

        public Criteria andWeekIn(List<String> values) {
            addCriterion("week in", values, "week");
            return (Criteria) this;
        }

        public Criteria andWeekNotIn(List<String> values) {
            addCriterion("week not in", values, "week");
            return (Criteria) this;
        }

        public Criteria andWeekBetween(String value1, String value2) {
            addCriterion("week between", value1, value2, "week");
            return (Criteria) this;
        }

        public Criteria andWeekNotBetween(String value1, String value2) {
            addCriterion("week not between", value1, value2, "week");
            return (Criteria) this;
        }

        public Criteria andSubsidyIsNull() {
            addCriterion("subsidy is null");
            return (Criteria) this;
        }

        public Criteria andSubsidyIsNotNull() {
            addCriterion("subsidy is not null");
            return (Criteria) this;
        }

        public Criteria andSubsidyEqualTo(Double value) {
            addCriterion("subsidy =", value, "subsidy");
            return (Criteria) this;
        }

        public Criteria andSubsidyNotEqualTo(Double value) {
            addCriterion("subsidy <>", value, "subsidy");
            return (Criteria) this;
        }

        public Criteria andSubsidyGreaterThan(Double value) {
            addCriterion("subsidy >", value, "subsidy");
            return (Criteria) this;
        }

        public Criteria andSubsidyGreaterThanOrEqualTo(Double value) {
            addCriterion("subsidy >=", value, "subsidy");
            return (Criteria) this;
        }

        public Criteria andSubsidyLessThan(Double value) {
            addCriterion("subsidy <", value, "subsidy");
            return (Criteria) this;
        }

        public Criteria andSubsidyLessThanOrEqualTo(Double value) {
            addCriterion("subsidy <=", value, "subsidy");
            return (Criteria) this;
        }

        public Criteria andSubsidyIn(List<Double> values) {
            addCriterion("subsidy in", values, "subsidy");
            return (Criteria) this;
        }

        public Criteria andSubsidyNotIn(List<Double> values) {
            addCriterion("subsidy not in", values, "subsidy");
            return (Criteria) this;
        }

        public Criteria andSubsidyBetween(Double value1, Double value2) {
            addCriterion("subsidy between", value1, value2, "subsidy");
            return (Criteria) this;
        }

        public Criteria andSubsidyNotBetween(Double value1, Double value2) {
            addCriterion("subsidy not between", value1, value2, "subsidy");
            return (Criteria) this;
        }

        public Criteria andValidDayIsNull() {
            addCriterion("validDay is null");
            return (Criteria) this;
        }

        public Criteria andValidDayIsNotNull() {
            addCriterion("validDay is not null");
            return (Criteria) this;
        }

        public Criteria andValidDayEqualTo(Integer value) {
            addCriterion("validDay =", value, "validDay");
            return (Criteria) this;
        }

        public Criteria andValidDayNotEqualTo(Integer value) {
            addCriterion("validDay <>", value, "validDay");
            return (Criteria) this;
        }

        public Criteria andValidDayGreaterThan(Integer value) {
            addCriterion("validDay >", value, "validDay");
            return (Criteria) this;
        }

        public Criteria andValidDayGreaterThanOrEqualTo(Integer value) {
            addCriterion("validDay >=", value, "validDay");
            return (Criteria) this;
        }

        public Criteria andValidDayLessThan(Integer value) {
            addCriterion("validDay <", value, "validDay");
            return (Criteria) this;
        }

        public Criteria andValidDayLessThanOrEqualTo(Integer value) {
            addCriterion("validDay <=", value, "validDay");
            return (Criteria) this;
        }

        public Criteria andValidDayIn(List<Integer> values) {
            addCriterion("validDay in", values, "validDay");
            return (Criteria) this;
        }

        public Criteria andValidDayNotIn(List<Integer> values) {
            addCriterion("validDay not in", values, "validDay");
            return (Criteria) this;
        }

        public Criteria andValidDayBetween(Integer value1, Integer value2) {
            addCriterion("validDay between", value1, value2, "validDay");
            return (Criteria) this;
        }

        public Criteria andValidDayNotBetween(Integer value1, Integer value2) {
            addCriterion("validDay not between", value1, value2, "validDay");
            return (Criteria) this;
        }

        public Criteria andDiscardIsNull() {
            addCriterion("discard is null");
            return (Criteria) this;
        }

        public Criteria andDiscardIsNotNull() {
            addCriterion("discard is not null");
            return (Criteria) this;
        }

        public Criteria andDiscardEqualTo(Boolean value) {
            addCriterion("discard =", value, "discard");
            return (Criteria) this;
        }

        public Criteria andDiscardNotEqualTo(Boolean value) {
            addCriterion("discard <>", value, "discard");
            return (Criteria) this;
        }

        public Criteria andDiscardGreaterThan(Boolean value) {
            addCriterion("discard >", value, "discard");
            return (Criteria) this;
        }

        public Criteria andDiscardGreaterThanOrEqualTo(Boolean value) {
            addCriterion("discard >=", value, "discard");
            return (Criteria) this;
        }

        public Criteria andDiscardLessThan(Boolean value) {
            addCriterion("discard <", value, "discard");
            return (Criteria) this;
        }

        public Criteria andDiscardLessThanOrEqualTo(Boolean value) {
            addCriterion("discard <=", value, "discard");
            return (Criteria) this;
        }

        public Criteria andDiscardIn(List<Boolean> values) {
            addCriterion("discard in", values, "discard");
            return (Criteria) this;
        }

        public Criteria andDiscardNotIn(List<Boolean> values) {
            addCriterion("discard not in", values, "discard");
            return (Criteria) this;
        }

        public Criteria andDiscardBetween(Boolean value1, Boolean value2) {
            addCriterion("discard between", value1, value2, "discard");
            return (Criteria) this;
        }

        public Criteria andDiscardNotBetween(Boolean value1, Boolean value2) {
            addCriterion("discard not between", value1, value2, "discard");
            return (Criteria) this;
        }

        public Criteria andDiscardTimeIsNull() {
            addCriterion("discardTime is null");
            return (Criteria) this;
        }

        public Criteria andDiscardTimeIsNotNull() {
            addCriterion("discardTime is not null");
            return (Criteria) this;
        }

        public Criteria andDiscardTimeEqualTo(Date value) {
            addCriterion("discardTime =", value, "discardTime");
            return (Criteria) this;
        }

        public Criteria andDiscardTimeNotEqualTo(Date value) {
            addCriterion("discardTime <>", value, "discardTime");
            return (Criteria) this;
        }

        public Criteria andDiscardTimeGreaterThan(Date value) {
            addCriterion("discardTime >", value, "discardTime");
            return (Criteria) this;
        }

        public Criteria andDiscardTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("discardTime >=", value, "discardTime");
            return (Criteria) this;
        }

        public Criteria andDiscardTimeLessThan(Date value) {
            addCriterion("discardTime <", value, "discardTime");
            return (Criteria) this;
        }

        public Criteria andDiscardTimeLessThanOrEqualTo(Date value) {
            addCriterion("discardTime <=", value, "discardTime");
            return (Criteria) this;
        }

        public Criteria andDiscardTimeIn(List<Date> values) {
            addCriterion("discardTime in", values, "discardTime");
            return (Criteria) this;
        }

        public Criteria andDiscardTimeNotIn(List<Date> values) {
            addCriterion("discardTime not in", values, "discardTime");
            return (Criteria) this;
        }

        public Criteria andDiscardTimeBetween(Date value1, Date value2) {
            addCriterion("discardTime between", value1, value2, "discardTime");
            return (Criteria) this;
        }

        public Criteria andDiscardTimeNotBetween(Date value1, Date value2) {
            addCriterion("discardTime not between", value1, value2, "discardTime");
            return (Criteria) this;
        }

        public Criteria andCreateByIsNull() {
            addCriterion("createBy is null");
            return (Criteria) this;
        }

        public Criteria andCreateByIsNotNull() {
            addCriterion("createBy is not null");
            return (Criteria) this;
        }

        public Criteria andCreateByEqualTo(Integer value) {
            addCriterion("createBy =", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByNotEqualTo(Integer value) {
            addCriterion("createBy <>", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByGreaterThan(Integer value) {
            addCriterion("createBy >", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByGreaterThanOrEqualTo(Integer value) {
            addCriterion("createBy >=", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByLessThan(Integer value) {
            addCriterion("createBy <", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByLessThanOrEqualTo(Integer value) {
            addCriterion("createBy <=", value, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByIn(List<Integer> values) {
            addCriterion("createBy in", values, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByNotIn(List<Integer> values) {
            addCriterion("createBy not in", values, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByBetween(Integer value1, Integer value2) {
            addCriterion("createBy between", value1, value2, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateByNotBetween(Integer value1, Integer value2) {
            addCriterion("createBy not between", value1, value2, "createBy");
            return (Criteria) this;
        }

        public Criteria andCreateTypeIsNull() {
            addCriterion("createType is null");
            return (Criteria) this;
        }

        public Criteria andCreateTypeIsNotNull() {
            addCriterion("createType is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTypeEqualTo(Integer value) {
            addCriterion("createType =", value, "createType");
            return (Criteria) this;
        }

        public Criteria andCreateTypeNotEqualTo(Integer value) {
            addCriterion("createType <>", value, "createType");
            return (Criteria) this;
        }

        public Criteria andCreateTypeGreaterThan(Integer value) {
            addCriterion("createType >", value, "createType");
            return (Criteria) this;
        }

        public Criteria andCreateTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("createType >=", value, "createType");
            return (Criteria) this;
        }

        public Criteria andCreateTypeLessThan(Integer value) {
            addCriterion("createType <", value, "createType");
            return (Criteria) this;
        }

        public Criteria andCreateTypeLessThanOrEqualTo(Integer value) {
            addCriterion("createType <=", value, "createType");
            return (Criteria) this;
        }

        public Criteria andCreateTypeIn(List<Integer> values) {
            addCriterion("createType in", values, "createType");
            return (Criteria) this;
        }

        public Criteria andCreateTypeNotIn(List<Integer> values) {
            addCriterion("createType not in", values, "createType");
            return (Criteria) this;
        }

        public Criteria andCreateTypeBetween(Integer value1, Integer value2) {
            addCriterion("createType between", value1, value2, "createType");
            return (Criteria) this;
        }

        public Criteria andCreateTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("createType not between", value1, value2, "createType");
            return (Criteria) this;
        }

        public Criteria andUserTypeIsNull() {
            addCriterion("userType is null");
            return (Criteria) this;
        }

        public Criteria andUserTypeIsNotNull() {
            addCriterion("userType is not null");
            return (Criteria) this;
        }

        public Criteria andUserTypeEqualTo(Integer value) {
            addCriterion("userType =", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeNotEqualTo(Integer value) {
            addCriterion("userType <>", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeGreaterThan(Integer value) {
            addCriterion("userType >", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("userType >=", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeLessThan(Integer value) {
            addCriterion("userType <", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeLessThanOrEqualTo(Integer value) {
            addCriterion("userType <=", value, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeIn(List<Integer> values) {
            addCriterion("userType in", values, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeNotIn(List<Integer> values) {
            addCriterion("userType not in", values, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeBetween(Integer value1, Integer value2) {
            addCriterion("userType between", value1, value2, "userType");
            return (Criteria) this;
        }

        public Criteria andUserTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("userType not between", value1, value2, "userType");
            return (Criteria) this;
        }

        public Criteria andDayIsNull() {
            addCriterion("day is null");
            return (Criteria) this;
        }

        public Criteria andDayIsNotNull() {
            addCriterion("day is not null");
            return (Criteria) this;
        }

        public Criteria andDayEqualTo(Integer value) {
            addCriterion("day =", value, "day");
            return (Criteria) this;
        }

        public Criteria andDayNotEqualTo(Integer value) {
            addCriterion("day <>", value, "day");
            return (Criteria) this;
        }

        public Criteria andDayGreaterThan(Integer value) {
            addCriterion("day >", value, "day");
            return (Criteria) this;
        }

        public Criteria andDayGreaterThanOrEqualTo(Integer value) {
            addCriterion("day >=", value, "day");
            return (Criteria) this;
        }

        public Criteria andDayLessThan(Integer value) {
            addCriterion("day <", value, "day");
            return (Criteria) this;
        }

        public Criteria andDayLessThanOrEqualTo(Integer value) {
            addCriterion("day <=", value, "day");
            return (Criteria) this;
        }

        public Criteria andDayIn(List<Integer> values) {
            addCriterion("day in", values, "day");
            return (Criteria) this;
        }

        public Criteria andDayNotIn(List<Integer> values) {
            addCriterion("day not in", values, "day");
            return (Criteria) this;
        }

        public Criteria andDayBetween(Integer value1, Integer value2) {
            addCriterion("day between", value1, value2, "day");
            return (Criteria) this;
        }

        public Criteria andDayNotBetween(Integer value1, Integer value2) {
            addCriterion("day not between", value1, value2, "day");
            return (Criteria) this;
        }

        public Criteria andLessIsNull() {
            addCriterion("less is null");
            return (Criteria) this;
        }

        public Criteria andLessIsNotNull() {
            addCriterion("less is not null");
            return (Criteria) this;
        }

        public Criteria andLessEqualTo(String value) {
            addCriterion("less =", value, "less");
            return (Criteria) this;
        }

        public Criteria andLessNotEqualTo(String value) {
            addCriterion("less <>", value, "less");
            return (Criteria) this;
        }

        public Criteria andLessGreaterThan(String value) {
            addCriterion("less >", value, "less");
            return (Criteria) this;
        }

        public Criteria andLessGreaterThanOrEqualTo(String value) {
            addCriterion("less >=", value, "less");
            return (Criteria) this;
        }

        public Criteria andLessLessThan(String value) {
            addCriterion("less <", value, "less");
            return (Criteria) this;
        }

        public Criteria andLessLessThanOrEqualTo(String value) {
            addCriterion("less <=", value, "less");
            return (Criteria) this;
        }

        public Criteria andLessLike(String value) {
            addCriterion("less like", value, "less");
            return (Criteria) this;
        }

        public Criteria andLessNotLike(String value) {
            addCriterion("less not like", value, "less");
            return (Criteria) this;
        }

        public Criteria andLessIn(List<String> values) {
            addCriterion("less in", values, "less");
            return (Criteria) this;
        }

        public Criteria andLessNotIn(List<String> values) {
            addCriterion("less not in", values, "less");
            return (Criteria) this;
        }

        public Criteria andLessBetween(String value1, String value2) {
            addCriterion("less between", value1, value2, "less");
            return (Criteria) this;
        }

        public Criteria andLessNotBetween(String value1, String value2) {
            addCriterion("less not between", value1, value2, "less");
            return (Criteria) this;
        }

        public Criteria andLimitTypeIsNull() {
            addCriterion("limitType is null");
            return (Criteria) this;
        }

        public Criteria andLimitTypeIsNotNull() {
            addCriterion("limitType is not null");
            return (Criteria) this;
        }

        public Criteria andLimitTypeEqualTo(Integer value) {
            addCriterion("limitType =", value, "limitType");
            return (Criteria) this;
        }

        public Criteria andLimitTypeNotEqualTo(Integer value) {
            addCriterion("limitType <>", value, "limitType");
            return (Criteria) this;
        }

        public Criteria andLimitTypeGreaterThan(Integer value) {
            addCriterion("limitType >", value, "limitType");
            return (Criteria) this;
        }

        public Criteria andLimitTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("limitType >=", value, "limitType");
            return (Criteria) this;
        }

        public Criteria andLimitTypeLessThan(Integer value) {
            addCriterion("limitType <", value, "limitType");
            return (Criteria) this;
        }

        public Criteria andLimitTypeLessThanOrEqualTo(Integer value) {
            addCriterion("limitType <=", value, "limitType");
            return (Criteria) this;
        }

        public Criteria andLimitTypeIn(List<Integer> values) {
            addCriterion("limitType in", values, "limitType");
            return (Criteria) this;
        }

        public Criteria andLimitTypeNotIn(List<Integer> values) {
            addCriterion("limitType not in", values, "limitType");
            return (Criteria) this;
        }

        public Criteria andLimitTypeBetween(Integer value1, Integer value2) {
            addCriterion("limitType between", value1, value2, "limitType");
            return (Criteria) this;
        }

        public Criteria andLimitTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("limitType not between", value1, value2, "limitType");
            return (Criteria) this;
        }

        public Criteria andNumIsNull() {
            addCriterion("num is null");
            return (Criteria) this;
        }

        public Criteria andNumIsNotNull() {
            addCriterion("num is not null");
            return (Criteria) this;
        }

        public Criteria andNumEqualTo(Integer value) {
            addCriterion("num =", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumNotEqualTo(Integer value) {
            addCriterion("num <>", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumGreaterThan(Integer value) {
            addCriterion("num >", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("num >=", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumLessThan(Integer value) {
            addCriterion("num <", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumLessThanOrEqualTo(Integer value) {
            addCriterion("num <=", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumIn(List<Integer> values) {
            addCriterion("num in", values, "num");
            return (Criteria) this;
        }

        public Criteria andNumNotIn(List<Integer> values) {
            addCriterion("num not in", values, "num");
            return (Criteria) this;
        }

        public Criteria andNumBetween(Integer value1, Integer value2) {
            addCriterion("num between", value1, value2, "num");
            return (Criteria) this;
        }

        public Criteria andNumNotBetween(Integer value1, Integer value2) {
            addCriterion("num not between", value1, value2, "num");
            return (Criteria) this;
        }

        public Criteria andPersonLimitTypeIsNull() {
            addCriterion("personLimitType is null");
            return (Criteria) this;
        }

        public Criteria andPersonLimitTypeIsNotNull() {
            addCriterion("personLimitType is not null");
            return (Criteria) this;
        }

        public Criteria andPersonLimitTypeEqualTo(Integer value) {
            addCriterion("personLimitType =", value, "personLimitType");
            return (Criteria) this;
        }

        public Criteria andPersonLimitTypeNotEqualTo(Integer value) {
            addCriterion("personLimitType <>", value, "personLimitType");
            return (Criteria) this;
        }

        public Criteria andPersonLimitTypeGreaterThan(Integer value) {
            addCriterion("personLimitType >", value, "personLimitType");
            return (Criteria) this;
        }

        public Criteria andPersonLimitTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("personLimitType >=", value, "personLimitType");
            return (Criteria) this;
        }

        public Criteria andPersonLimitTypeLessThan(Integer value) {
            addCriterion("personLimitType <", value, "personLimitType");
            return (Criteria) this;
        }

        public Criteria andPersonLimitTypeLessThanOrEqualTo(Integer value) {
            addCriterion("personLimitType <=", value, "personLimitType");
            return (Criteria) this;
        }

        public Criteria andPersonLimitTypeIn(List<Integer> values) {
            addCriterion("personLimitType in", values, "personLimitType");
            return (Criteria) this;
        }

        public Criteria andPersonLimitTypeNotIn(List<Integer> values) {
            addCriterion("personLimitType not in", values, "personLimitType");
            return (Criteria) this;
        }

        public Criteria andPersonLimitTypeBetween(Integer value1, Integer value2) {
            addCriterion("personLimitType between", value1, value2, "personLimitType");
            return (Criteria) this;
        }

        public Criteria andPersonLimitTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("personLimitType not between", value1, value2, "personLimitType");
            return (Criteria) this;
        }

        public Criteria andGetNumIsNull() {
            addCriterion("getNum is null");
            return (Criteria) this;
        }

        public Criteria andGetNumIsNotNull() {
            addCriterion("getNum is not null");
            return (Criteria) this;
        }

        public Criteria andGetNumEqualTo(Integer value) {
            addCriterion("getNum =", value, "getNum");
            return (Criteria) this;
        }

        public Criteria andGetNumNotEqualTo(Integer value) {
            addCriterion("getNum <>", value, "getNum");
            return (Criteria) this;
        }

        public Criteria andGetNumGreaterThan(Integer value) {
            addCriterion("getNum >", value, "getNum");
            return (Criteria) this;
        }

        public Criteria andGetNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("getNum >=", value, "getNum");
            return (Criteria) this;
        }

        public Criteria andGetNumLessThan(Integer value) {
            addCriterion("getNum <", value, "getNum");
            return (Criteria) this;
        }

        public Criteria andGetNumLessThanOrEqualTo(Integer value) {
            addCriterion("getNum <=", value, "getNum");
            return (Criteria) this;
        }

        public Criteria andGetNumIn(List<Integer> values) {
            addCriterion("getNum in", values, "getNum");
            return (Criteria) this;
        }

        public Criteria andGetNumNotIn(List<Integer> values) {
            addCriterion("getNum not in", values, "getNum");
            return (Criteria) this;
        }

        public Criteria andGetNumBetween(Integer value1, Integer value2) {
            addCriterion("getNum between", value1, value2, "getNum");
            return (Criteria) this;
        }

        public Criteria andGetNumNotBetween(Integer value1, Integer value2) {
            addCriterion("getNum not between", value1, value2, "getNum");
            return (Criteria) this;
        }

        public Criteria andDayGetNumIsNull() {
            addCriterion("dayGetNum is null");
            return (Criteria) this;
        }

        public Criteria andDayGetNumIsNotNull() {
            addCriterion("dayGetNum is not null");
            return (Criteria) this;
        }

        public Criteria andDayGetNumEqualTo(Integer value) {
            addCriterion("dayGetNum =", value, "dayGetNum");
            return (Criteria) this;
        }

        public Criteria andDayGetNumNotEqualTo(Integer value) {
            addCriterion("dayGetNum <>", value, "dayGetNum");
            return (Criteria) this;
        }

        public Criteria andDayGetNumGreaterThan(Integer value) {
            addCriterion("dayGetNum >", value, "dayGetNum");
            return (Criteria) this;
        }

        public Criteria andDayGetNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("dayGetNum >=", value, "dayGetNum");
            return (Criteria) this;
        }

        public Criteria andDayGetNumLessThan(Integer value) {
            addCriterion("dayGetNum <", value, "dayGetNum");
            return (Criteria) this;
        }

        public Criteria andDayGetNumLessThanOrEqualTo(Integer value) {
            addCriterion("dayGetNum <=", value, "dayGetNum");
            return (Criteria) this;
        }

        public Criteria andDayGetNumIn(List<Integer> values) {
            addCriterion("dayGetNum in", values, "dayGetNum");
            return (Criteria) this;
        }

        public Criteria andDayGetNumNotIn(List<Integer> values) {
            addCriterion("dayGetNum not in", values, "dayGetNum");
            return (Criteria) this;
        }

        public Criteria andDayGetNumBetween(Integer value1, Integer value2) {
            addCriterion("dayGetNum between", value1, value2, "dayGetNum");
            return (Criteria) this;
        }

        public Criteria andDayGetNumNotBetween(Integer value1, Integer value2) {
            addCriterion("dayGetNum not between", value1, value2, "dayGetNum");
            return (Criteria) this;
        }

        public Criteria andDayGetTimeIsNull() {
            addCriterion("dayGetTime is null");
            return (Criteria) this;
        }

        public Criteria andDayGetTimeIsNotNull() {
            addCriterion("dayGetTime is not null");
            return (Criteria) this;
        }

        public Criteria andDayGetTimeEqualTo(Date value) {
            addCriterion("dayGetTime =", value, "dayGetTime");
            return (Criteria) this;
        }

        public Criteria andDayGetTimeNotEqualTo(Date value) {
            addCriterion("dayGetTime <>", value, "dayGetTime");
            return (Criteria) this;
        }

        public Criteria andDayGetTimeGreaterThan(Date value) {
            addCriterion("dayGetTime >", value, "dayGetTime");
            return (Criteria) this;
        }

        public Criteria andDayGetTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("dayGetTime >=", value, "dayGetTime");
            return (Criteria) this;
        }

        public Criteria andDayGetTimeLessThan(Date value) {
            addCriterion("dayGetTime <", value, "dayGetTime");
            return (Criteria) this;
        }

        public Criteria andDayGetTimeLessThanOrEqualTo(Date value) {
            addCriterion("dayGetTime <=", value, "dayGetTime");
            return (Criteria) this;
        }

        public Criteria andDayGetTimeIn(List<Date> values) {
            addCriterion("dayGetTime in", values, "dayGetTime");
            return (Criteria) this;
        }

        public Criteria andDayGetTimeNotIn(List<Date> values) {
            addCriterion("dayGetTime not in", values, "dayGetTime");
            return (Criteria) this;
        }

        public Criteria andDayGetTimeBetween(Date value1, Date value2) {
            addCriterion("dayGetTime between", value1, value2, "dayGetTime");
            return (Criteria) this;
        }

        public Criteria andDayGetTimeNotBetween(Date value1, Date value2) {
            addCriterion("dayGetTime not between", value1, value2, "dayGetTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}