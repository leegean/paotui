package com.runfast.waimai.dao.model;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
public class RunfastActivity implements Serializable {
    public static enum CreateType{
        // 商户id，代理商id，平台账号id分别对应（0，1，2）
        business(0),
        agent(1),
        plateform(2);




        private int code;
        CreateType(int code) {
            this.code = code;
        }

        public int getCode() {
            return code;
        }
    }
    private Integer id;

    private Integer busId;

    private String busname;

    private Date createTime;

    private Double discount;

    private Date endTime;

    private Double fulls;

    private Double lesss;

    private String name;

    /**
     * 活动内容 1满减  2打折3赠品4特价5满减免运费6优惠券7免部分配送费8新用户立减活动9首单立减活动10商户红包11下单返红包,12：代理商发红包
     */
    private Integer ptype;

    private Date startTime;

    private Integer type;

    private String goodids;

    private String goodsname;

    private Integer agentId;

    private String agentName;

    private Integer stops;

    private String goods;

    private BigDecimal disprice;

    private String stanidsname;

    private Integer deleted;

    /**
     * 是否与其他活动同享
     */
    private Boolean shared;

    /**
     * 生效时间，数字1到7分别对应星期一到星期日，多个逗号分隔
     */
    private String week;

    /**
     * 是否限购
     */
    private Boolean is_limited;

    /**
     * 限购类型（超出后是否允许原价购买0否 1是）
     */
    private Integer limit_type;

    /**
     * 限购数量
     */
    private Integer limit_num;

    /**
     * 代理商补贴费用（不超过红包固定金额或者随机金额的下限）
     */
    private Double agent_subsidy;

    /**
     * 1：特惠专区；其他：普通活动
     */
    private Integer special_type;

    /**
     * 优选专区活动主题
     */
    private String special_name;

    private Date start1;

    private Date start2;

    private Date end1;

    private Date end2;

    private Date start3;

    private Date end3;

    private String special_img;

    /**
     * 是否废弃了
     */
    private Boolean discard;

    /**
     * 作废时间
     */
    private Date discardTime;

    /**
     * 活动创建者id(商户id，代理商id，平台账号id)
     */
    private Integer createBy;

    /**
     * 商户id，代理商id，平台账号id分别对应（0，1，2）
     */
    private Integer createType;

    /**
     * 0：全部用户，1：近x日未下单用户,2:随机用户
     */
    private Integer redUserType;

    /**
     * 近x日未下单
     */
    private Integer redDay;

    /**
     * 金额范围（逗号分隔）
     */
    private String redAmount;

    /**
     * 0:每天限量；1：总量限制
     */
    private Integer redLimitType;

    /**
     * 每天限量或者总量限制类型的数量
     */
    private Integer redNum;

    /**
     * 0:每人1张；1：每人每天1张
     */
    private Integer redPersonLimitType;

    /**
     * 该红包已经被领取的数量
     */
    private Integer redGetNum;

    /**
     * 每日被领取的数量
     */
    private Integer redDayGetNum;

    /**
     * 每日最后领取时间
     */
    private Date redDayGetTime;

    /**
     * 红包有效天数
     */
    private Integer redValidDay;

    /**
     * 满返红包
     */
    private Double fullReturn;

    private String stanids;
    private static final long serialVersionUID = 1L;
    private List<RunfastActivityTarget> activityTargetList;

    /**
     * 红包是否已经领取
     */
    private Boolean picked;

}