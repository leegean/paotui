package com.runfast.waimai.dao.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RunfastWithdraworderExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public RunfastWithdraworderExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andBusinessIdIsNull() {
            addCriterion("businessId is null");
            return (Criteria) this;
        }

        public Criteria andBusinessIdIsNotNull() {
            addCriterion("businessId is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessIdEqualTo(Integer value) {
            addCriterion("businessId =", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdNotEqualTo(Integer value) {
            addCriterion("businessId <>", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdGreaterThan(Integer value) {
            addCriterion("businessId >", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("businessId >=", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdLessThan(Integer value) {
            addCriterion("businessId <", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdLessThanOrEqualTo(Integer value) {
            addCriterion("businessId <=", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdIn(List<Integer> values) {
            addCriterion("businessId in", values, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdNotIn(List<Integer> values) {
            addCriterion("businessId not in", values, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdBetween(Integer value1, Integer value2) {
            addCriterion("businessId between", value1, value2, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdNotBetween(Integer value1, Integer value2) {
            addCriterion("businessId not between", value1, value2, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessNameIsNull() {
            addCriterion("businessName is null");
            return (Criteria) this;
        }

        public Criteria andBusinessNameIsNotNull() {
            addCriterion("businessName is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessNameEqualTo(String value) {
            addCriterion("businessName =", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameNotEqualTo(String value) {
            addCriterion("businessName <>", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameGreaterThan(String value) {
            addCriterion("businessName >", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameGreaterThanOrEqualTo(String value) {
            addCriterion("businessName >=", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameLessThan(String value) {
            addCriterion("businessName <", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameLessThanOrEqualTo(String value) {
            addCriterion("businessName <=", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameLike(String value) {
            addCriterion("businessName like", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameNotLike(String value) {
            addCriterion("businessName not like", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameIn(List<String> values) {
            addCriterion("businessName in", values, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameNotIn(List<String> values) {
            addCriterion("businessName not in", values, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameBetween(String value1, String value2) {
            addCriterion("businessName between", value1, value2, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameNotBetween(String value1, String value2) {
            addCriterion("businessName not between", value1, value2, "businessName");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("createTime is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("createTime is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("createTime =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("createTime <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("createTime >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("createTime >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("createTime <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("createTime <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("createTime in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("createTime not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("createTime between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("createTime not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andOidIsNull() {
            addCriterion("oid is null");
            return (Criteria) this;
        }

        public Criteria andOidIsNotNull() {
            addCriterion("oid is not null");
            return (Criteria) this;
        }

        public Criteria andOidEqualTo(Integer value) {
            addCriterion("oid =", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotEqualTo(Integer value) {
            addCriterion("oid <>", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidGreaterThan(Integer value) {
            addCriterion("oid >", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidGreaterThanOrEqualTo(Integer value) {
            addCriterion("oid >=", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidLessThan(Integer value) {
            addCriterion("oid <", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidLessThanOrEqualTo(Integer value) {
            addCriterion("oid <=", value, "oid");
            return (Criteria) this;
        }

        public Criteria andOidIn(List<Integer> values) {
            addCriterion("oid in", values, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotIn(List<Integer> values) {
            addCriterion("oid not in", values, "oid");
            return (Criteria) this;
        }

        public Criteria andOidBetween(Integer value1, Integer value2) {
            addCriterion("oid between", value1, value2, "oid");
            return (Criteria) this;
        }

        public Criteria andOidNotBetween(Integer value1, Integer value2) {
            addCriterion("oid not between", value1, value2, "oid");
            return (Criteria) this;
        }

        public Criteria andOrderCodeIsNull() {
            addCriterion("orderCode is null");
            return (Criteria) this;
        }

        public Criteria andOrderCodeIsNotNull() {
            addCriterion("orderCode is not null");
            return (Criteria) this;
        }

        public Criteria andOrderCodeEqualTo(String value) {
            addCriterion("orderCode =", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeNotEqualTo(String value) {
            addCriterion("orderCode <>", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeGreaterThan(String value) {
            addCriterion("orderCode >", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeGreaterThanOrEqualTo(String value) {
            addCriterion("orderCode >=", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeLessThan(String value) {
            addCriterion("orderCode <", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeLessThanOrEqualTo(String value) {
            addCriterion("orderCode <=", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeLike(String value) {
            addCriterion("orderCode like", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeNotLike(String value) {
            addCriterion("orderCode not like", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeIn(List<String> values) {
            addCriterion("orderCode in", values, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeNotIn(List<String> values) {
            addCriterion("orderCode not in", values, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeBetween(String value1, String value2) {
            addCriterion("orderCode between", value1, value2, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeNotBetween(String value1, String value2) {
            addCriterion("orderCode not between", value1, value2, "orderCode");
            return (Criteria) this;
        }

        public Criteria andTotalpayIsNull() {
            addCriterion("totalpay is null");
            return (Criteria) this;
        }

        public Criteria andTotalpayIsNotNull() {
            addCriterion("totalpay is not null");
            return (Criteria) this;
        }

        public Criteria andTotalpayEqualTo(BigDecimal value) {
            addCriterion("totalpay =", value, "totalpay");
            return (Criteria) this;
        }

        public Criteria andTotalpayNotEqualTo(BigDecimal value) {
            addCriterion("totalpay <>", value, "totalpay");
            return (Criteria) this;
        }

        public Criteria andTotalpayGreaterThan(BigDecimal value) {
            addCriterion("totalpay >", value, "totalpay");
            return (Criteria) this;
        }

        public Criteria andTotalpayGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("totalpay >=", value, "totalpay");
            return (Criteria) this;
        }

        public Criteria andTotalpayLessThan(BigDecimal value) {
            addCriterion("totalpay <", value, "totalpay");
            return (Criteria) this;
        }

        public Criteria andTotalpayLessThanOrEqualTo(BigDecimal value) {
            addCriterion("totalpay <=", value, "totalpay");
            return (Criteria) this;
        }

        public Criteria andTotalpayIn(List<BigDecimal> values) {
            addCriterion("totalpay in", values, "totalpay");
            return (Criteria) this;
        }

        public Criteria andTotalpayNotIn(List<BigDecimal> values) {
            addCriterion("totalpay not in", values, "totalpay");
            return (Criteria) this;
        }

        public Criteria andTotalpayBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("totalpay between", value1, value2, "totalpay");
            return (Criteria) this;
        }

        public Criteria andTotalpayNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("totalpay not between", value1, value2, "totalpay");
            return (Criteria) this;
        }

        public Criteria andWidIsNull() {
            addCriterion("wid is null");
            return (Criteria) this;
        }

        public Criteria andWidIsNotNull() {
            addCriterion("wid is not null");
            return (Criteria) this;
        }

        public Criteria andWidEqualTo(Integer value) {
            addCriterion("wid =", value, "wid");
            return (Criteria) this;
        }

        public Criteria andWidNotEqualTo(Integer value) {
            addCriterion("wid <>", value, "wid");
            return (Criteria) this;
        }

        public Criteria andWidGreaterThan(Integer value) {
            addCriterion("wid >", value, "wid");
            return (Criteria) this;
        }

        public Criteria andWidGreaterThanOrEqualTo(Integer value) {
            addCriterion("wid >=", value, "wid");
            return (Criteria) this;
        }

        public Criteria andWidLessThan(Integer value) {
            addCriterion("wid <", value, "wid");
            return (Criteria) this;
        }

        public Criteria andWidLessThanOrEqualTo(Integer value) {
            addCriterion("wid <=", value, "wid");
            return (Criteria) this;
        }

        public Criteria andWidIn(List<Integer> values) {
            addCriterion("wid in", values, "wid");
            return (Criteria) this;
        }

        public Criteria andWidNotIn(List<Integer> values) {
            addCriterion("wid not in", values, "wid");
            return (Criteria) this;
        }

        public Criteria andWidBetween(Integer value1, Integer value2) {
            addCriterion("wid between", value1, value2, "wid");
            return (Criteria) this;
        }

        public Criteria andWidNotBetween(Integer value1, Integer value2) {
            addCriterion("wid not between", value1, value2, "wid");
            return (Criteria) this;
        }

        public Criteria andBusinesspayIsNull() {
            addCriterion("businesspay is null");
            return (Criteria) this;
        }

        public Criteria andBusinesspayIsNotNull() {
            addCriterion("businesspay is not null");
            return (Criteria) this;
        }

        public Criteria andBusinesspayEqualTo(BigDecimal value) {
            addCriterion("businesspay =", value, "businesspay");
            return (Criteria) this;
        }

        public Criteria andBusinesspayNotEqualTo(BigDecimal value) {
            addCriterion("businesspay <>", value, "businesspay");
            return (Criteria) this;
        }

        public Criteria andBusinesspayGreaterThan(BigDecimal value) {
            addCriterion("businesspay >", value, "businesspay");
            return (Criteria) this;
        }

        public Criteria andBusinesspayGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("businesspay >=", value, "businesspay");
            return (Criteria) this;
        }

        public Criteria andBusinesspayLessThan(BigDecimal value) {
            addCriterion("businesspay <", value, "businesspay");
            return (Criteria) this;
        }

        public Criteria andBusinesspayLessThanOrEqualTo(BigDecimal value) {
            addCriterion("businesspay <=", value, "businesspay");
            return (Criteria) this;
        }

        public Criteria andBusinesspayIn(List<BigDecimal> values) {
            addCriterion("businesspay in", values, "businesspay");
            return (Criteria) this;
        }

        public Criteria andBusinesspayNotIn(List<BigDecimal> values) {
            addCriterion("businesspay not in", values, "businesspay");
            return (Criteria) this;
        }

        public Criteria andBusinesspayBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("businesspay between", value1, value2, "businesspay");
            return (Criteria) this;
        }

        public Criteria andBusinesspayNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("businesspay not between", value1, value2, "businesspay");
            return (Criteria) this;
        }

        public Criteria andBusinessgetIsNull() {
            addCriterion("businessget is null");
            return (Criteria) this;
        }

        public Criteria andBusinessgetIsNotNull() {
            addCriterion("businessget is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessgetEqualTo(BigDecimal value) {
            addCriterion("businessget =", value, "businessget");
            return (Criteria) this;
        }

        public Criteria andBusinessgetNotEqualTo(BigDecimal value) {
            addCriterion("businessget <>", value, "businessget");
            return (Criteria) this;
        }

        public Criteria andBusinessgetGreaterThan(BigDecimal value) {
            addCriterion("businessget >", value, "businessget");
            return (Criteria) this;
        }

        public Criteria andBusinessgetGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("businessget >=", value, "businessget");
            return (Criteria) this;
        }

        public Criteria andBusinessgetLessThan(BigDecimal value) {
            addCriterion("businessget <", value, "businessget");
            return (Criteria) this;
        }

        public Criteria andBusinessgetLessThanOrEqualTo(BigDecimal value) {
            addCriterion("businessget <=", value, "businessget");
            return (Criteria) this;
        }

        public Criteria andBusinessgetIn(List<BigDecimal> values) {
            addCriterion("businessget in", values, "businessget");
            return (Criteria) this;
        }

        public Criteria andBusinessgetNotIn(List<BigDecimal> values) {
            addCriterion("businessget not in", values, "businessget");
            return (Criteria) this;
        }

        public Criteria andBusinessgetBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("businessget between", value1, value2, "businessget");
            return (Criteria) this;
        }

        public Criteria andBusinessgetNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("businessget not between", value1, value2, "businessget");
            return (Criteria) this;
        }

        public Criteria andAgentIdIsNull() {
            addCriterion("agentId is null");
            return (Criteria) this;
        }

        public Criteria andAgentIdIsNotNull() {
            addCriterion("agentId is not null");
            return (Criteria) this;
        }

        public Criteria andAgentIdEqualTo(Integer value) {
            addCriterion("agentId =", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotEqualTo(Integer value) {
            addCriterion("agentId <>", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdGreaterThan(Integer value) {
            addCriterion("agentId >", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("agentId >=", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdLessThan(Integer value) {
            addCriterion("agentId <", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdLessThanOrEqualTo(Integer value) {
            addCriterion("agentId <=", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdIn(List<Integer> values) {
            addCriterion("agentId in", values, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotIn(List<Integer> values) {
            addCriterion("agentId not in", values, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdBetween(Integer value1, Integer value2) {
            addCriterion("agentId between", value1, value2, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotBetween(Integer value1, Integer value2) {
            addCriterion("agentId not between", value1, value2, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentNameIsNull() {
            addCriterion("agentName is null");
            return (Criteria) this;
        }

        public Criteria andAgentNameIsNotNull() {
            addCriterion("agentName is not null");
            return (Criteria) this;
        }

        public Criteria andAgentNameEqualTo(String value) {
            addCriterion("agentName =", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotEqualTo(String value) {
            addCriterion("agentName <>", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameGreaterThan(String value) {
            addCriterion("agentName >", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameGreaterThanOrEqualTo(String value) {
            addCriterion("agentName >=", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameLessThan(String value) {
            addCriterion("agentName <", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameLessThanOrEqualTo(String value) {
            addCriterion("agentName <=", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameLike(String value) {
            addCriterion("agentName like", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotLike(String value) {
            addCriterion("agentName not like", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameIn(List<String> values) {
            addCriterion("agentName in", values, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotIn(List<String> values) {
            addCriterion("agentName not in", values, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameBetween(String value1, String value2) {
            addCriterion("agentName between", value1, value2, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotBetween(String value1, String value2) {
            addCriterion("agentName not between", value1, value2, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentgetIsNull() {
            addCriterion("agentget is null");
            return (Criteria) this;
        }

        public Criteria andAgentgetIsNotNull() {
            addCriterion("agentget is not null");
            return (Criteria) this;
        }

        public Criteria andAgentgetEqualTo(BigDecimal value) {
            addCriterion("agentget =", value, "agentget");
            return (Criteria) this;
        }

        public Criteria andAgentgetNotEqualTo(BigDecimal value) {
            addCriterion("agentget <>", value, "agentget");
            return (Criteria) this;
        }

        public Criteria andAgentgetGreaterThan(BigDecimal value) {
            addCriterion("agentget >", value, "agentget");
            return (Criteria) this;
        }

        public Criteria andAgentgetGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("agentget >=", value, "agentget");
            return (Criteria) this;
        }

        public Criteria andAgentgetLessThan(BigDecimal value) {
            addCriterion("agentget <", value, "agentget");
            return (Criteria) this;
        }

        public Criteria andAgentgetLessThanOrEqualTo(BigDecimal value) {
            addCriterion("agentget <=", value, "agentget");
            return (Criteria) this;
        }

        public Criteria andAgentgetIn(List<BigDecimal> values) {
            addCriterion("agentget in", values, "agentget");
            return (Criteria) this;
        }

        public Criteria andAgentgetNotIn(List<BigDecimal> values) {
            addCriterion("agentget not in", values, "agentget");
            return (Criteria) this;
        }

        public Criteria andAgentgetBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("agentget between", value1, value2, "agentget");
            return (Criteria) this;
        }

        public Criteria andAgentgetNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("agentget not between", value1, value2, "agentget");
            return (Criteria) this;
        }

        public Criteria andShowpsIsNull() {
            addCriterion("showps is null");
            return (Criteria) this;
        }

        public Criteria andShowpsIsNotNull() {
            addCriterion("showps is not null");
            return (Criteria) this;
        }

        public Criteria andShowpsEqualTo(BigDecimal value) {
            addCriterion("showps =", value, "showps");
            return (Criteria) this;
        }

        public Criteria andShowpsNotEqualTo(BigDecimal value) {
            addCriterion("showps <>", value, "showps");
            return (Criteria) this;
        }

        public Criteria andShowpsGreaterThan(BigDecimal value) {
            addCriterion("showps >", value, "showps");
            return (Criteria) this;
        }

        public Criteria andShowpsGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("showps >=", value, "showps");
            return (Criteria) this;
        }

        public Criteria andShowpsLessThan(BigDecimal value) {
            addCriterion("showps <", value, "showps");
            return (Criteria) this;
        }

        public Criteria andShowpsLessThanOrEqualTo(BigDecimal value) {
            addCriterion("showps <=", value, "showps");
            return (Criteria) this;
        }

        public Criteria andShowpsIn(List<BigDecimal> values) {
            addCriterion("showps in", values, "showps");
            return (Criteria) this;
        }

        public Criteria andShowpsNotIn(List<BigDecimal> values) {
            addCriterion("showps not in", values, "showps");
            return (Criteria) this;
        }

        public Criteria andShowpsBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("showps between", value1, value2, "showps");
            return (Criteria) this;
        }

        public Criteria andShowpsNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("showps not between", value1, value2, "showps");
            return (Criteria) this;
        }

        public Criteria andUsermunberIsNull() {
            addCriterion("usermunber is null");
            return (Criteria) this;
        }

        public Criteria andUsermunberIsNotNull() {
            addCriterion("usermunber is not null");
            return (Criteria) this;
        }

        public Criteria andUsermunberEqualTo(String value) {
            addCriterion("usermunber =", value, "usermunber");
            return (Criteria) this;
        }

        public Criteria andUsermunberNotEqualTo(String value) {
            addCriterion("usermunber <>", value, "usermunber");
            return (Criteria) this;
        }

        public Criteria andUsermunberGreaterThan(String value) {
            addCriterion("usermunber >", value, "usermunber");
            return (Criteria) this;
        }

        public Criteria andUsermunberGreaterThanOrEqualTo(String value) {
            addCriterion("usermunber >=", value, "usermunber");
            return (Criteria) this;
        }

        public Criteria andUsermunberLessThan(String value) {
            addCriterion("usermunber <", value, "usermunber");
            return (Criteria) this;
        }

        public Criteria andUsermunberLessThanOrEqualTo(String value) {
            addCriterion("usermunber <=", value, "usermunber");
            return (Criteria) this;
        }

        public Criteria andUsermunberLike(String value) {
            addCriterion("usermunber like", value, "usermunber");
            return (Criteria) this;
        }

        public Criteria andUsermunberNotLike(String value) {
            addCriterion("usermunber not like", value, "usermunber");
            return (Criteria) this;
        }

        public Criteria andUsermunberIn(List<String> values) {
            addCriterion("usermunber in", values, "usermunber");
            return (Criteria) this;
        }

        public Criteria andUsermunberNotIn(List<String> values) {
            addCriterion("usermunber not in", values, "usermunber");
            return (Criteria) this;
        }

        public Criteria andUsermunberBetween(String value1, String value2) {
            addCriterion("usermunber between", value1, value2, "usermunber");
            return (Criteria) this;
        }

        public Criteria andUsermunberNotBetween(String value1, String value2) {
            addCriterion("usermunber not between", value1, value2, "usermunber");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}