package com.runfast.waimai.dao.model;

import java.io.Serializable;
import java.util.Date;

public class RunfastBusinessAccounts implements Serializable {
    private Integer id;

    private String accounts;

    private Integer businessId;

    private String businessName;

    private Date createTime;

    private String name;

    private String password;

    private Integer purview;

    private String byRoles;

    private String mobile;

    private Integer agentId;

    private String agentName;

    private String bdpushChannelId;

    private String bdpushUserId;

    private Integer bptype;

    private Integer ispush;

    private String otherId;

    private Integer pushType;

    private String alias;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public RunfastBusinessAccounts withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccounts() {
        return accounts;
    }

    public RunfastBusinessAccounts withAccounts(String accounts) {
        this.setAccounts(accounts);
        return this;
    }

    public void setAccounts(String accounts) {
        this.accounts = accounts == null ? null : accounts.trim();
    }

    public Integer getBusinessId() {
        return businessId;
    }

    public RunfastBusinessAccounts withBusinessId(Integer businessId) {
        this.setBusinessId(businessId);
        return this;
    }

    public void setBusinessId(Integer businessId) {
        this.businessId = businessId;
    }

    public String getBusinessName() {
        return businessName;
    }

    public RunfastBusinessAccounts withBusinessName(String businessName) {
        this.setBusinessName(businessName);
        return this;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName == null ? null : businessName.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public RunfastBusinessAccounts withCreateTime(Date createTime) {
        this.setCreateTime(createTime);
        return this;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getName() {
        return name;
    }

    public RunfastBusinessAccounts withName(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getPassword() {
        return password;
    }

    public RunfastBusinessAccounts withPassword(String password) {
        this.setPassword(password);
        return this;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public Integer getPurview() {
        return purview;
    }

    public RunfastBusinessAccounts withPurview(Integer purview) {
        this.setPurview(purview);
        return this;
    }

    public void setPurview(Integer purview) {
        this.purview = purview;
    }

    public String getByRoles() {
        return byRoles;
    }

    public RunfastBusinessAccounts withByRoles(String byRoles) {
        this.setByRoles(byRoles);
        return this;
    }

    public void setByRoles(String byRoles) {
        this.byRoles = byRoles == null ? null : byRoles.trim();
    }

    public String getMobile() {
        return mobile;
    }

    public RunfastBusinessAccounts withMobile(String mobile) {
        this.setMobile(mobile);
        return this;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public Integer getAgentId() {
        return agentId;
    }

    public RunfastBusinessAccounts withAgentId(Integer agentId) {
        this.setAgentId(agentId);
        return this;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public String getAgentName() {
        return agentName;
    }

    public RunfastBusinessAccounts withAgentName(String agentName) {
        this.setAgentName(agentName);
        return this;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName == null ? null : agentName.trim();
    }

    public String getBdpushChannelId() {
        return bdpushChannelId;
    }

    public RunfastBusinessAccounts withBdpushChannelId(String bdpushChannelId) {
        this.setBdpushChannelId(bdpushChannelId);
        return this;
    }

    public void setBdpushChannelId(String bdpushChannelId) {
        this.bdpushChannelId = bdpushChannelId == null ? null : bdpushChannelId.trim();
    }

    public String getBdpushUserId() {
        return bdpushUserId;
    }

    public RunfastBusinessAccounts withBdpushUserId(String bdpushUserId) {
        this.setBdpushUserId(bdpushUserId);
        return this;
    }

    public void setBdpushUserId(String bdpushUserId) {
        this.bdpushUserId = bdpushUserId == null ? null : bdpushUserId.trim();
    }

    public Integer getBptype() {
        return bptype;
    }

    public RunfastBusinessAccounts withBptype(Integer bptype) {
        this.setBptype(bptype);
        return this;
    }

    public void setBptype(Integer bptype) {
        this.bptype = bptype;
    }

    public Integer getIspush() {
        return ispush;
    }

    public RunfastBusinessAccounts withIspush(Integer ispush) {
        this.setIspush(ispush);
        return this;
    }

    public void setIspush(Integer ispush) {
        this.ispush = ispush;
    }

    public String getOtherId() {
        return otherId;
    }

    public RunfastBusinessAccounts withOtherId(String otherId) {
        this.setOtherId(otherId);
        return this;
    }

    public void setOtherId(String otherId) {
        this.otherId = otherId == null ? null : otherId.trim();
    }

    public Integer getPushType() {
        return pushType;
    }

    public RunfastBusinessAccounts withPushType(Integer pushType) {
        this.setPushType(pushType);
        return this;
    }

    public void setPushType(Integer pushType) {
        this.pushType = pushType;
    }

    public String getAlias() {
        return alias;
    }

    public RunfastBusinessAccounts withAlias(String alias) {
        this.setAlias(alias);
        return this;
    }

    public void setAlias(String alias) {
        this.alias = alias == null ? null : alias.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", accounts=").append(accounts);
        sb.append(", businessId=").append(businessId);
        sb.append(", businessName=").append(businessName);
        sb.append(", createTime=").append(createTime);
        sb.append(", name=").append(name);
        sb.append(", password=").append(password);
        sb.append(", purview=").append(purview);
        sb.append(", byRoles=").append(byRoles);
        sb.append(", mobile=").append(mobile);
        sb.append(", agentId=").append(agentId);
        sb.append(", agentName=").append(agentName);
        sb.append(", bdpushChannelId=").append(bdpushChannelId);
        sb.append(", bdpushUserId=").append(bdpushUserId);
        sb.append(", bptype=").append(bptype);
        sb.append(", ispush=").append(ispush);
        sb.append(", otherId=").append(otherId);
        sb.append(", pushType=").append(pushType);
        sb.append(", alias=").append(alias);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}