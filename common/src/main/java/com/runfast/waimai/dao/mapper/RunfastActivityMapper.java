package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastActivity;
import com.runfast.waimai.dao.model.RunfastActivityExample;
import com.runfast.waimai.dao.model.RunfastGoodsSell;
import com.runfast.waimai.web.dto.ActivityDto;
import com.runfast.waimai.web.dto.BusinessDto;
import com.runfast.waimai.web.dto.TargetWithOneActivityDto;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Mapper
public interface RunfastActivityMapper extends IMapper<RunfastActivity, Integer, RunfastActivityExample> {

    List<Map<String,Object>> getBusinessValidActivities(int businessId, ArrayList<Integer> typeList);

    List<RunfastActivity> getAgentZoneActivityIn(int agentId, List<Integer> activityTypeList);

    RunfastActivity getActivityWithTarget(Integer activityId, Pageable pageable);

    List<TargetWithOneActivityDto> findBusinessActivity(Integer businessId, List<Integer> activityTypeList);

    List<RunfastActivity> findAgentRed(Integer agentId);

    RunfastActivity get(Integer redId);

    List<ActivityDto> findBusinessActivityDto(Integer businessId);

    List<RunfastActivity> findRedActivity(Integer agentId);

    List<RunfastActivity> findBusinessRedActivity(Integer businessId, Integer activityType);

    Integer resetRedDay(Integer activityId);
}