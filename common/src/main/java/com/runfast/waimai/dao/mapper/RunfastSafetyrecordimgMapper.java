package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastSafetyrecordimg;
import com.runfast.waimai.dao.model.RunfastSafetyrecordimgExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastSafetyrecordimgMapper extends IMapper<RunfastSafetyrecordimg, Integer, RunfastSafetyrecordimgExample> {
}