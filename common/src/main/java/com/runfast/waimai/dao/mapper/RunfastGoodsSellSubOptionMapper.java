package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastGoodsSellSubOption;
import com.runfast.waimai.dao.model.RunfastGoodsSellSubOptionExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastGoodsSellSubOptionMapper extends IMapper<RunfastGoodsSellSubOption, Integer, RunfastGoodsSellSubOptionExample> {
}