package com.runfast.waimai.dao.model;

import java.io.Serializable;
import java.util.Date;

public class RunfastVisitRecord implements Serializable {
    private Integer id;

    private Integer cid;

    private String cname;

    private Date createTime;

    private Integer pid;

    private String pname;

    private Integer type;

    private String visitdate;

    private Integer visitnum;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public RunfastVisitRecord withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCid() {
        return cid;
    }

    public RunfastVisitRecord withCid(Integer cid) {
        this.setCid(cid);
        return this;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public String getCname() {
        return cname;
    }

    public RunfastVisitRecord withCname(String cname) {
        this.setCname(cname);
        return this;
    }

    public void setCname(String cname) {
        this.cname = cname == null ? null : cname.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public RunfastVisitRecord withCreateTime(Date createTime) {
        this.setCreateTime(createTime);
        return this;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getPid() {
        return pid;
    }

    public RunfastVisitRecord withPid(Integer pid) {
        this.setPid(pid);
        return this;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getPname() {
        return pname;
    }

    public RunfastVisitRecord withPname(String pname) {
        this.setPname(pname);
        return this;
    }

    public void setPname(String pname) {
        this.pname = pname == null ? null : pname.trim();
    }

    public Integer getType() {
        return type;
    }

    public RunfastVisitRecord withType(Integer type) {
        this.setType(type);
        return this;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getVisitdate() {
        return visitdate;
    }

    public RunfastVisitRecord withVisitdate(String visitdate) {
        this.setVisitdate(visitdate);
        return this;
    }

    public void setVisitdate(String visitdate) {
        this.visitdate = visitdate == null ? null : visitdate.trim();
    }

    public Integer getVisitnum() {
        return visitnum;
    }

    public RunfastVisitRecord withVisitnum(Integer visitnum) {
        this.setVisitnum(visitnum);
        return this;
    }

    public void setVisitnum(Integer visitnum) {
        this.visitnum = visitnum;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", cid=").append(cid);
        sb.append(", cname=").append(cname);
        sb.append(", createTime=").append(createTime);
        sb.append(", pid=").append(pid);
        sb.append(", pname=").append(pname);
        sb.append(", type=").append(type);
        sb.append(", visitdate=").append(visitdate);
        sb.append(", visitnum=").append(visitnum);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}