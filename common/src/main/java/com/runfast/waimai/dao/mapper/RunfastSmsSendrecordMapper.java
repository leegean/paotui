package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastSmsSendrecord;
import com.runfast.waimai.dao.model.RunfastSmsSendrecordExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastSmsSendrecordMapper extends IMapper<RunfastSmsSendrecord, Integer, RunfastSmsSendrecordExample> {
}