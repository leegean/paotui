package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastGoodsSell;
import com.runfast.waimai.dao.model.RunfastGoodsSellExample;
import com.runfast.waimai.entity.GoodsOptionPair;
import com.runfast.waimai.entity.GoodsStandarPair;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.data.domain.Pageable;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Mapper
public interface RunfastGoodsSellMapper extends IMapper<RunfastGoodsSell, Integer, RunfastGoodsSellExample> {
    List<RunfastGoodsSell> search(String name, Integer businessId, Pageable pageable);

    List<Map<String,Object>> findAgentOffZoneGoods(Integer agentId, double userLng, double userLat, Date deliveryStart1, Date deliveryEnd1, Date deliveryStart2, Date deliveryEnd2, Pageable pageable);


    List<Map<String, Object>> checkGoodsWithStandar(Integer businessId, Set<GoodsStandarPair> goodsStandarPairSet);

    List<Map<String, Object>> checkGoodsWithOption(Integer businessId, Set<GoodsOptionPair> goodsOptionPairSet);

    RunfastGoodsSell detail(int goodsId);

    Long countOfRequired(Integer businessId);
}