package com.runfast.waimai.dao.model;

import java.io.Serializable;

public class RunfastGoodsSellSubOption implements Serializable {
    private Integer id;

    private String barCode;

    private Integer del;

    private Integer goodsSellId;

    private String goodsSellName;

    private String name;

    private Integer optionId;

    private String optionName;

    private Integer sort;

    private Integer businessId;

    private String businessName;

    /**
     * 是否删除
     */
    private Boolean deleted;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public RunfastGoodsSellSubOption withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBarCode() {
        return barCode;
    }

    public RunfastGoodsSellSubOption withBarCode(String barCode) {
        this.setBarCode(barCode);
        return this;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode == null ? null : barCode.trim();
    }

    public Integer getDel() {
        return del;
    }

    public RunfastGoodsSellSubOption withDel(Integer del) {
        this.setDel(del);
        return this;
    }

    public void setDel(Integer del) {
        this.del = del;
    }

    public Integer getGoodsSellId() {
        return goodsSellId;
    }

    public RunfastGoodsSellSubOption withGoodsSellId(Integer goodsSellId) {
        this.setGoodsSellId(goodsSellId);
        return this;
    }

    public void setGoodsSellId(Integer goodsSellId) {
        this.goodsSellId = goodsSellId;
    }

    public String getGoodsSellName() {
        return goodsSellName;
    }

    public RunfastGoodsSellSubOption withGoodsSellName(String goodsSellName) {
        this.setGoodsSellName(goodsSellName);
        return this;
    }

    public void setGoodsSellName(String goodsSellName) {
        this.goodsSellName = goodsSellName == null ? null : goodsSellName.trim();
    }

    public String getName() {
        return name;
    }

    public RunfastGoodsSellSubOption withName(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getOptionId() {
        return optionId;
    }

    public RunfastGoodsSellSubOption withOptionId(Integer optionId) {
        this.setOptionId(optionId);
        return this;
    }

    public void setOptionId(Integer optionId) {
        this.optionId = optionId;
    }

    public String getOptionName() {
        return optionName;
    }

    public RunfastGoodsSellSubOption withOptionName(String optionName) {
        this.setOptionName(optionName);
        return this;
    }

    public void setOptionName(String optionName) {
        this.optionName = optionName == null ? null : optionName.trim();
    }

    public Integer getSort() {
        return sort;
    }

    public RunfastGoodsSellSubOption withSort(Integer sort) {
        this.setSort(sort);
        return this;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getBusinessId() {
        return businessId;
    }

    public RunfastGoodsSellSubOption withBusinessId(Integer businessId) {
        this.setBusinessId(businessId);
        return this;
    }

    public void setBusinessId(Integer businessId) {
        this.businessId = businessId;
    }

    public String getBusinessName() {
        return businessName;
    }

    public RunfastGoodsSellSubOption withBusinessName(String businessName) {
        this.setBusinessName(businessName);
        return this;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName == null ? null : businessName.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", barCode=").append(barCode);
        sb.append(", del=").append(del);
        sb.append(", goodsSellId=").append(goodsSellId);
        sb.append(", goodsSellName=").append(goodsSellName);
        sb.append(", name=").append(name);
        sb.append(", optionId=").append(optionId);
        sb.append(", optionName=").append(optionName);
        sb.append(", sort=").append(sort);
        sb.append(", businessId=").append(businessId);
        sb.append(", businessName=").append(businessName);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}