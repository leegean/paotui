package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastOrderActivity;
import com.runfast.waimai.dao.model.RunfastOrderActivityExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastOrderActivityMapper extends IMapper<RunfastOrderActivity, Integer, RunfastOrderActivityExample> {
}