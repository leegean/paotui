package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastScorerecord;
import com.runfast.waimai.dao.model.RunfastScorerecordExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastScorerecordMapper extends IMapper<RunfastScorerecord, Integer, RunfastScorerecordExample> {
}