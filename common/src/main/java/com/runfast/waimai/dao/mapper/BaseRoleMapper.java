package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.BaseRole;
import com.runfast.waimai.dao.model.BaseRoleExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BaseRoleMapper extends IMapper<BaseRole, Integer, BaseRoleExample> {
}