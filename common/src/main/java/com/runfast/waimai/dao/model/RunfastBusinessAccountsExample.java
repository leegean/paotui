package com.runfast.waimai.dao.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RunfastBusinessAccountsExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public RunfastBusinessAccountsExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andAccountsIsNull() {
            addCriterion("accounts is null");
            return (Criteria) this;
        }

        public Criteria andAccountsIsNotNull() {
            addCriterion("accounts is not null");
            return (Criteria) this;
        }

        public Criteria andAccountsEqualTo(String value) {
            addCriterion("accounts =", value, "accounts");
            return (Criteria) this;
        }

        public Criteria andAccountsNotEqualTo(String value) {
            addCriterion("accounts <>", value, "accounts");
            return (Criteria) this;
        }

        public Criteria andAccountsGreaterThan(String value) {
            addCriterion("accounts >", value, "accounts");
            return (Criteria) this;
        }

        public Criteria andAccountsGreaterThanOrEqualTo(String value) {
            addCriterion("accounts >=", value, "accounts");
            return (Criteria) this;
        }

        public Criteria andAccountsLessThan(String value) {
            addCriterion("accounts <", value, "accounts");
            return (Criteria) this;
        }

        public Criteria andAccountsLessThanOrEqualTo(String value) {
            addCriterion("accounts <=", value, "accounts");
            return (Criteria) this;
        }

        public Criteria andAccountsLike(String value) {
            addCriterion("accounts like", value, "accounts");
            return (Criteria) this;
        }

        public Criteria andAccountsNotLike(String value) {
            addCriterion("accounts not like", value, "accounts");
            return (Criteria) this;
        }

        public Criteria andAccountsIn(List<String> values) {
            addCriterion("accounts in", values, "accounts");
            return (Criteria) this;
        }

        public Criteria andAccountsNotIn(List<String> values) {
            addCriterion("accounts not in", values, "accounts");
            return (Criteria) this;
        }

        public Criteria andAccountsBetween(String value1, String value2) {
            addCriterion("accounts between", value1, value2, "accounts");
            return (Criteria) this;
        }

        public Criteria andAccountsNotBetween(String value1, String value2) {
            addCriterion("accounts not between", value1, value2, "accounts");
            return (Criteria) this;
        }

        public Criteria andBusinessIdIsNull() {
            addCriterion("businessId is null");
            return (Criteria) this;
        }

        public Criteria andBusinessIdIsNotNull() {
            addCriterion("businessId is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessIdEqualTo(Integer value) {
            addCriterion("businessId =", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdNotEqualTo(Integer value) {
            addCriterion("businessId <>", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdGreaterThan(Integer value) {
            addCriterion("businessId >", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("businessId >=", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdLessThan(Integer value) {
            addCriterion("businessId <", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdLessThanOrEqualTo(Integer value) {
            addCriterion("businessId <=", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdIn(List<Integer> values) {
            addCriterion("businessId in", values, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdNotIn(List<Integer> values) {
            addCriterion("businessId not in", values, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdBetween(Integer value1, Integer value2) {
            addCriterion("businessId between", value1, value2, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdNotBetween(Integer value1, Integer value2) {
            addCriterion("businessId not between", value1, value2, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessNameIsNull() {
            addCriterion("businessName is null");
            return (Criteria) this;
        }

        public Criteria andBusinessNameIsNotNull() {
            addCriterion("businessName is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessNameEqualTo(String value) {
            addCriterion("businessName =", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameNotEqualTo(String value) {
            addCriterion("businessName <>", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameGreaterThan(String value) {
            addCriterion("businessName >", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameGreaterThanOrEqualTo(String value) {
            addCriterion("businessName >=", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameLessThan(String value) {
            addCriterion("businessName <", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameLessThanOrEqualTo(String value) {
            addCriterion("businessName <=", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameLike(String value) {
            addCriterion("businessName like", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameNotLike(String value) {
            addCriterion("businessName not like", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameIn(List<String> values) {
            addCriterion("businessName in", values, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameNotIn(List<String> values) {
            addCriterion("businessName not in", values, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameBetween(String value1, String value2) {
            addCriterion("businessName between", value1, value2, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameNotBetween(String value1, String value2) {
            addCriterion("businessName not between", value1, value2, "businessName");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("createTime is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("createTime is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("createTime =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("createTime <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("createTime >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("createTime >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("createTime <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("createTime <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("createTime in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("createTime not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("createTime between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("createTime not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andPasswordIsNull() {
            addCriterion("password is null");
            return (Criteria) this;
        }

        public Criteria andPasswordIsNotNull() {
            addCriterion("password is not null");
            return (Criteria) this;
        }

        public Criteria andPasswordEqualTo(String value) {
            addCriterion("password =", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotEqualTo(String value) {
            addCriterion("password <>", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordGreaterThan(String value) {
            addCriterion("password >", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordGreaterThanOrEqualTo(String value) {
            addCriterion("password >=", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLessThan(String value) {
            addCriterion("password <", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLessThanOrEqualTo(String value) {
            addCriterion("password <=", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLike(String value) {
            addCriterion("password like", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotLike(String value) {
            addCriterion("password not like", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordIn(List<String> values) {
            addCriterion("password in", values, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotIn(List<String> values) {
            addCriterion("password not in", values, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordBetween(String value1, String value2) {
            addCriterion("password between", value1, value2, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotBetween(String value1, String value2) {
            addCriterion("password not between", value1, value2, "password");
            return (Criteria) this;
        }

        public Criteria andPurviewIsNull() {
            addCriterion("purview is null");
            return (Criteria) this;
        }

        public Criteria andPurviewIsNotNull() {
            addCriterion("purview is not null");
            return (Criteria) this;
        }

        public Criteria andPurviewEqualTo(Integer value) {
            addCriterion("purview =", value, "purview");
            return (Criteria) this;
        }

        public Criteria andPurviewNotEqualTo(Integer value) {
            addCriterion("purview <>", value, "purview");
            return (Criteria) this;
        }

        public Criteria andPurviewGreaterThan(Integer value) {
            addCriterion("purview >", value, "purview");
            return (Criteria) this;
        }

        public Criteria andPurviewGreaterThanOrEqualTo(Integer value) {
            addCriterion("purview >=", value, "purview");
            return (Criteria) this;
        }

        public Criteria andPurviewLessThan(Integer value) {
            addCriterion("purview <", value, "purview");
            return (Criteria) this;
        }

        public Criteria andPurviewLessThanOrEqualTo(Integer value) {
            addCriterion("purview <=", value, "purview");
            return (Criteria) this;
        }

        public Criteria andPurviewIn(List<Integer> values) {
            addCriterion("purview in", values, "purview");
            return (Criteria) this;
        }

        public Criteria andPurviewNotIn(List<Integer> values) {
            addCriterion("purview not in", values, "purview");
            return (Criteria) this;
        }

        public Criteria andPurviewBetween(Integer value1, Integer value2) {
            addCriterion("purview between", value1, value2, "purview");
            return (Criteria) this;
        }

        public Criteria andPurviewNotBetween(Integer value1, Integer value2) {
            addCriterion("purview not between", value1, value2, "purview");
            return (Criteria) this;
        }

        public Criteria andByRolesIsNull() {
            addCriterion("byRoles is null");
            return (Criteria) this;
        }

        public Criteria andByRolesIsNotNull() {
            addCriterion("byRoles is not null");
            return (Criteria) this;
        }

        public Criteria andByRolesEqualTo(String value) {
            addCriterion("byRoles =", value, "byRoles");
            return (Criteria) this;
        }

        public Criteria andByRolesNotEqualTo(String value) {
            addCriterion("byRoles <>", value, "byRoles");
            return (Criteria) this;
        }

        public Criteria andByRolesGreaterThan(String value) {
            addCriterion("byRoles >", value, "byRoles");
            return (Criteria) this;
        }

        public Criteria andByRolesGreaterThanOrEqualTo(String value) {
            addCriterion("byRoles >=", value, "byRoles");
            return (Criteria) this;
        }

        public Criteria andByRolesLessThan(String value) {
            addCriterion("byRoles <", value, "byRoles");
            return (Criteria) this;
        }

        public Criteria andByRolesLessThanOrEqualTo(String value) {
            addCriterion("byRoles <=", value, "byRoles");
            return (Criteria) this;
        }

        public Criteria andByRolesLike(String value) {
            addCriterion("byRoles like", value, "byRoles");
            return (Criteria) this;
        }

        public Criteria andByRolesNotLike(String value) {
            addCriterion("byRoles not like", value, "byRoles");
            return (Criteria) this;
        }

        public Criteria andByRolesIn(List<String> values) {
            addCriterion("byRoles in", values, "byRoles");
            return (Criteria) this;
        }

        public Criteria andByRolesNotIn(List<String> values) {
            addCriterion("byRoles not in", values, "byRoles");
            return (Criteria) this;
        }

        public Criteria andByRolesBetween(String value1, String value2) {
            addCriterion("byRoles between", value1, value2, "byRoles");
            return (Criteria) this;
        }

        public Criteria andByRolesNotBetween(String value1, String value2) {
            addCriterion("byRoles not between", value1, value2, "byRoles");
            return (Criteria) this;
        }

        public Criteria andMobileIsNull() {
            addCriterion("mobile is null");
            return (Criteria) this;
        }

        public Criteria andMobileIsNotNull() {
            addCriterion("mobile is not null");
            return (Criteria) this;
        }

        public Criteria andMobileEqualTo(String value) {
            addCriterion("mobile =", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotEqualTo(String value) {
            addCriterion("mobile <>", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileGreaterThan(String value) {
            addCriterion("mobile >", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileGreaterThanOrEqualTo(String value) {
            addCriterion("mobile >=", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLessThan(String value) {
            addCriterion("mobile <", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLessThanOrEqualTo(String value) {
            addCriterion("mobile <=", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLike(String value) {
            addCriterion("mobile like", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotLike(String value) {
            addCriterion("mobile not like", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileIn(List<String> values) {
            addCriterion("mobile in", values, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotIn(List<String> values) {
            addCriterion("mobile not in", values, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileBetween(String value1, String value2) {
            addCriterion("mobile between", value1, value2, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotBetween(String value1, String value2) {
            addCriterion("mobile not between", value1, value2, "mobile");
            return (Criteria) this;
        }

        public Criteria andAgentIdIsNull() {
            addCriterion("agentId is null");
            return (Criteria) this;
        }

        public Criteria andAgentIdIsNotNull() {
            addCriterion("agentId is not null");
            return (Criteria) this;
        }

        public Criteria andAgentIdEqualTo(Integer value) {
            addCriterion("agentId =", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotEqualTo(Integer value) {
            addCriterion("agentId <>", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdGreaterThan(Integer value) {
            addCriterion("agentId >", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("agentId >=", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdLessThan(Integer value) {
            addCriterion("agentId <", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdLessThanOrEqualTo(Integer value) {
            addCriterion("agentId <=", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdIn(List<Integer> values) {
            addCriterion("agentId in", values, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotIn(List<Integer> values) {
            addCriterion("agentId not in", values, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdBetween(Integer value1, Integer value2) {
            addCriterion("agentId between", value1, value2, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotBetween(Integer value1, Integer value2) {
            addCriterion("agentId not between", value1, value2, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentNameIsNull() {
            addCriterion("agentName is null");
            return (Criteria) this;
        }

        public Criteria andAgentNameIsNotNull() {
            addCriterion("agentName is not null");
            return (Criteria) this;
        }

        public Criteria andAgentNameEqualTo(String value) {
            addCriterion("agentName =", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotEqualTo(String value) {
            addCriterion("agentName <>", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameGreaterThan(String value) {
            addCriterion("agentName >", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameGreaterThanOrEqualTo(String value) {
            addCriterion("agentName >=", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameLessThan(String value) {
            addCriterion("agentName <", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameLessThanOrEqualTo(String value) {
            addCriterion("agentName <=", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameLike(String value) {
            addCriterion("agentName like", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotLike(String value) {
            addCriterion("agentName not like", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameIn(List<String> values) {
            addCriterion("agentName in", values, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotIn(List<String> values) {
            addCriterion("agentName not in", values, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameBetween(String value1, String value2) {
            addCriterion("agentName between", value1, value2, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotBetween(String value1, String value2) {
            addCriterion("agentName not between", value1, value2, "agentName");
            return (Criteria) this;
        }

        public Criteria andBdpushChannelIdIsNull() {
            addCriterion("bdpushChannelId is null");
            return (Criteria) this;
        }

        public Criteria andBdpushChannelIdIsNotNull() {
            addCriterion("bdpushChannelId is not null");
            return (Criteria) this;
        }

        public Criteria andBdpushChannelIdEqualTo(String value) {
            addCriterion("bdpushChannelId =", value, "bdpushChannelId");
            return (Criteria) this;
        }

        public Criteria andBdpushChannelIdNotEqualTo(String value) {
            addCriterion("bdpushChannelId <>", value, "bdpushChannelId");
            return (Criteria) this;
        }

        public Criteria andBdpushChannelIdGreaterThan(String value) {
            addCriterion("bdpushChannelId >", value, "bdpushChannelId");
            return (Criteria) this;
        }

        public Criteria andBdpushChannelIdGreaterThanOrEqualTo(String value) {
            addCriterion("bdpushChannelId >=", value, "bdpushChannelId");
            return (Criteria) this;
        }

        public Criteria andBdpushChannelIdLessThan(String value) {
            addCriterion("bdpushChannelId <", value, "bdpushChannelId");
            return (Criteria) this;
        }

        public Criteria andBdpushChannelIdLessThanOrEqualTo(String value) {
            addCriterion("bdpushChannelId <=", value, "bdpushChannelId");
            return (Criteria) this;
        }

        public Criteria andBdpushChannelIdLike(String value) {
            addCriterion("bdpushChannelId like", value, "bdpushChannelId");
            return (Criteria) this;
        }

        public Criteria andBdpushChannelIdNotLike(String value) {
            addCriterion("bdpushChannelId not like", value, "bdpushChannelId");
            return (Criteria) this;
        }

        public Criteria andBdpushChannelIdIn(List<String> values) {
            addCriterion("bdpushChannelId in", values, "bdpushChannelId");
            return (Criteria) this;
        }

        public Criteria andBdpushChannelIdNotIn(List<String> values) {
            addCriterion("bdpushChannelId not in", values, "bdpushChannelId");
            return (Criteria) this;
        }

        public Criteria andBdpushChannelIdBetween(String value1, String value2) {
            addCriterion("bdpushChannelId between", value1, value2, "bdpushChannelId");
            return (Criteria) this;
        }

        public Criteria andBdpushChannelIdNotBetween(String value1, String value2) {
            addCriterion("bdpushChannelId not between", value1, value2, "bdpushChannelId");
            return (Criteria) this;
        }

        public Criteria andBdpushUserIdIsNull() {
            addCriterion("bdpushUserId is null");
            return (Criteria) this;
        }

        public Criteria andBdpushUserIdIsNotNull() {
            addCriterion("bdpushUserId is not null");
            return (Criteria) this;
        }

        public Criteria andBdpushUserIdEqualTo(String value) {
            addCriterion("bdpushUserId =", value, "bdpushUserId");
            return (Criteria) this;
        }

        public Criteria andBdpushUserIdNotEqualTo(String value) {
            addCriterion("bdpushUserId <>", value, "bdpushUserId");
            return (Criteria) this;
        }

        public Criteria andBdpushUserIdGreaterThan(String value) {
            addCriterion("bdpushUserId >", value, "bdpushUserId");
            return (Criteria) this;
        }

        public Criteria andBdpushUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("bdpushUserId >=", value, "bdpushUserId");
            return (Criteria) this;
        }

        public Criteria andBdpushUserIdLessThan(String value) {
            addCriterion("bdpushUserId <", value, "bdpushUserId");
            return (Criteria) this;
        }

        public Criteria andBdpushUserIdLessThanOrEqualTo(String value) {
            addCriterion("bdpushUserId <=", value, "bdpushUserId");
            return (Criteria) this;
        }

        public Criteria andBdpushUserIdLike(String value) {
            addCriterion("bdpushUserId like", value, "bdpushUserId");
            return (Criteria) this;
        }

        public Criteria andBdpushUserIdNotLike(String value) {
            addCriterion("bdpushUserId not like", value, "bdpushUserId");
            return (Criteria) this;
        }

        public Criteria andBdpushUserIdIn(List<String> values) {
            addCriterion("bdpushUserId in", values, "bdpushUserId");
            return (Criteria) this;
        }

        public Criteria andBdpushUserIdNotIn(List<String> values) {
            addCriterion("bdpushUserId not in", values, "bdpushUserId");
            return (Criteria) this;
        }

        public Criteria andBdpushUserIdBetween(String value1, String value2) {
            addCriterion("bdpushUserId between", value1, value2, "bdpushUserId");
            return (Criteria) this;
        }

        public Criteria andBdpushUserIdNotBetween(String value1, String value2) {
            addCriterion("bdpushUserId not between", value1, value2, "bdpushUserId");
            return (Criteria) this;
        }

        public Criteria andBptypeIsNull() {
            addCriterion("bptype is null");
            return (Criteria) this;
        }

        public Criteria andBptypeIsNotNull() {
            addCriterion("bptype is not null");
            return (Criteria) this;
        }

        public Criteria andBptypeEqualTo(Integer value) {
            addCriterion("bptype =", value, "bptype");
            return (Criteria) this;
        }

        public Criteria andBptypeNotEqualTo(Integer value) {
            addCriterion("bptype <>", value, "bptype");
            return (Criteria) this;
        }

        public Criteria andBptypeGreaterThan(Integer value) {
            addCriterion("bptype >", value, "bptype");
            return (Criteria) this;
        }

        public Criteria andBptypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("bptype >=", value, "bptype");
            return (Criteria) this;
        }

        public Criteria andBptypeLessThan(Integer value) {
            addCriterion("bptype <", value, "bptype");
            return (Criteria) this;
        }

        public Criteria andBptypeLessThanOrEqualTo(Integer value) {
            addCriterion("bptype <=", value, "bptype");
            return (Criteria) this;
        }

        public Criteria andBptypeIn(List<Integer> values) {
            addCriterion("bptype in", values, "bptype");
            return (Criteria) this;
        }

        public Criteria andBptypeNotIn(List<Integer> values) {
            addCriterion("bptype not in", values, "bptype");
            return (Criteria) this;
        }

        public Criteria andBptypeBetween(Integer value1, Integer value2) {
            addCriterion("bptype between", value1, value2, "bptype");
            return (Criteria) this;
        }

        public Criteria andBptypeNotBetween(Integer value1, Integer value2) {
            addCriterion("bptype not between", value1, value2, "bptype");
            return (Criteria) this;
        }

        public Criteria andIspushIsNull() {
            addCriterion("ispush is null");
            return (Criteria) this;
        }

        public Criteria andIspushIsNotNull() {
            addCriterion("ispush is not null");
            return (Criteria) this;
        }

        public Criteria andIspushEqualTo(Integer value) {
            addCriterion("ispush =", value, "ispush");
            return (Criteria) this;
        }

        public Criteria andIspushNotEqualTo(Integer value) {
            addCriterion("ispush <>", value, "ispush");
            return (Criteria) this;
        }

        public Criteria andIspushGreaterThan(Integer value) {
            addCriterion("ispush >", value, "ispush");
            return (Criteria) this;
        }

        public Criteria andIspushGreaterThanOrEqualTo(Integer value) {
            addCriterion("ispush >=", value, "ispush");
            return (Criteria) this;
        }

        public Criteria andIspushLessThan(Integer value) {
            addCriterion("ispush <", value, "ispush");
            return (Criteria) this;
        }

        public Criteria andIspushLessThanOrEqualTo(Integer value) {
            addCriterion("ispush <=", value, "ispush");
            return (Criteria) this;
        }

        public Criteria andIspushIn(List<Integer> values) {
            addCriterion("ispush in", values, "ispush");
            return (Criteria) this;
        }

        public Criteria andIspushNotIn(List<Integer> values) {
            addCriterion("ispush not in", values, "ispush");
            return (Criteria) this;
        }

        public Criteria andIspushBetween(Integer value1, Integer value2) {
            addCriterion("ispush between", value1, value2, "ispush");
            return (Criteria) this;
        }

        public Criteria andIspushNotBetween(Integer value1, Integer value2) {
            addCriterion("ispush not between", value1, value2, "ispush");
            return (Criteria) this;
        }

        public Criteria andOtherIdIsNull() {
            addCriterion("otherId is null");
            return (Criteria) this;
        }

        public Criteria andOtherIdIsNotNull() {
            addCriterion("otherId is not null");
            return (Criteria) this;
        }

        public Criteria andOtherIdEqualTo(String value) {
            addCriterion("otherId =", value, "otherId");
            return (Criteria) this;
        }

        public Criteria andOtherIdNotEqualTo(String value) {
            addCriterion("otherId <>", value, "otherId");
            return (Criteria) this;
        }

        public Criteria andOtherIdGreaterThan(String value) {
            addCriterion("otherId >", value, "otherId");
            return (Criteria) this;
        }

        public Criteria andOtherIdGreaterThanOrEqualTo(String value) {
            addCriterion("otherId >=", value, "otherId");
            return (Criteria) this;
        }

        public Criteria andOtherIdLessThan(String value) {
            addCriterion("otherId <", value, "otherId");
            return (Criteria) this;
        }

        public Criteria andOtherIdLessThanOrEqualTo(String value) {
            addCriterion("otherId <=", value, "otherId");
            return (Criteria) this;
        }

        public Criteria andOtherIdLike(String value) {
            addCriterion("otherId like", value, "otherId");
            return (Criteria) this;
        }

        public Criteria andOtherIdNotLike(String value) {
            addCriterion("otherId not like", value, "otherId");
            return (Criteria) this;
        }

        public Criteria andOtherIdIn(List<String> values) {
            addCriterion("otherId in", values, "otherId");
            return (Criteria) this;
        }

        public Criteria andOtherIdNotIn(List<String> values) {
            addCriterion("otherId not in", values, "otherId");
            return (Criteria) this;
        }

        public Criteria andOtherIdBetween(String value1, String value2) {
            addCriterion("otherId between", value1, value2, "otherId");
            return (Criteria) this;
        }

        public Criteria andOtherIdNotBetween(String value1, String value2) {
            addCriterion("otherId not between", value1, value2, "otherId");
            return (Criteria) this;
        }

        public Criteria andPushTypeIsNull() {
            addCriterion("pushType is null");
            return (Criteria) this;
        }

        public Criteria andPushTypeIsNotNull() {
            addCriterion("pushType is not null");
            return (Criteria) this;
        }

        public Criteria andPushTypeEqualTo(Integer value) {
            addCriterion("pushType =", value, "pushType");
            return (Criteria) this;
        }

        public Criteria andPushTypeNotEqualTo(Integer value) {
            addCriterion("pushType <>", value, "pushType");
            return (Criteria) this;
        }

        public Criteria andPushTypeGreaterThan(Integer value) {
            addCriterion("pushType >", value, "pushType");
            return (Criteria) this;
        }

        public Criteria andPushTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("pushType >=", value, "pushType");
            return (Criteria) this;
        }

        public Criteria andPushTypeLessThan(Integer value) {
            addCriterion("pushType <", value, "pushType");
            return (Criteria) this;
        }

        public Criteria andPushTypeLessThanOrEqualTo(Integer value) {
            addCriterion("pushType <=", value, "pushType");
            return (Criteria) this;
        }

        public Criteria andPushTypeIn(List<Integer> values) {
            addCriterion("pushType in", values, "pushType");
            return (Criteria) this;
        }

        public Criteria andPushTypeNotIn(List<Integer> values) {
            addCriterion("pushType not in", values, "pushType");
            return (Criteria) this;
        }

        public Criteria andPushTypeBetween(Integer value1, Integer value2) {
            addCriterion("pushType between", value1, value2, "pushType");
            return (Criteria) this;
        }

        public Criteria andPushTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("pushType not between", value1, value2, "pushType");
            return (Criteria) this;
        }

        public Criteria andAliasIsNull() {
            addCriterion("alias is null");
            return (Criteria) this;
        }

        public Criteria andAliasIsNotNull() {
            addCriterion("alias is not null");
            return (Criteria) this;
        }

        public Criteria andAliasEqualTo(String value) {
            addCriterion("alias =", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasNotEqualTo(String value) {
            addCriterion("alias <>", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasGreaterThan(String value) {
            addCriterion("alias >", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasGreaterThanOrEqualTo(String value) {
            addCriterion("alias >=", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasLessThan(String value) {
            addCriterion("alias <", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasLessThanOrEqualTo(String value) {
            addCriterion("alias <=", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasLike(String value) {
            addCriterion("alias like", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasNotLike(String value) {
            addCriterion("alias not like", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasIn(List<String> values) {
            addCriterion("alias in", values, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasNotIn(List<String> values) {
            addCriterion("alias not in", values, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasBetween(String value1, String value2) {
            addCriterion("alias between", value1, value2, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasNotBetween(String value1, String value2) {
            addCriterion("alias not between", value1, value2, "alias");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}