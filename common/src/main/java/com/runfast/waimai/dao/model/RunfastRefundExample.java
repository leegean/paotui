package com.runfast.waimai.dao.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RunfastRefundExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public RunfastRefundExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andCreate_timeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreate_timeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreate_timeEqualTo(Date value) {
            addCriterion("create_time =", value, "create_time");
            return (Criteria) this;
        }

        public Criteria andCreate_timeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "create_time");
            return (Criteria) this;
        }

        public Criteria andCreate_timeGreaterThan(Date value) {
            addCriterion("create_time >", value, "create_time");
            return (Criteria) this;
        }

        public Criteria andCreate_timeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "create_time");
            return (Criteria) this;
        }

        public Criteria andCreate_timeLessThan(Date value) {
            addCriterion("create_time <", value, "create_time");
            return (Criteria) this;
        }

        public Criteria andCreate_timeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "create_time");
            return (Criteria) this;
        }

        public Criteria andCreate_timeIn(List<Date> values) {
            addCriterion("create_time in", values, "create_time");
            return (Criteria) this;
        }

        public Criteria andCreate_timeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "create_time");
            return (Criteria) this;
        }

        public Criteria andCreate_timeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "create_time");
            return (Criteria) this;
        }

        public Criteria andCreate_timeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "create_time");
            return (Criteria) this;
        }

        public Criteria andUpdate_timeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdate_timeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdate_timeEqualTo(Date value) {
            addCriterion("update_time =", value, "update_time");
            return (Criteria) this;
        }

        public Criteria andUpdate_timeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "update_time");
            return (Criteria) this;
        }

        public Criteria andUpdate_timeGreaterThan(Date value) {
            addCriterion("update_time >", value, "update_time");
            return (Criteria) this;
        }

        public Criteria andUpdate_timeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "update_time");
            return (Criteria) this;
        }

        public Criteria andUpdate_timeLessThan(Date value) {
            addCriterion("update_time <", value, "update_time");
            return (Criteria) this;
        }

        public Criteria andUpdate_timeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "update_time");
            return (Criteria) this;
        }

        public Criteria andUpdate_timeIn(List<Date> values) {
            addCriterion("update_time in", values, "update_time");
            return (Criteria) this;
        }

        public Criteria andUpdate_timeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "update_time");
            return (Criteria) this;
        }

        public Criteria andUpdate_timeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "update_time");
            return (Criteria) this;
        }

        public Criteria andUpdate_timeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "update_time");
            return (Criteria) this;
        }

        public Criteria andAmountIsNull() {
            addCriterion("amount is null");
            return (Criteria) this;
        }

        public Criteria andAmountIsNotNull() {
            addCriterion("amount is not null");
            return (Criteria) this;
        }

        public Criteria andAmountEqualTo(Integer value) {
            addCriterion("amount =", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountNotEqualTo(Integer value) {
            addCriterion("amount <>", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountGreaterThan(Integer value) {
            addCriterion("amount >", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountGreaterThanOrEqualTo(Integer value) {
            addCriterion("amount >=", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountLessThan(Integer value) {
            addCriterion("amount <", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountLessThanOrEqualTo(Integer value) {
            addCriterion("amount <=", value, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountIn(List<Integer> values) {
            addCriterion("amount in", values, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountNotIn(List<Integer> values) {
            addCriterion("amount not in", values, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountBetween(Integer value1, Integer value2) {
            addCriterion("amount between", value1, value2, "amount");
            return (Criteria) this;
        }

        public Criteria andAmountNotBetween(Integer value1, Integer value2) {
            addCriterion("amount not between", value1, value2, "amount");
            return (Criteria) this;
        }

        public Criteria andRefund_noIsNull() {
            addCriterion("refund_no is null");
            return (Criteria) this;
        }

        public Criteria andRefund_noIsNotNull() {
            addCriterion("refund_no is not null");
            return (Criteria) this;
        }

        public Criteria andRefund_noEqualTo(String value) {
            addCriterion("refund_no =", value, "refund_no");
            return (Criteria) this;
        }

        public Criteria andRefund_noNotEqualTo(String value) {
            addCriterion("refund_no <>", value, "refund_no");
            return (Criteria) this;
        }

        public Criteria andRefund_noGreaterThan(String value) {
            addCriterion("refund_no >", value, "refund_no");
            return (Criteria) this;
        }

        public Criteria andRefund_noGreaterThanOrEqualTo(String value) {
            addCriterion("refund_no >=", value, "refund_no");
            return (Criteria) this;
        }

        public Criteria andRefund_noLessThan(String value) {
            addCriterion("refund_no <", value, "refund_no");
            return (Criteria) this;
        }

        public Criteria andRefund_noLessThanOrEqualTo(String value) {
            addCriterion("refund_no <=", value, "refund_no");
            return (Criteria) this;
        }

        public Criteria andRefund_noLike(String value) {
            addCriterion("refund_no like", value, "refund_no");
            return (Criteria) this;
        }

        public Criteria andRefund_noNotLike(String value) {
            addCriterion("refund_no not like", value, "refund_no");
            return (Criteria) this;
        }

        public Criteria andRefund_noIn(List<String> values) {
            addCriterion("refund_no in", values, "refund_no");
            return (Criteria) this;
        }

        public Criteria andRefund_noNotIn(List<String> values) {
            addCriterion("refund_no not in", values, "refund_no");
            return (Criteria) this;
        }

        public Criteria andRefund_noBetween(String value1, String value2) {
            addCriterion("refund_no between", value1, value2, "refund_no");
            return (Criteria) this;
        }

        public Criteria andRefund_noNotBetween(String value1, String value2) {
            addCriterion("refund_no not between", value1, value2, "refund_no");
            return (Criteria) this;
        }

        public Criteria andDescriptionIsNull() {
            addCriterion("description is null");
            return (Criteria) this;
        }

        public Criteria andDescriptionIsNotNull() {
            addCriterion("description is not null");
            return (Criteria) this;
        }

        public Criteria andDescriptionEqualTo(String value) {
            addCriterion("description =", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotEqualTo(String value) {
            addCriterion("description <>", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionGreaterThan(String value) {
            addCriterion("description >", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionGreaterThanOrEqualTo(String value) {
            addCriterion("description >=", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLessThan(String value) {
            addCriterion("description <", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLessThanOrEqualTo(String value) {
            addCriterion("description <=", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLike(String value) {
            addCriterion("description like", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotLike(String value) {
            addCriterion("description not like", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionIn(List<String> values) {
            addCriterion("description in", values, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotIn(List<String> values) {
            addCriterion("description not in", values, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionBetween(String value1, String value2) {
            addCriterion("description between", value1, value2, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotBetween(String value1, String value2) {
            addCriterion("description not between", value1, value2, "description");
            return (Criteria) this;
        }

        public Criteria andFailure_codeIsNull() {
            addCriterion("failure_code is null");
            return (Criteria) this;
        }

        public Criteria andFailure_codeIsNotNull() {
            addCriterion("failure_code is not null");
            return (Criteria) this;
        }

        public Criteria andFailure_codeEqualTo(String value) {
            addCriterion("failure_code =", value, "failure_code");
            return (Criteria) this;
        }

        public Criteria andFailure_codeNotEqualTo(String value) {
            addCriterion("failure_code <>", value, "failure_code");
            return (Criteria) this;
        }

        public Criteria andFailure_codeGreaterThan(String value) {
            addCriterion("failure_code >", value, "failure_code");
            return (Criteria) this;
        }

        public Criteria andFailure_codeGreaterThanOrEqualTo(String value) {
            addCriterion("failure_code >=", value, "failure_code");
            return (Criteria) this;
        }

        public Criteria andFailure_codeLessThan(String value) {
            addCriterion("failure_code <", value, "failure_code");
            return (Criteria) this;
        }

        public Criteria andFailure_codeLessThanOrEqualTo(String value) {
            addCriterion("failure_code <=", value, "failure_code");
            return (Criteria) this;
        }

        public Criteria andFailure_codeLike(String value) {
            addCriterion("failure_code like", value, "failure_code");
            return (Criteria) this;
        }

        public Criteria andFailure_codeNotLike(String value) {
            addCriterion("failure_code not like", value, "failure_code");
            return (Criteria) this;
        }

        public Criteria andFailure_codeIn(List<String> values) {
            addCriterion("failure_code in", values, "failure_code");
            return (Criteria) this;
        }

        public Criteria andFailure_codeNotIn(List<String> values) {
            addCriterion("failure_code not in", values, "failure_code");
            return (Criteria) this;
        }

        public Criteria andFailure_codeBetween(String value1, String value2) {
            addCriterion("failure_code between", value1, value2, "failure_code");
            return (Criteria) this;
        }

        public Criteria andFailure_codeNotBetween(String value1, String value2) {
            addCriterion("failure_code not between", value1, value2, "failure_code");
            return (Criteria) this;
        }

        public Criteria andFailure_msgIsNull() {
            addCriterion("failure_msg is null");
            return (Criteria) this;
        }

        public Criteria andFailure_msgIsNotNull() {
            addCriterion("failure_msg is not null");
            return (Criteria) this;
        }

        public Criteria andFailure_msgEqualTo(String value) {
            addCriterion("failure_msg =", value, "failure_msg");
            return (Criteria) this;
        }

        public Criteria andFailure_msgNotEqualTo(String value) {
            addCriterion("failure_msg <>", value, "failure_msg");
            return (Criteria) this;
        }

        public Criteria andFailure_msgGreaterThan(String value) {
            addCriterion("failure_msg >", value, "failure_msg");
            return (Criteria) this;
        }

        public Criteria andFailure_msgGreaterThanOrEqualTo(String value) {
            addCriterion("failure_msg >=", value, "failure_msg");
            return (Criteria) this;
        }

        public Criteria andFailure_msgLessThan(String value) {
            addCriterion("failure_msg <", value, "failure_msg");
            return (Criteria) this;
        }

        public Criteria andFailure_msgLessThanOrEqualTo(String value) {
            addCriterion("failure_msg <=", value, "failure_msg");
            return (Criteria) this;
        }

        public Criteria andFailure_msgLike(String value) {
            addCriterion("failure_msg like", value, "failure_msg");
            return (Criteria) this;
        }

        public Criteria andFailure_msgNotLike(String value) {
            addCriterion("failure_msg not like", value, "failure_msg");
            return (Criteria) this;
        }

        public Criteria andFailure_msgIn(List<String> values) {
            addCriterion("failure_msg in", values, "failure_msg");
            return (Criteria) this;
        }

        public Criteria andFailure_msgNotIn(List<String> values) {
            addCriterion("failure_msg not in", values, "failure_msg");
            return (Criteria) this;
        }

        public Criteria andFailure_msgBetween(String value1, String value2) {
            addCriterion("failure_msg between", value1, value2, "failure_msg");
            return (Criteria) this;
        }

        public Criteria andFailure_msgNotBetween(String value1, String value2) {
            addCriterion("failure_msg not between", value1, value2, "failure_msg");
            return (Criteria) this;
        }

        public Criteria andOrder_noIsNull() {
            addCriterion("order_no is null");
            return (Criteria) this;
        }

        public Criteria andOrder_noIsNotNull() {
            addCriterion("order_no is not null");
            return (Criteria) this;
        }

        public Criteria andOrder_noEqualTo(String value) {
            addCriterion("order_no =", value, "order_no");
            return (Criteria) this;
        }

        public Criteria andOrder_noNotEqualTo(String value) {
            addCriterion("order_no <>", value, "order_no");
            return (Criteria) this;
        }

        public Criteria andOrder_noGreaterThan(String value) {
            addCriterion("order_no >", value, "order_no");
            return (Criteria) this;
        }

        public Criteria andOrder_noGreaterThanOrEqualTo(String value) {
            addCriterion("order_no >=", value, "order_no");
            return (Criteria) this;
        }

        public Criteria andOrder_noLessThan(String value) {
            addCriterion("order_no <", value, "order_no");
            return (Criteria) this;
        }

        public Criteria andOrder_noLessThanOrEqualTo(String value) {
            addCriterion("order_no <=", value, "order_no");
            return (Criteria) this;
        }

        public Criteria andOrder_noLike(String value) {
            addCriterion("order_no like", value, "order_no");
            return (Criteria) this;
        }

        public Criteria andOrder_noNotLike(String value) {
            addCriterion("order_no not like", value, "order_no");
            return (Criteria) this;
        }

        public Criteria andOrder_noIn(List<String> values) {
            addCriterion("order_no in", values, "order_no");
            return (Criteria) this;
        }

        public Criteria andOrder_noNotIn(List<String> values) {
            addCriterion("order_no not in", values, "order_no");
            return (Criteria) this;
        }

        public Criteria andOrder_noBetween(String value1, String value2) {
            addCriterion("order_no between", value1, value2, "order_no");
            return (Criteria) this;
        }

        public Criteria andOrder_noNotBetween(String value1, String value2) {
            addCriterion("order_no not between", value1, value2, "order_no");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andSucceedIsNull() {
            addCriterion("succeed is null");
            return (Criteria) this;
        }

        public Criteria andSucceedIsNotNull() {
            addCriterion("succeed is not null");
            return (Criteria) this;
        }

        public Criteria andSucceedEqualTo(Boolean value) {
            addCriterion("succeed =", value, "succeed");
            return (Criteria) this;
        }

        public Criteria andSucceedNotEqualTo(Boolean value) {
            addCriterion("succeed <>", value, "succeed");
            return (Criteria) this;
        }

        public Criteria andSucceedGreaterThan(Boolean value) {
            addCriterion("succeed >", value, "succeed");
            return (Criteria) this;
        }

        public Criteria andSucceedGreaterThanOrEqualTo(Boolean value) {
            addCriterion("succeed >=", value, "succeed");
            return (Criteria) this;
        }

        public Criteria andSucceedLessThan(Boolean value) {
            addCriterion("succeed <", value, "succeed");
            return (Criteria) this;
        }

        public Criteria andSucceedLessThanOrEqualTo(Boolean value) {
            addCriterion("succeed <=", value, "succeed");
            return (Criteria) this;
        }

        public Criteria andSucceedIn(List<Boolean> values) {
            addCriterion("succeed in", values, "succeed");
            return (Criteria) this;
        }

        public Criteria andSucceedNotIn(List<Boolean> values) {
            addCriterion("succeed not in", values, "succeed");
            return (Criteria) this;
        }

        public Criteria andSucceedBetween(Boolean value1, Boolean value2) {
            addCriterion("succeed between", value1, value2, "succeed");
            return (Criteria) this;
        }

        public Criteria andSucceedNotBetween(Boolean value1, Boolean value2) {
            addCriterion("succeed not between", value1, value2, "succeed");
            return (Criteria) this;
        }

        public Criteria andTime_succeedIsNull() {
            addCriterion("time_succeed is null");
            return (Criteria) this;
        }

        public Criteria andTime_succeedIsNotNull() {
            addCriterion("time_succeed is not null");
            return (Criteria) this;
        }

        public Criteria andTime_succeedEqualTo(Date value) {
            addCriterion("time_succeed =", value, "time_succeed");
            return (Criteria) this;
        }

        public Criteria andTime_succeedNotEqualTo(Date value) {
            addCriterion("time_succeed <>", value, "time_succeed");
            return (Criteria) this;
        }

        public Criteria andTime_succeedGreaterThan(Date value) {
            addCriterion("time_succeed >", value, "time_succeed");
            return (Criteria) this;
        }

        public Criteria andTime_succeedGreaterThanOrEqualTo(Date value) {
            addCriterion("time_succeed >=", value, "time_succeed");
            return (Criteria) this;
        }

        public Criteria andTime_succeedLessThan(Date value) {
            addCriterion("time_succeed <", value, "time_succeed");
            return (Criteria) this;
        }

        public Criteria andTime_succeedLessThanOrEqualTo(Date value) {
            addCriterion("time_succeed <=", value, "time_succeed");
            return (Criteria) this;
        }

        public Criteria andTime_succeedIn(List<Date> values) {
            addCriterion("time_succeed in", values, "time_succeed");
            return (Criteria) this;
        }

        public Criteria andTime_succeedNotIn(List<Date> values) {
            addCriterion("time_succeed not in", values, "time_succeed");
            return (Criteria) this;
        }

        public Criteria andTime_succeedBetween(Date value1, Date value2) {
            addCriterion("time_succeed between", value1, value2, "time_succeed");
            return (Criteria) this;
        }

        public Criteria andTime_succeedNotBetween(Date value1, Date value2) {
            addCriterion("time_succeed not between", value1, value2, "time_succeed");
            return (Criteria) this;
        }

        public Criteria andTransaction_noIsNull() {
            addCriterion("transaction_no is null");
            return (Criteria) this;
        }

        public Criteria andTransaction_noIsNotNull() {
            addCriterion("transaction_no is not null");
            return (Criteria) this;
        }

        public Criteria andTransaction_noEqualTo(String value) {
            addCriterion("transaction_no =", value, "transaction_no");
            return (Criteria) this;
        }

        public Criteria andTransaction_noNotEqualTo(String value) {
            addCriterion("transaction_no <>", value, "transaction_no");
            return (Criteria) this;
        }

        public Criteria andTransaction_noGreaterThan(String value) {
            addCriterion("transaction_no >", value, "transaction_no");
            return (Criteria) this;
        }

        public Criteria andTransaction_noGreaterThanOrEqualTo(String value) {
            addCriterion("transaction_no >=", value, "transaction_no");
            return (Criteria) this;
        }

        public Criteria andTransaction_noLessThan(String value) {
            addCriterion("transaction_no <", value, "transaction_no");
            return (Criteria) this;
        }

        public Criteria andTransaction_noLessThanOrEqualTo(String value) {
            addCriterion("transaction_no <=", value, "transaction_no");
            return (Criteria) this;
        }

        public Criteria andTransaction_noLike(String value) {
            addCriterion("transaction_no like", value, "transaction_no");
            return (Criteria) this;
        }

        public Criteria andTransaction_noNotLike(String value) {
            addCriterion("transaction_no not like", value, "transaction_no");
            return (Criteria) this;
        }

        public Criteria andTransaction_noIn(List<String> values) {
            addCriterion("transaction_no in", values, "transaction_no");
            return (Criteria) this;
        }

        public Criteria andTransaction_noNotIn(List<String> values) {
            addCriterion("transaction_no not in", values, "transaction_no");
            return (Criteria) this;
        }

        public Criteria andTransaction_noBetween(String value1, String value2) {
            addCriterion("transaction_no between", value1, value2, "transaction_no");
            return (Criteria) this;
        }

        public Criteria andTransaction_noNotBetween(String value1, String value2) {
            addCriterion("transaction_no not between", value1, value2, "transaction_no");
            return (Criteria) this;
        }

        public Criteria andOrder_idIsNull() {
            addCriterion("order_id is null");
            return (Criteria) this;
        }

        public Criteria andOrder_idIsNotNull() {
            addCriterion("order_id is not null");
            return (Criteria) this;
        }

        public Criteria andOrder_idEqualTo(Integer value) {
            addCriterion("order_id =", value, "order_id");
            return (Criteria) this;
        }

        public Criteria andOrder_idNotEqualTo(Integer value) {
            addCriterion("order_id <>", value, "order_id");
            return (Criteria) this;
        }

        public Criteria andOrder_idGreaterThan(Integer value) {
            addCriterion("order_id >", value, "order_id");
            return (Criteria) this;
        }

        public Criteria andOrder_idGreaterThanOrEqualTo(Integer value) {
            addCriterion("order_id >=", value, "order_id");
            return (Criteria) this;
        }

        public Criteria andOrder_idLessThan(Integer value) {
            addCriterion("order_id <", value, "order_id");
            return (Criteria) this;
        }

        public Criteria andOrder_idLessThanOrEqualTo(Integer value) {
            addCriterion("order_id <=", value, "order_id");
            return (Criteria) this;
        }

        public Criteria andOrder_idIn(List<Integer> values) {
            addCriterion("order_id in", values, "order_id");
            return (Criteria) this;
        }

        public Criteria andOrder_idNotIn(List<Integer> values) {
            addCriterion("order_id not in", values, "order_id");
            return (Criteria) this;
        }

        public Criteria andOrder_idBetween(Integer value1, Integer value2) {
            addCriterion("order_id between", value1, value2, "order_id");
            return (Criteria) this;
        }

        public Criteria andOrder_idNotBetween(Integer value1, Integer value2) {
            addCriterion("order_id not between", value1, value2, "order_id");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}