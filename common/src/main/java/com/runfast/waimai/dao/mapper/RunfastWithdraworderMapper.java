package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastWithdraworder;
import com.runfast.waimai.dao.model.RunfastWithdraworderExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastWithdraworderMapper extends IMapper<RunfastWithdraworder, Integer, RunfastWithdraworderExample> {
}