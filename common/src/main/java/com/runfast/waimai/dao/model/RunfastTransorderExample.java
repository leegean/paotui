package com.runfast.waimai.dao.model;

import java.util.ArrayList;
import java.util.List;

public class RunfastTransorderExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public RunfastTransorderExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andAskShopperIdIsNull() {
            addCriterion("askShopperId is null");
            return (Criteria) this;
        }

        public Criteria andAskShopperIdIsNotNull() {
            addCriterion("askShopperId is not null");
            return (Criteria) this;
        }

        public Criteria andAskShopperIdEqualTo(Integer value) {
            addCriterion("askShopperId =", value, "askShopperId");
            return (Criteria) this;
        }

        public Criteria andAskShopperIdNotEqualTo(Integer value) {
            addCriterion("askShopperId <>", value, "askShopperId");
            return (Criteria) this;
        }

        public Criteria andAskShopperIdGreaterThan(Integer value) {
            addCriterion("askShopperId >", value, "askShopperId");
            return (Criteria) this;
        }

        public Criteria andAskShopperIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("askShopperId >=", value, "askShopperId");
            return (Criteria) this;
        }

        public Criteria andAskShopperIdLessThan(Integer value) {
            addCriterion("askShopperId <", value, "askShopperId");
            return (Criteria) this;
        }

        public Criteria andAskShopperIdLessThanOrEqualTo(Integer value) {
            addCriterion("askShopperId <=", value, "askShopperId");
            return (Criteria) this;
        }

        public Criteria andAskShopperIdIn(List<Integer> values) {
            addCriterion("askShopperId in", values, "askShopperId");
            return (Criteria) this;
        }

        public Criteria andAskShopperIdNotIn(List<Integer> values) {
            addCriterion("askShopperId not in", values, "askShopperId");
            return (Criteria) this;
        }

        public Criteria andAskShopperIdBetween(Integer value1, Integer value2) {
            addCriterion("askShopperId between", value1, value2, "askShopperId");
            return (Criteria) this;
        }

        public Criteria andAskShopperIdNotBetween(Integer value1, Integer value2) {
            addCriterion("askShopperId not between", value1, value2, "askShopperId");
            return (Criteria) this;
        }

        public Criteria andCommentIsNull() {
            addCriterion("comment is null");
            return (Criteria) this;
        }

        public Criteria andCommentIsNotNull() {
            addCriterion("comment is not null");
            return (Criteria) this;
        }

        public Criteria andCommentEqualTo(String value) {
            addCriterion("comment =", value, "comment");
            return (Criteria) this;
        }

        public Criteria andCommentNotEqualTo(String value) {
            addCriterion("comment <>", value, "comment");
            return (Criteria) this;
        }

        public Criteria andCommentGreaterThan(String value) {
            addCriterion("comment >", value, "comment");
            return (Criteria) this;
        }

        public Criteria andCommentGreaterThanOrEqualTo(String value) {
            addCriterion("comment >=", value, "comment");
            return (Criteria) this;
        }

        public Criteria andCommentLessThan(String value) {
            addCriterion("comment <", value, "comment");
            return (Criteria) this;
        }

        public Criteria andCommentLessThanOrEqualTo(String value) {
            addCriterion("comment <=", value, "comment");
            return (Criteria) this;
        }

        public Criteria andCommentLike(String value) {
            addCriterion("comment like", value, "comment");
            return (Criteria) this;
        }

        public Criteria andCommentNotLike(String value) {
            addCriterion("comment not like", value, "comment");
            return (Criteria) this;
        }

        public Criteria andCommentIn(List<String> values) {
            addCriterion("comment in", values, "comment");
            return (Criteria) this;
        }

        public Criteria andCommentNotIn(List<String> values) {
            addCriterion("comment not in", values, "comment");
            return (Criteria) this;
        }

        public Criteria andCommentBetween(String value1, String value2) {
            addCriterion("comment between", value1, value2, "comment");
            return (Criteria) this;
        }

        public Criteria andCommentNotBetween(String value1, String value2) {
            addCriterion("comment not between", value1, value2, "comment");
            return (Criteria) this;
        }

        public Criteria andOrderCodeIsNull() {
            addCriterion("orderCode is null");
            return (Criteria) this;
        }

        public Criteria andOrderCodeIsNotNull() {
            addCriterion("orderCode is not null");
            return (Criteria) this;
        }

        public Criteria andOrderCodeEqualTo(String value) {
            addCriterion("orderCode =", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeNotEqualTo(String value) {
            addCriterion("orderCode <>", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeGreaterThan(String value) {
            addCriterion("orderCode >", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeGreaterThanOrEqualTo(String value) {
            addCriterion("orderCode >=", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeLessThan(String value) {
            addCriterion("orderCode <", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeLessThanOrEqualTo(String value) {
            addCriterion("orderCode <=", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeLike(String value) {
            addCriterion("orderCode like", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeNotLike(String value) {
            addCriterion("orderCode not like", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeIn(List<String> values) {
            addCriterion("orderCode in", values, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeNotIn(List<String> values) {
            addCriterion("orderCode not in", values, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeBetween(String value1, String value2) {
            addCriterion("orderCode between", value1, value2, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeNotBetween(String value1, String value2) {
            addCriterion("orderCode not between", value1, value2, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderIdIsNull() {
            addCriterion("orderId is null");
            return (Criteria) this;
        }

        public Criteria andOrderIdIsNotNull() {
            addCriterion("orderId is not null");
            return (Criteria) this;
        }

        public Criteria andOrderIdEqualTo(Integer value) {
            addCriterion("orderId =", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotEqualTo(Integer value) {
            addCriterion("orderId <>", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdGreaterThan(Integer value) {
            addCriterion("orderId >", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("orderId >=", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdLessThan(Integer value) {
            addCriterion("orderId <", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdLessThanOrEqualTo(Integer value) {
            addCriterion("orderId <=", value, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdIn(List<Integer> values) {
            addCriterion("orderId in", values, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotIn(List<Integer> values) {
            addCriterion("orderId not in", values, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdBetween(Integer value1, Integer value2) {
            addCriterion("orderId between", value1, value2, "orderId");
            return (Criteria) this;
        }

        public Criteria andOrderIdNotBetween(Integer value1, Integer value2) {
            addCriterion("orderId not between", value1, value2, "orderId");
            return (Criteria) this;
        }

        public Criteria andResShopperIdIsNull() {
            addCriterion("resShopperId is null");
            return (Criteria) this;
        }

        public Criteria andResShopperIdIsNotNull() {
            addCriterion("resShopperId is not null");
            return (Criteria) this;
        }

        public Criteria andResShopperIdEqualTo(Integer value) {
            addCriterion("resShopperId =", value, "resShopperId");
            return (Criteria) this;
        }

        public Criteria andResShopperIdNotEqualTo(Integer value) {
            addCriterion("resShopperId <>", value, "resShopperId");
            return (Criteria) this;
        }

        public Criteria andResShopperIdGreaterThan(Integer value) {
            addCriterion("resShopperId >", value, "resShopperId");
            return (Criteria) this;
        }

        public Criteria andResShopperIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("resShopperId >=", value, "resShopperId");
            return (Criteria) this;
        }

        public Criteria andResShopperIdLessThan(Integer value) {
            addCriterion("resShopperId <", value, "resShopperId");
            return (Criteria) this;
        }

        public Criteria andResShopperIdLessThanOrEqualTo(Integer value) {
            addCriterion("resShopperId <=", value, "resShopperId");
            return (Criteria) this;
        }

        public Criteria andResShopperIdIn(List<Integer> values) {
            addCriterion("resShopperId in", values, "resShopperId");
            return (Criteria) this;
        }

        public Criteria andResShopperIdNotIn(List<Integer> values) {
            addCriterion("resShopperId not in", values, "resShopperId");
            return (Criteria) this;
        }

        public Criteria andResShopperIdBetween(Integer value1, Integer value2) {
            addCriterion("resShopperId between", value1, value2, "resShopperId");
            return (Criteria) this;
        }

        public Criteria andResShopperIdNotBetween(Integer value1, Integer value2) {
            addCriterion("resShopperId not between", value1, value2, "resShopperId");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andTakeTypeIsNull() {
            addCriterion("takeType is null");
            return (Criteria) this;
        }

        public Criteria andTakeTypeIsNotNull() {
            addCriterion("takeType is not null");
            return (Criteria) this;
        }

        public Criteria andTakeTypeEqualTo(Integer value) {
            addCriterion("takeType =", value, "takeType");
            return (Criteria) this;
        }

        public Criteria andTakeTypeNotEqualTo(Integer value) {
            addCriterion("takeType <>", value, "takeType");
            return (Criteria) this;
        }

        public Criteria andTakeTypeGreaterThan(Integer value) {
            addCriterion("takeType >", value, "takeType");
            return (Criteria) this;
        }

        public Criteria andTakeTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("takeType >=", value, "takeType");
            return (Criteria) this;
        }

        public Criteria andTakeTypeLessThan(Integer value) {
            addCriterion("takeType <", value, "takeType");
            return (Criteria) this;
        }

        public Criteria andTakeTypeLessThanOrEqualTo(Integer value) {
            addCriterion("takeType <=", value, "takeType");
            return (Criteria) this;
        }

        public Criteria andTakeTypeIn(List<Integer> values) {
            addCriterion("takeType in", values, "takeType");
            return (Criteria) this;
        }

        public Criteria andTakeTypeNotIn(List<Integer> values) {
            addCriterion("takeType not in", values, "takeType");
            return (Criteria) this;
        }

        public Criteria andTakeTypeBetween(Integer value1, Integer value2) {
            addCriterion("takeType between", value1, value2, "takeType");
            return (Criteria) this;
        }

        public Criteria andTakeTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("takeType not between", value1, value2, "takeType");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}