package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastEnshrine;
import com.runfast.waimai.dao.model.RunfastEnshrineExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastEnshrineMapper extends IMapper<RunfastEnshrine, Integer, RunfastEnshrineExample> {
    Integer deleteByPrimaryKey(Integer id);
}