package com.runfast.waimai.dao.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class RunfastRefund implements Serializable {
    public enum Status{
        create(0),pending(1),success(2),fail(5);
        private int code;

        Status(int code) {
            this.code = code;
        }

        public int getCode() {
            return code;
        }
    }
    private Integer id;

    private Date create_time;

    private Date update_time;

    /**
     * 退款金额
     */
    private Integer amount;

    /**
     * 退款单号
     */
    private String refund_no;

    private String description;

    private String failure_code;

    private String failure_msg;

    /**
     * 订单号
     */
    private String order_no;

    /**
     * 退款状态
     */
    private Integer status;

    /**
     * 是否退款成功
     */
    private Boolean succeed;

    private Date time_succeed;

    private String transaction_no;

    private Integer order_id;

    private static final long serialVersionUID = 1L;

}