package com.runfast.waimai.dao.model;

import java.io.Serializable;

public class RunfastAgentMgrHasRunfastAgentbusiness implements Serializable {
    private Integer runfast_agent_mgr_id;

    private Integer runfast_agentbusiness_id;

    private static final long serialVersionUID = 1L;

    public Integer getRunfast_agent_mgr_id() {
        return runfast_agent_mgr_id;
    }

    public RunfastAgentMgrHasRunfastAgentbusiness withRunfast_agent_mgr_id(Integer runfast_agent_mgr_id) {
        this.setRunfast_agent_mgr_id(runfast_agent_mgr_id);
        return this;
    }

    public void setRunfast_agent_mgr_id(Integer runfast_agent_mgr_id) {
        this.runfast_agent_mgr_id = runfast_agent_mgr_id;
    }

    public Integer getRunfast_agentbusiness_id() {
        return runfast_agentbusiness_id;
    }

    public RunfastAgentMgrHasRunfastAgentbusiness withRunfast_agentbusiness_id(Integer runfast_agentbusiness_id) {
        this.setRunfast_agentbusiness_id(runfast_agentbusiness_id);
        return this;
    }

    public void setRunfast_agentbusiness_id(Integer runfast_agentbusiness_id) {
        this.runfast_agentbusiness_id = runfast_agentbusiness_id;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", runfast_agent_mgr_id=").append(runfast_agent_mgr_id);
        sb.append(", runfast_agentbusiness_id=").append(runfast_agentbusiness_id);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}