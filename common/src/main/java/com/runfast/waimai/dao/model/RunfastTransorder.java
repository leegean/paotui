package com.runfast.waimai.dao.model;

import java.io.Serializable;

public class RunfastTransorder implements Serializable {
    private Integer id;

    private Integer askShopperId;

    private String comment;

    private String orderCode;

    private Integer orderId;

    private Integer resShopperId;

    private Integer status;

    private Integer takeType;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public RunfastTransorder withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAskShopperId() {
        return askShopperId;
    }

    public RunfastTransorder withAskShopperId(Integer askShopperId) {
        this.setAskShopperId(askShopperId);
        return this;
    }

    public void setAskShopperId(Integer askShopperId) {
        this.askShopperId = askShopperId;
    }

    public String getComment() {
        return comment;
    }

    public RunfastTransorder withComment(String comment) {
        this.setComment(comment);
        return this;
    }

    public void setComment(String comment) {
        this.comment = comment == null ? null : comment.trim();
    }

    public String getOrderCode() {
        return orderCode;
    }

    public RunfastTransorder withOrderCode(String orderCode) {
        this.setOrderCode(orderCode);
        return this;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode == null ? null : orderCode.trim();
    }

    public Integer getOrderId() {
        return orderId;
    }

    public RunfastTransorder withOrderId(Integer orderId) {
        this.setOrderId(orderId);
        return this;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getResShopperId() {
        return resShopperId;
    }

    public RunfastTransorder withResShopperId(Integer resShopperId) {
        this.setResShopperId(resShopperId);
        return this;
    }

    public void setResShopperId(Integer resShopperId) {
        this.resShopperId = resShopperId;
    }

    public Integer getStatus() {
        return status;
    }

    public RunfastTransorder withStatus(Integer status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getTakeType() {
        return takeType;
    }

    public RunfastTransorder withTakeType(Integer takeType) {
        this.setTakeType(takeType);
        return this;
    }

    public void setTakeType(Integer takeType) {
        this.takeType = takeType;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", askShopperId=").append(askShopperId);
        sb.append(", comment=").append(comment);
        sb.append(", orderCode=").append(orderCode);
        sb.append(", orderId=").append(orderId);
        sb.append(", resShopperId=").append(resShopperId);
        sb.append(", status=").append(status);
        sb.append(", takeType=").append(takeType);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}