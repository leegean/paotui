package com.runfast.waimai.dao.model;

import com.runfast.common.entity.validate.group.Create;
import com.runfast.common.entity.validate.group.Update;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

public class RunfastBusinessalliance implements Serializable {
    private Integer id;
    @NotBlank(groups = {Create.class})
    private String address;

    @NotBlank(groups = {Create.class})
    private String businessName;

    @NotBlank(groups = {Create.class})
    private String contacts;

    private Date createTime;

    @NotBlank(groups = {Create.class})
    private String local;

    @NotBlank(groups = {Create.class})
    private String moblie;

    private String remark;



    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public RunfastBusinessalliance withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public RunfastBusinessalliance withAddress(String address) {
        this.setAddress(address);
        return this;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getBusinessName() {
        return businessName;
    }

    public RunfastBusinessalliance withBusinessName(String businessName) {
        this.setBusinessName(businessName);
        return this;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName == null ? null : businessName.trim();
    }

    public String getContacts() {
        return contacts;
    }

    public RunfastBusinessalliance withContacts(String contacts) {
        this.setContacts(contacts);
        return this;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts == null ? null : contacts.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public RunfastBusinessalliance withCreateTime(Date createTime) {
        this.setCreateTime(createTime);
        return this;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getLocal() {
        return local;
    }

    public RunfastBusinessalliance withLocal(String local) {
        this.setLocal(local);
        return this;
    }

    public void setLocal(String local) {
        this.local = local == null ? null : local.trim();
    }

    public String getMoblie() {
        return moblie;
    }

    public RunfastBusinessalliance withMoblie(String moblie) {
        this.setMoblie(moblie);
        return this;
    }

    public void setMoblie(String moblie) {
        this.moblie = moblie == null ? null : moblie.trim();
    }

    public String getRemark() {
        return remark;
    }

    public RunfastBusinessalliance withRemark(String remark) {
        this.setRemark(remark);
        return this;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", address=").append(address);
        sb.append(", businessName=").append(businessName);
        sb.append(", contacts=").append(contacts);
        sb.append(", createTime=").append(createTime);
        sb.append(", local=").append(local);
        sb.append(", moblie=").append(moblie);
        sb.append(", remark=").append(remark);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}