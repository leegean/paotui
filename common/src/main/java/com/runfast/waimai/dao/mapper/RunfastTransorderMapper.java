package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastTransorder;
import com.runfast.waimai.dao.model.RunfastTransorderExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastTransorderMapper extends IMapper<RunfastTransorder, Integer, RunfastTransorderExample> {
}