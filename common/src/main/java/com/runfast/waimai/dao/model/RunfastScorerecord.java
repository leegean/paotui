package com.runfast.waimai.dao.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class RunfastScorerecord implements Serializable {
    private Integer id;

    private String cardnumber;

    private Integer cid;

    private Date createTime;

    private String mobile;

    private String name;

    private Integer score;

    private Integer type;

    private BigDecimal monetary;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public RunfastScorerecord withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCardnumber() {
        return cardnumber;
    }

    public RunfastScorerecord withCardnumber(String cardnumber) {
        this.setCardnumber(cardnumber);
        return this;
    }

    public void setCardnumber(String cardnumber) {
        this.cardnumber = cardnumber == null ? null : cardnumber.trim();
    }

    public Integer getCid() {
        return cid;
    }

    public RunfastScorerecord withCid(Integer cid) {
        this.setCid(cid);
        return this;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public RunfastScorerecord withCreateTime(Date createTime) {
        this.setCreateTime(createTime);
        return this;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getMobile() {
        return mobile;
    }

    public RunfastScorerecord withMobile(String mobile) {
        this.setMobile(mobile);
        return this;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public String getName() {
        return name;
    }

    public RunfastScorerecord withName(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getScore() {
        return score;
    }

    public RunfastScorerecord withScore(Integer score) {
        this.setScore(score);
        return this;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getType() {
        return type;
    }

    public RunfastScorerecord withType(Integer type) {
        this.setType(type);
        return this;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public BigDecimal getMonetary() {
        return monetary;
    }

    public RunfastScorerecord withMonetary(BigDecimal monetary) {
        this.setMonetary(monetary);
        return this;
    }

    public void setMonetary(BigDecimal monetary) {
        this.monetary = monetary;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", cardnumber=").append(cardnumber);
        sb.append(", cid=").append(cid);
        sb.append(", createTime=").append(createTime);
        sb.append(", mobile=").append(mobile);
        sb.append(", name=").append(name);
        sb.append(", score=").append(score);
        sb.append(", type=").append(type);
        sb.append(", monetary=").append(monetary);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}