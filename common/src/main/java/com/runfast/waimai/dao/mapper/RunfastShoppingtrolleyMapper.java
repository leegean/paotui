package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastShoppingtrolley;
import com.runfast.waimai.dao.model.RunfastShoppingtrolleyExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastShoppingtrolleyMapper extends IMapper<RunfastShoppingtrolley, Integer, RunfastShoppingtrolleyExample> {
}