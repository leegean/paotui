package com.runfast.waimai.dao.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class RunfastWithdraworder implements Serializable {
    private Integer id;

    private Integer businessId;

    private String businessName;

    private Date createTime;

    private Integer oid;

    private String orderCode;

    private BigDecimal totalpay;

    private Integer wid;

    private BigDecimal businesspay;

    private BigDecimal businessget;

    private Integer agentId;

    private String agentName;

    private BigDecimal agentget;

    private BigDecimal showps;

    private String usermunber;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public RunfastWithdraworder withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBusinessId() {
        return businessId;
    }

    public RunfastWithdraworder withBusinessId(Integer businessId) {
        this.setBusinessId(businessId);
        return this;
    }

    public void setBusinessId(Integer businessId) {
        this.businessId = businessId;
    }

    public String getBusinessName() {
        return businessName;
    }

    public RunfastWithdraworder withBusinessName(String businessName) {
        this.setBusinessName(businessName);
        return this;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName == null ? null : businessName.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public RunfastWithdraworder withCreateTime(Date createTime) {
        this.setCreateTime(createTime);
        return this;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getOid() {
        return oid;
    }

    public RunfastWithdraworder withOid(Integer oid) {
        this.setOid(oid);
        return this;
    }

    public void setOid(Integer oid) {
        this.oid = oid;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public RunfastWithdraworder withOrderCode(String orderCode) {
        this.setOrderCode(orderCode);
        return this;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode == null ? null : orderCode.trim();
    }

    public BigDecimal getTotalpay() {
        return totalpay;
    }

    public RunfastWithdraworder withTotalpay(BigDecimal totalpay) {
        this.setTotalpay(totalpay);
        return this;
    }

    public void setTotalpay(BigDecimal totalpay) {
        this.totalpay = totalpay;
    }

    public Integer getWid() {
        return wid;
    }

    public RunfastWithdraworder withWid(Integer wid) {
        this.setWid(wid);
        return this;
    }

    public void setWid(Integer wid) {
        this.wid = wid;
    }

    public BigDecimal getBusinesspay() {
        return businesspay;
    }

    public RunfastWithdraworder withBusinesspay(BigDecimal businesspay) {
        this.setBusinesspay(businesspay);
        return this;
    }

    public void setBusinesspay(BigDecimal businesspay) {
        this.businesspay = businesspay;
    }

    public BigDecimal getBusinessget() {
        return businessget;
    }

    public RunfastWithdraworder withBusinessget(BigDecimal businessget) {
        this.setBusinessget(businessget);
        return this;
    }

    public void setBusinessget(BigDecimal businessget) {
        this.businessget = businessget;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public RunfastWithdraworder withAgentId(Integer agentId) {
        this.setAgentId(agentId);
        return this;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public String getAgentName() {
        return agentName;
    }

    public RunfastWithdraworder withAgentName(String agentName) {
        this.setAgentName(agentName);
        return this;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName == null ? null : agentName.trim();
    }

    public BigDecimal getAgentget() {
        return agentget;
    }

    public RunfastWithdraworder withAgentget(BigDecimal agentget) {
        this.setAgentget(agentget);
        return this;
    }

    public void setAgentget(BigDecimal agentget) {
        this.agentget = agentget;
    }

    public BigDecimal getShowps() {
        return showps;
    }

    public RunfastWithdraworder withShowps(BigDecimal showps) {
        this.setShowps(showps);
        return this;
    }

    public void setShowps(BigDecimal showps) {
        this.showps = showps;
    }

    public String getUsermunber() {
        return usermunber;
    }

    public RunfastWithdraworder withUsermunber(String usermunber) {
        this.setUsermunber(usermunber);
        return this;
    }

    public void setUsermunber(String usermunber) {
        this.usermunber = usermunber == null ? null : usermunber.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", businessId=").append(businessId);
        sb.append(", businessName=").append(businessName);
        sb.append(", createTime=").append(createTime);
        sb.append(", oid=").append(oid);
        sb.append(", orderCode=").append(orderCode);
        sb.append(", totalpay=").append(totalpay);
        sb.append(", wid=").append(wid);
        sb.append(", businesspay=").append(businesspay);
        sb.append(", businessget=").append(businessget);
        sb.append(", agentId=").append(agentId);
        sb.append(", agentName=").append(agentName);
        sb.append(", agentget=").append(agentget);
        sb.append(", showps=").append(showps);
        sb.append(", usermunber=").append(usermunber);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}