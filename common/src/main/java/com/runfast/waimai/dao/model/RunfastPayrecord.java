package com.runfast.waimai.dao.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class RunfastPayrecord implements Serializable {
    private Integer id;

    private Date createTime;

    private String fromUser;

    private Date modifTime;

    private String openid;

    private Integer orderId;

    private String orderNo;

    private Integer payType;

    private String toUser;

    private BigDecimal totalPrice;

    private String trade_no;

    private Integer type;

    private Integer userId;

    private String userName;

    private Integer appOrwx;

    private String info;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public RunfastPayrecord withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public RunfastPayrecord withCreateTime(Date createTime) {
        this.setCreateTime(createTime);
        return this;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getFromUser() {
        return fromUser;
    }

    public RunfastPayrecord withFromUser(String fromUser) {
        this.setFromUser(fromUser);
        return this;
    }

    public void setFromUser(String fromUser) {
        this.fromUser = fromUser == null ? null : fromUser.trim();
    }

    public Date getModifTime() {
        return modifTime;
    }

    public RunfastPayrecord withModifTime(Date modifTime) {
        this.setModifTime(modifTime);
        return this;
    }

    public void setModifTime(Date modifTime) {
        this.modifTime = modifTime;
    }

    public String getOpenid() {
        return openid;
    }

    public RunfastPayrecord withOpenid(String openid) {
        this.setOpenid(openid);
        return this;
    }

    public void setOpenid(String openid) {
        this.openid = openid == null ? null : openid.trim();
    }

    public Integer getOrderId() {
        return orderId;
    }

    public RunfastPayrecord withOrderId(Integer orderId) {
        this.setOrderId(orderId);
        return this;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public RunfastPayrecord withOrderNo(String orderNo) {
        this.setOrderNo(orderNo);
        return this;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo == null ? null : orderNo.trim();
    }

    public Integer getPayType() {
        return payType;
    }

    public RunfastPayrecord withPayType(Integer payType) {
        this.setPayType(payType);
        return this;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public String getToUser() {
        return toUser;
    }

    public RunfastPayrecord withToUser(String toUser) {
        this.setToUser(toUser);
        return this;
    }

    public void setToUser(String toUser) {
        this.toUser = toUser == null ? null : toUser.trim();
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public RunfastPayrecord withTotalPrice(BigDecimal totalPrice) {
        this.setTotalPrice(totalPrice);
        return this;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getTrade_no() {
        return trade_no;
    }

    public RunfastPayrecord withTrade_no(String trade_no) {
        this.setTrade_no(trade_no);
        return this;
    }

    public void setTrade_no(String trade_no) {
        this.trade_no = trade_no == null ? null : trade_no.trim();
    }

    public Integer getType() {
        return type;
    }

    public RunfastPayrecord withType(Integer type) {
        this.setType(type);
        return this;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getUserId() {
        return userId;
    }

    public RunfastPayrecord withUserId(Integer userId) {
        this.setUserId(userId);
        return this;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public RunfastPayrecord withUserName(String userName) {
        this.setUserName(userName);
        return this;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public Integer getAppOrwx() {
        return appOrwx;
    }

    public RunfastPayrecord withAppOrwx(Integer appOrwx) {
        this.setAppOrwx(appOrwx);
        return this;
    }

    public void setAppOrwx(Integer appOrwx) {
        this.appOrwx = appOrwx;
    }

    public String getInfo() {
        return info;
    }

    public RunfastPayrecord withInfo(String info) {
        this.setInfo(info);
        return this;
    }

    public void setInfo(String info) {
        this.info = info == null ? null : info.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", createTime=").append(createTime);
        sb.append(", fromUser=").append(fromUser);
        sb.append(", modifTime=").append(modifTime);
        sb.append(", openid=").append(openid);
        sb.append(", orderId=").append(orderId);
        sb.append(", orderNo=").append(orderNo);
        sb.append(", payType=").append(payType);
        sb.append(", toUser=").append(toUser);
        sb.append(", totalPrice=").append(totalPrice);
        sb.append(", trade_no=").append(trade_no);
        sb.append(", type=").append(type);
        sb.append(", userId=").append(userId);
        sb.append(", userName=").append(userName);
        sb.append(", appOrwx=").append(appOrwx);
        sb.append(", info=").append(info);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}