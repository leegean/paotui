package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.BsTravelAdvert;
import com.runfast.waimai.dao.model.BsTravelAdvertExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BsTravelAdvertMapper extends IMapper<BsTravelAdvert, Integer, BsTravelAdvertExample> {
}