package com.runfast.waimai.dao.model;

import java.io.Serializable;
import java.util.Date;

public class RunfastPrinter implements Serializable {
    private Integer id;

    private Boolean activated;

    private Integer businessId;

    private Integer createBy;

    private Date createDate;

    private String key;

    private String sn;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public RunfastPrinter withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getActivated() {
        return activated;
    }

    public RunfastPrinter withActivated(Boolean activated) {
        this.setActivated(activated);
        return this;
    }

    public void setActivated(Boolean activated) {
        this.activated = activated;
    }

    public Integer getBusinessId() {
        return businessId;
    }

    public RunfastPrinter withBusinessId(Integer businessId) {
        this.setBusinessId(businessId);
        return this;
    }

    public void setBusinessId(Integer businessId) {
        this.businessId = businessId;
    }

    public Integer getCreateBy() {
        return createBy;
    }

    public RunfastPrinter withCreateBy(Integer createBy) {
        this.setCreateBy(createBy);
        return this;
    }

    public void setCreateBy(Integer createBy) {
        this.createBy = createBy;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public RunfastPrinter withCreateDate(Date createDate) {
        this.setCreateDate(createDate);
        return this;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getKey() {
        return key;
    }

    public RunfastPrinter withKey(String key) {
        this.setKey(key);
        return this;
    }

    public void setKey(String key) {
        this.key = key == null ? null : key.trim();
    }

    public String getSn() {
        return sn;
    }

    public RunfastPrinter withSn(String sn) {
        this.setSn(sn);
        return this;
    }

    public void setSn(String sn) {
        this.sn = sn == null ? null : sn.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", activated=").append(activated);
        sb.append(", businessId=").append(businessId);
        sb.append(", createBy=").append(createBy);
        sb.append(", createDate=").append(createDate);
        sb.append(", key=").append(key);
        sb.append(", sn=").append(sn);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}