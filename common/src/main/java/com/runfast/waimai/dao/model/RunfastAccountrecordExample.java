package com.runfast.waimai.dao.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RunfastAccountrecordExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public RunfastAccountrecordExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andBalanceIsNull() {
            addCriterion("balance is null");
            return (Criteria) this;
        }

        public Criteria andBalanceIsNotNull() {
            addCriterion("balance is not null");
            return (Criteria) this;
        }

        public Criteria andBalanceEqualTo(BigDecimal value) {
            addCriterion("balance =", value, "balance");
            return (Criteria) this;
        }

        public Criteria andBalanceNotEqualTo(BigDecimal value) {
            addCriterion("balance <>", value, "balance");
            return (Criteria) this;
        }

        public Criteria andBalanceGreaterThan(BigDecimal value) {
            addCriterion("balance >", value, "balance");
            return (Criteria) this;
        }

        public Criteria andBalanceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("balance >=", value, "balance");
            return (Criteria) this;
        }

        public Criteria andBalanceLessThan(BigDecimal value) {
            addCriterion("balance <", value, "balance");
            return (Criteria) this;
        }

        public Criteria andBalanceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("balance <=", value, "balance");
            return (Criteria) this;
        }

        public Criteria andBalanceIn(List<BigDecimal> values) {
            addCriterion("balance in", values, "balance");
            return (Criteria) this;
        }

        public Criteria andBalanceNotIn(List<BigDecimal> values) {
            addCriterion("balance not in", values, "balance");
            return (Criteria) this;
        }

        public Criteria andBalanceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("balance between", value1, value2, "balance");
            return (Criteria) this;
        }

        public Criteria andBalanceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("balance not between", value1, value2, "balance");
            return (Criteria) this;
        }

        public Criteria andCardnumberIsNull() {
            addCriterion("cardnumber is null");
            return (Criteria) this;
        }

        public Criteria andCardnumberIsNotNull() {
            addCriterion("cardnumber is not null");
            return (Criteria) this;
        }

        public Criteria andCardnumberEqualTo(String value) {
            addCriterion("cardnumber =", value, "cardnumber");
            return (Criteria) this;
        }

        public Criteria andCardnumberNotEqualTo(String value) {
            addCriterion("cardnumber <>", value, "cardnumber");
            return (Criteria) this;
        }

        public Criteria andCardnumberGreaterThan(String value) {
            addCriterion("cardnumber >", value, "cardnumber");
            return (Criteria) this;
        }

        public Criteria andCardnumberGreaterThanOrEqualTo(String value) {
            addCriterion("cardnumber >=", value, "cardnumber");
            return (Criteria) this;
        }

        public Criteria andCardnumberLessThan(String value) {
            addCriterion("cardnumber <", value, "cardnumber");
            return (Criteria) this;
        }

        public Criteria andCardnumberLessThanOrEqualTo(String value) {
            addCriterion("cardnumber <=", value, "cardnumber");
            return (Criteria) this;
        }

        public Criteria andCardnumberLike(String value) {
            addCriterion("cardnumber like", value, "cardnumber");
            return (Criteria) this;
        }

        public Criteria andCardnumberNotLike(String value) {
            addCriterion("cardnumber not like", value, "cardnumber");
            return (Criteria) this;
        }

        public Criteria andCardnumberIn(List<String> values) {
            addCriterion("cardnumber in", values, "cardnumber");
            return (Criteria) this;
        }

        public Criteria andCardnumberNotIn(List<String> values) {
            addCriterion("cardnumber not in", values, "cardnumber");
            return (Criteria) this;
        }

        public Criteria andCardnumberBetween(String value1, String value2) {
            addCriterion("cardnumber between", value1, value2, "cardnumber");
            return (Criteria) this;
        }

        public Criteria andCardnumberNotBetween(String value1, String value2) {
            addCriterion("cardnumber not between", value1, value2, "cardnumber");
            return (Criteria) this;
        }

        public Criteria andCidIsNull() {
            addCriterion("cid is null");
            return (Criteria) this;
        }

        public Criteria andCidIsNotNull() {
            addCriterion("cid is not null");
            return (Criteria) this;
        }

        public Criteria andCidEqualTo(Integer value) {
            addCriterion("cid =", value, "cid");
            return (Criteria) this;
        }

        public Criteria andCidNotEqualTo(Integer value) {
            addCriterion("cid <>", value, "cid");
            return (Criteria) this;
        }

        public Criteria andCidGreaterThan(Integer value) {
            addCriterion("cid >", value, "cid");
            return (Criteria) this;
        }

        public Criteria andCidGreaterThanOrEqualTo(Integer value) {
            addCriterion("cid >=", value, "cid");
            return (Criteria) this;
        }

        public Criteria andCidLessThan(Integer value) {
            addCriterion("cid <", value, "cid");
            return (Criteria) this;
        }

        public Criteria andCidLessThanOrEqualTo(Integer value) {
            addCriterion("cid <=", value, "cid");
            return (Criteria) this;
        }

        public Criteria andCidIn(List<Integer> values) {
            addCriterion("cid in", values, "cid");
            return (Criteria) this;
        }

        public Criteria andCidNotIn(List<Integer> values) {
            addCriterion("cid not in", values, "cid");
            return (Criteria) this;
        }

        public Criteria andCidBetween(Integer value1, Integer value2) {
            addCriterion("cid between", value1, value2, "cid");
            return (Criteria) this;
        }

        public Criteria andCidNotBetween(Integer value1, Integer value2) {
            addCriterion("cid not between", value1, value2, "cid");
            return (Criteria) this;
        }

        public Criteria andMinmonetyIsNull() {
            addCriterion("minmonety is null");
            return (Criteria) this;
        }

        public Criteria andMinmonetyIsNotNull() {
            addCriterion("minmonety is not null");
            return (Criteria) this;
        }

        public Criteria andMinmonetyEqualTo(BigDecimal value) {
            addCriterion("minmonety =", value, "minmonety");
            return (Criteria) this;
        }

        public Criteria andMinmonetyNotEqualTo(BigDecimal value) {
            addCriterion("minmonety <>", value, "minmonety");
            return (Criteria) this;
        }

        public Criteria andMinmonetyGreaterThan(BigDecimal value) {
            addCriterion("minmonety >", value, "minmonety");
            return (Criteria) this;
        }

        public Criteria andMinmonetyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("minmonety >=", value, "minmonety");
            return (Criteria) this;
        }

        public Criteria andMinmonetyLessThan(BigDecimal value) {
            addCriterion("minmonety <", value, "minmonety");
            return (Criteria) this;
        }

        public Criteria andMinmonetyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("minmonety <=", value, "minmonety");
            return (Criteria) this;
        }

        public Criteria andMinmonetyIn(List<BigDecimal> values) {
            addCriterion("minmonety in", values, "minmonety");
            return (Criteria) this;
        }

        public Criteria andMinmonetyNotIn(List<BigDecimal> values) {
            addCriterion("minmonety not in", values, "minmonety");
            return (Criteria) this;
        }

        public Criteria andMinmonetyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("minmonety between", value1, value2, "minmonety");
            return (Criteria) this;
        }

        public Criteria andMinmonetyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("minmonety not between", value1, value2, "minmonety");
            return (Criteria) this;
        }

        public Criteria andMobileIsNull() {
            addCriterion("mobile is null");
            return (Criteria) this;
        }

        public Criteria andMobileIsNotNull() {
            addCriterion("mobile is not null");
            return (Criteria) this;
        }

        public Criteria andMobileEqualTo(String value) {
            addCriterion("mobile =", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotEqualTo(String value) {
            addCriterion("mobile <>", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileGreaterThan(String value) {
            addCriterion("mobile >", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileGreaterThanOrEqualTo(String value) {
            addCriterion("mobile >=", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLessThan(String value) {
            addCriterion("mobile <", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLessThanOrEqualTo(String value) {
            addCriterion("mobile <=", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLike(String value) {
            addCriterion("mobile like", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotLike(String value) {
            addCriterion("mobile not like", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileIn(List<String> values) {
            addCriterion("mobile in", values, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotIn(List<String> values) {
            addCriterion("mobile not in", values, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileBetween(String value1, String value2) {
            addCriterion("mobile between", value1, value2, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotBetween(String value1, String value2) {
            addCriterion("mobile not between", value1, value2, "mobile");
            return (Criteria) this;
        }

        public Criteria andMonetaryIsNull() {
            addCriterion("monetary is null");
            return (Criteria) this;
        }

        public Criteria andMonetaryIsNotNull() {
            addCriterion("monetary is not null");
            return (Criteria) this;
        }

        public Criteria andMonetaryEqualTo(BigDecimal value) {
            addCriterion("monetary =", value, "monetary");
            return (Criteria) this;
        }

        public Criteria andMonetaryNotEqualTo(BigDecimal value) {
            addCriterion("monetary <>", value, "monetary");
            return (Criteria) this;
        }

        public Criteria andMonetaryGreaterThan(BigDecimal value) {
            addCriterion("monetary >", value, "monetary");
            return (Criteria) this;
        }

        public Criteria andMonetaryGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("monetary >=", value, "monetary");
            return (Criteria) this;
        }

        public Criteria andMonetaryLessThan(BigDecimal value) {
            addCriterion("monetary <", value, "monetary");
            return (Criteria) this;
        }

        public Criteria andMonetaryLessThanOrEqualTo(BigDecimal value) {
            addCriterion("monetary <=", value, "monetary");
            return (Criteria) this;
        }

        public Criteria andMonetaryIn(List<BigDecimal> values) {
            addCriterion("monetary in", values, "monetary");
            return (Criteria) this;
        }

        public Criteria andMonetaryNotIn(List<BigDecimal> values) {
            addCriterion("monetary not in", values, "monetary");
            return (Criteria) this;
        }

        public Criteria andMonetaryBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("monetary between", value1, value2, "monetary");
            return (Criteria) this;
        }

        public Criteria andMonetaryNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("monetary not between", value1, value2, "monetary");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andTypeIsNull() {
            addCriterion("type is null");
            return (Criteria) this;
        }

        public Criteria andTypeIsNotNull() {
            addCriterion("type is not null");
            return (Criteria) this;
        }

        public Criteria andTypeEqualTo(Integer value) {
            addCriterion("type =", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotEqualTo(Integer value) {
            addCriterion("type <>", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThan(Integer value) {
            addCriterion("type >", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("type >=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThan(Integer value) {
            addCriterion("type <", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThanOrEqualTo(Integer value) {
            addCriterion("type <=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeIn(List<Integer> values) {
            addCriterion("type in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotIn(List<Integer> values) {
            addCriterion("type not in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeBetween(Integer value1, Integer value2) {
            addCriterion("type between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("type not between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("createTime is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("createTime is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("createTime =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("createTime <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("createTime >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("createTime >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("createTime <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("createTime <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("createTime in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("createTime not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("createTime between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("createTime not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andGenreTypeIsNull() {
            addCriterion("genreType is null");
            return (Criteria) this;
        }

        public Criteria andGenreTypeIsNotNull() {
            addCriterion("genreType is not null");
            return (Criteria) this;
        }

        public Criteria andGenreTypeEqualTo(Integer value) {
            addCriterion("genreType =", value, "genreType");
            return (Criteria) this;
        }

        public Criteria andGenreTypeNotEqualTo(Integer value) {
            addCriterion("genreType <>", value, "genreType");
            return (Criteria) this;
        }

        public Criteria andGenreTypeGreaterThan(Integer value) {
            addCriterion("genreType >", value, "genreType");
            return (Criteria) this;
        }

        public Criteria andGenreTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("genreType >=", value, "genreType");
            return (Criteria) this;
        }

        public Criteria andGenreTypeLessThan(Integer value) {
            addCriterion("genreType <", value, "genreType");
            return (Criteria) this;
        }

        public Criteria andGenreTypeLessThanOrEqualTo(Integer value) {
            addCriterion("genreType <=", value, "genreType");
            return (Criteria) this;
        }

        public Criteria andGenreTypeIn(List<Integer> values) {
            addCriterion("genreType in", values, "genreType");
            return (Criteria) this;
        }

        public Criteria andGenreTypeNotIn(List<Integer> values) {
            addCriterion("genreType not in", values, "genreType");
            return (Criteria) this;
        }

        public Criteria andGenreTypeBetween(Integer value1, Integer value2) {
            addCriterion("genreType between", value1, value2, "genreType");
            return (Criteria) this;
        }

        public Criteria andGenreTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("genreType not between", value1, value2, "genreType");
            return (Criteria) this;
        }

        public Criteria andTypenameIsNull() {
            addCriterion("typename is null");
            return (Criteria) this;
        }

        public Criteria andTypenameIsNotNull() {
            addCriterion("typename is not null");
            return (Criteria) this;
        }

        public Criteria andTypenameEqualTo(String value) {
            addCriterion("typename =", value, "typename");
            return (Criteria) this;
        }

        public Criteria andTypenameNotEqualTo(String value) {
            addCriterion("typename <>", value, "typename");
            return (Criteria) this;
        }

        public Criteria andTypenameGreaterThan(String value) {
            addCriterion("typename >", value, "typename");
            return (Criteria) this;
        }

        public Criteria andTypenameGreaterThanOrEqualTo(String value) {
            addCriterion("typename >=", value, "typename");
            return (Criteria) this;
        }

        public Criteria andTypenameLessThan(String value) {
            addCriterion("typename <", value, "typename");
            return (Criteria) this;
        }

        public Criteria andTypenameLessThanOrEqualTo(String value) {
            addCriterion("typename <=", value, "typename");
            return (Criteria) this;
        }

        public Criteria andTypenameLike(String value) {
            addCriterion("typename like", value, "typename");
            return (Criteria) this;
        }

        public Criteria andTypenameNotLike(String value) {
            addCriterion("typename not like", value, "typename");
            return (Criteria) this;
        }

        public Criteria andTypenameIn(List<String> values) {
            addCriterion("typename in", values, "typename");
            return (Criteria) this;
        }

        public Criteria andTypenameNotIn(List<String> values) {
            addCriterion("typename not in", values, "typename");
            return (Criteria) this;
        }

        public Criteria andTypenameBetween(String value1, String value2) {
            addCriterion("typename between", value1, value2, "typename");
            return (Criteria) this;
        }

        public Criteria andTypenameNotBetween(String value1, String value2) {
            addCriterion("typename not between", value1, value2, "typename");
            return (Criteria) this;
        }

        public Criteria andBeforemonetyIsNull() {
            addCriterion("beforemonety is null");
            return (Criteria) this;
        }

        public Criteria andBeforemonetyIsNotNull() {
            addCriterion("beforemonety is not null");
            return (Criteria) this;
        }

        public Criteria andBeforemonetyEqualTo(BigDecimal value) {
            addCriterion("beforemonety =", value, "beforemonety");
            return (Criteria) this;
        }

        public Criteria andBeforemonetyNotEqualTo(BigDecimal value) {
            addCriterion("beforemonety <>", value, "beforemonety");
            return (Criteria) this;
        }

        public Criteria andBeforemonetyGreaterThan(BigDecimal value) {
            addCriterion("beforemonety >", value, "beforemonety");
            return (Criteria) this;
        }

        public Criteria andBeforemonetyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("beforemonety >=", value, "beforemonety");
            return (Criteria) this;
        }

        public Criteria andBeforemonetyLessThan(BigDecimal value) {
            addCriterion("beforemonety <", value, "beforemonety");
            return (Criteria) this;
        }

        public Criteria andBeforemonetyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("beforemonety <=", value, "beforemonety");
            return (Criteria) this;
        }

        public Criteria andBeforemonetyIn(List<BigDecimal> values) {
            addCriterion("beforemonety in", values, "beforemonety");
            return (Criteria) this;
        }

        public Criteria andBeforemonetyNotIn(List<BigDecimal> values) {
            addCriterion("beforemonety not in", values, "beforemonety");
            return (Criteria) this;
        }

        public Criteria andBeforemonetyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("beforemonety between", value1, value2, "beforemonety");
            return (Criteria) this;
        }

        public Criteria andBeforemonetyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("beforemonety not between", value1, value2, "beforemonety");
            return (Criteria) this;
        }

        public Criteria andShowtypeIsNull() {
            addCriterion("showtype is null");
            return (Criteria) this;
        }

        public Criteria andShowtypeIsNotNull() {
            addCriterion("showtype is not null");
            return (Criteria) this;
        }

        public Criteria andShowtypeEqualTo(Integer value) {
            addCriterion("showtype =", value, "showtype");
            return (Criteria) this;
        }

        public Criteria andShowtypeNotEqualTo(Integer value) {
            addCriterion("showtype <>", value, "showtype");
            return (Criteria) this;
        }

        public Criteria andShowtypeGreaterThan(Integer value) {
            addCriterion("showtype >", value, "showtype");
            return (Criteria) this;
        }

        public Criteria andShowtypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("showtype >=", value, "showtype");
            return (Criteria) this;
        }

        public Criteria andShowtypeLessThan(Integer value) {
            addCriterion("showtype <", value, "showtype");
            return (Criteria) this;
        }

        public Criteria andShowtypeLessThanOrEqualTo(Integer value) {
            addCriterion("showtype <=", value, "showtype");
            return (Criteria) this;
        }

        public Criteria andShowtypeIn(List<Integer> values) {
            addCriterion("showtype in", values, "showtype");
            return (Criteria) this;
        }

        public Criteria andShowtypeNotIn(List<Integer> values) {
            addCriterion("showtype not in", values, "showtype");
            return (Criteria) this;
        }

        public Criteria andShowtypeBetween(Integer value1, Integer value2) {
            addCriterion("showtype between", value1, value2, "showtype");
            return (Criteria) this;
        }

        public Criteria andShowtypeNotBetween(Integer value1, Integer value2) {
            addCriterion("showtype not between", value1, value2, "showtype");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}