package com.runfast.waimai.dao.model;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class RunfastGoodsSellChildren implements Serializable {
    private Integer id;

    private Integer businessId;

    private String businessName;

    private Integer goodsSellId;

    private String goodsSellName;

    private Integer num;

    private String orderCode;

    private Integer pid;

    private Double price;

    private Double totalprice;

    private Integer goodsSellOptionId;

    private String goodsSellOptionName;

    private Integer goodsSellStandardId;

    private String goodsSellStandardName;

    private Date createTime;

    private Integer status;

    private Integer userId;

    private BigDecimal disprice;

    private String optionIds;

    private Integer ptype;

    private Integer activity;

    private Integer errend;

    private Integer activityId;

    private String activityName;

    private Integer activityType;

    private String goods;

    /**
     * 优惠后的总金额
     */
    private Double totalDisprice;

    private String standarOptionName;

    private static final long serialVersionUID = 1L;

}