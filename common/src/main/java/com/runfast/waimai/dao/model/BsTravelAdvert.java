package com.runfast.waimai.dao.model;

import java.io.Serializable;
import java.util.Date;

public class BsTravelAdvert implements Serializable {
    private Integer id;

    /**
     * 广告图片
     */
    private String adImages;

    /**
     * 排序
     */
    private Integer adRank;

    /**
     * 添加时间
     */
    private Date createTime;

    /**
     * 链接地址
     */
    private String linkAddr;

    /**
     * 广告位置  1首页顶部轮播大图广告2外卖首页中通栏横幅左侧广告3外卖首页中通栏横幅右侧广告4外卖首页底部横幅广告
     */
    private Integer location;

    /**
     * 广告标题
     */
    private String title;

    /**
     * 是否启用:1启用,0不启用
     */
    private Integer used;

    /**
     * 链接类型1内容  2链接 3商家分类类型、4商家、5商品、6自定义
     */
    private Integer adType;

    /**
     * 代理商Id
     */
    private Integer agentId;

    /**
     * 代理商名称
     */
    private String agentName;

    /**
     * 对象名称
     */
    private String typename;

    /**
     * 广告类型0外卖1商城2一元购
     */
    private Integer type;

    /**
     * 底部位置在第几位插入
     */
    private Integer num;

    /**
     * 广告分类里对应具体商家，商家分类等id
     */
    private Integer commonId;

    /**
     * 广告内容
     */
    private String content;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public BsTravelAdvert withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAdImages() {
        return adImages;
    }

    public BsTravelAdvert withAdImages(String adImages) {
        this.setAdImages(adImages);
        return this;
    }

    public void setAdImages(String adImages) {
        this.adImages = adImages == null ? null : adImages.trim();
    }

    public Integer getAdRank() {
        return adRank;
    }

    public BsTravelAdvert withAdRank(Integer adRank) {
        this.setAdRank(adRank);
        return this;
    }

    public void setAdRank(Integer adRank) {
        this.adRank = adRank;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public BsTravelAdvert withCreateTime(Date createTime) {
        this.setCreateTime(createTime);
        return this;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getLinkAddr() {
        return linkAddr;
    }

    public BsTravelAdvert withLinkAddr(String linkAddr) {
        this.setLinkAddr(linkAddr);
        return this;
    }

    public void setLinkAddr(String linkAddr) {
        this.linkAddr = linkAddr == null ? null : linkAddr.trim();
    }

    public Integer getLocation() {
        return location;
    }

    public BsTravelAdvert withLocation(Integer location) {
        this.setLocation(location);
        return this;
    }

    public void setLocation(Integer location) {
        this.location = location;
    }

    public String getTitle() {
        return title;
    }

    public BsTravelAdvert withTitle(String title) {
        this.setTitle(title);
        return this;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public Integer getUsed() {
        return used;
    }

    public BsTravelAdvert withUsed(Integer used) {
        this.setUsed(used);
        return this;
    }

    public void setUsed(Integer used) {
        this.used = used;
    }

    public Integer getAdType() {
        return adType;
    }

    public BsTravelAdvert withAdType(Integer adType) {
        this.setAdType(adType);
        return this;
    }

    public void setAdType(Integer adType) {
        this.adType = adType;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public BsTravelAdvert withAgentId(Integer agentId) {
        this.setAgentId(agentId);
        return this;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public String getAgentName() {
        return agentName;
    }

    public BsTravelAdvert withAgentName(String agentName) {
        this.setAgentName(agentName);
        return this;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName == null ? null : agentName.trim();
    }

    public String getTypename() {
        return typename;
    }

    public BsTravelAdvert withTypename(String typename) {
        this.setTypename(typename);
        return this;
    }

    public void setTypename(String typename) {
        this.typename = typename == null ? null : typename.trim();
    }

    public Integer getType() {
        return type;
    }

    public BsTravelAdvert withType(Integer type) {
        this.setType(type);
        return this;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getNum() {
        return num;
    }

    public BsTravelAdvert withNum(Integer num) {
        this.setNum(num);
        return this;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Integer getCommonId() {
        return commonId;
    }

    public BsTravelAdvert withCommonId(Integer commonId) {
        this.setCommonId(commonId);
        return this;
    }

    public void setCommonId(Integer commonId) {
        this.commonId = commonId;
    }

    public String getContent() {
        return content;
    }

    public BsTravelAdvert withContent(String content) {
        this.setContent(content);
        return this;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", adImages=").append(adImages);
        sb.append(", adRank=").append(adRank);
        sb.append(", createTime=").append(createTime);
        sb.append(", linkAddr=").append(linkAddr);
        sb.append(", location=").append(location);
        sb.append(", title=").append(title);
        sb.append(", used=").append(used);
        sb.append(", adType=").append(adType);
        sb.append(", agentId=").append(agentId);
        sb.append(", agentName=").append(agentName);
        sb.append(", typename=").append(typename);
        sb.append(", type=").append(type);
        sb.append(", num=").append(num);
        sb.append(", commonId=").append(commonId);
        sb.append(", content=").append(content);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}