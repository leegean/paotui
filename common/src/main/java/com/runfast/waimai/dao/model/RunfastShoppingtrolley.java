package com.runfast.waimai.dao.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class RunfastShoppingtrolley implements Serializable {
    private Integer id;

    private String barCode;

    private Integer businessId;

    private String businessName;

    private Integer cid;

    private String cname;

    private Integer goodsSellId;

    private String goodsSellName;

    private Integer goodsSellOptionId;

    private String goodsSellOptionName;

    private Integer goodsSellStandardId;

    private String goodsSellStandardName;

    private Integer num;

    private Double price;

    private String openid;

    private Integer goodsSellRecordId;

    private Integer snum;

    private BigDecimal packing;

    private Integer ptype;

    private BigDecimal disprice;

    private BigDecimal pricedis;

    private String optionIds;

    private Integer islimited;

    private Integer limitNum;

    private Integer couponid;

    private BigDecimal full;

    private BigDecimal total;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public RunfastShoppingtrolley withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBarCode() {
        return barCode;
    }

    public RunfastShoppingtrolley withBarCode(String barCode) {
        this.setBarCode(barCode);
        return this;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode == null ? null : barCode.trim();
    }

    public Integer getBusinessId() {
        return businessId;
    }

    public RunfastShoppingtrolley withBusinessId(Integer businessId) {
        this.setBusinessId(businessId);
        return this;
    }

    public void setBusinessId(Integer businessId) {
        this.businessId = businessId;
    }

    public String getBusinessName() {
        return businessName;
    }

    public RunfastShoppingtrolley withBusinessName(String businessName) {
        this.setBusinessName(businessName);
        return this;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName == null ? null : businessName.trim();
    }

    public Integer getCid() {
        return cid;
    }

    public RunfastShoppingtrolley withCid(Integer cid) {
        this.setCid(cid);
        return this;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public String getCname() {
        return cname;
    }

    public RunfastShoppingtrolley withCname(String cname) {
        this.setCname(cname);
        return this;
    }

    public void setCname(String cname) {
        this.cname = cname == null ? null : cname.trim();
    }

    public Integer getGoodsSellId() {
        return goodsSellId;
    }

    public RunfastShoppingtrolley withGoodsSellId(Integer goodsSellId) {
        this.setGoodsSellId(goodsSellId);
        return this;
    }

    public void setGoodsSellId(Integer goodsSellId) {
        this.goodsSellId = goodsSellId;
    }

    public String getGoodsSellName() {
        return goodsSellName;
    }

    public RunfastShoppingtrolley withGoodsSellName(String goodsSellName) {
        this.setGoodsSellName(goodsSellName);
        return this;
    }

    public void setGoodsSellName(String goodsSellName) {
        this.goodsSellName = goodsSellName == null ? null : goodsSellName.trim();
    }

    public Integer getGoodsSellOptionId() {
        return goodsSellOptionId;
    }

    public RunfastShoppingtrolley withGoodsSellOptionId(Integer goodsSellOptionId) {
        this.setGoodsSellOptionId(goodsSellOptionId);
        return this;
    }

    public void setGoodsSellOptionId(Integer goodsSellOptionId) {
        this.goodsSellOptionId = goodsSellOptionId;
    }

    public String getGoodsSellOptionName() {
        return goodsSellOptionName;
    }

    public RunfastShoppingtrolley withGoodsSellOptionName(String goodsSellOptionName) {
        this.setGoodsSellOptionName(goodsSellOptionName);
        return this;
    }

    public void setGoodsSellOptionName(String goodsSellOptionName) {
        this.goodsSellOptionName = goodsSellOptionName == null ? null : goodsSellOptionName.trim();
    }

    public Integer getGoodsSellStandardId() {
        return goodsSellStandardId;
    }

    public RunfastShoppingtrolley withGoodsSellStandardId(Integer goodsSellStandardId) {
        this.setGoodsSellStandardId(goodsSellStandardId);
        return this;
    }

    public void setGoodsSellStandardId(Integer goodsSellStandardId) {
        this.goodsSellStandardId = goodsSellStandardId;
    }

    public String getGoodsSellStandardName() {
        return goodsSellStandardName;
    }

    public RunfastShoppingtrolley withGoodsSellStandardName(String goodsSellStandardName) {
        this.setGoodsSellStandardName(goodsSellStandardName);
        return this;
    }

    public void setGoodsSellStandardName(String goodsSellStandardName) {
        this.goodsSellStandardName = goodsSellStandardName == null ? null : goodsSellStandardName.trim();
    }

    public Integer getNum() {
        return num;
    }

    public RunfastShoppingtrolley withNum(Integer num) {
        this.setNum(num);
        return this;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Double getPrice() {
        return price;
    }

    public RunfastShoppingtrolley withPrice(Double price) {
        this.setPrice(price);
        return this;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getOpenid() {
        return openid;
    }

    public RunfastShoppingtrolley withOpenid(String openid) {
        this.setOpenid(openid);
        return this;
    }

    public void setOpenid(String openid) {
        this.openid = openid == null ? null : openid.trim();
    }

    public Integer getGoodsSellRecordId() {
        return goodsSellRecordId;
    }

    public RunfastShoppingtrolley withGoodsSellRecordId(Integer goodsSellRecordId) {
        this.setGoodsSellRecordId(goodsSellRecordId);
        return this;
    }

    public void setGoodsSellRecordId(Integer goodsSellRecordId) {
        this.goodsSellRecordId = goodsSellRecordId;
    }

    public Integer getSnum() {
        return snum;
    }

    public RunfastShoppingtrolley withSnum(Integer snum) {
        this.setSnum(snum);
        return this;
    }

    public void setSnum(Integer snum) {
        this.snum = snum;
    }

    public BigDecimal getPacking() {
        return packing;
    }

    public RunfastShoppingtrolley withPacking(BigDecimal packing) {
        this.setPacking(packing);
        return this;
    }

    public void setPacking(BigDecimal packing) {
        this.packing = packing;
    }

    public Integer getPtype() {
        return ptype;
    }

    public RunfastShoppingtrolley withPtype(Integer ptype) {
        this.setPtype(ptype);
        return this;
    }

    public void setPtype(Integer ptype) {
        this.ptype = ptype;
    }

    public BigDecimal getDisprice() {
        return disprice;
    }

    public RunfastShoppingtrolley withDisprice(BigDecimal disprice) {
        this.setDisprice(disprice);
        return this;
    }

    public void setDisprice(BigDecimal disprice) {
        this.disprice = disprice;
    }

    public BigDecimal getPricedis() {
        return pricedis;
    }

    public RunfastShoppingtrolley withPricedis(BigDecimal pricedis) {
        this.setPricedis(pricedis);
        return this;
    }

    public void setPricedis(BigDecimal pricedis) {
        this.pricedis = pricedis;
    }

    public String getOptionIds() {
        return optionIds;
    }

    public RunfastShoppingtrolley withOptionIds(String optionIds) {
        this.setOptionIds(optionIds);
        return this;
    }

    public void setOptionIds(String optionIds) {
        this.optionIds = optionIds == null ? null : optionIds.trim();
    }

    public Integer getIslimited() {
        return islimited;
    }

    public RunfastShoppingtrolley withIslimited(Integer islimited) {
        this.setIslimited(islimited);
        return this;
    }

    public void setIslimited(Integer islimited) {
        this.islimited = islimited;
    }

    public Integer getLimitNum() {
        return limitNum;
    }

    public RunfastShoppingtrolley withLimitNum(Integer limitNum) {
        this.setLimitNum(limitNum);
        return this;
    }

    public void setLimitNum(Integer limitNum) {
        this.limitNum = limitNum;
    }

    public Integer getCouponid() {
        return couponid;
    }

    public RunfastShoppingtrolley withCouponid(Integer couponid) {
        this.setCouponid(couponid);
        return this;
    }

    public void setCouponid(Integer couponid) {
        this.couponid = couponid;
    }

    public BigDecimal getFull() {
        return full;
    }

    public RunfastShoppingtrolley withFull(BigDecimal full) {
        this.setFull(full);
        return this;
    }

    public void setFull(BigDecimal full) {
        this.full = full;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public RunfastShoppingtrolley withTotal(BigDecimal total) {
        this.setTotal(total);
        return this;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", barCode=").append(barCode);
        sb.append(", businessId=").append(businessId);
        sb.append(", businessName=").append(businessName);
        sb.append(", cid=").append(cid);
        sb.append(", cname=").append(cname);
        sb.append(", goodsSellId=").append(goodsSellId);
        sb.append(", goodsSellName=").append(goodsSellName);
        sb.append(", goodsSellOptionId=").append(goodsSellOptionId);
        sb.append(", goodsSellOptionName=").append(goodsSellOptionName);
        sb.append(", goodsSellStandardId=").append(goodsSellStandardId);
        sb.append(", goodsSellStandardName=").append(goodsSellStandardName);
        sb.append(", num=").append(num);
        sb.append(", price=").append(price);
        sb.append(", openid=").append(openid);
        sb.append(", goodsSellRecordId=").append(goodsSellRecordId);
        sb.append(", snum=").append(snum);
        sb.append(", packing=").append(packing);
        sb.append(", ptype=").append(ptype);
        sb.append(", disprice=").append(disprice);
        sb.append(", pricedis=").append(pricedis);
        sb.append(", optionIds=").append(optionIds);
        sb.append(", islimited=").append(islimited);
        sb.append(", limitNum=").append(limitNum);
        sb.append(", couponid=").append(couponid);
        sb.append(", full=").append(full);
        sb.append(", total=").append(total);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}