package com.runfast.waimai.dao.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class RunfastEnshrine implements Serializable {
    private Integer id;

    private Integer cid;

    private Date createTime;

    private String name;

    private Integer shopId;

    private String shopname;

    private String mobile;

    private String openid;

    private Integer type;

    private String imgPath;

    private BigDecimal startPay;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public RunfastEnshrine withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCid() {
        return cid;
    }

    public RunfastEnshrine withCid(Integer cid) {
        this.setCid(cid);
        return this;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public RunfastEnshrine withCreateTime(Date createTime) {
        this.setCreateTime(createTime);
        return this;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getName() {
        return name;
    }

    public RunfastEnshrine withName(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getShopId() {
        return shopId;
    }

    public RunfastEnshrine withShopId(Integer shopId) {
        this.setShopId(shopId);
        return this;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public String getShopname() {
        return shopname;
    }

    public RunfastEnshrine withShopname(String shopname) {
        this.setShopname(shopname);
        return this;
    }

    public void setShopname(String shopname) {
        this.shopname = shopname == null ? null : shopname.trim();
    }

    public String getMobile() {
        return mobile;
    }

    public RunfastEnshrine withMobile(String mobile) {
        this.setMobile(mobile);
        return this;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public String getOpenid() {
        return openid;
    }

    public RunfastEnshrine withOpenid(String openid) {
        this.setOpenid(openid);
        return this;
    }

    public void setOpenid(String openid) {
        this.openid = openid == null ? null : openid.trim();
    }

    public Integer getType() {
        return type;
    }

    public RunfastEnshrine withType(Integer type) {
        this.setType(type);
        return this;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getImgPath() {
        return imgPath;
    }

    public RunfastEnshrine withImgPath(String imgPath) {
        this.setImgPath(imgPath);
        return this;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath == null ? null : imgPath.trim();
    }

    public BigDecimal getStartPay() {
        return startPay;
    }

    public RunfastEnshrine withStartPay(BigDecimal startPay) {
        this.setStartPay(startPay);
        return this;
    }

    public void setStartPay(BigDecimal startPay) {
        this.startPay = startPay;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", cid=").append(cid);
        sb.append(", createTime=").append(createTime);
        sb.append(", name=").append(name);
        sb.append(", shopId=").append(shopId);
        sb.append(", shopname=").append(shopname);
        sb.append(", mobile=").append(mobile);
        sb.append(", openid=").append(openid);
        sb.append(", type=").append(type);
        sb.append(", imgPath=").append(imgPath);
        sb.append(", startPay=").append(startPay);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}