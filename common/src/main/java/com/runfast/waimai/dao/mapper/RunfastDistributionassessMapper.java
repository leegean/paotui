package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastDistributionassess;
import com.runfast.waimai.dao.model.RunfastDistributionassessExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastDistributionassessMapper extends IMapper<RunfastDistributionassess, Integer, RunfastDistributionassessExample> {
}