package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastPayrecord;
import com.runfast.waimai.dao.model.RunfastPayrecordExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastPayrecordMapper extends IMapper<RunfastPayrecord, Integer, RunfastPayrecordExample> {
}