package com.runfast.waimai.dao.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RunfastGoodsSellRecordExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public RunfastGoodsSellRecordExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andBusinessIdIsNull() {
            addCriterion("businessId is null");
            return (Criteria) this;
        }

        public Criteria andBusinessIdIsNotNull() {
            addCriterion("businessId is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessIdEqualTo(Integer value) {
            addCriterion("businessId =", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdNotEqualTo(Integer value) {
            addCriterion("businessId <>", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdGreaterThan(Integer value) {
            addCriterion("businessId >", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("businessId >=", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdLessThan(Integer value) {
            addCriterion("businessId <", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdLessThanOrEqualTo(Integer value) {
            addCriterion("businessId <=", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdIn(List<Integer> values) {
            addCriterion("businessId in", values, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdNotIn(List<Integer> values) {
            addCriterion("businessId not in", values, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdBetween(Integer value1, Integer value2) {
            addCriterion("businessId between", value1, value2, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdNotBetween(Integer value1, Integer value2) {
            addCriterion("businessId not between", value1, value2, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessNameIsNull() {
            addCriterion("businessName is null");
            return (Criteria) this;
        }

        public Criteria andBusinessNameIsNotNull() {
            addCriterion("businessName is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessNameEqualTo(String value) {
            addCriterion("businessName =", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameNotEqualTo(String value) {
            addCriterion("businessName <>", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameGreaterThan(String value) {
            addCriterion("businessName >", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameGreaterThanOrEqualTo(String value) {
            addCriterion("businessName >=", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameLessThan(String value) {
            addCriterion("businessName <", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameLessThanOrEqualTo(String value) {
            addCriterion("businessName <=", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameLike(String value) {
            addCriterion("businessName like", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameNotLike(String value) {
            addCriterion("businessName not like", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameIn(List<String> values) {
            addCriterion("businessName in", values, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameNotIn(List<String> values) {
            addCriterion("businessName not in", values, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameBetween(String value1, String value2) {
            addCriterion("businessName between", value1, value2, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameNotBetween(String value1, String value2) {
            addCriterion("businessName not between", value1, value2, "businessName");
            return (Criteria) this;
        }

        public Criteria andContentIsNull() {
            addCriterion("content is null");
            return (Criteria) this;
        }

        public Criteria andContentIsNotNull() {
            addCriterion("content is not null");
            return (Criteria) this;
        }

        public Criteria andContentEqualTo(String value) {
            addCriterion("content =", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotEqualTo(String value) {
            addCriterion("content <>", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentGreaterThan(String value) {
            addCriterion("content >", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentGreaterThanOrEqualTo(String value) {
            addCriterion("content >=", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLessThan(String value) {
            addCriterion("content <", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLessThanOrEqualTo(String value) {
            addCriterion("content <=", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLike(String value) {
            addCriterion("content like", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotLike(String value) {
            addCriterion("content not like", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentIn(List<String> values) {
            addCriterion("content in", values, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotIn(List<String> values) {
            addCriterion("content not in", values, "content");
            return (Criteria) this;
        }

        public Criteria andContentBetween(String value1, String value2) {
            addCriterion("content between", value1, value2, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotBetween(String value1, String value2) {
            addCriterion("content not between", value1, value2, "content");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("createTime is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("createTime is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("createTime =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("createTime <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("createTime >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("createTime >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("createTime <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("createTime <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("createTime in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("createTime not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("createTime between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("createTime not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdIsNull() {
            addCriterion("goodsSellId is null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdIsNotNull() {
            addCriterion("goodsSellId is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdEqualTo(Integer value) {
            addCriterion("goodsSellId =", value, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdNotEqualTo(Integer value) {
            addCriterion("goodsSellId <>", value, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdGreaterThan(Integer value) {
            addCriterion("goodsSellId >", value, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("goodsSellId >=", value, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdLessThan(Integer value) {
            addCriterion("goodsSellId <", value, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdLessThanOrEqualTo(Integer value) {
            addCriterion("goodsSellId <=", value, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdIn(List<Integer> values) {
            addCriterion("goodsSellId in", values, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdNotIn(List<Integer> values) {
            addCriterion("goodsSellId not in", values, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdBetween(Integer value1, Integer value2) {
            addCriterion("goodsSellId between", value1, value2, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdNotBetween(Integer value1, Integer value2) {
            addCriterion("goodsSellId not between", value1, value2, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameIsNull() {
            addCriterion("goodsSellName is null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameIsNotNull() {
            addCriterion("goodsSellName is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameEqualTo(String value) {
            addCriterion("goodsSellName =", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameNotEqualTo(String value) {
            addCriterion("goodsSellName <>", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameGreaterThan(String value) {
            addCriterion("goodsSellName >", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameGreaterThanOrEqualTo(String value) {
            addCriterion("goodsSellName >=", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameLessThan(String value) {
            addCriterion("goodsSellName <", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameLessThanOrEqualTo(String value) {
            addCriterion("goodsSellName <=", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameLike(String value) {
            addCriterion("goodsSellName like", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameNotLike(String value) {
            addCriterion("goodsSellName not like", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameIn(List<String> values) {
            addCriterion("goodsSellName in", values, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameNotIn(List<String> values) {
            addCriterion("goodsSellName not in", values, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameBetween(String value1, String value2) {
            addCriterion("goodsSellName between", value1, value2, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameNotBetween(String value1, String value2) {
            addCriterion("goodsSellName not between", value1, value2, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andOrderCodeIsNull() {
            addCriterion("orderCode is null");
            return (Criteria) this;
        }

        public Criteria andOrderCodeIsNotNull() {
            addCriterion("orderCode is not null");
            return (Criteria) this;
        }

        public Criteria andOrderCodeEqualTo(String value) {
            addCriterion("orderCode =", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeNotEqualTo(String value) {
            addCriterion("orderCode <>", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeGreaterThan(String value) {
            addCriterion("orderCode >", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeGreaterThanOrEqualTo(String value) {
            addCriterion("orderCode >=", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeLessThan(String value) {
            addCriterion("orderCode <", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeLessThanOrEqualTo(String value) {
            addCriterion("orderCode <=", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeLike(String value) {
            addCriterion("orderCode like", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeNotLike(String value) {
            addCriterion("orderCode not like", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeIn(List<String> values) {
            addCriterion("orderCode in", values, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeNotIn(List<String> values) {
            addCriterion("orderCode not in", values, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeBetween(String value1, String value2) {
            addCriterion("orderCode between", value1, value2, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeNotBetween(String value1, String value2) {
            addCriterion("orderCode not between", value1, value2, "orderCode");
            return (Criteria) this;
        }

        public Criteria andPriceIsNull() {
            addCriterion("price is null");
            return (Criteria) this;
        }

        public Criteria andPriceIsNotNull() {
            addCriterion("price is not null");
            return (Criteria) this;
        }

        public Criteria andPriceEqualTo(BigDecimal value) {
            addCriterion("price =", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotEqualTo(BigDecimal value) {
            addCriterion("price <>", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceGreaterThan(BigDecimal value) {
            addCriterion("price >", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("price >=", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceLessThan(BigDecimal value) {
            addCriterion("price <", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("price <=", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceIn(List<BigDecimal> values) {
            addCriterion("price in", values, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotIn(List<BigDecimal> values) {
            addCriterion("price not in", values, "price");
            return (Criteria) this;
        }

        public Criteria andPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("price between", value1, value2, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("price not between", value1, value2, "price");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andUserAddressIsNull() {
            addCriterion("userAddress is null");
            return (Criteria) this;
        }

        public Criteria andUserAddressIsNotNull() {
            addCriterion("userAddress is not null");
            return (Criteria) this;
        }

        public Criteria andUserAddressEqualTo(String value) {
            addCriterion("userAddress =", value, "userAddress");
            return (Criteria) this;
        }

        public Criteria andUserAddressNotEqualTo(String value) {
            addCriterion("userAddress <>", value, "userAddress");
            return (Criteria) this;
        }

        public Criteria andUserAddressGreaterThan(String value) {
            addCriterion("userAddress >", value, "userAddress");
            return (Criteria) this;
        }

        public Criteria andUserAddressGreaterThanOrEqualTo(String value) {
            addCriterion("userAddress >=", value, "userAddress");
            return (Criteria) this;
        }

        public Criteria andUserAddressLessThan(String value) {
            addCriterion("userAddress <", value, "userAddress");
            return (Criteria) this;
        }

        public Criteria andUserAddressLessThanOrEqualTo(String value) {
            addCriterion("userAddress <=", value, "userAddress");
            return (Criteria) this;
        }

        public Criteria andUserAddressLike(String value) {
            addCriterion("userAddress like", value, "userAddress");
            return (Criteria) this;
        }

        public Criteria andUserAddressNotLike(String value) {
            addCriterion("userAddress not like", value, "userAddress");
            return (Criteria) this;
        }

        public Criteria andUserAddressIn(List<String> values) {
            addCriterion("userAddress in", values, "userAddress");
            return (Criteria) this;
        }

        public Criteria andUserAddressNotIn(List<String> values) {
            addCriterion("userAddress not in", values, "userAddress");
            return (Criteria) this;
        }

        public Criteria andUserAddressBetween(String value1, String value2) {
            addCriterion("userAddress between", value1, value2, "userAddress");
            return (Criteria) this;
        }

        public Criteria andUserAddressNotBetween(String value1, String value2) {
            addCriterion("userAddress not between", value1, value2, "userAddress");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNull() {
            addCriterion("userId is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("userId is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(Integer value) {
            addCriterion("userId =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(Integer value) {
            addCriterion("userId <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(Integer value) {
            addCriterion("userId >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("userId >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(Integer value) {
            addCriterion("userId <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(Integer value) {
            addCriterion("userId <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<Integer> values) {
            addCriterion("userId in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<Integer> values) {
            addCriterion("userId not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(Integer value1, Integer value2) {
            addCriterion("userId between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(Integer value1, Integer value2) {
            addCriterion("userId not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserMobileIsNull() {
            addCriterion("userMobile is null");
            return (Criteria) this;
        }

        public Criteria andUserMobileIsNotNull() {
            addCriterion("userMobile is not null");
            return (Criteria) this;
        }

        public Criteria andUserMobileEqualTo(String value) {
            addCriterion("userMobile =", value, "userMobile");
            return (Criteria) this;
        }

        public Criteria andUserMobileNotEqualTo(String value) {
            addCriterion("userMobile <>", value, "userMobile");
            return (Criteria) this;
        }

        public Criteria andUserMobileGreaterThan(String value) {
            addCriterion("userMobile >", value, "userMobile");
            return (Criteria) this;
        }

        public Criteria andUserMobileGreaterThanOrEqualTo(String value) {
            addCriterion("userMobile >=", value, "userMobile");
            return (Criteria) this;
        }

        public Criteria andUserMobileLessThan(String value) {
            addCriterion("userMobile <", value, "userMobile");
            return (Criteria) this;
        }

        public Criteria andUserMobileLessThanOrEqualTo(String value) {
            addCriterion("userMobile <=", value, "userMobile");
            return (Criteria) this;
        }

        public Criteria andUserMobileLike(String value) {
            addCriterion("userMobile like", value, "userMobile");
            return (Criteria) this;
        }

        public Criteria andUserMobileNotLike(String value) {
            addCriterion("userMobile not like", value, "userMobile");
            return (Criteria) this;
        }

        public Criteria andUserMobileIn(List<String> values) {
            addCriterion("userMobile in", values, "userMobile");
            return (Criteria) this;
        }

        public Criteria andUserMobileNotIn(List<String> values) {
            addCriterion("userMobile not in", values, "userMobile");
            return (Criteria) this;
        }

        public Criteria andUserMobileBetween(String value1, String value2) {
            addCriterion("userMobile between", value1, value2, "userMobile");
            return (Criteria) this;
        }

        public Criteria andUserMobileNotBetween(String value1, String value2) {
            addCriterion("userMobile not between", value1, value2, "userMobile");
            return (Criteria) this;
        }

        public Criteria andUserNameIsNull() {
            addCriterion("userName is null");
            return (Criteria) this;
        }

        public Criteria andUserNameIsNotNull() {
            addCriterion("userName is not null");
            return (Criteria) this;
        }

        public Criteria andUserNameEqualTo(String value) {
            addCriterion("userName =", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotEqualTo(String value) {
            addCriterion("userName <>", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameGreaterThan(String value) {
            addCriterion("userName >", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameGreaterThanOrEqualTo(String value) {
            addCriterion("userName >=", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLessThan(String value) {
            addCriterion("userName <", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLessThanOrEqualTo(String value) {
            addCriterion("userName <=", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLike(String value) {
            addCriterion("userName like", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotLike(String value) {
            addCriterion("userName not like", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameIn(List<String> values) {
            addCriterion("userName in", values, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotIn(List<String> values) {
            addCriterion("userName not in", values, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameBetween(String value1, String value2) {
            addCriterion("userName between", value1, value2, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotBetween(String value1, String value2) {
            addCriterion("userName not between", value1, value2, "userName");
            return (Criteria) this;
        }

        public Criteria andShopperIsNull() {
            addCriterion("shopper is null");
            return (Criteria) this;
        }

        public Criteria andShopperIsNotNull() {
            addCriterion("shopper is not null");
            return (Criteria) this;
        }

        public Criteria andShopperEqualTo(String value) {
            addCriterion("shopper =", value, "shopper");
            return (Criteria) this;
        }

        public Criteria andShopperNotEqualTo(String value) {
            addCriterion("shopper <>", value, "shopper");
            return (Criteria) this;
        }

        public Criteria andShopperGreaterThan(String value) {
            addCriterion("shopper >", value, "shopper");
            return (Criteria) this;
        }

        public Criteria andShopperGreaterThanOrEqualTo(String value) {
            addCriterion("shopper >=", value, "shopper");
            return (Criteria) this;
        }

        public Criteria andShopperLessThan(String value) {
            addCriterion("shopper <", value, "shopper");
            return (Criteria) this;
        }

        public Criteria andShopperLessThanOrEqualTo(String value) {
            addCriterion("shopper <=", value, "shopper");
            return (Criteria) this;
        }

        public Criteria andShopperLike(String value) {
            addCriterion("shopper like", value, "shopper");
            return (Criteria) this;
        }

        public Criteria andShopperNotLike(String value) {
            addCriterion("shopper not like", value, "shopper");
            return (Criteria) this;
        }

        public Criteria andShopperIn(List<String> values) {
            addCriterion("shopper in", values, "shopper");
            return (Criteria) this;
        }

        public Criteria andShopperNotIn(List<String> values) {
            addCriterion("shopper not in", values, "shopper");
            return (Criteria) this;
        }

        public Criteria andShopperBetween(String value1, String value2) {
            addCriterion("shopper between", value1, value2, "shopper");
            return (Criteria) this;
        }

        public Criteria andShopperNotBetween(String value1, String value2) {
            addCriterion("shopper not between", value1, value2, "shopper");
            return (Criteria) this;
        }

        public Criteria andShopperIdIsNull() {
            addCriterion("shopperId is null");
            return (Criteria) this;
        }

        public Criteria andShopperIdIsNotNull() {
            addCriterion("shopperId is not null");
            return (Criteria) this;
        }

        public Criteria andShopperIdEqualTo(Integer value) {
            addCriterion("shopperId =", value, "shopperId");
            return (Criteria) this;
        }

        public Criteria andShopperIdNotEqualTo(Integer value) {
            addCriterion("shopperId <>", value, "shopperId");
            return (Criteria) this;
        }

        public Criteria andShopperIdGreaterThan(Integer value) {
            addCriterion("shopperId >", value, "shopperId");
            return (Criteria) this;
        }

        public Criteria andShopperIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("shopperId >=", value, "shopperId");
            return (Criteria) this;
        }

        public Criteria andShopperIdLessThan(Integer value) {
            addCriterion("shopperId <", value, "shopperId");
            return (Criteria) this;
        }

        public Criteria andShopperIdLessThanOrEqualTo(Integer value) {
            addCriterion("shopperId <=", value, "shopperId");
            return (Criteria) this;
        }

        public Criteria andShopperIdIn(List<Integer> values) {
            addCriterion("shopperId in", values, "shopperId");
            return (Criteria) this;
        }

        public Criteria andShopperIdNotIn(List<Integer> values) {
            addCriterion("shopperId not in", values, "shopperId");
            return (Criteria) this;
        }

        public Criteria andShopperIdBetween(Integer value1, Integer value2) {
            addCriterion("shopperId between", value1, value2, "shopperId");
            return (Criteria) this;
        }

        public Criteria andShopperIdNotBetween(Integer value1, Integer value2) {
            addCriterion("shopperId not between", value1, value2, "shopperId");
            return (Criteria) this;
        }

        public Criteria andGoodsTotalIsNull() {
            addCriterion("goodsTotal is null");
            return (Criteria) this;
        }

        public Criteria andGoodsTotalIsNotNull() {
            addCriterion("goodsTotal is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsTotalEqualTo(Integer value) {
            addCriterion("goodsTotal =", value, "goodsTotal");
            return (Criteria) this;
        }

        public Criteria andGoodsTotalNotEqualTo(Integer value) {
            addCriterion("goodsTotal <>", value, "goodsTotal");
            return (Criteria) this;
        }

        public Criteria andGoodsTotalGreaterThan(Integer value) {
            addCriterion("goodsTotal >", value, "goodsTotal");
            return (Criteria) this;
        }

        public Criteria andGoodsTotalGreaterThanOrEqualTo(Integer value) {
            addCriterion("goodsTotal >=", value, "goodsTotal");
            return (Criteria) this;
        }

        public Criteria andGoodsTotalLessThan(Integer value) {
            addCriterion("goodsTotal <", value, "goodsTotal");
            return (Criteria) this;
        }

        public Criteria andGoodsTotalLessThanOrEqualTo(Integer value) {
            addCriterion("goodsTotal <=", value, "goodsTotal");
            return (Criteria) this;
        }

        public Criteria andGoodsTotalIn(List<Integer> values) {
            addCriterion("goodsTotal in", values, "goodsTotal");
            return (Criteria) this;
        }

        public Criteria andGoodsTotalNotIn(List<Integer> values) {
            addCriterion("goodsTotal not in", values, "goodsTotal");
            return (Criteria) this;
        }

        public Criteria andGoodsTotalBetween(Integer value1, Integer value2) {
            addCriterion("goodsTotal between", value1, value2, "goodsTotal");
            return (Criteria) this;
        }

        public Criteria andGoodsTotalNotBetween(Integer value1, Integer value2) {
            addCriterion("goodsTotal not between", value1, value2, "goodsTotal");
            return (Criteria) this;
        }

        public Criteria andUserAddressIdIsNull() {
            addCriterion("userAddressId is null");
            return (Criteria) this;
        }

        public Criteria andUserAddressIdIsNotNull() {
            addCriterion("userAddressId is not null");
            return (Criteria) this;
        }

        public Criteria andUserAddressIdEqualTo(Integer value) {
            addCriterion("userAddressId =", value, "userAddressId");
            return (Criteria) this;
        }

        public Criteria andUserAddressIdNotEqualTo(Integer value) {
            addCriterion("userAddressId <>", value, "userAddressId");
            return (Criteria) this;
        }

        public Criteria andUserAddressIdGreaterThan(Integer value) {
            addCriterion("userAddressId >", value, "userAddressId");
            return (Criteria) this;
        }

        public Criteria andUserAddressIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("userAddressId >=", value, "userAddressId");
            return (Criteria) this;
        }

        public Criteria andUserAddressIdLessThan(Integer value) {
            addCriterion("userAddressId <", value, "userAddressId");
            return (Criteria) this;
        }

        public Criteria andUserAddressIdLessThanOrEqualTo(Integer value) {
            addCriterion("userAddressId <=", value, "userAddressId");
            return (Criteria) this;
        }

        public Criteria andUserAddressIdIn(List<Integer> values) {
            addCriterion("userAddressId in", values, "userAddressId");
            return (Criteria) this;
        }

        public Criteria andUserAddressIdNotIn(List<Integer> values) {
            addCriterion("userAddressId not in", values, "userAddressId");
            return (Criteria) this;
        }

        public Criteria andUserAddressIdBetween(Integer value1, Integer value2) {
            addCriterion("userAddressId between", value1, value2, "userAddressId");
            return (Criteria) this;
        }

        public Criteria andUserAddressIdNotBetween(Integer value1, Integer value2) {
            addCriterion("userAddressId not between", value1, value2, "userAddressId");
            return (Criteria) this;
        }

        public Criteria andYhpriceIsNull() {
            addCriterion("yhprice is null");
            return (Criteria) this;
        }

        public Criteria andYhpriceIsNotNull() {
            addCriterion("yhprice is not null");
            return (Criteria) this;
        }

        public Criteria andYhpriceEqualTo(Double value) {
            addCriterion("yhprice =", value, "yhprice");
            return (Criteria) this;
        }

        public Criteria andYhpriceNotEqualTo(Double value) {
            addCriterion("yhprice <>", value, "yhprice");
            return (Criteria) this;
        }

        public Criteria andYhpriceGreaterThan(Double value) {
            addCriterion("yhprice >", value, "yhprice");
            return (Criteria) this;
        }

        public Criteria andYhpriceGreaterThanOrEqualTo(Double value) {
            addCriterion("yhprice >=", value, "yhprice");
            return (Criteria) this;
        }

        public Criteria andYhpriceLessThan(Double value) {
            addCriterion("yhprice <", value, "yhprice");
            return (Criteria) this;
        }

        public Criteria andYhpriceLessThanOrEqualTo(Double value) {
            addCriterion("yhprice <=", value, "yhprice");
            return (Criteria) this;
        }

        public Criteria andYhpriceIn(List<Double> values) {
            addCriterion("yhprice in", values, "yhprice");
            return (Criteria) this;
        }

        public Criteria andYhpriceNotIn(List<Double> values) {
            addCriterion("yhprice not in", values, "yhprice");
            return (Criteria) this;
        }

        public Criteria andYhpriceBetween(Double value1, Double value2) {
            addCriterion("yhprice between", value1, value2, "yhprice");
            return (Criteria) this;
        }

        public Criteria andYhpriceNotBetween(Double value1, Double value2) {
            addCriterion("yhprice not between", value1, value2, "yhprice");
            return (Criteria) this;
        }

        public Criteria andPackingIsNull() {
            addCriterion("packing is null");
            return (Criteria) this;
        }

        public Criteria andPackingIsNotNull() {
            addCriterion("packing is not null");
            return (Criteria) this;
        }

        public Criteria andPackingEqualTo(BigDecimal value) {
            addCriterion("packing =", value, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingNotEqualTo(BigDecimal value) {
            addCriterion("packing <>", value, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingGreaterThan(BigDecimal value) {
            addCriterion("packing >", value, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("packing >=", value, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingLessThan(BigDecimal value) {
            addCriterion("packing <", value, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingLessThanOrEqualTo(BigDecimal value) {
            addCriterion("packing <=", value, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingIn(List<BigDecimal> values) {
            addCriterion("packing in", values, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingNotIn(List<BigDecimal> values) {
            addCriterion("packing not in", values, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("packing between", value1, value2, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("packing not between", value1, value2, "packing");
            return (Criteria) this;
        }

        public Criteria andShowpsIsNull() {
            addCriterion("showps is null");
            return (Criteria) this;
        }

        public Criteria andShowpsIsNotNull() {
            addCriterion("showps is not null");
            return (Criteria) this;
        }

        public Criteria andShowpsEqualTo(BigDecimal value) {
            addCriterion("showps =", value, "showps");
            return (Criteria) this;
        }

        public Criteria andShowpsNotEqualTo(BigDecimal value) {
            addCriterion("showps <>", value, "showps");
            return (Criteria) this;
        }

        public Criteria andShowpsGreaterThan(BigDecimal value) {
            addCriterion("showps >", value, "showps");
            return (Criteria) this;
        }

        public Criteria andShowpsGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("showps >=", value, "showps");
            return (Criteria) this;
        }

        public Criteria andShowpsLessThan(BigDecimal value) {
            addCriterion("showps <", value, "showps");
            return (Criteria) this;
        }

        public Criteria andShowpsLessThanOrEqualTo(BigDecimal value) {
            addCriterion("showps <=", value, "showps");
            return (Criteria) this;
        }

        public Criteria andShowpsIn(List<BigDecimal> values) {
            addCriterion("showps in", values, "showps");
            return (Criteria) this;
        }

        public Criteria andShowpsNotIn(List<BigDecimal> values) {
            addCriterion("showps not in", values, "showps");
            return (Criteria) this;
        }

        public Criteria andShowpsBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("showps between", value1, value2, "showps");
            return (Criteria) this;
        }

        public Criteria andShowpsNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("showps not between", value1, value2, "showps");
            return (Criteria) this;
        }

        public Criteria andStatStrIsNull() {
            addCriterion("statStr is null");
            return (Criteria) this;
        }

        public Criteria andStatStrIsNotNull() {
            addCriterion("statStr is not null");
            return (Criteria) this;
        }

        public Criteria andStatStrEqualTo(String value) {
            addCriterion("statStr =", value, "statStr");
            return (Criteria) this;
        }

        public Criteria andStatStrNotEqualTo(String value) {
            addCriterion("statStr <>", value, "statStr");
            return (Criteria) this;
        }

        public Criteria andStatStrGreaterThan(String value) {
            addCriterion("statStr >", value, "statStr");
            return (Criteria) this;
        }

        public Criteria andStatStrGreaterThanOrEqualTo(String value) {
            addCriterion("statStr >=", value, "statStr");
            return (Criteria) this;
        }

        public Criteria andStatStrLessThan(String value) {
            addCriterion("statStr <", value, "statStr");
            return (Criteria) this;
        }

        public Criteria andStatStrLessThanOrEqualTo(String value) {
            addCriterion("statStr <=", value, "statStr");
            return (Criteria) this;
        }

        public Criteria andStatStrLike(String value) {
            addCriterion("statStr like", value, "statStr");
            return (Criteria) this;
        }

        public Criteria andStatStrNotLike(String value) {
            addCriterion("statStr not like", value, "statStr");
            return (Criteria) this;
        }

        public Criteria andStatStrIn(List<String> values) {
            addCriterion("statStr in", values, "statStr");
            return (Criteria) this;
        }

        public Criteria andStatStrNotIn(List<String> values) {
            addCriterion("statStr not in", values, "statStr");
            return (Criteria) this;
        }

        public Criteria andStatStrBetween(String value1, String value2) {
            addCriterion("statStr between", value1, value2, "statStr");
            return (Criteria) this;
        }

        public Criteria andStatStrNotBetween(String value1, String value2) {
            addCriterion("statStr not between", value1, value2, "statStr");
            return (Criteria) this;
        }

        public Criteria andEndDateIsNull() {
            addCriterion("endDate is null");
            return (Criteria) this;
        }

        public Criteria andEndDateIsNotNull() {
            addCriterion("endDate is not null");
            return (Criteria) this;
        }

        public Criteria andEndDateEqualTo(Date value) {
            addCriterion("endDate =", value, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateNotEqualTo(Date value) {
            addCriterion("endDate <>", value, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateGreaterThan(Date value) {
            addCriterion("endDate >", value, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateGreaterThanOrEqualTo(Date value) {
            addCriterion("endDate >=", value, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateLessThan(Date value) {
            addCriterion("endDate <", value, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateLessThanOrEqualTo(Date value) {
            addCriterion("endDate <=", value, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateIn(List<Date> values) {
            addCriterion("endDate in", values, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateNotIn(List<Date> values) {
            addCriterion("endDate not in", values, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateBetween(Date value1, Date value2) {
            addCriterion("endDate between", value1, value2, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateNotBetween(Date value1, Date value2) {
            addCriterion("endDate not between", value1, value2, "endDate");
            return (Criteria) this;
        }

        public Criteria andRidIsNull() {
            addCriterion("rid is null");
            return (Criteria) this;
        }

        public Criteria andRidIsNotNull() {
            addCriterion("rid is not null");
            return (Criteria) this;
        }

        public Criteria andRidEqualTo(Integer value) {
            addCriterion("rid =", value, "rid");
            return (Criteria) this;
        }

        public Criteria andRidNotEqualTo(Integer value) {
            addCriterion("rid <>", value, "rid");
            return (Criteria) this;
        }

        public Criteria andRidGreaterThan(Integer value) {
            addCriterion("rid >", value, "rid");
            return (Criteria) this;
        }

        public Criteria andRidGreaterThanOrEqualTo(Integer value) {
            addCriterion("rid >=", value, "rid");
            return (Criteria) this;
        }

        public Criteria andRidLessThan(Integer value) {
            addCriterion("rid <", value, "rid");
            return (Criteria) this;
        }

        public Criteria andRidLessThanOrEqualTo(Integer value) {
            addCriterion("rid <=", value, "rid");
            return (Criteria) this;
        }

        public Criteria andRidIn(List<Integer> values) {
            addCriterion("rid in", values, "rid");
            return (Criteria) this;
        }

        public Criteria andRidNotIn(List<Integer> values) {
            addCriterion("rid not in", values, "rid");
            return (Criteria) this;
        }

        public Criteria andRidBetween(Integer value1, Integer value2) {
            addCriterion("rid between", value1, value2, "rid");
            return (Criteria) this;
        }

        public Criteria andRidNotBetween(Integer value1, Integer value2) {
            addCriterion("rid not between", value1, value2, "rid");
            return (Criteria) this;
        }

        public Criteria andStartDateIsNull() {
            addCriterion("startDate is null");
            return (Criteria) this;
        }

        public Criteria andStartDateIsNotNull() {
            addCriterion("startDate is not null");
            return (Criteria) this;
        }

        public Criteria andStartDateEqualTo(Date value) {
            addCriterion("startDate =", value, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateNotEqualTo(Date value) {
            addCriterion("startDate <>", value, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateGreaterThan(Date value) {
            addCriterion("startDate >", value, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateGreaterThanOrEqualTo(Date value) {
            addCriterion("startDate >=", value, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateLessThan(Date value) {
            addCriterion("startDate <", value, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateLessThanOrEqualTo(Date value) {
            addCriterion("startDate <=", value, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateIn(List<Date> values) {
            addCriterion("startDate in", values, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateNotIn(List<Date> values) {
            addCriterion("startDate not in", values, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateBetween(Date value1, Date value2) {
            addCriterion("startDate between", value1, value2, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateNotBetween(Date value1, Date value2) {
            addCriterion("startDate not between", value1, value2, "startDate");
            return (Criteria) this;
        }

        public Criteria andTotalpayIsNull() {
            addCriterion("totalpay is null");
            return (Criteria) this;
        }

        public Criteria andTotalpayIsNotNull() {
            addCriterion("totalpay is not null");
            return (Criteria) this;
        }

        public Criteria andTotalpayEqualTo(BigDecimal value) {
            addCriterion("totalpay =", value, "totalpay");
            return (Criteria) this;
        }

        public Criteria andTotalpayNotEqualTo(BigDecimal value) {
            addCriterion("totalpay <>", value, "totalpay");
            return (Criteria) this;
        }

        public Criteria andTotalpayGreaterThan(BigDecimal value) {
            addCriterion("totalpay >", value, "totalpay");
            return (Criteria) this;
        }

        public Criteria andTotalpayGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("totalpay >=", value, "totalpay");
            return (Criteria) this;
        }

        public Criteria andTotalpayLessThan(BigDecimal value) {
            addCriterion("totalpay <", value, "totalpay");
            return (Criteria) this;
        }

        public Criteria andTotalpayLessThanOrEqualTo(BigDecimal value) {
            addCriterion("totalpay <=", value, "totalpay");
            return (Criteria) this;
        }

        public Criteria andTotalpayIn(List<BigDecimal> values) {
            addCriterion("totalpay in", values, "totalpay");
            return (Criteria) this;
        }

        public Criteria andTotalpayNotIn(List<BigDecimal> values) {
            addCriterion("totalpay not in", values, "totalpay");
            return (Criteria) this;
        }

        public Criteria andTotalpayBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("totalpay between", value1, value2, "totalpay");
            return (Criteria) this;
        }

        public Criteria andTotalpayNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("totalpay not between", value1, value2, "totalpay");
            return (Criteria) this;
        }

        public Criteria andDistanceIsNull() {
            addCriterion("distance is null");
            return (Criteria) this;
        }

        public Criteria andDistanceIsNotNull() {
            addCriterion("distance is not null");
            return (Criteria) this;
        }

        public Criteria andDistanceEqualTo(String value) {
            addCriterion("distance =", value, "distance");
            return (Criteria) this;
        }

        public Criteria andDistanceNotEqualTo(String value) {
            addCriterion("distance <>", value, "distance");
            return (Criteria) this;
        }

        public Criteria andDistanceGreaterThan(String value) {
            addCriterion("distance >", value, "distance");
            return (Criteria) this;
        }

        public Criteria andDistanceGreaterThanOrEqualTo(String value) {
            addCriterion("distance >=", value, "distance");
            return (Criteria) this;
        }

        public Criteria andDistanceLessThan(String value) {
            addCriterion("distance <", value, "distance");
            return (Criteria) this;
        }

        public Criteria andDistanceLessThanOrEqualTo(String value) {
            addCriterion("distance <=", value, "distance");
            return (Criteria) this;
        }

        public Criteria andDistanceLike(String value) {
            addCriterion("distance like", value, "distance");
            return (Criteria) this;
        }

        public Criteria andDistanceNotLike(String value) {
            addCriterion("distance not like", value, "distance");
            return (Criteria) this;
        }

        public Criteria andDistanceIn(List<String> values) {
            addCriterion("distance in", values, "distance");
            return (Criteria) this;
        }

        public Criteria andDistanceNotIn(List<String> values) {
            addCriterion("distance not in", values, "distance");
            return (Criteria) this;
        }

        public Criteria andDistanceBetween(String value1, String value2) {
            addCriterion("distance between", value1, value2, "distance");
            return (Criteria) this;
        }

        public Criteria andDistanceNotBetween(String value1, String value2) {
            addCriterion("distance not between", value1, value2, "distance");
            return (Criteria) this;
        }

        public Criteria andIswithdrawIsNull() {
            addCriterion("iswithdraw is null");
            return (Criteria) this;
        }

        public Criteria andIswithdrawIsNotNull() {
            addCriterion("iswithdraw is not null");
            return (Criteria) this;
        }

        public Criteria andIswithdrawEqualTo(Integer value) {
            addCriterion("iswithdraw =", value, "iswithdraw");
            return (Criteria) this;
        }

        public Criteria andIswithdrawNotEqualTo(Integer value) {
            addCriterion("iswithdraw <>", value, "iswithdraw");
            return (Criteria) this;
        }

        public Criteria andIswithdrawGreaterThan(Integer value) {
            addCriterion("iswithdraw >", value, "iswithdraw");
            return (Criteria) this;
        }

        public Criteria andIswithdrawGreaterThanOrEqualTo(Integer value) {
            addCriterion("iswithdraw >=", value, "iswithdraw");
            return (Criteria) this;
        }

        public Criteria andIswithdrawLessThan(Integer value) {
            addCriterion("iswithdraw <", value, "iswithdraw");
            return (Criteria) this;
        }

        public Criteria andIswithdrawLessThanOrEqualTo(Integer value) {
            addCriterion("iswithdraw <=", value, "iswithdraw");
            return (Criteria) this;
        }

        public Criteria andIswithdrawIn(List<Integer> values) {
            addCriterion("iswithdraw in", values, "iswithdraw");
            return (Criteria) this;
        }

        public Criteria andIswithdrawNotIn(List<Integer> values) {
            addCriterion("iswithdraw not in", values, "iswithdraw");
            return (Criteria) this;
        }

        public Criteria andIswithdrawBetween(Integer value1, Integer value2) {
            addCriterion("iswithdraw between", value1, value2, "iswithdraw");
            return (Criteria) this;
        }

        public Criteria andIswithdrawNotBetween(Integer value1, Integer value2) {
            addCriterion("iswithdraw not between", value1, value2, "iswithdraw");
            return (Criteria) this;
        }

        public Criteria andAddressIsNull() {
            addCriterion("address is null");
            return (Criteria) this;
        }

        public Criteria andAddressIsNotNull() {
            addCriterion("address is not null");
            return (Criteria) this;
        }

        public Criteria andAddressEqualTo(String value) {
            addCriterion("address =", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotEqualTo(String value) {
            addCriterion("address <>", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressGreaterThan(String value) {
            addCriterion("address >", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressGreaterThanOrEqualTo(String value) {
            addCriterion("address >=", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLessThan(String value) {
            addCriterion("address <", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLessThanOrEqualTo(String value) {
            addCriterion("address <=", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressLike(String value) {
            addCriterion("address like", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotLike(String value) {
            addCriterion("address not like", value, "address");
            return (Criteria) this;
        }

        public Criteria andAddressIn(List<String> values) {
            addCriterion("address in", values, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotIn(List<String> values) {
            addCriterion("address not in", values, "address");
            return (Criteria) this;
        }

        public Criteria andAddressBetween(String value1, String value2) {
            addCriterion("address between", value1, value2, "address");
            return (Criteria) this;
        }

        public Criteria andAddressNotBetween(String value1, String value2) {
            addCriterion("address not between", value1, value2, "address");
            return (Criteria) this;
        }

        public Criteria andBusinessDelIsNull() {
            addCriterion("businessDel is null");
            return (Criteria) this;
        }

        public Criteria andBusinessDelIsNotNull() {
            addCriterion("businessDel is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessDelEqualTo(Integer value) {
            addCriterion("businessDel =", value, "businessDel");
            return (Criteria) this;
        }

        public Criteria andBusinessDelNotEqualTo(Integer value) {
            addCriterion("businessDel <>", value, "businessDel");
            return (Criteria) this;
        }

        public Criteria andBusinessDelGreaterThan(Integer value) {
            addCriterion("businessDel >", value, "businessDel");
            return (Criteria) this;
        }

        public Criteria andBusinessDelGreaterThanOrEqualTo(Integer value) {
            addCriterion("businessDel >=", value, "businessDel");
            return (Criteria) this;
        }

        public Criteria andBusinessDelLessThan(Integer value) {
            addCriterion("businessDel <", value, "businessDel");
            return (Criteria) this;
        }

        public Criteria andBusinessDelLessThanOrEqualTo(Integer value) {
            addCriterion("businessDel <=", value, "businessDel");
            return (Criteria) this;
        }

        public Criteria andBusinessDelIn(List<Integer> values) {
            addCriterion("businessDel in", values, "businessDel");
            return (Criteria) this;
        }

        public Criteria andBusinessDelNotIn(List<Integer> values) {
            addCriterion("businessDel not in", values, "businessDel");
            return (Criteria) this;
        }

        public Criteria andBusinessDelBetween(Integer value1, Integer value2) {
            addCriterion("businessDel between", value1, value2, "businessDel");
            return (Criteria) this;
        }

        public Criteria andBusinessDelNotBetween(Integer value1, Integer value2) {
            addCriterion("businessDel not between", value1, value2, "businessDel");
            return (Criteria) this;
        }

        public Criteria andIsPayIsNull() {
            addCriterion("isPay is null");
            return (Criteria) this;
        }

        public Criteria andIsPayIsNotNull() {
            addCriterion("isPay is not null");
            return (Criteria) this;
        }

        public Criteria andIsPayEqualTo(Integer value) {
            addCriterion("isPay =", value, "isPay");
            return (Criteria) this;
        }

        public Criteria andIsPayNotEqualTo(Integer value) {
            addCriterion("isPay <>", value, "isPay");
            return (Criteria) this;
        }

        public Criteria andIsPayGreaterThan(Integer value) {
            addCriterion("isPay >", value, "isPay");
            return (Criteria) this;
        }

        public Criteria andIsPayGreaterThanOrEqualTo(Integer value) {
            addCriterion("isPay >=", value, "isPay");
            return (Criteria) this;
        }

        public Criteria andIsPayLessThan(Integer value) {
            addCriterion("isPay <", value, "isPay");
            return (Criteria) this;
        }

        public Criteria andIsPayLessThanOrEqualTo(Integer value) {
            addCriterion("isPay <=", value, "isPay");
            return (Criteria) this;
        }

        public Criteria andIsPayIn(List<Integer> values) {
            addCriterion("isPay in", values, "isPay");
            return (Criteria) this;
        }

        public Criteria andIsPayNotIn(List<Integer> values) {
            addCriterion("isPay not in", values, "isPay");
            return (Criteria) this;
        }

        public Criteria andIsPayBetween(Integer value1, Integer value2) {
            addCriterion("isPay between", value1, value2, "isPay");
            return (Criteria) this;
        }

        public Criteria andIsPayNotBetween(Integer value1, Integer value2) {
            addCriterion("isPay not between", value1, value2, "isPay");
            return (Criteria) this;
        }

        public Criteria andIsRefundIsNull() {
            addCriterion("isRefund is null");
            return (Criteria) this;
        }

        public Criteria andIsRefundIsNotNull() {
            addCriterion("isRefund is not null");
            return (Criteria) this;
        }

        public Criteria andIsRefundEqualTo(Integer value) {
            addCriterion("isRefund =", value, "isRefund");
            return (Criteria) this;
        }

        public Criteria andIsRefundNotEqualTo(Integer value) {
            addCriterion("isRefund <>", value, "isRefund");
            return (Criteria) this;
        }

        public Criteria andIsRefundGreaterThan(Integer value) {
            addCriterion("isRefund >", value, "isRefund");
            return (Criteria) this;
        }

        public Criteria andIsRefundGreaterThanOrEqualTo(Integer value) {
            addCriterion("isRefund >=", value, "isRefund");
            return (Criteria) this;
        }

        public Criteria andIsRefundLessThan(Integer value) {
            addCriterion("isRefund <", value, "isRefund");
            return (Criteria) this;
        }

        public Criteria andIsRefundLessThanOrEqualTo(Integer value) {
            addCriterion("isRefund <=", value, "isRefund");
            return (Criteria) this;
        }

        public Criteria andIsRefundIn(List<Integer> values) {
            addCriterion("isRefund in", values, "isRefund");
            return (Criteria) this;
        }

        public Criteria andIsRefundNotIn(List<Integer> values) {
            addCriterion("isRefund not in", values, "isRefund");
            return (Criteria) this;
        }

        public Criteria andIsRefundBetween(Integer value1, Integer value2) {
            addCriterion("isRefund between", value1, value2, "isRefund");
            return (Criteria) this;
        }

        public Criteria andIsRefundNotBetween(Integer value1, Integer value2) {
            addCriterion("isRefund not between", value1, value2, "isRefund");
            return (Criteria) this;
        }

        public Criteria andUserDelIsNull() {
            addCriterion("userDel is null");
            return (Criteria) this;
        }

        public Criteria andUserDelIsNotNull() {
            addCriterion("userDel is not null");
            return (Criteria) this;
        }

        public Criteria andUserDelEqualTo(Integer value) {
            addCriterion("userDel =", value, "userDel");
            return (Criteria) this;
        }

        public Criteria andUserDelNotEqualTo(Integer value) {
            addCriterion("userDel <>", value, "userDel");
            return (Criteria) this;
        }

        public Criteria andUserDelGreaterThan(Integer value) {
            addCriterion("userDel >", value, "userDel");
            return (Criteria) this;
        }

        public Criteria andUserDelGreaterThanOrEqualTo(Integer value) {
            addCriterion("userDel >=", value, "userDel");
            return (Criteria) this;
        }

        public Criteria andUserDelLessThan(Integer value) {
            addCriterion("userDel <", value, "userDel");
            return (Criteria) this;
        }

        public Criteria andUserDelLessThanOrEqualTo(Integer value) {
            addCriterion("userDel <=", value, "userDel");
            return (Criteria) this;
        }

        public Criteria andUserDelIn(List<Integer> values) {
            addCriterion("userDel in", values, "userDel");
            return (Criteria) this;
        }

        public Criteria andUserDelNotIn(List<Integer> values) {
            addCriterion("userDel not in", values, "userDel");
            return (Criteria) this;
        }

        public Criteria andUserDelBetween(Integer value1, Integer value2) {
            addCriterion("userDel between", value1, value2, "userDel");
            return (Criteria) this;
        }

        public Criteria andUserDelNotBetween(Integer value1, Integer value2) {
            addCriterion("userDel not between", value1, value2, "userDel");
            return (Criteria) this;
        }

        public Criteria andIsComentIsNull() {
            addCriterion("isComent is null");
            return (Criteria) this;
        }

        public Criteria andIsComentIsNotNull() {
            addCriterion("isComent is not null");
            return (Criteria) this;
        }

        public Criteria andIsComentEqualTo(Integer value) {
            addCriterion("isComent =", value, "isComent");
            return (Criteria) this;
        }

        public Criteria andIsComentNotEqualTo(Integer value) {
            addCriterion("isComent <>", value, "isComent");
            return (Criteria) this;
        }

        public Criteria andIsComentGreaterThan(Integer value) {
            addCriterion("isComent >", value, "isComent");
            return (Criteria) this;
        }

        public Criteria andIsComentGreaterThanOrEqualTo(Integer value) {
            addCriterion("isComent >=", value, "isComent");
            return (Criteria) this;
        }

        public Criteria andIsComentLessThan(Integer value) {
            addCriterion("isComent <", value, "isComent");
            return (Criteria) this;
        }

        public Criteria andIsComentLessThanOrEqualTo(Integer value) {
            addCriterion("isComent <=", value, "isComent");
            return (Criteria) this;
        }

        public Criteria andIsComentIn(List<Integer> values) {
            addCriterion("isComent in", values, "isComent");
            return (Criteria) this;
        }

        public Criteria andIsComentNotIn(List<Integer> values) {
            addCriterion("isComent not in", values, "isComent");
            return (Criteria) this;
        }

        public Criteria andIsComentBetween(Integer value1, Integer value2) {
            addCriterion("isComent between", value1, value2, "isComent");
            return (Criteria) this;
        }

        public Criteria andIsComentNotBetween(Integer value1, Integer value2) {
            addCriterion("isComent not between", value1, value2, "isComent");
            return (Criteria) this;
        }

        public Criteria andIsReceiveIsNull() {
            addCriterion("isReceive is null");
            return (Criteria) this;
        }

        public Criteria andIsReceiveIsNotNull() {
            addCriterion("isReceive is not null");
            return (Criteria) this;
        }

        public Criteria andIsReceiveEqualTo(Integer value) {
            addCriterion("isReceive =", value, "isReceive");
            return (Criteria) this;
        }

        public Criteria andIsReceiveNotEqualTo(Integer value) {
            addCriterion("isReceive <>", value, "isReceive");
            return (Criteria) this;
        }

        public Criteria andIsReceiveGreaterThan(Integer value) {
            addCriterion("isReceive >", value, "isReceive");
            return (Criteria) this;
        }

        public Criteria andIsReceiveGreaterThanOrEqualTo(Integer value) {
            addCriterion("isReceive >=", value, "isReceive");
            return (Criteria) this;
        }

        public Criteria andIsReceiveLessThan(Integer value) {
            addCriterion("isReceive <", value, "isReceive");
            return (Criteria) this;
        }

        public Criteria andIsReceiveLessThanOrEqualTo(Integer value) {
            addCriterion("isReceive <=", value, "isReceive");
            return (Criteria) this;
        }

        public Criteria andIsReceiveIn(List<Integer> values) {
            addCriterion("isReceive in", values, "isReceive");
            return (Criteria) this;
        }

        public Criteria andIsReceiveNotIn(List<Integer> values) {
            addCriterion("isReceive not in", values, "isReceive");
            return (Criteria) this;
        }

        public Criteria andIsReceiveBetween(Integer value1, Integer value2) {
            addCriterion("isReceive between", value1, value2, "isReceive");
            return (Criteria) this;
        }

        public Criteria andIsReceiveNotBetween(Integer value1, Integer value2) {
            addCriterion("isReceive not between", value1, value2, "isReceive");
            return (Criteria) this;
        }

        public Criteria andIsCancelIsNull() {
            addCriterion("isCancel is null");
            return (Criteria) this;
        }

        public Criteria andIsCancelIsNotNull() {
            addCriterion("isCancel is not null");
            return (Criteria) this;
        }

        public Criteria andIsCancelEqualTo(Integer value) {
            addCriterion("isCancel =", value, "isCancel");
            return (Criteria) this;
        }

        public Criteria andIsCancelNotEqualTo(Integer value) {
            addCriterion("isCancel <>", value, "isCancel");
            return (Criteria) this;
        }

        public Criteria andIsCancelGreaterThan(Integer value) {
            addCriterion("isCancel >", value, "isCancel");
            return (Criteria) this;
        }

        public Criteria andIsCancelGreaterThanOrEqualTo(Integer value) {
            addCriterion("isCancel >=", value, "isCancel");
            return (Criteria) this;
        }

        public Criteria andIsCancelLessThan(Integer value) {
            addCriterion("isCancel <", value, "isCancel");
            return (Criteria) this;
        }

        public Criteria andIsCancelLessThanOrEqualTo(Integer value) {
            addCriterion("isCancel <=", value, "isCancel");
            return (Criteria) this;
        }

        public Criteria andIsCancelIn(List<Integer> values) {
            addCriterion("isCancel in", values, "isCancel");
            return (Criteria) this;
        }

        public Criteria andIsCancelNotIn(List<Integer> values) {
            addCriterion("isCancel not in", values, "isCancel");
            return (Criteria) this;
        }

        public Criteria andIsCancelBetween(Integer value1, Integer value2) {
            addCriterion("isCancel between", value1, value2, "isCancel");
            return (Criteria) this;
        }

        public Criteria andIsCancelNotBetween(Integer value1, Integer value2) {
            addCriterion("isCancel not between", value1, value2, "isCancel");
            return (Criteria) this;
        }

        public Criteria andCommissonIsNull() {
            addCriterion("commisson is null");
            return (Criteria) this;
        }

        public Criteria andCommissonIsNotNull() {
            addCriterion("commisson is not null");
            return (Criteria) this;
        }

        public Criteria andCommissonEqualTo(BigDecimal value) {
            addCriterion("commisson =", value, "commisson");
            return (Criteria) this;
        }

        public Criteria andCommissonNotEqualTo(BigDecimal value) {
            addCriterion("commisson <>", value, "commisson");
            return (Criteria) this;
        }

        public Criteria andCommissonGreaterThan(BigDecimal value) {
            addCriterion("commisson >", value, "commisson");
            return (Criteria) this;
        }

        public Criteria andCommissonGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("commisson >=", value, "commisson");
            return (Criteria) this;
        }

        public Criteria andCommissonLessThan(BigDecimal value) {
            addCriterion("commisson <", value, "commisson");
            return (Criteria) this;
        }

        public Criteria andCommissonLessThanOrEqualTo(BigDecimal value) {
            addCriterion("commisson <=", value, "commisson");
            return (Criteria) this;
        }

        public Criteria andCommissonIn(List<BigDecimal> values) {
            addCriterion("commisson in", values, "commisson");
            return (Criteria) this;
        }

        public Criteria andCommissonNotIn(List<BigDecimal> values) {
            addCriterion("commisson not in", values, "commisson");
            return (Criteria) this;
        }

        public Criteria andCommissonBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("commisson between", value1, value2, "commisson");
            return (Criteria) this;
        }

        public Criteria andCommissonNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("commisson not between", value1, value2, "commisson");
            return (Criteria) this;
        }

        public Criteria andShopperMoneyIsNull() {
            addCriterion("shopperMoney is null");
            return (Criteria) this;
        }

        public Criteria andShopperMoneyIsNotNull() {
            addCriterion("shopperMoney is not null");
            return (Criteria) this;
        }

        public Criteria andShopperMoneyEqualTo(BigDecimal value) {
            addCriterion("shopperMoney =", value, "shopperMoney");
            return (Criteria) this;
        }

        public Criteria andShopperMoneyNotEqualTo(BigDecimal value) {
            addCriterion("shopperMoney <>", value, "shopperMoney");
            return (Criteria) this;
        }

        public Criteria andShopperMoneyGreaterThan(BigDecimal value) {
            addCriterion("shopperMoney >", value, "shopperMoney");
            return (Criteria) this;
        }

        public Criteria andShopperMoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("shopperMoney >=", value, "shopperMoney");
            return (Criteria) this;
        }

        public Criteria andShopperMoneyLessThan(BigDecimal value) {
            addCriterion("shopperMoney <", value, "shopperMoney");
            return (Criteria) this;
        }

        public Criteria andShopperMoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("shopperMoney <=", value, "shopperMoney");
            return (Criteria) this;
        }

        public Criteria andShopperMoneyIn(List<BigDecimal> values) {
            addCriterion("shopperMoney in", values, "shopperMoney");
            return (Criteria) this;
        }

        public Criteria andShopperMoneyNotIn(List<BigDecimal> values) {
            addCriterion("shopperMoney not in", values, "shopperMoney");
            return (Criteria) this;
        }

        public Criteria andShopperMoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("shopperMoney between", value1, value2, "shopperMoney");
            return (Criteria) this;
        }

        public Criteria andShopperMoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("shopperMoney not between", value1, value2, "shopperMoney");
            return (Criteria) this;
        }

        public Criteria andBusinessAddressLatIsNull() {
            addCriterion("businessAddressLat is null");
            return (Criteria) this;
        }

        public Criteria andBusinessAddressLatIsNotNull() {
            addCriterion("businessAddressLat is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessAddressLatEqualTo(String value) {
            addCriterion("businessAddressLat =", value, "businessAddressLat");
            return (Criteria) this;
        }

        public Criteria andBusinessAddressLatNotEqualTo(String value) {
            addCriterion("businessAddressLat <>", value, "businessAddressLat");
            return (Criteria) this;
        }

        public Criteria andBusinessAddressLatGreaterThan(String value) {
            addCriterion("businessAddressLat >", value, "businessAddressLat");
            return (Criteria) this;
        }

        public Criteria andBusinessAddressLatGreaterThanOrEqualTo(String value) {
            addCriterion("businessAddressLat >=", value, "businessAddressLat");
            return (Criteria) this;
        }

        public Criteria andBusinessAddressLatLessThan(String value) {
            addCriterion("businessAddressLat <", value, "businessAddressLat");
            return (Criteria) this;
        }

        public Criteria andBusinessAddressLatLessThanOrEqualTo(String value) {
            addCriterion("businessAddressLat <=", value, "businessAddressLat");
            return (Criteria) this;
        }

        public Criteria andBusinessAddressLatLike(String value) {
            addCriterion("businessAddressLat like", value, "businessAddressLat");
            return (Criteria) this;
        }

        public Criteria andBusinessAddressLatNotLike(String value) {
            addCriterion("businessAddressLat not like", value, "businessAddressLat");
            return (Criteria) this;
        }

        public Criteria andBusinessAddressLatIn(List<String> values) {
            addCriterion("businessAddressLat in", values, "businessAddressLat");
            return (Criteria) this;
        }

        public Criteria andBusinessAddressLatNotIn(List<String> values) {
            addCriterion("businessAddressLat not in", values, "businessAddressLat");
            return (Criteria) this;
        }

        public Criteria andBusinessAddressLatBetween(String value1, String value2) {
            addCriterion("businessAddressLat between", value1, value2, "businessAddressLat");
            return (Criteria) this;
        }

        public Criteria andBusinessAddressLatNotBetween(String value1, String value2) {
            addCriterion("businessAddressLat not between", value1, value2, "businessAddressLat");
            return (Criteria) this;
        }

        public Criteria andBusinessAddressLngIsNull() {
            addCriterion("businessAddressLng is null");
            return (Criteria) this;
        }

        public Criteria andBusinessAddressLngIsNotNull() {
            addCriterion("businessAddressLng is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessAddressLngEqualTo(String value) {
            addCriterion("businessAddressLng =", value, "businessAddressLng");
            return (Criteria) this;
        }

        public Criteria andBusinessAddressLngNotEqualTo(String value) {
            addCriterion("businessAddressLng <>", value, "businessAddressLng");
            return (Criteria) this;
        }

        public Criteria andBusinessAddressLngGreaterThan(String value) {
            addCriterion("businessAddressLng >", value, "businessAddressLng");
            return (Criteria) this;
        }

        public Criteria andBusinessAddressLngGreaterThanOrEqualTo(String value) {
            addCriterion("businessAddressLng >=", value, "businessAddressLng");
            return (Criteria) this;
        }

        public Criteria andBusinessAddressLngLessThan(String value) {
            addCriterion("businessAddressLng <", value, "businessAddressLng");
            return (Criteria) this;
        }

        public Criteria andBusinessAddressLngLessThanOrEqualTo(String value) {
            addCriterion("businessAddressLng <=", value, "businessAddressLng");
            return (Criteria) this;
        }

        public Criteria andBusinessAddressLngLike(String value) {
            addCriterion("businessAddressLng like", value, "businessAddressLng");
            return (Criteria) this;
        }

        public Criteria andBusinessAddressLngNotLike(String value) {
            addCriterion("businessAddressLng not like", value, "businessAddressLng");
            return (Criteria) this;
        }

        public Criteria andBusinessAddressLngIn(List<String> values) {
            addCriterion("businessAddressLng in", values, "businessAddressLng");
            return (Criteria) this;
        }

        public Criteria andBusinessAddressLngNotIn(List<String> values) {
            addCriterion("businessAddressLng not in", values, "businessAddressLng");
            return (Criteria) this;
        }

        public Criteria andBusinessAddressLngBetween(String value1, String value2) {
            addCriterion("businessAddressLng between", value1, value2, "businessAddressLng");
            return (Criteria) this;
        }

        public Criteria andBusinessAddressLngNotBetween(String value1, String value2) {
            addCriterion("businessAddressLng not between", value1, value2, "businessAddressLng");
            return (Criteria) this;
        }

        public Criteria andUserAddressLatIsNull() {
            addCriterion("userAddressLat is null");
            return (Criteria) this;
        }

        public Criteria andUserAddressLatIsNotNull() {
            addCriterion("userAddressLat is not null");
            return (Criteria) this;
        }

        public Criteria andUserAddressLatEqualTo(String value) {
            addCriterion("userAddressLat =", value, "userAddressLat");
            return (Criteria) this;
        }

        public Criteria andUserAddressLatNotEqualTo(String value) {
            addCriterion("userAddressLat <>", value, "userAddressLat");
            return (Criteria) this;
        }

        public Criteria andUserAddressLatGreaterThan(String value) {
            addCriterion("userAddressLat >", value, "userAddressLat");
            return (Criteria) this;
        }

        public Criteria andUserAddressLatGreaterThanOrEqualTo(String value) {
            addCriterion("userAddressLat >=", value, "userAddressLat");
            return (Criteria) this;
        }

        public Criteria andUserAddressLatLessThan(String value) {
            addCriterion("userAddressLat <", value, "userAddressLat");
            return (Criteria) this;
        }

        public Criteria andUserAddressLatLessThanOrEqualTo(String value) {
            addCriterion("userAddressLat <=", value, "userAddressLat");
            return (Criteria) this;
        }

        public Criteria andUserAddressLatLike(String value) {
            addCriterion("userAddressLat like", value, "userAddressLat");
            return (Criteria) this;
        }

        public Criteria andUserAddressLatNotLike(String value) {
            addCriterion("userAddressLat not like", value, "userAddressLat");
            return (Criteria) this;
        }

        public Criteria andUserAddressLatIn(List<String> values) {
            addCriterion("userAddressLat in", values, "userAddressLat");
            return (Criteria) this;
        }

        public Criteria andUserAddressLatNotIn(List<String> values) {
            addCriterion("userAddressLat not in", values, "userAddressLat");
            return (Criteria) this;
        }

        public Criteria andUserAddressLatBetween(String value1, String value2) {
            addCriterion("userAddressLat between", value1, value2, "userAddressLat");
            return (Criteria) this;
        }

        public Criteria andUserAddressLatNotBetween(String value1, String value2) {
            addCriterion("userAddressLat not between", value1, value2, "userAddressLat");
            return (Criteria) this;
        }

        public Criteria andUserAddressLngIsNull() {
            addCriterion("userAddressLng is null");
            return (Criteria) this;
        }

        public Criteria andUserAddressLngIsNotNull() {
            addCriterion("userAddressLng is not null");
            return (Criteria) this;
        }

        public Criteria andUserAddressLngEqualTo(String value) {
            addCriterion("userAddressLng =", value, "userAddressLng");
            return (Criteria) this;
        }

        public Criteria andUserAddressLngNotEqualTo(String value) {
            addCriterion("userAddressLng <>", value, "userAddressLng");
            return (Criteria) this;
        }

        public Criteria andUserAddressLngGreaterThan(String value) {
            addCriterion("userAddressLng >", value, "userAddressLng");
            return (Criteria) this;
        }

        public Criteria andUserAddressLngGreaterThanOrEqualTo(String value) {
            addCriterion("userAddressLng >=", value, "userAddressLng");
            return (Criteria) this;
        }

        public Criteria andUserAddressLngLessThan(String value) {
            addCriterion("userAddressLng <", value, "userAddressLng");
            return (Criteria) this;
        }

        public Criteria andUserAddressLngLessThanOrEqualTo(String value) {
            addCriterion("userAddressLng <=", value, "userAddressLng");
            return (Criteria) this;
        }

        public Criteria andUserAddressLngLike(String value) {
            addCriterion("userAddressLng like", value, "userAddressLng");
            return (Criteria) this;
        }

        public Criteria andUserAddressLngNotLike(String value) {
            addCriterion("userAddressLng not like", value, "userAddressLng");
            return (Criteria) this;
        }

        public Criteria andUserAddressLngIn(List<String> values) {
            addCriterion("userAddressLng in", values, "userAddressLng");
            return (Criteria) this;
        }

        public Criteria andUserAddressLngNotIn(List<String> values) {
            addCriterion("userAddressLng not in", values, "userAddressLng");
            return (Criteria) this;
        }

        public Criteria andUserAddressLngBetween(String value1, String value2) {
            addCriterion("userAddressLng between", value1, value2, "userAddressLng");
            return (Criteria) this;
        }

        public Criteria andUserAddressLngNotBetween(String value1, String value2) {
            addCriterion("userAddressLng not between", value1, value2, "userAddressLng");
            return (Criteria) this;
        }

        public Criteria andBusinessAddrIsNull() {
            addCriterion("businessAddr is null");
            return (Criteria) this;
        }

        public Criteria andBusinessAddrIsNotNull() {
            addCriterion("businessAddr is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessAddrEqualTo(String value) {
            addCriterion("businessAddr =", value, "businessAddr");
            return (Criteria) this;
        }

        public Criteria andBusinessAddrNotEqualTo(String value) {
            addCriterion("businessAddr <>", value, "businessAddr");
            return (Criteria) this;
        }

        public Criteria andBusinessAddrGreaterThan(String value) {
            addCriterion("businessAddr >", value, "businessAddr");
            return (Criteria) this;
        }

        public Criteria andBusinessAddrGreaterThanOrEqualTo(String value) {
            addCriterion("businessAddr >=", value, "businessAddr");
            return (Criteria) this;
        }

        public Criteria andBusinessAddrLessThan(String value) {
            addCriterion("businessAddr <", value, "businessAddr");
            return (Criteria) this;
        }

        public Criteria andBusinessAddrLessThanOrEqualTo(String value) {
            addCriterion("businessAddr <=", value, "businessAddr");
            return (Criteria) this;
        }

        public Criteria andBusinessAddrLike(String value) {
            addCriterion("businessAddr like", value, "businessAddr");
            return (Criteria) this;
        }

        public Criteria andBusinessAddrNotLike(String value) {
            addCriterion("businessAddr not like", value, "businessAddr");
            return (Criteria) this;
        }

        public Criteria andBusinessAddrIn(List<String> values) {
            addCriterion("businessAddr in", values, "businessAddr");
            return (Criteria) this;
        }

        public Criteria andBusinessAddrNotIn(List<String> values) {
            addCriterion("businessAddr not in", values, "businessAddr");
            return (Criteria) this;
        }

        public Criteria andBusinessAddrBetween(String value1, String value2) {
            addCriterion("businessAddr between", value1, value2, "businessAddr");
            return (Criteria) this;
        }

        public Criteria andBusinessAddrNotBetween(String value1, String value2) {
            addCriterion("businessAddr not between", value1, value2, "businessAddr");
            return (Criteria) this;
        }

        public Criteria andBusinesspayIsNull() {
            addCriterion("businesspay is null");
            return (Criteria) this;
        }

        public Criteria andBusinesspayIsNotNull() {
            addCriterion("businesspay is not null");
            return (Criteria) this;
        }

        public Criteria andBusinesspayEqualTo(BigDecimal value) {
            addCriterion("businesspay =", value, "businesspay");
            return (Criteria) this;
        }

        public Criteria andBusinesspayNotEqualTo(BigDecimal value) {
            addCriterion("businesspay <>", value, "businesspay");
            return (Criteria) this;
        }

        public Criteria andBusinesspayGreaterThan(BigDecimal value) {
            addCriterion("businesspay >", value, "businesspay");
            return (Criteria) this;
        }

        public Criteria andBusinesspayGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("businesspay >=", value, "businesspay");
            return (Criteria) this;
        }

        public Criteria andBusinesspayLessThan(BigDecimal value) {
            addCriterion("businesspay <", value, "businesspay");
            return (Criteria) this;
        }

        public Criteria andBusinesspayLessThanOrEqualTo(BigDecimal value) {
            addCriterion("businesspay <=", value, "businesspay");
            return (Criteria) this;
        }

        public Criteria andBusinesspayIn(List<BigDecimal> values) {
            addCriterion("businesspay in", values, "businesspay");
            return (Criteria) this;
        }

        public Criteria andBusinesspayNotIn(List<BigDecimal> values) {
            addCriterion("businesspay not in", values, "businesspay");
            return (Criteria) this;
        }

        public Criteria andBusinesspayBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("businesspay between", value1, value2, "businesspay");
            return (Criteria) this;
        }

        public Criteria andBusinesspayNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("businesspay not between", value1, value2, "businesspay");
            return (Criteria) this;
        }

        public Criteria andRefundIsNull() {
            addCriterion("refund is null");
            return (Criteria) this;
        }

        public Criteria andRefundIsNotNull() {
            addCriterion("refund is not null");
            return (Criteria) this;
        }

        public Criteria andRefundEqualTo(BigDecimal value) {
            addCriterion("refund =", value, "refund");
            return (Criteria) this;
        }

        public Criteria andRefundNotEqualTo(BigDecimal value) {
            addCriterion("refund <>", value, "refund");
            return (Criteria) this;
        }

        public Criteria andRefundGreaterThan(BigDecimal value) {
            addCriterion("refund >", value, "refund");
            return (Criteria) this;
        }

        public Criteria andRefundGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("refund >=", value, "refund");
            return (Criteria) this;
        }

        public Criteria andRefundLessThan(BigDecimal value) {
            addCriterion("refund <", value, "refund");
            return (Criteria) this;
        }

        public Criteria andRefundLessThanOrEqualTo(BigDecimal value) {
            addCriterion("refund <=", value, "refund");
            return (Criteria) this;
        }

        public Criteria andRefundIn(List<BigDecimal> values) {
            addCriterion("refund in", values, "refund");
            return (Criteria) this;
        }

        public Criteria andRefundNotIn(List<BigDecimal> values) {
            addCriterion("refund not in", values, "refund");
            return (Criteria) this;
        }

        public Criteria andRefundBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("refund between", value1, value2, "refund");
            return (Criteria) this;
        }

        public Criteria andRefundNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("refund not between", value1, value2, "refund");
            return (Criteria) this;
        }

        public Criteria andIsDeliverIsNull() {
            addCriterion("isDeliver is null");
            return (Criteria) this;
        }

        public Criteria andIsDeliverIsNotNull() {
            addCriterion("isDeliver is not null");
            return (Criteria) this;
        }

        public Criteria andIsDeliverEqualTo(Integer value) {
            addCriterion("isDeliver =", value, "isDeliver");
            return (Criteria) this;
        }

        public Criteria andIsDeliverNotEqualTo(Integer value) {
            addCriterion("isDeliver <>", value, "isDeliver");
            return (Criteria) this;
        }

        public Criteria andIsDeliverGreaterThan(Integer value) {
            addCriterion("isDeliver >", value, "isDeliver");
            return (Criteria) this;
        }

        public Criteria andIsDeliverGreaterThanOrEqualTo(Integer value) {
            addCriterion("isDeliver >=", value, "isDeliver");
            return (Criteria) this;
        }

        public Criteria andIsDeliverLessThan(Integer value) {
            addCriterion("isDeliver <", value, "isDeliver");
            return (Criteria) this;
        }

        public Criteria andIsDeliverLessThanOrEqualTo(Integer value) {
            addCriterion("isDeliver <=", value, "isDeliver");
            return (Criteria) this;
        }

        public Criteria andIsDeliverIn(List<Integer> values) {
            addCriterion("isDeliver in", values, "isDeliver");
            return (Criteria) this;
        }

        public Criteria andIsDeliverNotIn(List<Integer> values) {
            addCriterion("isDeliver not in", values, "isDeliver");
            return (Criteria) this;
        }

        public Criteria andIsDeliverBetween(Integer value1, Integer value2) {
            addCriterion("isDeliver between", value1, value2, "isDeliver");
            return (Criteria) this;
        }

        public Criteria andIsDeliverNotBetween(Integer value1, Integer value2) {
            addCriterion("isDeliver not between", value1, value2, "isDeliver");
            return (Criteria) this;
        }

        public Criteria andQrcodeIsNull() {
            addCriterion("qrcode is null");
            return (Criteria) this;
        }

        public Criteria andQrcodeIsNotNull() {
            addCriterion("qrcode is not null");
            return (Criteria) this;
        }

        public Criteria andQrcodeEqualTo(String value) {
            addCriterion("qrcode =", value, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeNotEqualTo(String value) {
            addCriterion("qrcode <>", value, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeGreaterThan(String value) {
            addCriterion("qrcode >", value, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeGreaterThanOrEqualTo(String value) {
            addCriterion("qrcode >=", value, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeLessThan(String value) {
            addCriterion("qrcode <", value, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeLessThanOrEqualTo(String value) {
            addCriterion("qrcode <=", value, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeLike(String value) {
            addCriterion("qrcode like", value, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeNotLike(String value) {
            addCriterion("qrcode not like", value, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeIn(List<String> values) {
            addCriterion("qrcode in", values, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeNotIn(List<String> values) {
            addCriterion("qrcode not in", values, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeBetween(String value1, String value2) {
            addCriterion("qrcode between", value1, value2, "qrcode");
            return (Criteria) this;
        }

        public Criteria andQrcodeNotBetween(String value1, String value2) {
            addCriterion("qrcode not between", value1, value2, "qrcode");
            return (Criteria) this;
        }

        public Criteria andRefundcontextIsNull() {
            addCriterion("refundcontext is null");
            return (Criteria) this;
        }

        public Criteria andRefundcontextIsNotNull() {
            addCriterion("refundcontext is not null");
            return (Criteria) this;
        }

        public Criteria andRefundcontextEqualTo(String value) {
            addCriterion("refundcontext =", value, "refundcontext");
            return (Criteria) this;
        }

        public Criteria andRefundcontextNotEqualTo(String value) {
            addCriterion("refundcontext <>", value, "refundcontext");
            return (Criteria) this;
        }

        public Criteria andRefundcontextGreaterThan(String value) {
            addCriterion("refundcontext >", value, "refundcontext");
            return (Criteria) this;
        }

        public Criteria andRefundcontextGreaterThanOrEqualTo(String value) {
            addCriterion("refundcontext >=", value, "refundcontext");
            return (Criteria) this;
        }

        public Criteria andRefundcontextLessThan(String value) {
            addCriterion("refundcontext <", value, "refundcontext");
            return (Criteria) this;
        }

        public Criteria andRefundcontextLessThanOrEqualTo(String value) {
            addCriterion("refundcontext <=", value, "refundcontext");
            return (Criteria) this;
        }

        public Criteria andRefundcontextLike(String value) {
            addCriterion("refundcontext like", value, "refundcontext");
            return (Criteria) this;
        }

        public Criteria andRefundcontextNotLike(String value) {
            addCriterion("refundcontext not like", value, "refundcontext");
            return (Criteria) this;
        }

        public Criteria andRefundcontextIn(List<String> values) {
            addCriterion("refundcontext in", values, "refundcontext");
            return (Criteria) this;
        }

        public Criteria andRefundcontextNotIn(List<String> values) {
            addCriterion("refundcontext not in", values, "refundcontext");
            return (Criteria) this;
        }

        public Criteria andRefundcontextBetween(String value1, String value2) {
            addCriterion("refundcontext between", value1, value2, "refundcontext");
            return (Criteria) this;
        }

        public Criteria andRefundcontextNotBetween(String value1, String value2) {
            addCriterion("refundcontext not between", value1, value2, "refundcontext");
            return (Criteria) this;
        }

        public Criteria andBusinessgetIsNull() {
            addCriterion("businessget is null");
            return (Criteria) this;
        }

        public Criteria andBusinessgetIsNotNull() {
            addCriterion("businessget is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessgetEqualTo(BigDecimal value) {
            addCriterion("businessget =", value, "businessget");
            return (Criteria) this;
        }

        public Criteria andBusinessgetNotEqualTo(BigDecimal value) {
            addCriterion("businessget <>", value, "businessget");
            return (Criteria) this;
        }

        public Criteria andBusinessgetGreaterThan(BigDecimal value) {
            addCriterion("businessget >", value, "businessget");
            return (Criteria) this;
        }

        public Criteria andBusinessgetGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("businessget >=", value, "businessget");
            return (Criteria) this;
        }

        public Criteria andBusinessgetLessThan(BigDecimal value) {
            addCriterion("businessget <", value, "businessget");
            return (Criteria) this;
        }

        public Criteria andBusinessgetLessThanOrEqualTo(BigDecimal value) {
            addCriterion("businessget <=", value, "businessget");
            return (Criteria) this;
        }

        public Criteria andBusinessgetIn(List<BigDecimal> values) {
            addCriterion("businessget in", values, "businessget");
            return (Criteria) this;
        }

        public Criteria andBusinessgetNotIn(List<BigDecimal> values) {
            addCriterion("businessget not in", values, "businessget");
            return (Criteria) this;
        }

        public Criteria andBusinessgetBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("businessget between", value1, value2, "businessget");
            return (Criteria) this;
        }

        public Criteria andBusinessgetNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("businessget not between", value1, value2, "businessget");
            return (Criteria) this;
        }

        public Criteria andCityIdIsNull() {
            addCriterion("cityId is null");
            return (Criteria) this;
        }

        public Criteria andCityIdIsNotNull() {
            addCriterion("cityId is not null");
            return (Criteria) this;
        }

        public Criteria andCityIdEqualTo(String value) {
            addCriterion("cityId =", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdNotEqualTo(String value) {
            addCriterion("cityId <>", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdGreaterThan(String value) {
            addCriterion("cityId >", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdGreaterThanOrEqualTo(String value) {
            addCriterion("cityId >=", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdLessThan(String value) {
            addCriterion("cityId <", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdLessThanOrEqualTo(String value) {
            addCriterion("cityId <=", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdLike(String value) {
            addCriterion("cityId like", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdNotLike(String value) {
            addCriterion("cityId not like", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdIn(List<String> values) {
            addCriterion("cityId in", values, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdNotIn(List<String> values) {
            addCriterion("cityId not in", values, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdBetween(String value1, String value2) {
            addCriterion("cityId between", value1, value2, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdNotBetween(String value1, String value2) {
            addCriterion("cityId not between", value1, value2, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityNameIsNull() {
            addCriterion("cityName is null");
            return (Criteria) this;
        }

        public Criteria andCityNameIsNotNull() {
            addCriterion("cityName is not null");
            return (Criteria) this;
        }

        public Criteria andCityNameEqualTo(String value) {
            addCriterion("cityName =", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameNotEqualTo(String value) {
            addCriterion("cityName <>", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameGreaterThan(String value) {
            addCriterion("cityName >", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameGreaterThanOrEqualTo(String value) {
            addCriterion("cityName >=", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameLessThan(String value) {
            addCriterion("cityName <", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameLessThanOrEqualTo(String value) {
            addCriterion("cityName <=", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameLike(String value) {
            addCriterion("cityName like", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameNotLike(String value) {
            addCriterion("cityName not like", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameIn(List<String> values) {
            addCriterion("cityName in", values, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameNotIn(List<String> values) {
            addCriterion("cityName not in", values, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameBetween(String value1, String value2) {
            addCriterion("cityName between", value1, value2, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameNotBetween(String value1, String value2) {
            addCriterion("cityName not between", value1, value2, "cityName");
            return (Criteria) this;
        }

        public Criteria andCountyIdIsNull() {
            addCriterion("countyId is null");
            return (Criteria) this;
        }

        public Criteria andCountyIdIsNotNull() {
            addCriterion("countyId is not null");
            return (Criteria) this;
        }

        public Criteria andCountyIdEqualTo(String value) {
            addCriterion("countyId =", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdNotEqualTo(String value) {
            addCriterion("countyId <>", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdGreaterThan(String value) {
            addCriterion("countyId >", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdGreaterThanOrEqualTo(String value) {
            addCriterion("countyId >=", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdLessThan(String value) {
            addCriterion("countyId <", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdLessThanOrEqualTo(String value) {
            addCriterion("countyId <=", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdLike(String value) {
            addCriterion("countyId like", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdNotLike(String value) {
            addCriterion("countyId not like", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdIn(List<String> values) {
            addCriterion("countyId in", values, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdNotIn(List<String> values) {
            addCriterion("countyId not in", values, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdBetween(String value1, String value2) {
            addCriterion("countyId between", value1, value2, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdNotBetween(String value1, String value2) {
            addCriterion("countyId not between", value1, value2, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyNameIsNull() {
            addCriterion("countyName is null");
            return (Criteria) this;
        }

        public Criteria andCountyNameIsNotNull() {
            addCriterion("countyName is not null");
            return (Criteria) this;
        }

        public Criteria andCountyNameEqualTo(String value) {
            addCriterion("countyName =", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameNotEqualTo(String value) {
            addCriterion("countyName <>", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameGreaterThan(String value) {
            addCriterion("countyName >", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameGreaterThanOrEqualTo(String value) {
            addCriterion("countyName >=", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameLessThan(String value) {
            addCriterion("countyName <", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameLessThanOrEqualTo(String value) {
            addCriterion("countyName <=", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameLike(String value) {
            addCriterion("countyName like", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameNotLike(String value) {
            addCriterion("countyName not like", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameIn(List<String> values) {
            addCriterion("countyName in", values, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameNotIn(List<String> values) {
            addCriterion("countyName not in", values, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameBetween(String value1, String value2) {
            addCriterion("countyName between", value1, value2, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameNotBetween(String value1, String value2) {
            addCriterion("countyName not between", value1, value2, "countyName");
            return (Criteria) this;
        }

        public Criteria andTownIdIsNull() {
            addCriterion("townId is null");
            return (Criteria) this;
        }

        public Criteria andTownIdIsNotNull() {
            addCriterion("townId is not null");
            return (Criteria) this;
        }

        public Criteria andTownIdEqualTo(String value) {
            addCriterion("townId =", value, "townId");
            return (Criteria) this;
        }

        public Criteria andTownIdNotEqualTo(String value) {
            addCriterion("townId <>", value, "townId");
            return (Criteria) this;
        }

        public Criteria andTownIdGreaterThan(String value) {
            addCriterion("townId >", value, "townId");
            return (Criteria) this;
        }

        public Criteria andTownIdGreaterThanOrEqualTo(String value) {
            addCriterion("townId >=", value, "townId");
            return (Criteria) this;
        }

        public Criteria andTownIdLessThan(String value) {
            addCriterion("townId <", value, "townId");
            return (Criteria) this;
        }

        public Criteria andTownIdLessThanOrEqualTo(String value) {
            addCriterion("townId <=", value, "townId");
            return (Criteria) this;
        }

        public Criteria andTownIdLike(String value) {
            addCriterion("townId like", value, "townId");
            return (Criteria) this;
        }

        public Criteria andTownIdNotLike(String value) {
            addCriterion("townId not like", value, "townId");
            return (Criteria) this;
        }

        public Criteria andTownIdIn(List<String> values) {
            addCriterion("townId in", values, "townId");
            return (Criteria) this;
        }

        public Criteria andTownIdNotIn(List<String> values) {
            addCriterion("townId not in", values, "townId");
            return (Criteria) this;
        }

        public Criteria andTownIdBetween(String value1, String value2) {
            addCriterion("townId between", value1, value2, "townId");
            return (Criteria) this;
        }

        public Criteria andTownIdNotBetween(String value1, String value2) {
            addCriterion("townId not between", value1, value2, "townId");
            return (Criteria) this;
        }

        public Criteria andTownNameIsNull() {
            addCriterion("townName is null");
            return (Criteria) this;
        }

        public Criteria andTownNameIsNotNull() {
            addCriterion("townName is not null");
            return (Criteria) this;
        }

        public Criteria andTownNameEqualTo(String value) {
            addCriterion("townName =", value, "townName");
            return (Criteria) this;
        }

        public Criteria andTownNameNotEqualTo(String value) {
            addCriterion("townName <>", value, "townName");
            return (Criteria) this;
        }

        public Criteria andTownNameGreaterThan(String value) {
            addCriterion("townName >", value, "townName");
            return (Criteria) this;
        }

        public Criteria andTownNameGreaterThanOrEqualTo(String value) {
            addCriterion("townName >=", value, "townName");
            return (Criteria) this;
        }

        public Criteria andTownNameLessThan(String value) {
            addCriterion("townName <", value, "townName");
            return (Criteria) this;
        }

        public Criteria andTownNameLessThanOrEqualTo(String value) {
            addCriterion("townName <=", value, "townName");
            return (Criteria) this;
        }

        public Criteria andTownNameLike(String value) {
            addCriterion("townName like", value, "townName");
            return (Criteria) this;
        }

        public Criteria andTownNameNotLike(String value) {
            addCriterion("townName not like", value, "townName");
            return (Criteria) this;
        }

        public Criteria andTownNameIn(List<String> values) {
            addCriterion("townName in", values, "townName");
            return (Criteria) this;
        }

        public Criteria andTownNameNotIn(List<String> values) {
            addCriterion("townName not in", values, "townName");
            return (Criteria) this;
        }

        public Criteria andTownNameBetween(String value1, String value2) {
            addCriterion("townName between", value1, value2, "townName");
            return (Criteria) this;
        }

        public Criteria andTownNameNotBetween(String value1, String value2) {
            addCriterion("townName not between", value1, value2, "townName");
            return (Criteria) this;
        }

        public Criteria andAgentIdIsNull() {
            addCriterion("agentId is null");
            return (Criteria) this;
        }

        public Criteria andAgentIdIsNotNull() {
            addCriterion("agentId is not null");
            return (Criteria) this;
        }

        public Criteria andAgentIdEqualTo(Integer value) {
            addCriterion("agentId =", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotEqualTo(Integer value) {
            addCriterion("agentId <>", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdGreaterThan(Integer value) {
            addCriterion("agentId >", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("agentId >=", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdLessThan(Integer value) {
            addCriterion("agentId <", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdLessThanOrEqualTo(Integer value) {
            addCriterion("agentId <=", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdIn(List<Integer> values) {
            addCriterion("agentId in", values, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotIn(List<Integer> values) {
            addCriterion("agentId not in", values, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdBetween(Integer value1, Integer value2) {
            addCriterion("agentId between", value1, value2, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotBetween(Integer value1, Integer value2) {
            addCriterion("agentId not between", value1, value2, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentNameIsNull() {
            addCriterion("agentName is null");
            return (Criteria) this;
        }

        public Criteria andAgentNameIsNotNull() {
            addCriterion("agentName is not null");
            return (Criteria) this;
        }

        public Criteria andAgentNameEqualTo(String value) {
            addCriterion("agentName =", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotEqualTo(String value) {
            addCriterion("agentName <>", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameGreaterThan(String value) {
            addCriterion("agentName >", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameGreaterThanOrEqualTo(String value) {
            addCriterion("agentName >=", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameLessThan(String value) {
            addCriterion("agentName <", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameLessThanOrEqualTo(String value) {
            addCriterion("agentName <=", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameLike(String value) {
            addCriterion("agentName like", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotLike(String value) {
            addCriterion("agentName not like", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameIn(List<String> values) {
            addCriterion("agentName in", values, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotIn(List<String> values) {
            addCriterion("agentName not in", values, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameBetween(String value1, String value2) {
            addCriterion("agentName between", value1, value2, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotBetween(String value1, String value2) {
            addCriterion("agentName not between", value1, value2, "agentName");
            return (Criteria) this;
        }

        public Criteria andBusinessMobileIsNull() {
            addCriterion("businessMobile is null");
            return (Criteria) this;
        }

        public Criteria andBusinessMobileIsNotNull() {
            addCriterion("businessMobile is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessMobileEqualTo(String value) {
            addCriterion("businessMobile =", value, "businessMobile");
            return (Criteria) this;
        }

        public Criteria andBusinessMobileNotEqualTo(String value) {
            addCriterion("businessMobile <>", value, "businessMobile");
            return (Criteria) this;
        }

        public Criteria andBusinessMobileGreaterThan(String value) {
            addCriterion("businessMobile >", value, "businessMobile");
            return (Criteria) this;
        }

        public Criteria andBusinessMobileGreaterThanOrEqualTo(String value) {
            addCriterion("businessMobile >=", value, "businessMobile");
            return (Criteria) this;
        }

        public Criteria andBusinessMobileLessThan(String value) {
            addCriterion("businessMobile <", value, "businessMobile");
            return (Criteria) this;
        }

        public Criteria andBusinessMobileLessThanOrEqualTo(String value) {
            addCriterion("businessMobile <=", value, "businessMobile");
            return (Criteria) this;
        }

        public Criteria andBusinessMobileLike(String value) {
            addCriterion("businessMobile like", value, "businessMobile");
            return (Criteria) this;
        }

        public Criteria andBusinessMobileNotLike(String value) {
            addCriterion("businessMobile not like", value, "businessMobile");
            return (Criteria) this;
        }

        public Criteria andBusinessMobileIn(List<String> values) {
            addCriterion("businessMobile in", values, "businessMobile");
            return (Criteria) this;
        }

        public Criteria andBusinessMobileNotIn(List<String> values) {
            addCriterion("businessMobile not in", values, "businessMobile");
            return (Criteria) this;
        }

        public Criteria andBusinessMobileBetween(String value1, String value2) {
            addCriterion("businessMobile between", value1, value2, "businessMobile");
            return (Criteria) this;
        }

        public Criteria andBusinessMobileNotBetween(String value1, String value2) {
            addCriterion("businessMobile not between", value1, value2, "businessMobile");
            return (Criteria) this;
        }

        public Criteria andNoChargeIsNull() {
            addCriterion("noCharge is null");
            return (Criteria) this;
        }

        public Criteria andNoChargeIsNotNull() {
            addCriterion("noCharge is not null");
            return (Criteria) this;
        }

        public Criteria andNoChargeEqualTo(Double value) {
            addCriterion("noCharge =", value, "noCharge");
            return (Criteria) this;
        }

        public Criteria andNoChargeNotEqualTo(Double value) {
            addCriterion("noCharge <>", value, "noCharge");
            return (Criteria) this;
        }

        public Criteria andNoChargeGreaterThan(Double value) {
            addCriterion("noCharge >", value, "noCharge");
            return (Criteria) this;
        }

        public Criteria andNoChargeGreaterThanOrEqualTo(Double value) {
            addCriterion("noCharge >=", value, "noCharge");
            return (Criteria) this;
        }

        public Criteria andNoChargeLessThan(Double value) {
            addCriterion("noCharge <", value, "noCharge");
            return (Criteria) this;
        }

        public Criteria andNoChargeLessThanOrEqualTo(Double value) {
            addCriterion("noCharge <=", value, "noCharge");
            return (Criteria) this;
        }

        public Criteria andNoChargeIn(List<Double> values) {
            addCriterion("noCharge in", values, "noCharge");
            return (Criteria) this;
        }

        public Criteria andNoChargeNotIn(List<Double> values) {
            addCriterion("noCharge not in", values, "noCharge");
            return (Criteria) this;
        }

        public Criteria andNoChargeBetween(Double value1, Double value2) {
            addCriterion("noCharge between", value1, value2, "noCharge");
            return (Criteria) this;
        }

        public Criteria andNoChargeNotBetween(Double value1, Double value2) {
            addCriterion("noCharge not between", value1, value2, "noCharge");
            return (Criteria) this;
        }

        public Criteria andShopperMobileIsNull() {
            addCriterion("shopperMobile is null");
            return (Criteria) this;
        }

        public Criteria andShopperMobileIsNotNull() {
            addCriterion("shopperMobile is not null");
            return (Criteria) this;
        }

        public Criteria andShopperMobileEqualTo(String value) {
            addCriterion("shopperMobile =", value, "shopperMobile");
            return (Criteria) this;
        }

        public Criteria andShopperMobileNotEqualTo(String value) {
            addCriterion("shopperMobile <>", value, "shopperMobile");
            return (Criteria) this;
        }

        public Criteria andShopperMobileGreaterThan(String value) {
            addCriterion("shopperMobile >", value, "shopperMobile");
            return (Criteria) this;
        }

        public Criteria andShopperMobileGreaterThanOrEqualTo(String value) {
            addCriterion("shopperMobile >=", value, "shopperMobile");
            return (Criteria) this;
        }

        public Criteria andShopperMobileLessThan(String value) {
            addCriterion("shopperMobile <", value, "shopperMobile");
            return (Criteria) this;
        }

        public Criteria andShopperMobileLessThanOrEqualTo(String value) {
            addCriterion("shopperMobile <=", value, "shopperMobile");
            return (Criteria) this;
        }

        public Criteria andShopperMobileLike(String value) {
            addCriterion("shopperMobile like", value, "shopperMobile");
            return (Criteria) this;
        }

        public Criteria andShopperMobileNotLike(String value) {
            addCriterion("shopperMobile not like", value, "shopperMobile");
            return (Criteria) this;
        }

        public Criteria andShopperMobileIn(List<String> values) {
            addCriterion("shopperMobile in", values, "shopperMobile");
            return (Criteria) this;
        }

        public Criteria andShopperMobileNotIn(List<String> values) {
            addCriterion("shopperMobile not in", values, "shopperMobile");
            return (Criteria) this;
        }

        public Criteria andShopperMobileBetween(String value1, String value2) {
            addCriterion("shopperMobile between", value1, value2, "shopperMobile");
            return (Criteria) this;
        }

        public Criteria andShopperMobileNotBetween(String value1, String value2) {
            addCriterion("shopperMobile not between", value1, value2, "shopperMobile");
            return (Criteria) this;
        }

        public Criteria andShopperSignIsNull() {
            addCriterion("shopperSign is null");
            return (Criteria) this;
        }

        public Criteria andShopperSignIsNotNull() {
            addCriterion("shopperSign is not null");
            return (Criteria) this;
        }

        public Criteria andShopperSignEqualTo(Integer value) {
            addCriterion("shopperSign =", value, "shopperSign");
            return (Criteria) this;
        }

        public Criteria andShopperSignNotEqualTo(Integer value) {
            addCriterion("shopperSign <>", value, "shopperSign");
            return (Criteria) this;
        }

        public Criteria andShopperSignGreaterThan(Integer value) {
            addCriterion("shopperSign >", value, "shopperSign");
            return (Criteria) this;
        }

        public Criteria andShopperSignGreaterThanOrEqualTo(Integer value) {
            addCriterion("shopperSign >=", value, "shopperSign");
            return (Criteria) this;
        }

        public Criteria andShopperSignLessThan(Integer value) {
            addCriterion("shopperSign <", value, "shopperSign");
            return (Criteria) this;
        }

        public Criteria andShopperSignLessThanOrEqualTo(Integer value) {
            addCriterion("shopperSign <=", value, "shopperSign");
            return (Criteria) this;
        }

        public Criteria andShopperSignIn(List<Integer> values) {
            addCriterion("shopperSign in", values, "shopperSign");
            return (Criteria) this;
        }

        public Criteria andShopperSignNotIn(List<Integer> values) {
            addCriterion("shopperSign not in", values, "shopperSign");
            return (Criteria) this;
        }

        public Criteria andShopperSignBetween(Integer value1, Integer value2) {
            addCriterion("shopperSign between", value1, value2, "shopperSign");
            return (Criteria) this;
        }

        public Criteria andShopperSignNotBetween(Integer value1, Integer value2) {
            addCriterion("shopperSign not between", value1, value2, "shopperSign");
            return (Criteria) this;
        }

        public Criteria andActivityIdIsNull() {
            addCriterion("activityId is null");
            return (Criteria) this;
        }

        public Criteria andActivityIdIsNotNull() {
            addCriterion("activityId is not null");
            return (Criteria) this;
        }

        public Criteria andActivityIdEqualTo(Integer value) {
            addCriterion("activityId =", value, "activityId");
            return (Criteria) this;
        }

        public Criteria andActivityIdNotEqualTo(Integer value) {
            addCriterion("activityId <>", value, "activityId");
            return (Criteria) this;
        }

        public Criteria andActivityIdGreaterThan(Integer value) {
            addCriterion("activityId >", value, "activityId");
            return (Criteria) this;
        }

        public Criteria andActivityIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("activityId >=", value, "activityId");
            return (Criteria) this;
        }

        public Criteria andActivityIdLessThan(Integer value) {
            addCriterion("activityId <", value, "activityId");
            return (Criteria) this;
        }

        public Criteria andActivityIdLessThanOrEqualTo(Integer value) {
            addCriterion("activityId <=", value, "activityId");
            return (Criteria) this;
        }

        public Criteria andActivityIdIn(List<Integer> values) {
            addCriterion("activityId in", values, "activityId");
            return (Criteria) this;
        }

        public Criteria andActivityIdNotIn(List<Integer> values) {
            addCriterion("activityId not in", values, "activityId");
            return (Criteria) this;
        }

        public Criteria andActivityIdBetween(Integer value1, Integer value2) {
            addCriterion("activityId between", value1, value2, "activityId");
            return (Criteria) this;
        }

        public Criteria andActivityIdNotBetween(Integer value1, Integer value2) {
            addCriterion("activityId not between", value1, value2, "activityId");
            return (Criteria) this;
        }

        public Criteria andActivitynameIsNull() {
            addCriterion("activityname is null");
            return (Criteria) this;
        }

        public Criteria andActivitynameIsNotNull() {
            addCriterion("activityname is not null");
            return (Criteria) this;
        }

        public Criteria andActivitynameEqualTo(String value) {
            addCriterion("activityname =", value, "activityname");
            return (Criteria) this;
        }

        public Criteria andActivitynameNotEqualTo(String value) {
            addCriterion("activityname <>", value, "activityname");
            return (Criteria) this;
        }

        public Criteria andActivitynameGreaterThan(String value) {
            addCriterion("activityname >", value, "activityname");
            return (Criteria) this;
        }

        public Criteria andActivitynameGreaterThanOrEqualTo(String value) {
            addCriterion("activityname >=", value, "activityname");
            return (Criteria) this;
        }

        public Criteria andActivitynameLessThan(String value) {
            addCriterion("activityname <", value, "activityname");
            return (Criteria) this;
        }

        public Criteria andActivitynameLessThanOrEqualTo(String value) {
            addCriterion("activityname <=", value, "activityname");
            return (Criteria) this;
        }

        public Criteria andActivitynameLike(String value) {
            addCriterion("activityname like", value, "activityname");
            return (Criteria) this;
        }

        public Criteria andActivitynameNotLike(String value) {
            addCriterion("activityname not like", value, "activityname");
            return (Criteria) this;
        }

        public Criteria andActivitynameIn(List<String> values) {
            addCriterion("activityname in", values, "activityname");
            return (Criteria) this;
        }

        public Criteria andActivitynameNotIn(List<String> values) {
            addCriterion("activityname not in", values, "activityname");
            return (Criteria) this;
        }

        public Criteria andActivitynameBetween(String value1, String value2) {
            addCriterion("activityname between", value1, value2, "activityname");
            return (Criteria) this;
        }

        public Criteria andActivitynameNotBetween(String value1, String value2) {
            addCriterion("activityname not between", value1, value2, "activityname");
            return (Criteria) this;
        }

        public Criteria andActivitypriceIsNull() {
            addCriterion("activityprice is null");
            return (Criteria) this;
        }

        public Criteria andActivitypriceIsNotNull() {
            addCriterion("activityprice is not null");
            return (Criteria) this;
        }

        public Criteria andActivitypriceEqualTo(BigDecimal value) {
            addCriterion("activityprice =", value, "activityprice");
            return (Criteria) this;
        }

        public Criteria andActivitypriceNotEqualTo(BigDecimal value) {
            addCriterion("activityprice <>", value, "activityprice");
            return (Criteria) this;
        }

        public Criteria andActivitypriceGreaterThan(BigDecimal value) {
            addCriterion("activityprice >", value, "activityprice");
            return (Criteria) this;
        }

        public Criteria andActivitypriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("activityprice >=", value, "activityprice");
            return (Criteria) this;
        }

        public Criteria andActivitypriceLessThan(BigDecimal value) {
            addCriterion("activityprice <", value, "activityprice");
            return (Criteria) this;
        }

        public Criteria andActivitypriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("activityprice <=", value, "activityprice");
            return (Criteria) this;
        }

        public Criteria andActivitypriceIn(List<BigDecimal> values) {
            addCriterion("activityprice in", values, "activityprice");
            return (Criteria) this;
        }

        public Criteria andActivitypriceNotIn(List<BigDecimal> values) {
            addCriterion("activityprice not in", values, "activityprice");
            return (Criteria) this;
        }

        public Criteria andActivitypriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("activityprice between", value1, value2, "activityprice");
            return (Criteria) this;
        }

        public Criteria andActivitypriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("activityprice not between", value1, value2, "activityprice");
            return (Criteria) this;
        }

        public Criteria andDispriceIsNull() {
            addCriterion("disprice is null");
            return (Criteria) this;
        }

        public Criteria andDispriceIsNotNull() {
            addCriterion("disprice is not null");
            return (Criteria) this;
        }

        public Criteria andDispriceEqualTo(BigDecimal value) {
            addCriterion("disprice =", value, "disprice");
            return (Criteria) this;
        }

        public Criteria andDispriceNotEqualTo(BigDecimal value) {
            addCriterion("disprice <>", value, "disprice");
            return (Criteria) this;
        }

        public Criteria andDispriceGreaterThan(BigDecimal value) {
            addCriterion("disprice >", value, "disprice");
            return (Criteria) this;
        }

        public Criteria andDispriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("disprice >=", value, "disprice");
            return (Criteria) this;
        }

        public Criteria andDispriceLessThan(BigDecimal value) {
            addCriterion("disprice <", value, "disprice");
            return (Criteria) this;
        }

        public Criteria andDispriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("disprice <=", value, "disprice");
            return (Criteria) this;
        }

        public Criteria andDispriceIn(List<BigDecimal> values) {
            addCriterion("disprice in", values, "disprice");
            return (Criteria) this;
        }

        public Criteria andDispriceNotIn(List<BigDecimal> values) {
            addCriterion("disprice not in", values, "disprice");
            return (Criteria) this;
        }

        public Criteria andDispriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("disprice between", value1, value2, "disprice");
            return (Criteria) this;
        }

        public Criteria andDispriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("disprice not between", value1, value2, "disprice");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeIsNull() {
            addCriterion("distributionTime is null");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeIsNotNull() {
            addCriterion("distributionTime is not null");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeEqualTo(Integer value) {
            addCriterion("distributionTime =", value, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeNotEqualTo(Integer value) {
            addCriterion("distributionTime <>", value, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeGreaterThan(Integer value) {
            addCriterion("distributionTime >", value, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeGreaterThanOrEqualTo(Integer value) {
            addCriterion("distributionTime >=", value, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeLessThan(Integer value) {
            addCriterion("distributionTime <", value, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeLessThanOrEqualTo(Integer value) {
            addCriterion("distributionTime <=", value, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeIn(List<Integer> values) {
            addCriterion("distributionTime in", values, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeNotIn(List<Integer> values) {
            addCriterion("distributionTime not in", values, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeBetween(Integer value1, Integer value2) {
            addCriterion("distributionTime between", value1, value2, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andDistributionTimeNotBetween(Integer value1, Integer value2) {
            addCriterion("distributionTime not between", value1, value2, "distributionTime");
            return (Criteria) this;
        }

        public Criteria andOldShopperIsNull() {
            addCriterion("oldShopper is null");
            return (Criteria) this;
        }

        public Criteria andOldShopperIsNotNull() {
            addCriterion("oldShopper is not null");
            return (Criteria) this;
        }

        public Criteria andOldShopperEqualTo(String value) {
            addCriterion("oldShopper =", value, "oldShopper");
            return (Criteria) this;
        }

        public Criteria andOldShopperNotEqualTo(String value) {
            addCriterion("oldShopper <>", value, "oldShopper");
            return (Criteria) this;
        }

        public Criteria andOldShopperGreaterThan(String value) {
            addCriterion("oldShopper >", value, "oldShopper");
            return (Criteria) this;
        }

        public Criteria andOldShopperGreaterThanOrEqualTo(String value) {
            addCriterion("oldShopper >=", value, "oldShopper");
            return (Criteria) this;
        }

        public Criteria andOldShopperLessThan(String value) {
            addCriterion("oldShopper <", value, "oldShopper");
            return (Criteria) this;
        }

        public Criteria andOldShopperLessThanOrEqualTo(String value) {
            addCriterion("oldShopper <=", value, "oldShopper");
            return (Criteria) this;
        }

        public Criteria andOldShopperLike(String value) {
            addCriterion("oldShopper like", value, "oldShopper");
            return (Criteria) this;
        }

        public Criteria andOldShopperNotLike(String value) {
            addCriterion("oldShopper not like", value, "oldShopper");
            return (Criteria) this;
        }

        public Criteria andOldShopperIn(List<String> values) {
            addCriterion("oldShopper in", values, "oldShopper");
            return (Criteria) this;
        }

        public Criteria andOldShopperNotIn(List<String> values) {
            addCriterion("oldShopper not in", values, "oldShopper");
            return (Criteria) this;
        }

        public Criteria andOldShopperBetween(String value1, String value2) {
            addCriterion("oldShopper between", value1, value2, "oldShopper");
            return (Criteria) this;
        }

        public Criteria andOldShopperNotBetween(String value1, String value2) {
            addCriterion("oldShopper not between", value1, value2, "oldShopper");
            return (Criteria) this;
        }

        public Criteria andOldShopperIdIsNull() {
            addCriterion("oldShopperId is null");
            return (Criteria) this;
        }

        public Criteria andOldShopperIdIsNotNull() {
            addCriterion("oldShopperId is not null");
            return (Criteria) this;
        }

        public Criteria andOldShopperIdEqualTo(Integer value) {
            addCriterion("oldShopperId =", value, "oldShopperId");
            return (Criteria) this;
        }

        public Criteria andOldShopperIdNotEqualTo(Integer value) {
            addCriterion("oldShopperId <>", value, "oldShopperId");
            return (Criteria) this;
        }

        public Criteria andOldShopperIdGreaterThan(Integer value) {
            addCriterion("oldShopperId >", value, "oldShopperId");
            return (Criteria) this;
        }

        public Criteria andOldShopperIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("oldShopperId >=", value, "oldShopperId");
            return (Criteria) this;
        }

        public Criteria andOldShopperIdLessThan(Integer value) {
            addCriterion("oldShopperId <", value, "oldShopperId");
            return (Criteria) this;
        }

        public Criteria andOldShopperIdLessThanOrEqualTo(Integer value) {
            addCriterion("oldShopperId <=", value, "oldShopperId");
            return (Criteria) this;
        }

        public Criteria andOldShopperIdIn(List<Integer> values) {
            addCriterion("oldShopperId in", values, "oldShopperId");
            return (Criteria) this;
        }

        public Criteria andOldShopperIdNotIn(List<Integer> values) {
            addCriterion("oldShopperId not in", values, "oldShopperId");
            return (Criteria) this;
        }

        public Criteria andOldShopperIdBetween(Integer value1, Integer value2) {
            addCriterion("oldShopperId between", value1, value2, "oldShopperId");
            return (Criteria) this;
        }

        public Criteria andOldShopperIdNotBetween(Integer value1, Integer value2) {
            addCriterion("oldShopperId not between", value1, value2, "oldShopperId");
            return (Criteria) this;
        }

        public Criteria andOldShopperMobileIsNull() {
            addCriterion("oldShopperMobile is null");
            return (Criteria) this;
        }

        public Criteria andOldShopperMobileIsNotNull() {
            addCriterion("oldShopperMobile is not null");
            return (Criteria) this;
        }

        public Criteria andOldShopperMobileEqualTo(String value) {
            addCriterion("oldShopperMobile =", value, "oldShopperMobile");
            return (Criteria) this;
        }

        public Criteria andOldShopperMobileNotEqualTo(String value) {
            addCriterion("oldShopperMobile <>", value, "oldShopperMobile");
            return (Criteria) this;
        }

        public Criteria andOldShopperMobileGreaterThan(String value) {
            addCriterion("oldShopperMobile >", value, "oldShopperMobile");
            return (Criteria) this;
        }

        public Criteria andOldShopperMobileGreaterThanOrEqualTo(String value) {
            addCriterion("oldShopperMobile >=", value, "oldShopperMobile");
            return (Criteria) this;
        }

        public Criteria andOldShopperMobileLessThan(String value) {
            addCriterion("oldShopperMobile <", value, "oldShopperMobile");
            return (Criteria) this;
        }

        public Criteria andOldShopperMobileLessThanOrEqualTo(String value) {
            addCriterion("oldShopperMobile <=", value, "oldShopperMobile");
            return (Criteria) this;
        }

        public Criteria andOldShopperMobileLike(String value) {
            addCriterion("oldShopperMobile like", value, "oldShopperMobile");
            return (Criteria) this;
        }

        public Criteria andOldShopperMobileNotLike(String value) {
            addCriterion("oldShopperMobile not like", value, "oldShopperMobile");
            return (Criteria) this;
        }

        public Criteria andOldShopperMobileIn(List<String> values) {
            addCriterion("oldShopperMobile in", values, "oldShopperMobile");
            return (Criteria) this;
        }

        public Criteria andOldShopperMobileNotIn(List<String> values) {
            addCriterion("oldShopperMobile not in", values, "oldShopperMobile");
            return (Criteria) this;
        }

        public Criteria andOldShopperMobileBetween(String value1, String value2) {
            addCriterion("oldShopperMobile between", value1, value2, "oldShopperMobile");
            return (Criteria) this;
        }

        public Criteria andOldShopperMobileNotBetween(String value1, String value2) {
            addCriterion("oldShopperMobile not between", value1, value2, "oldShopperMobile");
            return (Criteria) this;
        }

        public Criteria andAgentgetIsNull() {
            addCriterion("agentget is null");
            return (Criteria) this;
        }

        public Criteria andAgentgetIsNotNull() {
            addCriterion("agentget is not null");
            return (Criteria) this;
        }

        public Criteria andAgentgetEqualTo(BigDecimal value) {
            addCriterion("agentget =", value, "agentget");
            return (Criteria) this;
        }

        public Criteria andAgentgetNotEqualTo(BigDecimal value) {
            addCriterion("agentget <>", value, "agentget");
            return (Criteria) this;
        }

        public Criteria andAgentgetGreaterThan(BigDecimal value) {
            addCriterion("agentget >", value, "agentget");
            return (Criteria) this;
        }

        public Criteria andAgentgetGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("agentget >=", value, "agentget");
            return (Criteria) this;
        }

        public Criteria andAgentgetLessThan(BigDecimal value) {
            addCriterion("agentget <", value, "agentget");
            return (Criteria) this;
        }

        public Criteria andAgentgetLessThanOrEqualTo(BigDecimal value) {
            addCriterion("agentget <=", value, "agentget");
            return (Criteria) this;
        }

        public Criteria andAgentgetIn(List<BigDecimal> values) {
            addCriterion("agentget in", values, "agentget");
            return (Criteria) this;
        }

        public Criteria andAgentgetNotIn(List<BigDecimal> values) {
            addCriterion("agentget not in", values, "agentget");
            return (Criteria) this;
        }

        public Criteria andAgentgetBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("agentget between", value1, value2, "agentget");
            return (Criteria) this;
        }

        public Criteria andAgentgetNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("agentget not between", value1, value2, "agentget");
            return (Criteria) this;
        }

        public Criteria andAceptTimeIsNull() {
            addCriterion("aceptTime is null");
            return (Criteria) this;
        }

        public Criteria andAceptTimeIsNotNull() {
            addCriterion("aceptTime is not null");
            return (Criteria) this;
        }

        public Criteria andAceptTimeEqualTo(Date value) {
            addCriterion("aceptTime =", value, "aceptTime");
            return (Criteria) this;
        }

        public Criteria andAceptTimeNotEqualTo(Date value) {
            addCriterion("aceptTime <>", value, "aceptTime");
            return (Criteria) this;
        }

        public Criteria andAceptTimeGreaterThan(Date value) {
            addCriterion("aceptTime >", value, "aceptTime");
            return (Criteria) this;
        }

        public Criteria andAceptTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("aceptTime >=", value, "aceptTime");
            return (Criteria) this;
        }

        public Criteria andAceptTimeLessThan(Date value) {
            addCriterion("aceptTime <", value, "aceptTime");
            return (Criteria) this;
        }

        public Criteria andAceptTimeLessThanOrEqualTo(Date value) {
            addCriterion("aceptTime <=", value, "aceptTime");
            return (Criteria) this;
        }

        public Criteria andAceptTimeIn(List<Date> values) {
            addCriterion("aceptTime in", values, "aceptTime");
            return (Criteria) this;
        }

        public Criteria andAceptTimeNotIn(List<Date> values) {
            addCriterion("aceptTime not in", values, "aceptTime");
            return (Criteria) this;
        }

        public Criteria andAceptTimeBetween(Date value1, Date value2) {
            addCriterion("aceptTime between", value1, value2, "aceptTime");
            return (Criteria) this;
        }

        public Criteria andAceptTimeNotBetween(Date value1, Date value2) {
            addCriterion("aceptTime not between", value1, value2, "aceptTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeIsNull() {
            addCriterion("payTime is null");
            return (Criteria) this;
        }

        public Criteria andPayTimeIsNotNull() {
            addCriterion("payTime is not null");
            return (Criteria) this;
        }

        public Criteria andPayTimeEqualTo(Date value) {
            addCriterion("payTime =", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeNotEqualTo(Date value) {
            addCriterion("payTime <>", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeGreaterThan(Date value) {
            addCriterion("payTime >", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("payTime >=", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeLessThan(Date value) {
            addCriterion("payTime <", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeLessThanOrEqualTo(Date value) {
            addCriterion("payTime <=", value, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeIn(List<Date> values) {
            addCriterion("payTime in", values, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeNotIn(List<Date> values) {
            addCriterion("payTime not in", values, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeBetween(Date value1, Date value2) {
            addCriterion("payTime between", value1, value2, "payTime");
            return (Criteria) this;
        }

        public Criteria andPayTimeNotBetween(Date value1, Date value2) {
            addCriterion("payTime not between", value1, value2, "payTime");
            return (Criteria) this;
        }

        public Criteria andIsmuteIsNull() {
            addCriterion("ismute is null");
            return (Criteria) this;
        }

        public Criteria andIsmuteIsNotNull() {
            addCriterion("ismute is not null");
            return (Criteria) this;
        }

        public Criteria andIsmuteEqualTo(Integer value) {
            addCriterion("ismute =", value, "ismute");
            return (Criteria) this;
        }

        public Criteria andIsmuteNotEqualTo(Integer value) {
            addCriterion("ismute <>", value, "ismute");
            return (Criteria) this;
        }

        public Criteria andIsmuteGreaterThan(Integer value) {
            addCriterion("ismute >", value, "ismute");
            return (Criteria) this;
        }

        public Criteria andIsmuteGreaterThanOrEqualTo(Integer value) {
            addCriterion("ismute >=", value, "ismute");
            return (Criteria) this;
        }

        public Criteria andIsmuteLessThan(Integer value) {
            addCriterion("ismute <", value, "ismute");
            return (Criteria) this;
        }

        public Criteria andIsmuteLessThanOrEqualTo(Integer value) {
            addCriterion("ismute <=", value, "ismute");
            return (Criteria) this;
        }

        public Criteria andIsmuteIn(List<Integer> values) {
            addCriterion("ismute in", values, "ismute");
            return (Criteria) this;
        }

        public Criteria andIsmuteNotIn(List<Integer> values) {
            addCriterion("ismute not in", values, "ismute");
            return (Criteria) this;
        }

        public Criteria andIsmuteBetween(Integer value1, Integer value2) {
            addCriterion("ismute between", value1, value2, "ismute");
            return (Criteria) this;
        }

        public Criteria andIsmuteNotBetween(Integer value1, Integer value2) {
            addCriterion("ismute not between", value1, value2, "ismute");
            return (Criteria) this;
        }

        public Criteria andIsClearingIsNull() {
            addCriterion("isClearing is null");
            return (Criteria) this;
        }

        public Criteria andIsClearingIsNotNull() {
            addCriterion("isClearing is not null");
            return (Criteria) this;
        }

        public Criteria andIsClearingEqualTo(Integer value) {
            addCriterion("isClearing =", value, "isClearing");
            return (Criteria) this;
        }

        public Criteria andIsClearingNotEqualTo(Integer value) {
            addCriterion("isClearing <>", value, "isClearing");
            return (Criteria) this;
        }

        public Criteria andIsClearingGreaterThan(Integer value) {
            addCriterion("isClearing >", value, "isClearing");
            return (Criteria) this;
        }

        public Criteria andIsClearingGreaterThanOrEqualTo(Integer value) {
            addCriterion("isClearing >=", value, "isClearing");
            return (Criteria) this;
        }

        public Criteria andIsClearingLessThan(Integer value) {
            addCriterion("isClearing <", value, "isClearing");
            return (Criteria) this;
        }

        public Criteria andIsClearingLessThanOrEqualTo(Integer value) {
            addCriterion("isClearing <=", value, "isClearing");
            return (Criteria) this;
        }

        public Criteria andIsClearingIn(List<Integer> values) {
            addCriterion("isClearing in", values, "isClearing");
            return (Criteria) this;
        }

        public Criteria andIsClearingNotIn(List<Integer> values) {
            addCriterion("isClearing not in", values, "isClearing");
            return (Criteria) this;
        }

        public Criteria andIsClearingBetween(Integer value1, Integer value2) {
            addCriterion("isClearing between", value1, value2, "isClearing");
            return (Criteria) this;
        }

        public Criteria andIsClearingNotBetween(Integer value1, Integer value2) {
            addCriterion("isClearing not between", value1, value2, "isClearing");
            return (Criteria) this;
        }

        public Criteria andRefundTimeIsNull() {
            addCriterion("refundTime is null");
            return (Criteria) this;
        }

        public Criteria andRefundTimeIsNotNull() {
            addCriterion("refundTime is not null");
            return (Criteria) this;
        }

        public Criteria andRefundTimeEqualTo(Date value) {
            addCriterion("refundTime =", value, "refundTime");
            return (Criteria) this;
        }

        public Criteria andRefundTimeNotEqualTo(Date value) {
            addCriterion("refundTime <>", value, "refundTime");
            return (Criteria) this;
        }

        public Criteria andRefundTimeGreaterThan(Date value) {
            addCriterion("refundTime >", value, "refundTime");
            return (Criteria) this;
        }

        public Criteria andRefundTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("refundTime >=", value, "refundTime");
            return (Criteria) this;
        }

        public Criteria andRefundTimeLessThan(Date value) {
            addCriterion("refundTime <", value, "refundTime");
            return (Criteria) this;
        }

        public Criteria andRefundTimeLessThanOrEqualTo(Date value) {
            addCriterion("refundTime <=", value, "refundTime");
            return (Criteria) this;
        }

        public Criteria andRefundTimeIn(List<Date> values) {
            addCriterion("refundTime in", values, "refundTime");
            return (Criteria) this;
        }

        public Criteria andRefundTimeNotIn(List<Date> values) {
            addCriterion("refundTime not in", values, "refundTime");
            return (Criteria) this;
        }

        public Criteria andRefundTimeBetween(Date value1, Date value2) {
            addCriterion("refundTime between", value1, value2, "refundTime");
            return (Criteria) this;
        }

        public Criteria andRefundTimeNotBetween(Date value1, Date value2) {
            addCriterion("refundTime not between", value1, value2, "refundTime");
            return (Criteria) this;
        }

        public Criteria andRefundTypeIsNull() {
            addCriterion("refundType is null");
            return (Criteria) this;
        }

        public Criteria andRefundTypeIsNotNull() {
            addCriterion("refundType is not null");
            return (Criteria) this;
        }

        public Criteria andRefundTypeEqualTo(Integer value) {
            addCriterion("refundType =", value, "refundType");
            return (Criteria) this;
        }

        public Criteria andRefundTypeNotEqualTo(Integer value) {
            addCriterion("refundType <>", value, "refundType");
            return (Criteria) this;
        }

        public Criteria andRefundTypeGreaterThan(Integer value) {
            addCriterion("refundType >", value, "refundType");
            return (Criteria) this;
        }

        public Criteria andRefundTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("refundType >=", value, "refundType");
            return (Criteria) this;
        }

        public Criteria andRefundTypeLessThan(Integer value) {
            addCriterion("refundType <", value, "refundType");
            return (Criteria) this;
        }

        public Criteria andRefundTypeLessThanOrEqualTo(Integer value) {
            addCriterion("refundType <=", value, "refundType");
            return (Criteria) this;
        }

        public Criteria andRefundTypeIn(List<Integer> values) {
            addCriterion("refundType in", values, "refundType");
            return (Criteria) this;
        }

        public Criteria andRefundTypeNotIn(List<Integer> values) {
            addCriterion("refundType not in", values, "refundType");
            return (Criteria) this;
        }

        public Criteria andRefundTypeBetween(Integer value1, Integer value2) {
            addCriterion("refundType between", value1, value2, "refundType");
            return (Criteria) this;
        }

        public Criteria andRefundTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("refundType not between", value1, value2, "refundType");
            return (Criteria) this;
        }

        public Criteria andAgentBusgetIsNull() {
            addCriterion("agentBusget is null");
            return (Criteria) this;
        }

        public Criteria andAgentBusgetIsNotNull() {
            addCriterion("agentBusget is not null");
            return (Criteria) this;
        }

        public Criteria andAgentBusgetEqualTo(BigDecimal value) {
            addCriterion("agentBusget =", value, "agentBusget");
            return (Criteria) this;
        }

        public Criteria andAgentBusgetNotEqualTo(BigDecimal value) {
            addCriterion("agentBusget <>", value, "agentBusget");
            return (Criteria) this;
        }

        public Criteria andAgentBusgetGreaterThan(BigDecimal value) {
            addCriterion("agentBusget >", value, "agentBusget");
            return (Criteria) this;
        }

        public Criteria andAgentBusgetGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("agentBusget >=", value, "agentBusget");
            return (Criteria) this;
        }

        public Criteria andAgentBusgetLessThan(BigDecimal value) {
            addCriterion("agentBusget <", value, "agentBusget");
            return (Criteria) this;
        }

        public Criteria andAgentBusgetLessThanOrEqualTo(BigDecimal value) {
            addCriterion("agentBusget <=", value, "agentBusget");
            return (Criteria) this;
        }

        public Criteria andAgentBusgetIn(List<BigDecimal> values) {
            addCriterion("agentBusget in", values, "agentBusget");
            return (Criteria) this;
        }

        public Criteria andAgentBusgetNotIn(List<BigDecimal> values) {
            addCriterion("agentBusget not in", values, "agentBusget");
            return (Criteria) this;
        }

        public Criteria andAgentBusgetBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("agentBusget between", value1, value2, "agentBusget");
            return (Criteria) this;
        }

        public Criteria andAgentBusgetNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("agentBusget not between", value1, value2, "agentBusget");
            return (Criteria) this;
        }

        public Criteria andAcoefficientIsNull() {
            addCriterion("acoefficient is null");
            return (Criteria) this;
        }

        public Criteria andAcoefficientIsNotNull() {
            addCriterion("acoefficient is not null");
            return (Criteria) this;
        }

        public Criteria andAcoefficientEqualTo(BigDecimal value) {
            addCriterion("acoefficient =", value, "acoefficient");
            return (Criteria) this;
        }

        public Criteria andAcoefficientNotEqualTo(BigDecimal value) {
            addCriterion("acoefficient <>", value, "acoefficient");
            return (Criteria) this;
        }

        public Criteria andAcoefficientGreaterThan(BigDecimal value) {
            addCriterion("acoefficient >", value, "acoefficient");
            return (Criteria) this;
        }

        public Criteria andAcoefficientGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("acoefficient >=", value, "acoefficient");
            return (Criteria) this;
        }

        public Criteria andAcoefficientLessThan(BigDecimal value) {
            addCriterion("acoefficient <", value, "acoefficient");
            return (Criteria) this;
        }

        public Criteria andAcoefficientLessThanOrEqualTo(BigDecimal value) {
            addCriterion("acoefficient <=", value, "acoefficient");
            return (Criteria) this;
        }

        public Criteria andAcoefficientIn(List<BigDecimal> values) {
            addCriterion("acoefficient in", values, "acoefficient");
            return (Criteria) this;
        }

        public Criteria andAcoefficientNotIn(List<BigDecimal> values) {
            addCriterion("acoefficient not in", values, "acoefficient");
            return (Criteria) this;
        }

        public Criteria andAcoefficientBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("acoefficient between", value1, value2, "acoefficient");
            return (Criteria) this;
        }

        public Criteria andAcoefficientNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("acoefficient not between", value1, value2, "acoefficient");
            return (Criteria) this;
        }

        public Criteria andAcoefficient2IsNull() {
            addCriterion("acoefficient2 is null");
            return (Criteria) this;
        }

        public Criteria andAcoefficient2IsNotNull() {
            addCriterion("acoefficient2 is not null");
            return (Criteria) this;
        }

        public Criteria andAcoefficient2EqualTo(BigDecimal value) {
            addCriterion("acoefficient2 =", value, "acoefficient2");
            return (Criteria) this;
        }

        public Criteria andAcoefficient2NotEqualTo(BigDecimal value) {
            addCriterion("acoefficient2 <>", value, "acoefficient2");
            return (Criteria) this;
        }

        public Criteria andAcoefficient2GreaterThan(BigDecimal value) {
            addCriterion("acoefficient2 >", value, "acoefficient2");
            return (Criteria) this;
        }

        public Criteria andAcoefficient2GreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("acoefficient2 >=", value, "acoefficient2");
            return (Criteria) this;
        }

        public Criteria andAcoefficient2LessThan(BigDecimal value) {
            addCriterion("acoefficient2 <", value, "acoefficient2");
            return (Criteria) this;
        }

        public Criteria andAcoefficient2LessThanOrEqualTo(BigDecimal value) {
            addCriterion("acoefficient2 <=", value, "acoefficient2");
            return (Criteria) this;
        }

        public Criteria andAcoefficient2In(List<BigDecimal> values) {
            addCriterion("acoefficient2 in", values, "acoefficient2");
            return (Criteria) this;
        }

        public Criteria andAcoefficient2NotIn(List<BigDecimal> values) {
            addCriterion("acoefficient2 not in", values, "acoefficient2");
            return (Criteria) this;
        }

        public Criteria andAcoefficient2Between(BigDecimal value1, BigDecimal value2) {
            addCriterion("acoefficient2 between", value1, value2, "acoefficient2");
            return (Criteria) this;
        }

        public Criteria andAcoefficient2NotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("acoefficient2 not between", value1, value2, "acoefficient2");
            return (Criteria) this;
        }

        public Criteria andAgentBusget2IsNull() {
            addCriterion("agentBusget2 is null");
            return (Criteria) this;
        }

        public Criteria andAgentBusget2IsNotNull() {
            addCriterion("agentBusget2 is not null");
            return (Criteria) this;
        }

        public Criteria andAgentBusget2EqualTo(BigDecimal value) {
            addCriterion("agentBusget2 =", value, "agentBusget2");
            return (Criteria) this;
        }

        public Criteria andAgentBusget2NotEqualTo(BigDecimal value) {
            addCriterion("agentBusget2 <>", value, "agentBusget2");
            return (Criteria) this;
        }

        public Criteria andAgentBusget2GreaterThan(BigDecimal value) {
            addCriterion("agentBusget2 >", value, "agentBusget2");
            return (Criteria) this;
        }

        public Criteria andAgentBusget2GreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("agentBusget2 >=", value, "agentBusget2");
            return (Criteria) this;
        }

        public Criteria andAgentBusget2LessThan(BigDecimal value) {
            addCriterion("agentBusget2 <", value, "agentBusget2");
            return (Criteria) this;
        }

        public Criteria andAgentBusget2LessThanOrEqualTo(BigDecimal value) {
            addCriterion("agentBusget2 <=", value, "agentBusget2");
            return (Criteria) this;
        }

        public Criteria andAgentBusget2In(List<BigDecimal> values) {
            addCriterion("agentBusget2 in", values, "agentBusget2");
            return (Criteria) this;
        }

        public Criteria andAgentBusget2NotIn(List<BigDecimal> values) {
            addCriterion("agentBusget2 not in", values, "agentBusget2");
            return (Criteria) this;
        }

        public Criteria andAgentBusget2Between(BigDecimal value1, BigDecimal value2) {
            addCriterion("agentBusget2 between", value1, value2, "agentBusget2");
            return (Criteria) this;
        }

        public Criteria andAgentBusget2NotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("agentBusget2 not between", value1, value2, "agentBusget2");
            return (Criteria) this;
        }

        public Criteria andCoefficientIsNull() {
            addCriterion("coefficient is null");
            return (Criteria) this;
        }

        public Criteria andCoefficientIsNotNull() {
            addCriterion("coefficient is not null");
            return (Criteria) this;
        }

        public Criteria andCoefficientEqualTo(BigDecimal value) {
            addCriterion("coefficient =", value, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientNotEqualTo(BigDecimal value) {
            addCriterion("coefficient <>", value, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientGreaterThan(BigDecimal value) {
            addCriterion("coefficient >", value, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("coefficient >=", value, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientLessThan(BigDecimal value) {
            addCriterion("coefficient <", value, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientLessThanOrEqualTo(BigDecimal value) {
            addCriterion("coefficient <=", value, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientIn(List<BigDecimal> values) {
            addCriterion("coefficient in", values, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientNotIn(List<BigDecimal> values) {
            addCriterion("coefficient not in", values, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("coefficient between", value1, value2, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("coefficient not between", value1, value2, "coefficient");
            return (Criteria) this;
        }

        public Criteria andIsfirstIsNull() {
            addCriterion("isfirst is null");
            return (Criteria) this;
        }

        public Criteria andIsfirstIsNotNull() {
            addCriterion("isfirst is not null");
            return (Criteria) this;
        }

        public Criteria andIsfirstEqualTo(Integer value) {
            addCriterion("isfirst =", value, "isfirst");
            return (Criteria) this;
        }

        public Criteria andIsfirstNotEqualTo(Integer value) {
            addCriterion("isfirst <>", value, "isfirst");
            return (Criteria) this;
        }

        public Criteria andIsfirstGreaterThan(Integer value) {
            addCriterion("isfirst >", value, "isfirst");
            return (Criteria) this;
        }

        public Criteria andIsfirstGreaterThanOrEqualTo(Integer value) {
            addCriterion("isfirst >=", value, "isfirst");
            return (Criteria) this;
        }

        public Criteria andIsfirstLessThan(Integer value) {
            addCriterion("isfirst <", value, "isfirst");
            return (Criteria) this;
        }

        public Criteria andIsfirstLessThanOrEqualTo(Integer value) {
            addCriterion("isfirst <=", value, "isfirst");
            return (Criteria) this;
        }

        public Criteria andIsfirstIn(List<Integer> values) {
            addCriterion("isfirst in", values, "isfirst");
            return (Criteria) this;
        }

        public Criteria andIsfirstNotIn(List<Integer> values) {
            addCriterion("isfirst not in", values, "isfirst");
            return (Criteria) this;
        }

        public Criteria andIsfirstBetween(Integer value1, Integer value2) {
            addCriterion("isfirst between", value1, value2, "isfirst");
            return (Criteria) this;
        }

        public Criteria andIsfirstNotBetween(Integer value1, Integer value2) {
            addCriterion("isfirst not between", value1, value2, "isfirst");
            return (Criteria) this;
        }

        public Criteria andPtypeIsNull() {
            addCriterion("ptype is null");
            return (Criteria) this;
        }

        public Criteria andPtypeIsNotNull() {
            addCriterion("ptype is not null");
            return (Criteria) this;
        }

        public Criteria andPtypeEqualTo(Integer value) {
            addCriterion("ptype =", value, "ptype");
            return (Criteria) this;
        }

        public Criteria andPtypeNotEqualTo(Integer value) {
            addCriterion("ptype <>", value, "ptype");
            return (Criteria) this;
        }

        public Criteria andPtypeGreaterThan(Integer value) {
            addCriterion("ptype >", value, "ptype");
            return (Criteria) this;
        }

        public Criteria andPtypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("ptype >=", value, "ptype");
            return (Criteria) this;
        }

        public Criteria andPtypeLessThan(Integer value) {
            addCriterion("ptype <", value, "ptype");
            return (Criteria) this;
        }

        public Criteria andPtypeLessThanOrEqualTo(Integer value) {
            addCriterion("ptype <=", value, "ptype");
            return (Criteria) this;
        }

        public Criteria andPtypeIn(List<Integer> values) {
            addCriterion("ptype in", values, "ptype");
            return (Criteria) this;
        }

        public Criteria andPtypeNotIn(List<Integer> values) {
            addCriterion("ptype not in", values, "ptype");
            return (Criteria) this;
        }

        public Criteria andPtypeBetween(Integer value1, Integer value2) {
            addCriterion("ptype between", value1, value2, "ptype");
            return (Criteria) this;
        }

        public Criteria andPtypeNotBetween(Integer value1, Integer value2) {
            addCriterion("ptype not between", value1, value2, "ptype");
            return (Criteria) this;
        }

        public Criteria andZjzdIsNull() {
            addCriterion("zjzd is null");
            return (Criteria) this;
        }

        public Criteria andZjzdIsNotNull() {
            addCriterion("zjzd is not null");
            return (Criteria) this;
        }

        public Criteria andZjzdEqualTo(BigDecimal value) {
            addCriterion("zjzd =", value, "zjzd");
            return (Criteria) this;
        }

        public Criteria andZjzdNotEqualTo(BigDecimal value) {
            addCriterion("zjzd <>", value, "zjzd");
            return (Criteria) this;
        }

        public Criteria andZjzdGreaterThan(BigDecimal value) {
            addCriterion("zjzd >", value, "zjzd");
            return (Criteria) this;
        }

        public Criteria andZjzdGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("zjzd >=", value, "zjzd");
            return (Criteria) this;
        }

        public Criteria andZjzdLessThan(BigDecimal value) {
            addCriterion("zjzd <", value, "zjzd");
            return (Criteria) this;
        }

        public Criteria andZjzdLessThanOrEqualTo(BigDecimal value) {
            addCriterion("zjzd <=", value, "zjzd");
            return (Criteria) this;
        }

        public Criteria andZjzdIn(List<BigDecimal> values) {
            addCriterion("zjzd in", values, "zjzd");
            return (Criteria) this;
        }

        public Criteria andZjzdNotIn(List<BigDecimal> values) {
            addCriterion("zjzd not in", values, "zjzd");
            return (Criteria) this;
        }

        public Criteria andZjzdBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("zjzd between", value1, value2, "zjzd");
            return (Criteria) this;
        }

        public Criteria andZjzdNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("zjzd not between", value1, value2, "zjzd");
            return (Criteria) this;
        }

        public Criteria andRange1IsNull() {
            addCriterion("range1 is null");
            return (Criteria) this;
        }

        public Criteria andRange1IsNotNull() {
            addCriterion("range1 is not null");
            return (Criteria) this;
        }

        public Criteria andRange1EqualTo(Integer value) {
            addCriterion("range1 =", value, "range1");
            return (Criteria) this;
        }

        public Criteria andRange1NotEqualTo(Integer value) {
            addCriterion("range1 <>", value, "range1");
            return (Criteria) this;
        }

        public Criteria andRange1GreaterThan(Integer value) {
            addCriterion("range1 >", value, "range1");
            return (Criteria) this;
        }

        public Criteria andRange1GreaterThanOrEqualTo(Integer value) {
            addCriterion("range1 >=", value, "range1");
            return (Criteria) this;
        }

        public Criteria andRange1LessThan(Integer value) {
            addCriterion("range1 <", value, "range1");
            return (Criteria) this;
        }

        public Criteria andRange1LessThanOrEqualTo(Integer value) {
            addCriterion("range1 <=", value, "range1");
            return (Criteria) this;
        }

        public Criteria andRange1In(List<Integer> values) {
            addCriterion("range1 in", values, "range1");
            return (Criteria) this;
        }

        public Criteria andRange1NotIn(List<Integer> values) {
            addCriterion("range1 not in", values, "range1");
            return (Criteria) this;
        }

        public Criteria andRange1Between(Integer value1, Integer value2) {
            addCriterion("range1 between", value1, value2, "range1");
            return (Criteria) this;
        }

        public Criteria andRange1NotBetween(Integer value1, Integer value2) {
            addCriterion("range1 not between", value1, value2, "range1");
            return (Criteria) this;
        }

        public Criteria andStypeIsNull() {
            addCriterion("stype is null");
            return (Criteria) this;
        }

        public Criteria andStypeIsNotNull() {
            addCriterion("stype is not null");
            return (Criteria) this;
        }

        public Criteria andStypeEqualTo(Integer value) {
            addCriterion("stype =", value, "stype");
            return (Criteria) this;
        }

        public Criteria andStypeNotEqualTo(Integer value) {
            addCriterion("stype <>", value, "stype");
            return (Criteria) this;
        }

        public Criteria andStypeGreaterThan(Integer value) {
            addCriterion("stype >", value, "stype");
            return (Criteria) this;
        }

        public Criteria andStypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("stype >=", value, "stype");
            return (Criteria) this;
        }

        public Criteria andStypeLessThan(Integer value) {
            addCriterion("stype <", value, "stype");
            return (Criteria) this;
        }

        public Criteria andStypeLessThanOrEqualTo(Integer value) {
            addCriterion("stype <=", value, "stype");
            return (Criteria) this;
        }

        public Criteria andStypeIn(List<Integer> values) {
            addCriterion("stype in", values, "stype");
            return (Criteria) this;
        }

        public Criteria andStypeNotIn(List<Integer> values) {
            addCriterion("stype not in", values, "stype");
            return (Criteria) this;
        }

        public Criteria andStypeBetween(Integer value1, Integer value2) {
            addCriterion("stype between", value1, value2, "stype");
            return (Criteria) this;
        }

        public Criteria andStypeNotBetween(Integer value1, Integer value2) {
            addCriterion("stype not between", value1, value2, "stype");
            return (Criteria) this;
        }

        public Criteria andCouponnameIsNull() {
            addCriterion("couponname is null");
            return (Criteria) this;
        }

        public Criteria andCouponnameIsNotNull() {
            addCriterion("couponname is not null");
            return (Criteria) this;
        }

        public Criteria andCouponnameEqualTo(String value) {
            addCriterion("couponname =", value, "couponname");
            return (Criteria) this;
        }

        public Criteria andCouponnameNotEqualTo(String value) {
            addCriterion("couponname <>", value, "couponname");
            return (Criteria) this;
        }

        public Criteria andCouponnameGreaterThan(String value) {
            addCriterion("couponname >", value, "couponname");
            return (Criteria) this;
        }

        public Criteria andCouponnameGreaterThanOrEqualTo(String value) {
            addCriterion("couponname >=", value, "couponname");
            return (Criteria) this;
        }

        public Criteria andCouponnameLessThan(String value) {
            addCriterion("couponname <", value, "couponname");
            return (Criteria) this;
        }

        public Criteria andCouponnameLessThanOrEqualTo(String value) {
            addCriterion("couponname <=", value, "couponname");
            return (Criteria) this;
        }

        public Criteria andCouponnameLike(String value) {
            addCriterion("couponname like", value, "couponname");
            return (Criteria) this;
        }

        public Criteria andCouponnameNotLike(String value) {
            addCriterion("couponname not like", value, "couponname");
            return (Criteria) this;
        }

        public Criteria andCouponnameIn(List<String> values) {
            addCriterion("couponname in", values, "couponname");
            return (Criteria) this;
        }

        public Criteria andCouponnameNotIn(List<String> values) {
            addCriterion("couponname not in", values, "couponname");
            return (Criteria) this;
        }

        public Criteria andCouponnameBetween(String value1, String value2) {
            addCriterion("couponname between", value1, value2, "couponname");
            return (Criteria) this;
        }

        public Criteria andCouponnameNotBetween(String value1, String value2) {
            addCriterion("couponname not between", value1, value2, "couponname");
            return (Criteria) this;
        }

        public Criteria andPayTypeIsNull() {
            addCriterion("payType is null");
            return (Criteria) this;
        }

        public Criteria andPayTypeIsNotNull() {
            addCriterion("payType is not null");
            return (Criteria) this;
        }

        public Criteria andPayTypeEqualTo(Integer value) {
            addCriterion("payType =", value, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeNotEqualTo(Integer value) {
            addCriterion("payType <>", value, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeGreaterThan(Integer value) {
            addCriterion("payType >", value, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("payType >=", value, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeLessThan(Integer value) {
            addCriterion("payType <", value, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeLessThanOrEqualTo(Integer value) {
            addCriterion("payType <=", value, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeIn(List<Integer> values) {
            addCriterion("payType in", values, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeNotIn(List<Integer> values) {
            addCriterion("payType not in", values, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeBetween(Integer value1, Integer value2) {
            addCriterion("payType between", value1, value2, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("payType not between", value1, value2, "payType");
            return (Criteria) this;
        }

        public Criteria andOrderNumberIsNull() {
            addCriterion("orderNumber is null");
            return (Criteria) this;
        }

        public Criteria andOrderNumberIsNotNull() {
            addCriterion("orderNumber is not null");
            return (Criteria) this;
        }

        public Criteria andOrderNumberEqualTo(Integer value) {
            addCriterion("orderNumber =", value, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberNotEqualTo(Integer value) {
            addCriterion("orderNumber <>", value, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberGreaterThan(Integer value) {
            addCriterion("orderNumber >", value, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberGreaterThanOrEqualTo(Integer value) {
            addCriterion("orderNumber >=", value, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberLessThan(Integer value) {
            addCriterion("orderNumber <", value, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberLessThanOrEqualTo(Integer value) {
            addCriterion("orderNumber <=", value, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberIn(List<Integer> values) {
            addCriterion("orderNumber in", values, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberNotIn(List<Integer> values) {
            addCriterion("orderNumber not in", values, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberBetween(Integer value1, Integer value2) {
            addCriterion("orderNumber between", value1, value2, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andOrderNumberNotBetween(Integer value1, Integer value2) {
            addCriterion("orderNumber not between", value1, value2, "orderNumber");
            return (Criteria) this;
        }

        public Criteria andAccptTimeIsNull() {
            addCriterion("accptTime is null");
            return (Criteria) this;
        }

        public Criteria andAccptTimeIsNotNull() {
            addCriterion("accptTime is not null");
            return (Criteria) this;
        }

        public Criteria andAccptTimeEqualTo(Date value) {
            addCriterion("accptTime =", value, "accptTime");
            return (Criteria) this;
        }

        public Criteria andAccptTimeNotEqualTo(Date value) {
            addCriterion("accptTime <>", value, "accptTime");
            return (Criteria) this;
        }

        public Criteria andAccptTimeGreaterThan(Date value) {
            addCriterion("accptTime >", value, "accptTime");
            return (Criteria) this;
        }

        public Criteria andAccptTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("accptTime >=", value, "accptTime");
            return (Criteria) this;
        }

        public Criteria andAccptTimeLessThan(Date value) {
            addCriterion("accptTime <", value, "accptTime");
            return (Criteria) this;
        }

        public Criteria andAccptTimeLessThanOrEqualTo(Date value) {
            addCriterion("accptTime <=", value, "accptTime");
            return (Criteria) this;
        }

        public Criteria andAccptTimeIn(List<Date> values) {
            addCriterion("accptTime in", values, "accptTime");
            return (Criteria) this;
        }

        public Criteria andAccptTimeNotIn(List<Date> values) {
            addCriterion("accptTime not in", values, "accptTime");
            return (Criteria) this;
        }

        public Criteria andAccptTimeBetween(Date value1, Date value2) {
            addCriterion("accptTime between", value1, value2, "accptTime");
            return (Criteria) this;
        }

        public Criteria andAccptTimeNotBetween(Date value1, Date value2) {
            addCriterion("accptTime not between", value1, value2, "accptTime");
            return (Criteria) this;
        }

        public Criteria andIsaccptIsNull() {
            addCriterion("isaccpt is null");
            return (Criteria) this;
        }

        public Criteria andIsaccptIsNotNull() {
            addCriterion("isaccpt is not null");
            return (Criteria) this;
        }

        public Criteria andIsaccptEqualTo(Integer value) {
            addCriterion("isaccpt =", value, "isaccpt");
            return (Criteria) this;
        }

        public Criteria andIsaccptNotEqualTo(Integer value) {
            addCriterion("isaccpt <>", value, "isaccpt");
            return (Criteria) this;
        }

        public Criteria andIsaccptGreaterThan(Integer value) {
            addCriterion("isaccpt >", value, "isaccpt");
            return (Criteria) this;
        }

        public Criteria andIsaccptGreaterThanOrEqualTo(Integer value) {
            addCriterion("isaccpt >=", value, "isaccpt");
            return (Criteria) this;
        }

        public Criteria andIsaccptLessThan(Integer value) {
            addCriterion("isaccpt <", value, "isaccpt");
            return (Criteria) this;
        }

        public Criteria andIsaccptLessThanOrEqualTo(Integer value) {
            addCriterion("isaccpt <=", value, "isaccpt");
            return (Criteria) this;
        }

        public Criteria andIsaccptIn(List<Integer> values) {
            addCriterion("isaccpt in", values, "isaccpt");
            return (Criteria) this;
        }

        public Criteria andIsaccptNotIn(List<Integer> values) {
            addCriterion("isaccpt not in", values, "isaccpt");
            return (Criteria) this;
        }

        public Criteria andIsaccptBetween(Integer value1, Integer value2) {
            addCriterion("isaccpt between", value1, value2, "isaccpt");
            return (Criteria) this;
        }

        public Criteria andIsaccptNotBetween(Integer value1, Integer value2) {
            addCriterion("isaccpt not between", value1, value2, "isaccpt");
            return (Criteria) this;
        }

        public Criteria andPushTypeIsNull() {
            addCriterion("pushType is null");
            return (Criteria) this;
        }

        public Criteria andPushTypeIsNotNull() {
            addCriterion("pushType is not null");
            return (Criteria) this;
        }

        public Criteria andPushTypeEqualTo(Integer value) {
            addCriterion("pushType =", value, "pushType");
            return (Criteria) this;
        }

        public Criteria andPushTypeNotEqualTo(Integer value) {
            addCriterion("pushType <>", value, "pushType");
            return (Criteria) this;
        }

        public Criteria andPushTypeGreaterThan(Integer value) {
            addCriterion("pushType >", value, "pushType");
            return (Criteria) this;
        }

        public Criteria andPushTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("pushType >=", value, "pushType");
            return (Criteria) this;
        }

        public Criteria andPushTypeLessThan(Integer value) {
            addCriterion("pushType <", value, "pushType");
            return (Criteria) this;
        }

        public Criteria andPushTypeLessThanOrEqualTo(Integer value) {
            addCriterion("pushType <=", value, "pushType");
            return (Criteria) this;
        }

        public Criteria andPushTypeIn(List<Integer> values) {
            addCriterion("pushType in", values, "pushType");
            return (Criteria) this;
        }

        public Criteria andPushTypeNotIn(List<Integer> values) {
            addCriterion("pushType not in", values, "pushType");
            return (Criteria) this;
        }

        public Criteria andPushTypeBetween(Integer value1, Integer value2) {
            addCriterion("pushType between", value1, value2, "pushType");
            return (Criteria) this;
        }

        public Criteria andPushTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("pushType not between", value1, value2, "pushType");
            return (Criteria) this;
        }

        public Criteria andUserPhoneIsNull() {
            addCriterion("userPhone is null");
            return (Criteria) this;
        }

        public Criteria andUserPhoneIsNotNull() {
            addCriterion("userPhone is not null");
            return (Criteria) this;
        }

        public Criteria andUserPhoneEqualTo(String value) {
            addCriterion("userPhone =", value, "userPhone");
            return (Criteria) this;
        }

        public Criteria andUserPhoneNotEqualTo(String value) {
            addCriterion("userPhone <>", value, "userPhone");
            return (Criteria) this;
        }

        public Criteria andUserPhoneGreaterThan(String value) {
            addCriterion("userPhone >", value, "userPhone");
            return (Criteria) this;
        }

        public Criteria andUserPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("userPhone >=", value, "userPhone");
            return (Criteria) this;
        }

        public Criteria andUserPhoneLessThan(String value) {
            addCriterion("userPhone <", value, "userPhone");
            return (Criteria) this;
        }

        public Criteria andUserPhoneLessThanOrEqualTo(String value) {
            addCriterion("userPhone <=", value, "userPhone");
            return (Criteria) this;
        }

        public Criteria andUserPhoneLike(String value) {
            addCriterion("userPhone like", value, "userPhone");
            return (Criteria) this;
        }

        public Criteria andUserPhoneNotLike(String value) {
            addCriterion("userPhone not like", value, "userPhone");
            return (Criteria) this;
        }

        public Criteria andUserPhoneIn(List<String> values) {
            addCriterion("userPhone in", values, "userPhone");
            return (Criteria) this;
        }

        public Criteria andUserPhoneNotIn(List<String> values) {
            addCriterion("userPhone not in", values, "userPhone");
            return (Criteria) this;
        }

        public Criteria andUserPhoneBetween(String value1, String value2) {
            addCriterion("userPhone between", value1, value2, "userPhone");
            return (Criteria) this;
        }

        public Criteria andUserPhoneNotBetween(String value1, String value2) {
            addCriterion("userPhone not between", value1, value2, "userPhone");
            return (Criteria) this;
        }

        public Criteria andAppOrwxIsNull() {
            addCriterion("appOrwx is null");
            return (Criteria) this;
        }

        public Criteria andAppOrwxIsNotNull() {
            addCriterion("appOrwx is not null");
            return (Criteria) this;
        }

        public Criteria andAppOrwxEqualTo(Integer value) {
            addCriterion("appOrwx =", value, "appOrwx");
            return (Criteria) this;
        }

        public Criteria andAppOrwxNotEqualTo(Integer value) {
            addCriterion("appOrwx <>", value, "appOrwx");
            return (Criteria) this;
        }

        public Criteria andAppOrwxGreaterThan(Integer value) {
            addCriterion("appOrwx >", value, "appOrwx");
            return (Criteria) this;
        }

        public Criteria andAppOrwxGreaterThanOrEqualTo(Integer value) {
            addCriterion("appOrwx >=", value, "appOrwx");
            return (Criteria) this;
        }

        public Criteria andAppOrwxLessThan(Integer value) {
            addCriterion("appOrwx <", value, "appOrwx");
            return (Criteria) this;
        }

        public Criteria andAppOrwxLessThanOrEqualTo(Integer value) {
            addCriterion("appOrwx <=", value, "appOrwx");
            return (Criteria) this;
        }

        public Criteria andAppOrwxIn(List<Integer> values) {
            addCriterion("appOrwx in", values, "appOrwx");
            return (Criteria) this;
        }

        public Criteria andAppOrwxNotIn(List<Integer> values) {
            addCriterion("appOrwx not in", values, "appOrwx");
            return (Criteria) this;
        }

        public Criteria andAppOrwxBetween(Integer value1, Integer value2) {
            addCriterion("appOrwx between", value1, value2, "appOrwx");
            return (Criteria) this;
        }

        public Criteria andAppOrwxNotBetween(Integer value1, Integer value2) {
            addCriterion("appOrwx not between", value1, value2, "appOrwx");
            return (Criteria) this;
        }

        public Criteria andReadyTimeIsNull() {
            addCriterion("readyTime is null");
            return (Criteria) this;
        }

        public Criteria andReadyTimeIsNotNull() {
            addCriterion("readyTime is not null");
            return (Criteria) this;
        }

        public Criteria andReadyTimeEqualTo(Date value) {
            addCriterion("readyTime =", value, "readyTime");
            return (Criteria) this;
        }

        public Criteria andReadyTimeNotEqualTo(Date value) {
            addCriterion("readyTime <>", value, "readyTime");
            return (Criteria) this;
        }

        public Criteria andReadyTimeGreaterThan(Date value) {
            addCriterion("readyTime >", value, "readyTime");
            return (Criteria) this;
        }

        public Criteria andReadyTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("readyTime >=", value, "readyTime");
            return (Criteria) this;
        }

        public Criteria andReadyTimeLessThan(Date value) {
            addCriterion("readyTime <", value, "readyTime");
            return (Criteria) this;
        }

        public Criteria andReadyTimeLessThanOrEqualTo(Date value) {
            addCriterion("readyTime <=", value, "readyTime");
            return (Criteria) this;
        }

        public Criteria andReadyTimeIn(List<Date> values) {
            addCriterion("readyTime in", values, "readyTime");
            return (Criteria) this;
        }

        public Criteria andReadyTimeNotIn(List<Date> values) {
            addCriterion("readyTime not in", values, "readyTime");
            return (Criteria) this;
        }

        public Criteria andReadyTimeBetween(Date value1, Date value2) {
            addCriterion("readyTime between", value1, value2, "readyTime");
            return (Criteria) this;
        }

        public Criteria andReadyTimeNotBetween(Date value1, Date value2) {
            addCriterion("readyTime not between", value1, value2, "readyTime");
            return (Criteria) this;
        }

        public Criteria andDisTimeIsNull() {
            addCriterion("disTime is null");
            return (Criteria) this;
        }

        public Criteria andDisTimeIsNotNull() {
            addCriterion("disTime is not null");
            return (Criteria) this;
        }

        public Criteria andDisTimeEqualTo(Date value) {
            addCriterion("disTime =", value, "disTime");
            return (Criteria) this;
        }

        public Criteria andDisTimeNotEqualTo(Date value) {
            addCriterion("disTime <>", value, "disTime");
            return (Criteria) this;
        }

        public Criteria andDisTimeGreaterThan(Date value) {
            addCriterion("disTime >", value, "disTime");
            return (Criteria) this;
        }

        public Criteria andDisTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("disTime >=", value, "disTime");
            return (Criteria) this;
        }

        public Criteria andDisTimeLessThan(Date value) {
            addCriterion("disTime <", value, "disTime");
            return (Criteria) this;
        }

        public Criteria andDisTimeLessThanOrEqualTo(Date value) {
            addCriterion("disTime <=", value, "disTime");
            return (Criteria) this;
        }

        public Criteria andDisTimeIn(List<Date> values) {
            addCriterion("disTime in", values, "disTime");
            return (Criteria) this;
        }

        public Criteria andDisTimeNotIn(List<Date> values) {
            addCriterion("disTime not in", values, "disTime");
            return (Criteria) this;
        }

        public Criteria andDisTimeBetween(Date value1, Date value2) {
            addCriterion("disTime between", value1, value2, "disTime");
            return (Criteria) this;
        }

        public Criteria andDisTimeNotBetween(Date value1, Date value2) {
            addCriterion("disTime not between", value1, value2, "disTime");
            return (Criteria) this;
        }

        public Criteria andErrendIsNull() {
            addCriterion("errend is null");
            return (Criteria) this;
        }

        public Criteria andErrendIsNotNull() {
            addCriterion("errend is not null");
            return (Criteria) this;
        }

        public Criteria andErrendEqualTo(Integer value) {
            addCriterion("errend =", value, "errend");
            return (Criteria) this;
        }

        public Criteria andErrendNotEqualTo(Integer value) {
            addCriterion("errend <>", value, "errend");
            return (Criteria) this;
        }

        public Criteria andErrendGreaterThan(Integer value) {
            addCriterion("errend >", value, "errend");
            return (Criteria) this;
        }

        public Criteria andErrendGreaterThanOrEqualTo(Integer value) {
            addCriterion("errend >=", value, "errend");
            return (Criteria) this;
        }

        public Criteria andErrendLessThan(Integer value) {
            addCriterion("errend <", value, "errend");
            return (Criteria) this;
        }

        public Criteria andErrendLessThanOrEqualTo(Integer value) {
            addCriterion("errend <=", value, "errend");
            return (Criteria) this;
        }

        public Criteria andErrendIn(List<Integer> values) {
            addCriterion("errend in", values, "errend");
            return (Criteria) this;
        }

        public Criteria andErrendNotIn(List<Integer> values) {
            addCriterion("errend not in", values, "errend");
            return (Criteria) this;
        }

        public Criteria andErrendBetween(Integer value1, Integer value2) {
            addCriterion("errend between", value1, value2, "errend");
            return (Criteria) this;
        }

        public Criteria andErrendNotBetween(Integer value1, Integer value2) {
            addCriterion("errend not between", value1, value2, "errend");
            return (Criteria) this;
        }

        public Criteria andTeamidIsNull() {
            addCriterion("teamid is null");
            return (Criteria) this;
        }

        public Criteria andTeamidIsNotNull() {
            addCriterion("teamid is not null");
            return (Criteria) this;
        }

        public Criteria andTeamidEqualTo(Integer value) {
            addCriterion("teamid =", value, "teamid");
            return (Criteria) this;
        }

        public Criteria andTeamidNotEqualTo(Integer value) {
            addCriterion("teamid <>", value, "teamid");
            return (Criteria) this;
        }

        public Criteria andTeamidGreaterThan(Integer value) {
            addCriterion("teamid >", value, "teamid");
            return (Criteria) this;
        }

        public Criteria andTeamidGreaterThanOrEqualTo(Integer value) {
            addCriterion("teamid >=", value, "teamid");
            return (Criteria) this;
        }

        public Criteria andTeamidLessThan(Integer value) {
            addCriterion("teamid <", value, "teamid");
            return (Criteria) this;
        }

        public Criteria andTeamidLessThanOrEqualTo(Integer value) {
            addCriterion("teamid <=", value, "teamid");
            return (Criteria) this;
        }

        public Criteria andTeamidIn(List<Integer> values) {
            addCriterion("teamid in", values, "teamid");
            return (Criteria) this;
        }

        public Criteria andTeamidNotIn(List<Integer> values) {
            addCriterion("teamid not in", values, "teamid");
            return (Criteria) this;
        }

        public Criteria andTeamidBetween(Integer value1, Integer value2) {
            addCriterion("teamid between", value1, value2, "teamid");
            return (Criteria) this;
        }

        public Criteria andTeamidNotBetween(Integer value1, Integer value2) {
            addCriterion("teamid not between", value1, value2, "teamid");
            return (Criteria) this;
        }

        public Criteria andTeamnameIsNull() {
            addCriterion("teamname is null");
            return (Criteria) this;
        }

        public Criteria andTeamnameIsNotNull() {
            addCriterion("teamname is not null");
            return (Criteria) this;
        }

        public Criteria andTeamnameEqualTo(String value) {
            addCriterion("teamname =", value, "teamname");
            return (Criteria) this;
        }

        public Criteria andTeamnameNotEqualTo(String value) {
            addCriterion("teamname <>", value, "teamname");
            return (Criteria) this;
        }

        public Criteria andTeamnameGreaterThan(String value) {
            addCriterion("teamname >", value, "teamname");
            return (Criteria) this;
        }

        public Criteria andTeamnameGreaterThanOrEqualTo(String value) {
            addCriterion("teamname >=", value, "teamname");
            return (Criteria) this;
        }

        public Criteria andTeamnameLessThan(String value) {
            addCriterion("teamname <", value, "teamname");
            return (Criteria) this;
        }

        public Criteria andTeamnameLessThanOrEqualTo(String value) {
            addCriterion("teamname <=", value, "teamname");
            return (Criteria) this;
        }

        public Criteria andTeamnameLike(String value) {
            addCriterion("teamname like", value, "teamname");
            return (Criteria) this;
        }

        public Criteria andTeamnameNotLike(String value) {
            addCriterion("teamname not like", value, "teamname");
            return (Criteria) this;
        }

        public Criteria andTeamnameIn(List<String> values) {
            addCriterion("teamname in", values, "teamname");
            return (Criteria) this;
        }

        public Criteria andTeamnameNotIn(List<String> values) {
            addCriterion("teamname not in", values, "teamname");
            return (Criteria) this;
        }

        public Criteria andTeamnameBetween(String value1, String value2) {
            addCriterion("teamname between", value1, value2, "teamname");
            return (Criteria) this;
        }

        public Criteria andTeamnameNotBetween(String value1, String value2) {
            addCriterion("teamname not between", value1, value2, "teamname");
            return (Criteria) this;
        }

        public Criteria andIstimerefundIsNull() {
            addCriterion("istimerefund is null");
            return (Criteria) this;
        }

        public Criteria andIstimerefundIsNotNull() {
            addCriterion("istimerefund is not null");
            return (Criteria) this;
        }

        public Criteria andIstimerefundEqualTo(Integer value) {
            addCriterion("istimerefund =", value, "istimerefund");
            return (Criteria) this;
        }

        public Criteria andIstimerefundNotEqualTo(Integer value) {
            addCriterion("istimerefund <>", value, "istimerefund");
            return (Criteria) this;
        }

        public Criteria andIstimerefundGreaterThan(Integer value) {
            addCriterion("istimerefund >", value, "istimerefund");
            return (Criteria) this;
        }

        public Criteria andIstimerefundGreaterThanOrEqualTo(Integer value) {
            addCriterion("istimerefund >=", value, "istimerefund");
            return (Criteria) this;
        }

        public Criteria andIstimerefundLessThan(Integer value) {
            addCriterion("istimerefund <", value, "istimerefund");
            return (Criteria) this;
        }

        public Criteria andIstimerefundLessThanOrEqualTo(Integer value) {
            addCriterion("istimerefund <=", value, "istimerefund");
            return (Criteria) this;
        }

        public Criteria andIstimerefundIn(List<Integer> values) {
            addCriterion("istimerefund in", values, "istimerefund");
            return (Criteria) this;
        }

        public Criteria andIstimerefundNotIn(List<Integer> values) {
            addCriterion("istimerefund not in", values, "istimerefund");
            return (Criteria) this;
        }

        public Criteria andIstimerefundBetween(Integer value1, Integer value2) {
            addCriterion("istimerefund between", value1, value2, "istimerefund");
            return (Criteria) this;
        }

        public Criteria andIstimerefundNotBetween(Integer value1, Integer value2) {
            addCriterion("istimerefund not between", value1, value2, "istimerefund");
            return (Criteria) this;
        }

        public Criteria andIssubsidyIsNull() {
            addCriterion("issubsidy is null");
            return (Criteria) this;
        }

        public Criteria andIssubsidyIsNotNull() {
            addCriterion("issubsidy is not null");
            return (Criteria) this;
        }

        public Criteria andIssubsidyEqualTo(Integer value) {
            addCriterion("issubsidy =", value, "issubsidy");
            return (Criteria) this;
        }

        public Criteria andIssubsidyNotEqualTo(Integer value) {
            addCriterion("issubsidy <>", value, "issubsidy");
            return (Criteria) this;
        }

        public Criteria andIssubsidyGreaterThan(Integer value) {
            addCriterion("issubsidy >", value, "issubsidy");
            return (Criteria) this;
        }

        public Criteria andIssubsidyGreaterThanOrEqualTo(Integer value) {
            addCriterion("issubsidy >=", value, "issubsidy");
            return (Criteria) this;
        }

        public Criteria andIssubsidyLessThan(Integer value) {
            addCriterion("issubsidy <", value, "issubsidy");
            return (Criteria) this;
        }

        public Criteria andIssubsidyLessThanOrEqualTo(Integer value) {
            addCriterion("issubsidy <=", value, "issubsidy");
            return (Criteria) this;
        }

        public Criteria andIssubsidyIn(List<Integer> values) {
            addCriterion("issubsidy in", values, "issubsidy");
            return (Criteria) this;
        }

        public Criteria andIssubsidyNotIn(List<Integer> values) {
            addCriterion("issubsidy not in", values, "issubsidy");
            return (Criteria) this;
        }

        public Criteria andIssubsidyBetween(Integer value1, Integer value2) {
            addCriterion("issubsidy between", value1, value2, "issubsidy");
            return (Criteria) this;
        }

        public Criteria andIssubsidyNotBetween(Integer value1, Integer value2) {
            addCriterion("issubsidy not between", value1, value2, "issubsidy");
            return (Criteria) this;
        }

        public Criteria andLesspsIsNull() {
            addCriterion("lessps is null");
            return (Criteria) this;
        }

        public Criteria andLesspsIsNotNull() {
            addCriterion("lessps is not null");
            return (Criteria) this;
        }

        public Criteria andLesspsEqualTo(BigDecimal value) {
            addCriterion("lessps =", value, "lessps");
            return (Criteria) this;
        }

        public Criteria andLesspsNotEqualTo(BigDecimal value) {
            addCriterion("lessps <>", value, "lessps");
            return (Criteria) this;
        }

        public Criteria andLesspsGreaterThan(BigDecimal value) {
            addCriterion("lessps >", value, "lessps");
            return (Criteria) this;
        }

        public Criteria andLesspsGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("lessps >=", value, "lessps");
            return (Criteria) this;
        }

        public Criteria andLesspsLessThan(BigDecimal value) {
            addCriterion("lessps <", value, "lessps");
            return (Criteria) this;
        }

        public Criteria andLesspsLessThanOrEqualTo(BigDecimal value) {
            addCriterion("lessps <=", value, "lessps");
            return (Criteria) this;
        }

        public Criteria andLesspsIn(List<BigDecimal> values) {
            addCriterion("lessps in", values, "lessps");
            return (Criteria) this;
        }

        public Criteria andLesspsNotIn(List<BigDecimal> values) {
            addCriterion("lessps not in", values, "lessps");
            return (Criteria) this;
        }

        public Criteria andLesspsBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("lessps between", value1, value2, "lessps");
            return (Criteria) this;
        }

        public Criteria andLesspsNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("lessps not between", value1, value2, "lessps");
            return (Criteria) this;
        }

        public Criteria andSubsidyIsNull() {
            addCriterion("subsidy is null");
            return (Criteria) this;
        }

        public Criteria andSubsidyIsNotNull() {
            addCriterion("subsidy is not null");
            return (Criteria) this;
        }

        public Criteria andSubsidyEqualTo(BigDecimal value) {
            addCriterion("subsidy =", value, "subsidy");
            return (Criteria) this;
        }

        public Criteria andSubsidyNotEqualTo(BigDecimal value) {
            addCriterion("subsidy <>", value, "subsidy");
            return (Criteria) this;
        }

        public Criteria andSubsidyGreaterThan(BigDecimal value) {
            addCriterion("subsidy >", value, "subsidy");
            return (Criteria) this;
        }

        public Criteria andSubsidyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("subsidy >=", value, "subsidy");
            return (Criteria) this;
        }

        public Criteria andSubsidyLessThan(BigDecimal value) {
            addCriterion("subsidy <", value, "subsidy");
            return (Criteria) this;
        }

        public Criteria andSubsidyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("subsidy <=", value, "subsidy");
            return (Criteria) this;
        }

        public Criteria andSubsidyIn(List<BigDecimal> values) {
            addCriterion("subsidy in", values, "subsidy");
            return (Criteria) this;
        }

        public Criteria andSubsidyNotIn(List<BigDecimal> values) {
            addCriterion("subsidy not in", values, "subsidy");
            return (Criteria) this;
        }

        public Criteria andSubsidyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("subsidy between", value1, value2, "subsidy");
            return (Criteria) this;
        }

        public Criteria andSubsidyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("subsidy not between", value1, value2, "subsidy");
            return (Criteria) this;
        }

        public Criteria andAgreeIsNull() {
            addCriterion("agree is null");
            return (Criteria) this;
        }

        public Criteria andAgreeIsNotNull() {
            addCriterion("agree is not null");
            return (Criteria) this;
        }

        public Criteria andAgreeEqualTo(Integer value) {
            addCriterion("agree =", value, "agree");
            return (Criteria) this;
        }

        public Criteria andAgreeNotEqualTo(Integer value) {
            addCriterion("agree <>", value, "agree");
            return (Criteria) this;
        }

        public Criteria andAgreeGreaterThan(Integer value) {
            addCriterion("agree >", value, "agree");
            return (Criteria) this;
        }

        public Criteria andAgreeGreaterThanOrEqualTo(Integer value) {
            addCriterion("agree >=", value, "agree");
            return (Criteria) this;
        }

        public Criteria andAgreeLessThan(Integer value) {
            addCriterion("agree <", value, "agree");
            return (Criteria) this;
        }

        public Criteria andAgreeLessThanOrEqualTo(Integer value) {
            addCriterion("agree <=", value, "agree");
            return (Criteria) this;
        }

        public Criteria andAgreeIn(List<Integer> values) {
            addCriterion("agree in", values, "agree");
            return (Criteria) this;
        }

        public Criteria andAgreeNotIn(List<Integer> values) {
            addCriterion("agree not in", values, "agree");
            return (Criteria) this;
        }

        public Criteria andAgreeBetween(Integer value1, Integer value2) {
            addCriterion("agree between", value1, value2, "agree");
            return (Criteria) this;
        }

        public Criteria andAgreeNotBetween(Integer value1, Integer value2) {
            addCriterion("agree not between", value1, value2, "agree");
            return (Criteria) this;
        }

        public Criteria andContent1IsNull() {
            addCriterion("content1 is null");
            return (Criteria) this;
        }

        public Criteria andContent1IsNotNull() {
            addCriterion("content1 is not null");
            return (Criteria) this;
        }

        public Criteria andContent1EqualTo(String value) {
            addCriterion("content1 =", value, "content1");
            return (Criteria) this;
        }

        public Criteria andContent1NotEqualTo(String value) {
            addCriterion("content1 <>", value, "content1");
            return (Criteria) this;
        }

        public Criteria andContent1GreaterThan(String value) {
            addCriterion("content1 >", value, "content1");
            return (Criteria) this;
        }

        public Criteria andContent1GreaterThanOrEqualTo(String value) {
            addCriterion("content1 >=", value, "content1");
            return (Criteria) this;
        }

        public Criteria andContent1LessThan(String value) {
            addCriterion("content1 <", value, "content1");
            return (Criteria) this;
        }

        public Criteria andContent1LessThanOrEqualTo(String value) {
            addCriterion("content1 <=", value, "content1");
            return (Criteria) this;
        }

        public Criteria andContent1Like(String value) {
            addCriterion("content1 like", value, "content1");
            return (Criteria) this;
        }

        public Criteria andContent1NotLike(String value) {
            addCriterion("content1 not like", value, "content1");
            return (Criteria) this;
        }

        public Criteria andContent1In(List<String> values) {
            addCriterion("content1 in", values, "content1");
            return (Criteria) this;
        }

        public Criteria andContent1NotIn(List<String> values) {
            addCriterion("content1 not in", values, "content1");
            return (Criteria) this;
        }

        public Criteria andContent1Between(String value1, String value2) {
            addCriterion("content1 between", value1, value2, "content1");
            return (Criteria) this;
        }

        public Criteria andContent1NotBetween(String value1, String value2) {
            addCriterion("content1 not between", value1, value2, "content1");
            return (Criteria) this;
        }

        public Criteria andDeliveryFeeIsNull() {
            addCriterion("deliveryFee is null");
            return (Criteria) this;
        }

        public Criteria andDeliveryFeeIsNotNull() {
            addCriterion("deliveryFee is not null");
            return (Criteria) this;
        }

        public Criteria andDeliveryFeeEqualTo(BigDecimal value) {
            addCriterion("deliveryFee =", value, "deliveryFee");
            return (Criteria) this;
        }

        public Criteria andDeliveryFeeNotEqualTo(BigDecimal value) {
            addCriterion("deliveryFee <>", value, "deliveryFee");
            return (Criteria) this;
        }

        public Criteria andDeliveryFeeGreaterThan(BigDecimal value) {
            addCriterion("deliveryFee >", value, "deliveryFee");
            return (Criteria) this;
        }

        public Criteria andDeliveryFeeGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("deliveryFee >=", value, "deliveryFee");
            return (Criteria) this;
        }

        public Criteria andDeliveryFeeLessThan(BigDecimal value) {
            addCriterion("deliveryFee <", value, "deliveryFee");
            return (Criteria) this;
        }

        public Criteria andDeliveryFeeLessThanOrEqualTo(BigDecimal value) {
            addCriterion("deliveryFee <=", value, "deliveryFee");
            return (Criteria) this;
        }

        public Criteria andDeliveryFeeIn(List<BigDecimal> values) {
            addCriterion("deliveryFee in", values, "deliveryFee");
            return (Criteria) this;
        }

        public Criteria andDeliveryFeeNotIn(List<BigDecimal> values) {
            addCriterion("deliveryFee not in", values, "deliveryFee");
            return (Criteria) this;
        }

        public Criteria andDeliveryFeeBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("deliveryFee between", value1, value2, "deliveryFee");
            return (Criteria) this;
        }

        public Criteria andDeliveryFeeNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("deliveryFee not between", value1, value2, "deliveryFee");
            return (Criteria) this;
        }

        public Criteria andSelfTimeIsNull() {
            addCriterion("selfTime is null");
            return (Criteria) this;
        }

        public Criteria andSelfTimeIsNotNull() {
            addCriterion("selfTime is not null");
            return (Criteria) this;
        }

        public Criteria andSelfTimeEqualTo(Date value) {
            addCriterion("selfTime =", value, "selfTime");
            return (Criteria) this;
        }

        public Criteria andSelfTimeNotEqualTo(Date value) {
            addCriterion("selfTime <>", value, "selfTime");
            return (Criteria) this;
        }

        public Criteria andSelfTimeGreaterThan(Date value) {
            addCriterion("selfTime >", value, "selfTime");
            return (Criteria) this;
        }

        public Criteria andSelfTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("selfTime >=", value, "selfTime");
            return (Criteria) this;
        }

        public Criteria andSelfTimeLessThan(Date value) {
            addCriterion("selfTime <", value, "selfTime");
            return (Criteria) this;
        }

        public Criteria andSelfTimeLessThanOrEqualTo(Date value) {
            addCriterion("selfTime <=", value, "selfTime");
            return (Criteria) this;
        }

        public Criteria andSelfTimeIn(List<Date> values) {
            addCriterion("selfTime in", values, "selfTime");
            return (Criteria) this;
        }

        public Criteria andSelfTimeNotIn(List<Date> values) {
            addCriterion("selfTime not in", values, "selfTime");
            return (Criteria) this;
        }

        public Criteria andSelfTimeBetween(Date value1, Date value2) {
            addCriterion("selfTime between", value1, value2, "selfTime");
            return (Criteria) this;
        }

        public Criteria andSelfTimeNotBetween(Date value1, Date value2) {
            addCriterion("selfTime not between", value1, value2, "selfTime");
            return (Criteria) this;
        }

        public Criteria andSelfMobileIsNull() {
            addCriterion("selfMobile is null");
            return (Criteria) this;
        }

        public Criteria andSelfMobileIsNotNull() {
            addCriterion("selfMobile is not null");
            return (Criteria) this;
        }

        public Criteria andSelfMobileEqualTo(String value) {
            addCriterion("selfMobile =", value, "selfMobile");
            return (Criteria) this;
        }

        public Criteria andSelfMobileNotEqualTo(String value) {
            addCriterion("selfMobile <>", value, "selfMobile");
            return (Criteria) this;
        }

        public Criteria andSelfMobileGreaterThan(String value) {
            addCriterion("selfMobile >", value, "selfMobile");
            return (Criteria) this;
        }

        public Criteria andSelfMobileGreaterThanOrEqualTo(String value) {
            addCriterion("selfMobile >=", value, "selfMobile");
            return (Criteria) this;
        }

        public Criteria andSelfMobileLessThan(String value) {
            addCriterion("selfMobile <", value, "selfMobile");
            return (Criteria) this;
        }

        public Criteria andSelfMobileLessThanOrEqualTo(String value) {
            addCriterion("selfMobile <=", value, "selfMobile");
            return (Criteria) this;
        }

        public Criteria andSelfMobileLike(String value) {
            addCriterion("selfMobile like", value, "selfMobile");
            return (Criteria) this;
        }

        public Criteria andSelfMobileNotLike(String value) {
            addCriterion("selfMobile not like", value, "selfMobile");
            return (Criteria) this;
        }

        public Criteria andSelfMobileIn(List<String> values) {
            addCriterion("selfMobile in", values, "selfMobile");
            return (Criteria) this;
        }

        public Criteria andSelfMobileNotIn(List<String> values) {
            addCriterion("selfMobile not in", values, "selfMobile");
            return (Criteria) this;
        }

        public Criteria andSelfMobileBetween(String value1, String value2) {
            addCriterion("selfMobile between", value1, value2, "selfMobile");
            return (Criteria) this;
        }

        public Criteria andSelfMobileNotBetween(String value1, String value2) {
            addCriterion("selfMobile not between", value1, value2, "selfMobile");
            return (Criteria) this;
        }

        public Criteria andSuportSelfIsNull() {
            addCriterion("suportSelf is null");
            return (Criteria) this;
        }

        public Criteria andSuportSelfIsNotNull() {
            addCriterion("suportSelf is not null");
            return (Criteria) this;
        }

        public Criteria andSuportSelfEqualTo(Boolean value) {
            addCriterion("suportSelf =", value, "suportSelf");
            return (Criteria) this;
        }

        public Criteria andSuportSelfNotEqualTo(Boolean value) {
            addCriterion("suportSelf <>", value, "suportSelf");
            return (Criteria) this;
        }

        public Criteria andSuportSelfGreaterThan(Boolean value) {
            addCriterion("suportSelf >", value, "suportSelf");
            return (Criteria) this;
        }

        public Criteria andSuportSelfGreaterThanOrEqualTo(Boolean value) {
            addCriterion("suportSelf >=", value, "suportSelf");
            return (Criteria) this;
        }

        public Criteria andSuportSelfLessThan(Boolean value) {
            addCriterion("suportSelf <", value, "suportSelf");
            return (Criteria) this;
        }

        public Criteria andSuportSelfLessThanOrEqualTo(Boolean value) {
            addCriterion("suportSelf <=", value, "suportSelf");
            return (Criteria) this;
        }

        public Criteria andSuportSelfIn(List<Boolean> values) {
            addCriterion("suportSelf in", values, "suportSelf");
            return (Criteria) this;
        }

        public Criteria andSuportSelfNotIn(List<Boolean> values) {
            addCriterion("suportSelf not in", values, "suportSelf");
            return (Criteria) this;
        }

        public Criteria andSuportSelfBetween(Boolean value1, Boolean value2) {
            addCriterion("suportSelf between", value1, value2, "suportSelf");
            return (Criteria) this;
        }

        public Criteria andSuportSelfNotBetween(Boolean value1, Boolean value2) {
            addCriterion("suportSelf not between", value1, value2, "suportSelf");
            return (Criteria) this;
        }

        public Criteria andEatInBusinessIsNull() {
            addCriterion("eatInBusiness is null");
            return (Criteria) this;
        }

        public Criteria andEatInBusinessIsNotNull() {
            addCriterion("eatInBusiness is not null");
            return (Criteria) this;
        }

        public Criteria andEatInBusinessEqualTo(Boolean value) {
            addCriterion("eatInBusiness =", value, "eatInBusiness");
            return (Criteria) this;
        }

        public Criteria andEatInBusinessNotEqualTo(Boolean value) {
            addCriterion("eatInBusiness <>", value, "eatInBusiness");
            return (Criteria) this;
        }

        public Criteria andEatInBusinessGreaterThan(Boolean value) {
            addCriterion("eatInBusiness >", value, "eatInBusiness");
            return (Criteria) this;
        }

        public Criteria andEatInBusinessGreaterThanOrEqualTo(Boolean value) {
            addCriterion("eatInBusiness >=", value, "eatInBusiness");
            return (Criteria) this;
        }

        public Criteria andEatInBusinessLessThan(Boolean value) {
            addCriterion("eatInBusiness <", value, "eatInBusiness");
            return (Criteria) this;
        }

        public Criteria andEatInBusinessLessThanOrEqualTo(Boolean value) {
            addCriterion("eatInBusiness <=", value, "eatInBusiness");
            return (Criteria) this;
        }

        public Criteria andEatInBusinessIn(List<Boolean> values) {
            addCriterion("eatInBusiness in", values, "eatInBusiness");
            return (Criteria) this;
        }

        public Criteria andEatInBusinessNotIn(List<Boolean> values) {
            addCriterion("eatInBusiness not in", values, "eatInBusiness");
            return (Criteria) this;
        }

        public Criteria andEatInBusinessBetween(Boolean value1, Boolean value2) {
            addCriterion("eatInBusiness between", value1, value2, "eatInBusiness");
            return (Criteria) this;
        }

        public Criteria andEatInBusinessNotBetween(Boolean value1, Boolean value2) {
            addCriterion("eatInBusiness not between", value1, value2, "eatInBusiness");
            return (Criteria) this;
        }

        public Criteria andCancelTimeIsNull() {
            addCriterion("cancelTime is null");
            return (Criteria) this;
        }

        public Criteria andCancelTimeIsNotNull() {
            addCriterion("cancelTime is not null");
            return (Criteria) this;
        }

        public Criteria andCancelTimeEqualTo(Date value) {
            addCriterion("cancelTime =", value, "cancelTime");
            return (Criteria) this;
        }

        public Criteria andCancelTimeNotEqualTo(Date value) {
            addCriterion("cancelTime <>", value, "cancelTime");
            return (Criteria) this;
        }

        public Criteria andCancelTimeGreaterThan(Date value) {
            addCriterion("cancelTime >", value, "cancelTime");
            return (Criteria) this;
        }

        public Criteria andCancelTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("cancelTime >=", value, "cancelTime");
            return (Criteria) this;
        }

        public Criteria andCancelTimeLessThan(Date value) {
            addCriterion("cancelTime <", value, "cancelTime");
            return (Criteria) this;
        }

        public Criteria andCancelTimeLessThanOrEqualTo(Date value) {
            addCriterion("cancelTime <=", value, "cancelTime");
            return (Criteria) this;
        }

        public Criteria andCancelTimeIn(List<Date> values) {
            addCriterion("cancelTime in", values, "cancelTime");
            return (Criteria) this;
        }

        public Criteria andCancelTimeNotIn(List<Date> values) {
            addCriterion("cancelTime not in", values, "cancelTime");
            return (Criteria) this;
        }

        public Criteria andCancelTimeBetween(Date value1, Date value2) {
            addCriterion("cancelTime between", value1, value2, "cancelTime");
            return (Criteria) this;
        }

        public Criteria andCancelTimeNotBetween(Date value1, Date value2) {
            addCriterion("cancelTime not between", value1, value2, "cancelTime");
            return (Criteria) this;
        }

        public Criteria andUserAddressTagIsNull() {
            addCriterion("userAddressTag is null");
            return (Criteria) this;
        }

        public Criteria andUserAddressTagIsNotNull() {
            addCriterion("userAddressTag is not null");
            return (Criteria) this;
        }

        public Criteria andUserAddressTagEqualTo(Integer value) {
            addCriterion("userAddressTag =", value, "userAddressTag");
            return (Criteria) this;
        }

        public Criteria andUserAddressTagNotEqualTo(Integer value) {
            addCriterion("userAddressTag <>", value, "userAddressTag");
            return (Criteria) this;
        }

        public Criteria andUserAddressTagGreaterThan(Integer value) {
            addCriterion("userAddressTag >", value, "userAddressTag");
            return (Criteria) this;
        }

        public Criteria andUserAddressTagGreaterThanOrEqualTo(Integer value) {
            addCriterion("userAddressTag >=", value, "userAddressTag");
            return (Criteria) this;
        }

        public Criteria andUserAddressTagLessThan(Integer value) {
            addCriterion("userAddressTag <", value, "userAddressTag");
            return (Criteria) this;
        }

        public Criteria andUserAddressTagLessThanOrEqualTo(Integer value) {
            addCriterion("userAddressTag <=", value, "userAddressTag");
            return (Criteria) this;
        }

        public Criteria andUserAddressTagIn(List<Integer> values) {
            addCriterion("userAddressTag in", values, "userAddressTag");
            return (Criteria) this;
        }

        public Criteria andUserAddressTagNotIn(List<Integer> values) {
            addCriterion("userAddressTag not in", values, "userAddressTag");
            return (Criteria) this;
        }

        public Criteria andUserAddressTagBetween(Integer value1, Integer value2) {
            addCriterion("userAddressTag between", value1, value2, "userAddressTag");
            return (Criteria) this;
        }

        public Criteria andUserAddressTagNotBetween(Integer value1, Integer value2) {
            addCriterion("userAddressTag not between", value1, value2, "userAddressTag");
            return (Criteria) this;
        }

        public Criteria andBookedIsNull() {
            addCriterion("booked is null");
            return (Criteria) this;
        }

        public Criteria andBookedIsNotNull() {
            addCriterion("booked is not null");
            return (Criteria) this;
        }

        public Criteria andBookedEqualTo(Boolean value) {
            addCriterion("booked =", value, "booked");
            return (Criteria) this;
        }

        public Criteria andBookedNotEqualTo(Boolean value) {
            addCriterion("booked <>", value, "booked");
            return (Criteria) this;
        }

        public Criteria andBookedGreaterThan(Boolean value) {
            addCriterion("booked >", value, "booked");
            return (Criteria) this;
        }

        public Criteria andBookedGreaterThanOrEqualTo(Boolean value) {
            addCriterion("booked >=", value, "booked");
            return (Criteria) this;
        }

        public Criteria andBookedLessThan(Boolean value) {
            addCriterion("booked <", value, "booked");
            return (Criteria) this;
        }

        public Criteria andBookedLessThanOrEqualTo(Boolean value) {
            addCriterion("booked <=", value, "booked");
            return (Criteria) this;
        }

        public Criteria andBookedIn(List<Boolean> values) {
            addCriterion("booked in", values, "booked");
            return (Criteria) this;
        }

        public Criteria andBookedNotIn(List<Boolean> values) {
            addCriterion("booked not in", values, "booked");
            return (Criteria) this;
        }

        public Criteria andBookedBetween(Boolean value1, Boolean value2) {
            addCriterion("booked between", value1, value2, "booked");
            return (Criteria) this;
        }

        public Criteria andBookedNotBetween(Boolean value1, Boolean value2) {
            addCriterion("booked not between", value1, value2, "booked");
            return (Criteria) this;
        }

        public Criteria andBookTimeIsNull() {
            addCriterion("bookTime is null");
            return (Criteria) this;
        }

        public Criteria andBookTimeIsNotNull() {
            addCriterion("bookTime is not null");
            return (Criteria) this;
        }

        public Criteria andBookTimeEqualTo(Date value) {
            addCriterion("bookTime =", value, "bookTime");
            return (Criteria) this;
        }

        public Criteria andBookTimeNotEqualTo(Date value) {
            addCriterion("bookTime <>", value, "bookTime");
            return (Criteria) this;
        }

        public Criteria andBookTimeGreaterThan(Date value) {
            addCriterion("bookTime >", value, "bookTime");
            return (Criteria) this;
        }

        public Criteria andBookTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("bookTime >=", value, "bookTime");
            return (Criteria) this;
        }

        public Criteria andBookTimeLessThan(Date value) {
            addCriterion("bookTime <", value, "bookTime");
            return (Criteria) this;
        }

        public Criteria andBookTimeLessThanOrEqualTo(Date value) {
            addCriterion("bookTime <=", value, "bookTime");
            return (Criteria) this;
        }

        public Criteria andBookTimeIn(List<Date> values) {
            addCriterion("bookTime in", values, "bookTime");
            return (Criteria) this;
        }

        public Criteria andBookTimeNotIn(List<Date> values) {
            addCriterion("bookTime not in", values, "bookTime");
            return (Criteria) this;
        }

        public Criteria andBookTimeBetween(Date value1, Date value2) {
            addCriterion("bookTime between", value1, value2, "bookTime");
            return (Criteria) this;
        }

        public Criteria andBookTimeNotBetween(Date value1, Date value2) {
            addCriterion("bookTime not between", value1, value2, "bookTime");
            return (Criteria) this;
        }

        public Criteria andUserAddressGenderIsNull() {
            addCriterion("userAddressGender is null");
            return (Criteria) this;
        }

        public Criteria andUserAddressGenderIsNotNull() {
            addCriterion("userAddressGender is not null");
            return (Criteria) this;
        }

        public Criteria andUserAddressGenderEqualTo(Integer value) {
            addCriterion("userAddressGender =", value, "userAddressGender");
            return (Criteria) this;
        }

        public Criteria andUserAddressGenderNotEqualTo(Integer value) {
            addCriterion("userAddressGender <>", value, "userAddressGender");
            return (Criteria) this;
        }

        public Criteria andUserAddressGenderGreaterThan(Integer value) {
            addCriterion("userAddressGender >", value, "userAddressGender");
            return (Criteria) this;
        }

        public Criteria andUserAddressGenderGreaterThanOrEqualTo(Integer value) {
            addCriterion("userAddressGender >=", value, "userAddressGender");
            return (Criteria) this;
        }

        public Criteria andUserAddressGenderLessThan(Integer value) {
            addCriterion("userAddressGender <", value, "userAddressGender");
            return (Criteria) this;
        }

        public Criteria andUserAddressGenderLessThanOrEqualTo(Integer value) {
            addCriterion("userAddressGender <=", value, "userAddressGender");
            return (Criteria) this;
        }

        public Criteria andUserAddressGenderIn(List<Integer> values) {
            addCriterion("userAddressGender in", values, "userAddressGender");
            return (Criteria) this;
        }

        public Criteria andUserAddressGenderNotIn(List<Integer> values) {
            addCriterion("userAddressGender not in", values, "userAddressGender");
            return (Criteria) this;
        }

        public Criteria andUserAddressGenderBetween(Integer value1, Integer value2) {
            addCriterion("userAddressGender between", value1, value2, "userAddressGender");
            return (Criteria) this;
        }

        public Criteria andUserAddressGenderNotBetween(Integer value1, Integer value2) {
            addCriterion("userAddressGender not between", value1, value2, "userAddressGender");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}