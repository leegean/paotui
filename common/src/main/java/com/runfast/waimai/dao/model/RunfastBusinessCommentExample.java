package com.runfast.waimai.dao.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RunfastBusinessCommentExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public RunfastBusinessCommentExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andBusinessIdIsNull() {
            addCriterion("businessId is null");
            return (Criteria) this;
        }

        public Criteria andBusinessIdIsNotNull() {
            addCriterion("businessId is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessIdEqualTo(Integer value) {
            addCriterion("businessId =", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdNotEqualTo(Integer value) {
            addCriterion("businessId <>", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdGreaterThan(Integer value) {
            addCriterion("businessId >", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("businessId >=", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdLessThan(Integer value) {
            addCriterion("businessId <", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdLessThanOrEqualTo(Integer value) {
            addCriterion("businessId <=", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdIn(List<Integer> values) {
            addCriterion("businessId in", values, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdNotIn(List<Integer> values) {
            addCriterion("businessId not in", values, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdBetween(Integer value1, Integer value2) {
            addCriterion("businessId between", value1, value2, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdNotBetween(Integer value1, Integer value2) {
            addCriterion("businessId not between", value1, value2, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessNameIsNull() {
            addCriterion("businessName is null");
            return (Criteria) this;
        }

        public Criteria andBusinessNameIsNotNull() {
            addCriterion("businessName is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessNameEqualTo(String value) {
            addCriterion("businessName =", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameNotEqualTo(String value) {
            addCriterion("businessName <>", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameGreaterThan(String value) {
            addCriterion("businessName >", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameGreaterThanOrEqualTo(String value) {
            addCriterion("businessName >=", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameLessThan(String value) {
            addCriterion("businessName <", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameLessThanOrEqualTo(String value) {
            addCriterion("businessName <=", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameLike(String value) {
            addCriterion("businessName like", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameNotLike(String value) {
            addCriterion("businessName not like", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameIn(List<String> values) {
            addCriterion("businessName in", values, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameNotIn(List<String> values) {
            addCriterion("businessName not in", values, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameBetween(String value1, String value2) {
            addCriterion("businessName between", value1, value2, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameNotBetween(String value1, String value2) {
            addCriterion("businessName not between", value1, value2, "businessName");
            return (Criteria) this;
        }

        public Criteria andCostIsNull() {
            addCriterion("cost is null");
            return (Criteria) this;
        }

        public Criteria andCostIsNotNull() {
            addCriterion("cost is not null");
            return (Criteria) this;
        }

        public Criteria andCostEqualTo(Double value) {
            addCriterion("cost =", value, "cost");
            return (Criteria) this;
        }

        public Criteria andCostNotEqualTo(Double value) {
            addCriterion("cost <>", value, "cost");
            return (Criteria) this;
        }

        public Criteria andCostGreaterThan(Double value) {
            addCriterion("cost >", value, "cost");
            return (Criteria) this;
        }

        public Criteria andCostGreaterThanOrEqualTo(Double value) {
            addCriterion("cost >=", value, "cost");
            return (Criteria) this;
        }

        public Criteria andCostLessThan(Double value) {
            addCriterion("cost <", value, "cost");
            return (Criteria) this;
        }

        public Criteria andCostLessThanOrEqualTo(Double value) {
            addCriterion("cost <=", value, "cost");
            return (Criteria) this;
        }

        public Criteria andCostIn(List<Double> values) {
            addCriterion("cost in", values, "cost");
            return (Criteria) this;
        }

        public Criteria andCostNotIn(List<Double> values) {
            addCriterion("cost not in", values, "cost");
            return (Criteria) this;
        }

        public Criteria andCostBetween(Double value1, Double value2) {
            addCriterion("cost between", value1, value2, "cost");
            return (Criteria) this;
        }

        public Criteria andCostNotBetween(Double value1, Double value2) {
            addCriterion("cost not between", value1, value2, "cost");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("createTime is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("createTime is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("createTime =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("createTime <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("createTime >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("createTime >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("createTime <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("createTime <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("createTime in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("createTime not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("createTime between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("createTime not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andDelicerIdIsNull() {
            addCriterion("delicerId is null");
            return (Criteria) this;
        }

        public Criteria andDelicerIdIsNotNull() {
            addCriterion("delicerId is not null");
            return (Criteria) this;
        }

        public Criteria andDelicerIdEqualTo(Integer value) {
            addCriterion("delicerId =", value, "delicerId");
            return (Criteria) this;
        }

        public Criteria andDelicerIdNotEqualTo(Integer value) {
            addCriterion("delicerId <>", value, "delicerId");
            return (Criteria) this;
        }

        public Criteria andDelicerIdGreaterThan(Integer value) {
            addCriterion("delicerId >", value, "delicerId");
            return (Criteria) this;
        }

        public Criteria andDelicerIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("delicerId >=", value, "delicerId");
            return (Criteria) this;
        }

        public Criteria andDelicerIdLessThan(Integer value) {
            addCriterion("delicerId <", value, "delicerId");
            return (Criteria) this;
        }

        public Criteria andDelicerIdLessThanOrEqualTo(Integer value) {
            addCriterion("delicerId <=", value, "delicerId");
            return (Criteria) this;
        }

        public Criteria andDelicerIdIn(List<Integer> values) {
            addCriterion("delicerId in", values, "delicerId");
            return (Criteria) this;
        }

        public Criteria andDelicerIdNotIn(List<Integer> values) {
            addCriterion("delicerId not in", values, "delicerId");
            return (Criteria) this;
        }

        public Criteria andDelicerIdBetween(Integer value1, Integer value2) {
            addCriterion("delicerId between", value1, value2, "delicerId");
            return (Criteria) this;
        }

        public Criteria andDelicerIdNotBetween(Integer value1, Integer value2) {
            addCriterion("delicerId not between", value1, value2, "delicerId");
            return (Criteria) this;
        }

        public Criteria andDelicerNameIsNull() {
            addCriterion("delicerName is null");
            return (Criteria) this;
        }

        public Criteria andDelicerNameIsNotNull() {
            addCriterion("delicerName is not null");
            return (Criteria) this;
        }

        public Criteria andDelicerNameEqualTo(String value) {
            addCriterion("delicerName =", value, "delicerName");
            return (Criteria) this;
        }

        public Criteria andDelicerNameNotEqualTo(String value) {
            addCriterion("delicerName <>", value, "delicerName");
            return (Criteria) this;
        }

        public Criteria andDelicerNameGreaterThan(String value) {
            addCriterion("delicerName >", value, "delicerName");
            return (Criteria) this;
        }

        public Criteria andDelicerNameGreaterThanOrEqualTo(String value) {
            addCriterion("delicerName >=", value, "delicerName");
            return (Criteria) this;
        }

        public Criteria andDelicerNameLessThan(String value) {
            addCriterion("delicerName <", value, "delicerName");
            return (Criteria) this;
        }

        public Criteria andDelicerNameLessThanOrEqualTo(String value) {
            addCriterion("delicerName <=", value, "delicerName");
            return (Criteria) this;
        }

        public Criteria andDelicerNameLike(String value) {
            addCriterion("delicerName like", value, "delicerName");
            return (Criteria) this;
        }

        public Criteria andDelicerNameNotLike(String value) {
            addCriterion("delicerName not like", value, "delicerName");
            return (Criteria) this;
        }

        public Criteria andDelicerNameIn(List<String> values) {
            addCriterion("delicerName in", values, "delicerName");
            return (Criteria) this;
        }

        public Criteria andDelicerNameNotIn(List<String> values) {
            addCriterion("delicerName not in", values, "delicerName");
            return (Criteria) this;
        }

        public Criteria andDelicerNameBetween(String value1, String value2) {
            addCriterion("delicerName between", value1, value2, "delicerName");
            return (Criteria) this;
        }

        public Criteria andDelicerNameNotBetween(String value1, String value2) {
            addCriterion("delicerName not between", value1, value2, "delicerName");
            return (Criteria) this;
        }

        public Criteria andDelicerTimeIsNull() {
            addCriterion("delicerTime is null");
            return (Criteria) this;
        }

        public Criteria andDelicerTimeIsNotNull() {
            addCriterion("delicerTime is not null");
            return (Criteria) this;
        }

        public Criteria andDelicerTimeEqualTo(Date value) {
            addCriterion("delicerTime =", value, "delicerTime");
            return (Criteria) this;
        }

        public Criteria andDelicerTimeNotEqualTo(Date value) {
            addCriterion("delicerTime <>", value, "delicerTime");
            return (Criteria) this;
        }

        public Criteria andDelicerTimeGreaterThan(Date value) {
            addCriterion("delicerTime >", value, "delicerTime");
            return (Criteria) this;
        }

        public Criteria andDelicerTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("delicerTime >=", value, "delicerTime");
            return (Criteria) this;
        }

        public Criteria andDelicerTimeLessThan(Date value) {
            addCriterion("delicerTime <", value, "delicerTime");
            return (Criteria) this;
        }

        public Criteria andDelicerTimeLessThanOrEqualTo(Date value) {
            addCriterion("delicerTime <=", value, "delicerTime");
            return (Criteria) this;
        }

        public Criteria andDelicerTimeIn(List<Date> values) {
            addCriterion("delicerTime in", values, "delicerTime");
            return (Criteria) this;
        }

        public Criteria andDelicerTimeNotIn(List<Date> values) {
            addCriterion("delicerTime not in", values, "delicerTime");
            return (Criteria) this;
        }

        public Criteria andDelicerTimeBetween(Date value1, Date value2) {
            addCriterion("delicerTime between", value1, value2, "delicerTime");
            return (Criteria) this;
        }

        public Criteria andDelicerTimeNotBetween(Date value1, Date value2) {
            addCriterion("delicerTime not between", value1, value2, "delicerTime");
            return (Criteria) this;
        }

        public Criteria andOrderTimeIsNull() {
            addCriterion("orderTime is null");
            return (Criteria) this;
        }

        public Criteria andOrderTimeIsNotNull() {
            addCriterion("orderTime is not null");
            return (Criteria) this;
        }

        public Criteria andOrderTimeEqualTo(Date value) {
            addCriterion("orderTime =", value, "orderTime");
            return (Criteria) this;
        }

        public Criteria andOrderTimeNotEqualTo(Date value) {
            addCriterion("orderTime <>", value, "orderTime");
            return (Criteria) this;
        }

        public Criteria andOrderTimeGreaterThan(Date value) {
            addCriterion("orderTime >", value, "orderTime");
            return (Criteria) this;
        }

        public Criteria andOrderTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("orderTime >=", value, "orderTime");
            return (Criteria) this;
        }

        public Criteria andOrderTimeLessThan(Date value) {
            addCriterion("orderTime <", value, "orderTime");
            return (Criteria) this;
        }

        public Criteria andOrderTimeLessThanOrEqualTo(Date value) {
            addCriterion("orderTime <=", value, "orderTime");
            return (Criteria) this;
        }

        public Criteria andOrderTimeIn(List<Date> values) {
            addCriterion("orderTime in", values, "orderTime");
            return (Criteria) this;
        }

        public Criteria andOrderTimeNotIn(List<Date> values) {
            addCriterion("orderTime not in", values, "orderTime");
            return (Criteria) this;
        }

        public Criteria andOrderTimeBetween(Date value1, Date value2) {
            addCriterion("orderTime between", value1, value2, "orderTime");
            return (Criteria) this;
        }

        public Criteria andOrderTimeNotBetween(Date value1, Date value2) {
            addCriterion("orderTime not between", value1, value2, "orderTime");
            return (Criteria) this;
        }

        public Criteria andScoreIsNull() {
            addCriterion("score is null");
            return (Criteria) this;
        }

        public Criteria andScoreIsNotNull() {
            addCriterion("score is not null");
            return (Criteria) this;
        }

        public Criteria andScoreEqualTo(Double value) {
            addCriterion("score =", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotEqualTo(Double value) {
            addCriterion("score <>", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreGreaterThan(Double value) {
            addCriterion("score >", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreGreaterThanOrEqualTo(Double value) {
            addCriterion("score >=", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreLessThan(Double value) {
            addCriterion("score <", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreLessThanOrEqualTo(Double value) {
            addCriterion("score <=", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreIn(List<Double> values) {
            addCriterion("score in", values, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotIn(List<Double> values) {
            addCriterion("score not in", values, "score");
            return (Criteria) this;
        }

        public Criteria andScoreBetween(Double value1, Double value2) {
            addCriterion("score between", value1, value2, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotBetween(Double value1, Double value2) {
            addCriterion("score not between", value1, value2, "score");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNull() {
            addCriterion("userId is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("userId is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(Integer value) {
            addCriterion("userId =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(Integer value) {
            addCriterion("userId <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(Integer value) {
            addCriterion("userId >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("userId >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(Integer value) {
            addCriterion("userId <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(Integer value) {
            addCriterion("userId <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<Integer> values) {
            addCriterion("userId in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<Integer> values) {
            addCriterion("userId not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(Integer value1, Integer value2) {
            addCriterion("userId between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(Integer value1, Integer value2) {
            addCriterion("userId not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserNameIsNull() {
            addCriterion("userName is null");
            return (Criteria) this;
        }

        public Criteria andUserNameIsNotNull() {
            addCriterion("userName is not null");
            return (Criteria) this;
        }

        public Criteria andUserNameEqualTo(String value) {
            addCriterion("userName =", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotEqualTo(String value) {
            addCriterion("userName <>", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameGreaterThan(String value) {
            addCriterion("userName >", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameGreaterThanOrEqualTo(String value) {
            addCriterion("userName >=", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLessThan(String value) {
            addCriterion("userName <", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLessThanOrEqualTo(String value) {
            addCriterion("userName <=", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLike(String value) {
            addCriterion("userName like", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotLike(String value) {
            addCriterion("userName not like", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameIn(List<String> values) {
            addCriterion("userName in", values, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotIn(List<String> values) {
            addCriterion("userName not in", values, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameBetween(String value1, String value2) {
            addCriterion("userName between", value1, value2, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotBetween(String value1, String value2) {
            addCriterion("userName not between", value1, value2, "userName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdIsNull() {
            addCriterion("goodsSellId is null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdIsNotNull() {
            addCriterion("goodsSellId is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdEqualTo(Integer value) {
            addCriterion("goodsSellId =", value, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdNotEqualTo(Integer value) {
            addCriterion("goodsSellId <>", value, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdGreaterThan(Integer value) {
            addCriterion("goodsSellId >", value, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("goodsSellId >=", value, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdLessThan(Integer value) {
            addCriterion("goodsSellId <", value, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdLessThanOrEqualTo(Integer value) {
            addCriterion("goodsSellId <=", value, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdIn(List<Integer> values) {
            addCriterion("goodsSellId in", values, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdNotIn(List<Integer> values) {
            addCriterion("goodsSellId not in", values, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdBetween(Integer value1, Integer value2) {
            addCriterion("goodsSellId between", value1, value2, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdNotBetween(Integer value1, Integer value2) {
            addCriterion("goodsSellId not between", value1, value2, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameIsNull() {
            addCriterion("goodsSellName is null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameIsNotNull() {
            addCriterion("goodsSellName is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameEqualTo(String value) {
            addCriterion("goodsSellName =", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameNotEqualTo(String value) {
            addCriterion("goodsSellName <>", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameGreaterThan(String value) {
            addCriterion("goodsSellName >", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameGreaterThanOrEqualTo(String value) {
            addCriterion("goodsSellName >=", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameLessThan(String value) {
            addCriterion("goodsSellName <", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameLessThanOrEqualTo(String value) {
            addCriterion("goodsSellName <=", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameLike(String value) {
            addCriterion("goodsSellName like", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameNotLike(String value) {
            addCriterion("goodsSellName not like", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameIn(List<String> values) {
            addCriterion("goodsSellName in", values, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameNotIn(List<String> values) {
            addCriterion("goodsSellName not in", values, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameBetween(String value1, String value2) {
            addCriterion("goodsSellName between", value1, value2, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameNotBetween(String value1, String value2) {
            addCriterion("goodsSellName not between", value1, value2, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andOrderCodeIsNull() {
            addCriterion("orderCode is null");
            return (Criteria) this;
        }

        public Criteria andOrderCodeIsNotNull() {
            addCriterion("orderCode is not null");
            return (Criteria) this;
        }

        public Criteria andOrderCodeEqualTo(String value) {
            addCriterion("orderCode =", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeNotEqualTo(String value) {
            addCriterion("orderCode <>", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeGreaterThan(String value) {
            addCriterion("orderCode >", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeGreaterThanOrEqualTo(String value) {
            addCriterion("orderCode >=", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeLessThan(String value) {
            addCriterion("orderCode <", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeLessThanOrEqualTo(String value) {
            addCriterion("orderCode <=", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeLike(String value) {
            addCriterion("orderCode like", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeNotLike(String value) {
            addCriterion("orderCode not like", value, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeIn(List<String> values) {
            addCriterion("orderCode in", values, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeNotIn(List<String> values) {
            addCriterion("orderCode not in", values, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeBetween(String value1, String value2) {
            addCriterion("orderCode between", value1, value2, "orderCode");
            return (Criteria) this;
        }

        public Criteria andOrderCodeNotBetween(String value1, String value2) {
            addCriterion("orderCode not between", value1, value2, "orderCode");
            return (Criteria) this;
        }

        public Criteria andPicIsNull() {
            addCriterion("pic is null");
            return (Criteria) this;
        }

        public Criteria andPicIsNotNull() {
            addCriterion("pic is not null");
            return (Criteria) this;
        }

        public Criteria andPicEqualTo(String value) {
            addCriterion("pic =", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicNotEqualTo(String value) {
            addCriterion("pic <>", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicGreaterThan(String value) {
            addCriterion("pic >", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicGreaterThanOrEqualTo(String value) {
            addCriterion("pic >=", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicLessThan(String value) {
            addCriterion("pic <", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicLessThanOrEqualTo(String value) {
            addCriterion("pic <=", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicLike(String value) {
            addCriterion("pic like", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicNotLike(String value) {
            addCriterion("pic not like", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicIn(List<String> values) {
            addCriterion("pic in", values, "pic");
            return (Criteria) this;
        }

        public Criteria andPicNotIn(List<String> values) {
            addCriterion("pic not in", values, "pic");
            return (Criteria) this;
        }

        public Criteria andPicBetween(String value1, String value2) {
            addCriterion("pic between", value1, value2, "pic");
            return (Criteria) this;
        }

        public Criteria andPicNotBetween(String value1, String value2) {
            addCriterion("pic not between", value1, value2, "pic");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdIsNull() {
            addCriterion("goodsSellRecordId is null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdIsNotNull() {
            addCriterion("goodsSellRecordId is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdEqualTo(Integer value) {
            addCriterion("goodsSellRecordId =", value, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdNotEqualTo(Integer value) {
            addCriterion("goodsSellRecordId <>", value, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdGreaterThan(Integer value) {
            addCriterion("goodsSellRecordId >", value, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("goodsSellRecordId >=", value, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdLessThan(Integer value) {
            addCriterion("goodsSellRecordId <", value, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdLessThanOrEqualTo(Integer value) {
            addCriterion("goodsSellRecordId <=", value, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdIn(List<Integer> values) {
            addCriterion("goodsSellRecordId in", values, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdNotIn(List<Integer> values) {
            addCriterion("goodsSellRecordId not in", values, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdBetween(Integer value1, Integer value2) {
            addCriterion("goodsSellRecordId between", value1, value2, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdNotBetween(Integer value1, Integer value2) {
            addCriterion("goodsSellRecordId not between", value1, value2, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andAgentIdIsNull() {
            addCriterion("agentId is null");
            return (Criteria) this;
        }

        public Criteria andAgentIdIsNotNull() {
            addCriterion("agentId is not null");
            return (Criteria) this;
        }

        public Criteria andAgentIdEqualTo(Integer value) {
            addCriterion("agentId =", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotEqualTo(Integer value) {
            addCriterion("agentId <>", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdGreaterThan(Integer value) {
            addCriterion("agentId >", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("agentId >=", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdLessThan(Integer value) {
            addCriterion("agentId <", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdLessThanOrEqualTo(Integer value) {
            addCriterion("agentId <=", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdIn(List<Integer> values) {
            addCriterion("agentId in", values, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotIn(List<Integer> values) {
            addCriterion("agentId not in", values, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdBetween(Integer value1, Integer value2) {
            addCriterion("agentId between", value1, value2, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotBetween(Integer value1, Integer value2) {
            addCriterion("agentId not between", value1, value2, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentNameIsNull() {
            addCriterion("agentName is null");
            return (Criteria) this;
        }

        public Criteria andAgentNameIsNotNull() {
            addCriterion("agentName is not null");
            return (Criteria) this;
        }

        public Criteria andAgentNameEqualTo(String value) {
            addCriterion("agentName =", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotEqualTo(String value) {
            addCriterion("agentName <>", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameGreaterThan(String value) {
            addCriterion("agentName >", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameGreaterThanOrEqualTo(String value) {
            addCriterion("agentName >=", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameLessThan(String value) {
            addCriterion("agentName <", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameLessThanOrEqualTo(String value) {
            addCriterion("agentName <=", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameLike(String value) {
            addCriterion("agentName like", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotLike(String value) {
            addCriterion("agentName not like", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameIn(List<String> values) {
            addCriterion("agentName in", values, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotIn(List<String> values) {
            addCriterion("agentName not in", values, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameBetween(String value1, String value2) {
            addCriterion("agentName between", value1, value2, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotBetween(String value1, String value2) {
            addCriterion("agentName not between", value1, value2, "agentName");
            return (Criteria) this;
        }

        public Criteria andShangstrIsNull() {
            addCriterion("shangstr is null");
            return (Criteria) this;
        }

        public Criteria andShangstrIsNotNull() {
            addCriterion("shangstr is not null");
            return (Criteria) this;
        }

        public Criteria andShangstrEqualTo(String value) {
            addCriterion("shangstr =", value, "shangstr");
            return (Criteria) this;
        }

        public Criteria andShangstrNotEqualTo(String value) {
            addCriterion("shangstr <>", value, "shangstr");
            return (Criteria) this;
        }

        public Criteria andShangstrGreaterThan(String value) {
            addCriterion("shangstr >", value, "shangstr");
            return (Criteria) this;
        }

        public Criteria andShangstrGreaterThanOrEqualTo(String value) {
            addCriterion("shangstr >=", value, "shangstr");
            return (Criteria) this;
        }

        public Criteria andShangstrLessThan(String value) {
            addCriterion("shangstr <", value, "shangstr");
            return (Criteria) this;
        }

        public Criteria andShangstrLessThanOrEqualTo(String value) {
            addCriterion("shangstr <=", value, "shangstr");
            return (Criteria) this;
        }

        public Criteria andShangstrLike(String value) {
            addCriterion("shangstr like", value, "shangstr");
            return (Criteria) this;
        }

        public Criteria andShangstrNotLike(String value) {
            addCriterion("shangstr not like", value, "shangstr");
            return (Criteria) this;
        }

        public Criteria andShangstrIn(List<String> values) {
            addCriterion("shangstr in", values, "shangstr");
            return (Criteria) this;
        }

        public Criteria andShangstrNotIn(List<String> values) {
            addCriterion("shangstr not in", values, "shangstr");
            return (Criteria) this;
        }

        public Criteria andShangstrBetween(String value1, String value2) {
            addCriterion("shangstr between", value1, value2, "shangstr");
            return (Criteria) this;
        }

        public Criteria andShangstrNotBetween(String value1, String value2) {
            addCriterion("shangstr not between", value1, value2, "shangstr");
            return (Criteria) this;
        }

        public Criteria andFeedTimeIsNull() {
            addCriterion("feedTime is null");
            return (Criteria) this;
        }

        public Criteria andFeedTimeIsNotNull() {
            addCriterion("feedTime is not null");
            return (Criteria) this;
        }

        public Criteria andFeedTimeEqualTo(Date value) {
            addCriterion("feedTime =", value, "feedTime");
            return (Criteria) this;
        }

        public Criteria andFeedTimeNotEqualTo(Date value) {
            addCriterion("feedTime <>", value, "feedTime");
            return (Criteria) this;
        }

        public Criteria andFeedTimeGreaterThan(Date value) {
            addCriterion("feedTime >", value, "feedTime");
            return (Criteria) this;
        }

        public Criteria andFeedTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("feedTime >=", value, "feedTime");
            return (Criteria) this;
        }

        public Criteria andFeedTimeLessThan(Date value) {
            addCriterion("feedTime <", value, "feedTime");
            return (Criteria) this;
        }

        public Criteria andFeedTimeLessThanOrEqualTo(Date value) {
            addCriterion("feedTime <=", value, "feedTime");
            return (Criteria) this;
        }

        public Criteria andFeedTimeIn(List<Date> values) {
            addCriterion("feedTime in", values, "feedTime");
            return (Criteria) this;
        }

        public Criteria andFeedTimeNotIn(List<Date> values) {
            addCriterion("feedTime not in", values, "feedTime");
            return (Criteria) this;
        }

        public Criteria andFeedTimeBetween(Date value1, Date value2) {
            addCriterion("feedTime between", value1, value2, "feedTime");
            return (Criteria) this;
        }

        public Criteria andFeedTimeNotBetween(Date value1, Date value2) {
            addCriterion("feedTime not between", value1, value2, "feedTime");
            return (Criteria) this;
        }

        public Criteria andRecontentIsNull() {
            addCriterion("recontent is null");
            return (Criteria) this;
        }

        public Criteria andRecontentIsNotNull() {
            addCriterion("recontent is not null");
            return (Criteria) this;
        }

        public Criteria andRecontentEqualTo(String value) {
            addCriterion("recontent =", value, "recontent");
            return (Criteria) this;
        }

        public Criteria andRecontentNotEqualTo(String value) {
            addCriterion("recontent <>", value, "recontent");
            return (Criteria) this;
        }

        public Criteria andRecontentGreaterThan(String value) {
            addCriterion("recontent >", value, "recontent");
            return (Criteria) this;
        }

        public Criteria andRecontentGreaterThanOrEqualTo(String value) {
            addCriterion("recontent >=", value, "recontent");
            return (Criteria) this;
        }

        public Criteria andRecontentLessThan(String value) {
            addCriterion("recontent <", value, "recontent");
            return (Criteria) this;
        }

        public Criteria andRecontentLessThanOrEqualTo(String value) {
            addCriterion("recontent <=", value, "recontent");
            return (Criteria) this;
        }

        public Criteria andRecontentLike(String value) {
            addCriterion("recontent like", value, "recontent");
            return (Criteria) this;
        }

        public Criteria andRecontentNotLike(String value) {
            addCriterion("recontent not like", value, "recontent");
            return (Criteria) this;
        }

        public Criteria andRecontentIn(List<String> values) {
            addCriterion("recontent in", values, "recontent");
            return (Criteria) this;
        }

        public Criteria andRecontentNotIn(List<String> values) {
            addCriterion("recontent not in", values, "recontent");
            return (Criteria) this;
        }

        public Criteria andRecontentBetween(String value1, String value2) {
            addCriterion("recontent between", value1, value2, "recontent");
            return (Criteria) this;
        }

        public Criteria andRecontentNotBetween(String value1, String value2) {
            addCriterion("recontent not between", value1, value2, "recontent");
            return (Criteria) this;
        }

        public Criteria andRecreateTimeIsNull() {
            addCriterion("recreateTime is null");
            return (Criteria) this;
        }

        public Criteria andRecreateTimeIsNotNull() {
            addCriterion("recreateTime is not null");
            return (Criteria) this;
        }

        public Criteria andRecreateTimeEqualTo(Date value) {
            addCriterion("recreateTime =", value, "recreateTime");
            return (Criteria) this;
        }

        public Criteria andRecreateTimeNotEqualTo(Date value) {
            addCriterion("recreateTime <>", value, "recreateTime");
            return (Criteria) this;
        }

        public Criteria andRecreateTimeGreaterThan(Date value) {
            addCriterion("recreateTime >", value, "recreateTime");
            return (Criteria) this;
        }

        public Criteria andRecreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("recreateTime >=", value, "recreateTime");
            return (Criteria) this;
        }

        public Criteria andRecreateTimeLessThan(Date value) {
            addCriterion("recreateTime <", value, "recreateTime");
            return (Criteria) this;
        }

        public Criteria andRecreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("recreateTime <=", value, "recreateTime");
            return (Criteria) this;
        }

        public Criteria andRecreateTimeIn(List<Date> values) {
            addCriterion("recreateTime in", values, "recreateTime");
            return (Criteria) this;
        }

        public Criteria andRecreateTimeNotIn(List<Date> values) {
            addCriterion("recreateTime not in", values, "recreateTime");
            return (Criteria) this;
        }

        public Criteria andRecreateTimeBetween(Date value1, Date value2) {
            addCriterion("recreateTime between", value1, value2, "recreateTime");
            return (Criteria) this;
        }

        public Criteria andRecreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("recreateTime not between", value1, value2, "recreateTime");
            return (Criteria) this;
        }

        public Criteria andDeletedIsNull() {
            addCriterion("deleted is null");
            return (Criteria) this;
        }

        public Criteria andDeletedIsNotNull() {
            addCriterion("deleted is not null");
            return (Criteria) this;
        }

        public Criteria andDeletedEqualTo(Boolean value) {
            addCriterion("deleted =", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedNotEqualTo(Boolean value) {
            addCriterion("deleted <>", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedGreaterThan(Boolean value) {
            addCriterion("deleted >", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedGreaterThanOrEqualTo(Boolean value) {
            addCriterion("deleted >=", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedLessThan(Boolean value) {
            addCriterion("deleted <", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedLessThanOrEqualTo(Boolean value) {
            addCriterion("deleted <=", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedIn(List<Boolean> values) {
            addCriterion("deleted in", values, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedNotIn(List<Boolean> values) {
            addCriterion("deleted not in", values, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedBetween(Boolean value1, Boolean value2) {
            addCriterion("deleted between", value1, value2, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedNotBetween(Boolean value1, Boolean value2) {
            addCriterion("deleted not between", value1, value2, "deleted");
            return (Criteria) this;
        }

        public Criteria andDelicerScoreIsNull() {
            addCriterion("delicerScore is null");
            return (Criteria) this;
        }

        public Criteria andDelicerScoreIsNotNull() {
            addCriterion("delicerScore is not null");
            return (Criteria) this;
        }

        public Criteria andDelicerScoreEqualTo(Double value) {
            addCriterion("delicerScore =", value, "delicerScore");
            return (Criteria) this;
        }

        public Criteria andDelicerScoreNotEqualTo(Double value) {
            addCriterion("delicerScore <>", value, "delicerScore");
            return (Criteria) this;
        }

        public Criteria andDelicerScoreGreaterThan(Double value) {
            addCriterion("delicerScore >", value, "delicerScore");
            return (Criteria) this;
        }

        public Criteria andDelicerScoreGreaterThanOrEqualTo(Double value) {
            addCriterion("delicerScore >=", value, "delicerScore");
            return (Criteria) this;
        }

        public Criteria andDelicerScoreLessThan(Double value) {
            addCriterion("delicerScore <", value, "delicerScore");
            return (Criteria) this;
        }

        public Criteria andDelicerScoreLessThanOrEqualTo(Double value) {
            addCriterion("delicerScore <=", value, "delicerScore");
            return (Criteria) this;
        }

        public Criteria andDelicerScoreIn(List<Double> values) {
            addCriterion("delicerScore in", values, "delicerScore");
            return (Criteria) this;
        }

        public Criteria andDelicerScoreNotIn(List<Double> values) {
            addCriterion("delicerScore not in", values, "delicerScore");
            return (Criteria) this;
        }

        public Criteria andDelicerScoreBetween(Double value1, Double value2) {
            addCriterion("delicerScore between", value1, value2, "delicerScore");
            return (Criteria) this;
        }

        public Criteria andDelicerScoreNotBetween(Double value1, Double value2) {
            addCriterion("delicerScore not between", value1, value2, "delicerScore");
            return (Criteria) this;
        }

        public Criteria andTasteScoreIsNull() {
            addCriterion("tasteScore is null");
            return (Criteria) this;
        }

        public Criteria andTasteScoreIsNotNull() {
            addCriterion("tasteScore is not null");
            return (Criteria) this;
        }

        public Criteria andTasteScoreEqualTo(Integer value) {
            addCriterion("tasteScore =", value, "tasteScore");
            return (Criteria) this;
        }

        public Criteria andTasteScoreNotEqualTo(Integer value) {
            addCriterion("tasteScore <>", value, "tasteScore");
            return (Criteria) this;
        }

        public Criteria andTasteScoreGreaterThan(Integer value) {
            addCriterion("tasteScore >", value, "tasteScore");
            return (Criteria) this;
        }

        public Criteria andTasteScoreGreaterThanOrEqualTo(Integer value) {
            addCriterion("tasteScore >=", value, "tasteScore");
            return (Criteria) this;
        }

        public Criteria andTasteScoreLessThan(Integer value) {
            addCriterion("tasteScore <", value, "tasteScore");
            return (Criteria) this;
        }

        public Criteria andTasteScoreLessThanOrEqualTo(Integer value) {
            addCriterion("tasteScore <=", value, "tasteScore");
            return (Criteria) this;
        }

        public Criteria andTasteScoreIn(List<Integer> values) {
            addCriterion("tasteScore in", values, "tasteScore");
            return (Criteria) this;
        }

        public Criteria andTasteScoreNotIn(List<Integer> values) {
            addCriterion("tasteScore not in", values, "tasteScore");
            return (Criteria) this;
        }

        public Criteria andTasteScoreBetween(Integer value1, Integer value2) {
            addCriterion("tasteScore between", value1, value2, "tasteScore");
            return (Criteria) this;
        }

        public Criteria andTasteScoreNotBetween(Integer value1, Integer value2) {
            addCriterion("tasteScore not between", value1, value2, "tasteScore");
            return (Criteria) this;
        }

        public Criteria andPackagesScoreIsNull() {
            addCriterion("packagesScore is null");
            return (Criteria) this;
        }

        public Criteria andPackagesScoreIsNotNull() {
            addCriterion("packagesScore is not null");
            return (Criteria) this;
        }

        public Criteria andPackagesScoreEqualTo(Integer value) {
            addCriterion("packagesScore =", value, "packagesScore");
            return (Criteria) this;
        }

        public Criteria andPackagesScoreNotEqualTo(Integer value) {
            addCriterion("packagesScore <>", value, "packagesScore");
            return (Criteria) this;
        }

        public Criteria andPackagesScoreGreaterThan(Integer value) {
            addCriterion("packagesScore >", value, "packagesScore");
            return (Criteria) this;
        }

        public Criteria andPackagesScoreGreaterThanOrEqualTo(Integer value) {
            addCriterion("packagesScore >=", value, "packagesScore");
            return (Criteria) this;
        }

        public Criteria andPackagesScoreLessThan(Integer value) {
            addCriterion("packagesScore <", value, "packagesScore");
            return (Criteria) this;
        }

        public Criteria andPackagesScoreLessThanOrEqualTo(Integer value) {
            addCriterion("packagesScore <=", value, "packagesScore");
            return (Criteria) this;
        }

        public Criteria andPackagesScoreIn(List<Integer> values) {
            addCriterion("packagesScore in", values, "packagesScore");
            return (Criteria) this;
        }

        public Criteria andPackagesScoreNotIn(List<Integer> values) {
            addCriterion("packagesScore not in", values, "packagesScore");
            return (Criteria) this;
        }

        public Criteria andPackagesScoreBetween(Integer value1, Integer value2) {
            addCriterion("packagesScore between", value1, value2, "packagesScore");
            return (Criteria) this;
        }

        public Criteria andPackagesScoreNotBetween(Integer value1, Integer value2) {
            addCriterion("packagesScore not between", value1, value2, "packagesScore");
            return (Criteria) this;
        }

        public Criteria andAnonymousIsNull() {
            addCriterion("anonymous is null");
            return (Criteria) this;
        }

        public Criteria andAnonymousIsNotNull() {
            addCriterion("anonymous is not null");
            return (Criteria) this;
        }

        public Criteria andAnonymousEqualTo(Boolean value) {
            addCriterion("anonymous =", value, "anonymous");
            return (Criteria) this;
        }

        public Criteria andAnonymousNotEqualTo(Boolean value) {
            addCriterion("anonymous <>", value, "anonymous");
            return (Criteria) this;
        }

        public Criteria andAnonymousGreaterThan(Boolean value) {
            addCriterion("anonymous >", value, "anonymous");
            return (Criteria) this;
        }

        public Criteria andAnonymousGreaterThanOrEqualTo(Boolean value) {
            addCriterion("anonymous >=", value, "anonymous");
            return (Criteria) this;
        }

        public Criteria andAnonymousLessThan(Boolean value) {
            addCriterion("anonymous <", value, "anonymous");
            return (Criteria) this;
        }

        public Criteria andAnonymousLessThanOrEqualTo(Boolean value) {
            addCriterion("anonymous <=", value, "anonymous");
            return (Criteria) this;
        }

        public Criteria andAnonymousIn(List<Boolean> values) {
            addCriterion("anonymous in", values, "anonymous");
            return (Criteria) this;
        }

        public Criteria andAnonymousNotIn(List<Boolean> values) {
            addCriterion("anonymous not in", values, "anonymous");
            return (Criteria) this;
        }

        public Criteria andAnonymousBetween(Boolean value1, Boolean value2) {
            addCriterion("anonymous between", value1, value2, "anonymous");
            return (Criteria) this;
        }

        public Criteria andAnonymousNotBetween(Boolean value1, Boolean value2) {
            addCriterion("anonymous not between", value1, value2, "anonymous");
            return (Criteria) this;
        }

        public Criteria andDriverSatisfyIsNull() {
            addCriterion("driverSatisfy is null");
            return (Criteria) this;
        }

        public Criteria andDriverSatisfyIsNotNull() {
            addCriterion("driverSatisfy is not null");
            return (Criteria) this;
        }

        public Criteria andDriverSatisfyEqualTo(Boolean value) {
            addCriterion("driverSatisfy =", value, "driverSatisfy");
            return (Criteria) this;
        }

        public Criteria andDriverSatisfyNotEqualTo(Boolean value) {
            addCriterion("driverSatisfy <>", value, "driverSatisfy");
            return (Criteria) this;
        }

        public Criteria andDriverSatisfyGreaterThan(Boolean value) {
            addCriterion("driverSatisfy >", value, "driverSatisfy");
            return (Criteria) this;
        }

        public Criteria andDriverSatisfyGreaterThanOrEqualTo(Boolean value) {
            addCriterion("driverSatisfy >=", value, "driverSatisfy");
            return (Criteria) this;
        }

        public Criteria andDriverSatisfyLessThan(Boolean value) {
            addCriterion("driverSatisfy <", value, "driverSatisfy");
            return (Criteria) this;
        }

        public Criteria andDriverSatisfyLessThanOrEqualTo(Boolean value) {
            addCriterion("driverSatisfy <=", value, "driverSatisfy");
            return (Criteria) this;
        }

        public Criteria andDriverSatisfyIn(List<Boolean> values) {
            addCriterion("driverSatisfy in", values, "driverSatisfy");
            return (Criteria) this;
        }

        public Criteria andDriverSatisfyNotIn(List<Boolean> values) {
            addCriterion("driverSatisfy not in", values, "driverSatisfy");
            return (Criteria) this;
        }

        public Criteria andDriverSatisfyBetween(Boolean value1, Boolean value2) {
            addCriterion("driverSatisfy between", value1, value2, "driverSatisfy");
            return (Criteria) this;
        }

        public Criteria andDriverSatisfyNotBetween(Boolean value1, Boolean value2) {
            addCriterion("driverSatisfy not between", value1, value2, "driverSatisfy");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}