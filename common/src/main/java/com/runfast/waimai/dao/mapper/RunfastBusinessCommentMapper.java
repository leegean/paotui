package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastBusinessComment;
import com.runfast.waimai.dao.model.RunfastBusinessCommentExample;
import com.runfast.waimai.web.dto.UserBusinessCommentDto;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

@Mapper
public interface RunfastBusinessCommentMapper extends IMapper<RunfastBusinessComment, Integer, RunfastBusinessCommentExample> {
    Map<String,Object> businessCommentStat(Integer businessId, Integer userId);

    List<RunfastBusinessComment> find(Integer businessId, Integer userId, Pageable pageable);

    List<UserBusinessCommentDto> findUserComment(Integer userId);
}