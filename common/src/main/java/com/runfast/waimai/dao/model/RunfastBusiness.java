package com.runfast.waimai.dao.model;

import com.runfast.waimai.web.dto.ActivityDto;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class RunfastBusiness implements Serializable {
    private Integer id;

    private String address;

    private String cityId;

    private String cityName;

    private String content;

    private String countyId;

    private String countyName;

    private Date createTime;

    private String imgPath;

    private String latitude;

    private Integer levelId;

    private String levelName;

    private String longitude;

    private String mobile;

    private String name;

    private String saleDayTime;

    private String saleRange;

    private String saleTime;

    private String townId;

    private String townName;

    private Integer typeId;

    private String typeName;

    private Integer salesnum;

    private Integer sort;

    private BigDecimal startPay;

    private Date endwork;

    private Date startwork;

    private String worktoday;

    private BigDecimal packing;

    private Integer isDeliver;

    private Integer recommend;

    private Integer speed;

    private Integer status;

    private Date endTime1;

    private Date endTime2;

    private Date startTime1;

    private Date startTime2;

    private Double coefficient;

    private BigDecimal minmonety;

    private Integer period;

    private String account;

    private BigDecimal showps;

    private BigDecimal busshowps;

    private Date endwork2;

    private Date startwork2;

    private Integer statu;

    private Integer agentId;

    private String agentName;

    private Integer statusx;

    private Integer distributionTime;

    private Integer packTime;

    private Integer bank;

    private String establishbank;

    private String establishname;

    private String mini_imgPath;

    private Integer isopen;

    private BigDecimal baseCharge;

    private Integer isCharge;

    private String code;

    private String typestr;

    private Integer teamid;

    private String teamname;

    private Integer visitnum;

    private Integer issubsidy;

    private BigDecimal subsidy;

    private Boolean goldBusiness;

    private Boolean autoPrint;

    private Boolean autoTaking;

    /**
     * 是否优先排名
     */
    private Boolean priority;

    /**
     * 外卖负责人手机号
     */
    private String wm_mobile;

    private String face_image;

    private String inner_image;

    /**
     * 商家获得评论总分
     */
    private Integer score;

    /**
     * 主体资质：暂无（null或者0）,有（1）
     */
    private Integer mainCert;

    /**
     * 主体资质照片
     */
    private String mainImg;

    /**
     * 主体资质单位名称
     */
    private String mainOrgName;

    /**
     * 主体资质反对负责人
     */
    private String mainLeader;

    /**
     * 注册号
     */
    private String registerNum;

    /**
     * 注册地址
     */
    private String registerAddress;

    /**
     * 有效期
     */
    private Date mainExpirationDate;

    /**
     * 行业资质：暂无（null或者0）,有（1）
     */
    private Integer tradeCert;

    /**
     * 行业照片
     */
    private String tradeImg;

    /**
     * 单位名称
     */
    private String tradeOrgName;

    /**
     * 法定负责人
     */
    private String tradeLeader;

    /**
     * 许可证编号
     */
    private String licenseNum;

    /**
     * 许可证地址
     */
    private String licenseAddress;

    /**
     * 有效期
     */
    private Date tradeExpirationDate;

    /**
     * 是否支持自取（1：自取）
     */
    private Boolean suportSelf;

    /**
     * 老板姓名:对应页面：联系人姓名
     */
    private String boss_name;

    /**
     * 待完善的完成了第一个步,0完成，1待完善
     */
    private Boolean undone;

    /**
     * 营业结束时间3
     */
    private Date endwork3;

    /**
     * 营业开始时间3
     */
    private Date startwork3;

    /**
     * 是否绑定银行卡：false未绑定，true已绑定
     */
    private Boolean binding_bank;

    /**
     * 金牌商家标签的过期时间（15天）
     */
    private Date expire_time;

    /**
     * 省份id
     */
    private String province_id;

    /**
     * 省份名称
     */
    private String province_name;

    /**
     * 主分类id
     */
    private Integer main_class_id;

    /**
     * 次分类id1
     */
    private Integer sub_class_id1;

    /**
     * 次分类id2
     */
    private Integer sub_class_id2;

    /**
     * 1:支持预定
     */
    private Boolean bookable;

    /**
     * 预定时间
     */
    private Date bookTime;

    /**
     * 0或null为折后抽点，1折前抽点（默认折后抽点）
     */
    private Integer drawMode;

    private String branchName;

    private static final long serialVersionUID = 1L;

}