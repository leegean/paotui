package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastBusinessBusnameauditing;
import com.runfast.waimai.dao.model.RunfastBusinessBusnameauditingExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastBusinessBusnameauditingMapper extends IMapper<RunfastBusinessBusnameauditing, Integer, RunfastBusinessBusnameauditingExample> {
}