package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.BaseUser;
import com.runfast.waimai.dao.model.BaseUserExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BaseUserMapper extends IMapper<BaseUser, Integer, BaseUserExample> {
}