package com.runfast.waimai.dao.model;

import java.io.Serializable;

public class RunfastBusinessHasType implements Serializable {
    private Integer id;

    /**
     * 一级级分类id
     */
    private Integer firstId;

    /**
     * 二级级分类id
     */
    private Integer secondId;

    /**
     * 商家id
     */
    private Integer businessId;

    /**
     * 分类类型（0：主分类；1：次分类）
     */
    private Integer type;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public RunfastBusinessHasType withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFirstId() {
        return firstId;
    }

    public RunfastBusinessHasType withFirstId(Integer firstId) {
        this.setFirstId(firstId);
        return this;
    }

    public void setFirstId(Integer firstId) {
        this.firstId = firstId;
    }

    public Integer getSecondId() {
        return secondId;
    }

    public RunfastBusinessHasType withSecondId(Integer secondId) {
        this.setSecondId(secondId);
        return this;
    }

    public void setSecondId(Integer secondId) {
        this.secondId = secondId;
    }

    public Integer getBusinessId() {
        return businessId;
    }

    public RunfastBusinessHasType withBusinessId(Integer businessId) {
        this.setBusinessId(businessId);
        return this;
    }

    public void setBusinessId(Integer businessId) {
        this.businessId = businessId;
    }

    public Integer getType() {
        return type;
    }

    public RunfastBusinessHasType withType(Integer type) {
        this.setType(type);
        return this;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", firstId=").append(firstId);
        sb.append(", secondId=").append(secondId);
        sb.append(", businessId=").append(businessId);
        sb.append(", type=").append(type);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}