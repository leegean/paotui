package com.runfast.waimai.dao.model;

import java.io.Serializable;
import java.util.Date;

public class BasePurview implements Serializable {
    private Integer pid;

    private Integer byid;

    private Date createtime;

    private String link;

    private String pname;

    private Integer ptype;

    private Integer sort;

    private String target;

    private Integer showType;

    private String info;

    private static final long serialVersionUID = 1L;

    public Integer getPid() {
        return pid;
    }

    public BasePurview withPid(Integer pid) {
        this.setPid(pid);
        return this;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public Integer getByid() {
        return byid;
    }

    public BasePurview withByid(Integer byid) {
        this.setByid(byid);
        return this;
    }

    public void setByid(Integer byid) {
        this.byid = byid;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public BasePurview withCreatetime(Date createtime) {
        this.setCreatetime(createtime);
        return this;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public String getLink() {
        return link;
    }

    public BasePurview withLink(String link) {
        this.setLink(link);
        return this;
    }

    public void setLink(String link) {
        this.link = link == null ? null : link.trim();
    }

    public String getPname() {
        return pname;
    }

    public BasePurview withPname(String pname) {
        this.setPname(pname);
        return this;
    }

    public void setPname(String pname) {
        this.pname = pname == null ? null : pname.trim();
    }

    public Integer getPtype() {
        return ptype;
    }

    public BasePurview withPtype(Integer ptype) {
        this.setPtype(ptype);
        return this;
    }

    public void setPtype(Integer ptype) {
        this.ptype = ptype;
    }

    public Integer getSort() {
        return sort;
    }

    public BasePurview withSort(Integer sort) {
        this.setSort(sort);
        return this;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getTarget() {
        return target;
    }

    public BasePurview withTarget(String target) {
        this.setTarget(target);
        return this;
    }

    public void setTarget(String target) {
        this.target = target == null ? null : target.trim();
    }

    public Integer getShowType() {
        return showType;
    }

    public BasePurview withShowType(Integer showType) {
        this.setShowType(showType);
        return this;
    }

    public void setShowType(Integer showType) {
        this.showType = showType;
    }

    public String getInfo() {
        return info;
    }

    public BasePurview withInfo(String info) {
        this.setInfo(info);
        return this;
    }

    public void setInfo(String info) {
        this.info = info == null ? null : info.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", pid=").append(pid);
        sb.append(", byid=").append(byid);
        sb.append(", createtime=").append(createtime);
        sb.append(", link=").append(link);
        sb.append(", pname=").append(pname);
        sb.append(", ptype=").append(ptype);
        sb.append(", sort=").append(sort);
        sb.append(", target=").append(target);
        sb.append(", showType=").append(showType);
        sb.append(", info=").append(info);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}