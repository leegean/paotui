package com.runfast.waimai.dao.model;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class RunfastRedPacketRecord implements Serializable {
    private Integer id;

    /**
     * 领取人
     */
    private Integer userId;

    /**
     * 领取时间
     */
    private Date createTime;

    /**
     * 红包活动id
     */
    private Integer redId;

    /**
     * 可以使用天数
     */
    private Date endTime;

    private Boolean used;

    private static final long serialVersionUID = 1L;

    /**
     * 红包可使用金额(针对随机金额)
     */
    private Double less;

    private Double full;

}