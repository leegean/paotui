package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastBusinessalliance;
import com.runfast.waimai.dao.model.RunfastBusinessallianceExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastBusinessallianceMapper extends IMapper<RunfastBusinessalliance, Integer, RunfastBusinessallianceExample> {
}