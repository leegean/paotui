package com.runfast.waimai.dao.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class RunfastShoppingtrolleyExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public RunfastShoppingtrolleyExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andBarCodeIsNull() {
            addCriterion("barCode is null");
            return (Criteria) this;
        }

        public Criteria andBarCodeIsNotNull() {
            addCriterion("barCode is not null");
            return (Criteria) this;
        }

        public Criteria andBarCodeEqualTo(String value) {
            addCriterion("barCode =", value, "barCode");
            return (Criteria) this;
        }

        public Criteria andBarCodeNotEqualTo(String value) {
            addCriterion("barCode <>", value, "barCode");
            return (Criteria) this;
        }

        public Criteria andBarCodeGreaterThan(String value) {
            addCriterion("barCode >", value, "barCode");
            return (Criteria) this;
        }

        public Criteria andBarCodeGreaterThanOrEqualTo(String value) {
            addCriterion("barCode >=", value, "barCode");
            return (Criteria) this;
        }

        public Criteria andBarCodeLessThan(String value) {
            addCriterion("barCode <", value, "barCode");
            return (Criteria) this;
        }

        public Criteria andBarCodeLessThanOrEqualTo(String value) {
            addCriterion("barCode <=", value, "barCode");
            return (Criteria) this;
        }

        public Criteria andBarCodeLike(String value) {
            addCriterion("barCode like", value, "barCode");
            return (Criteria) this;
        }

        public Criteria andBarCodeNotLike(String value) {
            addCriterion("barCode not like", value, "barCode");
            return (Criteria) this;
        }

        public Criteria andBarCodeIn(List<String> values) {
            addCriterion("barCode in", values, "barCode");
            return (Criteria) this;
        }

        public Criteria andBarCodeNotIn(List<String> values) {
            addCriterion("barCode not in", values, "barCode");
            return (Criteria) this;
        }

        public Criteria andBarCodeBetween(String value1, String value2) {
            addCriterion("barCode between", value1, value2, "barCode");
            return (Criteria) this;
        }

        public Criteria andBarCodeNotBetween(String value1, String value2) {
            addCriterion("barCode not between", value1, value2, "barCode");
            return (Criteria) this;
        }

        public Criteria andBusinessIdIsNull() {
            addCriterion("businessId is null");
            return (Criteria) this;
        }

        public Criteria andBusinessIdIsNotNull() {
            addCriterion("businessId is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessIdEqualTo(Integer value) {
            addCriterion("businessId =", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdNotEqualTo(Integer value) {
            addCriterion("businessId <>", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdGreaterThan(Integer value) {
            addCriterion("businessId >", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("businessId >=", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdLessThan(Integer value) {
            addCriterion("businessId <", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdLessThanOrEqualTo(Integer value) {
            addCriterion("businessId <=", value, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdIn(List<Integer> values) {
            addCriterion("businessId in", values, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdNotIn(List<Integer> values) {
            addCriterion("businessId not in", values, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdBetween(Integer value1, Integer value2) {
            addCriterion("businessId between", value1, value2, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessIdNotBetween(Integer value1, Integer value2) {
            addCriterion("businessId not between", value1, value2, "businessId");
            return (Criteria) this;
        }

        public Criteria andBusinessNameIsNull() {
            addCriterion("businessName is null");
            return (Criteria) this;
        }

        public Criteria andBusinessNameIsNotNull() {
            addCriterion("businessName is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessNameEqualTo(String value) {
            addCriterion("businessName =", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameNotEqualTo(String value) {
            addCriterion("businessName <>", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameGreaterThan(String value) {
            addCriterion("businessName >", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameGreaterThanOrEqualTo(String value) {
            addCriterion("businessName >=", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameLessThan(String value) {
            addCriterion("businessName <", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameLessThanOrEqualTo(String value) {
            addCriterion("businessName <=", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameLike(String value) {
            addCriterion("businessName like", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameNotLike(String value) {
            addCriterion("businessName not like", value, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameIn(List<String> values) {
            addCriterion("businessName in", values, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameNotIn(List<String> values) {
            addCriterion("businessName not in", values, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameBetween(String value1, String value2) {
            addCriterion("businessName between", value1, value2, "businessName");
            return (Criteria) this;
        }

        public Criteria andBusinessNameNotBetween(String value1, String value2) {
            addCriterion("businessName not between", value1, value2, "businessName");
            return (Criteria) this;
        }

        public Criteria andCidIsNull() {
            addCriterion("cid is null");
            return (Criteria) this;
        }

        public Criteria andCidIsNotNull() {
            addCriterion("cid is not null");
            return (Criteria) this;
        }

        public Criteria andCidEqualTo(Integer value) {
            addCriterion("cid =", value, "cid");
            return (Criteria) this;
        }

        public Criteria andCidNotEqualTo(Integer value) {
            addCriterion("cid <>", value, "cid");
            return (Criteria) this;
        }

        public Criteria andCidGreaterThan(Integer value) {
            addCriterion("cid >", value, "cid");
            return (Criteria) this;
        }

        public Criteria andCidGreaterThanOrEqualTo(Integer value) {
            addCriterion("cid >=", value, "cid");
            return (Criteria) this;
        }

        public Criteria andCidLessThan(Integer value) {
            addCriterion("cid <", value, "cid");
            return (Criteria) this;
        }

        public Criteria andCidLessThanOrEqualTo(Integer value) {
            addCriterion("cid <=", value, "cid");
            return (Criteria) this;
        }

        public Criteria andCidIn(List<Integer> values) {
            addCriterion("cid in", values, "cid");
            return (Criteria) this;
        }

        public Criteria andCidNotIn(List<Integer> values) {
            addCriterion("cid not in", values, "cid");
            return (Criteria) this;
        }

        public Criteria andCidBetween(Integer value1, Integer value2) {
            addCriterion("cid between", value1, value2, "cid");
            return (Criteria) this;
        }

        public Criteria andCidNotBetween(Integer value1, Integer value2) {
            addCriterion("cid not between", value1, value2, "cid");
            return (Criteria) this;
        }

        public Criteria andCnameIsNull() {
            addCriterion("cname is null");
            return (Criteria) this;
        }

        public Criteria andCnameIsNotNull() {
            addCriterion("cname is not null");
            return (Criteria) this;
        }

        public Criteria andCnameEqualTo(String value) {
            addCriterion("cname =", value, "cname");
            return (Criteria) this;
        }

        public Criteria andCnameNotEqualTo(String value) {
            addCriterion("cname <>", value, "cname");
            return (Criteria) this;
        }

        public Criteria andCnameGreaterThan(String value) {
            addCriterion("cname >", value, "cname");
            return (Criteria) this;
        }

        public Criteria andCnameGreaterThanOrEqualTo(String value) {
            addCriterion("cname >=", value, "cname");
            return (Criteria) this;
        }

        public Criteria andCnameLessThan(String value) {
            addCriterion("cname <", value, "cname");
            return (Criteria) this;
        }

        public Criteria andCnameLessThanOrEqualTo(String value) {
            addCriterion("cname <=", value, "cname");
            return (Criteria) this;
        }

        public Criteria andCnameLike(String value) {
            addCriterion("cname like", value, "cname");
            return (Criteria) this;
        }

        public Criteria andCnameNotLike(String value) {
            addCriterion("cname not like", value, "cname");
            return (Criteria) this;
        }

        public Criteria andCnameIn(List<String> values) {
            addCriterion("cname in", values, "cname");
            return (Criteria) this;
        }

        public Criteria andCnameNotIn(List<String> values) {
            addCriterion("cname not in", values, "cname");
            return (Criteria) this;
        }

        public Criteria andCnameBetween(String value1, String value2) {
            addCriterion("cname between", value1, value2, "cname");
            return (Criteria) this;
        }

        public Criteria andCnameNotBetween(String value1, String value2) {
            addCriterion("cname not between", value1, value2, "cname");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdIsNull() {
            addCriterion("goodsSellId is null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdIsNotNull() {
            addCriterion("goodsSellId is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdEqualTo(Integer value) {
            addCriterion("goodsSellId =", value, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdNotEqualTo(Integer value) {
            addCriterion("goodsSellId <>", value, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdGreaterThan(Integer value) {
            addCriterion("goodsSellId >", value, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("goodsSellId >=", value, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdLessThan(Integer value) {
            addCriterion("goodsSellId <", value, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdLessThanOrEqualTo(Integer value) {
            addCriterion("goodsSellId <=", value, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdIn(List<Integer> values) {
            addCriterion("goodsSellId in", values, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdNotIn(List<Integer> values) {
            addCriterion("goodsSellId not in", values, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdBetween(Integer value1, Integer value2) {
            addCriterion("goodsSellId between", value1, value2, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellIdNotBetween(Integer value1, Integer value2) {
            addCriterion("goodsSellId not between", value1, value2, "goodsSellId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameIsNull() {
            addCriterion("goodsSellName is null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameIsNotNull() {
            addCriterion("goodsSellName is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameEqualTo(String value) {
            addCriterion("goodsSellName =", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameNotEqualTo(String value) {
            addCriterion("goodsSellName <>", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameGreaterThan(String value) {
            addCriterion("goodsSellName >", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameGreaterThanOrEqualTo(String value) {
            addCriterion("goodsSellName >=", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameLessThan(String value) {
            addCriterion("goodsSellName <", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameLessThanOrEqualTo(String value) {
            addCriterion("goodsSellName <=", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameLike(String value) {
            addCriterion("goodsSellName like", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameNotLike(String value) {
            addCriterion("goodsSellName not like", value, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameIn(List<String> values) {
            addCriterion("goodsSellName in", values, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameNotIn(List<String> values) {
            addCriterion("goodsSellName not in", values, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameBetween(String value1, String value2) {
            addCriterion("goodsSellName between", value1, value2, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellNameNotBetween(String value1, String value2) {
            addCriterion("goodsSellName not between", value1, value2, "goodsSellName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionIdIsNull() {
            addCriterion("goodsSellOptionId is null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionIdIsNotNull() {
            addCriterion("goodsSellOptionId is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionIdEqualTo(Integer value) {
            addCriterion("goodsSellOptionId =", value, "goodsSellOptionId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionIdNotEqualTo(Integer value) {
            addCriterion("goodsSellOptionId <>", value, "goodsSellOptionId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionIdGreaterThan(Integer value) {
            addCriterion("goodsSellOptionId >", value, "goodsSellOptionId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("goodsSellOptionId >=", value, "goodsSellOptionId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionIdLessThan(Integer value) {
            addCriterion("goodsSellOptionId <", value, "goodsSellOptionId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionIdLessThanOrEqualTo(Integer value) {
            addCriterion("goodsSellOptionId <=", value, "goodsSellOptionId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionIdIn(List<Integer> values) {
            addCriterion("goodsSellOptionId in", values, "goodsSellOptionId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionIdNotIn(List<Integer> values) {
            addCriterion("goodsSellOptionId not in", values, "goodsSellOptionId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionIdBetween(Integer value1, Integer value2) {
            addCriterion("goodsSellOptionId between", value1, value2, "goodsSellOptionId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionIdNotBetween(Integer value1, Integer value2) {
            addCriterion("goodsSellOptionId not between", value1, value2, "goodsSellOptionId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionNameIsNull() {
            addCriterion("goodsSellOptionName is null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionNameIsNotNull() {
            addCriterion("goodsSellOptionName is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionNameEqualTo(String value) {
            addCriterion("goodsSellOptionName =", value, "goodsSellOptionName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionNameNotEqualTo(String value) {
            addCriterion("goodsSellOptionName <>", value, "goodsSellOptionName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionNameGreaterThan(String value) {
            addCriterion("goodsSellOptionName >", value, "goodsSellOptionName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionNameGreaterThanOrEqualTo(String value) {
            addCriterion("goodsSellOptionName >=", value, "goodsSellOptionName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionNameLessThan(String value) {
            addCriterion("goodsSellOptionName <", value, "goodsSellOptionName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionNameLessThanOrEqualTo(String value) {
            addCriterion("goodsSellOptionName <=", value, "goodsSellOptionName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionNameLike(String value) {
            addCriterion("goodsSellOptionName like", value, "goodsSellOptionName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionNameNotLike(String value) {
            addCriterion("goodsSellOptionName not like", value, "goodsSellOptionName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionNameIn(List<String> values) {
            addCriterion("goodsSellOptionName in", values, "goodsSellOptionName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionNameNotIn(List<String> values) {
            addCriterion("goodsSellOptionName not in", values, "goodsSellOptionName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionNameBetween(String value1, String value2) {
            addCriterion("goodsSellOptionName between", value1, value2, "goodsSellOptionName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellOptionNameNotBetween(String value1, String value2) {
            addCriterion("goodsSellOptionName not between", value1, value2, "goodsSellOptionName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardIdIsNull() {
            addCriterion("goodsSellStandardId is null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardIdIsNotNull() {
            addCriterion("goodsSellStandardId is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardIdEqualTo(Integer value) {
            addCriterion("goodsSellStandardId =", value, "goodsSellStandardId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardIdNotEqualTo(Integer value) {
            addCriterion("goodsSellStandardId <>", value, "goodsSellStandardId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardIdGreaterThan(Integer value) {
            addCriterion("goodsSellStandardId >", value, "goodsSellStandardId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("goodsSellStandardId >=", value, "goodsSellStandardId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardIdLessThan(Integer value) {
            addCriterion("goodsSellStandardId <", value, "goodsSellStandardId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardIdLessThanOrEqualTo(Integer value) {
            addCriterion("goodsSellStandardId <=", value, "goodsSellStandardId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardIdIn(List<Integer> values) {
            addCriterion("goodsSellStandardId in", values, "goodsSellStandardId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardIdNotIn(List<Integer> values) {
            addCriterion("goodsSellStandardId not in", values, "goodsSellStandardId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardIdBetween(Integer value1, Integer value2) {
            addCriterion("goodsSellStandardId between", value1, value2, "goodsSellStandardId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardIdNotBetween(Integer value1, Integer value2) {
            addCriterion("goodsSellStandardId not between", value1, value2, "goodsSellStandardId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardNameIsNull() {
            addCriterion("goodsSellStandardName is null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardNameIsNotNull() {
            addCriterion("goodsSellStandardName is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardNameEqualTo(String value) {
            addCriterion("goodsSellStandardName =", value, "goodsSellStandardName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardNameNotEqualTo(String value) {
            addCriterion("goodsSellStandardName <>", value, "goodsSellStandardName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardNameGreaterThan(String value) {
            addCriterion("goodsSellStandardName >", value, "goodsSellStandardName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardNameGreaterThanOrEqualTo(String value) {
            addCriterion("goodsSellStandardName >=", value, "goodsSellStandardName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardNameLessThan(String value) {
            addCriterion("goodsSellStandardName <", value, "goodsSellStandardName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardNameLessThanOrEqualTo(String value) {
            addCriterion("goodsSellStandardName <=", value, "goodsSellStandardName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardNameLike(String value) {
            addCriterion("goodsSellStandardName like", value, "goodsSellStandardName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardNameNotLike(String value) {
            addCriterion("goodsSellStandardName not like", value, "goodsSellStandardName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardNameIn(List<String> values) {
            addCriterion("goodsSellStandardName in", values, "goodsSellStandardName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardNameNotIn(List<String> values) {
            addCriterion("goodsSellStandardName not in", values, "goodsSellStandardName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardNameBetween(String value1, String value2) {
            addCriterion("goodsSellStandardName between", value1, value2, "goodsSellStandardName");
            return (Criteria) this;
        }

        public Criteria andGoodsSellStandardNameNotBetween(String value1, String value2) {
            addCriterion("goodsSellStandardName not between", value1, value2, "goodsSellStandardName");
            return (Criteria) this;
        }

        public Criteria andNumIsNull() {
            addCriterion("num is null");
            return (Criteria) this;
        }

        public Criteria andNumIsNotNull() {
            addCriterion("num is not null");
            return (Criteria) this;
        }

        public Criteria andNumEqualTo(Integer value) {
            addCriterion("num =", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumNotEqualTo(Integer value) {
            addCriterion("num <>", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumGreaterThan(Integer value) {
            addCriterion("num >", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("num >=", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumLessThan(Integer value) {
            addCriterion("num <", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumLessThanOrEqualTo(Integer value) {
            addCriterion("num <=", value, "num");
            return (Criteria) this;
        }

        public Criteria andNumIn(List<Integer> values) {
            addCriterion("num in", values, "num");
            return (Criteria) this;
        }

        public Criteria andNumNotIn(List<Integer> values) {
            addCriterion("num not in", values, "num");
            return (Criteria) this;
        }

        public Criteria andNumBetween(Integer value1, Integer value2) {
            addCriterion("num between", value1, value2, "num");
            return (Criteria) this;
        }

        public Criteria andNumNotBetween(Integer value1, Integer value2) {
            addCriterion("num not between", value1, value2, "num");
            return (Criteria) this;
        }

        public Criteria andPriceIsNull() {
            addCriterion("price is null");
            return (Criteria) this;
        }

        public Criteria andPriceIsNotNull() {
            addCriterion("price is not null");
            return (Criteria) this;
        }

        public Criteria andPriceEqualTo(Double value) {
            addCriterion("price =", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotEqualTo(Double value) {
            addCriterion("price <>", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceGreaterThan(Double value) {
            addCriterion("price >", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceGreaterThanOrEqualTo(Double value) {
            addCriterion("price >=", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceLessThan(Double value) {
            addCriterion("price <", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceLessThanOrEqualTo(Double value) {
            addCriterion("price <=", value, "price");
            return (Criteria) this;
        }

        public Criteria andPriceIn(List<Double> values) {
            addCriterion("price in", values, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotIn(List<Double> values) {
            addCriterion("price not in", values, "price");
            return (Criteria) this;
        }

        public Criteria andPriceBetween(Double value1, Double value2) {
            addCriterion("price between", value1, value2, "price");
            return (Criteria) this;
        }

        public Criteria andPriceNotBetween(Double value1, Double value2) {
            addCriterion("price not between", value1, value2, "price");
            return (Criteria) this;
        }

        public Criteria andOpenidIsNull() {
            addCriterion("openid is null");
            return (Criteria) this;
        }

        public Criteria andOpenidIsNotNull() {
            addCriterion("openid is not null");
            return (Criteria) this;
        }

        public Criteria andOpenidEqualTo(String value) {
            addCriterion("openid =", value, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidNotEqualTo(String value) {
            addCriterion("openid <>", value, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidGreaterThan(String value) {
            addCriterion("openid >", value, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidGreaterThanOrEqualTo(String value) {
            addCriterion("openid >=", value, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidLessThan(String value) {
            addCriterion("openid <", value, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidLessThanOrEqualTo(String value) {
            addCriterion("openid <=", value, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidLike(String value) {
            addCriterion("openid like", value, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidNotLike(String value) {
            addCriterion("openid not like", value, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidIn(List<String> values) {
            addCriterion("openid in", values, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidNotIn(List<String> values) {
            addCriterion("openid not in", values, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidBetween(String value1, String value2) {
            addCriterion("openid between", value1, value2, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidNotBetween(String value1, String value2) {
            addCriterion("openid not between", value1, value2, "openid");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdIsNull() {
            addCriterion("GoodsSellRecordId is null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdIsNotNull() {
            addCriterion("GoodsSellRecordId is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdEqualTo(Integer value) {
            addCriterion("GoodsSellRecordId =", value, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdNotEqualTo(Integer value) {
            addCriterion("GoodsSellRecordId <>", value, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdGreaterThan(Integer value) {
            addCriterion("GoodsSellRecordId >", value, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("GoodsSellRecordId >=", value, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdLessThan(Integer value) {
            addCriterion("GoodsSellRecordId <", value, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdLessThanOrEqualTo(Integer value) {
            addCriterion("GoodsSellRecordId <=", value, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdIn(List<Integer> values) {
            addCriterion("GoodsSellRecordId in", values, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdNotIn(List<Integer> values) {
            addCriterion("GoodsSellRecordId not in", values, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdBetween(Integer value1, Integer value2) {
            addCriterion("GoodsSellRecordId between", value1, value2, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andGoodsSellRecordIdNotBetween(Integer value1, Integer value2) {
            addCriterion("GoodsSellRecordId not between", value1, value2, "goodsSellRecordId");
            return (Criteria) this;
        }

        public Criteria andSnumIsNull() {
            addCriterion("snum is null");
            return (Criteria) this;
        }

        public Criteria andSnumIsNotNull() {
            addCriterion("snum is not null");
            return (Criteria) this;
        }

        public Criteria andSnumEqualTo(Integer value) {
            addCriterion("snum =", value, "snum");
            return (Criteria) this;
        }

        public Criteria andSnumNotEqualTo(Integer value) {
            addCriterion("snum <>", value, "snum");
            return (Criteria) this;
        }

        public Criteria andSnumGreaterThan(Integer value) {
            addCriterion("snum >", value, "snum");
            return (Criteria) this;
        }

        public Criteria andSnumGreaterThanOrEqualTo(Integer value) {
            addCriterion("snum >=", value, "snum");
            return (Criteria) this;
        }

        public Criteria andSnumLessThan(Integer value) {
            addCriterion("snum <", value, "snum");
            return (Criteria) this;
        }

        public Criteria andSnumLessThanOrEqualTo(Integer value) {
            addCriterion("snum <=", value, "snum");
            return (Criteria) this;
        }

        public Criteria andSnumIn(List<Integer> values) {
            addCriterion("snum in", values, "snum");
            return (Criteria) this;
        }

        public Criteria andSnumNotIn(List<Integer> values) {
            addCriterion("snum not in", values, "snum");
            return (Criteria) this;
        }

        public Criteria andSnumBetween(Integer value1, Integer value2) {
            addCriterion("snum between", value1, value2, "snum");
            return (Criteria) this;
        }

        public Criteria andSnumNotBetween(Integer value1, Integer value2) {
            addCriterion("snum not between", value1, value2, "snum");
            return (Criteria) this;
        }

        public Criteria andPackingIsNull() {
            addCriterion("packing is null");
            return (Criteria) this;
        }

        public Criteria andPackingIsNotNull() {
            addCriterion("packing is not null");
            return (Criteria) this;
        }

        public Criteria andPackingEqualTo(BigDecimal value) {
            addCriterion("packing =", value, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingNotEqualTo(BigDecimal value) {
            addCriterion("packing <>", value, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingGreaterThan(BigDecimal value) {
            addCriterion("packing >", value, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("packing >=", value, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingLessThan(BigDecimal value) {
            addCriterion("packing <", value, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingLessThanOrEqualTo(BigDecimal value) {
            addCriterion("packing <=", value, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingIn(List<BigDecimal> values) {
            addCriterion("packing in", values, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingNotIn(List<BigDecimal> values) {
            addCriterion("packing not in", values, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("packing between", value1, value2, "packing");
            return (Criteria) this;
        }

        public Criteria andPackingNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("packing not between", value1, value2, "packing");
            return (Criteria) this;
        }

        public Criteria andPtypeIsNull() {
            addCriterion("ptype is null");
            return (Criteria) this;
        }

        public Criteria andPtypeIsNotNull() {
            addCriterion("ptype is not null");
            return (Criteria) this;
        }

        public Criteria andPtypeEqualTo(Integer value) {
            addCriterion("ptype =", value, "ptype");
            return (Criteria) this;
        }

        public Criteria andPtypeNotEqualTo(Integer value) {
            addCriterion("ptype <>", value, "ptype");
            return (Criteria) this;
        }

        public Criteria andPtypeGreaterThan(Integer value) {
            addCriterion("ptype >", value, "ptype");
            return (Criteria) this;
        }

        public Criteria andPtypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("ptype >=", value, "ptype");
            return (Criteria) this;
        }

        public Criteria andPtypeLessThan(Integer value) {
            addCriterion("ptype <", value, "ptype");
            return (Criteria) this;
        }

        public Criteria andPtypeLessThanOrEqualTo(Integer value) {
            addCriterion("ptype <=", value, "ptype");
            return (Criteria) this;
        }

        public Criteria andPtypeIn(List<Integer> values) {
            addCriterion("ptype in", values, "ptype");
            return (Criteria) this;
        }

        public Criteria andPtypeNotIn(List<Integer> values) {
            addCriterion("ptype not in", values, "ptype");
            return (Criteria) this;
        }

        public Criteria andPtypeBetween(Integer value1, Integer value2) {
            addCriterion("ptype between", value1, value2, "ptype");
            return (Criteria) this;
        }

        public Criteria andPtypeNotBetween(Integer value1, Integer value2) {
            addCriterion("ptype not between", value1, value2, "ptype");
            return (Criteria) this;
        }

        public Criteria andDispriceIsNull() {
            addCriterion("disprice is null");
            return (Criteria) this;
        }

        public Criteria andDispriceIsNotNull() {
            addCriterion("disprice is not null");
            return (Criteria) this;
        }

        public Criteria andDispriceEqualTo(BigDecimal value) {
            addCriterion("disprice =", value, "disprice");
            return (Criteria) this;
        }

        public Criteria andDispriceNotEqualTo(BigDecimal value) {
            addCriterion("disprice <>", value, "disprice");
            return (Criteria) this;
        }

        public Criteria andDispriceGreaterThan(BigDecimal value) {
            addCriterion("disprice >", value, "disprice");
            return (Criteria) this;
        }

        public Criteria andDispriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("disprice >=", value, "disprice");
            return (Criteria) this;
        }

        public Criteria andDispriceLessThan(BigDecimal value) {
            addCriterion("disprice <", value, "disprice");
            return (Criteria) this;
        }

        public Criteria andDispriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("disprice <=", value, "disprice");
            return (Criteria) this;
        }

        public Criteria andDispriceIn(List<BigDecimal> values) {
            addCriterion("disprice in", values, "disprice");
            return (Criteria) this;
        }

        public Criteria andDispriceNotIn(List<BigDecimal> values) {
            addCriterion("disprice not in", values, "disprice");
            return (Criteria) this;
        }

        public Criteria andDispriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("disprice between", value1, value2, "disprice");
            return (Criteria) this;
        }

        public Criteria andDispriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("disprice not between", value1, value2, "disprice");
            return (Criteria) this;
        }

        public Criteria andPricedisIsNull() {
            addCriterion("pricedis is null");
            return (Criteria) this;
        }

        public Criteria andPricedisIsNotNull() {
            addCriterion("pricedis is not null");
            return (Criteria) this;
        }

        public Criteria andPricedisEqualTo(BigDecimal value) {
            addCriterion("pricedis =", value, "pricedis");
            return (Criteria) this;
        }

        public Criteria andPricedisNotEqualTo(BigDecimal value) {
            addCriterion("pricedis <>", value, "pricedis");
            return (Criteria) this;
        }

        public Criteria andPricedisGreaterThan(BigDecimal value) {
            addCriterion("pricedis >", value, "pricedis");
            return (Criteria) this;
        }

        public Criteria andPricedisGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("pricedis >=", value, "pricedis");
            return (Criteria) this;
        }

        public Criteria andPricedisLessThan(BigDecimal value) {
            addCriterion("pricedis <", value, "pricedis");
            return (Criteria) this;
        }

        public Criteria andPricedisLessThanOrEqualTo(BigDecimal value) {
            addCriterion("pricedis <=", value, "pricedis");
            return (Criteria) this;
        }

        public Criteria andPricedisIn(List<BigDecimal> values) {
            addCriterion("pricedis in", values, "pricedis");
            return (Criteria) this;
        }

        public Criteria andPricedisNotIn(List<BigDecimal> values) {
            addCriterion("pricedis not in", values, "pricedis");
            return (Criteria) this;
        }

        public Criteria andPricedisBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("pricedis between", value1, value2, "pricedis");
            return (Criteria) this;
        }

        public Criteria andPricedisNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("pricedis not between", value1, value2, "pricedis");
            return (Criteria) this;
        }

        public Criteria andOptionIdsIsNull() {
            addCriterion("optionIds is null");
            return (Criteria) this;
        }

        public Criteria andOptionIdsIsNotNull() {
            addCriterion("optionIds is not null");
            return (Criteria) this;
        }

        public Criteria andOptionIdsEqualTo(String value) {
            addCriterion("optionIds =", value, "optionIds");
            return (Criteria) this;
        }

        public Criteria andOptionIdsNotEqualTo(String value) {
            addCriterion("optionIds <>", value, "optionIds");
            return (Criteria) this;
        }

        public Criteria andOptionIdsGreaterThan(String value) {
            addCriterion("optionIds >", value, "optionIds");
            return (Criteria) this;
        }

        public Criteria andOptionIdsGreaterThanOrEqualTo(String value) {
            addCriterion("optionIds >=", value, "optionIds");
            return (Criteria) this;
        }

        public Criteria andOptionIdsLessThan(String value) {
            addCriterion("optionIds <", value, "optionIds");
            return (Criteria) this;
        }

        public Criteria andOptionIdsLessThanOrEqualTo(String value) {
            addCriterion("optionIds <=", value, "optionIds");
            return (Criteria) this;
        }

        public Criteria andOptionIdsLike(String value) {
            addCriterion("optionIds like", value, "optionIds");
            return (Criteria) this;
        }

        public Criteria andOptionIdsNotLike(String value) {
            addCriterion("optionIds not like", value, "optionIds");
            return (Criteria) this;
        }

        public Criteria andOptionIdsIn(List<String> values) {
            addCriterion("optionIds in", values, "optionIds");
            return (Criteria) this;
        }

        public Criteria andOptionIdsNotIn(List<String> values) {
            addCriterion("optionIds not in", values, "optionIds");
            return (Criteria) this;
        }

        public Criteria andOptionIdsBetween(String value1, String value2) {
            addCriterion("optionIds between", value1, value2, "optionIds");
            return (Criteria) this;
        }

        public Criteria andOptionIdsNotBetween(String value1, String value2) {
            addCriterion("optionIds not between", value1, value2, "optionIds");
            return (Criteria) this;
        }

        public Criteria andIslimitedIsNull() {
            addCriterion("islimited is null");
            return (Criteria) this;
        }

        public Criteria andIslimitedIsNotNull() {
            addCriterion("islimited is not null");
            return (Criteria) this;
        }

        public Criteria andIslimitedEqualTo(Integer value) {
            addCriterion("islimited =", value, "islimited");
            return (Criteria) this;
        }

        public Criteria andIslimitedNotEqualTo(Integer value) {
            addCriterion("islimited <>", value, "islimited");
            return (Criteria) this;
        }

        public Criteria andIslimitedGreaterThan(Integer value) {
            addCriterion("islimited >", value, "islimited");
            return (Criteria) this;
        }

        public Criteria andIslimitedGreaterThanOrEqualTo(Integer value) {
            addCriterion("islimited >=", value, "islimited");
            return (Criteria) this;
        }

        public Criteria andIslimitedLessThan(Integer value) {
            addCriterion("islimited <", value, "islimited");
            return (Criteria) this;
        }

        public Criteria andIslimitedLessThanOrEqualTo(Integer value) {
            addCriterion("islimited <=", value, "islimited");
            return (Criteria) this;
        }

        public Criteria andIslimitedIn(List<Integer> values) {
            addCriterion("islimited in", values, "islimited");
            return (Criteria) this;
        }

        public Criteria andIslimitedNotIn(List<Integer> values) {
            addCriterion("islimited not in", values, "islimited");
            return (Criteria) this;
        }

        public Criteria andIslimitedBetween(Integer value1, Integer value2) {
            addCriterion("islimited between", value1, value2, "islimited");
            return (Criteria) this;
        }

        public Criteria andIslimitedNotBetween(Integer value1, Integer value2) {
            addCriterion("islimited not between", value1, value2, "islimited");
            return (Criteria) this;
        }

        public Criteria andLimitNumIsNull() {
            addCriterion("limitNum is null");
            return (Criteria) this;
        }

        public Criteria andLimitNumIsNotNull() {
            addCriterion("limitNum is not null");
            return (Criteria) this;
        }

        public Criteria andLimitNumEqualTo(Integer value) {
            addCriterion("limitNum =", value, "limitNum");
            return (Criteria) this;
        }

        public Criteria andLimitNumNotEqualTo(Integer value) {
            addCriterion("limitNum <>", value, "limitNum");
            return (Criteria) this;
        }

        public Criteria andLimitNumGreaterThan(Integer value) {
            addCriterion("limitNum >", value, "limitNum");
            return (Criteria) this;
        }

        public Criteria andLimitNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("limitNum >=", value, "limitNum");
            return (Criteria) this;
        }

        public Criteria andLimitNumLessThan(Integer value) {
            addCriterion("limitNum <", value, "limitNum");
            return (Criteria) this;
        }

        public Criteria andLimitNumLessThanOrEqualTo(Integer value) {
            addCriterion("limitNum <=", value, "limitNum");
            return (Criteria) this;
        }

        public Criteria andLimitNumIn(List<Integer> values) {
            addCriterion("limitNum in", values, "limitNum");
            return (Criteria) this;
        }

        public Criteria andLimitNumNotIn(List<Integer> values) {
            addCriterion("limitNum not in", values, "limitNum");
            return (Criteria) this;
        }

        public Criteria andLimitNumBetween(Integer value1, Integer value2) {
            addCriterion("limitNum between", value1, value2, "limitNum");
            return (Criteria) this;
        }

        public Criteria andLimitNumNotBetween(Integer value1, Integer value2) {
            addCriterion("limitNum not between", value1, value2, "limitNum");
            return (Criteria) this;
        }

        public Criteria andCouponidIsNull() {
            addCriterion("couponid is null");
            return (Criteria) this;
        }

        public Criteria andCouponidIsNotNull() {
            addCriterion("couponid is not null");
            return (Criteria) this;
        }

        public Criteria andCouponidEqualTo(Integer value) {
            addCriterion("couponid =", value, "couponid");
            return (Criteria) this;
        }

        public Criteria andCouponidNotEqualTo(Integer value) {
            addCriterion("couponid <>", value, "couponid");
            return (Criteria) this;
        }

        public Criteria andCouponidGreaterThan(Integer value) {
            addCriterion("couponid >", value, "couponid");
            return (Criteria) this;
        }

        public Criteria andCouponidGreaterThanOrEqualTo(Integer value) {
            addCriterion("couponid >=", value, "couponid");
            return (Criteria) this;
        }

        public Criteria andCouponidLessThan(Integer value) {
            addCriterion("couponid <", value, "couponid");
            return (Criteria) this;
        }

        public Criteria andCouponidLessThanOrEqualTo(Integer value) {
            addCriterion("couponid <=", value, "couponid");
            return (Criteria) this;
        }

        public Criteria andCouponidIn(List<Integer> values) {
            addCriterion("couponid in", values, "couponid");
            return (Criteria) this;
        }

        public Criteria andCouponidNotIn(List<Integer> values) {
            addCriterion("couponid not in", values, "couponid");
            return (Criteria) this;
        }

        public Criteria andCouponidBetween(Integer value1, Integer value2) {
            addCriterion("couponid between", value1, value2, "couponid");
            return (Criteria) this;
        }

        public Criteria andCouponidNotBetween(Integer value1, Integer value2) {
            addCriterion("couponid not between", value1, value2, "couponid");
            return (Criteria) this;
        }

        public Criteria andFullIsNull() {
            addCriterion("full is null");
            return (Criteria) this;
        }

        public Criteria andFullIsNotNull() {
            addCriterion("full is not null");
            return (Criteria) this;
        }

        public Criteria andFullEqualTo(BigDecimal value) {
            addCriterion("full =", value, "full");
            return (Criteria) this;
        }

        public Criteria andFullNotEqualTo(BigDecimal value) {
            addCriterion("full <>", value, "full");
            return (Criteria) this;
        }

        public Criteria andFullGreaterThan(BigDecimal value) {
            addCriterion("full >", value, "full");
            return (Criteria) this;
        }

        public Criteria andFullGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("full >=", value, "full");
            return (Criteria) this;
        }

        public Criteria andFullLessThan(BigDecimal value) {
            addCriterion("full <", value, "full");
            return (Criteria) this;
        }

        public Criteria andFullLessThanOrEqualTo(BigDecimal value) {
            addCriterion("full <=", value, "full");
            return (Criteria) this;
        }

        public Criteria andFullIn(List<BigDecimal> values) {
            addCriterion("full in", values, "full");
            return (Criteria) this;
        }

        public Criteria andFullNotIn(List<BigDecimal> values) {
            addCriterion("full not in", values, "full");
            return (Criteria) this;
        }

        public Criteria andFullBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("full between", value1, value2, "full");
            return (Criteria) this;
        }

        public Criteria andFullNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("full not between", value1, value2, "full");
            return (Criteria) this;
        }

        public Criteria andTotalIsNull() {
            addCriterion("total is null");
            return (Criteria) this;
        }

        public Criteria andTotalIsNotNull() {
            addCriterion("total is not null");
            return (Criteria) this;
        }

        public Criteria andTotalEqualTo(BigDecimal value) {
            addCriterion("total =", value, "total");
            return (Criteria) this;
        }

        public Criteria andTotalNotEqualTo(BigDecimal value) {
            addCriterion("total <>", value, "total");
            return (Criteria) this;
        }

        public Criteria andTotalGreaterThan(BigDecimal value) {
            addCriterion("total >", value, "total");
            return (Criteria) this;
        }

        public Criteria andTotalGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("total >=", value, "total");
            return (Criteria) this;
        }

        public Criteria andTotalLessThan(BigDecimal value) {
            addCriterion("total <", value, "total");
            return (Criteria) this;
        }

        public Criteria andTotalLessThanOrEqualTo(BigDecimal value) {
            addCriterion("total <=", value, "total");
            return (Criteria) this;
        }

        public Criteria andTotalIn(List<BigDecimal> values) {
            addCriterion("total in", values, "total");
            return (Criteria) this;
        }

        public Criteria andTotalNotIn(List<BigDecimal> values) {
            addCriterion("total not in", values, "total");
            return (Criteria) this;
        }

        public Criteria andTotalBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("total between", value1, value2, "total");
            return (Criteria) this;
        }

        public Criteria andTotalNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("total not between", value1, value2, "total");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}