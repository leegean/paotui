package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastComplain;
import com.runfast.waimai.dao.model.RunfastComplainExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastComplainMapper extends IMapper<RunfastComplain, Integer, RunfastComplainExample> {
}