package com.runfast.waimai.dao.model;

import java.io.Serializable;

public class BaseUserRole implements Serializable {
    private Integer userid;

    private Integer rid;

    private static final long serialVersionUID = 1L;

    public Integer getUserid() {
        return userid;
    }

    public BaseUserRole withUserid(Integer userid) {
        this.setUserid(userid);
        return this;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getRid() {
        return rid;
    }

    public BaseUserRole withRid(Integer rid) {
        this.setRid(rid);
        return this;
    }

    public void setRid(Integer rid) {
        this.rid = rid;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", userid=").append(userid);
        sb.append(", rid=").append(rid);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}