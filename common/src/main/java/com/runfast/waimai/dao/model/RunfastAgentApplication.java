package com.runfast.waimai.dao.model;

import com.runfast.common.entity.validate.group.Create;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

public class RunfastAgentApplication implements Serializable {
    private Integer id;

    /**
     * 姓名
     */
    @NotBlank(groups = {Create.class})
    private String name;

    @NotBlank(groups = {Create.class})
    private String mobile;

    private Date createTime;

    private Integer status;

    /**
     * 公司名称
     */
    private String company;

    /**
     * 公司成立时间
     */
    private Date setupTime;

    /**
     * 公司地址
     */
    private String address;

    /**
     * 公司规模
     */
    private String scale;

    /**
     * 可投入资金
     */
    private String investAmount;

    /**
     * 主营业务
     */
    private String mainBusiness;

    /**
     * 相关行业经验
     */
    private String experience;

    /**
     * 了解当地餐饮市场
     */
    private String knowFoodMarket;

    /**
     * 市场资源
     */
    private String marketResource;

    private String email;

    /**
     * 负责人身份证号
     */
    private String idNumber;

    /**
     * 营业执照
     */
    private String licenseImg;

    /**
     * 意向代理城市
     */
    @NotBlank(groups = {Create.class})
    private String intentionCity;

    /**
     * 相关资源或经验介绍
     */
    @NotBlank(groups = {Create.class})
    private String introduce;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public RunfastAgentApplication withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public RunfastAgentApplication withName(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getMobile() {
        return mobile;
    }

    public RunfastAgentApplication withMobile(String mobile) {
        this.setMobile(mobile);
        return this;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public RunfastAgentApplication withCreateTime(Date createTime) {
        this.setCreateTime(createTime);
        return this;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getStatus() {
        return status;
    }

    public RunfastAgentApplication withStatus(Integer status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCompany() {
        return company;
    }

    public RunfastAgentApplication withCompany(String company) {
        this.setCompany(company);
        return this;
    }

    public void setCompany(String company) {
        this.company = company == null ? null : company.trim();
    }

    public Date getSetupTime() {
        return setupTime;
    }

    public RunfastAgentApplication withSetupTime(Date setupTime) {
        this.setSetupTime(setupTime);
        return this;
    }

    public void setSetupTime(Date setupTime) {
        this.setupTime = setupTime;
    }

    public String getAddress() {
        return address;
    }

    public RunfastAgentApplication withAddress(String address) {
        this.setAddress(address);
        return this;
    }

    public void setAddress(String address) {
        this.address = address == null ? null : address.trim();
    }

    public String getScale() {
        return scale;
    }

    public RunfastAgentApplication withScale(String scale) {
        this.setScale(scale);
        return this;
    }

    public void setScale(String scale) {
        this.scale = scale == null ? null : scale.trim();
    }

    public String getInvestAmount() {
        return investAmount;
    }

    public RunfastAgentApplication withInvestAmount(String investAmount) {
        this.setInvestAmount(investAmount);
        return this;
    }

    public void setInvestAmount(String investAmount) {
        this.investAmount = investAmount == null ? null : investAmount.trim();
    }

    public String getMainBusiness() {
        return mainBusiness;
    }

    public RunfastAgentApplication withMainBusiness(String mainBusiness) {
        this.setMainBusiness(mainBusiness);
        return this;
    }

    public void setMainBusiness(String mainBusiness) {
        this.mainBusiness = mainBusiness == null ? null : mainBusiness.trim();
    }

    public String getExperience() {
        return experience;
    }

    public RunfastAgentApplication withExperience(String experience) {
        this.setExperience(experience);
        return this;
    }

    public void setExperience(String experience) {
        this.experience = experience == null ? null : experience.trim();
    }

    public String getKnowFoodMarket() {
        return knowFoodMarket;
    }

    public RunfastAgentApplication withKnowFoodMarket(String knowFoodMarket) {
        this.setKnowFoodMarket(knowFoodMarket);
        return this;
    }

    public void setKnowFoodMarket(String knowFoodMarket) {
        this.knowFoodMarket = knowFoodMarket == null ? null : knowFoodMarket.trim();
    }

    public String getMarketResource() {
        return marketResource;
    }

    public RunfastAgentApplication withMarketResource(String marketResource) {
        this.setMarketResource(marketResource);
        return this;
    }

    public void setMarketResource(String marketResource) {
        this.marketResource = marketResource == null ? null : marketResource.trim();
    }

    public String getEmail() {
        return email;
    }

    public RunfastAgentApplication withEmail(String email) {
        this.setEmail(email);
        return this;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public String getIdNumber() {
        return idNumber;
    }

    public RunfastAgentApplication withIdNumber(String idNumber) {
        this.setIdNumber(idNumber);
        return this;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber == null ? null : idNumber.trim();
    }

    public String getLicenseImg() {
        return licenseImg;
    }

    public RunfastAgentApplication withLicenseImg(String licenseImg) {
        this.setLicenseImg(licenseImg);
        return this;
    }

    public void setLicenseImg(String licenseImg) {
        this.licenseImg = licenseImg == null ? null : licenseImg.trim();
    }

    public String getIntentionCity() {
        return intentionCity;
    }

    public RunfastAgentApplication withIntentionCity(String intentionCity) {
        this.setIntentionCity(intentionCity);
        return this;
    }

    public void setIntentionCity(String intentionCity) {
        this.intentionCity = intentionCity == null ? null : intentionCity.trim();
    }

    public String getIntroduce() {
        return introduce;
    }

    public RunfastAgentApplication withIntroduce(String introduce) {
        this.setIntroduce(introduce);
        return this;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce == null ? null : introduce.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", mobile=").append(mobile);
        sb.append(", createTime=").append(createTime);
        sb.append(", status=").append(status);
        sb.append(", company=").append(company);
        sb.append(", setupTime=").append(setupTime);
        sb.append(", address=").append(address);
        sb.append(", scale=").append(scale);
        sb.append(", investAmount=").append(investAmount);
        sb.append(", mainBusiness=").append(mainBusiness);
        sb.append(", experience=").append(experience);
        sb.append(", knowFoodMarket=").append(knowFoodMarket);
        sb.append(", marketResource=").append(marketResource);
        sb.append(", email=").append(email);
        sb.append(", idNumber=").append(idNumber);
        sb.append(", licenseImg=").append(licenseImg);
        sb.append(", intentionCity=").append(intentionCity);
        sb.append(", introduce=").append(introduce);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}