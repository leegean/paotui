package com.runfast.waimai.dao.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class RunfastWithdraw implements Serializable {
    private Integer id;

    private String account;

    private Integer atuditId;

    private String atuditName;

    private Date atuditTime;

    private String content;

    private Date createTime;

    private BigDecimal monetary;

    private Date readyTime;

    private Integer status;

    private Integer type;

    private Integer userId;

    private String userMobile;

    private String userName;

    private Integer applytype;

    private Integer state;

    private String readrName;

    private Integer rid;

    private Integer agentId;

    private String agentName;

    private Integer bankid;

    private String banktype;

    private String name;

    private String wdate;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public RunfastWithdraw withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public RunfastWithdraw withAccount(String account) {
        this.setAccount(account);
        return this;
    }

    public void setAccount(String account) {
        this.account = account == null ? null : account.trim();
    }

    public Integer getAtuditId() {
        return atuditId;
    }

    public RunfastWithdraw withAtuditId(Integer atuditId) {
        this.setAtuditId(atuditId);
        return this;
    }

    public void setAtuditId(Integer atuditId) {
        this.atuditId = atuditId;
    }

    public String getAtuditName() {
        return atuditName;
    }

    public RunfastWithdraw withAtuditName(String atuditName) {
        this.setAtuditName(atuditName);
        return this;
    }

    public void setAtuditName(String atuditName) {
        this.atuditName = atuditName == null ? null : atuditName.trim();
    }

    public Date getAtuditTime() {
        return atuditTime;
    }

    public RunfastWithdraw withAtuditTime(Date atuditTime) {
        this.setAtuditTime(atuditTime);
        return this;
    }

    public void setAtuditTime(Date atuditTime) {
        this.atuditTime = atuditTime;
    }

    public String getContent() {
        return content;
    }

    public RunfastWithdraw withContent(String content) {
        this.setContent(content);
        return this;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public RunfastWithdraw withCreateTime(Date createTime) {
        this.setCreateTime(createTime);
        return this;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public BigDecimal getMonetary() {
        return monetary;
    }

    public RunfastWithdraw withMonetary(BigDecimal monetary) {
        this.setMonetary(monetary);
        return this;
    }

    public void setMonetary(BigDecimal monetary) {
        this.monetary = monetary;
    }

    public Date getReadyTime() {
        return readyTime;
    }

    public RunfastWithdraw withReadyTime(Date readyTime) {
        this.setReadyTime(readyTime);
        return this;
    }

    public void setReadyTime(Date readyTime) {
        this.readyTime = readyTime;
    }

    public Integer getStatus() {
        return status;
    }

    public RunfastWithdraw withStatus(Integer status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getType() {
        return type;
    }

    public RunfastWithdraw withType(Integer type) {
        this.setType(type);
        return this;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getUserId() {
        return userId;
    }

    public RunfastWithdraw withUserId(Integer userId) {
        this.setUserId(userId);
        return this;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserMobile() {
        return userMobile;
    }

    public RunfastWithdraw withUserMobile(String userMobile) {
        this.setUserMobile(userMobile);
        return this;
    }

    public void setUserMobile(String userMobile) {
        this.userMobile = userMobile == null ? null : userMobile.trim();
    }

    public String getUserName() {
        return userName;
    }

    public RunfastWithdraw withUserName(String userName) {
        this.setUserName(userName);
        return this;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public Integer getApplytype() {
        return applytype;
    }

    public RunfastWithdraw withApplytype(Integer applytype) {
        this.setApplytype(applytype);
        return this;
    }

    public void setApplytype(Integer applytype) {
        this.applytype = applytype;
    }

    public Integer getState() {
        return state;
    }

    public RunfastWithdraw withState(Integer state) {
        this.setState(state);
        return this;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getReadrName() {
        return readrName;
    }

    public RunfastWithdraw withReadrName(String readrName) {
        this.setReadrName(readrName);
        return this;
    }

    public void setReadrName(String readrName) {
        this.readrName = readrName == null ? null : readrName.trim();
    }

    public Integer getRid() {
        return rid;
    }

    public RunfastWithdraw withRid(Integer rid) {
        this.setRid(rid);
        return this;
    }

    public void setRid(Integer rid) {
        this.rid = rid;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public RunfastWithdraw withAgentId(Integer agentId) {
        this.setAgentId(agentId);
        return this;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public String getAgentName() {
        return agentName;
    }

    public RunfastWithdraw withAgentName(String agentName) {
        this.setAgentName(agentName);
        return this;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName == null ? null : agentName.trim();
    }

    public Integer getBankid() {
        return bankid;
    }

    public RunfastWithdraw withBankid(Integer bankid) {
        this.setBankid(bankid);
        return this;
    }

    public void setBankid(Integer bankid) {
        this.bankid = bankid;
    }

    public String getBanktype() {
        return banktype;
    }

    public RunfastWithdraw withBanktype(String banktype) {
        this.setBanktype(banktype);
        return this;
    }

    public void setBanktype(String banktype) {
        this.banktype = banktype == null ? null : banktype.trim();
    }

    public String getName() {
        return name;
    }

    public RunfastWithdraw withName(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getWdate() {
        return wdate;
    }

    public RunfastWithdraw withWdate(String wdate) {
        this.setWdate(wdate);
        return this;
    }

    public void setWdate(String wdate) {
        this.wdate = wdate == null ? null : wdate.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", account=").append(account);
        sb.append(", atuditId=").append(atuditId);
        sb.append(", atuditName=").append(atuditName);
        sb.append(", atuditTime=").append(atuditTime);
        sb.append(", content=").append(content);
        sb.append(", createTime=").append(createTime);
        sb.append(", monetary=").append(monetary);
        sb.append(", readyTime=").append(readyTime);
        sb.append(", status=").append(status);
        sb.append(", type=").append(type);
        sb.append(", userId=").append(userId);
        sb.append(", userMobile=").append(userMobile);
        sb.append(", userName=").append(userName);
        sb.append(", applytype=").append(applytype);
        sb.append(", state=").append(state);
        sb.append(", readrName=").append(readrName);
        sb.append(", rid=").append(rid);
        sb.append(", agentId=").append(agentId);
        sb.append(", agentName=").append(agentName);
        sb.append(", bankid=").append(bankid);
        sb.append(", banktype=").append(banktype);
        sb.append(", name=").append(name);
        sb.append(", wdate=").append(wdate);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}