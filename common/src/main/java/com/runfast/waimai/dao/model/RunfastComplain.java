package com.runfast.waimai.dao.model;

import com.runfast.common.entity.validate.group.Create;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

public class RunfastComplain implements Serializable {
    private Integer id;

    private Integer businessId;

    private String businessName;

    private Date createTime;

    private Integer goodsSellId;

    private String goodsSellName;

    private Integer isCheck;

    private Integer isReply;

    private Date replyTime;

    @NotBlank(groups = {Create.class})
    private String userEmail;

    private Integer userId;

    private String userName;

    private Integer goodsSellRecordId;

    private String goodsSellCode;

    private Integer agentId;

    private String agentName;

    private Integer type;

    @NotBlank(groups = {Create.class})
    private String content;

    private String replyContent;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public RunfastComplain withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBusinessId() {
        return businessId;
    }

    public RunfastComplain withBusinessId(Integer businessId) {
        this.setBusinessId(businessId);
        return this;
    }

    public void setBusinessId(Integer businessId) {
        this.businessId = businessId;
    }

    public String getBusinessName() {
        return businessName;
    }

    public RunfastComplain withBusinessName(String businessName) {
        this.setBusinessName(businessName);
        return this;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName == null ? null : businessName.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public RunfastComplain withCreateTime(Date createTime) {
        this.setCreateTime(createTime);
        return this;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getGoodsSellId() {
        return goodsSellId;
    }

    public RunfastComplain withGoodsSellId(Integer goodsSellId) {
        this.setGoodsSellId(goodsSellId);
        return this;
    }

    public void setGoodsSellId(Integer goodsSellId) {
        this.goodsSellId = goodsSellId;
    }

    public String getGoodsSellName() {
        return goodsSellName;
    }

    public RunfastComplain withGoodsSellName(String goodsSellName) {
        this.setGoodsSellName(goodsSellName);
        return this;
    }

    public void setGoodsSellName(String goodsSellName) {
        this.goodsSellName = goodsSellName == null ? null : goodsSellName.trim();
    }

    public Integer getIsCheck() {
        return isCheck;
    }

    public RunfastComplain withIsCheck(Integer isCheck) {
        this.setIsCheck(isCheck);
        return this;
    }

    public void setIsCheck(Integer isCheck) {
        this.isCheck = isCheck;
    }

    public Integer getIsReply() {
        return isReply;
    }

    public RunfastComplain withIsReply(Integer isReply) {
        this.setIsReply(isReply);
        return this;
    }

    public void setIsReply(Integer isReply) {
        this.isReply = isReply;
    }

    public Date getReplyTime() {
        return replyTime;
    }

    public RunfastComplain withReplyTime(Date replyTime) {
        this.setReplyTime(replyTime);
        return this;
    }

    public void setReplyTime(Date replyTime) {
        this.replyTime = replyTime;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public RunfastComplain withUserEmail(String userEmail) {
        this.setUserEmail(userEmail);
        return this;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail == null ? null : userEmail.trim();
    }

    public Integer getUserId() {
        return userId;
    }

    public RunfastComplain withUserId(Integer userId) {
        this.setUserId(userId);
        return this;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public RunfastComplain withUserName(String userName) {
        this.setUserName(userName);
        return this;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public Integer getGoodsSellRecordId() {
        return goodsSellRecordId;
    }

    public RunfastComplain withGoodsSellRecordId(Integer goodsSellRecordId) {
        this.setGoodsSellRecordId(goodsSellRecordId);
        return this;
    }

    public void setGoodsSellRecordId(Integer goodsSellRecordId) {
        this.goodsSellRecordId = goodsSellRecordId;
    }

    public String getGoodsSellCode() {
        return goodsSellCode;
    }

    public RunfastComplain withGoodsSellCode(String goodsSellCode) {
        this.setGoodsSellCode(goodsSellCode);
        return this;
    }

    public void setGoodsSellCode(String goodsSellCode) {
        this.goodsSellCode = goodsSellCode == null ? null : goodsSellCode.trim();
    }

    public Integer getAgentId() {
        return agentId;
    }

    public RunfastComplain withAgentId(Integer agentId) {
        this.setAgentId(agentId);
        return this;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public String getAgentName() {
        return agentName;
    }

    public RunfastComplain withAgentName(String agentName) {
        this.setAgentName(agentName);
        return this;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName == null ? null : agentName.trim();
    }

    public Integer getType() {
        return type;
    }

    public RunfastComplain withType(Integer type) {
        this.setType(type);
        return this;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public RunfastComplain withContent(String content) {
        this.setContent(content);
        return this;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public String getReplyContent() {
        return replyContent;
    }

    public RunfastComplain withReplyContent(String replyContent) {
        this.setReplyContent(replyContent);
        return this;
    }

    public void setReplyContent(String replyContent) {
        this.replyContent = replyContent == null ? null : replyContent.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", businessId=").append(businessId);
        sb.append(", businessName=").append(businessName);
        sb.append(", createTime=").append(createTime);
        sb.append(", goodsSellId=").append(goodsSellId);
        sb.append(", goodsSellName=").append(goodsSellName);
        sb.append(", isCheck=").append(isCheck);
        sb.append(", isReply=").append(isReply);
        sb.append(", replyTime=").append(replyTime);
        sb.append(", userEmail=").append(userEmail);
        sb.append(", userId=").append(userId);
        sb.append(", userName=").append(userName);
        sb.append(", goodsSellRecordId=").append(goodsSellRecordId);
        sb.append(", goodsSellCode=").append(goodsSellCode);
        sb.append(", agentId=").append(agentId);
        sb.append(", agentName=").append(agentName);
        sb.append(", type=").append(type);
        sb.append(", content=").append(content);
        sb.append(", replyContent=").append(replyContent);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}