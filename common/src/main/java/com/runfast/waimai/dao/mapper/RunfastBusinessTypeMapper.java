package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastBusinessType;
import com.runfast.waimai.dao.model.RunfastBusinessTypeExample;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface RunfastBusinessTypeMapper extends IMapper<RunfastBusinessType, Integer, RunfastBusinessTypeExample> {
    List<RunfastBusinessType> findAllBusinessType();
}