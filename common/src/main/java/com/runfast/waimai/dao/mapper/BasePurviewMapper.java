package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.BasePurview;
import com.runfast.waimai.dao.model.BasePurviewExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BasePurviewMapper extends IMapper<BasePurview, Integer, BasePurviewExample> {
}