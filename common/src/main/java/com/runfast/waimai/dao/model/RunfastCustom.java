package com.runfast.waimai.dao.model;

import java.io.Serializable;
import java.util.Date;

public class RunfastCustom implements Serializable {
    private Integer id;

    private Date createTime;

    private String mobile;

    private String qrcode;

    private Integer agenId;

    private String agenName;

    private Integer isMaster;

    private Integer type;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public RunfastCustom withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public RunfastCustom withCreateTime(Date createTime) {
        this.setCreateTime(createTime);
        return this;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getMobile() {
        return mobile;
    }

    public RunfastCustom withMobile(String mobile) {
        this.setMobile(mobile);
        return this;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public String getQrcode() {
        return qrcode;
    }

    public RunfastCustom withQrcode(String qrcode) {
        this.setQrcode(qrcode);
        return this;
    }

    public void setQrcode(String qrcode) {
        this.qrcode = qrcode == null ? null : qrcode.trim();
    }

    public Integer getAgenId() {
        return agenId;
    }

    public RunfastCustom withAgenId(Integer agenId) {
        this.setAgenId(agenId);
        return this;
    }

    public void setAgenId(Integer agenId) {
        this.agenId = agenId;
    }

    public String getAgenName() {
        return agenName;
    }

    public RunfastCustom withAgenName(String agenName) {
        this.setAgenName(agenName);
        return this;
    }

    public void setAgenName(String agenName) {
        this.agenName = agenName == null ? null : agenName.trim();
    }

    public Integer getIsMaster() {
        return isMaster;
    }

    public RunfastCustom withIsMaster(Integer isMaster) {
        this.setIsMaster(isMaster);
        return this;
    }

    public void setIsMaster(Integer isMaster) {
        this.isMaster = isMaster;
    }

    public Integer getType() {
        return type;
    }

    public RunfastCustom withType(Integer type) {
        this.setType(type);
        return this;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", createTime=").append(createTime);
        sb.append(", mobile=").append(mobile);
        sb.append(", qrcode=").append(qrcode);
        sb.append(", agenId=").append(agenId);
        sb.append(", agenName=").append(agenName);
        sb.append(", isMaster=").append(isMaster);
        sb.append(", type=").append(type);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}