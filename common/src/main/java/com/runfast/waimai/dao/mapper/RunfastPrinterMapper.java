package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastPrinter;
import com.runfast.waimai.dao.model.RunfastPrinterExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastPrinterMapper extends IMapper<RunfastPrinter, Integer, RunfastPrinterExample> {
}