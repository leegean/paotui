package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastGoodsSellStandard;
import com.runfast.waimai.dao.model.RunfastGoodsSellStandardExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastGoodsSellStandardMapper extends IMapper<RunfastGoodsSellStandard, Integer, RunfastGoodsSellStandardExample> {
}