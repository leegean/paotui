package com.runfast.waimai.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.waimai.dao.model.RunfastAccountrecord;
import com.runfast.waimai.dao.model.RunfastAccountrecordExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastAccountrecordMapper extends IMapper<RunfastAccountrecord, Integer, RunfastAccountrecordExample> {
}