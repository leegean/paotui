package com.runfast.paotui.mq;

import com.runfast.paotui.dao.model.Order;
import com.runfast.waimai.dao.model.RunfastGoodsSellRecord;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.converter.ContentTypeDelegatingMessageConverter;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import sun.java2d.pipe.SpanShapeRenderer;

import java.util.Date;
import java.util.UUID;

/**
 * @author: lijin
 * @date: 2018年04月18日
 */

@Slf4j
@Component
public class AmqpClient {
    private final AmqpAdmin amqpAdmin;
    private final AmqpTemplate amqpTemplate;

    public static final String OrderExchange = "order_exchange";

    public static final String WmBindingKey = "wm_order_exchange";

    public static final String PtBindingKey = "pt_order_exchange";
    @Autowired
    public AmqpClient(AmqpAdmin amqpAdmin, AmqpTemplate amqpTemplate) {
        this.amqpAdmin = amqpAdmin;
        this.amqpTemplate = amqpTemplate;
    }

    @Async
    public void publish(String exchange, Object object){
        MessageProperties messageProperties = new MessageProperties();
        messageProperties.setMessageId(UUID.randomUUID().toString());
        messageProperties.setTimestamp(new Date());

//        SimpleMessageConverter messageConverter = new SimpleMessageConverter();
        Jackson2JsonMessageConverter messageConverter = new Jackson2JsonMessageConverter();
        Message message = messageConverter.toMessage(object, messageProperties);
        amqpTemplate.send(exchange,null, message);
        log.info("推送订单至消息队列：" +message.toString());

    }


   /*@RabbitListener(bindings = @QueueBinding(value=@Queue, exchange = @Exchange(name = OrderExchange,type = ExchangeTypes.FANOUT)))
    public void processMessage11(Message message) {

        log.info("1: "+ message.toString());

    }

    @RabbitListener(bindings = @QueueBinding(value=@Queue, exchange = @Exchange(name = OrderExchange,type = ExchangeTypes.FANOUT)))
    public void processMessage12(Message message) {

        log.info("2: "+ message.toString());

    }

    @RabbitListener(bindings = @QueueBinding(value=@Queue, exchange = @Exchange(name = OrderExchange,type = ExchangeTypes.FANOUT)))
    public void processMessage13(Message message) {

        log.info("3: "+ message.toString());

    }*/

}
