package com.runfast.paotui.dao.model;

import com.runfast.pay.Channel;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Data
public class Order implements Serializable {
    public enum Status {CREATED, CANCELED, PAID, TAKEN, COMPLETED,PICKED}

    public enum FromType {SPECIFIED, NEARBY}

    public enum Type {DAIGOU, QUSONGJIAN}



    public interface Daigou {
    }

    public interface Qusongjian {
    }

    public interface Specified {
    }

    public interface NearBy {
    }

    private Integer id;
    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 已支付金额
     */
    private Integer amountPaid;

    /**
     * 应支付金额
     */
    private Integer amountPayable;

    /**
     * 配送费
     */
    private Integer deliveryCost;

    /**
     * 源地址
     */
    @NotBlank(groups = {Specified.class})
    private String fromAddress;

    /**
     * 源纬度
     */
    @NotNull(groups = {Specified.class})
    private Double fromLat;

    /**
     * 源经度
     */
    @NotNull(groups = {Specified.class})
    private Double fromLng;

    /**
     * 源地址类型
     */
    @NotNull(groups = {Daigou.class})
    private FromType fromType;

    /**
     * 商品描述
     */
    @NotBlank(groups = {Daigou.class})
    private String goodsDescription;

    /**
     * 商品类型
     */
    @NotNull(groups = {Qusongjian.class})
    private String goodsType;

    /**
     * 商品重量
     */
    @NotNull(groups = {Qusongjian.class})
    private Integer goodsWeight;

    /**
     * 保险
     */
    private Integer insurance;

    /**
     * 订单号
     */
    private String orderNo;

    /**
     * 订单状态
     */
    private Status status;

    /**
     * 取货时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @NotNull(groups = {Qusongjian.class})
    private Date pickTime;

    /**
     * 备注
     */
    private String remark;

    /**
     * 建议商品价格
     */
    private Integer suggestionCost;

    /**
     * 小费
     */
    private Integer tip;

    /**
     * 当前订单所属用户
     */
    private Integer cuserId;

    /**
     * 骑手id
     */
    private Integer driverId;

    /**
     * 源地址id
     */
    @NotNull(groups = {Qusongjian.class})
    private Integer fromId;

    /**
     * 目的地id
     */
    @NotNull(groups = {Daigou.class, Qusongjian.class})
    private Integer toId;

    /**
     * 订单类型
     */
    @NotNull(groups = {Daigou.class, Qusongjian.class})
    private Type type;

    /**
     * 距离
     */
    private Integer distance;

    /**
     * 预计配送时间
     */
    private Integer deliveryDuration;

    /**
     * 骑手收入
     */
    private Integer driverIncome;

    /**
     * 平台收入
     */
    private Integer plateformIncome;

    /**
     * 叶子节点代理商收入
     */
    private Integer agentIncome;
    @NotNull(groups = {Daigou.class, Qusongjian.class})
    private Double userLng;
    @NotNull(groups = {Daigou.class, Qusongjian.class})
    private Double userLat;

    /**
     * 附近代理商
     */
    private Integer agentId;

    /**
     * 退款总金额
     */
    private Integer amountRefunded;

    /**
     * 是否存在退款
     */
    private Boolean refunded;

    /**
     * 是否已支付
     */
    private Boolean paid;

    /**
     * 是否已取消
     */
    private Boolean canceled;

    /**
     * 是否已接单
     */
    private Boolean taken;

    /**
     * 是否已完成
     */
    private Boolean completed;

    private Date timePaid;

    private Date timeCanceled;

    private Date timeTaken;

    private Date timeCompleted;

    /**
     * 支付渠道
     */
    private Channel channel;

    private Boolean deleted;
    /**
     * 取件的地址名称
     */
    @NotBlank(groups = {Specified.class})
    private String fromName;

    private static final long serialVersionUID = 1L;

    /**
     * 申请取消状态，1:用户提出 2:骑手同意3:不同意；4：系统超时确认
     */
    private Integer isCancel;

    /**
     * 申请取消的时间
     */
    private Date cancelTime;

}