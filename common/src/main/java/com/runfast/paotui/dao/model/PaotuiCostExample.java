package com.runfast.paotui.dao.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PaotuiCostExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PaotuiCostExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNull() {
            addCriterion("createTime is null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIsNotNull() {
            addCriterion("createTime is not null");
            return (Criteria) this;
        }

        public Criteria andCreatetimeEqualTo(Date value) {
            addCriterion("createTime =", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotEqualTo(Date value) {
            addCriterion("createTime <>", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThan(Date value) {
            addCriterion("createTime >", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeGreaterThanOrEqualTo(Date value) {
            addCriterion("createTime >=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThan(Date value) {
            addCriterion("createTime <", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeLessThanOrEqualTo(Date value) {
            addCriterion("createTime <=", value, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeIn(List<Date> values) {
            addCriterion("createTime in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotIn(List<Date> values) {
            addCriterion("createTime not in", values, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeBetween(Date value1, Date value2) {
            addCriterion("createTime between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andCreatetimeNotBetween(Date value1, Date value2) {
            addCriterion("createTime not between", value1, value2, "createtime");
            return (Criteria) this;
        }

        public Criteria andBasedistanceIsNull() {
            addCriterion("baseDistance is null");
            return (Criteria) this;
        }

        public Criteria andBasedistanceIsNotNull() {
            addCriterion("baseDistance is not null");
            return (Criteria) this;
        }

        public Criteria andBasedistanceEqualTo(Double value) {
            addCriterion("baseDistance =", value, "basedistance");
            return (Criteria) this;
        }

        public Criteria andBasedistanceNotEqualTo(Double value) {
            addCriterion("baseDistance <>", value, "basedistance");
            return (Criteria) this;
        }

        public Criteria andBasedistanceGreaterThan(Double value) {
            addCriterion("baseDistance >", value, "basedistance");
            return (Criteria) this;
        }

        public Criteria andBasedistanceGreaterThanOrEqualTo(Double value) {
            addCriterion("baseDistance >=", value, "basedistance");
            return (Criteria) this;
        }

        public Criteria andBasedistanceLessThan(Double value) {
            addCriterion("baseDistance <", value, "basedistance");
            return (Criteria) this;
        }

        public Criteria andBasedistanceLessThanOrEqualTo(Double value) {
            addCriterion("baseDistance <=", value, "basedistance");
            return (Criteria) this;
        }

        public Criteria andBasedistanceIn(List<Double> values) {
            addCriterion("baseDistance in", values, "basedistance");
            return (Criteria) this;
        }

        public Criteria andBasedistanceNotIn(List<Double> values) {
            addCriterion("baseDistance not in", values, "basedistance");
            return (Criteria) this;
        }

        public Criteria andBasedistanceBetween(Double value1, Double value2) {
            addCriterion("baseDistance between", value1, value2, "basedistance");
            return (Criteria) this;
        }

        public Criteria andBasedistanceNotBetween(Double value1, Double value2) {
            addCriterion("baseDistance not between", value1, value2, "basedistance");
            return (Criteria) this;
        }

        public Criteria andBasictimeIsNull() {
            addCriterion("basicTime is null");
            return (Criteria) this;
        }

        public Criteria andBasictimeIsNotNull() {
            addCriterion("basicTime is not null");
            return (Criteria) this;
        }

        public Criteria andBasictimeEqualTo(Integer value) {
            addCriterion("basicTime =", value, "basictime");
            return (Criteria) this;
        }

        public Criteria andBasictimeNotEqualTo(Integer value) {
            addCriterion("basicTime <>", value, "basictime");
            return (Criteria) this;
        }

        public Criteria andBasictimeGreaterThan(Integer value) {
            addCriterion("basicTime >", value, "basictime");
            return (Criteria) this;
        }

        public Criteria andBasictimeGreaterThanOrEqualTo(Integer value) {
            addCriterion("basicTime >=", value, "basictime");
            return (Criteria) this;
        }

        public Criteria andBasictimeLessThan(Integer value) {
            addCriterion("basicTime <", value, "basictime");
            return (Criteria) this;
        }

        public Criteria andBasictimeLessThanOrEqualTo(Integer value) {
            addCriterion("basicTime <=", value, "basictime");
            return (Criteria) this;
        }

        public Criteria andBasictimeIn(List<Integer> values) {
            addCriterion("basicTime in", values, "basictime");
            return (Criteria) this;
        }

        public Criteria andBasictimeNotIn(List<Integer> values) {
            addCriterion("basicTime not in", values, "basictime");
            return (Criteria) this;
        }

        public Criteria andBasictimeBetween(Integer value1, Integer value2) {
            addCriterion("basicTime between", value1, value2, "basictime");
            return (Criteria) this;
        }

        public Criteria andBasictimeNotBetween(Integer value1, Integer value2) {
            addCriterion("basicTime not between", value1, value2, "basictime");
            return (Criteria) this;
        }

        public Criteria andDaigoubasecostIsNull() {
            addCriterion("daigouBaseCost is null");
            return (Criteria) this;
        }

        public Criteria andDaigoubasecostIsNotNull() {
            addCriterion("daigouBaseCost is not null");
            return (Criteria) this;
        }

        public Criteria andDaigoubasecostEqualTo(BigDecimal value) {
            addCriterion("daigouBaseCost =", value, "daigoubasecost");
            return (Criteria) this;
        }

        public Criteria andDaigoubasecostNotEqualTo(BigDecimal value) {
            addCriterion("daigouBaseCost <>", value, "daigoubasecost");
            return (Criteria) this;
        }

        public Criteria andDaigoubasecostGreaterThan(BigDecimal value) {
            addCriterion("daigouBaseCost >", value, "daigoubasecost");
            return (Criteria) this;
        }

        public Criteria andDaigoubasecostGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("daigouBaseCost >=", value, "daigoubasecost");
            return (Criteria) this;
        }

        public Criteria andDaigoubasecostLessThan(BigDecimal value) {
            addCriterion("daigouBaseCost <", value, "daigoubasecost");
            return (Criteria) this;
        }

        public Criteria andDaigoubasecostLessThanOrEqualTo(BigDecimal value) {
            addCriterion("daigouBaseCost <=", value, "daigoubasecost");
            return (Criteria) this;
        }

        public Criteria andDaigoubasecostIn(List<BigDecimal> values) {
            addCriterion("daigouBaseCost in", values, "daigoubasecost");
            return (Criteria) this;
        }

        public Criteria andDaigoubasecostNotIn(List<BigDecimal> values) {
            addCriterion("daigouBaseCost not in", values, "daigoubasecost");
            return (Criteria) this;
        }

        public Criteria andDaigoubasecostBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("daigouBaseCost between", value1, value2, "daigoubasecost");
            return (Criteria) this;
        }

        public Criteria andDaigoubasecostNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("daigouBaseCost not between", value1, value2, "daigoubasecost");
            return (Criteria) this;
        }

        public Criteria andQusongjianbasecostIsNull() {
            addCriterion("qusongjianBaseCost is null");
            return (Criteria) this;
        }

        public Criteria andQusongjianbasecostIsNotNull() {
            addCriterion("qusongjianBaseCost is not null");
            return (Criteria) this;
        }

        public Criteria andQusongjianbasecostEqualTo(BigDecimal value) {
            addCriterion("qusongjianBaseCost =", value, "qusongjianbasecost");
            return (Criteria) this;
        }

        public Criteria andQusongjianbasecostNotEqualTo(BigDecimal value) {
            addCriterion("qusongjianBaseCost <>", value, "qusongjianbasecost");
            return (Criteria) this;
        }

        public Criteria andQusongjianbasecostGreaterThan(BigDecimal value) {
            addCriterion("qusongjianBaseCost >", value, "qusongjianbasecost");
            return (Criteria) this;
        }

        public Criteria andQusongjianbasecostGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("qusongjianBaseCost >=", value, "qusongjianbasecost");
            return (Criteria) this;
        }

        public Criteria andQusongjianbasecostLessThan(BigDecimal value) {
            addCriterion("qusongjianBaseCost <", value, "qusongjianbasecost");
            return (Criteria) this;
        }

        public Criteria andQusongjianbasecostLessThanOrEqualTo(BigDecimal value) {
            addCriterion("qusongjianBaseCost <=", value, "qusongjianbasecost");
            return (Criteria) this;
        }

        public Criteria andQusongjianbasecostIn(List<BigDecimal> values) {
            addCriterion("qusongjianBaseCost in", values, "qusongjianbasecost");
            return (Criteria) this;
        }

        public Criteria andQusongjianbasecostNotIn(List<BigDecimal> values) {
            addCriterion("qusongjianBaseCost not in", values, "qusongjianbasecost");
            return (Criteria) this;
        }

        public Criteria andQusongjianbasecostBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("qusongjianBaseCost between", value1, value2, "qusongjianbasecost");
            return (Criteria) this;
        }

        public Criteria andQusongjianbasecostNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("qusongjianBaseCost not between", value1, value2, "qusongjianbasecost");
            return (Criteria) this;
        }

        public Criteria andOvertimedistanceIsNull() {
            addCriterion("overTimeDistance is null");
            return (Criteria) this;
        }

        public Criteria andOvertimedistanceIsNotNull() {
            addCriterion("overTimeDistance is not null");
            return (Criteria) this;
        }

        public Criteria andOvertimedistanceEqualTo(Double value) {
            addCriterion("overTimeDistance =", value, "overtimedistance");
            return (Criteria) this;
        }

        public Criteria andOvertimedistanceNotEqualTo(Double value) {
            addCriterion("overTimeDistance <>", value, "overtimedistance");
            return (Criteria) this;
        }

        public Criteria andOvertimedistanceGreaterThan(Double value) {
            addCriterion("overTimeDistance >", value, "overtimedistance");
            return (Criteria) this;
        }

        public Criteria andOvertimedistanceGreaterThanOrEqualTo(Double value) {
            addCriterion("overTimeDistance >=", value, "overtimedistance");
            return (Criteria) this;
        }

        public Criteria andOvertimedistanceLessThan(Double value) {
            addCriterion("overTimeDistance <", value, "overtimedistance");
            return (Criteria) this;
        }

        public Criteria andOvertimedistanceLessThanOrEqualTo(Double value) {
            addCriterion("overTimeDistance <=", value, "overtimedistance");
            return (Criteria) this;
        }

        public Criteria andOvertimedistanceIn(List<Double> values) {
            addCriterion("overTimeDistance in", values, "overtimedistance");
            return (Criteria) this;
        }

        public Criteria andOvertimedistanceNotIn(List<Double> values) {
            addCriterion("overTimeDistance not in", values, "overtimedistance");
            return (Criteria) this;
        }

        public Criteria andOvertimedistanceBetween(Double value1, Double value2) {
            addCriterion("overTimeDistance between", value1, value2, "overtimedistance");
            return (Criteria) this;
        }

        public Criteria andOvertimedistanceNotBetween(Double value1, Double value2) {
            addCriterion("overTimeDistance not between", value1, value2, "overtimedistance");
            return (Criteria) this;
        }

        public Criteria andOvertimetimeIsNull() {
            addCriterion("overTimeTime is null");
            return (Criteria) this;
        }

        public Criteria andOvertimetimeIsNotNull() {
            addCriterion("overTimeTime is not null");
            return (Criteria) this;
        }

        public Criteria andOvertimetimeEqualTo(Integer value) {
            addCriterion("overTimeTime =", value, "overtimetime");
            return (Criteria) this;
        }

        public Criteria andOvertimetimeNotEqualTo(Integer value) {
            addCriterion("overTimeTime <>", value, "overtimetime");
            return (Criteria) this;
        }

        public Criteria andOvertimetimeGreaterThan(Integer value) {
            addCriterion("overTimeTime >", value, "overtimetime");
            return (Criteria) this;
        }

        public Criteria andOvertimetimeGreaterThanOrEqualTo(Integer value) {
            addCriterion("overTimeTime >=", value, "overtimetime");
            return (Criteria) this;
        }

        public Criteria andOvertimetimeLessThan(Integer value) {
            addCriterion("overTimeTime <", value, "overtimetime");
            return (Criteria) this;
        }

        public Criteria andOvertimetimeLessThanOrEqualTo(Integer value) {
            addCriterion("overTimeTime <=", value, "overtimetime");
            return (Criteria) this;
        }

        public Criteria andOvertimetimeIn(List<Integer> values) {
            addCriterion("overTimeTime in", values, "overtimetime");
            return (Criteria) this;
        }

        public Criteria andOvertimetimeNotIn(List<Integer> values) {
            addCriterion("overTimeTime not in", values, "overtimetime");
            return (Criteria) this;
        }

        public Criteria andOvertimetimeBetween(Integer value1, Integer value2) {
            addCriterion("overTimeTime between", value1, value2, "overtimetime");
            return (Criteria) this;
        }

        public Criteria andOvertimetimeNotBetween(Integer value1, Integer value2) {
            addCriterion("overTimeTime not between", value1, value2, "overtimetime");
            return (Criteria) this;
        }

        public Criteria andIsdefaultIsNull() {
            addCriterion("isDefault is null");
            return (Criteria) this;
        }

        public Criteria andIsdefaultIsNotNull() {
            addCriterion("isDefault is not null");
            return (Criteria) this;
        }

        public Criteria andIsdefaultEqualTo(Integer value) {
            addCriterion("isDefault =", value, "isdefault");
            return (Criteria) this;
        }

        public Criteria andIsdefaultNotEqualTo(Integer value) {
            addCriterion("isDefault <>", value, "isdefault");
            return (Criteria) this;
        }

        public Criteria andIsdefaultGreaterThan(Integer value) {
            addCriterion("isDefault >", value, "isdefault");
            return (Criteria) this;
        }

        public Criteria andIsdefaultGreaterThanOrEqualTo(Integer value) {
            addCriterion("isDefault >=", value, "isdefault");
            return (Criteria) this;
        }

        public Criteria andIsdefaultLessThan(Integer value) {
            addCriterion("isDefault <", value, "isdefault");
            return (Criteria) this;
        }

        public Criteria andIsdefaultLessThanOrEqualTo(Integer value) {
            addCriterion("isDefault <=", value, "isdefault");
            return (Criteria) this;
        }

        public Criteria andIsdefaultIn(List<Integer> values) {
            addCriterion("isDefault in", values, "isdefault");
            return (Criteria) this;
        }

        public Criteria andIsdefaultNotIn(List<Integer> values) {
            addCriterion("isDefault not in", values, "isdefault");
            return (Criteria) this;
        }

        public Criteria andIsdefaultBetween(Integer value1, Integer value2) {
            addCriterion("isDefault between", value1, value2, "isdefault");
            return (Criteria) this;
        }

        public Criteria andIsdefaultNotBetween(Integer value1, Integer value2) {
            addCriterion("isDefault not between", value1, value2, "isdefault");
            return (Criteria) this;
        }

        public Criteria andAgentidIsNull() {
            addCriterion("agentId is null");
            return (Criteria) this;
        }

        public Criteria andAgentidIsNotNull() {
            addCriterion("agentId is not null");
            return (Criteria) this;
        }

        public Criteria andAgentidEqualTo(Integer value) {
            addCriterion("agentId =", value, "agentid");
            return (Criteria) this;
        }

        public Criteria andAgentidNotEqualTo(Integer value) {
            addCriterion("agentId <>", value, "agentid");
            return (Criteria) this;
        }

        public Criteria andAgentidGreaterThan(Integer value) {
            addCriterion("agentId >", value, "agentid");
            return (Criteria) this;
        }

        public Criteria andAgentidGreaterThanOrEqualTo(Integer value) {
            addCriterion("agentId >=", value, "agentid");
            return (Criteria) this;
        }

        public Criteria andAgentidLessThan(Integer value) {
            addCriterion("agentId <", value, "agentid");
            return (Criteria) this;
        }

        public Criteria andAgentidLessThanOrEqualTo(Integer value) {
            addCriterion("agentId <=", value, "agentid");
            return (Criteria) this;
        }

        public Criteria andAgentidIn(List<Integer> values) {
            addCriterion("agentId in", values, "agentid");
            return (Criteria) this;
        }

        public Criteria andAgentidNotIn(List<Integer> values) {
            addCriterion("agentId not in", values, "agentid");
            return (Criteria) this;
        }

        public Criteria andAgentidBetween(Integer value1, Integer value2) {
            addCriterion("agentId between", value1, value2, "agentid");
            return (Criteria) this;
        }

        public Criteria andAgentidNotBetween(Integer value1, Integer value2) {
            addCriterion("agentId not between", value1, value2, "agentid");
            return (Criteria) this;
        }

        public Criteria andAgentnameIsNull() {
            addCriterion("agentName is null");
            return (Criteria) this;
        }

        public Criteria andAgentnameIsNotNull() {
            addCriterion("agentName is not null");
            return (Criteria) this;
        }

        public Criteria andAgentnameEqualTo(String value) {
            addCriterion("agentName =", value, "agentname");
            return (Criteria) this;
        }

        public Criteria andAgentnameNotEqualTo(String value) {
            addCriterion("agentName <>", value, "agentname");
            return (Criteria) this;
        }

        public Criteria andAgentnameGreaterThan(String value) {
            addCriterion("agentName >", value, "agentname");
            return (Criteria) this;
        }

        public Criteria andAgentnameGreaterThanOrEqualTo(String value) {
            addCriterion("agentName >=", value, "agentname");
            return (Criteria) this;
        }

        public Criteria andAgentnameLessThan(String value) {
            addCriterion("agentName <", value, "agentname");
            return (Criteria) this;
        }

        public Criteria andAgentnameLessThanOrEqualTo(String value) {
            addCriterion("agentName <=", value, "agentname");
            return (Criteria) this;
        }

        public Criteria andAgentnameLike(String value) {
            addCriterion("agentName like", value, "agentname");
            return (Criteria) this;
        }

        public Criteria andAgentnameNotLike(String value) {
            addCriterion("agentName not like", value, "agentname");
            return (Criteria) this;
        }

        public Criteria andAgentnameIn(List<String> values) {
            addCriterion("agentName in", values, "agentname");
            return (Criteria) this;
        }

        public Criteria andAgentnameNotIn(List<String> values) {
            addCriterion("agentName not in", values, "agentname");
            return (Criteria) this;
        }

        public Criteria andAgentnameBetween(String value1, String value2) {
            addCriterion("agentName between", value1, value2, "agentname");
            return (Criteria) this;
        }

        public Criteria andAgentnameNotBetween(String value1, String value2) {
            addCriterion("agentName not between", value1, value2, "agentname");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}