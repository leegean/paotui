package com.runfast.paotui.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.paotui.dao.model.Account;
import com.runfast.paotui.dao.model.AccountExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AccountMapper extends IMapper<Account, Integer, AccountExample> {
}