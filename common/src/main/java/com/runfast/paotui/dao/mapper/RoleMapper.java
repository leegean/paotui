package com.runfast.paotui.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.paotui.dao.model.Role;
import com.runfast.paotui.dao.model.RoleExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RoleMapper extends IMapper<Role, Integer, RoleExample> {
}