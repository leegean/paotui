package com.runfast.paotui.dao.model;

import java.io.Serializable;
import java.util.Date;

public class Refund implements Serializable {
    public enum Status {PENDING, SUCCESS, FAIL}

    private Integer id;

    private Date createTime;

    private Date updateTime;

    /**
     * 退款金额
     */
    private Integer amount;

    /**
     * 退款单号
     */
    private String chargeOrderNo;

    private String description;

    private String failureCode;

    private String failureMsg;

    /**
     * 订单号
     */
    private String orderNo;

    /**
     * 退款状态
     */
    private Status status;

    /**
     * 是否退款成功
     */
    private Boolean succeed;

    private Date timeSucceed;

    private String transactionNo;

    private Integer orderId;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public Refund withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public Refund withCreateTime(Date createTime) {
        this.setCreateTime(createTime);
        return this;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public Refund withUpdateTime(Date updateTime) {
        this.setUpdateTime(updateTime);
        return this;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getAmount() {
        return amount;
    }

    public Refund withAmount(Integer amount) {
        this.setAmount(amount);
        return this;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getChargeOrderNo() {
        return chargeOrderNo;
    }

    public Refund withChargeOrderNo(String chargeOrderNo) {
        this.setChargeOrderNo(chargeOrderNo);
        return this;
    }

    public void setChargeOrderNo(String chargeOrderNo) {
        this.chargeOrderNo = chargeOrderNo == null ? null : chargeOrderNo.trim();
    }

    public String getDescription() {
        return description;
    }

    public Refund withDescription(String description) {
        this.setDescription(description);
        return this;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public String getFailureCode() {
        return failureCode;
    }

    public Refund withFailureCode(String failureCode) {
        this.setFailureCode(failureCode);
        return this;
    }

    public void setFailureCode(String failureCode) {
        this.failureCode = failureCode == null ? null : failureCode.trim();
    }

    public String getFailureMsg() {
        return failureMsg;
    }

    public Refund withFailureMsg(String failureMsg) {
        this.setFailureMsg(failureMsg);
        return this;
    }

    public void setFailureMsg(String failureMsg) {
        this.failureMsg = failureMsg == null ? null : failureMsg.trim();
    }

    public String getOrderNo() {
        return orderNo;
    }

    public Refund withOrderNo(String orderNo) {
        this.setOrderNo(orderNo);
        return this;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo == null ? null : orderNo.trim();
    }

    public Status getStatus() {
        return status;
    }

    public Refund withStatus(Status status) {
        this.setStatus(status);
        return this;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Boolean getSucceed() {
        return succeed;
    }

    public Refund withSucceed(Boolean succeed) {
        this.setSucceed(succeed);
        return this;
    }

    public void setSucceed(Boolean succeed) {
        this.succeed = succeed;
    }

    public Date getTimeSucceed() {
        return timeSucceed;
    }

    public Refund withTimeSucceed(Date timeSucceed) {
        this.setTimeSucceed(timeSucceed);
        return this;
    }

    public void setTimeSucceed(Date timeSucceed) {
        this.timeSucceed = timeSucceed;
    }

    public String getTransactionNo() {
        return transactionNo;
    }

    public Refund withTransactionNo(String transactionNo) {
        this.setTransactionNo(transactionNo);
        return this;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo == null ? null : transactionNo.trim();
    }

    public Integer getOrderId() {
        return orderId;
    }

    public Refund withOrderId(Integer orderId) {
        this.setOrderId(orderId);
        return this;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", amount=").append(amount);
        sb.append(", chargeOrderNo=").append(chargeOrderNo);
        sb.append(", description=").append(description);
        sb.append(", failureCode=").append(failureCode);
        sb.append(", failureMsg=").append(failureMsg);
        sb.append(", orderNo=").append(orderNo);
        sb.append(", status=").append(status);
        sb.append(", succeed=").append(succeed);
        sb.append(", timeSucceed=").append(timeSucceed);
        sb.append(", transactionNo=").append(transactionNo);
        sb.append(", orderId=").append(orderId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}