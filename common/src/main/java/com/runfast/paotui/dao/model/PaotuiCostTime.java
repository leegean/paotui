package com.runfast.paotui.dao.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class PaotuiCostTime implements Serializable {
    private Integer id;

    /**
     * 起始时间
     */
    private Date starttime;

    /**
     * 结束时间
     */
    private Date endtime;

    /**
     * 每单加收费用
     */
    private BigDecimal fee;

    /**
     * 跑腿计费模板id
     */
    private Integer costid;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public PaotuiCostTime withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getStarttime() {
        return starttime;
    }

    public PaotuiCostTime withStarttime(Date starttime) {
        this.setStarttime(starttime);
        return this;
    }

    public void setStarttime(Date starttime) {
        this.starttime = starttime;
    }

    public Date getEndtime() {
        return endtime;
    }

    public PaotuiCostTime withEndtime(Date endtime) {
        this.setEndtime(endtime);
        return this;
    }

    public void setEndtime(Date endtime) {
        this.endtime = endtime;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public PaotuiCostTime withFee(BigDecimal fee) {
        this.setFee(fee);
        return this;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public Integer getCostid() {
        return costid;
    }

    public PaotuiCostTime withCostid(Integer costid) {
        this.setCostid(costid);
        return this;
    }

    public void setCostid(Integer costid) {
        this.costid = costid;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", starttime=").append(starttime);
        sb.append(", endtime=").append(endtime);
        sb.append(", fee=").append(fee);
        sb.append(", costid=").append(costid);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}