package com.runfast.paotui.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.paotui.dao.model.AccountRoles;
import com.runfast.paotui.dao.model.AccountRolesExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AccountRolesMapper extends IMapper<AccountRoles, Integer, AccountRolesExample> {
}