package com.runfast.paotui.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.paotui.dao.model.Order;
import com.runfast.paotui.dao.model.OrderExample;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Mapper
public interface OrderMapper extends IMapper<Order, Integer, OrderExample> {
    List<Map<String,Integer>> findConfirmCancelOrder(Date time);
}