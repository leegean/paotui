package com.runfast.paotui.dao.model;

import java.io.Serializable;

public class AccountRoles implements Serializable {
    private Integer accountsId;

    private Integer rolesId;

    private static final long serialVersionUID = 1L;

    public Integer getAccountsId() {
        return accountsId;
    }

    public AccountRoles withAccountsId(Integer accountsId) {
        this.setAccountsId(accountsId);
        return this;
    }

    public void setAccountsId(Integer accountsId) {
        this.accountsId = accountsId;
    }

    public Integer getRolesId() {
        return rolesId;
    }

    public AccountRoles withRolesId(Integer rolesId) {
        this.setRolesId(rolesId);
        return this;
    }

    public void setRolesId(Integer rolesId) {
        this.rolesId = rolesId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", accountsId=").append(accountsId);
        sb.append(", rolesId=").append(rolesId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}