package com.runfast.paotui.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.paotui.dao.model.PaotuiCostDistance;
import com.runfast.paotui.dao.model.PaotuiCostDistanceExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PaotuiCostDistanceMapper extends IMapper<PaotuiCostDistance, Integer, PaotuiCostDistanceExample> {
}