package com.runfast.paotui.dao.model;

import java.io.Serializable;
import java.util.Date;

public class Charge implements Serializable {
    private Integer id;

    private Date createTime;

    private Date updateTime;

    /**
     * 应支付金额
     */
    private Integer amount;

    /**
     * 已退款金额
     */
    private Integer amountRefunded;

    /**
     * 支付渠道
     */
    private Integer channel;

    private String description;

    private String failureCode;

    private String failureMsg;

    private String orderNo;

    /**
     * 是否已支付
     */
    private Boolean paid;

    /**
     * 是否存在退款
     */
    private Boolean refunded;

    /**
     * 是否已撤销
     */
    private Boolean reversed;

    /**
     * 支付时间
     */
    private Date timePaid;

    /**
     * 流水单号
     */
    private String transactionNo;

    private Integer orderId;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public Charge withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public Charge withCreateTime(Date createTime) {
        this.setCreateTime(createTime);
        return this;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public Charge withUpdateTime(Date updateTime) {
        this.setUpdateTime(updateTime);
        return this;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getAmount() {
        return amount;
    }

    public Charge withAmount(Integer amount) {
        this.setAmount(amount);
        return this;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getAmountRefunded() {
        return amountRefunded;
    }

    public Charge withAmountRefunded(Integer amountRefunded) {
        this.setAmountRefunded(amountRefunded);
        return this;
    }

    public void setAmountRefunded(Integer amountRefunded) {
        this.amountRefunded = amountRefunded;
    }

    public Integer getChannel() {
        return channel;
    }

    public Charge withChannel(Integer channel) {
        this.setChannel(channel);
        return this;
    }

    public void setChannel(Integer channel) {
        this.channel = channel;
    }

    public String getDescription() {
        return description;
    }

    public Charge withDescription(String description) {
        this.setDescription(description);
        return this;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public String getFailureCode() {
        return failureCode;
    }

    public Charge withFailureCode(String failureCode) {
        this.setFailureCode(failureCode);
        return this;
    }

    public void setFailureCode(String failureCode) {
        this.failureCode = failureCode == null ? null : failureCode.trim();
    }

    public String getFailureMsg() {
        return failureMsg;
    }

    public Charge withFailureMsg(String failureMsg) {
        this.setFailureMsg(failureMsg);
        return this;
    }

    public void setFailureMsg(String failureMsg) {
        this.failureMsg = failureMsg == null ? null : failureMsg.trim();
    }

    public String getOrderNo() {
        return orderNo;
    }

    public Charge withOrderNo(String orderNo) {
        this.setOrderNo(orderNo);
        return this;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo == null ? null : orderNo.trim();
    }

    public Boolean getPaid() {
        return paid;
    }

    public Charge withPaid(Boolean paid) {
        this.setPaid(paid);
        return this;
    }

    public void setPaid(Boolean paid) {
        this.paid = paid;
    }

    public Boolean getRefunded() {
        return refunded;
    }

    public Charge withRefunded(Boolean refunded) {
        this.setRefunded(refunded);
        return this;
    }

    public void setRefunded(Boolean refunded) {
        this.refunded = refunded;
    }

    public Boolean getReversed() {
        return reversed;
    }

    public Charge withReversed(Boolean reversed) {
        this.setReversed(reversed);
        return this;
    }

    public void setReversed(Boolean reversed) {
        this.reversed = reversed;
    }

    public Date getTimePaid() {
        return timePaid;
    }

    public Charge withTimePaid(Date timePaid) {
        this.setTimePaid(timePaid);
        return this;
    }

    public void setTimePaid(Date timePaid) {
        this.timePaid = timePaid;
    }

    public String getTransactionNo() {
        return transactionNo;
    }

    public Charge withTransactionNo(String transactionNo) {
        this.setTransactionNo(transactionNo);
        return this;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo == null ? null : transactionNo.trim();
    }

    public Integer getOrderId() {
        return orderId;
    }

    public Charge withOrderId(Integer orderId) {
        this.setOrderId(orderId);
        return this;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", amount=").append(amount);
        sb.append(", amountRefunded=").append(amountRefunded);
        sb.append(", channel=").append(channel);
        sb.append(", description=").append(description);
        sb.append(", failureCode=").append(failureCode);
        sb.append(", failureMsg=").append(failureMsg);
        sb.append(", orderNo=").append(orderNo);
        sb.append(", paid=").append(paid);
        sb.append(", refunded=").append(refunded);
        sb.append(", reversed=").append(reversed);
        sb.append(", timePaid=").append(timePaid);
        sb.append(", transactionNo=").append(transactionNo);
        sb.append(", orderId=").append(orderId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}