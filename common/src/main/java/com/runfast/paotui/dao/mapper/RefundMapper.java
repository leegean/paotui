package com.runfast.paotui.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.paotui.dao.model.Refund;
import com.runfast.paotui.dao.model.RefundExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RefundMapper extends IMapper<Refund, Integer, RefundExample> {
}