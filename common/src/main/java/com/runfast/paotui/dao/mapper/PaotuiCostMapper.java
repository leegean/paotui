package com.runfast.paotui.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.paotui.dao.model.PaotuiCost;
import com.runfast.paotui.dao.model.PaotuiCostExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PaotuiCostMapper extends IMapper<PaotuiCost, Integer, PaotuiCostExample> {
}