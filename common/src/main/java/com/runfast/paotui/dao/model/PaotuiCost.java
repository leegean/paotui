package com.runfast.paotui.dao.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class PaotuiCost implements Serializable {
    private Integer id;

    /**
     * 创建时间
     */
    private Date createtime;

    /**
     * 基础配送距离(KM)
     */
    private Double basedistance;

    /**
     * 基础配送时间(分钟)
     */
    private Integer basictime;

    /**
     * 代购基础配送费
     */
    private BigDecimal daigoubasecost;

    /**
     * 取送件基础配送费
     */
    private BigDecimal qusongjianbasecost;

    /**
     * 加时距离
     */
    private Double overtimedistance;

    /**
     * 加时分钟
     */
    private Integer overtimetime;

    /**
     * 是否默认
     */
    private Integer isdefault;

    /**
     * 代理商id
     */
    private Integer agentid;

    /**
     * 代理商名字
     */
    private String agentname;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public PaotuiCost withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public PaotuiCost withCreatetime(Date createtime) {
        this.setCreatetime(createtime);
        return this;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public Double getBasedistance() {
        return basedistance;
    }

    public PaotuiCost withBasedistance(Double basedistance) {
        this.setBasedistance(basedistance);
        return this;
    }

    public void setBasedistance(Double basedistance) {
        this.basedistance = basedistance;
    }

    public Integer getBasictime() {
        return basictime;
    }

    public PaotuiCost withBasictime(Integer basictime) {
        this.setBasictime(basictime);
        return this;
    }

    public void setBasictime(Integer basictime) {
        this.basictime = basictime;
    }

    public BigDecimal getDaigoubasecost() {
        return daigoubasecost;
    }

    public PaotuiCost withDaigoubasecost(BigDecimal daigoubasecost) {
        this.setDaigoubasecost(daigoubasecost);
        return this;
    }

    public void setDaigoubasecost(BigDecimal daigoubasecost) {
        this.daigoubasecost = daigoubasecost;
    }

    public BigDecimal getQusongjianbasecost() {
        return qusongjianbasecost;
    }

    public PaotuiCost withQusongjianbasecost(BigDecimal qusongjianbasecost) {
        this.setQusongjianbasecost(qusongjianbasecost);
        return this;
    }

    public void setQusongjianbasecost(BigDecimal qusongjianbasecost) {
        this.qusongjianbasecost = qusongjianbasecost;
    }

    public Double getOvertimedistance() {
        return overtimedistance;
    }

    public PaotuiCost withOvertimedistance(Double overtimedistance) {
        this.setOvertimedistance(overtimedistance);
        return this;
    }

    public void setOvertimedistance(Double overtimedistance) {
        this.overtimedistance = overtimedistance;
    }

    public Integer getOvertimetime() {
        return overtimetime;
    }

    public PaotuiCost withOvertimetime(Integer overtimetime) {
        this.setOvertimetime(overtimetime);
        return this;
    }

    public void setOvertimetime(Integer overtimetime) {
        this.overtimetime = overtimetime;
    }

    public Integer getIsdefault() {
        return isdefault;
    }

    public PaotuiCost withIsdefault(Integer isdefault) {
        this.setIsdefault(isdefault);
        return this;
    }

    public void setIsdefault(Integer isdefault) {
        this.isdefault = isdefault;
    }

    public Integer getAgentid() {
        return agentid;
    }

    public PaotuiCost withAgentid(Integer agentid) {
        this.setAgentid(agentid);
        return this;
    }

    public void setAgentid(Integer agentid) {
        this.agentid = agentid;
    }

    public String getAgentname() {
        return agentname;
    }

    public PaotuiCost withAgentname(String agentname) {
        this.setAgentname(agentname);
        return this;
    }

    public void setAgentname(String agentname) {
        this.agentname = agentname == null ? null : agentname.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", createtime=").append(createtime);
        sb.append(", basedistance=").append(basedistance);
        sb.append(", basictime=").append(basictime);
        sb.append(", daigoubasecost=").append(daigoubasecost);
        sb.append(", qusongjianbasecost=").append(qusongjianbasecost);
        sb.append(", overtimedistance=").append(overtimedistance);
        sb.append(", overtimetime=").append(overtimetime);
        sb.append(", isdefault=").append(isdefault);
        sb.append(", agentid=").append(agentid);
        sb.append(", agentname=").append(agentname);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}