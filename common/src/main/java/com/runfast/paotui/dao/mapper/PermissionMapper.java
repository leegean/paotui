package com.runfast.paotui.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.paotui.dao.model.Permission;
import com.runfast.paotui.dao.model.PermissionExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PermissionMapper extends IMapper<Permission, Integer, PermissionExample> {
}