package com.runfast.paotui.dao.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Catalog implements Serializable {
    private Integer id;

    private Boolean deleted;

    private Date createTime;

    private Date updateTime;

    private Integer createBy;

    private Integer updateBy;

    /**
     * 分类名
     */
    private String name;

    /**
     * 是否热门
     */
    private Boolean hot;

    /**
     * 建议（逗号分隔）

     */
    private List<String> suggestions;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public Catalog withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public Catalog withDeleted(Boolean deleted) {
        this.setDeleted(deleted);
        return this;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public Catalog withCreateTime(Date createTime) {
        this.setCreateTime(createTime);
        return this;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public Catalog withUpdateTime(Date updateTime) {
        this.setUpdateTime(updateTime);
        return this;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getCreateBy() {
        return createBy;
    }

    public Catalog withCreateBy(Integer createBy) {
        this.setCreateBy(createBy);
        return this;
    }

    public void setCreateBy(Integer createBy) {
        this.createBy = createBy;
    }

    public Integer getUpdateBy() {
        return updateBy;
    }

    public Catalog withUpdateBy(Integer updateBy) {
        this.setUpdateBy(updateBy);
        return this;
    }

    public void setUpdateBy(Integer updateBy) {
        this.updateBy = updateBy;
    }

    public String getName() {
        return name;
    }

    public Catalog withName(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Boolean getHot() {
        return hot;
    }

    public Catalog withHot(Boolean hot) {
        this.setHot(hot);
        return this;
    }

    public void setHot(Boolean hot) {
        this.hot = hot;
    }

    public List<String> getSuggestions() {
        return suggestions;
    }

    public Catalog withSuggestions(List<String> suggestions) {
        this.setSuggestions(suggestions);
        return this;
    }

    public void setSuggestions(List<String> suggestions) {
        this.suggestions = suggestions;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", deleted=").append(deleted);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", createBy=").append(createBy);
        sb.append(", updateBy=").append(updateBy);
        sb.append(", name=").append(name);
        sb.append(", hot=").append(hot);
        sb.append(", suggestions=").append(suggestions);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}