package com.runfast.paotui.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.paotui.dao.model.PaotuiCostWeight;
import com.runfast.paotui.dao.model.PaotuiCostWeightExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PaotuiCostWeightMapper extends IMapper<PaotuiCostWeight, Integer, PaotuiCostWeightExample> {
}