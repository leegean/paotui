package com.runfast.paotui.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.paotui.dao.model.PaotuiCostTime;
import com.runfast.paotui.dao.model.PaotuiCostTimeExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PaotuiCostTimeMapper extends IMapper<PaotuiCostTime, Integer, PaotuiCostTimeExample> {
}