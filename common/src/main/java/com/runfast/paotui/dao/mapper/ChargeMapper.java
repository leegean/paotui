package com.runfast.paotui.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.paotui.dao.model.Charge;
import com.runfast.paotui.dao.model.ChargeExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ChargeMapper extends IMapper<Charge, Integer, ChargeExample> {
}