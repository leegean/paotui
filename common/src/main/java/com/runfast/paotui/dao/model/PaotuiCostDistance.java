package com.runfast.paotui.dao.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class PaotuiCostDistance implements Serializable {
    private Integer id;

    /**
     * 起始距离
     */
    private Double startdistance;

    /**
     * 结束距离
     */
    private Double enddistance;

    /**
     * 每公里加收费用
     */
    private BigDecimal fee;

    /**
     * 跑腿计费模板id
     */
    private Integer costid;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public PaotuiCostDistance withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getStartdistance() {
        return startdistance;
    }

    public PaotuiCostDistance withStartdistance(Double startdistance) {
        this.setStartdistance(startdistance);
        return this;
    }

    public void setStartdistance(Double startdistance) {
        this.startdistance = startdistance;
    }

    public Double getEnddistance() {
        return enddistance;
    }

    public PaotuiCostDistance withEnddistance(Double enddistance) {
        this.setEnddistance(enddistance);
        return this;
    }

    public void setEnddistance(Double enddistance) {
        this.enddistance = enddistance;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public PaotuiCostDistance withFee(BigDecimal fee) {
        this.setFee(fee);
        return this;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public Integer getCostid() {
        return costid;
    }

    public PaotuiCostDistance withCostid(Integer costid) {
        this.setCostid(costid);
        return this;
    }

    public void setCostid(Integer costid) {
        this.costid = costid;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", startdistance=").append(startdistance);
        sb.append(", enddistance=").append(enddistance);
        sb.append(", fee=").append(fee);
        sb.append(", costid=").append(costid);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}