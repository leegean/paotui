package com.runfast.paotui.dao.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OrderExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public OrderExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("update_time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("update_time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("update_time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("update_time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("update_time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("update_time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("update_time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("update_time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("update_time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("update_time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("update_time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("update_time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andAmountPaidIsNull() {
            addCriterion("amount_paid is null");
            return (Criteria) this;
        }

        public Criteria andAmountPaidIsNotNull() {
            addCriterion("amount_paid is not null");
            return (Criteria) this;
        }

        public Criteria andAmountPaidEqualTo(Integer value) {
            addCriterion("amount_paid =", value, "amountPaid");
            return (Criteria) this;
        }

        public Criteria andAmountPaidNotEqualTo(Integer value) {
            addCriterion("amount_paid <>", value, "amountPaid");
            return (Criteria) this;
        }

        public Criteria andAmountPaidGreaterThan(Integer value) {
            addCriterion("amount_paid >", value, "amountPaid");
            return (Criteria) this;
        }

        public Criteria andAmountPaidGreaterThanOrEqualTo(Integer value) {
            addCriterion("amount_paid >=", value, "amountPaid");
            return (Criteria) this;
        }

        public Criteria andAmountPaidLessThan(Integer value) {
            addCriterion("amount_paid <", value, "amountPaid");
            return (Criteria) this;
        }

        public Criteria andAmountPaidLessThanOrEqualTo(Integer value) {
            addCriterion("amount_paid <=", value, "amountPaid");
            return (Criteria) this;
        }

        public Criteria andAmountPaidIn(List<Integer> values) {
            addCriterion("amount_paid in", values, "amountPaid");
            return (Criteria) this;
        }

        public Criteria andAmountPaidNotIn(List<Integer> values) {
            addCriterion("amount_paid not in", values, "amountPaid");
            return (Criteria) this;
        }

        public Criteria andAmountPaidBetween(Integer value1, Integer value2) {
            addCriterion("amount_paid between", value1, value2, "amountPaid");
            return (Criteria) this;
        }

        public Criteria andAmountPaidNotBetween(Integer value1, Integer value2) {
            addCriterion("amount_paid not between", value1, value2, "amountPaid");
            return (Criteria) this;
        }

        public Criteria andAmountPayableIsNull() {
            addCriterion("amount_payable is null");
            return (Criteria) this;
        }

        public Criteria andAmountPayableIsNotNull() {
            addCriterion("amount_payable is not null");
            return (Criteria) this;
        }

        public Criteria andAmountPayableEqualTo(Integer value) {
            addCriterion("amount_payable =", value, "amountPayable");
            return (Criteria) this;
        }

        public Criteria andAmountPayableNotEqualTo(Integer value) {
            addCriterion("amount_payable <>", value, "amountPayable");
            return (Criteria) this;
        }

        public Criteria andAmountPayableGreaterThan(Integer value) {
            addCriterion("amount_payable >", value, "amountPayable");
            return (Criteria) this;
        }

        public Criteria andAmountPayableGreaterThanOrEqualTo(Integer value) {
            addCriterion("amount_payable >=", value, "amountPayable");
            return (Criteria) this;
        }

        public Criteria andAmountPayableLessThan(Integer value) {
            addCriterion("amount_payable <", value, "amountPayable");
            return (Criteria) this;
        }

        public Criteria andAmountPayableLessThanOrEqualTo(Integer value) {
            addCriterion("amount_payable <=", value, "amountPayable");
            return (Criteria) this;
        }

        public Criteria andAmountPayableIn(List<Integer> values) {
            addCriterion("amount_payable in", values, "amountPayable");
            return (Criteria) this;
        }

        public Criteria andAmountPayableNotIn(List<Integer> values) {
            addCriterion("amount_payable not in", values, "amountPayable");
            return (Criteria) this;
        }

        public Criteria andAmountPayableBetween(Integer value1, Integer value2) {
            addCriterion("amount_payable between", value1, value2, "amountPayable");
            return (Criteria) this;
        }

        public Criteria andAmountPayableNotBetween(Integer value1, Integer value2) {
            addCriterion("amount_payable not between", value1, value2, "amountPayable");
            return (Criteria) this;
        }

        public Criteria andDeliveryCostIsNull() {
            addCriterion("delivery_cost is null");
            return (Criteria) this;
        }

        public Criteria andDeliveryCostIsNotNull() {
            addCriterion("delivery_cost is not null");
            return (Criteria) this;
        }

        public Criteria andDeliveryCostEqualTo(Integer value) {
            addCriterion("delivery_cost =", value, "deliveryCost");
            return (Criteria) this;
        }

        public Criteria andDeliveryCostNotEqualTo(Integer value) {
            addCriterion("delivery_cost <>", value, "deliveryCost");
            return (Criteria) this;
        }

        public Criteria andDeliveryCostGreaterThan(Integer value) {
            addCriterion("delivery_cost >", value, "deliveryCost");
            return (Criteria) this;
        }

        public Criteria andDeliveryCostGreaterThanOrEqualTo(Integer value) {
            addCriterion("delivery_cost >=", value, "deliveryCost");
            return (Criteria) this;
        }

        public Criteria andDeliveryCostLessThan(Integer value) {
            addCriterion("delivery_cost <", value, "deliveryCost");
            return (Criteria) this;
        }

        public Criteria andDeliveryCostLessThanOrEqualTo(Integer value) {
            addCriterion("delivery_cost <=", value, "deliveryCost");
            return (Criteria) this;
        }

        public Criteria andDeliveryCostIn(List<Integer> values) {
            addCriterion("delivery_cost in", values, "deliveryCost");
            return (Criteria) this;
        }

        public Criteria andDeliveryCostNotIn(List<Integer> values) {
            addCriterion("delivery_cost not in", values, "deliveryCost");
            return (Criteria) this;
        }

        public Criteria andDeliveryCostBetween(Integer value1, Integer value2) {
            addCriterion("delivery_cost between", value1, value2, "deliveryCost");
            return (Criteria) this;
        }

        public Criteria andDeliveryCostNotBetween(Integer value1, Integer value2) {
            addCriterion("delivery_cost not between", value1, value2, "deliveryCost");
            return (Criteria) this;
        }

        public Criteria andFromAddressIsNull() {
            addCriterion("from_address is null");
            return (Criteria) this;
        }

        public Criteria andFromAddressIsNotNull() {
            addCriterion("from_address is not null");
            return (Criteria) this;
        }

        public Criteria andFromAddressEqualTo(String value) {
            addCriterion("from_address =", value, "fromAddress");
            return (Criteria) this;
        }

        public Criteria andFromAddressNotEqualTo(String value) {
            addCriterion("from_address <>", value, "fromAddress");
            return (Criteria) this;
        }

        public Criteria andFromAddressGreaterThan(String value) {
            addCriterion("from_address >", value, "fromAddress");
            return (Criteria) this;
        }

        public Criteria andFromAddressGreaterThanOrEqualTo(String value) {
            addCriterion("from_address >=", value, "fromAddress");
            return (Criteria) this;
        }

        public Criteria andFromAddressLessThan(String value) {
            addCriterion("from_address <", value, "fromAddress");
            return (Criteria) this;
        }

        public Criteria andFromAddressLessThanOrEqualTo(String value) {
            addCriterion("from_address <=", value, "fromAddress");
            return (Criteria) this;
        }

        public Criteria andFromAddressLike(String value) {
            addCriterion("from_address like", value, "fromAddress");
            return (Criteria) this;
        }

        public Criteria andFromAddressNotLike(String value) {
            addCriterion("from_address not like", value, "fromAddress");
            return (Criteria) this;
        }

        public Criteria andFromAddressIn(List<String> values) {
            addCriterion("from_address in", values, "fromAddress");
            return (Criteria) this;
        }

        public Criteria andFromAddressNotIn(List<String> values) {
            addCriterion("from_address not in", values, "fromAddress");
            return (Criteria) this;
        }

        public Criteria andFromAddressBetween(String value1, String value2) {
            addCriterion("from_address between", value1, value2, "fromAddress");
            return (Criteria) this;
        }

        public Criteria andFromAddressNotBetween(String value1, String value2) {
            addCriterion("from_address not between", value1, value2, "fromAddress");
            return (Criteria) this;
        }

        public Criteria andFromLatIsNull() {
            addCriterion("from_lat is null");
            return (Criteria) this;
        }

        public Criteria andFromLatIsNotNull() {
            addCriterion("from_lat is not null");
            return (Criteria) this;
        }

        public Criteria andFromLatEqualTo(Double value) {
            addCriterion("from_lat =", value, "fromLat");
            return (Criteria) this;
        }

        public Criteria andFromLatNotEqualTo(Double value) {
            addCriterion("from_lat <>", value, "fromLat");
            return (Criteria) this;
        }

        public Criteria andFromLatGreaterThan(Double value) {
            addCriterion("from_lat >", value, "fromLat");
            return (Criteria) this;
        }

        public Criteria andFromLatGreaterThanOrEqualTo(Double value) {
            addCriterion("from_lat >=", value, "fromLat");
            return (Criteria) this;
        }

        public Criteria andFromLatLessThan(Double value) {
            addCriterion("from_lat <", value, "fromLat");
            return (Criteria) this;
        }

        public Criteria andFromLatLessThanOrEqualTo(Double value) {
            addCriterion("from_lat <=", value, "fromLat");
            return (Criteria) this;
        }

        public Criteria andFromLatIn(List<Double> values) {
            addCriterion("from_lat in", values, "fromLat");
            return (Criteria) this;
        }

        public Criteria andFromLatNotIn(List<Double> values) {
            addCriterion("from_lat not in", values, "fromLat");
            return (Criteria) this;
        }

        public Criteria andFromLatBetween(Double value1, Double value2) {
            addCriterion("from_lat between", value1, value2, "fromLat");
            return (Criteria) this;
        }

        public Criteria andFromLatNotBetween(Double value1, Double value2) {
            addCriterion("from_lat not between", value1, value2, "fromLat");
            return (Criteria) this;
        }

        public Criteria andFromLngIsNull() {
            addCriterion("from_lng is null");
            return (Criteria) this;
        }

        public Criteria andFromLngIsNotNull() {
            addCriterion("from_lng is not null");
            return (Criteria) this;
        }

        public Criteria andFromLngEqualTo(Double value) {
            addCriterion("from_lng =", value, "fromLng");
            return (Criteria) this;
        }

        public Criteria andFromLngNotEqualTo(Double value) {
            addCriterion("from_lng <>", value, "fromLng");
            return (Criteria) this;
        }

        public Criteria andFromLngGreaterThan(Double value) {
            addCriterion("from_lng >", value, "fromLng");
            return (Criteria) this;
        }

        public Criteria andFromLngGreaterThanOrEqualTo(Double value) {
            addCriterion("from_lng >=", value, "fromLng");
            return (Criteria) this;
        }

        public Criteria andFromLngLessThan(Double value) {
            addCriterion("from_lng <", value, "fromLng");
            return (Criteria) this;
        }

        public Criteria andFromLngLessThanOrEqualTo(Double value) {
            addCriterion("from_lng <=", value, "fromLng");
            return (Criteria) this;
        }

        public Criteria andFromLngIn(List<Double> values) {
            addCriterion("from_lng in", values, "fromLng");
            return (Criteria) this;
        }

        public Criteria andFromLngNotIn(List<Double> values) {
            addCriterion("from_lng not in", values, "fromLng");
            return (Criteria) this;
        }

        public Criteria andFromLngBetween(Double value1, Double value2) {
            addCriterion("from_lng between", value1, value2, "fromLng");
            return (Criteria) this;
        }

        public Criteria andFromLngNotBetween(Double value1, Double value2) {
            addCriterion("from_lng not between", value1, value2, "fromLng");
            return (Criteria) this;
        }

        public Criteria andFromTypeIsNull() {
            addCriterion("from_type is null");
            return (Criteria) this;
        }

        public Criteria andFromTypeIsNotNull() {
            addCriterion("from_type is not null");
            return (Criteria) this;
        }

        public Criteria andFromTypeEqualTo(Integer value) {
            addCriterion("from_type =", value, "fromType");
            return (Criteria) this;
        }

        public Criteria andFromTypeNotEqualTo(Integer value) {
            addCriterion("from_type <>", value, "fromType");
            return (Criteria) this;
        }

        public Criteria andFromTypeGreaterThan(Integer value) {
            addCriterion("from_type >", value, "fromType");
            return (Criteria) this;
        }

        public Criteria andFromTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("from_type >=", value, "fromType");
            return (Criteria) this;
        }

        public Criteria andFromTypeLessThan(Integer value) {
            addCriterion("from_type <", value, "fromType");
            return (Criteria) this;
        }

        public Criteria andFromTypeLessThanOrEqualTo(Integer value) {
            addCriterion("from_type <=", value, "fromType");
            return (Criteria) this;
        }

        public Criteria andFromTypeIn(List<Integer> values) {
            addCriterion("from_type in", values, "fromType");
            return (Criteria) this;
        }

        public Criteria andFromTypeNotIn(List<Integer> values) {
            addCriterion("from_type not in", values, "fromType");
            return (Criteria) this;
        }

        public Criteria andFromTypeBetween(Integer value1, Integer value2) {
            addCriterion("from_type between", value1, value2, "fromType");
            return (Criteria) this;
        }

        public Criteria andFromTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("from_type not between", value1, value2, "fromType");
            return (Criteria) this;
        }

        public Criteria andGoodsDescriptionIsNull() {
            addCriterion("goods_description is null");
            return (Criteria) this;
        }

        public Criteria andGoodsDescriptionIsNotNull() {
            addCriterion("goods_description is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsDescriptionEqualTo(String value) {
            addCriterion("goods_description =", value, "goodsDescription");
            return (Criteria) this;
        }

        public Criteria andGoodsDescriptionNotEqualTo(String value) {
            addCriterion("goods_description <>", value, "goodsDescription");
            return (Criteria) this;
        }

        public Criteria andGoodsDescriptionGreaterThan(String value) {
            addCriterion("goods_description >", value, "goodsDescription");
            return (Criteria) this;
        }

        public Criteria andGoodsDescriptionGreaterThanOrEqualTo(String value) {
            addCriterion("goods_description >=", value, "goodsDescription");
            return (Criteria) this;
        }

        public Criteria andGoodsDescriptionLessThan(String value) {
            addCriterion("goods_description <", value, "goodsDescription");
            return (Criteria) this;
        }

        public Criteria andGoodsDescriptionLessThanOrEqualTo(String value) {
            addCriterion("goods_description <=", value, "goodsDescription");
            return (Criteria) this;
        }

        public Criteria andGoodsDescriptionLike(String value) {
            addCriterion("goods_description like", value, "goodsDescription");
            return (Criteria) this;
        }

        public Criteria andGoodsDescriptionNotLike(String value) {
            addCriterion("goods_description not like", value, "goodsDescription");
            return (Criteria) this;
        }

        public Criteria andGoodsDescriptionIn(List<String> values) {
            addCriterion("goods_description in", values, "goodsDescription");
            return (Criteria) this;
        }

        public Criteria andGoodsDescriptionNotIn(List<String> values) {
            addCriterion("goods_description not in", values, "goodsDescription");
            return (Criteria) this;
        }

        public Criteria andGoodsDescriptionBetween(String value1, String value2) {
            addCriterion("goods_description between", value1, value2, "goodsDescription");
            return (Criteria) this;
        }

        public Criteria andGoodsDescriptionNotBetween(String value1, String value2) {
            addCriterion("goods_description not between", value1, value2, "goodsDescription");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeIsNull() {
            addCriterion("goods_type is null");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeIsNotNull() {
            addCriterion("goods_type is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeEqualTo(String value) {
            addCriterion("goods_type =", value, "goodsType");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeNotEqualTo(String value) {
            addCriterion("goods_type <>", value, "goodsType");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeGreaterThan(String value) {
            addCriterion("goods_type >", value, "goodsType");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeGreaterThanOrEqualTo(String value) {
            addCriterion("goods_type >=", value, "goodsType");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeLessThan(String value) {
            addCriterion("goods_type <", value, "goodsType");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeLessThanOrEqualTo(String value) {
            addCriterion("goods_type <=", value, "goodsType");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeLike(String value) {
            addCriterion("goods_type like", value, "goodsType");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeNotLike(String value) {
            addCriterion("goods_type not like", value, "goodsType");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeIn(List<String> values) {
            addCriterion("goods_type in", values, "goodsType");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeNotIn(List<String> values) {
            addCriterion("goods_type not in", values, "goodsType");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeBetween(String value1, String value2) {
            addCriterion("goods_type between", value1, value2, "goodsType");
            return (Criteria) this;
        }

        public Criteria andGoodsTypeNotBetween(String value1, String value2) {
            addCriterion("goods_type not between", value1, value2, "goodsType");
            return (Criteria) this;
        }

        public Criteria andGoodsWeightIsNull() {
            addCriterion("goods_weight is null");
            return (Criteria) this;
        }

        public Criteria andGoodsWeightIsNotNull() {
            addCriterion("goods_weight is not null");
            return (Criteria) this;
        }

        public Criteria andGoodsWeightEqualTo(Integer value) {
            addCriterion("goods_weight =", value, "goodsWeight");
            return (Criteria) this;
        }

        public Criteria andGoodsWeightNotEqualTo(Integer value) {
            addCriterion("goods_weight <>", value, "goodsWeight");
            return (Criteria) this;
        }

        public Criteria andGoodsWeightGreaterThan(Integer value) {
            addCriterion("goods_weight >", value, "goodsWeight");
            return (Criteria) this;
        }

        public Criteria andGoodsWeightGreaterThanOrEqualTo(Integer value) {
            addCriterion("goods_weight >=", value, "goodsWeight");
            return (Criteria) this;
        }

        public Criteria andGoodsWeightLessThan(Integer value) {
            addCriterion("goods_weight <", value, "goodsWeight");
            return (Criteria) this;
        }

        public Criteria andGoodsWeightLessThanOrEqualTo(Integer value) {
            addCriterion("goods_weight <=", value, "goodsWeight");
            return (Criteria) this;
        }

        public Criteria andGoodsWeightIn(List<Integer> values) {
            addCriterion("goods_weight in", values, "goodsWeight");
            return (Criteria) this;
        }

        public Criteria andGoodsWeightNotIn(List<Integer> values) {
            addCriterion("goods_weight not in", values, "goodsWeight");
            return (Criteria) this;
        }

        public Criteria andGoodsWeightBetween(Integer value1, Integer value2) {
            addCriterion("goods_weight between", value1, value2, "goodsWeight");
            return (Criteria) this;
        }

        public Criteria andGoodsWeightNotBetween(Integer value1, Integer value2) {
            addCriterion("goods_weight not between", value1, value2, "goodsWeight");
            return (Criteria) this;
        }

        public Criteria andInsuranceIsNull() {
            addCriterion("insurance is null");
            return (Criteria) this;
        }

        public Criteria andInsuranceIsNotNull() {
            addCriterion("insurance is not null");
            return (Criteria) this;
        }

        public Criteria andInsuranceEqualTo(Integer value) {
            addCriterion("insurance =", value, "insurance");
            return (Criteria) this;
        }

        public Criteria andInsuranceNotEqualTo(Integer value) {
            addCriterion("insurance <>", value, "insurance");
            return (Criteria) this;
        }

        public Criteria andInsuranceGreaterThan(Integer value) {
            addCriterion("insurance >", value, "insurance");
            return (Criteria) this;
        }

        public Criteria andInsuranceGreaterThanOrEqualTo(Integer value) {
            addCriterion("insurance >=", value, "insurance");
            return (Criteria) this;
        }

        public Criteria andInsuranceLessThan(Integer value) {
            addCriterion("insurance <", value, "insurance");
            return (Criteria) this;
        }

        public Criteria andInsuranceLessThanOrEqualTo(Integer value) {
            addCriterion("insurance <=", value, "insurance");
            return (Criteria) this;
        }

        public Criteria andInsuranceIn(List<Integer> values) {
            addCriterion("insurance in", values, "insurance");
            return (Criteria) this;
        }

        public Criteria andInsuranceNotIn(List<Integer> values) {
            addCriterion("insurance not in", values, "insurance");
            return (Criteria) this;
        }

        public Criteria andInsuranceBetween(Integer value1, Integer value2) {
            addCriterion("insurance between", value1, value2, "insurance");
            return (Criteria) this;
        }

        public Criteria andInsuranceNotBetween(Integer value1, Integer value2) {
            addCriterion("insurance not between", value1, value2, "insurance");
            return (Criteria) this;
        }

        public Criteria andOrderNoIsNull() {
            addCriterion("order_no is null");
            return (Criteria) this;
        }

        public Criteria andOrderNoIsNotNull() {
            addCriterion("order_no is not null");
            return (Criteria) this;
        }

        public Criteria andOrderNoEqualTo(String value) {
            addCriterion("order_no =", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoNotEqualTo(String value) {
            addCriterion("order_no <>", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoGreaterThan(String value) {
            addCriterion("order_no >", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoGreaterThanOrEqualTo(String value) {
            addCriterion("order_no >=", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoLessThan(String value) {
            addCriterion("order_no <", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoLessThanOrEqualTo(String value) {
            addCriterion("order_no <=", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoLike(String value) {
            addCriterion("order_no like", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoNotLike(String value) {
            addCriterion("order_no not like", value, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoIn(List<String> values) {
            addCriterion("order_no in", values, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoNotIn(List<String> values) {
            addCriterion("order_no not in", values, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoBetween(String value1, String value2) {
            addCriterion("order_no between", value1, value2, "orderNo");
            return (Criteria) this;
        }

        public Criteria andOrderNoNotBetween(String value1, String value2) {
            addCriterion("order_no not between", value1, value2, "orderNo");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andPickTimeIsNull() {
            addCriterion("pick_time is null");
            return (Criteria) this;
        }

        public Criteria andPickTimeIsNotNull() {
            addCriterion("pick_time is not null");
            return (Criteria) this;
        }

        public Criteria andPickTimeEqualTo(Date value) {
            addCriterion("pick_time =", value, "pickTime");
            return (Criteria) this;
        }

        public Criteria andPickTimeNotEqualTo(Date value) {
            addCriterion("pick_time <>", value, "pickTime");
            return (Criteria) this;
        }

        public Criteria andPickTimeGreaterThan(Date value) {
            addCriterion("pick_time >", value, "pickTime");
            return (Criteria) this;
        }

        public Criteria andPickTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("pick_time >=", value, "pickTime");
            return (Criteria) this;
        }

        public Criteria andPickTimeLessThan(Date value) {
            addCriterion("pick_time <", value, "pickTime");
            return (Criteria) this;
        }

        public Criteria andPickTimeLessThanOrEqualTo(Date value) {
            addCriterion("pick_time <=", value, "pickTime");
            return (Criteria) this;
        }

        public Criteria andPickTimeIn(List<Date> values) {
            addCriterion("pick_time in", values, "pickTime");
            return (Criteria) this;
        }

        public Criteria andPickTimeNotIn(List<Date> values) {
            addCriterion("pick_time not in", values, "pickTime");
            return (Criteria) this;
        }

        public Criteria andPickTimeBetween(Date value1, Date value2) {
            addCriterion("pick_time between", value1, value2, "pickTime");
            return (Criteria) this;
        }

        public Criteria andPickTimeNotBetween(Date value1, Date value2) {
            addCriterion("pick_time not between", value1, value2, "pickTime");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("remark is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("remark is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("remark =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("remark <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("remark >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("remark >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("remark <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("remark <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("remark like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("remark not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("remark in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("remark not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("remark between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("remark not between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andSuggestionCostIsNull() {
            addCriterion("suggestion_cost is null");
            return (Criteria) this;
        }

        public Criteria andSuggestionCostIsNotNull() {
            addCriterion("suggestion_cost is not null");
            return (Criteria) this;
        }

        public Criteria andSuggestionCostEqualTo(Integer value) {
            addCriterion("suggestion_cost =", value, "suggestionCost");
            return (Criteria) this;
        }

        public Criteria andSuggestionCostNotEqualTo(Integer value) {
            addCriterion("suggestion_cost <>", value, "suggestionCost");
            return (Criteria) this;
        }

        public Criteria andSuggestionCostGreaterThan(Integer value) {
            addCriterion("suggestion_cost >", value, "suggestionCost");
            return (Criteria) this;
        }

        public Criteria andSuggestionCostGreaterThanOrEqualTo(Integer value) {
            addCriterion("suggestion_cost >=", value, "suggestionCost");
            return (Criteria) this;
        }

        public Criteria andSuggestionCostLessThan(Integer value) {
            addCriterion("suggestion_cost <", value, "suggestionCost");
            return (Criteria) this;
        }

        public Criteria andSuggestionCostLessThanOrEqualTo(Integer value) {
            addCriterion("suggestion_cost <=", value, "suggestionCost");
            return (Criteria) this;
        }

        public Criteria andSuggestionCostIn(List<Integer> values) {
            addCriterion("suggestion_cost in", values, "suggestionCost");
            return (Criteria) this;
        }

        public Criteria andSuggestionCostNotIn(List<Integer> values) {
            addCriterion("suggestion_cost not in", values, "suggestionCost");
            return (Criteria) this;
        }

        public Criteria andSuggestionCostBetween(Integer value1, Integer value2) {
            addCriterion("suggestion_cost between", value1, value2, "suggestionCost");
            return (Criteria) this;
        }

        public Criteria andSuggestionCostNotBetween(Integer value1, Integer value2) {
            addCriterion("suggestion_cost not between", value1, value2, "suggestionCost");
            return (Criteria) this;
        }

        public Criteria andTipIsNull() {
            addCriterion("tip is null");
            return (Criteria) this;
        }

        public Criteria andTipIsNotNull() {
            addCriterion("tip is not null");
            return (Criteria) this;
        }

        public Criteria andTipEqualTo(Integer value) {
            addCriterion("tip =", value, "tip");
            return (Criteria) this;
        }

        public Criteria andTipNotEqualTo(Integer value) {
            addCriterion("tip <>", value, "tip");
            return (Criteria) this;
        }

        public Criteria andTipGreaterThan(Integer value) {
            addCriterion("tip >", value, "tip");
            return (Criteria) this;
        }

        public Criteria andTipGreaterThanOrEqualTo(Integer value) {
            addCriterion("tip >=", value, "tip");
            return (Criteria) this;
        }

        public Criteria andTipLessThan(Integer value) {
            addCriterion("tip <", value, "tip");
            return (Criteria) this;
        }

        public Criteria andTipLessThanOrEqualTo(Integer value) {
            addCriterion("tip <=", value, "tip");
            return (Criteria) this;
        }

        public Criteria andTipIn(List<Integer> values) {
            addCriterion("tip in", values, "tip");
            return (Criteria) this;
        }

        public Criteria andTipNotIn(List<Integer> values) {
            addCriterion("tip not in", values, "tip");
            return (Criteria) this;
        }

        public Criteria andTipBetween(Integer value1, Integer value2) {
            addCriterion("tip between", value1, value2, "tip");
            return (Criteria) this;
        }

        public Criteria andTipNotBetween(Integer value1, Integer value2) {
            addCriterion("tip not between", value1, value2, "tip");
            return (Criteria) this;
        }

        public Criteria andCuserIdIsNull() {
            addCriterion("cuser_id is null");
            return (Criteria) this;
        }

        public Criteria andCuserIdIsNotNull() {
            addCriterion("cuser_id is not null");
            return (Criteria) this;
        }

        public Criteria andCuserIdEqualTo(Integer value) {
            addCriterion("cuser_id =", value, "cuserId");
            return (Criteria) this;
        }

        public Criteria andCuserIdNotEqualTo(Integer value) {
            addCriterion("cuser_id <>", value, "cuserId");
            return (Criteria) this;
        }

        public Criteria andCuserIdGreaterThan(Integer value) {
            addCriterion("cuser_id >", value, "cuserId");
            return (Criteria) this;
        }

        public Criteria andCuserIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("cuser_id >=", value, "cuserId");
            return (Criteria) this;
        }

        public Criteria andCuserIdLessThan(Integer value) {
            addCriterion("cuser_id <", value, "cuserId");
            return (Criteria) this;
        }

        public Criteria andCuserIdLessThanOrEqualTo(Integer value) {
            addCriterion("cuser_id <=", value, "cuserId");
            return (Criteria) this;
        }

        public Criteria andCuserIdIn(List<Integer> values) {
            addCriterion("cuser_id in", values, "cuserId");
            return (Criteria) this;
        }

        public Criteria andCuserIdNotIn(List<Integer> values) {
            addCriterion("cuser_id not in", values, "cuserId");
            return (Criteria) this;
        }

        public Criteria andCuserIdBetween(Integer value1, Integer value2) {
            addCriterion("cuser_id between", value1, value2, "cuserId");
            return (Criteria) this;
        }

        public Criteria andCuserIdNotBetween(Integer value1, Integer value2) {
            addCriterion("cuser_id not between", value1, value2, "cuserId");
            return (Criteria) this;
        }

        public Criteria andDriverIdIsNull() {
            addCriterion("driver_id is null");
            return (Criteria) this;
        }

        public Criteria andDriverIdIsNotNull() {
            addCriterion("driver_id is not null");
            return (Criteria) this;
        }

        public Criteria andDriverIdEqualTo(Integer value) {
            addCriterion("driver_id =", value, "driverId");
            return (Criteria) this;
        }

        public Criteria andDriverIdNotEqualTo(Integer value) {
            addCriterion("driver_id <>", value, "driverId");
            return (Criteria) this;
        }

        public Criteria andDriverIdGreaterThan(Integer value) {
            addCriterion("driver_id >", value, "driverId");
            return (Criteria) this;
        }

        public Criteria andDriverIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("driver_id >=", value, "driverId");
            return (Criteria) this;
        }

        public Criteria andDriverIdLessThan(Integer value) {
            addCriterion("driver_id <", value, "driverId");
            return (Criteria) this;
        }

        public Criteria andDriverIdLessThanOrEqualTo(Integer value) {
            addCriterion("driver_id <=", value, "driverId");
            return (Criteria) this;
        }

        public Criteria andDriverIdIn(List<Integer> values) {
            addCriterion("driver_id in", values, "driverId");
            return (Criteria) this;
        }

        public Criteria andDriverIdNotIn(List<Integer> values) {
            addCriterion("driver_id not in", values, "driverId");
            return (Criteria) this;
        }

        public Criteria andDriverIdBetween(Integer value1, Integer value2) {
            addCriterion("driver_id between", value1, value2, "driverId");
            return (Criteria) this;
        }

        public Criteria andDriverIdNotBetween(Integer value1, Integer value2) {
            addCriterion("driver_id not between", value1, value2, "driverId");
            return (Criteria) this;
        }

        public Criteria andFromIdIsNull() {
            addCriterion("from_id is null");
            return (Criteria) this;
        }

        public Criteria andFromIdIsNotNull() {
            addCriterion("from_id is not null");
            return (Criteria) this;
        }

        public Criteria andFromIdEqualTo(Integer value) {
            addCriterion("from_id =", value, "fromId");
            return (Criteria) this;
        }

        public Criteria andFromIdNotEqualTo(Integer value) {
            addCriterion("from_id <>", value, "fromId");
            return (Criteria) this;
        }

        public Criteria andFromIdGreaterThan(Integer value) {
            addCriterion("from_id >", value, "fromId");
            return (Criteria) this;
        }

        public Criteria andFromIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("from_id >=", value, "fromId");
            return (Criteria) this;
        }

        public Criteria andFromIdLessThan(Integer value) {
            addCriterion("from_id <", value, "fromId");
            return (Criteria) this;
        }

        public Criteria andFromIdLessThanOrEqualTo(Integer value) {
            addCriterion("from_id <=", value, "fromId");
            return (Criteria) this;
        }

        public Criteria andFromIdIn(List<Integer> values) {
            addCriterion("from_id in", values, "fromId");
            return (Criteria) this;
        }

        public Criteria andFromIdNotIn(List<Integer> values) {
            addCriterion("from_id not in", values, "fromId");
            return (Criteria) this;
        }

        public Criteria andFromIdBetween(Integer value1, Integer value2) {
            addCriterion("from_id between", value1, value2, "fromId");
            return (Criteria) this;
        }

        public Criteria andFromIdNotBetween(Integer value1, Integer value2) {
            addCriterion("from_id not between", value1, value2, "fromId");
            return (Criteria) this;
        }

        public Criteria andToIdIsNull() {
            addCriterion("to_id is null");
            return (Criteria) this;
        }

        public Criteria andToIdIsNotNull() {
            addCriterion("to_id is not null");
            return (Criteria) this;
        }

        public Criteria andToIdEqualTo(Integer value) {
            addCriterion("to_id =", value, "toId");
            return (Criteria) this;
        }

        public Criteria andToIdNotEqualTo(Integer value) {
            addCriterion("to_id <>", value, "toId");
            return (Criteria) this;
        }

        public Criteria andToIdGreaterThan(Integer value) {
            addCriterion("to_id >", value, "toId");
            return (Criteria) this;
        }

        public Criteria andToIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("to_id >=", value, "toId");
            return (Criteria) this;
        }

        public Criteria andToIdLessThan(Integer value) {
            addCriterion("to_id <", value, "toId");
            return (Criteria) this;
        }

        public Criteria andToIdLessThanOrEqualTo(Integer value) {
            addCriterion("to_id <=", value, "toId");
            return (Criteria) this;
        }

        public Criteria andToIdIn(List<Integer> values) {
            addCriterion("to_id in", values, "toId");
            return (Criteria) this;
        }

        public Criteria andToIdNotIn(List<Integer> values) {
            addCriterion("to_id not in", values, "toId");
            return (Criteria) this;
        }

        public Criteria andToIdBetween(Integer value1, Integer value2) {
            addCriterion("to_id between", value1, value2, "toId");
            return (Criteria) this;
        }

        public Criteria andToIdNotBetween(Integer value1, Integer value2) {
            addCriterion("to_id not between", value1, value2, "toId");
            return (Criteria) this;
        }

        public Criteria andTypeIsNull() {
            addCriterion("type is null");
            return (Criteria) this;
        }

        public Criteria andTypeIsNotNull() {
            addCriterion("type is not null");
            return (Criteria) this;
        }

        public Criteria andTypeEqualTo(Integer value) {
            addCriterion("type =", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotEqualTo(Integer value) {
            addCriterion("type <>", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThan(Integer value) {
            addCriterion("type >", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("type >=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThan(Integer value) {
            addCriterion("type <", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeLessThanOrEqualTo(Integer value) {
            addCriterion("type <=", value, "type");
            return (Criteria) this;
        }

        public Criteria andTypeIn(List<Integer> values) {
            addCriterion("type in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotIn(List<Integer> values) {
            addCriterion("type not in", values, "type");
            return (Criteria) this;
        }

        public Criteria andTypeBetween(Integer value1, Integer value2) {
            addCriterion("type between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("type not between", value1, value2, "type");
            return (Criteria) this;
        }

        public Criteria andDistanceIsNull() {
            addCriterion("distance is null");
            return (Criteria) this;
        }

        public Criteria andDistanceIsNotNull() {
            addCriterion("distance is not null");
            return (Criteria) this;
        }

        public Criteria andDistanceEqualTo(Integer value) {
            addCriterion("distance =", value, "distance");
            return (Criteria) this;
        }

        public Criteria andDistanceNotEqualTo(Integer value) {
            addCriterion("distance <>", value, "distance");
            return (Criteria) this;
        }

        public Criteria andDistanceGreaterThan(Integer value) {
            addCriterion("distance >", value, "distance");
            return (Criteria) this;
        }

        public Criteria andDistanceGreaterThanOrEqualTo(Integer value) {
            addCriterion("distance >=", value, "distance");
            return (Criteria) this;
        }

        public Criteria andDistanceLessThan(Integer value) {
            addCriterion("distance <", value, "distance");
            return (Criteria) this;
        }

        public Criteria andDistanceLessThanOrEqualTo(Integer value) {
            addCriterion("distance <=", value, "distance");
            return (Criteria) this;
        }

        public Criteria andDistanceIn(List<Integer> values) {
            addCriterion("distance in", values, "distance");
            return (Criteria) this;
        }

        public Criteria andDistanceNotIn(List<Integer> values) {
            addCriterion("distance not in", values, "distance");
            return (Criteria) this;
        }

        public Criteria andDistanceBetween(Integer value1, Integer value2) {
            addCriterion("distance between", value1, value2, "distance");
            return (Criteria) this;
        }

        public Criteria andDistanceNotBetween(Integer value1, Integer value2) {
            addCriterion("distance not between", value1, value2, "distance");
            return (Criteria) this;
        }

        public Criteria andDeliveryDurationIsNull() {
            addCriterion("delivery_duration is null");
            return (Criteria) this;
        }

        public Criteria andDeliveryDurationIsNotNull() {
            addCriterion("delivery_duration is not null");
            return (Criteria) this;
        }

        public Criteria andDeliveryDurationEqualTo(Integer value) {
            addCriterion("delivery_duration =", value, "deliveryDuration");
            return (Criteria) this;
        }

        public Criteria andDeliveryDurationNotEqualTo(Integer value) {
            addCriterion("delivery_duration <>", value, "deliveryDuration");
            return (Criteria) this;
        }

        public Criteria andDeliveryDurationGreaterThan(Integer value) {
            addCriterion("delivery_duration >", value, "deliveryDuration");
            return (Criteria) this;
        }

        public Criteria andDeliveryDurationGreaterThanOrEqualTo(Integer value) {
            addCriterion("delivery_duration >=", value, "deliveryDuration");
            return (Criteria) this;
        }

        public Criteria andDeliveryDurationLessThan(Integer value) {
            addCriterion("delivery_duration <", value, "deliveryDuration");
            return (Criteria) this;
        }

        public Criteria andDeliveryDurationLessThanOrEqualTo(Integer value) {
            addCriterion("delivery_duration <=", value, "deliveryDuration");
            return (Criteria) this;
        }

        public Criteria andDeliveryDurationIn(List<Integer> values) {
            addCriterion("delivery_duration in", values, "deliveryDuration");
            return (Criteria) this;
        }

        public Criteria andDeliveryDurationNotIn(List<Integer> values) {
            addCriterion("delivery_duration not in", values, "deliveryDuration");
            return (Criteria) this;
        }

        public Criteria andDeliveryDurationBetween(Integer value1, Integer value2) {
            addCriterion("delivery_duration between", value1, value2, "deliveryDuration");
            return (Criteria) this;
        }

        public Criteria andDeliveryDurationNotBetween(Integer value1, Integer value2) {
            addCriterion("delivery_duration not between", value1, value2, "deliveryDuration");
            return (Criteria) this;
        }

        public Criteria andDriverIncomeIsNull() {
            addCriterion("driver_income is null");
            return (Criteria) this;
        }

        public Criteria andDriverIncomeIsNotNull() {
            addCriterion("driver_income is not null");
            return (Criteria) this;
        }

        public Criteria andDriverIncomeEqualTo(Integer value) {
            addCriterion("driver_income =", value, "driverIncome");
            return (Criteria) this;
        }

        public Criteria andDriverIncomeNotEqualTo(Integer value) {
            addCriterion("driver_income <>", value, "driverIncome");
            return (Criteria) this;
        }

        public Criteria andDriverIncomeGreaterThan(Integer value) {
            addCriterion("driver_income >", value, "driverIncome");
            return (Criteria) this;
        }

        public Criteria andDriverIncomeGreaterThanOrEqualTo(Integer value) {
            addCriterion("driver_income >=", value, "driverIncome");
            return (Criteria) this;
        }

        public Criteria andDriverIncomeLessThan(Integer value) {
            addCriterion("driver_income <", value, "driverIncome");
            return (Criteria) this;
        }

        public Criteria andDriverIncomeLessThanOrEqualTo(Integer value) {
            addCriterion("driver_income <=", value, "driverIncome");
            return (Criteria) this;
        }

        public Criteria andDriverIncomeIn(List<Integer> values) {
            addCriterion("driver_income in", values, "driverIncome");
            return (Criteria) this;
        }

        public Criteria andDriverIncomeNotIn(List<Integer> values) {
            addCriterion("driver_income not in", values, "driverIncome");
            return (Criteria) this;
        }

        public Criteria andDriverIncomeBetween(Integer value1, Integer value2) {
            addCriterion("driver_income between", value1, value2, "driverIncome");
            return (Criteria) this;
        }

        public Criteria andDriverIncomeNotBetween(Integer value1, Integer value2) {
            addCriterion("driver_income not between", value1, value2, "driverIncome");
            return (Criteria) this;
        }

        public Criteria andPlateformIncomeIsNull() {
            addCriterion("plateform_income is null");
            return (Criteria) this;
        }

        public Criteria andPlateformIncomeIsNotNull() {
            addCriterion("plateform_income is not null");
            return (Criteria) this;
        }

        public Criteria andPlateformIncomeEqualTo(Integer value) {
            addCriterion("plateform_income =", value, "plateformIncome");
            return (Criteria) this;
        }

        public Criteria andPlateformIncomeNotEqualTo(Integer value) {
            addCriterion("plateform_income <>", value, "plateformIncome");
            return (Criteria) this;
        }

        public Criteria andPlateformIncomeGreaterThan(Integer value) {
            addCriterion("plateform_income >", value, "plateformIncome");
            return (Criteria) this;
        }

        public Criteria andPlateformIncomeGreaterThanOrEqualTo(Integer value) {
            addCriterion("plateform_income >=", value, "plateformIncome");
            return (Criteria) this;
        }

        public Criteria andPlateformIncomeLessThan(Integer value) {
            addCriterion("plateform_income <", value, "plateformIncome");
            return (Criteria) this;
        }

        public Criteria andPlateformIncomeLessThanOrEqualTo(Integer value) {
            addCriterion("plateform_income <=", value, "plateformIncome");
            return (Criteria) this;
        }

        public Criteria andPlateformIncomeIn(List<Integer> values) {
            addCriterion("plateform_income in", values, "plateformIncome");
            return (Criteria) this;
        }

        public Criteria andPlateformIncomeNotIn(List<Integer> values) {
            addCriterion("plateform_income not in", values, "plateformIncome");
            return (Criteria) this;
        }

        public Criteria andPlateformIncomeBetween(Integer value1, Integer value2) {
            addCriterion("plateform_income between", value1, value2, "plateformIncome");
            return (Criteria) this;
        }

        public Criteria andPlateformIncomeNotBetween(Integer value1, Integer value2) {
            addCriterion("plateform_income not between", value1, value2, "plateformIncome");
            return (Criteria) this;
        }

        public Criteria andAgentIncomeIsNull() {
            addCriterion("agent_income is null");
            return (Criteria) this;
        }

        public Criteria andAgentIncomeIsNotNull() {
            addCriterion("agent_income is not null");
            return (Criteria) this;
        }

        public Criteria andAgentIncomeEqualTo(Integer value) {
            addCriterion("agent_income =", value, "agentIncome");
            return (Criteria) this;
        }

        public Criteria andAgentIncomeNotEqualTo(Integer value) {
            addCriterion("agent_income <>", value, "agentIncome");
            return (Criteria) this;
        }

        public Criteria andAgentIncomeGreaterThan(Integer value) {
            addCriterion("agent_income >", value, "agentIncome");
            return (Criteria) this;
        }

        public Criteria andAgentIncomeGreaterThanOrEqualTo(Integer value) {
            addCriterion("agent_income >=", value, "agentIncome");
            return (Criteria) this;
        }

        public Criteria andAgentIncomeLessThan(Integer value) {
            addCriterion("agent_income <", value, "agentIncome");
            return (Criteria) this;
        }

        public Criteria andAgentIncomeLessThanOrEqualTo(Integer value) {
            addCriterion("agent_income <=", value, "agentIncome");
            return (Criteria) this;
        }

        public Criteria andAgentIncomeIn(List<Integer> values) {
            addCriterion("agent_income in", values, "agentIncome");
            return (Criteria) this;
        }

        public Criteria andAgentIncomeNotIn(List<Integer> values) {
            addCriterion("agent_income not in", values, "agentIncome");
            return (Criteria) this;
        }

        public Criteria andAgentIncomeBetween(Integer value1, Integer value2) {
            addCriterion("agent_income between", value1, value2, "agentIncome");
            return (Criteria) this;
        }

        public Criteria andAgentIncomeNotBetween(Integer value1, Integer value2) {
            addCriterion("agent_income not between", value1, value2, "agentIncome");
            return (Criteria) this;
        }

        public Criteria andUserLngIsNull() {
            addCriterion("user_lng is null");
            return (Criteria) this;
        }

        public Criteria andUserLngIsNotNull() {
            addCriterion("user_lng is not null");
            return (Criteria) this;
        }

        public Criteria andUserLngEqualTo(Double value) {
            addCriterion("user_lng =", value, "userLng");
            return (Criteria) this;
        }

        public Criteria andUserLngNotEqualTo(Double value) {
            addCriterion("user_lng <>", value, "userLng");
            return (Criteria) this;
        }

        public Criteria andUserLngGreaterThan(Double value) {
            addCriterion("user_lng >", value, "userLng");
            return (Criteria) this;
        }

        public Criteria andUserLngGreaterThanOrEqualTo(Double value) {
            addCriterion("user_lng >=", value, "userLng");
            return (Criteria) this;
        }

        public Criteria andUserLngLessThan(Double value) {
            addCriterion("user_lng <", value, "userLng");
            return (Criteria) this;
        }

        public Criteria andUserLngLessThanOrEqualTo(Double value) {
            addCriterion("user_lng <=", value, "userLng");
            return (Criteria) this;
        }

        public Criteria andUserLngIn(List<Double> values) {
            addCriterion("user_lng in", values, "userLng");
            return (Criteria) this;
        }

        public Criteria andUserLngNotIn(List<Double> values) {
            addCriterion("user_lng not in", values, "userLng");
            return (Criteria) this;
        }

        public Criteria andUserLngBetween(Double value1, Double value2) {
            addCriterion("user_lng between", value1, value2, "userLng");
            return (Criteria) this;
        }

        public Criteria andUserLngNotBetween(Double value1, Double value2) {
            addCriterion("user_lng not between", value1, value2, "userLng");
            return (Criteria) this;
        }

        public Criteria andUserLatIsNull() {
            addCriterion("user_lat is null");
            return (Criteria) this;
        }

        public Criteria andUserLatIsNotNull() {
            addCriterion("user_lat is not null");
            return (Criteria) this;
        }

        public Criteria andUserLatEqualTo(Double value) {
            addCriterion("user_lat =", value, "userLat");
            return (Criteria) this;
        }

        public Criteria andUserLatNotEqualTo(Double value) {
            addCriterion("user_lat <>", value, "userLat");
            return (Criteria) this;
        }

        public Criteria andUserLatGreaterThan(Double value) {
            addCriterion("user_lat >", value, "userLat");
            return (Criteria) this;
        }

        public Criteria andUserLatGreaterThanOrEqualTo(Double value) {
            addCriterion("user_lat >=", value, "userLat");
            return (Criteria) this;
        }

        public Criteria andUserLatLessThan(Double value) {
            addCriterion("user_lat <", value, "userLat");
            return (Criteria) this;
        }

        public Criteria andUserLatLessThanOrEqualTo(Double value) {
            addCriterion("user_lat <=", value, "userLat");
            return (Criteria) this;
        }

        public Criteria andUserLatIn(List<Double> values) {
            addCriterion("user_lat in", values, "userLat");
            return (Criteria) this;
        }

        public Criteria andUserLatNotIn(List<Double> values) {
            addCriterion("user_lat not in", values, "userLat");
            return (Criteria) this;
        }

        public Criteria andUserLatBetween(Double value1, Double value2) {
            addCriterion("user_lat between", value1, value2, "userLat");
            return (Criteria) this;
        }

        public Criteria andUserLatNotBetween(Double value1, Double value2) {
            addCriterion("user_lat not between", value1, value2, "userLat");
            return (Criteria) this;
        }

        public Criteria andAgentIdIsNull() {
            addCriterion("agent_id is null");
            return (Criteria) this;
        }

        public Criteria andAgentIdIsNotNull() {
            addCriterion("agent_id is not null");
            return (Criteria) this;
        }

        public Criteria andAgentIdEqualTo(Integer value) {
            addCriterion("agent_id =", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotEqualTo(Integer value) {
            addCriterion("agent_id <>", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdGreaterThan(Integer value) {
            addCriterion("agent_id >", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("agent_id >=", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdLessThan(Integer value) {
            addCriterion("agent_id <", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdLessThanOrEqualTo(Integer value) {
            addCriterion("agent_id <=", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdIn(List<Integer> values) {
            addCriterion("agent_id in", values, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotIn(List<Integer> values) {
            addCriterion("agent_id not in", values, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdBetween(Integer value1, Integer value2) {
            addCriterion("agent_id between", value1, value2, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotBetween(Integer value1, Integer value2) {
            addCriterion("agent_id not between", value1, value2, "agentId");
            return (Criteria) this;
        }

        public Criteria andAmountRefundedIsNull() {
            addCriterion("amount_refunded is null");
            return (Criteria) this;
        }

        public Criteria andAmountRefundedIsNotNull() {
            addCriterion("amount_refunded is not null");
            return (Criteria) this;
        }

        public Criteria andAmountRefundedEqualTo(Integer value) {
            addCriterion("amount_refunded =", value, "amountRefunded");
            return (Criteria) this;
        }

        public Criteria andAmountRefundedNotEqualTo(Integer value) {
            addCriterion("amount_refunded <>", value, "amountRefunded");
            return (Criteria) this;
        }

        public Criteria andAmountRefundedGreaterThan(Integer value) {
            addCriterion("amount_refunded >", value, "amountRefunded");
            return (Criteria) this;
        }

        public Criteria andAmountRefundedGreaterThanOrEqualTo(Integer value) {
            addCriterion("amount_refunded >=", value, "amountRefunded");
            return (Criteria) this;
        }

        public Criteria andAmountRefundedLessThan(Integer value) {
            addCriterion("amount_refunded <", value, "amountRefunded");
            return (Criteria) this;
        }

        public Criteria andAmountRefundedLessThanOrEqualTo(Integer value) {
            addCriterion("amount_refunded <=", value, "amountRefunded");
            return (Criteria) this;
        }

        public Criteria andAmountRefundedIn(List<Integer> values) {
            addCriterion("amount_refunded in", values, "amountRefunded");
            return (Criteria) this;
        }

        public Criteria andAmountRefundedNotIn(List<Integer> values) {
            addCriterion("amount_refunded not in", values, "amountRefunded");
            return (Criteria) this;
        }

        public Criteria andAmountRefundedBetween(Integer value1, Integer value2) {
            addCriterion("amount_refunded between", value1, value2, "amountRefunded");
            return (Criteria) this;
        }

        public Criteria andAmountRefundedNotBetween(Integer value1, Integer value2) {
            addCriterion("amount_refunded not between", value1, value2, "amountRefunded");
            return (Criteria) this;
        }

        public Criteria andRefundedIsNull() {
            addCriterion("refunded is null");
            return (Criteria) this;
        }

        public Criteria andRefundedIsNotNull() {
            addCriterion("refunded is not null");
            return (Criteria) this;
        }

        public Criteria andRefundedEqualTo(Boolean value) {
            addCriterion("refunded =", value, "refunded");
            return (Criteria) this;
        }

        public Criteria andRefundedNotEqualTo(Boolean value) {
            addCriterion("refunded <>", value, "refunded");
            return (Criteria) this;
        }

        public Criteria andRefundedGreaterThan(Boolean value) {
            addCriterion("refunded >", value, "refunded");
            return (Criteria) this;
        }

        public Criteria andRefundedGreaterThanOrEqualTo(Boolean value) {
            addCriterion("refunded >=", value, "refunded");
            return (Criteria) this;
        }

        public Criteria andRefundedLessThan(Boolean value) {
            addCriterion("refunded <", value, "refunded");
            return (Criteria) this;
        }

        public Criteria andRefundedLessThanOrEqualTo(Boolean value) {
            addCriterion("refunded <=", value, "refunded");
            return (Criteria) this;
        }

        public Criteria andRefundedIn(List<Boolean> values) {
            addCriterion("refunded in", values, "refunded");
            return (Criteria) this;
        }

        public Criteria andRefundedNotIn(List<Boolean> values) {
            addCriterion("refunded not in", values, "refunded");
            return (Criteria) this;
        }

        public Criteria andRefundedBetween(Boolean value1, Boolean value2) {
            addCriterion("refunded between", value1, value2, "refunded");
            return (Criteria) this;
        }

        public Criteria andRefundedNotBetween(Boolean value1, Boolean value2) {
            addCriterion("refunded not between", value1, value2, "refunded");
            return (Criteria) this;
        }

        public Criteria andPaidIsNull() {
            addCriterion("paid is null");
            return (Criteria) this;
        }

        public Criteria andPaidIsNotNull() {
            addCriterion("paid is not null");
            return (Criteria) this;
        }

        public Criteria andPaidEqualTo(Boolean value) {
            addCriterion("paid =", value, "paid");
            return (Criteria) this;
        }

        public Criteria andPaidNotEqualTo(Boolean value) {
            addCriterion("paid <>", value, "paid");
            return (Criteria) this;
        }

        public Criteria andPaidGreaterThan(Boolean value) {
            addCriterion("paid >", value, "paid");
            return (Criteria) this;
        }

        public Criteria andPaidGreaterThanOrEqualTo(Boolean value) {
            addCriterion("paid >=", value, "paid");
            return (Criteria) this;
        }

        public Criteria andPaidLessThan(Boolean value) {
            addCriterion("paid <", value, "paid");
            return (Criteria) this;
        }

        public Criteria andPaidLessThanOrEqualTo(Boolean value) {
            addCriterion("paid <=", value, "paid");
            return (Criteria) this;
        }

        public Criteria andPaidIn(List<Boolean> values) {
            addCriterion("paid in", values, "paid");
            return (Criteria) this;
        }

        public Criteria andPaidNotIn(List<Boolean> values) {
            addCriterion("paid not in", values, "paid");
            return (Criteria) this;
        }

        public Criteria andPaidBetween(Boolean value1, Boolean value2) {
            addCriterion("paid between", value1, value2, "paid");
            return (Criteria) this;
        }

        public Criteria andPaidNotBetween(Boolean value1, Boolean value2) {
            addCriterion("paid not between", value1, value2, "paid");
            return (Criteria) this;
        }

        public Criteria andCanceledIsNull() {
            addCriterion("canceled is null");
            return (Criteria) this;
        }

        public Criteria andCanceledIsNotNull() {
            addCriterion("canceled is not null");
            return (Criteria) this;
        }

        public Criteria andCanceledEqualTo(Boolean value) {
            addCriterion("canceled =", value, "canceled");
            return (Criteria) this;
        }

        public Criteria andCanceledNotEqualTo(Boolean value) {
            addCriterion("canceled <>", value, "canceled");
            return (Criteria) this;
        }

        public Criteria andCanceledGreaterThan(Boolean value) {
            addCriterion("canceled >", value, "canceled");
            return (Criteria) this;
        }

        public Criteria andCanceledGreaterThanOrEqualTo(Boolean value) {
            addCriterion("canceled >=", value, "canceled");
            return (Criteria) this;
        }

        public Criteria andCanceledLessThan(Boolean value) {
            addCriterion("canceled <", value, "canceled");
            return (Criteria) this;
        }

        public Criteria andCanceledLessThanOrEqualTo(Boolean value) {
            addCriterion("canceled <=", value, "canceled");
            return (Criteria) this;
        }

        public Criteria andCanceledIn(List<Boolean> values) {
            addCriterion("canceled in", values, "canceled");
            return (Criteria) this;
        }

        public Criteria andCanceledNotIn(List<Boolean> values) {
            addCriterion("canceled not in", values, "canceled");
            return (Criteria) this;
        }

        public Criteria andCanceledBetween(Boolean value1, Boolean value2) {
            addCriterion("canceled between", value1, value2, "canceled");
            return (Criteria) this;
        }

        public Criteria andCanceledNotBetween(Boolean value1, Boolean value2) {
            addCriterion("canceled not between", value1, value2, "canceled");
            return (Criteria) this;
        }

        public Criteria andTakenIsNull() {
            addCriterion("taken is null");
            return (Criteria) this;
        }

        public Criteria andTakenIsNotNull() {
            addCriterion("taken is not null");
            return (Criteria) this;
        }

        public Criteria andTakenEqualTo(Boolean value) {
            addCriterion("taken =", value, "taken");
            return (Criteria) this;
        }

        public Criteria andTakenNotEqualTo(Boolean value) {
            addCriterion("taken <>", value, "taken");
            return (Criteria) this;
        }

        public Criteria andTakenGreaterThan(Boolean value) {
            addCriterion("taken >", value, "taken");
            return (Criteria) this;
        }

        public Criteria andTakenGreaterThanOrEqualTo(Boolean value) {
            addCriterion("taken >=", value, "taken");
            return (Criteria) this;
        }

        public Criteria andTakenLessThan(Boolean value) {
            addCriterion("taken <", value, "taken");
            return (Criteria) this;
        }

        public Criteria andTakenLessThanOrEqualTo(Boolean value) {
            addCriterion("taken <=", value, "taken");
            return (Criteria) this;
        }

        public Criteria andTakenIn(List<Boolean> values) {
            addCriterion("taken in", values, "taken");
            return (Criteria) this;
        }

        public Criteria andTakenNotIn(List<Boolean> values) {
            addCriterion("taken not in", values, "taken");
            return (Criteria) this;
        }

        public Criteria andTakenBetween(Boolean value1, Boolean value2) {
            addCriterion("taken between", value1, value2, "taken");
            return (Criteria) this;
        }

        public Criteria andTakenNotBetween(Boolean value1, Boolean value2) {
            addCriterion("taken not between", value1, value2, "taken");
            return (Criteria) this;
        }

        public Criteria andCompletedIsNull() {
            addCriterion("completed is null");
            return (Criteria) this;
        }

        public Criteria andCompletedIsNotNull() {
            addCriterion("completed is not null");
            return (Criteria) this;
        }

        public Criteria andCompletedEqualTo(Boolean value) {
            addCriterion("completed =", value, "completed");
            return (Criteria) this;
        }

        public Criteria andCompletedNotEqualTo(Boolean value) {
            addCriterion("completed <>", value, "completed");
            return (Criteria) this;
        }

        public Criteria andCompletedGreaterThan(Boolean value) {
            addCriterion("completed >", value, "completed");
            return (Criteria) this;
        }

        public Criteria andCompletedGreaterThanOrEqualTo(Boolean value) {
            addCriterion("completed >=", value, "completed");
            return (Criteria) this;
        }

        public Criteria andCompletedLessThan(Boolean value) {
            addCriterion("completed <", value, "completed");
            return (Criteria) this;
        }

        public Criteria andCompletedLessThanOrEqualTo(Boolean value) {
            addCriterion("completed <=", value, "completed");
            return (Criteria) this;
        }

        public Criteria andCompletedIn(List<Boolean> values) {
            addCriterion("completed in", values, "completed");
            return (Criteria) this;
        }

        public Criteria andCompletedNotIn(List<Boolean> values) {
            addCriterion("completed not in", values, "completed");
            return (Criteria) this;
        }

        public Criteria andCompletedBetween(Boolean value1, Boolean value2) {
            addCriterion("completed between", value1, value2, "completed");
            return (Criteria) this;
        }

        public Criteria andCompletedNotBetween(Boolean value1, Boolean value2) {
            addCriterion("completed not between", value1, value2, "completed");
            return (Criteria) this;
        }

        public Criteria andTimePaidIsNull() {
            addCriterion("time_paid is null");
            return (Criteria) this;
        }

        public Criteria andTimePaidIsNotNull() {
            addCriterion("time_paid is not null");
            return (Criteria) this;
        }

        public Criteria andTimePaidEqualTo(Date value) {
            addCriterion("time_paid =", value, "timePaid");
            return (Criteria) this;
        }

        public Criteria andTimePaidNotEqualTo(Date value) {
            addCriterion("time_paid <>", value, "timePaid");
            return (Criteria) this;
        }

        public Criteria andTimePaidGreaterThan(Date value) {
            addCriterion("time_paid >", value, "timePaid");
            return (Criteria) this;
        }

        public Criteria andTimePaidGreaterThanOrEqualTo(Date value) {
            addCriterion("time_paid >=", value, "timePaid");
            return (Criteria) this;
        }

        public Criteria andTimePaidLessThan(Date value) {
            addCriterion("time_paid <", value, "timePaid");
            return (Criteria) this;
        }

        public Criteria andTimePaidLessThanOrEqualTo(Date value) {
            addCriterion("time_paid <=", value, "timePaid");
            return (Criteria) this;
        }

        public Criteria andTimePaidIn(List<Date> values) {
            addCriterion("time_paid in", values, "timePaid");
            return (Criteria) this;
        }

        public Criteria andTimePaidNotIn(List<Date> values) {
            addCriterion("time_paid not in", values, "timePaid");
            return (Criteria) this;
        }

        public Criteria andTimePaidBetween(Date value1, Date value2) {
            addCriterion("time_paid between", value1, value2, "timePaid");
            return (Criteria) this;
        }

        public Criteria andTimePaidNotBetween(Date value1, Date value2) {
            addCriterion("time_paid not between", value1, value2, "timePaid");
            return (Criteria) this;
        }

        public Criteria andTimeCanceledIsNull() {
            addCriterion("time_canceled is null");
            return (Criteria) this;
        }

        public Criteria andTimeCanceledIsNotNull() {
            addCriterion("time_canceled is not null");
            return (Criteria) this;
        }

        public Criteria andTimeCanceledEqualTo(Date value) {
            addCriterion("time_canceled =", value, "timeCanceled");
            return (Criteria) this;
        }

        public Criteria andTimeCanceledNotEqualTo(Date value) {
            addCriterion("time_canceled <>", value, "timeCanceled");
            return (Criteria) this;
        }

        public Criteria andTimeCanceledGreaterThan(Date value) {
            addCriterion("time_canceled >", value, "timeCanceled");
            return (Criteria) this;
        }

        public Criteria andTimeCanceledGreaterThanOrEqualTo(Date value) {
            addCriterion("time_canceled >=", value, "timeCanceled");
            return (Criteria) this;
        }

        public Criteria andTimeCanceledLessThan(Date value) {
            addCriterion("time_canceled <", value, "timeCanceled");
            return (Criteria) this;
        }

        public Criteria andTimeCanceledLessThanOrEqualTo(Date value) {
            addCriterion("time_canceled <=", value, "timeCanceled");
            return (Criteria) this;
        }

        public Criteria andTimeCanceledIn(List<Date> values) {
            addCriterion("time_canceled in", values, "timeCanceled");
            return (Criteria) this;
        }

        public Criteria andTimeCanceledNotIn(List<Date> values) {
            addCriterion("time_canceled not in", values, "timeCanceled");
            return (Criteria) this;
        }

        public Criteria andTimeCanceledBetween(Date value1, Date value2) {
            addCriterion("time_canceled between", value1, value2, "timeCanceled");
            return (Criteria) this;
        }

        public Criteria andTimeCanceledNotBetween(Date value1, Date value2) {
            addCriterion("time_canceled not between", value1, value2, "timeCanceled");
            return (Criteria) this;
        }

        public Criteria andTimeTakenIsNull() {
            addCriterion("time_taken is null");
            return (Criteria) this;
        }

        public Criteria andTimeTakenIsNotNull() {
            addCriterion("time_taken is not null");
            return (Criteria) this;
        }

        public Criteria andTimeTakenEqualTo(Date value) {
            addCriterion("time_taken =", value, "timeTaken");
            return (Criteria) this;
        }

        public Criteria andTimeTakenNotEqualTo(Date value) {
            addCriterion("time_taken <>", value, "timeTaken");
            return (Criteria) this;
        }

        public Criteria andTimeTakenGreaterThan(Date value) {
            addCriterion("time_taken >", value, "timeTaken");
            return (Criteria) this;
        }

        public Criteria andTimeTakenGreaterThanOrEqualTo(Date value) {
            addCriterion("time_taken >=", value, "timeTaken");
            return (Criteria) this;
        }

        public Criteria andTimeTakenLessThan(Date value) {
            addCriterion("time_taken <", value, "timeTaken");
            return (Criteria) this;
        }

        public Criteria andTimeTakenLessThanOrEqualTo(Date value) {
            addCriterion("time_taken <=", value, "timeTaken");
            return (Criteria) this;
        }

        public Criteria andTimeTakenIn(List<Date> values) {
            addCriterion("time_taken in", values, "timeTaken");
            return (Criteria) this;
        }

        public Criteria andTimeTakenNotIn(List<Date> values) {
            addCriterion("time_taken not in", values, "timeTaken");
            return (Criteria) this;
        }

        public Criteria andTimeTakenBetween(Date value1, Date value2) {
            addCriterion("time_taken between", value1, value2, "timeTaken");
            return (Criteria) this;
        }

        public Criteria andTimeTakenNotBetween(Date value1, Date value2) {
            addCriterion("time_taken not between", value1, value2, "timeTaken");
            return (Criteria) this;
        }

        public Criteria andTimeCompletedIsNull() {
            addCriterion("time_completed is null");
            return (Criteria) this;
        }

        public Criteria andTimeCompletedIsNotNull() {
            addCriterion("time_completed is not null");
            return (Criteria) this;
        }

        public Criteria andTimeCompletedEqualTo(Date value) {
            addCriterion("time_completed =", value, "timeCompleted");
            return (Criteria) this;
        }

        public Criteria andTimeCompletedNotEqualTo(Date value) {
            addCriterion("time_completed <>", value, "timeCompleted");
            return (Criteria) this;
        }

        public Criteria andTimeCompletedGreaterThan(Date value) {
            addCriterion("time_completed >", value, "timeCompleted");
            return (Criteria) this;
        }

        public Criteria andTimeCompletedGreaterThanOrEqualTo(Date value) {
            addCriterion("time_completed >=", value, "timeCompleted");
            return (Criteria) this;
        }

        public Criteria andTimeCompletedLessThan(Date value) {
            addCriterion("time_completed <", value, "timeCompleted");
            return (Criteria) this;
        }

        public Criteria andTimeCompletedLessThanOrEqualTo(Date value) {
            addCriterion("time_completed <=", value, "timeCompleted");
            return (Criteria) this;
        }

        public Criteria andTimeCompletedIn(List<Date> values) {
            addCriterion("time_completed in", values, "timeCompleted");
            return (Criteria) this;
        }

        public Criteria andTimeCompletedNotIn(List<Date> values) {
            addCriterion("time_completed not in", values, "timeCompleted");
            return (Criteria) this;
        }

        public Criteria andTimeCompletedBetween(Date value1, Date value2) {
            addCriterion("time_completed between", value1, value2, "timeCompleted");
            return (Criteria) this;
        }

        public Criteria andTimeCompletedNotBetween(Date value1, Date value2) {
            addCriterion("time_completed not between", value1, value2, "timeCompleted");
            return (Criteria) this;
        }

        public Criteria andChannelIsNull() {
            addCriterion("channel is null");
            return (Criteria) this;
        }

        public Criteria andChannelIsNotNull() {
            addCriterion("channel is not null");
            return (Criteria) this;
        }

        public Criteria andChannelEqualTo(Integer value) {
            addCriterion("channel =", value, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelNotEqualTo(Integer value) {
            addCriterion("channel <>", value, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelGreaterThan(Integer value) {
            addCriterion("channel >", value, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelGreaterThanOrEqualTo(Integer value) {
            addCriterion("channel >=", value, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelLessThan(Integer value) {
            addCriterion("channel <", value, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelLessThanOrEqualTo(Integer value) {
            addCriterion("channel <=", value, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelIn(List<Integer> values) {
            addCriterion("channel in", values, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelNotIn(List<Integer> values) {
            addCriterion("channel not in", values, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelBetween(Integer value1, Integer value2) {
            addCriterion("channel between", value1, value2, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelNotBetween(Integer value1, Integer value2) {
            addCriterion("channel not between", value1, value2, "channel");
            return (Criteria) this;
        }

        public Criteria andDeletedIsNull() {
            addCriterion("deleted is null");
            return (Criteria) this;
        }

        public Criteria andDeletedIsNotNull() {
            addCriterion("deleted is not null");
            return (Criteria) this;
        }

        public Criteria andDeletedEqualTo(Boolean value) {
            addCriterion("deleted =", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedNotEqualTo(Boolean value) {
            addCriterion("deleted <>", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedGreaterThan(Boolean value) {
            addCriterion("deleted >", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedGreaterThanOrEqualTo(Boolean value) {
            addCriterion("deleted >=", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedLessThan(Boolean value) {
            addCriterion("deleted <", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedLessThanOrEqualTo(Boolean value) {
            addCriterion("deleted <=", value, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedIn(List<Boolean> values) {
            addCriterion("deleted in", values, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedNotIn(List<Boolean> values) {
            addCriterion("deleted not in", values, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedBetween(Boolean value1, Boolean value2) {
            addCriterion("deleted between", value1, value2, "deleted");
            return (Criteria) this;
        }

        public Criteria andDeletedNotBetween(Boolean value1, Boolean value2) {
            addCriterion("deleted not between", value1, value2, "deleted");
            return (Criteria) this;
        }

        public Criteria andFromNameIsNull() {
            addCriterion("from_name is null");
            return (Criteria) this;
        }

        public Criteria andFromNameIsNotNull() {
            addCriterion("from_name is not null");
            return (Criteria) this;
        }

        public Criteria andFromNameEqualTo(String value) {
            addCriterion("from_name =", value, "fromName");
            return (Criteria) this;
        }

        public Criteria andFromNameNotEqualTo(String value) {
            addCriterion("from_name <>", value, "fromName");
            return (Criteria) this;
        }

        public Criteria andFromNameGreaterThan(String value) {
            addCriterion("from_name >", value, "fromName");
            return (Criteria) this;
        }

        public Criteria andFromNameGreaterThanOrEqualTo(String value) {
            addCriterion("from_name >=", value, "fromName");
            return (Criteria) this;
        }

        public Criteria andFromNameLessThan(String value) {
            addCriterion("from_name <", value, "fromName");
            return (Criteria) this;
        }

        public Criteria andFromNameLessThanOrEqualTo(String value) {
            addCriterion("from_name <=", value, "fromName");
            return (Criteria) this;
        }

        public Criteria andFromNameLike(String value) {
            addCriterion("from_name like", value, "fromName");
            return (Criteria) this;
        }

        public Criteria andFromNameNotLike(String value) {
            addCriterion("from_name not like", value, "fromName");
            return (Criteria) this;
        }

        public Criteria andFromNameIn(List<String> values) {
            addCriterion("from_name in", values, "fromName");
            return (Criteria) this;
        }

        public Criteria andFromNameNotIn(List<String> values) {
            addCriterion("from_name not in", values, "fromName");
            return (Criteria) this;
        }

        public Criteria andFromNameBetween(String value1, String value2) {
            addCriterion("from_name between", value1, value2, "fromName");
            return (Criteria) this;
        }

        public Criteria andFromNameNotBetween(String value1, String value2) {
            addCriterion("from_name not between", value1, value2, "fromName");
            return (Criteria) this;
        }

        public Criteria andIsCancelIsNull() {
            addCriterion("is_cancel is null");
            return (Criteria) this;
        }

        public Criteria andIsCancelIsNotNull() {
            addCriterion("is_cancel is not null");
            return (Criteria) this;
        }

        public Criteria andIsCancelEqualTo(Integer value) {
            addCriterion("is_cancel =", value, "isCancel");
            return (Criteria) this;
        }

        public Criteria andIsCancelNotEqualTo(Integer value) {
            addCriterion("is_cancel <>", value, "isCancel");
            return (Criteria) this;
        }

        public Criteria andIsCancelGreaterThan(Integer value) {
            addCriterion("is_cancel >", value, "isCancel");
            return (Criteria) this;
        }

        public Criteria andIsCancelGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_cancel >=", value, "isCancel");
            return (Criteria) this;
        }

        public Criteria andIsCancelLessThan(Integer value) {
            addCriterion("is_cancel <", value, "isCancel");
            return (Criteria) this;
        }

        public Criteria andIsCancelLessThanOrEqualTo(Integer value) {
            addCriterion("is_cancel <=", value, "isCancel");
            return (Criteria) this;
        }

        public Criteria andIsCancelIn(List<Integer> values) {
            addCriterion("is_cancel in", values, "isCancel");
            return (Criteria) this;
        }

        public Criteria andIsCancelNotIn(List<Integer> values) {
            addCriterion("is_cancel not in", values, "isCancel");
            return (Criteria) this;
        }

        public Criteria andIsCancelBetween(Integer value1, Integer value2) {
            addCriterion("is_cancel between", value1, value2, "isCancel");
            return (Criteria) this;
        }

        public Criteria andIsCancelNotBetween(Integer value1, Integer value2) {
            addCriterion("is_cancel not between", value1, value2, "isCancel");
            return (Criteria) this;
        }

        public Criteria andCancelTimeIsNull() {
            addCriterion("cancel_time is null");
            return (Criteria) this;
        }

        public Criteria andCancelTimeIsNotNull() {
            addCriterion("cancel_time is not null");
            return (Criteria) this;
        }

        public Criteria andCancelTimeEqualTo(Date value) {
            addCriterion("cancel_time =", value, "cancelTime");
            return (Criteria) this;
        }

        public Criteria andCancelTimeNotEqualTo(Date value) {
            addCriterion("cancel_time <>", value, "cancelTime");
            return (Criteria) this;
        }

        public Criteria andCancelTimeGreaterThan(Date value) {
            addCriterion("cancel_time >", value, "cancelTime");
            return (Criteria) this;
        }

        public Criteria andCancelTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("cancel_time >=", value, "cancelTime");
            return (Criteria) this;
        }

        public Criteria andCancelTimeLessThan(Date value) {
            addCriterion("cancel_time <", value, "cancelTime");
            return (Criteria) this;
        }

        public Criteria andCancelTimeLessThanOrEqualTo(Date value) {
            addCriterion("cancel_time <=", value, "cancelTime");
            return (Criteria) this;
        }

        public Criteria andCancelTimeIn(List<Date> values) {
            addCriterion("cancel_time in", values, "cancelTime");
            return (Criteria) this;
        }

        public Criteria andCancelTimeNotIn(List<Date> values) {
            addCriterion("cancel_time not in", values, "cancelTime");
            return (Criteria) this;
        }

        public Criteria andCancelTimeBetween(Date value1, Date value2) {
            addCriterion("cancel_time between", value1, value2, "cancelTime");
            return (Criteria) this;
        }

        public Criteria andCancelTimeNotBetween(Date value1, Date value2) {
            addCriterion("cancel_time not between", value1, value2, "cancelTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}