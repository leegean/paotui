package com.runfast.paotui.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.paotui.dao.model.User;
import com.runfast.paotui.dao.model.UserExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends IMapper<User, Integer, UserExample> {
}