package com.runfast.paotui.dao.model;

import java.io.Serializable;
import java.util.Date;

public class WeightTemplate implements Serializable {
    private Integer id;

    private Boolean deleted;

    private Date createTime;

    private Date updateTime;

    private Integer createBy;

    private Integer updateBy;

    /**
     * 重量下限（包含）
     */
    private Integer lower;

    /**
     * 重量上限
     */
    private Integer upper;

    /**
     * 费用
     */
    private Integer cost;

    private Integer agentId;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public WeightTemplate withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public WeightTemplate withDeleted(Boolean deleted) {
        this.setDeleted(deleted);
        return this;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public WeightTemplate withCreateTime(Date createTime) {
        this.setCreateTime(createTime);
        return this;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public WeightTemplate withUpdateTime(Date updateTime) {
        this.setUpdateTime(updateTime);
        return this;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getCreateBy() {
        return createBy;
    }

    public WeightTemplate withCreateBy(Integer createBy) {
        this.setCreateBy(createBy);
        return this;
    }

    public void setCreateBy(Integer createBy) {
        this.createBy = createBy;
    }

    public Integer getUpdateBy() {
        return updateBy;
    }

    public WeightTemplate withUpdateBy(Integer updateBy) {
        this.setUpdateBy(updateBy);
        return this;
    }

    public void setUpdateBy(Integer updateBy) {
        this.updateBy = updateBy;
    }

    public Integer getLower() {
        return lower;
    }

    public WeightTemplate withLower(Integer lower) {
        this.setLower(lower);
        return this;
    }

    public void setLower(Integer lower) {
        this.lower = lower;
    }

    public Integer getUpper() {
        return upper;
    }

    public WeightTemplate withUpper(Integer upper) {
        this.setUpper(upper);
        return this;
    }

    public void setUpper(Integer upper) {
        this.upper = upper;
    }

    public Integer getCost() {
        return cost;
    }

    public WeightTemplate withCost(Integer cost) {
        this.setCost(cost);
        return this;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public WeightTemplate withAgentId(Integer agentId) {
        this.setAgentId(agentId);
        return this;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", deleted=").append(deleted);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", createBy=").append(createBy);
        sb.append(", updateBy=").append(updateBy);
        sb.append(", lower=").append(lower);
        sb.append(", upper=").append(upper);
        sb.append(", cost=").append(cost);
        sb.append(", agentId=").append(agentId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}