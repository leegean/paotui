package com.runfast.paotui.dao.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class PaotuiCostWeight implements Serializable {
    private Integer id;

    /**
     * 起始重量（kg）
     */
    private Double startweight;

    /**
     * 结束重量（kg）
     */
    private Double endweight;

    /**
     * 每千克加收费用
     */
    private BigDecimal fee;

    /**
     * 跑腿计费模板id
     */
    private Integer costid;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public PaotuiCostWeight withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getStartweight() {
        return startweight;
    }

    public PaotuiCostWeight withStartweight(Double startweight) {
        this.setStartweight(startweight);
        return this;
    }

    public void setStartweight(Double startweight) {
        this.startweight = startweight;
    }

    public Double getEndweight() {
        return endweight;
    }

    public PaotuiCostWeight withEndweight(Double endweight) {
        this.setEndweight(endweight);
        return this;
    }

    public void setEndweight(Double endweight) {
        this.endweight = endweight;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public PaotuiCostWeight withFee(BigDecimal fee) {
        this.setFee(fee);
        return this;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public Integer getCostid() {
        return costid;
    }

    public PaotuiCostWeight withCostid(Integer costid) {
        this.setCostid(costid);
        return this;
    }

    public void setCostid(Integer costid) {
        this.costid = costid;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", startweight=").append(startweight);
        sb.append(", endweight=").append(endweight);
        sb.append(", fee=").append(fee);
        sb.append(", costid=").append(costid);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}