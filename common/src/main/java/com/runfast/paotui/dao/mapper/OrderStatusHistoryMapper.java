package com.runfast.paotui.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.paotui.dao.model.OrderStatusHistory;
import com.runfast.paotui.dao.model.OrderStatusHistoryExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrderStatusHistoryMapper extends IMapper<OrderStatusHistory, Integer, OrderStatusHistoryExample> {
}