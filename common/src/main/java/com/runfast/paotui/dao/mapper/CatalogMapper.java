package com.runfast.paotui.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.paotui.dao.model.Catalog;
import com.runfast.paotui.dao.model.CatalogExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CatalogMapper extends IMapper<Catalog, Integer, CatalogExample> {
}