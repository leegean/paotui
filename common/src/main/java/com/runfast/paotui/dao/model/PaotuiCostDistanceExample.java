package com.runfast.paotui.dao.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class PaotuiCostDistanceExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PaotuiCostDistanceExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andStartdistanceIsNull() {
            addCriterion("startDistance is null");
            return (Criteria) this;
        }

        public Criteria andStartdistanceIsNotNull() {
            addCriterion("startDistance is not null");
            return (Criteria) this;
        }

        public Criteria andStartdistanceEqualTo(Double value) {
            addCriterion("startDistance =", value, "startdistance");
            return (Criteria) this;
        }

        public Criteria andStartdistanceNotEqualTo(Double value) {
            addCriterion("startDistance <>", value, "startdistance");
            return (Criteria) this;
        }

        public Criteria andStartdistanceGreaterThan(Double value) {
            addCriterion("startDistance >", value, "startdistance");
            return (Criteria) this;
        }

        public Criteria andStartdistanceGreaterThanOrEqualTo(Double value) {
            addCriterion("startDistance >=", value, "startdistance");
            return (Criteria) this;
        }

        public Criteria andStartdistanceLessThan(Double value) {
            addCriterion("startDistance <", value, "startdistance");
            return (Criteria) this;
        }

        public Criteria andStartdistanceLessThanOrEqualTo(Double value) {
            addCriterion("startDistance <=", value, "startdistance");
            return (Criteria) this;
        }

        public Criteria andStartdistanceIn(List<Double> values) {
            addCriterion("startDistance in", values, "startdistance");
            return (Criteria) this;
        }

        public Criteria andStartdistanceNotIn(List<Double> values) {
            addCriterion("startDistance not in", values, "startdistance");
            return (Criteria) this;
        }

        public Criteria andStartdistanceBetween(Double value1, Double value2) {
            addCriterion("startDistance between", value1, value2, "startdistance");
            return (Criteria) this;
        }

        public Criteria andStartdistanceNotBetween(Double value1, Double value2) {
            addCriterion("startDistance not between", value1, value2, "startdistance");
            return (Criteria) this;
        }

        public Criteria andEnddistanceIsNull() {
            addCriterion("endDistance is null");
            return (Criteria) this;
        }

        public Criteria andEnddistanceIsNotNull() {
            addCriterion("endDistance is not null");
            return (Criteria) this;
        }

        public Criteria andEnddistanceEqualTo(Double value) {
            addCriterion("endDistance =", value, "enddistance");
            return (Criteria) this;
        }

        public Criteria andEnddistanceNotEqualTo(Double value) {
            addCriterion("endDistance <>", value, "enddistance");
            return (Criteria) this;
        }

        public Criteria andEnddistanceGreaterThan(Double value) {
            addCriterion("endDistance >", value, "enddistance");
            return (Criteria) this;
        }

        public Criteria andEnddistanceGreaterThanOrEqualTo(Double value) {
            addCriterion("endDistance >=", value, "enddistance");
            return (Criteria) this;
        }

        public Criteria andEnddistanceLessThan(Double value) {
            addCriterion("endDistance <", value, "enddistance");
            return (Criteria) this;
        }

        public Criteria andEnddistanceLessThanOrEqualTo(Double value) {
            addCriterion("endDistance <=", value, "enddistance");
            return (Criteria) this;
        }

        public Criteria andEnddistanceIn(List<Double> values) {
            addCriterion("endDistance in", values, "enddistance");
            return (Criteria) this;
        }

        public Criteria andEnddistanceNotIn(List<Double> values) {
            addCriterion("endDistance not in", values, "enddistance");
            return (Criteria) this;
        }

        public Criteria andEnddistanceBetween(Double value1, Double value2) {
            addCriterion("endDistance between", value1, value2, "enddistance");
            return (Criteria) this;
        }

        public Criteria andEnddistanceNotBetween(Double value1, Double value2) {
            addCriterion("endDistance not between", value1, value2, "enddistance");
            return (Criteria) this;
        }

        public Criteria andFeeIsNull() {
            addCriterion("fee is null");
            return (Criteria) this;
        }

        public Criteria andFeeIsNotNull() {
            addCriterion("fee is not null");
            return (Criteria) this;
        }

        public Criteria andFeeEqualTo(BigDecimal value) {
            addCriterion("fee =", value, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeNotEqualTo(BigDecimal value) {
            addCriterion("fee <>", value, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeGreaterThan(BigDecimal value) {
            addCriterion("fee >", value, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("fee >=", value, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeLessThan(BigDecimal value) {
            addCriterion("fee <", value, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeLessThanOrEqualTo(BigDecimal value) {
            addCriterion("fee <=", value, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeIn(List<BigDecimal> values) {
            addCriterion("fee in", values, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeNotIn(List<BigDecimal> values) {
            addCriterion("fee not in", values, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("fee between", value1, value2, "fee");
            return (Criteria) this;
        }

        public Criteria andFeeNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("fee not between", value1, value2, "fee");
            return (Criteria) this;
        }

        public Criteria andCostidIsNull() {
            addCriterion("costId is null");
            return (Criteria) this;
        }

        public Criteria andCostidIsNotNull() {
            addCriterion("costId is not null");
            return (Criteria) this;
        }

        public Criteria andCostidEqualTo(Integer value) {
            addCriterion("costId =", value, "costid");
            return (Criteria) this;
        }

        public Criteria andCostidNotEqualTo(Integer value) {
            addCriterion("costId <>", value, "costid");
            return (Criteria) this;
        }

        public Criteria andCostidGreaterThan(Integer value) {
            addCriterion("costId >", value, "costid");
            return (Criteria) this;
        }

        public Criteria andCostidGreaterThanOrEqualTo(Integer value) {
            addCriterion("costId >=", value, "costid");
            return (Criteria) this;
        }

        public Criteria andCostidLessThan(Integer value) {
            addCriterion("costId <", value, "costid");
            return (Criteria) this;
        }

        public Criteria andCostidLessThanOrEqualTo(Integer value) {
            addCriterion("costId <=", value, "costid");
            return (Criteria) this;
        }

        public Criteria andCostidIn(List<Integer> values) {
            addCriterion("costId in", values, "costid");
            return (Criteria) this;
        }

        public Criteria andCostidNotIn(List<Integer> values) {
            addCriterion("costId not in", values, "costid");
            return (Criteria) this;
        }

        public Criteria andCostidBetween(Integer value1, Integer value2) {
            addCriterion("costId between", value1, value2, "costid");
            return (Criteria) this;
        }

        public Criteria andCostidNotBetween(Integer value1, Integer value2) {
            addCriterion("costId not between", value1, value2, "costid");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}