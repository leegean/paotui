package com.runfast.paotui.web.controller.user;

import com.runfast.common.dao.model.RunfastAgentbusiness;
import com.runfast.common.dao.model.RunfastDeliverCost;
import com.runfast.common.dao.model.RunfastShopperExample;
import com.runfast.common.utils.GPSUtil;
import com.runfast.common.web.controller.BaseController;
import com.runfast.common.web.entity.Result;
import com.runfast.common.web.entity.ResultCode;
import com.runfast.paotui.dao.model.*;
import com.runfast.common.service.RunfastAgentbusinessService;
import com.runfast.common.service.RunfastShopperService;
import com.runfast.paotui.service.PaotuiCostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

/**
 * 首页接口
 */
@RestController
@RequestMapping(value = "/api/user/home",headers = "token")
public class HomeController extends BaseController<Catalog, Integer> {

    @Autowired
    private RunfastAgentbusinessService agentbusinessService;

    @Autowired
    private RunfastShopperService shopperService;

    @Autowired
    private PaotuiCostService costService;

    /**
     * 获取用户附件骑手数量
     * @param userLng 用户经度
     * @param userLat 用户纬度
     * @return
     */
    @PostMapping("/driverCountNearBy")
    public Result driverCountNearBy(@RequestParam double userLng, @RequestParam double userLat) {
        RunfastAgentbusiness agentNearBy = agentbusinessService.getAgentNearByRange(userLng, userLat);

        if(agentNearBy==null)return Result.fail(ResultCode.AGENT_NEAR_BY_NOT_EXIST);
        RunfastShopperExample shopperExample = new RunfastShopperExample();
        shopperExample.createCriteria().andAgentIdEqualTo(agentNearBy.getId());
        long count = shopperService.countByExample(shopperExample);
        return Result.ok("", count);
    }

    /**
     * 获取跑腿基础配送费
     * @param agentId
     * @return
     */
    @PostMapping("/baseDeliveryFee")
    public Result baseDeliveryFee(@RequestParam Integer agentId) {

        PaotuiCost defaultCost = costService.getDefaultCost(agentId);
        if(defaultCost!=null) return Result.ok("", defaultCost);
        else return Result.fail(ResultCode.FAIL, "代理商未设置跑腿配送模板");

    }



}
