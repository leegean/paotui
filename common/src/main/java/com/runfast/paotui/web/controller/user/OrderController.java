package com.runfast.paotui.web.controller.user;

import com.runfast.common.utils.TokenUtil;
import com.runfast.common.web.controller.BaseController;
import com.runfast.common.web.entity.Result;
import com.runfast.paotui.dao.model.*;
import com.runfast.paotui.service.OrderService;
import com.runfast.paotui.service.OrderStatusHistoryService;
import com.runfast.common.service.RunfastCuseraddressService;
import com.runfast.paotui.web.dto.OrderDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * 跑腿订单接口
 */
@RestController("userOrderController")
@RequestMapping(value= "/api/user/order", headers = "token")
public class OrderController extends BaseController<Order, Integer> {

    @Autowired
    private OrderService orderService;

    @Autowired
    private RunfastCuseraddressService cuseraddressService;

    @Autowired
    private OrderStatusHistoryService orderStatusHistoryService;
    /**
     * 获取订单分页列表
     * @param pageable 分页请求参数
     * @return
     */
    @PostMapping("/list")
    public Result<List<OrderDto>> list(@PageableDefault Pageable pageable, @RequestHeader String token) throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        Integer userId = TokenUtil.getUserId(token);
        OrderExample orderExample = new OrderExample();
        orderExample.setOrderByClause("id desc");
        orderExample.createCriteria().andCuserIdEqualTo(userId);
        List<Order> orders = orderService.selectByExampleWithPageable(orderExample, pageable);

        List<OrderDto> orderDtos = new ArrayList<>();
        for (Order order : orders) {
            orderDtos.add(orderService.toOrderDto(order));
        }

        return Result.ok("", orderDtos);
    }



    /**
     * 确认订单
     * @param order 请求参数
     * @return
     */
    @PostMapping(value = "/confirm")
    public Result confirm(@ModelAttribute Order order, @RequestHeader String token) {

//        order.setCuserId(SessionContext.getCurrentUser().getId());
        Integer userId = TokenUtil.getUserId(token);
        order.setCuserId(userId);
        return orderService.confirm(order);

    }

    /**
     * 取消订单
     * @param orderId 订单Id
     * @return
     */
    @PostMapping("/cancel")
    public Result cancel(@RequestParam Integer orderId, @RequestHeader String token) {
        Integer userId = TokenUtil.getUserId(token);
        return orderService.applyCancel(userId, orderId);

    }


    /**
     * 根据填写的订单获取配送信息
     * @param order 
     * @return
     */
    @PostMapping("/fillIn")
    public Result fillIn(@ModelAttribute Order order, @RequestHeader String token) {
        Integer userId = TokenUtil.getUserId(token);
        order.setCuserId(userId);
        return orderService.fillIn(order);

    }


    /**
     * 删除订单
     * @param orderId 订单id
     * @param token
     * @return
     */
    @PostMapping("/delete")
    public Result delete(@RequestParam int orderId, @RequestHeader String token) {
        Integer userId = TokenUtil.getUserId(token);
        Order order = new Order();
        order.setId(orderId);
        order.setDeleted(true);
        orderService.updateByPrimaryKeySelective(order);
        return Result.ok("");

    }

    /**
     * 再来一单
     * @param orderId
     * @param token
     * @return
     */
    @PostMapping("/recur")
    public Result recur(@RequestParam int orderId, @RequestHeader String token) {

        Order order = orderService.selectByPrimaryKey(orderId);
        return Result.ok("", order);

    }

    /**
     * 获取订单状态列表
     * @param orderId 订单id
     * @param token
     * @return
     */
    @PostMapping("/status")
    public Result status(@RequestParam int orderId, @RequestHeader String token) {


        OrderStatusHistoryExample orderStatusHistoryExample = new OrderStatusHistoryExample();
        orderStatusHistoryExample.createCriteria().andOrderIdEqualTo(orderId);
        orderStatusHistoryExample.setOrderByClause("id desc");

        List<OrderStatusHistory> orderStatusHistories = orderStatusHistoryService.selectByExample(orderStatusHistoryExample);
        return Result.ok("", orderStatusHistories);

    }


    @PostMapping("/detail")
    public Result<OrderDto> detail(@RequestParam int orderId, @RequestHeader String token) throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {


        Order order = orderService.selectByPrimaryKey(orderId);
        return Result.ok("", orderService.toOrderDto(order));

    }

    @PostMapping("/wallletPay")
    public Result  wallletPay(@RequestParam Integer orderId, @RequestParam String password ) {

        return orderService.walletPay(orderId, password);
    }


}
