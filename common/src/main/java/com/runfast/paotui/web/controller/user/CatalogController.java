package com.runfast.paotui.web.controller.user;

import com.gxuc.runfast.bean.Cuser;
import com.runfast.common.utils.SessionContext;
import com.runfast.common.utils.TokenUtil;
import com.runfast.common.web.controller.BaseController;
import com.runfast.common.web.entity.Result;
import com.runfast.common.web.entity.ResultCode;
import com.runfast.paotui.dao.model.Catalog;
import com.runfast.paotui.dao.model.CatalogExample;
import com.runfast.paotui.dao.type.ListToStringTypeHandler;
import com.runfast.paotui.service.CatalogService;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.runfast.paotui.dao.type.ListToStringTypeHandler.separator;

/**
 * 分类接口
 */
@RestController
@RequestMapping(value = "/api/user/catalog",headers = "token")
public class CatalogController extends BaseController<Catalog, Integer> {

    @Autowired
    private CatalogService catalogService;

    /**
     * 获取商品分类列表
     * @param token
     * @return
     */
    @PostMapping("/list")
    public Result<List<Catalog>> list(@RequestHeader String token) {
        List<Catalog> catalogs = catalogService.selectByExampleWithBLOBs(null);
        return Result.ok("", catalogs);
    }

    /**
     * 添加分类目录
     * @param name 分类名
     * @param suggestions 推荐商品名，多个以','分割
     * @param token
     * @return
     */
    @PostMapping("/add")
    public Result add(@RequestParam String name,@RequestParam String suggestions,@RequestHeader String token) {

        Validate.notBlank(name,"name 不能为空");
        Validate.notBlank(suggestions, "suggestions 不能为空,且必须以','分割");
        Integer userId = TokenUtil.getUserId(token);

        CatalogExample catalogExample = new CatalogExample();
        catalogExample.createCriteria().andNameEqualTo(name);
        List<Catalog> catalogs = catalogService.selectByExample(catalogExample);
        if(!catalogs.isEmpty())return Result.fail(ResultCode.PARAMETER_ERROR,"已经存在该分类");


        Catalog catalog = new Catalog();
        catalog.setName(name);

        String[] strings = suggestions.split("["+separator[0]+","+separator[1]+"]", -1);
        ArrayList<String> suggestionList = new ArrayList<>();
        for (String string : strings) {

            suggestionList.add(string);
        }
        catalog.setCreateBy(userId);
        catalog.setCreateTime(new Date());
        catalog.setDeleted(false);
        catalog.setHot(false);
        catalog.setUpdateBy(userId);
        catalog.setUpdateTime(new Date());
        catalog.setSuggestions(suggestionList);
        catalogService.insertSelective(catalog);
        return Result.ok("", catalog);
    }


}
