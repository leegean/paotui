package com.runfast.paotui.web.controller.sys;

import com.runfast.common.web.entity.Result;
import com.runfast.pay.service.PayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@RestController("sysPayController")
@RequestMapping("/api/sys/pay")
public class PayController {

    @Resource(name = "ptPayService")
    private PayService payService;


    @PostMapping("alipayNotify")
    public String alipayNotify(Map<String, String> paramsNotify) {
        return payService.handleAlipayNotify(paramsNotify);
    }

    @PostMapping("wxpayNotify")
    public String wxpayNotify(String xmlNotify) {
        return payService.handleWxpayNotify(xmlNotify);
    }

    @PostMapping("wxpayRefundNotify")
    public String wxpayRefundNotify(String xmlNotify) {
        return payService.handleWxpayRefundNotify(xmlNotify);
    }


    @PostMapping("orderQuery")
    public Result orderQuery(String orderNo) {
        return payService.orderQuery(orderNo);
    }


}
