package com.runfast.paotui.web.controller.user;

import com.alipay.api.AlipayApiException;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.internal.util.AlipayUtils;
import com.runfast.common.web.entity.Result;
import com.runfast.common.web.entity.ResultCode;
import com.runfast.pay.Channel;
import com.runfast.pay.alipay.AlipayConfig;
import com.runfast.pay.service.PayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

/**
 * 支付接口
 */
@RestController("userPayController")
@RequestMapping(value = "/api/user/pay",headers = "token")
public class PayController {

    @Resource(name = "ptPayService")
    private PayService payService;

    /**
     * 获取支付宝或微信预支付信息
     * @param channel 支付渠道
     * @param orderNo 订单号
     * @param request
     * @return
     */
    @PostMapping("prepay")
    public Result<String> prepay(@RequestParam Channel channel, @RequestParam String orderNo, HttpServletRequest request) {
        return payService.prepay(channel, orderNo, request.getRemoteAddr().endsWith("0:0:0:0:0:0:0:1")?"127.0.0.1":request.getRemoteAddr());
    }


    /**
     * 查询订单状态
     * @param orderNo 订单号
     * @return
     */
    @PostMapping("orderQuery")
    public Result orderQuery(@RequestParam String orderNo) {
        return payService.orderQuery(orderNo);
    }


    /**
     * 退款查询
     * @param orderNo 订单号
     * @return
     */
    @PostMapping("refundQuery")
    public Result refundQuery(@RequestParam String orderNo) {
        return payService.refundQuery(orderNo);
    }


    @PostMapping("check")
    public Result check(@RequestParam String content,String sign) throws AlipayApiException {
        boolean b = AlipaySignature.rsaCheck(content, sign, AlipayConfig.ali_public_key, AlipayConfig.input_charset, AlipayConfig.sign_type);
        return b?Result.ok(""):Result.fail(ResultCode.FAIL,"校验失败");
    }


    @PostMapping("refund")
    public Result refund(@RequestParam String orderNo, @RequestParam Integer amountPaid) {


        return payService.refund(orderNo, UUID.randomUUID().toString(), amountPaid, "手动退款");
    }

}
