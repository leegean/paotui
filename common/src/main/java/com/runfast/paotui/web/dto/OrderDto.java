package com.runfast.paotui.web.dto;

import com.runfast.paotui.dao.model.Order;
import com.runfast.pay.Channel;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @author: lijin
 * @date: 2018年05月03日
 */

@Data
public class OrderDto extends Order{

    /**
     * 源地址用户名称
     */
    private String fromName;

    /**
     * 源地址用户手机号
     */
    private String fromMobile;


    /**
     * 源地址
     */
    private String toAddress;

    /**
     * 源地址用户名称
     */
    private String toName;

    /**
     * 源地址用户手机号
     */
    private String toMobile;


    /**
     * 骑手姓名
     */
    private String driverName;

    /**
     * 骑手手机号
     */
    private String driverMobile;



}
