package com.runfast.paotui.web.controller.user;

import com.runfast.common.web.entity.Result;
import com.runfast.paotui.dao.model.Catalog;
import com.runfast.paotui.mq.AmqpClient;
import com.runfast.pay.Channel;
import com.runfast.pay.service.PayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 支付接口
 */
@RestController
@RequestMapping(value = "/api/user/amqp",headers = "token")
public class AmqpController {

    @Autowired
    private AmqpClient amqpClient;
    @PostMapping("publish")
    public Result send() {
        return Result.ok("");
    }


}
