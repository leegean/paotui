package com.runfast.paotui.web.controller.driver;

import com.gxuc.runfast.bean.Shopper;
import com.runfast.common.utils.SessionContext;
import com.runfast.common.web.controller.BaseController;
import com.runfast.common.web.entity.Result;
import com.runfast.paotui.dao.model.Order;
import com.runfast.paotui.dao.model.OrderExample;
import com.runfast.paotui.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController("driverOrderController")
@RequestMapping("/api/driver/order")
public class OrderController extends BaseController<Order, Integer> {

    @Autowired
    private OrderService orderService;


    @PostMapping("/list")
    public Result<List<Order>> list(@PageableDefault Pageable pageable) {

        Shopper currentDriver = SessionContext.getCurrentDriver();
        OrderExample orderExample = new OrderExample();
        orderExample.createCriteria().andDriverIdEqualTo(currentDriver.getId());
        List<Order> orders = orderService.selectByExampleWithPageable(orderExample, pageable);
        return Result.ok("", orders);
    }

    @PostMapping("/take")
    public Result take(Integer orderId) {
        Integer id = SessionContext.getCurrentDriver().getId();
        return orderService.take(id, orderId);

    }

}
