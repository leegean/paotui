package com.runfast.paotui.task.scheduler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * @author: lijin
 * @date: 2018年03月03日
 */
@Component
public class ExampleBusinessObject implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExampleBusinessObject.class);

    public void doIt() {
        // do the actual work
        LOGGER.info("----------------------");
    }
}
