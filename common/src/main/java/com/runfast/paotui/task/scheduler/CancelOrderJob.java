package com.runfast.paotui.task.scheduler;

import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * @author: lijin
 * @date: 2018年04月13日
 */
@Slf4j
public class CancelOrderJob extends QuartzJobBean {

    private String name;


    // Inject the "name" job data property
    public void setName(String name) {
    }

    @Override
    protected void executeInternal(JobExecutionContext context)
            throws JobExecutionException {
        log.info("===============" + context.getJobInstance().getClass() + ": " + context.getFireTime());
    }

}
