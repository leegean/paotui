package com.runfast.paotui.task.async;

import cn.jpush.api.push.PushResult;
import com.runfast.common.dao.model.*;
import com.runfast.common.service.RunfastLoginRecordService;
import com.runfast.paotui.dao.model.Order;
import com.runfast.paotui.dao.model.Role;
import com.runfast.paotui.mq.AmqpClient;
import com.runfast.common.service.RunfastCuserService;
import com.runfast.common.service.RunfastShopperService;
import com.runfast.paotui.utils.JiGuangPushUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author: lijin
 * @date: 2018年04月13日
 */

@Async
@Component
@Slf4j
public class MessagePushTask {

    @Autowired
    private RunfastShopperService shopperService;

    @Autowired
    private RunfastCuserService cuserService;

    @Autowired
    private RunfastLoginRecordService loginRecordService;

    @Autowired
    private AmqpClient amqpClient;


    public void newOrderNotify(String orderNo, Integer agentNearById) {
        HashMap<String, String> params = new HashMap<>();
        params.put("orderNo", orderNo);
        String title = "跑腿订单提醒";
        String alert = "您有新的跑腿订单" + orderNo;
        String sound = "neworder.caf";


        RunfastShopperExample shopperExample = new RunfastShopperExample();
        shopperExample.createCriteria().andAgentIdEqualTo(agentNearById);
        List<RunfastShopper> shoppers = shopperService.selectByExample(shopperExample);

        List<String> driverAliasList = new ArrayList<>();
        for (RunfastShopper shopper : shoppers) {

            RunfastLoginRecordExample loginRecordExample = new RunfastLoginRecordExample();
            loginRecordExample.or().andAccountTypeEqualTo(2).andAccountIdEqualTo(shopper.getId()).andLogoutTimeIsNull();
            List<RunfastLoginRecord> loginRecordList = loginRecordService.selectByExample(loginRecordExample);


            for (RunfastLoginRecord loginRecord : loginRecordList) {

                String alias = loginRecord.getAlias();
                if(StringUtils.isNotBlank(alias)) driverAliasList.add(alias);
            }
        }
        if (!driverAliasList.isEmpty()) {
            PushResult pushResult = JiGuangPushUtil.push(Role.Predefine.DRIVER, 600, driverAliasList, alert,title, params, sound);
            if (!pushResult.isResultOK()) {

                StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[1];
                log.error("文件名：" + stackTraceElement.getFileName()
                        + " 类和方法名：" + stackTraceElement.getClassName() + "." + stackTraceElement.getMethodName()
                        + " 行号：" + stackTraceElement.getLineNumber()
                        + " 描述：" + pushResult.getOriginalContent());
            }
        }

    }
    public void applyCancelOrderNotify(Order order) {
        amqpClient.publish(AmqpClient.PtBindingKey, order);

        String orderNo = order.getOrderNo();
        HashMap<String, String> params = new HashMap<>();
        params.put("orderNo", orderNo);
        String title = "跑腿订单提醒";
        String alert = "用户申请取消订单" + orderNo;
        String sound = null;


        /**
         * 推送通知给骑手
         */

        Integer driverId = order.getDriverId();
        if(driverId!=null){
            List<String> driverAliasList = new ArrayList<>();
            driverAliasList.add(driverId+"");

            RunfastLoginRecordExample loginRecordExample = new RunfastLoginRecordExample();
            loginRecordExample.or().andAccountTypeEqualTo(2).andAccountIdEqualTo(driverId).andLogoutTimeIsNull();
            List<RunfastLoginRecord> loginRecordList = loginRecordService.selectByExample(loginRecordExample);


            for (RunfastLoginRecord loginRecord : loginRecordList) {

                String alias = loginRecord.getAlias();
                if(StringUtils.isNotBlank(alias)) driverAliasList.add(alias);
            }
            if (!driverAliasList.isEmpty()) {
                PushResult pushResult = JiGuangPushUtil.push(Role.Predefine.DRIVER, 600, driverAliasList, alert,title, params, sound);
                if (!pushResult.isResultOK()) {

                    StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[1];
                    log.error("文件名：" + stackTraceElement.getFileName()
                            + " 类和方法名：" + stackTraceElement.getClassName() + "." + stackTraceElement.getMethodName()
                            + " 行号：" + stackTraceElement.getLineNumber()
                            + " 描述：" + pushResult.getOriginalContent());
                }
            }
        }



    }

}
