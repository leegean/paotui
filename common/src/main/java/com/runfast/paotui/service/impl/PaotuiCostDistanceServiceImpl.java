package com.runfast.paotui.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.paotui.dao.model.PaotuiCostDistance;
import com.runfast.paotui.dao.model.PaotuiCostDistanceExample;
import com.runfast.paotui.service.PaotuiCostDistanceService;
import org.springframework.stereotype.Service;

@Service
public class PaotuiCostDistanceServiceImpl extends BaseService<PaotuiCostDistance, Integer, PaotuiCostDistanceExample> implements PaotuiCostDistanceService {
}