package com.runfast.paotui.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.paotui.dao.model.Refund;
import com.runfast.paotui.dao.model.RefundExample;
import com.runfast.paotui.service.RefundService;
import org.springframework.stereotype.Service;

@Service
public class RefundServiceImpl extends BaseService<Refund, Integer, RefundExample> implements RefundService {
}