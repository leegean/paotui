package com.runfast.paotui.service;

import com.runfast.common.service.IService;
import com.runfast.paotui.dao.model.Role;
import com.runfast.paotui.dao.model.RoleExample;

public interface RoleService extends IService<Role, Integer, RoleExample> {
}