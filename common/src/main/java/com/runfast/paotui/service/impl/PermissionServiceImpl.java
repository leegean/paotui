package com.runfast.paotui.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.paotui.dao.model.Permission;
import com.runfast.paotui.dao.model.PermissionExample;
import com.runfast.paotui.service.PermissionService;
import org.springframework.stereotype.Service;

@Service
public class PermissionServiceImpl extends BaseService<Permission, Integer, PermissionExample> implements PermissionService {
}