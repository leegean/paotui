package com.runfast.paotui.service;

import com.runfast.common.service.IService;
import com.runfast.common.web.entity.Result;
import com.runfast.paotui.dao.model.Order;
import com.runfast.paotui.dao.model.OrderExample;
import com.runfast.paotui.web.dto.OrderDto;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

public interface OrderService extends IService<Order, Integer, OrderExample> {
    Result confirm(Order order);

    Result take(Integer driverId, Integer orderId);

    Result cancel(Integer userId, Integer orderId);

    List<Map<String, Integer>> selectIdUserIdByExample(OrderExample example);

    Result fillIn(Order order);

    OrderDto toOrderDto(Order order) throws IllegalAccessException, InvocationTargetException, NoSuchMethodException;

    void complete(Integer userId, Integer id);

    Result walletPay(Integer orderId, String password);

    Result applyCancel(Integer userId, Integer orderId);

    List<Map<String, Integer>> findConfirmCancelOrder(Date time);

    void confirmCancel(Integer userId, Integer id);
}