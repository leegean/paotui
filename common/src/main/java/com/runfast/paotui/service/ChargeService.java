package com.runfast.paotui.service;

import com.runfast.common.service.IService;
import com.runfast.paotui.dao.model.Charge;
import com.runfast.paotui.dao.model.ChargeExample;

public interface ChargeService extends IService<Charge, Integer, ChargeExample> {
}