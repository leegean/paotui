package com.runfast.paotui.service;

import com.runfast.common.service.IService;
import com.runfast.paotui.dao.model.Catalog;
import com.runfast.paotui.dao.model.CatalogExample;

public interface CatalogService extends IService<Catalog, Integer, CatalogExample> {
}