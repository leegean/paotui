package com.runfast.paotui.service;

import com.runfast.common.service.IService;
import com.runfast.paotui.dao.model.PaotuiCostWeight;
import com.runfast.paotui.dao.model.PaotuiCostWeightExample;

public interface PaotuiCostWeightService extends IService<PaotuiCostWeight, Integer, PaotuiCostWeightExample> {
}