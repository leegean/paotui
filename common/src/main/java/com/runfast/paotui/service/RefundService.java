package com.runfast.paotui.service;

import com.runfast.common.service.IService;
import com.runfast.paotui.dao.model.Refund;
import com.runfast.paotui.dao.model.RefundExample;

public interface RefundService extends IService<Refund, Integer, RefundExample> {
}