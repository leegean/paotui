package com.runfast.paotui.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.paotui.dao.model.PaotuiCostWeight;
import com.runfast.paotui.dao.model.PaotuiCostWeightExample;
import com.runfast.paotui.service.PaotuiCostWeightService;
import org.springframework.stereotype.Service;

@Service
public class PaotuiCostWeightServiceImpl extends BaseService<PaotuiCostWeight, Integer, PaotuiCostWeightExample> implements PaotuiCostWeightService {
}