package com.runfast.paotui.service;

import com.runfast.common.service.IService;
import com.runfast.paotui.dao.model.PaotuiCost;
import com.runfast.paotui.dao.model.PaotuiCostExample;

public interface PaotuiCostService extends IService<PaotuiCost, Integer, PaotuiCostExample> {

    PaotuiCost getDefaultCost(Integer agentId);
}