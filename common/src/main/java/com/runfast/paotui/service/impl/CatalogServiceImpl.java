package com.runfast.paotui.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.paotui.dao.model.Catalog;
import com.runfast.paotui.dao.model.CatalogExample;
import com.runfast.paotui.service.CatalogService;
import org.springframework.stereotype.Service;

@Service
public class CatalogServiceImpl extends BaseService<Catalog, Integer, CatalogExample> implements CatalogService {
}