package com.runfast.paotui.service;

import com.runfast.common.service.IService;
import com.runfast.paotui.dao.model.OrderStatusHistory;
import com.runfast.paotui.dao.model.OrderStatusHistoryExample;

public interface OrderStatusHistoryService extends IService<OrderStatusHistory, Integer, OrderStatusHistoryExample> {
}