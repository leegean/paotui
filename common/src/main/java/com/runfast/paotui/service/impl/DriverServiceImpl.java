package com.runfast.paotui.service.impl;

import com.runfast.paotui.service.DriverService;
import com.runfast.paotui.service.OrderService;
import com.runfast.common.service.RunfastAgentbusinessService;
import com.runfast.common.service.RunfastShopperService;
import com.runfast.paotui.task.async.MessagePushTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: lijin
 * @date: 2018年04月14日
 */
@Service
public class DriverServiceImpl implements DriverService {

    @Autowired
    private OrderService orderService;

    @Autowired
    private RunfastAgentbusinessService agentbusinessService;

    @Autowired
    private RunfastShopperService shopperService;


    @Autowired
    private MessagePushTask messagePushTask;


}
