package com.runfast.paotui.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.paotui.dao.model.OrderStatusHistory;
import com.runfast.paotui.dao.model.OrderStatusHistoryExample;
import com.runfast.paotui.service.OrderStatusHistoryService;
import org.springframework.stereotype.Service;

@Service
public class OrderStatusHistoryServiceImpl extends BaseService<OrderStatusHistory, Integer, OrderStatusHistoryExample> implements OrderStatusHistoryService {
}