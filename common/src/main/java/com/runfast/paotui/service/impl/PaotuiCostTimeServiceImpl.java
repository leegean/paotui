package com.runfast.paotui.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.paotui.dao.model.PaotuiCostTime;
import com.runfast.paotui.dao.model.PaotuiCostTimeExample;
import com.runfast.paotui.service.PaotuiCostTimeService;
import org.springframework.stereotype.Service;

@Service
public class PaotuiCostTimeServiceImpl extends BaseService<PaotuiCostTime, Integer, PaotuiCostTimeExample> implements PaotuiCostTimeService {
}