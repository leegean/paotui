package com.runfast.paotui.service;

import com.runfast.common.service.IService;
import com.runfast.paotui.dao.model.WeightTemplate;
import com.runfast.paotui.dao.model.WeightTemplateExample;

public interface WeightTemplateService extends IService<WeightTemplate, Integer, WeightTemplateExample> {
}