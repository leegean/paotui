package com.runfast.paotui.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.paotui.dao.model.WeightTemplate;
import com.runfast.paotui.dao.model.WeightTemplateExample;
import com.runfast.paotui.service.WeightTemplateService;
import org.springframework.stereotype.Service;

@Service
public class WeightTemplateServiceImpl extends BaseService<WeightTemplate, Integer, WeightTemplateExample> implements WeightTemplateService {
}