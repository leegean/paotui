package com.runfast.paotui.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.paotui.dao.model.Charge;
import com.runfast.paotui.dao.model.ChargeExample;
import com.runfast.paotui.service.ChargeService;
import org.springframework.stereotype.Service;

@Service
public class ChargeServiceImpl extends BaseService<Charge, Integer, ChargeExample> implements ChargeService {
}