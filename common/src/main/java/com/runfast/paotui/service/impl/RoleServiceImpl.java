package com.runfast.paotui.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.paotui.dao.model.Role;
import com.runfast.paotui.dao.model.RoleExample;
import com.runfast.paotui.service.RoleService;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl extends BaseService<Role, Integer, RoleExample> implements RoleService {
}