package com.runfast.paotui.service;

import com.runfast.common.service.IService;
import com.runfast.paotui.dao.model.Account;
import com.runfast.paotui.dao.model.AccountExample;

public interface AccountService extends IService<Account, Integer, AccountExample> {
    Account login(String username, String password);
}