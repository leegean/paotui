package com.runfast.paotui.service;

import com.runfast.common.service.IService;
import com.runfast.paotui.dao.model.User;
import com.runfast.paotui.dao.model.UserExample;

public interface UserService extends IService<User, Integer, UserExample> {
}