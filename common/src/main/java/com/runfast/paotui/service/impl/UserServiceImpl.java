package com.runfast.paotui.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.paotui.dao.model.User;
import com.runfast.paotui.dao.model.UserExample;
import com.runfast.paotui.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends BaseService<User, Integer, UserExample> implements UserService {
}