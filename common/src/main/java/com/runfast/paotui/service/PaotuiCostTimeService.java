package com.runfast.paotui.service;

import com.runfast.common.service.IService;
import com.runfast.paotui.dao.model.PaotuiCostTime;
import com.runfast.paotui.dao.model.PaotuiCostTimeExample;

public interface PaotuiCostTimeService extends IService<PaotuiCostTime, Integer, PaotuiCostTimeExample> {
}