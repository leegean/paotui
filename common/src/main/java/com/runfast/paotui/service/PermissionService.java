package com.runfast.paotui.service;

import com.runfast.common.service.IService;
import com.runfast.paotui.dao.model.Permission;
import com.runfast.paotui.dao.model.PermissionExample;

public interface PermissionService extends IService<Permission, Integer, PermissionExample> {
}