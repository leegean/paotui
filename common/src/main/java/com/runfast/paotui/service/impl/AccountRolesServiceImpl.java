package com.runfast.paotui.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.paotui.dao.model.AccountRoles;
import com.runfast.paotui.dao.model.AccountRolesExample;
import com.runfast.paotui.service.AccountRolesService;
import org.springframework.stereotype.Service;

@Service
public class AccountRolesServiceImpl extends BaseService<AccountRoles, Integer, AccountRolesExample> implements AccountRolesService {
}