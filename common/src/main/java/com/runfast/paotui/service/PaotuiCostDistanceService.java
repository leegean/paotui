package com.runfast.paotui.service;

import com.runfast.common.service.IService;
import com.runfast.paotui.dao.model.PaotuiCostDistance;
import com.runfast.paotui.dao.model.PaotuiCostDistanceExample;

public interface PaotuiCostDistanceService extends IService<PaotuiCostDistance, Integer, PaotuiCostDistanceExample> {
}