package com.runfast.paotui.service;

import com.runfast.common.service.IService;
import com.runfast.paotui.dao.model.AccountRoles;
import com.runfast.paotui.dao.model.AccountRolesExample;

public interface AccountRolesService extends IService<AccountRoles, Integer, AccountRolesExample> {
}