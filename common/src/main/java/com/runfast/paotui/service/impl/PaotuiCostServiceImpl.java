package com.runfast.paotui.service.impl;

import com.runfast.common.dao.model.RunfastDeliverCost;
import com.runfast.common.exception.BaseException;
import com.runfast.common.service.BaseService;
import com.runfast.common.utils.DateUtils;
import com.runfast.common.web.entity.ResultCode;
import com.runfast.paotui.dao.model.PaotuiCost;
import com.runfast.paotui.dao.model.PaotuiCostExample;
import com.runfast.paotui.dao.model.WeightTemplate;
import com.runfast.paotui.dao.model.WeightTemplateExample;
import com.runfast.paotui.entity.DeliveryCost;
import com.runfast.paotui.service.PaotuiCostService;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
public class PaotuiCostServiceImpl extends BaseService<PaotuiCost, Integer, PaotuiCostExample> implements PaotuiCostService {
    @Override
    public PaotuiCost getDefaultCost(Integer agentId) {
        PaotuiCostExample costExample = new PaotuiCostExample();
        costExample.or().andAgentidEqualTo(agentId);
        costExample.setOrderByClause("id desc");

        List<PaotuiCost> costList = this.selectByExample(costExample);
        if(!costList.isEmpty()){
            for (PaotuiCost cost : costList) {
                Integer isdefault = cost.getIsdefault();
                if(isdefault!=null&&isdefault==1) return cost;
            }

            return costList.get(0);
        }else{
            return null;
        }
    }



}