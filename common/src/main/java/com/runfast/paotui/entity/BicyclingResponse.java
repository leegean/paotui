package com.runfast.paotui.entity;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

/**
 * @author: lijin
 * @date: 2018年03月29日
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class BicyclingResponse {

    private Integer errcode;
    private String errdetail;
    private String errmsg;
    private Data data;

    @lombok.Data
    public static class Data {
        private String origin;
        private String destination;
        private List<Path> paths;
    }

    @lombok.Data
    public static class Path {

        private Integer distance;
        private Integer duration;
        private List<Step> steps;
    }

    @lombok.Data
    public static class Step {

        private String instruction;
        private String road;
        private Integer distance;
        private String orientation;
        private Integer duration;
        private String polyline;
        private String action;
        private String assistant_action;
    }
}
