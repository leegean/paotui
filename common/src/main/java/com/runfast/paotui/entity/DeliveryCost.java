package com.runfast.paotui.entity;

import lombok.Data;

/**
 * 配送费包装类
 * @author: lijin
 * @date: 2018年05月08日
 */
@Data
public class DeliveryCost {

    private Integer baseFee;

    private Integer exceedFee;

    private Integer weightFee;

    private Integer deliveryFee;
}
