package com.runfast.paotui.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author: lijin
 * @date: 2018年03月29日
 */
@Data
public class Region implements Serializable {

    private Integer agentId;
    private String location;
    private String province;
    private String city;
    private String district;
    private String township;

    public Region(Integer agentId, String location, String province, String city, String district, String township) {
        this.agentId = agentId;
        this.location = location;
        this.province = province;
        this.city = city;
        this.district = district;
        this.township = township;
    }


    public Region(String location, String province, String city, String district, String township) {
        this.location = location;
        this.province = province;
        this.city = city;
        this.district = district;
        this.township = township;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }
}
