package com.runfast.paotui.utils;

import com.runfast.common.utils.JsonUtils;
import com.runfast.paotui.entity.BicyclingResponse;
import com.runfast.paotui.entity.Region;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: lijin
 * @date: 2018年03月28日
 */
@Slf4j
public abstract class AmapUtil {

    private static String key = "b5c718addc2ca374a24e326734c1eb9c";
    private static String key1 = "444f905bcb7010882717cae2a41f0653";

    private static RestTemplate restTemplate = new RestTemplate();

    public static String geo(String address, boolean batch) {


        HashMap<String, String> params = new HashMap<>();
        params.put("key", key);
        params.put("address", address);
        params.put("batch", String.valueOf(batch));
        String response = restTemplate.getForObject("http://restapi.amap.com/v3/geocode/geo?key={key}&address={address}&batch={batch}", String.class, params);


        return response;
    }

    public static String regeo(String location, boolean batch) {


        HashMap<String, String> params = new HashMap<>();
        params.put("key", key);
        params.put("location", location);
        params.put("batch", String.valueOf(batch));
        String response = restTemplate.getForObject("http://restapi.amap.com/v3/geocode/regeo?key={key}&location={location}&batch={batch}", String.class, params);

        return response;
    }

    public static List<String> getLocaions(String address, boolean batch) {
        ArrayList<String> locations = new ArrayList<>();
        String response = geo(address, batch);
        try {
            Map map = JsonUtils.getMapper().readValue(response, Map.class);
            Object status = map.get("status");
            if ("1".equals(status)) {
                ArrayList<Map> list = new ArrayList<>();
                if (batch) {
                    List geocodes = (List) map.get("geocodes");
                    list.addAll(geocodes);
                } else {

                    List geocodes = (List) map.get("geocodes");
                    list.addAll(geocodes);
                }


                for (Map geocode : list) {
                    Object location = geocode.get("location");
                    locations.add(location.toString());
                }


            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return locations;
    }

    public static List<Region> getRegins(String location, boolean batch) {
        log.info("getRegins location: " + location);
        ArrayList<Region> regions = new ArrayList<>();

        String response = regeo(location, batch);
        try {
            Map map = JsonUtils.getMapper().readValue(response, Map.class);
            Object status = map.get("status");
            if ("1".equals(status)) {

                ArrayList<Map> list = new ArrayList<>();
                if (batch) {
                    List regeocodes = (List) map.get("regeocodes");
                    list.addAll(regeocodes);
                } else {

                    Map regeocode = (Map) map.get("regeocode");
                    list.add(regeocode);
                }

                for (Map regeocode : list) {
                    Map addressComponent = (Map) regeocode.get("addressComponent");
                    Object province = addressComponent.get("province");
                    Object city = addressComponent.get("city");
                    Object district = addressComponent.get("district");
                    Object township = addressComponent.get("township");

                    String[] split = location.split("\\|");

                    log.info(province + " ," + city + " ," + district + " ," + township);
                    Region region = new Region(split[0], String.valueOf(province), String.valueOf(city), String.valueOf(district), String.valueOf(township));

                    regions.add(region);
                }

            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }


        return regions;
    }


    public static BicyclingResponse bicycling(String origin, String destination) {
            String[] originArr = origin.split(",");
            String[] destinationArr = destination.split(",");
            BigDecimal originBigDecimal0 = BigDecimal.valueOf(Double.valueOf(originArr[0]));
            BigDecimal originDecimal0 = originBigDecimal0.setScale(6, BigDecimal.ROUND_HALF_DOWN);


            BigDecimal originBigDecimal1 = BigDecimal.valueOf(Double.valueOf(originArr[1]));
            BigDecimal originDecimal1 = originBigDecimal1.setScale(6, BigDecimal.ROUND_HALF_DOWN);

            BigDecimal destinationBigDecimal0 = BigDecimal.valueOf(Double.valueOf(destinationArr[0]));
            BigDecimal destinationDecimal0 = destinationBigDecimal0.setScale(6, BigDecimal.ROUND_HALF_DOWN);

            BigDecimal destinationBigDecimal1 = BigDecimal.valueOf(Double.valueOf(destinationArr[1]));
            BigDecimal destinationDecimal1 = destinationBigDecimal1.setScale(6, BigDecimal.ROUND_HALF_DOWN);


            HashMap<String, String> params = new HashMap<>();
            params.put("key", key);
            params.put("origin", originDecimal0.toString() + "," + originDecimal1.toString());
            params.put("destination", destinationDecimal0.toString() + "," + destinationDecimal1.toString());

        ResponseEntity<String> responseEntity = restTemplate.getForEntity("http://restapi.amap.com/v4/direction/bicycling?key={key}&origin={origin}&destination={destination}", String.class, params);
        HttpStatus statusCode = responseEntity.getStatusCode();
        if (statusCode.is2xxSuccessful()) {
            try {
                return JsonUtils.getMapper().readValue(responseEntity.getBody(), BicyclingResponse.class);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } else {
            throw new RuntimeException(responseEntity.getBody());
        }
        }

}


