package com.runfast.paotui.utils;

import com.runfast.common.utils.SpringUtils;
import com.runfast.paotui.dao.model.Order;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author: lijin
 * @date: 2018年03月28日
 */
public abstract class OrderUtil {

    private static ReentrantLock lock = new ReentrantLock();

    public static String nextOrderNo(String prefix, Order preOrder) {
        String orderNo = null;

        if (preOrder != null) {
            String preOrderOrderNo = preOrder.getOrderNo();
            if (StringUtils.isNotBlank(preOrderOrderNo)) {
                StringBuffer sb = new StringBuffer();
                String order = preOrderOrderNo.substring(preOrderOrderNo.length() - 6);
                int x = 999999;
                try {
                    x = Integer.parseInt(order) + 1;
                    sb.append(x);
                    while (sb.length() < 6) {
                        sb.insert(0, "0");
                    }
                } catch (Exception e) {
                }
                orderNo = preOrderOrderNo + sb.toString();

            }

        } else {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            String format = dateFormat.format(new Date());
            orderNo = prefix + format + "000001";
        }

        return orderNo;
    }

    public static String generateOrderNo(String prefix) {
        StringRedisTemplate redisTemplate = SpringUtils.getBean(StringRedisTemplate.class);
        ListOperations<String, String> listOperations = redisTemplate.opsForList();
        ValueOperations<String, String> valueOperations = redisTemplate.opsForValue();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        String today = dateFormat.format(new Date());

        String keyPrefix = "orderNoPool:";
        String key = keyPrefix + today;
        String indexKey = keyPrefix + "index";
        String pop = listOperations.leftPop(key);
        if (pop == null) {
            lock.lock();
            pop = listOperations.leftPop(key);
            if (pop == null) {
                try {
                    redisTemplate.delete(keyPrefix + "*");
                    String s = valueOperations.get(indexKey);
                    int index = s != null ? Integer.valueOf(s) : 0;

                    for (int i = 1; i <= 2000; i++) {
                        StringBuilder stringBuilder = new StringBuilder(String.valueOf(++index));

                        while (stringBuilder.length() < 6) {
                            stringBuilder.insert(0, "0");
                        }
                        listOperations.rightPush(key, prefix + today + stringBuilder.toString());

                    }
                    valueOperations.set(indexKey, String.valueOf(index));
                    pop = listOperations.leftPop(key);
                } finally {

                    lock.unlock();
                }

            }

        }

        return pop;

    }
}
