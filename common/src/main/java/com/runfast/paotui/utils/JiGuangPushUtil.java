package com.runfast.paotui.utils;

import cn.jpush.api.JPushClient;
import cn.jpush.api.push.PushResult;
import cn.jpush.api.push.model.Message;
import cn.jpush.api.push.model.Options;
import cn.jpush.api.push.model.Platform;
import cn.jpush.api.push.model.PushPayload;
import cn.jpush.api.push.model.audience.Audience;
import cn.jpush.api.push.model.notification.AndroidNotification;
import cn.jpush.api.push.model.notification.IosNotification;
import cn.jpush.api.push.model.notification.Notification;
import com.runfast.paotui.dao.model.Role;
import org.apache.commons.lang3.Validate;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

/**
 * @author: lijin

 */
public abstract class JiGuangPushUtil {
    private static final boolean isApnsProduction = true;

    private static final JPushClient jpushClientToCustomer = new JPushClient("f01abcf874d90c586fdefe46", "caa0055eac2098fb55eaf77b");
    private static final JPushClient jpushClientToDriver = new JPushClient("4ac1486ca9dd2717207a50ee", "165ccffde64bf5dd1ab7ff04");
    private static final JPushClient jpushClientToBusiness = new JPushClient("2ab3f7deb57ab5703eb6d366", "c94ba5202ef88b506181459f");
    private static final JPushClient jpushClientToBusiness1 = new JPushClient("f9ae4ce5095c7a1c3ec334d7", "1354bc477364bc5f5271dda0");

    private static PushPayload buildPushObject(long timeToLive, Collection<String> aliases, String alert, String title, String msgTitle, String msgContent, Map<String, String> params, String sound) {

        return PushPayload.newBuilder()
                //指定要推送的平台，all代表当前应用配置了的所有平台，也可以传android等具体平台
                .setPlatform(Platform.android_ios())
                //指定推送的接收对象，all代表所有人，也可以指定已经设置成功的tag或alias或该应应用客户端调用接口获取到的registration id
                .setAudience(Audience.alias(aliases))
                //jpush的通知，android的由jpush直接下发，iOS的由apns服务器下发，Winphone的由mpns下发
                .setNotification(Notification.newBuilder()
                        .setAlert(alert)
                        //指定当前推送的android通知
                        .addPlatformNotification(AndroidNotification.newBuilder()
                                //这里指定了，则会覆盖上级统一指定的 alert 信息；内容可以为空字符串，则表示不展示到通知栏。
//                                .setAlert(alert)
                                //如果指定了，则通知里原来展示 App名称的地方，将展示成这个字段
                                .setTitle(title)
                                //此字段为透传字段，不会显示在通知栏。用户可以通过此字段来做一些定制需求，如特定的key传要指定跳转的页面（value）
                                .addExtras(params)

                                .build())
                        //指定当前推送的iOS通知
                        .addPlatformNotification(IosNotification.newBuilder()
                                //传一个IosAlert对象，指定apns title、title、subtitle等
//                                .setAlert(notification_title)
                                //直接传alert
                                //此项是指定此推送的badge自动加1
                                .incrBadge(1)
                                //此字段的值default表示系统默认声音；传sound.caf表示此推送以项目里面打包的sound.caf声音来提醒，
                                // 如果系统没有此音频则以系统默认声音提醒；此字段如果传空字符串，iOS9及以上的系统是无声音提醒，以下的系统是默认声音
                                .setSound(sound)
                                //此字段为透传字段，不会显示在通知栏。用户可以通过此字段来做一些定制需求，如特定的key传要指定跳转的页面（value）
                                .addExtras(params)
                                //此项说明此推送是一个background推送，想了解background看：http://docs.jpush.io/client/ios_tutorials/#ios-7-background-remote-notification
                                //取消此注释，消息推送时ios将无法在锁屏情况接收
                                // .setContentAvailable(true)

                                .build())


                        .build())
                //Platform指定了哪些平台就会像指定平台中符合推送条件的设备进行推送。 jpush的自定义消息，
                // sdk默认不做任何处理，不会有通知提示。建议看文档http://docs.jpush.io/guideline/faq/的
                // [通知与自定义消息有什么区别？]了解通知和自定义消息的区别
                .setMessage(Message.newBuilder()

                        .setMsgContent(msgContent)

                        .setTitle(msgTitle)

                        .addExtras(params)

                        .build())

                .setOptions(Options.newBuilder()
                        //此字段的值是用来指定本推送要推送的apns环境，false表示开发，true表示生产；对android和自定义消息无意义
                        .setApnsProduction(isApnsProduction)
                        //此字段是给开发者自己给推送编号，方便推送者分辨推送记录,纯粹用来作为 API 调用标识，API 返回时被原样返回，以方便 API 调用方匹配请求与返回。值为 0 表示该 messageid 无 sendno，所以字段取值范围为非 0 的 int.
                        .setSendno(0)
                        //此字段的值是用来指定本推送的离线保存时长，如果不传此字段则默认保存一天，最多指定保留十天；
                        //离线消息保留时长(秒)
                        .setTimeToLive(timeToLive)

                        .build())

                .build();

    }

    public static PushResult push(Role.Predefine role, long timeToLive, Collection<String> aliases, String alert,String title, Map<String, String> params, String sound) {

        Validate.notNull(role);
        Validate.notNull(aliases);
        Validate.notBlank(title);

        // For push, all you need do is to build PushPayload object.
        PushPayload payload = buildPushObject(timeToLive, aliases, alert,title, null, title, params, sound);

        JPushClient jPushClient = null;
        switch (role) {
            case CUSTOMER:
                jPushClient = jpushClientToCustomer;
                break;
            case DRIVER:
                jPushClient = jpushClientToDriver;
                break;
            case BUSINESS:
                jPushClient = jpushClientToBusiness;
                break;
            default:
                throw new RuntimeException("不支持的推送对象类型： role = " + role.toString());
        }
        try {
            PushResult result = jPushClient.sendPush(payload);
            return result;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    public static PushResult push(Role.Predefine role, long timeToLive, String alias, String alert,String title, Map<String, String> params, String sound) {

        Validate.notNull(role);
        Validate.notBlank(alias);
        Validate.notBlank(title);

        // For push, all you need do is to build PushPayload object.
        PushPayload payload = buildPushObject(timeToLive, Collections.singletonList(alias), alert, title, null, title, params, sound);

        JPushClient jPushClient = null;
        switch (role) {
            case CUSTOMER:
                jPushClient = jpushClientToCustomer;
                break;
            case DRIVER:
                jPushClient = jpushClientToDriver;
                break;
            case BUSINESS:
                jPushClient = jpushClientToBusiness;
                break;
            default:
                throw new RuntimeException("不支持的推送对象类型： role = " + role.toString());
        }
        try {
            PushResult result = jPushClient.sendPush(payload);
            return result;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
