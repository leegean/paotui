package com.runfast.paotui.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author: lijin
 * @date: 2018年04月26日
 */
public class Amqp {

    @Bean
    public Queue orderQueue(){
        return new Queue("order");
    }

    @Bean
    public Exchange orderExchage(){

        return new FanoutExchange("order");
    }

    @Bean
    public Binding orderBinding(){
        return BindingBuilder.bind(orderQueue()).to(orderExchage()).with("order").noargs();
    }
}
