package com.runfast.paotui.config;

import com.runfast.common.web.StringToBaseEnumConverterFactory;
import com.runfast.common.web.interceptor.MaliciousRequestInterceptor;
import com.runfast.common.web.interceptor.UrlPermissionInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author: lijin
 * @date: 2018年04月26日
 */

public class Web implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new MaliciousRequestInterceptor()).addPathPatterns("/**");
        registry.addInterceptor(new UrlPermissionInterceptor());
//        registry.addInterceptor(new LogInterceptor());
    }

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverterFactory(new StringToBaseEnumConverterFactory());
    }

}
