package com.runfast.pay.alipay;

/* *
 *类名：AlipayConfig
 *功能：基础配置类
 *详细：设置帐户有关信息及返回路径
 *版本：3.3
 *日期：2012-08-10
 *说明：
 *以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 *该代码仅供学习和研究支付宝接口使用，只是提供一个参考。
	
 *提示：如何获取安全校验码和合作身份者ID
 *1.用您的签约支付宝账号登录支付宝网站(www.alipay.com)
 *2.点击“商家服务”(https://b.alipay.com/order/myOrder.htm)
 *3.点击“查询合作者身份(PID)”、“查询安全校验码(Key)”

 *安全校验码查看时，输入支付密码后，页面呈灰色的现象，怎么办？
 *解决方法：
 *1、检查浏览器配置，不让浏览器做弹框屏蔽设置
 *2、更换浏览器或电脑，重新登录查询。
 */

public class AlipayConfig {

    //↓↓↓↓↓↓↓↓↓↓请在这里配置您的基本信息↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
    // 合作身份者ID，以2088开头由16位纯数字组成的字符串
    public static String partner = "2088421970796704";

    // 交易安全检验码，由数字和字母组成的32位字符串
    // 如果签名方式设置为“MD5”时，请设置该参数
    public static String key = "elc0ukgb5c5ihjlmquhzj16iyqg63dfx";

    // 商户的私钥
    // 如果签名方式设置为“0001”时，请设置该参数
//    public static String private_key = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAN4b62V2OAi2eguV3qEDdtrubFO2sRhV5xxkSVCbL9AsrLaFUIQjtYG8AF+NWCF2qjoVrUEw6F3AhyvRk99Cw6fESDhpQli3BYQDHvAsBeAyUfTOTzbXu0M82vMQtccCJ7NqQxAhXgQIeJktQ7Z39eFQXptV2pT66zDojQjUKidtAgMBAAECgYBih74wX3ZSHykbFJwTj6bdTmGN7YC4i/sfdDm14XIfhoID4jYquHCfodgEi35qMesoI0+dmOWsGWmdfbZ8l3CVmxsuppdQNSiZ8bFITYy6aQWPZpkMpQrJ8eXxLgPEkeR45LPSQN1MAe9zmY238U9s8J9FB0Ir89tQWdAE7v/SIQJBAP3AAQOosDEBz1/9rSBUa1zsJI2dD41wNwsfSVwpVuXTmYSe/O23OhSvQJHa6oeKYglkYoQi8hgxLAyGM0Ld6MUCQQDgFBe3izXa6wOpzWzvPhsQhH8lBDoG/5FTgLpAQ8C6LtRnMpUTX/NM7bUyLokdUwZTWOIgDHB98j1hX2tEs56JAkEAgYly+xsyVtJFuwsVRlvSMy2Zu3FibyHJ97ORD6tK4SiURk+jbvcKf2S34b2P3IGYapMEd9AfJppzkjEwGgKOwQJAMgyK15Yia0k+wjh98lne1auicnQlOdbvcMU51IzNiUhYbMxhe7hcsXwOkqzON1yJ9yfNKfddUORATXVwkiHEcQJBANxg07eSKHLufYufh2UtG1oYxuDHCpwfHWyxAerxw5vgSZTps7QcahDQnHRD98MLM+FwPcHp5mA1UTGCvJ/VPBA=";
    public static String private_key = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCE1TNdEcfZ8S7Oxq6AhreqXDJTBkN5vj3PoSqJvHDTB01YZvm7+2CZ6hlERfveJnDenXiw+lR+7HLsNn7tx315J+aXmEOukMB2ji34gn6Pfr0TG1fxrP1R5aqbIR8EUvnnDaYqTv/sUkJnw4zzuZt9l1oxRHm68yttOXeCbNPYIBw5rzpVQ2EXLDLhHZAMbPHb0A2ICEl74W2phd40r4HvgdFLSzfM5NijgV23ZxsO08yDXGfKtsO2M10aFRzL7g4bQ0npFO14ZKNyYrnyCNMSDD3KM79ba4gaiS5lcVAoHsuLlV7yiE9uU2f7PoPeOmQHrE9gwRV3Zck1pz7jlWyjAgMBAAECggEAGap0RTxlSDgw2iVLnGvrWxB7bM1eBkz69n5mkLyHdE8J49Xp1SeqL7Mq9KUtGGyHDIySwGduIiQfxIJ3HgyYsutBw/LYyQcRGRc5zLWyLJnJgMKcyIarX8Knp1fL/XLm9H0PqUcgbhIq4I/JUWKAp9Cxzak0eCn6Lx2WeO38NNsgMvhP8OFUO53GOc1VKqH6oVN7ZFnF94ynI7EyUnxqcv+RGqqaWUkdhFsLd/MSpVJ53StZr4Ad0LPVLUs7dP0UzsakdoHRmTl7iJj3nZQmXhFCEZbhcF8+7abt4OROXEb18/+kKWcVYYBDIbIBorYLwwMe0X6N/T+f2eKyeyrxeQKBgQC7voB6hfZEoyt2/PELNSoxjorYMgdrV61fejg4c2IGrW+3xkxM6oP7nBOdbnbQU/vUwNd83Edw3nOTRBme8QfjSmT9OoX53yrZAIiUq5XOxQeihmSFEnUvzx4TZx7JljjezURFWddMivKOxgQaZ0PA1RVnE7vwp6bqZfmSPPX3fwKBgQC1IA6bZWwucmKqT8Y+sNOTdKt37p0NlS4eOseIH9HMii6kpNetXjwYAsmttMg2BTJurgP12etTEZ2Vsf0ss7qlP8lZvRaZ8CJil3HIy2BwZLga363E3PkHETfi+jwkGrdSRVml9TeD5bXdotvacIVV2TxH5EWAwnN2FIEMqUM83QKBgHZo4jNkhjwoBWPYiFafQ2taEavlcdR7yfcr6IWCEFNR0+I+iSUBzBhHCgADCaC/N1eqAL7ZV52ST6aUDuy17FmoNCWaYaGE5UtUENBjn+p3TidtR7y+NXbf1II8SaiuvFOZdvOehDHceLsfnLriYlMekY8fXSudMaMoic138VIVAoGAXiMxNFb8aQ+OdFJOCRtCe+sxi337JqoYdCG8vuU614IiR/RdHIBT327jf2LshBcjZR8Qc+E1S9FmA/2kRm2LXoPOlNOQQaXrWS65NtFtaD0Q6vbCjAUFcUTjtRa111YDXcEpnPXcSrNDWNRiTCAbTMlBaWQdM5TdCcwSXpBCoAkCgYEAoRfGhwpexP8xCUR87yCHCZCuNFYamVEyeJRGOgeebFeWg3LD8X0rFK/HGny0zC347AvwE2daZ/8TL2vmb+l1GZx7MDe4AAUZzNGXREYqodwrlMEOavlL+jcOadCsq1cADl/OcXDCmqXQ/NS5fkenhj39OD3UQjAPR39kSA80nfw=";
//    public static String private_key = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAJdyppLF8YkmoqpvnXOwBjNUr06UCZ8XrRs+GjzRz+1Fs2wKwq2HGYSpBjOiji/iSdwTqIfct0GMAbnN82yOFJhdI2MJfNcxB5se4AEggrkBySdj2c8Ww/jRiWANHVBbWSHttynOMb0XUT3PNnxFr4E1HK5cu8FhN5iD8aPWBkrfAgMBAAECgYB/INGs4RYYn+LqGNldtEPwMMUbu+67ql84QBA8P48/yEONjRbQHVkRX+UB0RF0JosDf0j4CxGVpxYHx1bwJ/AmjkxYlfluPAkhTcbrT9e6Wtu3TSUiLtz/UTZdrDsmZTmv3YxNYPeMjqaNVNKuiQCvV21EK1Ekzzq0NJ4frhIjyQJBANXfrCrih288CTz/zr7PAW7lb7q56+UyY+dlUN9b8u64/fXS3ENCJ+iiR6JnvbFTfIStY3nbKVJfpFqGCYDDrmMCQQC1Rzps9M0t3XhXCgfmxZJJo6l5EjDcEfEGIM6cAW4/2re4Ai7aHCRGXnUEU8QkUUII+RecowJ7BROBg18MGExVAkAX+D6Ce+rz44WhiYXxSp/9fsdb5RR0Foat22QJ9HMwoBkOYPiArTxtzSo8IGZZJmMxN4GLma1y5vczkgwkm0nDAkEAqkABIT8wS/kKAkTLcvLvFvX8PlbVSHoZMTYylNXLI3FYxhpWwkVX1Db2E2BQgANIz4CRqmx18e+siCBHbeaMGQJBAMCctJ2EBNYMUTKTlP5u7AsIuZdyMZziX4kVeuY2ryreVbq7DjIS8RZLEULkMuSwJuN+UKbMiGYZhobIy+YLRZI=";
//
    // 支付宝的公钥
    // 如果签名方式设置为“0001”时，请设置该参数
//    public static String ali_public_key = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDDI6d306Q8fIfCOaTXyiUeJHkrIvYISRcc73s3vF1ZT7XN8RNPwJxo8pWaJMmvyTn9N4HQ632qJBVHf8sxHi/fEsraprwCtzvzQETrNRwVxLO5jVmRGi60j8Ue1efIlzPXV9je9mkjzOmdssymZkh2QhUrCmZYI/FCEa3/cNMW0QIDAQAB";
public static String ali_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAozTTtsLQWYQqX4CChTGvRCa7sGmZZIPw/j0y91krG3UXN6BP6LgZE72hs7SsFeIXgKo3v/q4K/0yiRxfZiTOZUZv62V2lwF1zP/cWaZp8WEDB74rR678YhpPwi2zeIcDVhwuY1cDAWkjHEPPcfS/RDw7uAzeem4Pbn/vbkNhTbUeFFfGyg0cquu1mvdJVDKeUKTD0neERXOkgxDY3ntMS4r/B+V+0clhrKjZPKIS0ACMgab0G2G7cHUj07CiigVMaBn6ZZfEWP5c1+4wxRHnTSgRsqDm9Z2l+cgdS7JRYtrbI1xLfxAGFsxr03z6CO6Wkqw2KHJeLbE9zFK2DTrvGQIDAQAB";
//    public static String ali_public_key = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCXcqaSxfGJJqKqb51zsAYzVK9OlAmfF60bPho80c/tRbNsCsKthxmEqQYzoo4v4kncE6iH3LdBjAG5zfNsjhSYXSNjCXzXMQebHuABIIK5AcknY9nPFsP40YlgDR1QW1kh7bcpzjG9F1E9zzZ8Ra+BNRyuXLvBYTeYg/Gj1gZK3wIDAQAB";



    // App端支付需要的AppID
    public static String aliPay_AppID = "2018053160321416";
    //↑↑↑↑↑↑↑↑↑↑请在这里配置您的基本信息↑↑↑↑↑↑↑↑↑↑↑↑↑↑↑


    // 调试用，创建TXT日志文件夹路径
    public static String log_path = "D:\\";

    // 字符编码格式 目前支持  utf-8
    public static String input_charset = "utf-8";

    // 签名方式，选择项：0001(RSA)、MD5
    public static String sign_type = "RSA2";
    // 无线的产品中，签名方式为rsa时，sign_type需赋值为0001而不是RSA


}
