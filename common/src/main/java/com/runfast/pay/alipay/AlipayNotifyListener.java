package com.runfast.pay.alipay;

/**
 * @author: lijin
 * @date: 2018年04月04日
 */
public interface AlipayNotifyListener {

    void onTradeSuccess();
}
