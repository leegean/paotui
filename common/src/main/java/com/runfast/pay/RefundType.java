package com.runfast.pay;

/**
 * @author: lijin
 * @date: 2018年05月04日
 */
public enum RefundType {
    full_with_fee("全额退款，包含配送费"),
    full_without_fee("全额退款，不包含配送费"),
    part("部分退款");

    private String description;
    RefundType(String description) {
        this.description = description;
    }

}
