package com.runfast.pay.service;

import com.alipay.api.response.*;

import java.util.Map;

/**
 * @author: lijin
 * @date: 2018年03月30日
 */
public interface AlipayService {


    Map<String, String> prepay(String orderNo, int amountPayable, String subject, String productCode, String timeoutExpress, String notifyUrl) ;

    AlipayTradeQueryResponse orderQuery(String orderNo);

    AlipayTradeFastpayRefundQueryResponse refundQuery(String refundNo);

    AlipayTradeRefundResponse applyRefund(String orderNo, String refundNo, Integer refundAmount);

    AlipayTradeCloseResponse closeOrder(String orderNo);

    AlipayTradeAppPayResponse realPay(String orderNo, int amountPayable, String subject, String productCode, String timeoutExpress, String notifyUrl);
}
