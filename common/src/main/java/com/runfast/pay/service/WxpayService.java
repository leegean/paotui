package com.runfast.pay.service;

import com.runfast.pay.Channel;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author: lijin
 * @date: 2018年04月02日
 */
@Service
public interface WxpayService {

    Map<String, String> applyRefund(Channel channel, String orderNo, String refundNo, Integer payAmount, Integer refundAmount, String notifyUrl);

    Map<String, String> orderQuery(Channel channel, String orderNo);

    Map<String, String> refundrQuery(Channel channel, String refundNo);

    Map<String, String> closeOrder(Channel channel, String orderNo);


    Map<String, String> prepay(Channel channel, String orderNo, String body, int payAmount, String clientIp, String notifyUrl, String openId);
}
