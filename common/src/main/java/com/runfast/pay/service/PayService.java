package com.runfast.pay.service;

import com.runfast.common.web.entity.Result;
import com.runfast.pay.Channel;
import com.runfast.waimai.dao.model.RunfastRefund;

import java.util.Map;

/**
 * @author: lijin
 * @date: 2018年03月30日
 */
public interface PayService {


    Result orderQuery(String orderNo);


    Result prepay(Channel channel, String orderNo, String clientIp);

    Result refund(String orderNo, String refundNo, Integer refundAmount, String description);


    Result refundQuery(String refundNo);

    String handleAlipayNotify(Map<String, String> notify);

    String handleWxpayNotify(String notify);


    String handleWxpayRefundNotify(String notify);
}
