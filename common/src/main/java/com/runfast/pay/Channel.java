package com.runfast.pay;

/**
 * @author: lijin
 * @date: 2018年03月30日
 */
public enum Channel {

    ALIPAY_APP(0, "支付宝app支付"),
    WXPAY_APP(1, "微信app支付"),
    BALANCE(2, "余额支付"),
    WXPAY_PUBLIC(3, "微信公众号支付"),
    WXPAY_MINI(4, "微信小程序支付");

    private final String description;

    public int getCode() {
        return code;
    }

    private final int code;

    private Channel(int code, String description) {
        this.code = code;
        this.description = description;
    }
}
