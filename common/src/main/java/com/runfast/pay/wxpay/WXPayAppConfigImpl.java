package com.runfast.pay.wxpay;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.util.ClassUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;

@Component("wxpayAppConfigImpl")
public class WXPayAppConfigImpl extends WXPayConfig {
    private byte[] certData;


    public void setKey(String key) {
        this.key = key;
    }

    @Value("${pay.account.app.key}")
    private String key;

    @Value("${pay.account.app.appId}")
    private String appId;

    @Value("${pay.account.app.MchId}")
    private String mchId;

    private WXPayAppConfigImpl() throws Exception {

        /*Resource resource = new DefaultResourceLoader().getResource("cert/apiclient_cert_app.p12");

        File file = resource.getFile();
        System.out.println(file.getAbsolutePath());
        InputStream certStream = resource.getInputStream();
        this.certData = new byte[(int) file.length()];
        certStream.read(certData);
        certStream.close();*/


        InputStream certStream = ClassUtils.class.getClassLoader().getResourceAsStream("cert/apiclient_cert.p12");
        byte[] buffer = new byte[1024] ;
        int len = 0;
        ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();
        while ( (len = certStream.read(buffer))>0){
            byteBuffer.write(buffer,0, len);
        }
        certData = byteBuffer.toByteArray();


    }


    public String getAppID() {
        return this.appId;
    }

    public String getMchID() {
        return this.mchId;
    }

    public String getKey() {
        return key;
    }

    public InputStream getCertStream() {
        ByteArrayInputStream certBis;
        certBis = new ByteArrayInputStream(this.certData);
        return certBis;
    }


    public int getHttpConnectTimeoutMs() {
        return 2000;
    }

    public int getHttpReadTimeoutMs() {
        return 10000;
    }

    IWXPayDomain getWXPayDomain() {
        return WXPayDomainSimpleImpl.instance();
    }

    public String getPrimaryDomain() {
        return "api.mch.weixin.qq.com";
    }

    public String getAlternateDomain() {
        return "api2.mch.weixin.qq.com";
    }

    @Override
    public int getReportWorkerNum() {
        return 1;
    }

    @Override
    public int getReportBatchSize() {
        return 2;
    }


}
