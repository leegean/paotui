package com.runfast.common.security.spring;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

/**
 * @author: lijin
 * @date: 2018年06月02日
 */
public class UserDataDetails<D> extends User {
    private D data;

    public UserDataDetails(String username, String password, Collection<? extends GrantedAuthority> authorities, D data) {
        super(username, password, authorities);
        this.data = data;
    }
    public UserDataDetails(String username, String password, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, authorities);
    }

    public UserDataDetails(String username, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
    }

    public D getData() {
        return data;
    }

    public void setData(D data) {
        this.data = data;
    }
}
