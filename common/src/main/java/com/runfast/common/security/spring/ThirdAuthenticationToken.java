package com.runfast.common.security.spring;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * @author: lijin
 * @date: 2018年06月02日
 */
public class ThirdAuthenticationToken extends UsernamePasswordAuthenticationToken {
    public ThirdAuthenticationToken(Object principal, Object credentials) {
        super(principal, credentials);
    }

    public ThirdAuthenticationToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities) {
        super(principal, credentials, authorities);
    }
}
