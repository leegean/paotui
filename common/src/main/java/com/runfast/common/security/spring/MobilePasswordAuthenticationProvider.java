package com.runfast.common.security.spring;

import com.runfast.common.dao.model.RunfastCuser;
import com.runfast.common.dao.model.RunfastCuserExample;
import com.runfast.common.service.RunfastCuserService;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.mapping.GrantedAuthoritiesMapper;
import org.springframework.security.core.authority.mapping.NullAuthoritiesMapper;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.util.Assert;

import java.util.Collections;
import java.util.List;

/**
 * @author: lijin
 * @date: 2018年06月02日
 */
public class MobilePasswordAuthenticationProvider implements AuthenticationProvider {
    private GrantedAuthoritiesMapper authoritiesMapper = new NullAuthoritiesMapper();

    private RunfastCuserService cuserService;

    public MobilePasswordAuthenticationProvider(RunfastCuserService cuserService) {
        this.cuserService = cuserService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        Assert.isInstanceOf(MobilePasswordAuthenticationToken.class, authentication,"not instance of MobileSmsCodeAuthenticationToken.class");
        String mobile = authentication.getPrincipal() == null ? "" : authentication.getName();

        String password = authentication.getCredentials() == null ? "" : authentication.getCredentials().toString();
        UserDataDetails<RunfastCuser> userDataDetails = cuserService.checkMobielPassword(mobile, password);



        return createSuccessAuthentication(userDataDetails, authentication, userDataDetails);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return MobilePasswordAuthenticationToken.class.isAssignableFrom(authentication);
    }

    protected Authentication createSuccessAuthentication(Object principal,
                                                         Authentication authentication, UserDetails user) {
        // Ensure we return the original credentials the user supplied,
        // so subsequent attempts are successful even with encoded passwords.
        // Also ensure we return the original getDetails(), so that future
        // authentication events after cache expiry contain the details
        MobilePasswordAuthenticationToken result = new MobilePasswordAuthenticationToken(
                principal, authentication.getCredentials(),
                authoritiesMapper.mapAuthorities(user.getAuthorities()));
        result.setDetails(authentication.getDetails());

        return result;
    }
}
