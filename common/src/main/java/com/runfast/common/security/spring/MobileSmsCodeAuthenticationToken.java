package com.runfast.common.security.spring;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * @author: lijin
 * @date: 2018年06月02日
 */
public class MobileSmsCodeAuthenticationToken extends UsernamePasswordAuthenticationToken {
    public MobileSmsCodeAuthenticationToken(Object principal, Object credentials) {
        super(principal, credentials);
    }

    public MobileSmsCodeAuthenticationToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities) {
        super(principal, credentials, authorities);
    }
}
