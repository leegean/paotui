package com.runfast.common.dao.model;

import java.io.Serializable;
import java.util.Date;

public class RunfastLoginRecord implements Serializable {
    private Integer id;

    private String alias;

    /**
     * 登陆时间
     */
    private Date loginTime;

    /**
     * 用户id或者商家id或者骑手id
     */
    private Integer accountId;

    /**
     * 0:用户id,1:商家id,2:骑手id
     */
    private Integer accountType;

    /**
     * 退出时间
     */
    private Date logoutTime;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public RunfastLoginRecord withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAlias() {
        return alias;
    }

    public RunfastLoginRecord withAlias(String alias) {
        this.setAlias(alias);
        return this;
    }

    public void setAlias(String alias) {
        this.alias = alias == null ? null : alias.trim();
    }

    public Date getLoginTime() {
        return loginTime;
    }

    public RunfastLoginRecord withLoginTime(Date loginTime) {
        this.setLoginTime(loginTime);
        return this;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public RunfastLoginRecord withAccountId(Integer accountId) {
        this.setAccountId(accountId);
        return this;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public Integer getAccountType() {
        return accountType;
    }

    public RunfastLoginRecord withAccountType(Integer accountType) {
        this.setAccountType(accountType);
        return this;
    }

    public void setAccountType(Integer accountType) {
        this.accountType = accountType;
    }

    public Date getLogoutTime() {
        return logoutTime;
    }

    public RunfastLoginRecord withLogoutTime(Date logoutTime) {
        this.setLogoutTime(logoutTime);
        return this;
    }

    public void setLogoutTime(Date logoutTime) {
        this.logoutTime = logoutTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", alias=").append(alias);
        sb.append(", loginTime=").append(loginTime);
        sb.append(", accountId=").append(accountId);
        sb.append(", accountType=").append(accountType);
        sb.append(", logoutTime=").append(logoutTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}