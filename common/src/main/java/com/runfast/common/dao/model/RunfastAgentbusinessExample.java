package com.runfast.common.dao.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RunfastAgentbusinessExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public RunfastAgentbusinessExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andAreasIsNull() {
            addCriterion("areas is null");
            return (Criteria) this;
        }

        public Criteria andAreasIsNotNull() {
            addCriterion("areas is not null");
            return (Criteria) this;
        }

        public Criteria andAreasEqualTo(String value) {
            addCriterion("areas =", value, "areas");
            return (Criteria) this;
        }

        public Criteria andAreasNotEqualTo(String value) {
            addCriterion("areas <>", value, "areas");
            return (Criteria) this;
        }

        public Criteria andAreasGreaterThan(String value) {
            addCriterion("areas >", value, "areas");
            return (Criteria) this;
        }

        public Criteria andAreasGreaterThanOrEqualTo(String value) {
            addCriterion("areas >=", value, "areas");
            return (Criteria) this;
        }

        public Criteria andAreasLessThan(String value) {
            addCriterion("areas <", value, "areas");
            return (Criteria) this;
        }

        public Criteria andAreasLessThanOrEqualTo(String value) {
            addCriterion("areas <=", value, "areas");
            return (Criteria) this;
        }

        public Criteria andAreasLike(String value) {
            addCriterion("areas like", value, "areas");
            return (Criteria) this;
        }

        public Criteria andAreasNotLike(String value) {
            addCriterion("areas not like", value, "areas");
            return (Criteria) this;
        }

        public Criteria andAreasIn(List<String> values) {
            addCriterion("areas in", values, "areas");
            return (Criteria) this;
        }

        public Criteria andAreasNotIn(List<String> values) {
            addCriterion("areas not in", values, "areas");
            return (Criteria) this;
        }

        public Criteria andAreasBetween(String value1, String value2) {
            addCriterion("areas between", value1, value2, "areas");
            return (Criteria) this;
        }

        public Criteria andAreasNotBetween(String value1, String value2) {
            addCriterion("areas not between", value1, value2, "areas");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("createTime is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("createTime is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("createTime =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("createTime <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("createTime >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("createTime >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("createTime <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("createTime <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("createTime in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("createTime not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("createTime between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("createTime not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andMobileIsNull() {
            addCriterion("mobile is null");
            return (Criteria) this;
        }

        public Criteria andMobileIsNotNull() {
            addCriterion("mobile is not null");
            return (Criteria) this;
        }

        public Criteria andMobileEqualTo(String value) {
            addCriterion("mobile =", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotEqualTo(String value) {
            addCriterion("mobile <>", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileGreaterThan(String value) {
            addCriterion("mobile >", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileGreaterThanOrEqualTo(String value) {
            addCriterion("mobile >=", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLessThan(String value) {
            addCriterion("mobile <", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLessThanOrEqualTo(String value) {
            addCriterion("mobile <=", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLike(String value) {
            addCriterion("mobile like", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotLike(String value) {
            addCriterion("mobile not like", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileIn(List<String> values) {
            addCriterion("mobile in", values, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotIn(List<String> values) {
            addCriterion("mobile not in", values, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileBetween(String value1, String value2) {
            addCriterion("mobile between", value1, value2, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotBetween(String value1, String value2) {
            addCriterion("mobile not between", value1, value2, "mobile");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andAgentIdIsNull() {
            addCriterion("agentId is null");
            return (Criteria) this;
        }

        public Criteria andAgentIdIsNotNull() {
            addCriterion("agentId is not null");
            return (Criteria) this;
        }

        public Criteria andAgentIdEqualTo(Integer value) {
            addCriterion("agentId =", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotEqualTo(Integer value) {
            addCriterion("agentId <>", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdGreaterThan(Integer value) {
            addCriterion("agentId >", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("agentId >=", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdLessThan(Integer value) {
            addCriterion("agentId <", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdLessThanOrEqualTo(Integer value) {
            addCriterion("agentId <=", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdIn(List<Integer> values) {
            addCriterion("agentId in", values, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotIn(List<Integer> values) {
            addCriterion("agentId not in", values, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdBetween(Integer value1, Integer value2) {
            addCriterion("agentId between", value1, value2, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotBetween(Integer value1, Integer value2) {
            addCriterion("agentId not between", value1, value2, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentNameIsNull() {
            addCriterion("agentName is null");
            return (Criteria) this;
        }

        public Criteria andAgentNameIsNotNull() {
            addCriterion("agentName is not null");
            return (Criteria) this;
        }

        public Criteria andAgentNameEqualTo(String value) {
            addCriterion("agentName =", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotEqualTo(String value) {
            addCriterion("agentName <>", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameGreaterThan(String value) {
            addCriterion("agentName >", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameGreaterThanOrEqualTo(String value) {
            addCriterion("agentName >=", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameLessThan(String value) {
            addCriterion("agentName <", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameLessThanOrEqualTo(String value) {
            addCriterion("agentName <=", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameLike(String value) {
            addCriterion("agentName like", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotLike(String value) {
            addCriterion("agentName not like", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameIn(List<String> values) {
            addCriterion("agentName in", values, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotIn(List<String> values) {
            addCriterion("agentName not in", values, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameBetween(String value1, String value2) {
            addCriterion("agentName between", value1, value2, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotBetween(String value1, String value2) {
            addCriterion("agentName not between", value1, value2, "agentName");
            return (Criteria) this;
        }

        public Criteria andCityIdIsNull() {
            addCriterion("cityId is null");
            return (Criteria) this;
        }

        public Criteria andCityIdIsNotNull() {
            addCriterion("cityId is not null");
            return (Criteria) this;
        }

        public Criteria andCityIdEqualTo(String value) {
            addCriterion("cityId =", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdNotEqualTo(String value) {
            addCriterion("cityId <>", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdGreaterThan(String value) {
            addCriterion("cityId >", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdGreaterThanOrEqualTo(String value) {
            addCriterion("cityId >=", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdLessThan(String value) {
            addCriterion("cityId <", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdLessThanOrEqualTo(String value) {
            addCriterion("cityId <=", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdLike(String value) {
            addCriterion("cityId like", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdNotLike(String value) {
            addCriterion("cityId not like", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdIn(List<String> values) {
            addCriterion("cityId in", values, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdNotIn(List<String> values) {
            addCriterion("cityId not in", values, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdBetween(String value1, String value2) {
            addCriterion("cityId between", value1, value2, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdNotBetween(String value1, String value2) {
            addCriterion("cityId not between", value1, value2, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityNameIsNull() {
            addCriterion("cityName is null");
            return (Criteria) this;
        }

        public Criteria andCityNameIsNotNull() {
            addCriterion("cityName is not null");
            return (Criteria) this;
        }

        public Criteria andCityNameEqualTo(String value) {
            addCriterion("cityName =", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameNotEqualTo(String value) {
            addCriterion("cityName <>", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameGreaterThan(String value) {
            addCriterion("cityName >", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameGreaterThanOrEqualTo(String value) {
            addCriterion("cityName >=", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameLessThan(String value) {
            addCriterion("cityName <", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameLessThanOrEqualTo(String value) {
            addCriterion("cityName <=", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameLike(String value) {
            addCriterion("cityName like", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameNotLike(String value) {
            addCriterion("cityName not like", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameIn(List<String> values) {
            addCriterion("cityName in", values, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameNotIn(List<String> values) {
            addCriterion("cityName not in", values, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameBetween(String value1, String value2) {
            addCriterion("cityName between", value1, value2, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameNotBetween(String value1, String value2) {
            addCriterion("cityName not between", value1, value2, "cityName");
            return (Criteria) this;
        }

        public Criteria andCountyIdIsNull() {
            addCriterion("countyId is null");
            return (Criteria) this;
        }

        public Criteria andCountyIdIsNotNull() {
            addCriterion("countyId is not null");
            return (Criteria) this;
        }

        public Criteria andCountyIdEqualTo(String value) {
            addCriterion("countyId =", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdNotEqualTo(String value) {
            addCriterion("countyId <>", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdGreaterThan(String value) {
            addCriterion("countyId >", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdGreaterThanOrEqualTo(String value) {
            addCriterion("countyId >=", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdLessThan(String value) {
            addCriterion("countyId <", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdLessThanOrEqualTo(String value) {
            addCriterion("countyId <=", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdLike(String value) {
            addCriterion("countyId like", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdNotLike(String value) {
            addCriterion("countyId not like", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdIn(List<String> values) {
            addCriterion("countyId in", values, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdNotIn(List<String> values) {
            addCriterion("countyId not in", values, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdBetween(String value1, String value2) {
            addCriterion("countyId between", value1, value2, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdNotBetween(String value1, String value2) {
            addCriterion("countyId not between", value1, value2, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyNameIsNull() {
            addCriterion("countyName is null");
            return (Criteria) this;
        }

        public Criteria andCountyNameIsNotNull() {
            addCriterion("countyName is not null");
            return (Criteria) this;
        }

        public Criteria andCountyNameEqualTo(String value) {
            addCriterion("countyName =", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameNotEqualTo(String value) {
            addCriterion("countyName <>", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameGreaterThan(String value) {
            addCriterion("countyName >", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameGreaterThanOrEqualTo(String value) {
            addCriterion("countyName >=", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameLessThan(String value) {
            addCriterion("countyName <", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameLessThanOrEqualTo(String value) {
            addCriterion("countyName <=", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameLike(String value) {
            addCriterion("countyName like", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameNotLike(String value) {
            addCriterion("countyName not like", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameIn(List<String> values) {
            addCriterion("countyName in", values, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameNotIn(List<String> values) {
            addCriterion("countyName not in", values, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameBetween(String value1, String value2) {
            addCriterion("countyName between", value1, value2, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameNotBetween(String value1, String value2) {
            addCriterion("countyName not between", value1, value2, "countyName");
            return (Criteria) this;
        }

        public Criteria andCoefficientIsNull() {
            addCriterion("coefficient is null");
            return (Criteria) this;
        }

        public Criteria andCoefficientIsNotNull() {
            addCriterion("coefficient is not null");
            return (Criteria) this;
        }

        public Criteria andCoefficientEqualTo(BigDecimal value) {
            addCriterion("coefficient =", value, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientNotEqualTo(BigDecimal value) {
            addCriterion("coefficient <>", value, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientGreaterThan(BigDecimal value) {
            addCriterion("coefficient >", value, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("coefficient >=", value, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientLessThan(BigDecimal value) {
            addCriterion("coefficient <", value, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientLessThanOrEqualTo(BigDecimal value) {
            addCriterion("coefficient <=", value, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientIn(List<BigDecimal> values) {
            addCriterion("coefficient in", values, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientNotIn(List<BigDecimal> values) {
            addCriterion("coefficient not in", values, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("coefficient between", value1, value2, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("coefficient not between", value1, value2, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCodeIsNull() {
            addCriterion("code is null");
            return (Criteria) this;
        }

        public Criteria andCodeIsNotNull() {
            addCriterion("code is not null");
            return (Criteria) this;
        }

        public Criteria andCodeEqualTo(String value) {
            addCriterion("code =", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeNotEqualTo(String value) {
            addCriterion("code <>", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeGreaterThan(String value) {
            addCriterion("code >", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeGreaterThanOrEqualTo(String value) {
            addCriterion("code >=", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeLessThan(String value) {
            addCriterion("code <", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeLessThanOrEqualTo(String value) {
            addCriterion("code <=", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeLike(String value) {
            addCriterion("code like", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeNotLike(String value) {
            addCriterion("code not like", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeIn(List<String> values) {
            addCriterion("code in", values, "code");
            return (Criteria) this;
        }

        public Criteria andCodeNotIn(List<String> values) {
            addCriterion("code not in", values, "code");
            return (Criteria) this;
        }

        public Criteria andCodeBetween(String value1, String value2) {
            addCriterion("code between", value1, value2, "code");
            return (Criteria) this;
        }

        public Criteria andCodeNotBetween(String value1, String value2) {
            addCriterion("code not between", value1, value2, "code");
            return (Criteria) this;
        }

        public Criteria andAreaIsNull() {
            addCriterion("area is null");
            return (Criteria) this;
        }

        public Criteria andAreaIsNotNull() {
            addCriterion("area is not null");
            return (Criteria) this;
        }

        public Criteria andAreaEqualTo(String value) {
            addCriterion("area =", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaNotEqualTo(String value) {
            addCriterion("area <>", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaGreaterThan(String value) {
            addCriterion("area >", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaGreaterThanOrEqualTo(String value) {
            addCriterion("area >=", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaLessThan(String value) {
            addCriterion("area <", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaLessThanOrEqualTo(String value) {
            addCriterion("area <=", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaLike(String value) {
            addCriterion("area like", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaNotLike(String value) {
            addCriterion("area not like", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaIn(List<String> values) {
            addCriterion("area in", values, "area");
            return (Criteria) this;
        }

        public Criteria andAreaNotIn(List<String> values) {
            addCriterion("area not in", values, "area");
            return (Criteria) this;
        }

        public Criteria andAreaBetween(String value1, String value2) {
            addCriterion("area between", value1, value2, "area");
            return (Criteria) this;
        }

        public Criteria andAreaNotBetween(String value1, String value2) {
            addCriterion("area not between", value1, value2, "area");
            return (Criteria) this;
        }

        public Criteria andAreaIdIsNull() {
            addCriterion("areaId is null");
            return (Criteria) this;
        }

        public Criteria andAreaIdIsNotNull() {
            addCriterion("areaId is not null");
            return (Criteria) this;
        }

        public Criteria andAreaIdEqualTo(String value) {
            addCriterion("areaId =", value, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdNotEqualTo(String value) {
            addCriterion("areaId <>", value, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdGreaterThan(String value) {
            addCriterion("areaId >", value, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdGreaterThanOrEqualTo(String value) {
            addCriterion("areaId >=", value, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdLessThan(String value) {
            addCriterion("areaId <", value, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdLessThanOrEqualTo(String value) {
            addCriterion("areaId <=", value, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdLike(String value) {
            addCriterion("areaId like", value, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdNotLike(String value) {
            addCriterion("areaId not like", value, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdIn(List<String> values) {
            addCriterion("areaId in", values, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdNotIn(List<String> values) {
            addCriterion("areaId not in", values, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdBetween(String value1, String value2) {
            addCriterion("areaId between", value1, value2, "areaId");
            return (Criteria) this;
        }

        public Criteria andAreaIdNotBetween(String value1, String value2) {
            addCriterion("areaId not between", value1, value2, "areaId");
            return (Criteria) this;
        }

        public Criteria andCoefficient2IsNull() {
            addCriterion("coefficient2 is null");
            return (Criteria) this;
        }

        public Criteria andCoefficient2IsNotNull() {
            addCriterion("coefficient2 is not null");
            return (Criteria) this;
        }

        public Criteria andCoefficient2EqualTo(BigDecimal value) {
            addCriterion("coefficient2 =", value, "coefficient2");
            return (Criteria) this;
        }

        public Criteria andCoefficient2NotEqualTo(BigDecimal value) {
            addCriterion("coefficient2 <>", value, "coefficient2");
            return (Criteria) this;
        }

        public Criteria andCoefficient2GreaterThan(BigDecimal value) {
            addCriterion("coefficient2 >", value, "coefficient2");
            return (Criteria) this;
        }

        public Criteria andCoefficient2GreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("coefficient2 >=", value, "coefficient2");
            return (Criteria) this;
        }

        public Criteria andCoefficient2LessThan(BigDecimal value) {
            addCriterion("coefficient2 <", value, "coefficient2");
            return (Criteria) this;
        }

        public Criteria andCoefficient2LessThanOrEqualTo(BigDecimal value) {
            addCriterion("coefficient2 <=", value, "coefficient2");
            return (Criteria) this;
        }

        public Criteria andCoefficient2In(List<BigDecimal> values) {
            addCriterion("coefficient2 in", values, "coefficient2");
            return (Criteria) this;
        }

        public Criteria andCoefficient2NotIn(List<BigDecimal> values) {
            addCriterion("coefficient2 not in", values, "coefficient2");
            return (Criteria) this;
        }

        public Criteria andCoefficient2Between(BigDecimal value1, BigDecimal value2) {
            addCriterion("coefficient2 between", value1, value2, "coefficient2");
            return (Criteria) this;
        }

        public Criteria andCoefficient2NotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("coefficient2 not between", value1, value2, "coefficient2");
            return (Criteria) this;
        }

        public Criteria andManagerCodeIsNull() {
            addCriterion("managerCode is null");
            return (Criteria) this;
        }

        public Criteria andManagerCodeIsNotNull() {
            addCriterion("managerCode is not null");
            return (Criteria) this;
        }

        public Criteria andManagerCodeEqualTo(String value) {
            addCriterion("managerCode =", value, "managerCode");
            return (Criteria) this;
        }

        public Criteria andManagerCodeNotEqualTo(String value) {
            addCriterion("managerCode <>", value, "managerCode");
            return (Criteria) this;
        }

        public Criteria andManagerCodeGreaterThan(String value) {
            addCriterion("managerCode >", value, "managerCode");
            return (Criteria) this;
        }

        public Criteria andManagerCodeGreaterThanOrEqualTo(String value) {
            addCriterion("managerCode >=", value, "managerCode");
            return (Criteria) this;
        }

        public Criteria andManagerCodeLessThan(String value) {
            addCriterion("managerCode <", value, "managerCode");
            return (Criteria) this;
        }

        public Criteria andManagerCodeLessThanOrEqualTo(String value) {
            addCriterion("managerCode <=", value, "managerCode");
            return (Criteria) this;
        }

        public Criteria andManagerCodeLike(String value) {
            addCriterion("managerCode like", value, "managerCode");
            return (Criteria) this;
        }

        public Criteria andManagerCodeNotLike(String value) {
            addCriterion("managerCode not like", value, "managerCode");
            return (Criteria) this;
        }

        public Criteria andManagerCodeIn(List<String> values) {
            addCriterion("managerCode in", values, "managerCode");
            return (Criteria) this;
        }

        public Criteria andManagerCodeNotIn(List<String> values) {
            addCriterion("managerCode not in", values, "managerCode");
            return (Criteria) this;
        }

        public Criteria andManagerCodeBetween(String value1, String value2) {
            addCriterion("managerCode between", value1, value2, "managerCode");
            return (Criteria) this;
        }

        public Criteria andManagerCodeNotBetween(String value1, String value2) {
            addCriterion("managerCode not between", value1, value2, "managerCode");
            return (Criteria) this;
        }

        public Criteria andManagerIdIsNull() {
            addCriterion("managerId is null");
            return (Criteria) this;
        }

        public Criteria andManagerIdIsNotNull() {
            addCriterion("managerId is not null");
            return (Criteria) this;
        }

        public Criteria andManagerIdEqualTo(Integer value) {
            addCriterion("managerId =", value, "managerId");
            return (Criteria) this;
        }

        public Criteria andManagerIdNotEqualTo(Integer value) {
            addCriterion("managerId <>", value, "managerId");
            return (Criteria) this;
        }

        public Criteria andManagerIdGreaterThan(Integer value) {
            addCriterion("managerId >", value, "managerId");
            return (Criteria) this;
        }

        public Criteria andManagerIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("managerId >=", value, "managerId");
            return (Criteria) this;
        }

        public Criteria andManagerIdLessThan(Integer value) {
            addCriterion("managerId <", value, "managerId");
            return (Criteria) this;
        }

        public Criteria andManagerIdLessThanOrEqualTo(Integer value) {
            addCriterion("managerId <=", value, "managerId");
            return (Criteria) this;
        }

        public Criteria andManagerIdIn(List<Integer> values) {
            addCriterion("managerId in", values, "managerId");
            return (Criteria) this;
        }

        public Criteria andManagerIdNotIn(List<Integer> values) {
            addCriterion("managerId not in", values, "managerId");
            return (Criteria) this;
        }

        public Criteria andManagerIdBetween(Integer value1, Integer value2) {
            addCriterion("managerId between", value1, value2, "managerId");
            return (Criteria) this;
        }

        public Criteria andManagerIdNotBetween(Integer value1, Integer value2) {
            addCriterion("managerId not between", value1, value2, "managerId");
            return (Criteria) this;
        }

        public Criteria andProvinceIsNull() {
            addCriterion("province is null");
            return (Criteria) this;
        }

        public Criteria andProvinceIsNotNull() {
            addCriterion("province is not null");
            return (Criteria) this;
        }

        public Criteria andProvinceEqualTo(String value) {
            addCriterion("province =", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotEqualTo(String value) {
            addCriterion("province <>", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceGreaterThan(String value) {
            addCriterion("province >", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceGreaterThanOrEqualTo(String value) {
            addCriterion("province >=", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceLessThan(String value) {
            addCriterion("province <", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceLessThanOrEqualTo(String value) {
            addCriterion("province <=", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceLike(String value) {
            addCriterion("province like", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotLike(String value) {
            addCriterion("province not like", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceIn(List<String> values) {
            addCriterion("province in", values, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotIn(List<String> values) {
            addCriterion("province not in", values, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceBetween(String value1, String value2) {
            addCriterion("province between", value1, value2, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotBetween(String value1, String value2) {
            addCriterion("province not between", value1, value2, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceIdIsNull() {
            addCriterion("provinceId is null");
            return (Criteria) this;
        }

        public Criteria andProvinceIdIsNotNull() {
            addCriterion("provinceId is not null");
            return (Criteria) this;
        }

        public Criteria andProvinceIdEqualTo(String value) {
            addCriterion("provinceId =", value, "provinceId");
            return (Criteria) this;
        }

        public Criteria andProvinceIdNotEqualTo(String value) {
            addCriterion("provinceId <>", value, "provinceId");
            return (Criteria) this;
        }

        public Criteria andProvinceIdGreaterThan(String value) {
            addCriterion("provinceId >", value, "provinceId");
            return (Criteria) this;
        }

        public Criteria andProvinceIdGreaterThanOrEqualTo(String value) {
            addCriterion("provinceId >=", value, "provinceId");
            return (Criteria) this;
        }

        public Criteria andProvinceIdLessThan(String value) {
            addCriterion("provinceId <", value, "provinceId");
            return (Criteria) this;
        }

        public Criteria andProvinceIdLessThanOrEqualTo(String value) {
            addCriterion("provinceId <=", value, "provinceId");
            return (Criteria) this;
        }

        public Criteria andProvinceIdLike(String value) {
            addCriterion("provinceId like", value, "provinceId");
            return (Criteria) this;
        }

        public Criteria andProvinceIdNotLike(String value) {
            addCriterion("provinceId not like", value, "provinceId");
            return (Criteria) this;
        }

        public Criteria andProvinceIdIn(List<String> values) {
            addCriterion("provinceId in", values, "provinceId");
            return (Criteria) this;
        }

        public Criteria andProvinceIdNotIn(List<String> values) {
            addCriterion("provinceId not in", values, "provinceId");
            return (Criteria) this;
        }

        public Criteria andProvinceIdBetween(String value1, String value2) {
            addCriterion("provinceId between", value1, value2, "provinceId");
            return (Criteria) this;
        }

        public Criteria andProvinceIdNotBetween(String value1, String value2) {
            addCriterion("provinceId not between", value1, value2, "provinceId");
            return (Criteria) this;
        }

        public Criteria andAidIsNull() {
            addCriterion("aid is null");
            return (Criteria) this;
        }

        public Criteria andAidIsNotNull() {
            addCriterion("aid is not null");
            return (Criteria) this;
        }

        public Criteria andAidEqualTo(Integer value) {
            addCriterion("aid =", value, "aid");
            return (Criteria) this;
        }

        public Criteria andAidNotEqualTo(Integer value) {
            addCriterion("aid <>", value, "aid");
            return (Criteria) this;
        }

        public Criteria andAidGreaterThan(Integer value) {
            addCriterion("aid >", value, "aid");
            return (Criteria) this;
        }

        public Criteria andAidGreaterThanOrEqualTo(Integer value) {
            addCriterion("aid >=", value, "aid");
            return (Criteria) this;
        }

        public Criteria andAidLessThan(Integer value) {
            addCriterion("aid <", value, "aid");
            return (Criteria) this;
        }

        public Criteria andAidLessThanOrEqualTo(Integer value) {
            addCriterion("aid <=", value, "aid");
            return (Criteria) this;
        }

        public Criteria andAidIn(List<Integer> values) {
            addCriterion("aid in", values, "aid");
            return (Criteria) this;
        }

        public Criteria andAidNotIn(List<Integer> values) {
            addCriterion("aid not in", values, "aid");
            return (Criteria) this;
        }

        public Criteria andAidBetween(Integer value1, Integer value2) {
            addCriterion("aid between", value1, value2, "aid");
            return (Criteria) this;
        }

        public Criteria andAidNotBetween(Integer value1, Integer value2) {
            addCriterion("aid not between", value1, value2, "aid");
            return (Criteria) this;
        }

        public Criteria andCidIsNull() {
            addCriterion("cid is null");
            return (Criteria) this;
        }

        public Criteria andCidIsNotNull() {
            addCriterion("cid is not null");
            return (Criteria) this;
        }

        public Criteria andCidEqualTo(Integer value) {
            addCriterion("cid =", value, "cid");
            return (Criteria) this;
        }

        public Criteria andCidNotEqualTo(Integer value) {
            addCriterion("cid <>", value, "cid");
            return (Criteria) this;
        }

        public Criteria andCidGreaterThan(Integer value) {
            addCriterion("cid >", value, "cid");
            return (Criteria) this;
        }

        public Criteria andCidGreaterThanOrEqualTo(Integer value) {
            addCriterion("cid >=", value, "cid");
            return (Criteria) this;
        }

        public Criteria andCidLessThan(Integer value) {
            addCriterion("cid <", value, "cid");
            return (Criteria) this;
        }

        public Criteria andCidLessThanOrEqualTo(Integer value) {
            addCriterion("cid <=", value, "cid");
            return (Criteria) this;
        }

        public Criteria andCidIn(List<Integer> values) {
            addCriterion("cid in", values, "cid");
            return (Criteria) this;
        }

        public Criteria andCidNotIn(List<Integer> values) {
            addCriterion("cid not in", values, "cid");
            return (Criteria) this;
        }

        public Criteria andCidBetween(Integer value1, Integer value2) {
            addCriterion("cid between", value1, value2, "cid");
            return (Criteria) this;
        }

        public Criteria andCidNotBetween(Integer value1, Integer value2) {
            addCriterion("cid not between", value1, value2, "cid");
            return (Criteria) this;
        }

        public Criteria andManagerNameIsNull() {
            addCriterion("managerName is null");
            return (Criteria) this;
        }

        public Criteria andManagerNameIsNotNull() {
            addCriterion("managerName is not null");
            return (Criteria) this;
        }

        public Criteria andManagerNameEqualTo(String value) {
            addCriterion("managerName =", value, "managerName");
            return (Criteria) this;
        }

        public Criteria andManagerNameNotEqualTo(String value) {
            addCriterion("managerName <>", value, "managerName");
            return (Criteria) this;
        }

        public Criteria andManagerNameGreaterThan(String value) {
            addCriterion("managerName >", value, "managerName");
            return (Criteria) this;
        }

        public Criteria andManagerNameGreaterThanOrEqualTo(String value) {
            addCriterion("managerName >=", value, "managerName");
            return (Criteria) this;
        }

        public Criteria andManagerNameLessThan(String value) {
            addCriterion("managerName <", value, "managerName");
            return (Criteria) this;
        }

        public Criteria andManagerNameLessThanOrEqualTo(String value) {
            addCriterion("managerName <=", value, "managerName");
            return (Criteria) this;
        }

        public Criteria andManagerNameLike(String value) {
            addCriterion("managerName like", value, "managerName");
            return (Criteria) this;
        }

        public Criteria andManagerNameNotLike(String value) {
            addCriterion("managerName not like", value, "managerName");
            return (Criteria) this;
        }

        public Criteria andManagerNameIn(List<String> values) {
            addCriterion("managerName in", values, "managerName");
            return (Criteria) this;
        }

        public Criteria andManagerNameNotIn(List<String> values) {
            addCriterion("managerName not in", values, "managerName");
            return (Criteria) this;
        }

        public Criteria andManagerNameBetween(String value1, String value2) {
            addCriterion("managerName between", value1, value2, "managerName");
            return (Criteria) this;
        }

        public Criteria andManagerNameNotBetween(String value1, String value2) {
            addCriterion("managerName not between", value1, value2, "managerName");
            return (Criteria) this;
        }

        public Criteria andPidIsNull() {
            addCriterion("pid is null");
            return (Criteria) this;
        }

        public Criteria andPidIsNotNull() {
            addCriterion("pid is not null");
            return (Criteria) this;
        }

        public Criteria andPidEqualTo(Integer value) {
            addCriterion("pid =", value, "pid");
            return (Criteria) this;
        }

        public Criteria andPidNotEqualTo(Integer value) {
            addCriterion("pid <>", value, "pid");
            return (Criteria) this;
        }

        public Criteria andPidGreaterThan(Integer value) {
            addCriterion("pid >", value, "pid");
            return (Criteria) this;
        }

        public Criteria andPidGreaterThanOrEqualTo(Integer value) {
            addCriterion("pid >=", value, "pid");
            return (Criteria) this;
        }

        public Criteria andPidLessThan(Integer value) {
            addCriterion("pid <", value, "pid");
            return (Criteria) this;
        }

        public Criteria andPidLessThanOrEqualTo(Integer value) {
            addCriterion("pid <=", value, "pid");
            return (Criteria) this;
        }

        public Criteria andPidIn(List<Integer> values) {
            addCriterion("pid in", values, "pid");
            return (Criteria) this;
        }

        public Criteria andPidNotIn(List<Integer> values) {
            addCriterion("pid not in", values, "pid");
            return (Criteria) this;
        }

        public Criteria andPidBetween(Integer value1, Integer value2) {
            addCriterion("pid between", value1, value2, "pid");
            return (Criteria) this;
        }

        public Criteria andPidNotBetween(Integer value1, Integer value2) {
            addCriterion("pid not between", value1, value2, "pid");
            return (Criteria) this;
        }

        public Criteria andTidIsNull() {
            addCriterion("tid is null");
            return (Criteria) this;
        }

        public Criteria andTidIsNotNull() {
            addCriterion("tid is not null");
            return (Criteria) this;
        }

        public Criteria andTidEqualTo(Integer value) {
            addCriterion("tid =", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotEqualTo(Integer value) {
            addCriterion("tid <>", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidGreaterThan(Integer value) {
            addCriterion("tid >", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidGreaterThanOrEqualTo(Integer value) {
            addCriterion("tid >=", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidLessThan(Integer value) {
            addCriterion("tid <", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidLessThanOrEqualTo(Integer value) {
            addCriterion("tid <=", value, "tid");
            return (Criteria) this;
        }

        public Criteria andTidIn(List<Integer> values) {
            addCriterion("tid in", values, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotIn(List<Integer> values) {
            addCriterion("tid not in", values, "tid");
            return (Criteria) this;
        }

        public Criteria andTidBetween(Integer value1, Integer value2) {
            addCriterion("tid between", value1, value2, "tid");
            return (Criteria) this;
        }

        public Criteria andTidNotBetween(Integer value1, Integer value2) {
            addCriterion("tid not between", value1, value2, "tid");
            return (Criteria) this;
        }

        public Criteria andIshaveupIsNull() {
            addCriterion("ishaveup is null");
            return (Criteria) this;
        }

        public Criteria andIshaveupIsNotNull() {
            addCriterion("ishaveup is not null");
            return (Criteria) this;
        }

        public Criteria andIshaveupEqualTo(Integer value) {
            addCriterion("ishaveup =", value, "ishaveup");
            return (Criteria) this;
        }

        public Criteria andIshaveupNotEqualTo(Integer value) {
            addCriterion("ishaveup <>", value, "ishaveup");
            return (Criteria) this;
        }

        public Criteria andIshaveupGreaterThan(Integer value) {
            addCriterion("ishaveup >", value, "ishaveup");
            return (Criteria) this;
        }

        public Criteria andIshaveupGreaterThanOrEqualTo(Integer value) {
            addCriterion("ishaveup >=", value, "ishaveup");
            return (Criteria) this;
        }

        public Criteria andIshaveupLessThan(Integer value) {
            addCriterion("ishaveup <", value, "ishaveup");
            return (Criteria) this;
        }

        public Criteria andIshaveupLessThanOrEqualTo(Integer value) {
            addCriterion("ishaveup <=", value, "ishaveup");
            return (Criteria) this;
        }

        public Criteria andIshaveupIn(List<Integer> values) {
            addCriterion("ishaveup in", values, "ishaveup");
            return (Criteria) this;
        }

        public Criteria andIshaveupNotIn(List<Integer> values) {
            addCriterion("ishaveup not in", values, "ishaveup");
            return (Criteria) this;
        }

        public Criteria andIshaveupBetween(Integer value1, Integer value2) {
            addCriterion("ishaveup between", value1, value2, "ishaveup");
            return (Criteria) this;
        }

        public Criteria andIshaveupNotBetween(Integer value1, Integer value2) {
            addCriterion("ishaveup not between", value1, value2, "ishaveup");
            return (Criteria) this;
        }

        public Criteria andCompany_nameIsNull() {
            addCriterion("company_name is null");
            return (Criteria) this;
        }

        public Criteria andCompany_nameIsNotNull() {
            addCriterion("company_name is not null");
            return (Criteria) this;
        }

        public Criteria andCompany_nameEqualTo(String value) {
            addCriterion("company_name =", value, "company_name");
            return (Criteria) this;
        }

        public Criteria andCompany_nameNotEqualTo(String value) {
            addCriterion("company_name <>", value, "company_name");
            return (Criteria) this;
        }

        public Criteria andCompany_nameGreaterThan(String value) {
            addCriterion("company_name >", value, "company_name");
            return (Criteria) this;
        }

        public Criteria andCompany_nameGreaterThanOrEqualTo(String value) {
            addCriterion("company_name >=", value, "company_name");
            return (Criteria) this;
        }

        public Criteria andCompany_nameLessThan(String value) {
            addCriterion("company_name <", value, "company_name");
            return (Criteria) this;
        }

        public Criteria andCompany_nameLessThanOrEqualTo(String value) {
            addCriterion("company_name <=", value, "company_name");
            return (Criteria) this;
        }

        public Criteria andCompany_nameLike(String value) {
            addCriterion("company_name like", value, "company_name");
            return (Criteria) this;
        }

        public Criteria andCompany_nameNotLike(String value) {
            addCriterion("company_name not like", value, "company_name");
            return (Criteria) this;
        }

        public Criteria andCompany_nameIn(List<String> values) {
            addCriterion("company_name in", values, "company_name");
            return (Criteria) this;
        }

        public Criteria andCompany_nameNotIn(List<String> values) {
            addCriterion("company_name not in", values, "company_name");
            return (Criteria) this;
        }

        public Criteria andCompany_nameBetween(String value1, String value2) {
            addCriterion("company_name between", value1, value2, "company_name");
            return (Criteria) this;
        }

        public Criteria andCompany_nameNotBetween(String value1, String value2) {
            addCriterion("company_name not between", value1, value2, "company_name");
            return (Criteria) this;
        }

        public Criteria andLegal_nameIsNull() {
            addCriterion("legal_name is null");
            return (Criteria) this;
        }

        public Criteria andLegal_nameIsNotNull() {
            addCriterion("legal_name is not null");
            return (Criteria) this;
        }

        public Criteria andLegal_nameEqualTo(String value) {
            addCriterion("legal_name =", value, "legal_name");
            return (Criteria) this;
        }

        public Criteria andLegal_nameNotEqualTo(String value) {
            addCriterion("legal_name <>", value, "legal_name");
            return (Criteria) this;
        }

        public Criteria andLegal_nameGreaterThan(String value) {
            addCriterion("legal_name >", value, "legal_name");
            return (Criteria) this;
        }

        public Criteria andLegal_nameGreaterThanOrEqualTo(String value) {
            addCriterion("legal_name >=", value, "legal_name");
            return (Criteria) this;
        }

        public Criteria andLegal_nameLessThan(String value) {
            addCriterion("legal_name <", value, "legal_name");
            return (Criteria) this;
        }

        public Criteria andLegal_nameLessThanOrEqualTo(String value) {
            addCriterion("legal_name <=", value, "legal_name");
            return (Criteria) this;
        }

        public Criteria andLegal_nameLike(String value) {
            addCriterion("legal_name like", value, "legal_name");
            return (Criteria) this;
        }

        public Criteria andLegal_nameNotLike(String value) {
            addCriterion("legal_name not like", value, "legal_name");
            return (Criteria) this;
        }

        public Criteria andLegal_nameIn(List<String> values) {
            addCriterion("legal_name in", values, "legal_name");
            return (Criteria) this;
        }

        public Criteria andLegal_nameNotIn(List<String> values) {
            addCriterion("legal_name not in", values, "legal_name");
            return (Criteria) this;
        }

        public Criteria andLegal_nameBetween(String value1, String value2) {
            addCriterion("legal_name between", value1, value2, "legal_name");
            return (Criteria) this;
        }

        public Criteria andLegal_nameNotBetween(String value1, String value2) {
            addCriterion("legal_name not between", value1, value2, "legal_name");
            return (Criteria) this;
        }

        public Criteria andLegal_mobileIsNull() {
            addCriterion("legal_mobile is null");
            return (Criteria) this;
        }

        public Criteria andLegal_mobileIsNotNull() {
            addCriterion("legal_mobile is not null");
            return (Criteria) this;
        }

        public Criteria andLegal_mobileEqualTo(String value) {
            addCriterion("legal_mobile =", value, "legal_mobile");
            return (Criteria) this;
        }

        public Criteria andLegal_mobileNotEqualTo(String value) {
            addCriterion("legal_mobile <>", value, "legal_mobile");
            return (Criteria) this;
        }

        public Criteria andLegal_mobileGreaterThan(String value) {
            addCriterion("legal_mobile >", value, "legal_mobile");
            return (Criteria) this;
        }

        public Criteria andLegal_mobileGreaterThanOrEqualTo(String value) {
            addCriterion("legal_mobile >=", value, "legal_mobile");
            return (Criteria) this;
        }

        public Criteria andLegal_mobileLessThan(String value) {
            addCriterion("legal_mobile <", value, "legal_mobile");
            return (Criteria) this;
        }

        public Criteria andLegal_mobileLessThanOrEqualTo(String value) {
            addCriterion("legal_mobile <=", value, "legal_mobile");
            return (Criteria) this;
        }

        public Criteria andLegal_mobileLike(String value) {
            addCriterion("legal_mobile like", value, "legal_mobile");
            return (Criteria) this;
        }

        public Criteria andLegal_mobileNotLike(String value) {
            addCriterion("legal_mobile not like", value, "legal_mobile");
            return (Criteria) this;
        }

        public Criteria andLegal_mobileIn(List<String> values) {
            addCriterion("legal_mobile in", values, "legal_mobile");
            return (Criteria) this;
        }

        public Criteria andLegal_mobileNotIn(List<String> values) {
            addCriterion("legal_mobile not in", values, "legal_mobile");
            return (Criteria) this;
        }

        public Criteria andLegal_mobileBetween(String value1, String value2) {
            addCriterion("legal_mobile between", value1, value2, "legal_mobile");
            return (Criteria) this;
        }

        public Criteria andLegal_mobileNotBetween(String value1, String value2) {
            addCriterion("legal_mobile not between", value1, value2, "legal_mobile");
            return (Criteria) this;
        }

        public Criteria andLegal_idNumberIsNull() {
            addCriterion("legal_idNumber is null");
            return (Criteria) this;
        }

        public Criteria andLegal_idNumberIsNotNull() {
            addCriterion("legal_idNumber is not null");
            return (Criteria) this;
        }

        public Criteria andLegal_idNumberEqualTo(String value) {
            addCriterion("legal_idNumber =", value, "legal_idNumber");
            return (Criteria) this;
        }

        public Criteria andLegal_idNumberNotEqualTo(String value) {
            addCriterion("legal_idNumber <>", value, "legal_idNumber");
            return (Criteria) this;
        }

        public Criteria andLegal_idNumberGreaterThan(String value) {
            addCriterion("legal_idNumber >", value, "legal_idNumber");
            return (Criteria) this;
        }

        public Criteria andLegal_idNumberGreaterThanOrEqualTo(String value) {
            addCriterion("legal_idNumber >=", value, "legal_idNumber");
            return (Criteria) this;
        }

        public Criteria andLegal_idNumberLessThan(String value) {
            addCriterion("legal_idNumber <", value, "legal_idNumber");
            return (Criteria) this;
        }

        public Criteria andLegal_idNumberLessThanOrEqualTo(String value) {
            addCriterion("legal_idNumber <=", value, "legal_idNumber");
            return (Criteria) this;
        }

        public Criteria andLegal_idNumberLike(String value) {
            addCriterion("legal_idNumber like", value, "legal_idNumber");
            return (Criteria) this;
        }

        public Criteria andLegal_idNumberNotLike(String value) {
            addCriterion("legal_idNumber not like", value, "legal_idNumber");
            return (Criteria) this;
        }

        public Criteria andLegal_idNumberIn(List<String> values) {
            addCriterion("legal_idNumber in", values, "legal_idNumber");
            return (Criteria) this;
        }

        public Criteria andLegal_idNumberNotIn(List<String> values) {
            addCriterion("legal_idNumber not in", values, "legal_idNumber");
            return (Criteria) this;
        }

        public Criteria andLegal_idNumberBetween(String value1, String value2) {
            addCriterion("legal_idNumber between", value1, value2, "legal_idNumber");
            return (Criteria) this;
        }

        public Criteria andLegal_idNumberNotBetween(String value1, String value2) {
            addCriterion("legal_idNumber not between", value1, value2, "legal_idNumber");
            return (Criteria) this;
        }

        public Criteria andLegal_id_positive_loadIsNull() {
            addCriterion("legal_id_positive_load is null");
            return (Criteria) this;
        }

        public Criteria andLegal_id_positive_loadIsNotNull() {
            addCriterion("legal_id_positive_load is not null");
            return (Criteria) this;
        }

        public Criteria andLegal_id_positive_loadEqualTo(String value) {
            addCriterion("legal_id_positive_load =", value, "legal_id_positive_load");
            return (Criteria) this;
        }

        public Criteria andLegal_id_positive_loadNotEqualTo(String value) {
            addCriterion("legal_id_positive_load <>", value, "legal_id_positive_load");
            return (Criteria) this;
        }

        public Criteria andLegal_id_positive_loadGreaterThan(String value) {
            addCriterion("legal_id_positive_load >", value, "legal_id_positive_load");
            return (Criteria) this;
        }

        public Criteria andLegal_id_positive_loadGreaterThanOrEqualTo(String value) {
            addCriterion("legal_id_positive_load >=", value, "legal_id_positive_load");
            return (Criteria) this;
        }

        public Criteria andLegal_id_positive_loadLessThan(String value) {
            addCriterion("legal_id_positive_load <", value, "legal_id_positive_load");
            return (Criteria) this;
        }

        public Criteria andLegal_id_positive_loadLessThanOrEqualTo(String value) {
            addCriterion("legal_id_positive_load <=", value, "legal_id_positive_load");
            return (Criteria) this;
        }

        public Criteria andLegal_id_positive_loadLike(String value) {
            addCriterion("legal_id_positive_load like", value, "legal_id_positive_load");
            return (Criteria) this;
        }

        public Criteria andLegal_id_positive_loadNotLike(String value) {
            addCriterion("legal_id_positive_load not like", value, "legal_id_positive_load");
            return (Criteria) this;
        }

        public Criteria andLegal_id_positive_loadIn(List<String> values) {
            addCriterion("legal_id_positive_load in", values, "legal_id_positive_load");
            return (Criteria) this;
        }

        public Criteria andLegal_id_positive_loadNotIn(List<String> values) {
            addCriterion("legal_id_positive_load not in", values, "legal_id_positive_load");
            return (Criteria) this;
        }

        public Criteria andLegal_id_positive_loadBetween(String value1, String value2) {
            addCriterion("legal_id_positive_load between", value1, value2, "legal_id_positive_load");
            return (Criteria) this;
        }

        public Criteria andLegal_id_positive_loadNotBetween(String value1, String value2) {
            addCriterion("legal_id_positive_load not between", value1, value2, "legal_id_positive_load");
            return (Criteria) this;
        }

        public Criteria andLegal_id_negative_loadIsNull() {
            addCriterion("legal_id_negative_load is null");
            return (Criteria) this;
        }

        public Criteria andLegal_id_negative_loadIsNotNull() {
            addCriterion("legal_id_negative_load is not null");
            return (Criteria) this;
        }

        public Criteria andLegal_id_negative_loadEqualTo(String value) {
            addCriterion("legal_id_negative_load =", value, "legal_id_negative_load");
            return (Criteria) this;
        }

        public Criteria andLegal_id_negative_loadNotEqualTo(String value) {
            addCriterion("legal_id_negative_load <>", value, "legal_id_negative_load");
            return (Criteria) this;
        }

        public Criteria andLegal_id_negative_loadGreaterThan(String value) {
            addCriterion("legal_id_negative_load >", value, "legal_id_negative_load");
            return (Criteria) this;
        }

        public Criteria andLegal_id_negative_loadGreaterThanOrEqualTo(String value) {
            addCriterion("legal_id_negative_load >=", value, "legal_id_negative_load");
            return (Criteria) this;
        }

        public Criteria andLegal_id_negative_loadLessThan(String value) {
            addCriterion("legal_id_negative_load <", value, "legal_id_negative_load");
            return (Criteria) this;
        }

        public Criteria andLegal_id_negative_loadLessThanOrEqualTo(String value) {
            addCriterion("legal_id_negative_load <=", value, "legal_id_negative_load");
            return (Criteria) this;
        }

        public Criteria andLegal_id_negative_loadLike(String value) {
            addCriterion("legal_id_negative_load like", value, "legal_id_negative_load");
            return (Criteria) this;
        }

        public Criteria andLegal_id_negative_loadNotLike(String value) {
            addCriterion("legal_id_negative_load not like", value, "legal_id_negative_load");
            return (Criteria) this;
        }

        public Criteria andLegal_id_negative_loadIn(List<String> values) {
            addCriterion("legal_id_negative_load in", values, "legal_id_negative_load");
            return (Criteria) this;
        }

        public Criteria andLegal_id_negative_loadNotIn(List<String> values) {
            addCriterion("legal_id_negative_load not in", values, "legal_id_negative_load");
            return (Criteria) this;
        }

        public Criteria andLegal_id_negative_loadBetween(String value1, String value2) {
            addCriterion("legal_id_negative_load between", value1, value2, "legal_id_negative_load");
            return (Criteria) this;
        }

        public Criteria andLegal_id_negative_loadNotBetween(String value1, String value2) {
            addCriterion("legal_id_negative_load not between", value1, value2, "legal_id_negative_load");
            return (Criteria) this;
        }

        public Criteria andUnified_social_credit_code_loadIsNull() {
            addCriterion("unified_social_credit_code_load is null");
            return (Criteria) this;
        }

        public Criteria andUnified_social_credit_code_loadIsNotNull() {
            addCriterion("unified_social_credit_code_load is not null");
            return (Criteria) this;
        }

        public Criteria andUnified_social_credit_code_loadEqualTo(String value) {
            addCriterion("unified_social_credit_code_load =", value, "unified_social_credit_code_load");
            return (Criteria) this;
        }

        public Criteria andUnified_social_credit_code_loadNotEqualTo(String value) {
            addCriterion("unified_social_credit_code_load <>", value, "unified_social_credit_code_load");
            return (Criteria) this;
        }

        public Criteria andUnified_social_credit_code_loadGreaterThan(String value) {
            addCriterion("unified_social_credit_code_load >", value, "unified_social_credit_code_load");
            return (Criteria) this;
        }

        public Criteria andUnified_social_credit_code_loadGreaterThanOrEqualTo(String value) {
            addCriterion("unified_social_credit_code_load >=", value, "unified_social_credit_code_load");
            return (Criteria) this;
        }

        public Criteria andUnified_social_credit_code_loadLessThan(String value) {
            addCriterion("unified_social_credit_code_load <", value, "unified_social_credit_code_load");
            return (Criteria) this;
        }

        public Criteria andUnified_social_credit_code_loadLessThanOrEqualTo(String value) {
            addCriterion("unified_social_credit_code_load <=", value, "unified_social_credit_code_load");
            return (Criteria) this;
        }

        public Criteria andUnified_social_credit_code_loadLike(String value) {
            addCriterion("unified_social_credit_code_load like", value, "unified_social_credit_code_load");
            return (Criteria) this;
        }

        public Criteria andUnified_social_credit_code_loadNotLike(String value) {
            addCriterion("unified_social_credit_code_load not like", value, "unified_social_credit_code_load");
            return (Criteria) this;
        }

        public Criteria andUnified_social_credit_code_loadIn(List<String> values) {
            addCriterion("unified_social_credit_code_load in", values, "unified_social_credit_code_load");
            return (Criteria) this;
        }

        public Criteria andUnified_social_credit_code_loadNotIn(List<String> values) {
            addCriterion("unified_social_credit_code_load not in", values, "unified_social_credit_code_load");
            return (Criteria) this;
        }

        public Criteria andUnified_social_credit_code_loadBetween(String value1, String value2) {
            addCriterion("unified_social_credit_code_load between", value1, value2, "unified_social_credit_code_load");
            return (Criteria) this;
        }

        public Criteria andUnified_social_credit_code_loadNotBetween(String value1, String value2) {
            addCriterion("unified_social_credit_code_load not between", value1, value2, "unified_social_credit_code_load");
            return (Criteria) this;
        }

        public Criteria andAdmin_nameIsNull() {
            addCriterion("admin_name is null");
            return (Criteria) this;
        }

        public Criteria andAdmin_nameIsNotNull() {
            addCriterion("admin_name is not null");
            return (Criteria) this;
        }

        public Criteria andAdmin_nameEqualTo(String value) {
            addCriterion("admin_name =", value, "admin_name");
            return (Criteria) this;
        }

        public Criteria andAdmin_nameNotEqualTo(String value) {
            addCriterion("admin_name <>", value, "admin_name");
            return (Criteria) this;
        }

        public Criteria andAdmin_nameGreaterThan(String value) {
            addCriterion("admin_name >", value, "admin_name");
            return (Criteria) this;
        }

        public Criteria andAdmin_nameGreaterThanOrEqualTo(String value) {
            addCriterion("admin_name >=", value, "admin_name");
            return (Criteria) this;
        }

        public Criteria andAdmin_nameLessThan(String value) {
            addCriterion("admin_name <", value, "admin_name");
            return (Criteria) this;
        }

        public Criteria andAdmin_nameLessThanOrEqualTo(String value) {
            addCriterion("admin_name <=", value, "admin_name");
            return (Criteria) this;
        }

        public Criteria andAdmin_nameLike(String value) {
            addCriterion("admin_name like", value, "admin_name");
            return (Criteria) this;
        }

        public Criteria andAdmin_nameNotLike(String value) {
            addCriterion("admin_name not like", value, "admin_name");
            return (Criteria) this;
        }

        public Criteria andAdmin_nameIn(List<String> values) {
            addCriterion("admin_name in", values, "admin_name");
            return (Criteria) this;
        }

        public Criteria andAdmin_nameNotIn(List<String> values) {
            addCriterion("admin_name not in", values, "admin_name");
            return (Criteria) this;
        }

        public Criteria andAdmin_nameBetween(String value1, String value2) {
            addCriterion("admin_name between", value1, value2, "admin_name");
            return (Criteria) this;
        }

        public Criteria andAdmin_nameNotBetween(String value1, String value2) {
            addCriterion("admin_name not between", value1, value2, "admin_name");
            return (Criteria) this;
        }

        public Criteria andAdmin_mobileIsNull() {
            addCriterion("admin_mobile is null");
            return (Criteria) this;
        }

        public Criteria andAdmin_mobileIsNotNull() {
            addCriterion("admin_mobile is not null");
            return (Criteria) this;
        }

        public Criteria andAdmin_mobileEqualTo(String value) {
            addCriterion("admin_mobile =", value, "admin_mobile");
            return (Criteria) this;
        }

        public Criteria andAdmin_mobileNotEqualTo(String value) {
            addCriterion("admin_mobile <>", value, "admin_mobile");
            return (Criteria) this;
        }

        public Criteria andAdmin_mobileGreaterThan(String value) {
            addCriterion("admin_mobile >", value, "admin_mobile");
            return (Criteria) this;
        }

        public Criteria andAdmin_mobileGreaterThanOrEqualTo(String value) {
            addCriterion("admin_mobile >=", value, "admin_mobile");
            return (Criteria) this;
        }

        public Criteria andAdmin_mobileLessThan(String value) {
            addCriterion("admin_mobile <", value, "admin_mobile");
            return (Criteria) this;
        }

        public Criteria andAdmin_mobileLessThanOrEqualTo(String value) {
            addCriterion("admin_mobile <=", value, "admin_mobile");
            return (Criteria) this;
        }

        public Criteria andAdmin_mobileLike(String value) {
            addCriterion("admin_mobile like", value, "admin_mobile");
            return (Criteria) this;
        }

        public Criteria andAdmin_mobileNotLike(String value) {
            addCriterion("admin_mobile not like", value, "admin_mobile");
            return (Criteria) this;
        }

        public Criteria andAdmin_mobileIn(List<String> values) {
            addCriterion("admin_mobile in", values, "admin_mobile");
            return (Criteria) this;
        }

        public Criteria andAdmin_mobileNotIn(List<String> values) {
            addCriterion("admin_mobile not in", values, "admin_mobile");
            return (Criteria) this;
        }

        public Criteria andAdmin_mobileBetween(String value1, String value2) {
            addCriterion("admin_mobile between", value1, value2, "admin_mobile");
            return (Criteria) this;
        }

        public Criteria andAdmin_mobileNotBetween(String value1, String value2) {
            addCriterion("admin_mobile not between", value1, value2, "admin_mobile");
            return (Criteria) this;
        }

        public Criteria andAdmin_idNumberIsNull() {
            addCriterion("admin_idNumber is null");
            return (Criteria) this;
        }

        public Criteria andAdmin_idNumberIsNotNull() {
            addCriterion("admin_idNumber is not null");
            return (Criteria) this;
        }

        public Criteria andAdmin_idNumberEqualTo(String value) {
            addCriterion("admin_idNumber =", value, "admin_idNumber");
            return (Criteria) this;
        }

        public Criteria andAdmin_idNumberNotEqualTo(String value) {
            addCriterion("admin_idNumber <>", value, "admin_idNumber");
            return (Criteria) this;
        }

        public Criteria andAdmin_idNumberGreaterThan(String value) {
            addCriterion("admin_idNumber >", value, "admin_idNumber");
            return (Criteria) this;
        }

        public Criteria andAdmin_idNumberGreaterThanOrEqualTo(String value) {
            addCriterion("admin_idNumber >=", value, "admin_idNumber");
            return (Criteria) this;
        }

        public Criteria andAdmin_idNumberLessThan(String value) {
            addCriterion("admin_idNumber <", value, "admin_idNumber");
            return (Criteria) this;
        }

        public Criteria andAdmin_idNumberLessThanOrEqualTo(String value) {
            addCriterion("admin_idNumber <=", value, "admin_idNumber");
            return (Criteria) this;
        }

        public Criteria andAdmin_idNumberLike(String value) {
            addCriterion("admin_idNumber like", value, "admin_idNumber");
            return (Criteria) this;
        }

        public Criteria andAdmin_idNumberNotLike(String value) {
            addCriterion("admin_idNumber not like", value, "admin_idNumber");
            return (Criteria) this;
        }

        public Criteria andAdmin_idNumberIn(List<String> values) {
            addCriterion("admin_idNumber in", values, "admin_idNumber");
            return (Criteria) this;
        }

        public Criteria andAdmin_idNumberNotIn(List<String> values) {
            addCriterion("admin_idNumber not in", values, "admin_idNumber");
            return (Criteria) this;
        }

        public Criteria andAdmin_idNumberBetween(String value1, String value2) {
            addCriterion("admin_idNumber between", value1, value2, "admin_idNumber");
            return (Criteria) this;
        }

        public Criteria andAdmin_idNumberNotBetween(String value1, String value2) {
            addCriterion("admin_idNumber not between", value1, value2, "admin_idNumber");
            return (Criteria) this;
        }

        public Criteria andAdmin_id_positive_loadIsNull() {
            addCriterion("admin_id_positive_load is null");
            return (Criteria) this;
        }

        public Criteria andAdmin_id_positive_loadIsNotNull() {
            addCriterion("admin_id_positive_load is not null");
            return (Criteria) this;
        }

        public Criteria andAdmin_id_positive_loadEqualTo(String value) {
            addCriterion("admin_id_positive_load =", value, "admin_id_positive_load");
            return (Criteria) this;
        }

        public Criteria andAdmin_id_positive_loadNotEqualTo(String value) {
            addCriterion("admin_id_positive_load <>", value, "admin_id_positive_load");
            return (Criteria) this;
        }

        public Criteria andAdmin_id_positive_loadGreaterThan(String value) {
            addCriterion("admin_id_positive_load >", value, "admin_id_positive_load");
            return (Criteria) this;
        }

        public Criteria andAdmin_id_positive_loadGreaterThanOrEqualTo(String value) {
            addCriterion("admin_id_positive_load >=", value, "admin_id_positive_load");
            return (Criteria) this;
        }

        public Criteria andAdmin_id_positive_loadLessThan(String value) {
            addCriterion("admin_id_positive_load <", value, "admin_id_positive_load");
            return (Criteria) this;
        }

        public Criteria andAdmin_id_positive_loadLessThanOrEqualTo(String value) {
            addCriterion("admin_id_positive_load <=", value, "admin_id_positive_load");
            return (Criteria) this;
        }

        public Criteria andAdmin_id_positive_loadLike(String value) {
            addCriterion("admin_id_positive_load like", value, "admin_id_positive_load");
            return (Criteria) this;
        }

        public Criteria andAdmin_id_positive_loadNotLike(String value) {
            addCriterion("admin_id_positive_load not like", value, "admin_id_positive_load");
            return (Criteria) this;
        }

        public Criteria andAdmin_id_positive_loadIn(List<String> values) {
            addCriterion("admin_id_positive_load in", values, "admin_id_positive_load");
            return (Criteria) this;
        }

        public Criteria andAdmin_id_positive_loadNotIn(List<String> values) {
            addCriterion("admin_id_positive_load not in", values, "admin_id_positive_load");
            return (Criteria) this;
        }

        public Criteria andAdmin_id_positive_loadBetween(String value1, String value2) {
            addCriterion("admin_id_positive_load between", value1, value2, "admin_id_positive_load");
            return (Criteria) this;
        }

        public Criteria andAdmin_id_positive_loadNotBetween(String value1, String value2) {
            addCriterion("admin_id_positive_load not between", value1, value2, "admin_id_positive_load");
            return (Criteria) this;
        }

        public Criteria andAdmin_id_negative_loadIsNull() {
            addCriterion("admin_id_negative_load is null");
            return (Criteria) this;
        }

        public Criteria andAdmin_id_negative_loadIsNotNull() {
            addCriterion("admin_id_negative_load is not null");
            return (Criteria) this;
        }

        public Criteria andAdmin_id_negative_loadEqualTo(String value) {
            addCriterion("admin_id_negative_load =", value, "admin_id_negative_load");
            return (Criteria) this;
        }

        public Criteria andAdmin_id_negative_loadNotEqualTo(String value) {
            addCriterion("admin_id_negative_load <>", value, "admin_id_negative_load");
            return (Criteria) this;
        }

        public Criteria andAdmin_id_negative_loadGreaterThan(String value) {
            addCriterion("admin_id_negative_load >", value, "admin_id_negative_load");
            return (Criteria) this;
        }

        public Criteria andAdmin_id_negative_loadGreaterThanOrEqualTo(String value) {
            addCriterion("admin_id_negative_load >=", value, "admin_id_negative_load");
            return (Criteria) this;
        }

        public Criteria andAdmin_id_negative_loadLessThan(String value) {
            addCriterion("admin_id_negative_load <", value, "admin_id_negative_load");
            return (Criteria) this;
        }

        public Criteria andAdmin_id_negative_loadLessThanOrEqualTo(String value) {
            addCriterion("admin_id_negative_load <=", value, "admin_id_negative_load");
            return (Criteria) this;
        }

        public Criteria andAdmin_id_negative_loadLike(String value) {
            addCriterion("admin_id_negative_load like", value, "admin_id_negative_load");
            return (Criteria) this;
        }

        public Criteria andAdmin_id_negative_loadNotLike(String value) {
            addCriterion("admin_id_negative_load not like", value, "admin_id_negative_load");
            return (Criteria) this;
        }

        public Criteria andAdmin_id_negative_loadIn(List<String> values) {
            addCriterion("admin_id_negative_load in", values, "admin_id_negative_load");
            return (Criteria) this;
        }

        public Criteria andAdmin_id_negative_loadNotIn(List<String> values) {
            addCriterion("admin_id_negative_load not in", values, "admin_id_negative_load");
            return (Criteria) this;
        }

        public Criteria andAdmin_id_negative_loadBetween(String value1, String value2) {
            addCriterion("admin_id_negative_load between", value1, value2, "admin_id_negative_load");
            return (Criteria) this;
        }

        public Criteria andAdmin_id_negative_loadNotBetween(String value1, String value2) {
            addCriterion("admin_id_negative_load not between", value1, value2, "admin_id_negative_load");
            return (Criteria) this;
        }

        public Criteria andPublic_accounts_nameIsNull() {
            addCriterion("public_accounts_name is null");
            return (Criteria) this;
        }

        public Criteria andPublic_accounts_nameIsNotNull() {
            addCriterion("public_accounts_name is not null");
            return (Criteria) this;
        }

        public Criteria andPublic_accounts_nameEqualTo(String value) {
            addCriterion("public_accounts_name =", value, "public_accounts_name");
            return (Criteria) this;
        }

        public Criteria andPublic_accounts_nameNotEqualTo(String value) {
            addCriterion("public_accounts_name <>", value, "public_accounts_name");
            return (Criteria) this;
        }

        public Criteria andPublic_accounts_nameGreaterThan(String value) {
            addCriterion("public_accounts_name >", value, "public_accounts_name");
            return (Criteria) this;
        }

        public Criteria andPublic_accounts_nameGreaterThanOrEqualTo(String value) {
            addCriterion("public_accounts_name >=", value, "public_accounts_name");
            return (Criteria) this;
        }

        public Criteria andPublic_accounts_nameLessThan(String value) {
            addCriterion("public_accounts_name <", value, "public_accounts_name");
            return (Criteria) this;
        }

        public Criteria andPublic_accounts_nameLessThanOrEqualTo(String value) {
            addCriterion("public_accounts_name <=", value, "public_accounts_name");
            return (Criteria) this;
        }

        public Criteria andPublic_accounts_nameLike(String value) {
            addCriterion("public_accounts_name like", value, "public_accounts_name");
            return (Criteria) this;
        }

        public Criteria andPublic_accounts_nameNotLike(String value) {
            addCriterion("public_accounts_name not like", value, "public_accounts_name");
            return (Criteria) this;
        }

        public Criteria andPublic_accounts_nameIn(List<String> values) {
            addCriterion("public_accounts_name in", values, "public_accounts_name");
            return (Criteria) this;
        }

        public Criteria andPublic_accounts_nameNotIn(List<String> values) {
            addCriterion("public_accounts_name not in", values, "public_accounts_name");
            return (Criteria) this;
        }

        public Criteria andPublic_accounts_nameBetween(String value1, String value2) {
            addCriterion("public_accounts_name between", value1, value2, "public_accounts_name");
            return (Criteria) this;
        }

        public Criteria andPublic_accounts_nameNotBetween(String value1, String value2) {
            addCriterion("public_accounts_name not between", value1, value2, "public_accounts_name");
            return (Criteria) this;
        }

        public Criteria andBank_accountIsNull() {
            addCriterion("bank_account is null");
            return (Criteria) this;
        }

        public Criteria andBank_accountIsNotNull() {
            addCriterion("bank_account is not null");
            return (Criteria) this;
        }

        public Criteria andBank_accountEqualTo(String value) {
            addCriterion("bank_account =", value, "bank_account");
            return (Criteria) this;
        }

        public Criteria andBank_accountNotEqualTo(String value) {
            addCriterion("bank_account <>", value, "bank_account");
            return (Criteria) this;
        }

        public Criteria andBank_accountGreaterThan(String value) {
            addCriterion("bank_account >", value, "bank_account");
            return (Criteria) this;
        }

        public Criteria andBank_accountGreaterThanOrEqualTo(String value) {
            addCriterion("bank_account >=", value, "bank_account");
            return (Criteria) this;
        }

        public Criteria andBank_accountLessThan(String value) {
            addCriterion("bank_account <", value, "bank_account");
            return (Criteria) this;
        }

        public Criteria andBank_accountLessThanOrEqualTo(String value) {
            addCriterion("bank_account <=", value, "bank_account");
            return (Criteria) this;
        }

        public Criteria andBank_accountLike(String value) {
            addCriterion("bank_account like", value, "bank_account");
            return (Criteria) this;
        }

        public Criteria andBank_accountNotLike(String value) {
            addCriterion("bank_account not like", value, "bank_account");
            return (Criteria) this;
        }

        public Criteria andBank_accountIn(List<String> values) {
            addCriterion("bank_account in", values, "bank_account");
            return (Criteria) this;
        }

        public Criteria andBank_accountNotIn(List<String> values) {
            addCriterion("bank_account not in", values, "bank_account");
            return (Criteria) this;
        }

        public Criteria andBank_accountBetween(String value1, String value2) {
            addCriterion("bank_account between", value1, value2, "bank_account");
            return (Criteria) this;
        }

        public Criteria andBank_accountNotBetween(String value1, String value2) {
            addCriterion("bank_account not between", value1, value2, "bank_account");
            return (Criteria) this;
        }

        public Criteria andAccount_bankIsNull() {
            addCriterion("account_bank is null");
            return (Criteria) this;
        }

        public Criteria andAccount_bankIsNotNull() {
            addCriterion("account_bank is not null");
            return (Criteria) this;
        }

        public Criteria andAccount_bankEqualTo(String value) {
            addCriterion("account_bank =", value, "account_bank");
            return (Criteria) this;
        }

        public Criteria andAccount_bankNotEqualTo(String value) {
            addCriterion("account_bank <>", value, "account_bank");
            return (Criteria) this;
        }

        public Criteria andAccount_bankGreaterThan(String value) {
            addCriterion("account_bank >", value, "account_bank");
            return (Criteria) this;
        }

        public Criteria andAccount_bankGreaterThanOrEqualTo(String value) {
            addCriterion("account_bank >=", value, "account_bank");
            return (Criteria) this;
        }

        public Criteria andAccount_bankLessThan(String value) {
            addCriterion("account_bank <", value, "account_bank");
            return (Criteria) this;
        }

        public Criteria andAccount_bankLessThanOrEqualTo(String value) {
            addCriterion("account_bank <=", value, "account_bank");
            return (Criteria) this;
        }

        public Criteria andAccount_bankLike(String value) {
            addCriterion("account_bank like", value, "account_bank");
            return (Criteria) this;
        }

        public Criteria andAccount_bankNotLike(String value) {
            addCriterion("account_bank not like", value, "account_bank");
            return (Criteria) this;
        }

        public Criteria andAccount_bankIn(List<String> values) {
            addCriterion("account_bank in", values, "account_bank");
            return (Criteria) this;
        }

        public Criteria andAccount_bankNotIn(List<String> values) {
            addCriterion("account_bank not in", values, "account_bank");
            return (Criteria) this;
        }

        public Criteria andAccount_bankBetween(String value1, String value2) {
            addCriterion("account_bank between", value1, value2, "account_bank");
            return (Criteria) this;
        }

        public Criteria andAccount_bankNotBetween(String value1, String value2) {
            addCriterion("account_bank not between", value1, value2, "account_bank");
            return (Criteria) this;
        }

        public Criteria andAccount_bank_province_cityIsNull() {
            addCriterion("account_bank_province_city is null");
            return (Criteria) this;
        }

        public Criteria andAccount_bank_province_cityIsNotNull() {
            addCriterion("account_bank_province_city is not null");
            return (Criteria) this;
        }

        public Criteria andAccount_bank_province_cityEqualTo(String value) {
            addCriterion("account_bank_province_city =", value, "account_bank_province_city");
            return (Criteria) this;
        }

        public Criteria andAccount_bank_province_cityNotEqualTo(String value) {
            addCriterion("account_bank_province_city <>", value, "account_bank_province_city");
            return (Criteria) this;
        }

        public Criteria andAccount_bank_province_cityGreaterThan(String value) {
            addCriterion("account_bank_province_city >", value, "account_bank_province_city");
            return (Criteria) this;
        }

        public Criteria andAccount_bank_province_cityGreaterThanOrEqualTo(String value) {
            addCriterion("account_bank_province_city >=", value, "account_bank_province_city");
            return (Criteria) this;
        }

        public Criteria andAccount_bank_province_cityLessThan(String value) {
            addCriterion("account_bank_province_city <", value, "account_bank_province_city");
            return (Criteria) this;
        }

        public Criteria andAccount_bank_province_cityLessThanOrEqualTo(String value) {
            addCriterion("account_bank_province_city <=", value, "account_bank_province_city");
            return (Criteria) this;
        }

        public Criteria andAccount_bank_province_cityLike(String value) {
            addCriterion("account_bank_province_city like", value, "account_bank_province_city");
            return (Criteria) this;
        }

        public Criteria andAccount_bank_province_cityNotLike(String value) {
            addCriterion("account_bank_province_city not like", value, "account_bank_province_city");
            return (Criteria) this;
        }

        public Criteria andAccount_bank_province_cityIn(List<String> values) {
            addCriterion("account_bank_province_city in", values, "account_bank_province_city");
            return (Criteria) this;
        }

        public Criteria andAccount_bank_province_cityNotIn(List<String> values) {
            addCriterion("account_bank_province_city not in", values, "account_bank_province_city");
            return (Criteria) this;
        }

        public Criteria andAccount_bank_province_cityBetween(String value1, String value2) {
            addCriterion("account_bank_province_city between", value1, value2, "account_bank_province_city");
            return (Criteria) this;
        }

        public Criteria andAccount_bank_province_cityNotBetween(String value1, String value2) {
            addCriterion("account_bank_province_city not between", value1, value2, "account_bank_province_city");
            return (Criteria) this;
        }

        public Criteria andBranch_nameIsNull() {
            addCriterion("branch_name is null");
            return (Criteria) this;
        }

        public Criteria andBranch_nameIsNotNull() {
            addCriterion("branch_name is not null");
            return (Criteria) this;
        }

        public Criteria andBranch_nameEqualTo(String value) {
            addCriterion("branch_name =", value, "branch_name");
            return (Criteria) this;
        }

        public Criteria andBranch_nameNotEqualTo(String value) {
            addCriterion("branch_name <>", value, "branch_name");
            return (Criteria) this;
        }

        public Criteria andBranch_nameGreaterThan(String value) {
            addCriterion("branch_name >", value, "branch_name");
            return (Criteria) this;
        }

        public Criteria andBranch_nameGreaterThanOrEqualTo(String value) {
            addCriterion("branch_name >=", value, "branch_name");
            return (Criteria) this;
        }

        public Criteria andBranch_nameLessThan(String value) {
            addCriterion("branch_name <", value, "branch_name");
            return (Criteria) this;
        }

        public Criteria andBranch_nameLessThanOrEqualTo(String value) {
            addCriterion("branch_name <=", value, "branch_name");
            return (Criteria) this;
        }

        public Criteria andBranch_nameLike(String value) {
            addCriterion("branch_name like", value, "branch_name");
            return (Criteria) this;
        }

        public Criteria andBranch_nameNotLike(String value) {
            addCriterion("branch_name not like", value, "branch_name");
            return (Criteria) this;
        }

        public Criteria andBranch_nameIn(List<String> values) {
            addCriterion("branch_name in", values, "branch_name");
            return (Criteria) this;
        }

        public Criteria andBranch_nameNotIn(List<String> values) {
            addCriterion("branch_name not in", values, "branch_name");
            return (Criteria) this;
        }

        public Criteria andBranch_nameBetween(String value1, String value2) {
            addCriterion("branch_name between", value1, value2, "branch_name");
            return (Criteria) this;
        }

        public Criteria andBranch_nameNotBetween(String value1, String value2) {
            addCriterion("branch_name not between", value1, value2, "branch_name");
            return (Criteria) this;
        }

        public Criteria andAccount_bank_link_bank_noIsNull() {
            addCriterion("account_bank_link_bank_no is null");
            return (Criteria) this;
        }

        public Criteria andAccount_bank_link_bank_noIsNotNull() {
            addCriterion("account_bank_link_bank_no is not null");
            return (Criteria) this;
        }

        public Criteria andAccount_bank_link_bank_noEqualTo(Integer value) {
            addCriterion("account_bank_link_bank_no =", value, "account_bank_link_bank_no");
            return (Criteria) this;
        }

        public Criteria andAccount_bank_link_bank_noNotEqualTo(Integer value) {
            addCriterion("account_bank_link_bank_no <>", value, "account_bank_link_bank_no");
            return (Criteria) this;
        }

        public Criteria andAccount_bank_link_bank_noGreaterThan(Integer value) {
            addCriterion("account_bank_link_bank_no >", value, "account_bank_link_bank_no");
            return (Criteria) this;
        }

        public Criteria andAccount_bank_link_bank_noGreaterThanOrEqualTo(Integer value) {
            addCriterion("account_bank_link_bank_no >=", value, "account_bank_link_bank_no");
            return (Criteria) this;
        }

        public Criteria andAccount_bank_link_bank_noLessThan(Integer value) {
            addCriterion("account_bank_link_bank_no <", value, "account_bank_link_bank_no");
            return (Criteria) this;
        }

        public Criteria andAccount_bank_link_bank_noLessThanOrEqualTo(Integer value) {
            addCriterion("account_bank_link_bank_no <=", value, "account_bank_link_bank_no");
            return (Criteria) this;
        }

        public Criteria andAccount_bank_link_bank_noIn(List<Integer> values) {
            addCriterion("account_bank_link_bank_no in", values, "account_bank_link_bank_no");
            return (Criteria) this;
        }

        public Criteria andAccount_bank_link_bank_noNotIn(List<Integer> values) {
            addCriterion("account_bank_link_bank_no not in", values, "account_bank_link_bank_no");
            return (Criteria) this;
        }

        public Criteria andAccount_bank_link_bank_noBetween(Integer value1, Integer value2) {
            addCriterion("account_bank_link_bank_no between", value1, value2, "account_bank_link_bank_no");
            return (Criteria) this;
        }

        public Criteria andAccount_bank_link_bank_noNotBetween(Integer value1, Integer value2) {
            addCriterion("account_bank_link_bank_no not between", value1, value2, "account_bank_link_bank_no");
            return (Criteria) this;
        }

        public Criteria andPayable_depositIsNull() {
            addCriterion("payable_deposit is null");
            return (Criteria) this;
        }

        public Criteria andPayable_depositIsNotNull() {
            addCriterion("payable_deposit is not null");
            return (Criteria) this;
        }

        public Criteria andPayable_depositEqualTo(BigDecimal value) {
            addCriterion("payable_deposit =", value, "payable_deposit");
            return (Criteria) this;
        }

        public Criteria andPayable_depositNotEqualTo(BigDecimal value) {
            addCriterion("payable_deposit <>", value, "payable_deposit");
            return (Criteria) this;
        }

        public Criteria andPayable_depositGreaterThan(BigDecimal value) {
            addCriterion("payable_deposit >", value, "payable_deposit");
            return (Criteria) this;
        }

        public Criteria andPayable_depositGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("payable_deposit >=", value, "payable_deposit");
            return (Criteria) this;
        }

        public Criteria andPayable_depositLessThan(BigDecimal value) {
            addCriterion("payable_deposit <", value, "payable_deposit");
            return (Criteria) this;
        }

        public Criteria andPayable_depositLessThanOrEqualTo(BigDecimal value) {
            addCriterion("payable_deposit <=", value, "payable_deposit");
            return (Criteria) this;
        }

        public Criteria andPayable_depositIn(List<BigDecimal> values) {
            addCriterion("payable_deposit in", values, "payable_deposit");
            return (Criteria) this;
        }

        public Criteria andPayable_depositNotIn(List<BigDecimal> values) {
            addCriterion("payable_deposit not in", values, "payable_deposit");
            return (Criteria) this;
        }

        public Criteria andPayable_depositBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("payable_deposit between", value1, value2, "payable_deposit");
            return (Criteria) this;
        }

        public Criteria andPayable_depositNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("payable_deposit not between", value1, value2, "payable_deposit");
            return (Criteria) this;
        }

        public Criteria andDeposit_paidIsNull() {
            addCriterion("deposit_paid is null");
            return (Criteria) this;
        }

        public Criteria andDeposit_paidIsNotNull() {
            addCriterion("deposit_paid is not null");
            return (Criteria) this;
        }

        public Criteria andDeposit_paidEqualTo(BigDecimal value) {
            addCriterion("deposit_paid =", value, "deposit_paid");
            return (Criteria) this;
        }

        public Criteria andDeposit_paidNotEqualTo(BigDecimal value) {
            addCriterion("deposit_paid <>", value, "deposit_paid");
            return (Criteria) this;
        }

        public Criteria andDeposit_paidGreaterThan(BigDecimal value) {
            addCriterion("deposit_paid >", value, "deposit_paid");
            return (Criteria) this;
        }

        public Criteria andDeposit_paidGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("deposit_paid >=", value, "deposit_paid");
            return (Criteria) this;
        }

        public Criteria andDeposit_paidLessThan(BigDecimal value) {
            addCriterion("deposit_paid <", value, "deposit_paid");
            return (Criteria) this;
        }

        public Criteria andDeposit_paidLessThanOrEqualTo(BigDecimal value) {
            addCriterion("deposit_paid <=", value, "deposit_paid");
            return (Criteria) this;
        }

        public Criteria andDeposit_paidIn(List<BigDecimal> values) {
            addCriterion("deposit_paid in", values, "deposit_paid");
            return (Criteria) this;
        }

        public Criteria andDeposit_paidNotIn(List<BigDecimal> values) {
            addCriterion("deposit_paid not in", values, "deposit_paid");
            return (Criteria) this;
        }

        public Criteria andDeposit_paidBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("deposit_paid between", value1, value2, "deposit_paid");
            return (Criteria) this;
        }

        public Criteria andDeposit_paidNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("deposit_paid not between", value1, value2, "deposit_paid");
            return (Criteria) this;
        }

        public Criteria andContract_start_timeIsNull() {
            addCriterion("contract_start_time is null");
            return (Criteria) this;
        }

        public Criteria andContract_start_timeIsNotNull() {
            addCriterion("contract_start_time is not null");
            return (Criteria) this;
        }

        public Criteria andContract_start_timeEqualTo(Date value) {
            addCriterion("contract_start_time =", value, "contract_start_time");
            return (Criteria) this;
        }

        public Criteria andContract_start_timeNotEqualTo(Date value) {
            addCriterion("contract_start_time <>", value, "contract_start_time");
            return (Criteria) this;
        }

        public Criteria andContract_start_timeGreaterThan(Date value) {
            addCriterion("contract_start_time >", value, "contract_start_time");
            return (Criteria) this;
        }

        public Criteria andContract_start_timeGreaterThanOrEqualTo(Date value) {
            addCriterion("contract_start_time >=", value, "contract_start_time");
            return (Criteria) this;
        }

        public Criteria andContract_start_timeLessThan(Date value) {
            addCriterion("contract_start_time <", value, "contract_start_time");
            return (Criteria) this;
        }

        public Criteria andContract_start_timeLessThanOrEqualTo(Date value) {
            addCriterion("contract_start_time <=", value, "contract_start_time");
            return (Criteria) this;
        }

        public Criteria andContract_start_timeIn(List<Date> values) {
            addCriterion("contract_start_time in", values, "contract_start_time");
            return (Criteria) this;
        }

        public Criteria andContract_start_timeNotIn(List<Date> values) {
            addCriterion("contract_start_time not in", values, "contract_start_time");
            return (Criteria) this;
        }

        public Criteria andContract_start_timeBetween(Date value1, Date value2) {
            addCriterion("contract_start_time between", value1, value2, "contract_start_time");
            return (Criteria) this;
        }

        public Criteria andContract_start_timeNotBetween(Date value1, Date value2) {
            addCriterion("contract_start_time not between", value1, value2, "contract_start_time");
            return (Criteria) this;
        }

        public Criteria andContract_end_timeIsNull() {
            addCriterion("contract_end_time is null");
            return (Criteria) this;
        }

        public Criteria andContract_end_timeIsNotNull() {
            addCriterion("contract_end_time is not null");
            return (Criteria) this;
        }

        public Criteria andContract_end_timeEqualTo(Date value) {
            addCriterion("contract_end_time =", value, "contract_end_time");
            return (Criteria) this;
        }

        public Criteria andContract_end_timeNotEqualTo(Date value) {
            addCriterion("contract_end_time <>", value, "contract_end_time");
            return (Criteria) this;
        }

        public Criteria andContract_end_timeGreaterThan(Date value) {
            addCriterion("contract_end_time >", value, "contract_end_time");
            return (Criteria) this;
        }

        public Criteria andContract_end_timeGreaterThanOrEqualTo(Date value) {
            addCriterion("contract_end_time >=", value, "contract_end_time");
            return (Criteria) this;
        }

        public Criteria andContract_end_timeLessThan(Date value) {
            addCriterion("contract_end_time <", value, "contract_end_time");
            return (Criteria) this;
        }

        public Criteria andContract_end_timeLessThanOrEqualTo(Date value) {
            addCriterion("contract_end_time <=", value, "contract_end_time");
            return (Criteria) this;
        }

        public Criteria andContract_end_timeIn(List<Date> values) {
            addCriterion("contract_end_time in", values, "contract_end_time");
            return (Criteria) this;
        }

        public Criteria andContract_end_timeNotIn(List<Date> values) {
            addCriterion("contract_end_time not in", values, "contract_end_time");
            return (Criteria) this;
        }

        public Criteria andContract_end_timeBetween(Date value1, Date value2) {
            addCriterion("contract_end_time between", value1, value2, "contract_end_time");
            return (Criteria) this;
        }

        public Criteria andContract_end_timeNotBetween(Date value1, Date value2) {
            addCriterion("contract_end_time not between", value1, value2, "contract_end_time");
            return (Criteria) this;
        }

        public Criteria andLevelIsNull() {
            addCriterion("level is null");
            return (Criteria) this;
        }

        public Criteria andLevelIsNotNull() {
            addCriterion("level is not null");
            return (Criteria) this;
        }

        public Criteria andLevelEqualTo(Integer value) {
            addCriterion("level =", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelNotEqualTo(Integer value) {
            addCriterion("level <>", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelGreaterThan(Integer value) {
            addCriterion("level >", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelGreaterThanOrEqualTo(Integer value) {
            addCriterion("level >=", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelLessThan(Integer value) {
            addCriterion("level <", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelLessThanOrEqualTo(Integer value) {
            addCriterion("level <=", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelIn(List<Integer> values) {
            addCriterion("level in", values, "level");
            return (Criteria) this;
        }

        public Criteria andLevelNotIn(List<Integer> values) {
            addCriterion("level not in", values, "level");
            return (Criteria) this;
        }

        public Criteria andLevelBetween(Integer value1, Integer value2) {
            addCriterion("level between", value1, value2, "level");
            return (Criteria) this;
        }

        public Criteria andLevelNotBetween(Integer value1, Integer value2) {
            addCriterion("level not between", value1, value2, "level");
            return (Criteria) this;
        }

        public Criteria andOffice_addressIsNull() {
            addCriterion("office_address is null");
            return (Criteria) this;
        }

        public Criteria andOffice_addressIsNotNull() {
            addCriterion("office_address is not null");
            return (Criteria) this;
        }

        public Criteria andOffice_addressEqualTo(String value) {
            addCriterion("office_address =", value, "office_address");
            return (Criteria) this;
        }

        public Criteria andOffice_addressNotEqualTo(String value) {
            addCriterion("office_address <>", value, "office_address");
            return (Criteria) this;
        }

        public Criteria andOffice_addressGreaterThan(String value) {
            addCriterion("office_address >", value, "office_address");
            return (Criteria) this;
        }

        public Criteria andOffice_addressGreaterThanOrEqualTo(String value) {
            addCriterion("office_address >=", value, "office_address");
            return (Criteria) this;
        }

        public Criteria andOffice_addressLessThan(String value) {
            addCriterion("office_address <", value, "office_address");
            return (Criteria) this;
        }

        public Criteria andOffice_addressLessThanOrEqualTo(String value) {
            addCriterion("office_address <=", value, "office_address");
            return (Criteria) this;
        }

        public Criteria andOffice_addressLike(String value) {
            addCriterion("office_address like", value, "office_address");
            return (Criteria) this;
        }

        public Criteria andOffice_addressNotLike(String value) {
            addCriterion("office_address not like", value, "office_address");
            return (Criteria) this;
        }

        public Criteria andOffice_addressIn(List<String> values) {
            addCriterion("office_address in", values, "office_address");
            return (Criteria) this;
        }

        public Criteria andOffice_addressNotIn(List<String> values) {
            addCriterion("office_address not in", values, "office_address");
            return (Criteria) this;
        }

        public Criteria andOffice_addressBetween(String value1, String value2) {
            addCriterion("office_address between", value1, value2, "office_address");
            return (Criteria) this;
        }

        public Criteria andOffice_addressNotBetween(String value1, String value2) {
            addCriterion("office_address not between", value1, value2, "office_address");
            return (Criteria) this;
        }

        public Criteria andValidity_period_yearIsNull() {
            addCriterion("validity_period_year is null");
            return (Criteria) this;
        }

        public Criteria andValidity_period_yearIsNotNull() {
            addCriterion("validity_period_year is not null");
            return (Criteria) this;
        }

        public Criteria andValidity_period_yearEqualTo(Integer value) {
            addCriterion("validity_period_year =", value, "validity_period_year");
            return (Criteria) this;
        }

        public Criteria andValidity_period_yearNotEqualTo(Integer value) {
            addCriterion("validity_period_year <>", value, "validity_period_year");
            return (Criteria) this;
        }

        public Criteria andValidity_period_yearGreaterThan(Integer value) {
            addCriterion("validity_period_year >", value, "validity_period_year");
            return (Criteria) this;
        }

        public Criteria andValidity_period_yearGreaterThanOrEqualTo(Integer value) {
            addCriterion("validity_period_year >=", value, "validity_period_year");
            return (Criteria) this;
        }

        public Criteria andValidity_period_yearLessThan(Integer value) {
            addCriterion("validity_period_year <", value, "validity_period_year");
            return (Criteria) this;
        }

        public Criteria andValidity_period_yearLessThanOrEqualTo(Integer value) {
            addCriterion("validity_period_year <=", value, "validity_period_year");
            return (Criteria) this;
        }

        public Criteria andValidity_period_yearIn(List<Integer> values) {
            addCriterion("validity_period_year in", values, "validity_period_year");
            return (Criteria) this;
        }

        public Criteria andValidity_period_yearNotIn(List<Integer> values) {
            addCriterion("validity_period_year not in", values, "validity_period_year");
            return (Criteria) this;
        }

        public Criteria andValidity_period_yearBetween(Integer value1, Integer value2) {
            addCriterion("validity_period_year between", value1, value2, "validity_period_year");
            return (Criteria) this;
        }

        public Criteria andValidity_period_yearNotBetween(Integer value1, Integer value2) {
            addCriterion("validity_period_year not between", value1, value2, "validity_period_year");
            return (Criteria) this;
        }

        public Criteria andFlageIsNull() {
            addCriterion("flage is null");
            return (Criteria) this;
        }

        public Criteria andFlageIsNotNull() {
            addCriterion("flage is not null");
            return (Criteria) this;
        }

        public Criteria andFlageEqualTo(Integer value) {
            addCriterion("flage =", value, "flage");
            return (Criteria) this;
        }

        public Criteria andFlageNotEqualTo(Integer value) {
            addCriterion("flage <>", value, "flage");
            return (Criteria) this;
        }

        public Criteria andFlageGreaterThan(Integer value) {
            addCriterion("flage >", value, "flage");
            return (Criteria) this;
        }

        public Criteria andFlageGreaterThanOrEqualTo(Integer value) {
            addCriterion("flage >=", value, "flage");
            return (Criteria) this;
        }

        public Criteria andFlageLessThan(Integer value) {
            addCriterion("flage <", value, "flage");
            return (Criteria) this;
        }

        public Criteria andFlageLessThanOrEqualTo(Integer value) {
            addCriterion("flage <=", value, "flage");
            return (Criteria) this;
        }

        public Criteria andFlageIn(List<Integer> values) {
            addCriterion("flage in", values, "flage");
            return (Criteria) this;
        }

        public Criteria andFlageNotIn(List<Integer> values) {
            addCriterion("flage not in", values, "flage");
            return (Criteria) this;
        }

        public Criteria andFlageBetween(Integer value1, Integer value2) {
            addCriterion("flage between", value1, value2, "flage");
            return (Criteria) this;
        }

        public Criteria andFlageNotBetween(Integer value1, Integer value2) {
            addCriterion("flage not between", value1, value2, "flage");
            return (Criteria) this;
        }

        public Criteria andOrder_modeIsNull() {
            addCriterion("order_mode is null");
            return (Criteria) this;
        }

        public Criteria andOrder_modeIsNotNull() {
            addCriterion("order_mode is not null");
            return (Criteria) this;
        }

        public Criteria andOrder_modeEqualTo(Integer value) {
            addCriterion("order_mode =", value, "order_mode");
            return (Criteria) this;
        }

        public Criteria andOrder_modeNotEqualTo(Integer value) {
            addCriterion("order_mode <>", value, "order_mode");
            return (Criteria) this;
        }

        public Criteria andOrder_modeGreaterThan(Integer value) {
            addCriterion("order_mode >", value, "order_mode");
            return (Criteria) this;
        }

        public Criteria andOrder_modeGreaterThanOrEqualTo(Integer value) {
            addCriterion("order_mode >=", value, "order_mode");
            return (Criteria) this;
        }

        public Criteria andOrder_modeLessThan(Integer value) {
            addCriterion("order_mode <", value, "order_mode");
            return (Criteria) this;
        }

        public Criteria andOrder_modeLessThanOrEqualTo(Integer value) {
            addCriterion("order_mode <=", value, "order_mode");
            return (Criteria) this;
        }

        public Criteria andOrder_modeIn(List<Integer> values) {
            addCriterion("order_mode in", values, "order_mode");
            return (Criteria) this;
        }

        public Criteria andOrder_modeNotIn(List<Integer> values) {
            addCriterion("order_mode not in", values, "order_mode");
            return (Criteria) this;
        }

        public Criteria andOrder_modeBetween(Integer value1, Integer value2) {
            addCriterion("order_mode between", value1, value2, "order_mode");
            return (Criteria) this;
        }

        public Criteria andOrder_modeNotBetween(Integer value1, Integer value2) {
            addCriterion("order_mode not between", value1, value2, "order_mode");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}