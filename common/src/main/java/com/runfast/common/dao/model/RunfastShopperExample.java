package com.runfast.common.dao.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RunfastShopperExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public RunfastShopperExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("createTime is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("createTime is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("createTime =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("createTime <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("createTime >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("createTime >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("createTime <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("createTime <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("createTime in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("createTime not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("createTime between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("createTime not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreditRatingIsNull() {
            addCriterion("creditRating is null");
            return (Criteria) this;
        }

        public Criteria andCreditRatingIsNotNull() {
            addCriterion("creditRating is not null");
            return (Criteria) this;
        }

        public Criteria andCreditRatingEqualTo(Integer value) {
            addCriterion("creditRating =", value, "creditRating");
            return (Criteria) this;
        }

        public Criteria andCreditRatingNotEqualTo(Integer value) {
            addCriterion("creditRating <>", value, "creditRating");
            return (Criteria) this;
        }

        public Criteria andCreditRatingGreaterThan(Integer value) {
            addCriterion("creditRating >", value, "creditRating");
            return (Criteria) this;
        }

        public Criteria andCreditRatingGreaterThanOrEqualTo(Integer value) {
            addCriterion("creditRating >=", value, "creditRating");
            return (Criteria) this;
        }

        public Criteria andCreditRatingLessThan(Integer value) {
            addCriterion("creditRating <", value, "creditRating");
            return (Criteria) this;
        }

        public Criteria andCreditRatingLessThanOrEqualTo(Integer value) {
            addCriterion("creditRating <=", value, "creditRating");
            return (Criteria) this;
        }

        public Criteria andCreditRatingIn(List<Integer> values) {
            addCriterion("creditRating in", values, "creditRating");
            return (Criteria) this;
        }

        public Criteria andCreditRatingNotIn(List<Integer> values) {
            addCriterion("creditRating not in", values, "creditRating");
            return (Criteria) this;
        }

        public Criteria andCreditRatingBetween(Integer value1, Integer value2) {
            addCriterion("creditRating between", value1, value2, "creditRating");
            return (Criteria) this;
        }

        public Criteria andCreditRatingNotBetween(Integer value1, Integer value2) {
            addCriterion("creditRating not between", value1, value2, "creditRating");
            return (Criteria) this;
        }

        public Criteria andDistributionIsNull() {
            addCriterion("distribution is null");
            return (Criteria) this;
        }

        public Criteria andDistributionIsNotNull() {
            addCriterion("distribution is not null");
            return (Criteria) this;
        }

        public Criteria andDistributionEqualTo(String value) {
            addCriterion("distribution =", value, "distribution");
            return (Criteria) this;
        }

        public Criteria andDistributionNotEqualTo(String value) {
            addCriterion("distribution <>", value, "distribution");
            return (Criteria) this;
        }

        public Criteria andDistributionGreaterThan(String value) {
            addCriterion("distribution >", value, "distribution");
            return (Criteria) this;
        }

        public Criteria andDistributionGreaterThanOrEqualTo(String value) {
            addCriterion("distribution >=", value, "distribution");
            return (Criteria) this;
        }

        public Criteria andDistributionLessThan(String value) {
            addCriterion("distribution <", value, "distribution");
            return (Criteria) this;
        }

        public Criteria andDistributionLessThanOrEqualTo(String value) {
            addCriterion("distribution <=", value, "distribution");
            return (Criteria) this;
        }

        public Criteria andDistributionLike(String value) {
            addCriterion("distribution like", value, "distribution");
            return (Criteria) this;
        }

        public Criteria andDistributionNotLike(String value) {
            addCriterion("distribution not like", value, "distribution");
            return (Criteria) this;
        }

        public Criteria andDistributionIn(List<String> values) {
            addCriterion("distribution in", values, "distribution");
            return (Criteria) this;
        }

        public Criteria andDistributionNotIn(List<String> values) {
            addCriterion("distribution not in", values, "distribution");
            return (Criteria) this;
        }

        public Criteria andDistributionBetween(String value1, String value2) {
            addCriterion("distribution between", value1, value2, "distribution");
            return (Criteria) this;
        }

        public Criteria andDistributionNotBetween(String value1, String value2) {
            addCriterion("distribution not between", value1, value2, "distribution");
            return (Criteria) this;
        }

        public Criteria andDistributionmunIsNull() {
            addCriterion("distributionmun is null");
            return (Criteria) this;
        }

        public Criteria andDistributionmunIsNotNull() {
            addCriterion("distributionmun is not null");
            return (Criteria) this;
        }

        public Criteria andDistributionmunEqualTo(Integer value) {
            addCriterion("distributionmun =", value, "distributionmun");
            return (Criteria) this;
        }

        public Criteria andDistributionmunNotEqualTo(Integer value) {
            addCriterion("distributionmun <>", value, "distributionmun");
            return (Criteria) this;
        }

        public Criteria andDistributionmunGreaterThan(Integer value) {
            addCriterion("distributionmun >", value, "distributionmun");
            return (Criteria) this;
        }

        public Criteria andDistributionmunGreaterThanOrEqualTo(Integer value) {
            addCriterion("distributionmun >=", value, "distributionmun");
            return (Criteria) this;
        }

        public Criteria andDistributionmunLessThan(Integer value) {
            addCriterion("distributionmun <", value, "distributionmun");
            return (Criteria) this;
        }

        public Criteria andDistributionmunLessThanOrEqualTo(Integer value) {
            addCriterion("distributionmun <=", value, "distributionmun");
            return (Criteria) this;
        }

        public Criteria andDistributionmunIn(List<Integer> values) {
            addCriterion("distributionmun in", values, "distributionmun");
            return (Criteria) this;
        }

        public Criteria andDistributionmunNotIn(List<Integer> values) {
            addCriterion("distributionmun not in", values, "distributionmun");
            return (Criteria) this;
        }

        public Criteria andDistributionmunBetween(Integer value1, Integer value2) {
            addCriterion("distributionmun between", value1, value2, "distributionmun");
            return (Criteria) this;
        }

        public Criteria andDistributionmunNotBetween(Integer value1, Integer value2) {
            addCriterion("distributionmun not between", value1, value2, "distributionmun");
            return (Criteria) this;
        }

        public Criteria andEndTimeIsNull() {
            addCriterion("endTime is null");
            return (Criteria) this;
        }

        public Criteria andEndTimeIsNotNull() {
            addCriterion("endTime is not null");
            return (Criteria) this;
        }

        public Criteria andEndTimeEqualTo(Date value) {
            addCriterion("endTime =", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotEqualTo(Date value) {
            addCriterion("endTime <>", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeGreaterThan(Date value) {
            addCriterion("endTime >", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("endTime >=", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeLessThan(Date value) {
            addCriterion("endTime <", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeLessThanOrEqualTo(Date value) {
            addCriterion("endTime <=", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeIn(List<Date> values) {
            addCriterion("endTime in", values, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotIn(List<Date> values) {
            addCriterion("endTime not in", values, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeBetween(Date value1, Date value2) {
            addCriterion("endTime between", value1, value2, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotBetween(Date value1, Date value2) {
            addCriterion("endTime not between", value1, value2, "endTime");
            return (Criteria) this;
        }

        public Criteria andMunberIsNull() {
            addCriterion("munber is null");
            return (Criteria) this;
        }

        public Criteria andMunberIsNotNull() {
            addCriterion("munber is not null");
            return (Criteria) this;
        }

        public Criteria andMunberEqualTo(String value) {
            addCriterion("munber =", value, "munber");
            return (Criteria) this;
        }

        public Criteria andMunberNotEqualTo(String value) {
            addCriterion("munber <>", value, "munber");
            return (Criteria) this;
        }

        public Criteria andMunberGreaterThan(String value) {
            addCriterion("munber >", value, "munber");
            return (Criteria) this;
        }

        public Criteria andMunberGreaterThanOrEqualTo(String value) {
            addCriterion("munber >=", value, "munber");
            return (Criteria) this;
        }

        public Criteria andMunberLessThan(String value) {
            addCriterion("munber <", value, "munber");
            return (Criteria) this;
        }

        public Criteria andMunberLessThanOrEqualTo(String value) {
            addCriterion("munber <=", value, "munber");
            return (Criteria) this;
        }

        public Criteria andMunberLike(String value) {
            addCriterion("munber like", value, "munber");
            return (Criteria) this;
        }

        public Criteria andMunberNotLike(String value) {
            addCriterion("munber not like", value, "munber");
            return (Criteria) this;
        }

        public Criteria andMunberIn(List<String> values) {
            addCriterion("munber in", values, "munber");
            return (Criteria) this;
        }

        public Criteria andMunberNotIn(List<String> values) {
            addCriterion("munber not in", values, "munber");
            return (Criteria) this;
        }

        public Criteria andMunberBetween(String value1, String value2) {
            addCriterion("munber between", value1, value2, "munber");
            return (Criteria) this;
        }

        public Criteria andMunberNotBetween(String value1, String value2) {
            addCriterion("munber not between", value1, value2, "munber");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andPasswordIsNull() {
            addCriterion("password is null");
            return (Criteria) this;
        }

        public Criteria andPasswordIsNotNull() {
            addCriterion("password is not null");
            return (Criteria) this;
        }

        public Criteria andPasswordEqualTo(String value) {
            addCriterion("password =", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotEqualTo(String value) {
            addCriterion("password <>", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordGreaterThan(String value) {
            addCriterion("password >", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordGreaterThanOrEqualTo(String value) {
            addCriterion("password >=", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLessThan(String value) {
            addCriterion("password <", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLessThanOrEqualTo(String value) {
            addCriterion("password <=", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLike(String value) {
            addCriterion("password like", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotLike(String value) {
            addCriterion("password not like", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordIn(List<String> values) {
            addCriterion("password in", values, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotIn(List<String> values) {
            addCriterion("password not in", values, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordBetween(String value1, String value2) {
            addCriterion("password between", value1, value2, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotBetween(String value1, String value2) {
            addCriterion("password not between", value1, value2, "password");
            return (Criteria) this;
        }

        public Criteria andStartTimeIsNull() {
            addCriterion("startTime is null");
            return (Criteria) this;
        }

        public Criteria andStartTimeIsNotNull() {
            addCriterion("startTime is not null");
            return (Criteria) this;
        }

        public Criteria andStartTimeEqualTo(Date value) {
            addCriterion("startTime =", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotEqualTo(Date value) {
            addCriterion("startTime <>", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeGreaterThan(Date value) {
            addCriterion("startTime >", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("startTime >=", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeLessThan(Date value) {
            addCriterion("startTime <", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeLessThanOrEqualTo(Date value) {
            addCriterion("startTime <=", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeIn(List<Date> values) {
            addCriterion("startTime in", values, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotIn(List<Date> values) {
            addCriterion("startTime not in", values, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeBetween(Date value1, Date value2) {
            addCriterion("startTime between", value1, value2, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotBetween(Date value1, Date value2) {
            addCriterion("startTime not between", value1, value2, "startTime");
            return (Criteria) this;
        }

        public Criteria andUserNameIsNull() {
            addCriterion("userName is null");
            return (Criteria) this;
        }

        public Criteria andUserNameIsNotNull() {
            addCriterion("userName is not null");
            return (Criteria) this;
        }

        public Criteria andUserNameEqualTo(String value) {
            addCriterion("userName =", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotEqualTo(String value) {
            addCriterion("userName <>", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameGreaterThan(String value) {
            addCriterion("userName >", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameGreaterThanOrEqualTo(String value) {
            addCriterion("userName >=", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLessThan(String value) {
            addCriterion("userName <", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLessThanOrEqualTo(String value) {
            addCriterion("userName <=", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLike(String value) {
            addCriterion("userName like", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotLike(String value) {
            addCriterion("userName not like", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameIn(List<String> values) {
            addCriterion("userName in", values, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotIn(List<String> values) {
            addCriterion("userName not in", values, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameBetween(String value1, String value2) {
            addCriterion("userName between", value1, value2, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotBetween(String value1, String value2) {
            addCriterion("userName not between", value1, value2, "userName");
            return (Criteria) this;
        }

        public Criteria andWorktodayIsNull() {
            addCriterion("worktoday is null");
            return (Criteria) this;
        }

        public Criteria andWorktodayIsNotNull() {
            addCriterion("worktoday is not null");
            return (Criteria) this;
        }

        public Criteria andWorktodayEqualTo(String value) {
            addCriterion("worktoday =", value, "worktoday");
            return (Criteria) this;
        }

        public Criteria andWorktodayNotEqualTo(String value) {
            addCriterion("worktoday <>", value, "worktoday");
            return (Criteria) this;
        }

        public Criteria andWorktodayGreaterThan(String value) {
            addCriterion("worktoday >", value, "worktoday");
            return (Criteria) this;
        }

        public Criteria andWorktodayGreaterThanOrEqualTo(String value) {
            addCriterion("worktoday >=", value, "worktoday");
            return (Criteria) this;
        }

        public Criteria andWorktodayLessThan(String value) {
            addCriterion("worktoday <", value, "worktoday");
            return (Criteria) this;
        }

        public Criteria andWorktodayLessThanOrEqualTo(String value) {
            addCriterion("worktoday <=", value, "worktoday");
            return (Criteria) this;
        }

        public Criteria andWorktodayLike(String value) {
            addCriterion("worktoday like", value, "worktoday");
            return (Criteria) this;
        }

        public Criteria andWorktodayNotLike(String value) {
            addCriterion("worktoday not like", value, "worktoday");
            return (Criteria) this;
        }

        public Criteria andWorktodayIn(List<String> values) {
            addCriterion("worktoday in", values, "worktoday");
            return (Criteria) this;
        }

        public Criteria andWorktodayNotIn(List<String> values) {
            addCriterion("worktoday not in", values, "worktoday");
            return (Criteria) this;
        }

        public Criteria andWorktodayBetween(String value1, String value2) {
            addCriterion("worktoday between", value1, value2, "worktoday");
            return (Criteria) this;
        }

        public Criteria andWorktodayNotBetween(String value1, String value2) {
            addCriterion("worktoday not between", value1, value2, "worktoday");
            return (Criteria) this;
        }

        public Criteria andLatitudeIsNull() {
            addCriterion("latitude is null");
            return (Criteria) this;
        }

        public Criteria andLatitudeIsNotNull() {
            addCriterion("latitude is not null");
            return (Criteria) this;
        }

        public Criteria andLatitudeEqualTo(String value) {
            addCriterion("latitude =", value, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeNotEqualTo(String value) {
            addCriterion("latitude <>", value, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeGreaterThan(String value) {
            addCriterion("latitude >", value, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeGreaterThanOrEqualTo(String value) {
            addCriterion("latitude >=", value, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeLessThan(String value) {
            addCriterion("latitude <", value, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeLessThanOrEqualTo(String value) {
            addCriterion("latitude <=", value, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeLike(String value) {
            addCriterion("latitude like", value, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeNotLike(String value) {
            addCriterion("latitude not like", value, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeIn(List<String> values) {
            addCriterion("latitude in", values, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeNotIn(List<String> values) {
            addCriterion("latitude not in", values, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeBetween(String value1, String value2) {
            addCriterion("latitude between", value1, value2, "latitude");
            return (Criteria) this;
        }

        public Criteria andLatitudeNotBetween(String value1, String value2) {
            addCriterion("latitude not between", value1, value2, "latitude");
            return (Criteria) this;
        }

        public Criteria andIntegeritudeIsNull() {
            addCriterion("longitude is null");
            return (Criteria) this;
        }

        public Criteria andIntegeritudeIsNotNull() {
            addCriterion("longitude is not null");
            return (Criteria) this;
        }

        public Criteria andIntegeritudeEqualTo(String value) {
            addCriterion("longitude =", value, "longitude");
            return (Criteria) this;
        }

        public Criteria andIntegeritudeNotEqualTo(String value) {
            addCriterion("longitude <>", value, "longitude");
            return (Criteria) this;
        }

        public Criteria andIntegeritudeGreaterThan(String value) {
            addCriterion("longitude >", value, "longitude");
            return (Criteria) this;
        }

        public Criteria andIntegeritudeGreaterThanOrEqualTo(String value) {
            addCriterion("longitude >=", value, "longitude");
            return (Criteria) this;
        }

        public Criteria andIntegeritudeLessThan(String value) {
            addCriterion("longitude <", value, "longitude");
            return (Criteria) this;
        }

        public Criteria andIntegeritudeLessThanOrEqualTo(String value) {
            addCriterion("longitude <=", value, "longitude");
            return (Criteria) this;
        }

        public Criteria andIntegeritudeLike(String value) {
            addCriterion("longitude like", value, "longitude");
            return (Criteria) this;
        }

        public Criteria andIntegeritudeNotLike(String value) {
            addCriterion("longitude not like", value, "longitude");
            return (Criteria) this;
        }

        public Criteria andIntegeritudeIn(List<String> values) {
            addCriterion("longitude in", values, "longitude");
            return (Criteria) this;
        }

        public Criteria andIntegeritudeNotIn(List<String> values) {
            addCriterion("longitude not in", values, "longitude");
            return (Criteria) this;
        }

        public Criteria andIntegeritudeBetween(String value1, String value2) {
            addCriterion("longitude between", value1, value2, "longitude");
            return (Criteria) this;
        }

        public Criteria andIntegeritudeNotBetween(String value1, String value2) {
            addCriterion("longitude not between", value1, value2, "longitude");
            return (Criteria) this;
        }

        public Criteria andInterimremainderIsNull() {
            addCriterion("interimremainder is null");
            return (Criteria) this;
        }

        public Criteria andInterimremainderIsNotNull() {
            addCriterion("interimremainder is not null");
            return (Criteria) this;
        }

        public Criteria andInterimremainderEqualTo(BigDecimal value) {
            addCriterion("interimremainder =", value, "interimremainder");
            return (Criteria) this;
        }

        public Criteria andInterimremainderNotEqualTo(BigDecimal value) {
            addCriterion("interimremainder <>", value, "interimremainder");
            return (Criteria) this;
        }

        public Criteria andInterimremainderGreaterThan(BigDecimal value) {
            addCriterion("interimremainder >", value, "interimremainder");
            return (Criteria) this;
        }

        public Criteria andInterimremainderGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("interimremainder >=", value, "interimremainder");
            return (Criteria) this;
        }

        public Criteria andInterimremainderLessThan(BigDecimal value) {
            addCriterion("interimremainder <", value, "interimremainder");
            return (Criteria) this;
        }

        public Criteria andInterimremainderLessThanOrEqualTo(BigDecimal value) {
            addCriterion("interimremainder <=", value, "interimremainder");
            return (Criteria) this;
        }

        public Criteria andInterimremainderIn(List<BigDecimal> values) {
            addCriterion("interimremainder in", values, "interimremainder");
            return (Criteria) this;
        }

        public Criteria andInterimremainderNotIn(List<BigDecimal> values) {
            addCriterion("interimremainder not in", values, "interimremainder");
            return (Criteria) this;
        }

        public Criteria andInterimremainderBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("interimremainder between", value1, value2, "interimremainder");
            return (Criteria) this;
        }

        public Criteria andInterimremainderNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("interimremainder not between", value1, value2, "interimremainder");
            return (Criteria) this;
        }

        public Criteria andMinmonetyIsNull() {
            addCriterion("minmonety is null");
            return (Criteria) this;
        }

        public Criteria andMinmonetyIsNotNull() {
            addCriterion("minmonety is not null");
            return (Criteria) this;
        }

        public Criteria andMinmonetyEqualTo(BigDecimal value) {
            addCriterion("minmonety =", value, "minmonety");
            return (Criteria) this;
        }

        public Criteria andMinmonetyNotEqualTo(BigDecimal value) {
            addCriterion("minmonety <>", value, "minmonety");
            return (Criteria) this;
        }

        public Criteria andMinmonetyGreaterThan(BigDecimal value) {
            addCriterion("minmonety >", value, "minmonety");
            return (Criteria) this;
        }

        public Criteria andMinmonetyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("minmonety >=", value, "minmonety");
            return (Criteria) this;
        }

        public Criteria andMinmonetyLessThan(BigDecimal value) {
            addCriterion("minmonety <", value, "minmonety");
            return (Criteria) this;
        }

        public Criteria andMinmonetyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("minmonety <=", value, "minmonety");
            return (Criteria) this;
        }

        public Criteria andMinmonetyIn(List<BigDecimal> values) {
            addCriterion("minmonety in", values, "minmonety");
            return (Criteria) this;
        }

        public Criteria andMinmonetyNotIn(List<BigDecimal> values) {
            addCriterion("minmonety not in", values, "minmonety");
            return (Criteria) this;
        }

        public Criteria andMinmonetyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("minmonety between", value1, value2, "minmonety");
            return (Criteria) this;
        }

        public Criteria andMinmonetyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("minmonety not between", value1, value2, "minmonety");
            return (Criteria) this;
        }

        public Criteria andRemainderIsNull() {
            addCriterion("remainder is null");
            return (Criteria) this;
        }

        public Criteria andRemainderIsNotNull() {
            addCriterion("remainder is not null");
            return (Criteria) this;
        }

        public Criteria andRemainderEqualTo(BigDecimal value) {
            addCriterion("remainder =", value, "remainder");
            return (Criteria) this;
        }

        public Criteria andRemainderNotEqualTo(BigDecimal value) {
            addCriterion("remainder <>", value, "remainder");
            return (Criteria) this;
        }

        public Criteria andRemainderGreaterThan(BigDecimal value) {
            addCriterion("remainder >", value, "remainder");
            return (Criteria) this;
        }

        public Criteria andRemainderGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("remainder >=", value, "remainder");
            return (Criteria) this;
        }

        public Criteria andRemainderLessThan(BigDecimal value) {
            addCriterion("remainder <", value, "remainder");
            return (Criteria) this;
        }

        public Criteria andRemainderLessThanOrEqualTo(BigDecimal value) {
            addCriterion("remainder <=", value, "remainder");
            return (Criteria) this;
        }

        public Criteria andRemainderIn(List<BigDecimal> values) {
            addCriterion("remainder in", values, "remainder");
            return (Criteria) this;
        }

        public Criteria andRemainderNotIn(List<BigDecimal> values) {
            addCriterion("remainder not in", values, "remainder");
            return (Criteria) this;
        }

        public Criteria andRemainderBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("remainder between", value1, value2, "remainder");
            return (Criteria) this;
        }

        public Criteria andRemainderNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("remainder not between", value1, value2, "remainder");
            return (Criteria) this;
        }

        public Criteria andBdchannelIdIsNull() {
            addCriterion("bdchannelId is null");
            return (Criteria) this;
        }

        public Criteria andBdchannelIdIsNotNull() {
            addCriterion("bdchannelId is not null");
            return (Criteria) this;
        }

        public Criteria andBdchannelIdEqualTo(String value) {
            addCriterion("bdchannelId =", value, "bdchannelId");
            return (Criteria) this;
        }

        public Criteria andBdchannelIdNotEqualTo(String value) {
            addCriterion("bdchannelId <>", value, "bdchannelId");
            return (Criteria) this;
        }

        public Criteria andBdchannelIdGreaterThan(String value) {
            addCriterion("bdchannelId >", value, "bdchannelId");
            return (Criteria) this;
        }

        public Criteria andBdchannelIdGreaterThanOrEqualTo(String value) {
            addCriterion("bdchannelId >=", value, "bdchannelId");
            return (Criteria) this;
        }

        public Criteria andBdchannelIdLessThan(String value) {
            addCriterion("bdchannelId <", value, "bdchannelId");
            return (Criteria) this;
        }

        public Criteria andBdchannelIdLessThanOrEqualTo(String value) {
            addCriterion("bdchannelId <=", value, "bdchannelId");
            return (Criteria) this;
        }

        public Criteria andBdchannelIdLike(String value) {
            addCriterion("bdchannelId like", value, "bdchannelId");
            return (Criteria) this;
        }

        public Criteria andBdchannelIdNotLike(String value) {
            addCriterion("bdchannelId not like", value, "bdchannelId");
            return (Criteria) this;
        }

        public Criteria andBdchannelIdIn(List<String> values) {
            addCriterion("bdchannelId in", values, "bdchannelId");
            return (Criteria) this;
        }

        public Criteria andBdchannelIdNotIn(List<String> values) {
            addCriterion("bdchannelId not in", values, "bdchannelId");
            return (Criteria) this;
        }

        public Criteria andBdchannelIdBetween(String value1, String value2) {
            addCriterion("bdchannelId between", value1, value2, "bdchannelId");
            return (Criteria) this;
        }

        public Criteria andBdchannelIdNotBetween(String value1, String value2) {
            addCriterion("bdchannelId not between", value1, value2, "bdchannelId");
            return (Criteria) this;
        }

        public Criteria andBduserIdIsNull() {
            addCriterion("bduserId is null");
            return (Criteria) this;
        }

        public Criteria andBduserIdIsNotNull() {
            addCriterion("bduserId is not null");
            return (Criteria) this;
        }

        public Criteria andBduserIdEqualTo(String value) {
            addCriterion("bduserId =", value, "bduserId");
            return (Criteria) this;
        }

        public Criteria andBduserIdNotEqualTo(String value) {
            addCriterion("bduserId <>", value, "bduserId");
            return (Criteria) this;
        }

        public Criteria andBduserIdGreaterThan(String value) {
            addCriterion("bduserId >", value, "bduserId");
            return (Criteria) this;
        }

        public Criteria andBduserIdGreaterThanOrEqualTo(String value) {
            addCriterion("bduserId >=", value, "bduserId");
            return (Criteria) this;
        }

        public Criteria andBduserIdLessThan(String value) {
            addCriterion("bduserId <", value, "bduserId");
            return (Criteria) this;
        }

        public Criteria andBduserIdLessThanOrEqualTo(String value) {
            addCriterion("bduserId <=", value, "bduserId");
            return (Criteria) this;
        }

        public Criteria andBduserIdLike(String value) {
            addCriterion("bduserId like", value, "bduserId");
            return (Criteria) this;
        }

        public Criteria andBduserIdNotLike(String value) {
            addCriterion("bduserId not like", value, "bduserId");
            return (Criteria) this;
        }

        public Criteria andBduserIdIn(List<String> values) {
            addCriterion("bduserId in", values, "bduserId");
            return (Criteria) this;
        }

        public Criteria andBduserIdNotIn(List<String> values) {
            addCriterion("bduserId not in", values, "bduserId");
            return (Criteria) this;
        }

        public Criteria andBduserIdBetween(String value1, String value2) {
            addCriterion("bduserId between", value1, value2, "bduserId");
            return (Criteria) this;
        }

        public Criteria andBduserIdNotBetween(String value1, String value2) {
            addCriterion("bduserId not between", value1, value2, "bduserId");
            return (Criteria) this;
        }

        public Criteria andBptypeIsNull() {
            addCriterion("bptype is null");
            return (Criteria) this;
        }

        public Criteria andBptypeIsNotNull() {
            addCriterion("bptype is not null");
            return (Criteria) this;
        }

        public Criteria andBptypeEqualTo(Integer value) {
            addCriterion("bptype =", value, "bptype");
            return (Criteria) this;
        }

        public Criteria andBptypeNotEqualTo(Integer value) {
            addCriterion("bptype <>", value, "bptype");
            return (Criteria) this;
        }

        public Criteria andBptypeGreaterThan(Integer value) {
            addCriterion("bptype >", value, "bptype");
            return (Criteria) this;
        }

        public Criteria andBptypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("bptype >=", value, "bptype");
            return (Criteria) this;
        }

        public Criteria andBptypeLessThan(Integer value) {
            addCriterion("bptype <", value, "bptype");
            return (Criteria) this;
        }

        public Criteria andBptypeLessThanOrEqualTo(Integer value) {
            addCriterion("bptype <=", value, "bptype");
            return (Criteria) this;
        }

        public Criteria andBptypeIn(List<Integer> values) {
            addCriterion("bptype in", values, "bptype");
            return (Criteria) this;
        }

        public Criteria andBptypeNotIn(List<Integer> values) {
            addCriterion("bptype not in", values, "bptype");
            return (Criteria) this;
        }

        public Criteria andBptypeBetween(Integer value1, Integer value2) {
            addCriterion("bptype between", value1, value2, "bptype");
            return (Criteria) this;
        }

        public Criteria andBptypeNotBetween(Integer value1, Integer value2) {
            addCriterion("bptype not between", value1, value2, "bptype");
            return (Criteria) this;
        }

        public Criteria andCreditLevelidIsNull() {
            addCriterion("creditLevelid is null");
            return (Criteria) this;
        }

        public Criteria andCreditLevelidIsNotNull() {
            addCriterion("creditLevelid is not null");
            return (Criteria) this;
        }

        public Criteria andCreditLevelidEqualTo(Integer value) {
            addCriterion("creditLevelid =", value, "creditLevelid");
            return (Criteria) this;
        }

        public Criteria andCreditLevelidNotEqualTo(Integer value) {
            addCriterion("creditLevelid <>", value, "creditLevelid");
            return (Criteria) this;
        }

        public Criteria andCreditLevelidGreaterThan(Integer value) {
            addCriterion("creditLevelid >", value, "creditLevelid");
            return (Criteria) this;
        }

        public Criteria andCreditLevelidGreaterThanOrEqualTo(Integer value) {
            addCriterion("creditLevelid >=", value, "creditLevelid");
            return (Criteria) this;
        }

        public Criteria andCreditLevelidLessThan(Integer value) {
            addCriterion("creditLevelid <", value, "creditLevelid");
            return (Criteria) this;
        }

        public Criteria andCreditLevelidLessThanOrEqualTo(Integer value) {
            addCriterion("creditLevelid <=", value, "creditLevelid");
            return (Criteria) this;
        }

        public Criteria andCreditLevelidIn(List<Integer> values) {
            addCriterion("creditLevelid in", values, "creditLevelid");
            return (Criteria) this;
        }

        public Criteria andCreditLevelidNotIn(List<Integer> values) {
            addCriterion("creditLevelid not in", values, "creditLevelid");
            return (Criteria) this;
        }

        public Criteria andCreditLevelidBetween(Integer value1, Integer value2) {
            addCriterion("creditLevelid between", value1, value2, "creditLevelid");
            return (Criteria) this;
        }

        public Criteria andCreditLevelidNotBetween(Integer value1, Integer value2) {
            addCriterion("creditLevelid not between", value1, value2, "creditLevelid");
            return (Criteria) this;
        }

        public Criteria andAgentIdIsNull() {
            addCriterion("agentId is null");
            return (Criteria) this;
        }

        public Criteria andAgentIdIsNotNull() {
            addCriterion("agentId is not null");
            return (Criteria) this;
        }

        public Criteria andAgentIdEqualTo(Integer value) {
            addCriterion("agentId =", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotEqualTo(Integer value) {
            addCriterion("agentId <>", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdGreaterThan(Integer value) {
            addCriterion("agentId >", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("agentId >=", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdLessThan(Integer value) {
            addCriterion("agentId <", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdLessThanOrEqualTo(Integer value) {
            addCriterion("agentId <=", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdIn(List<Integer> values) {
            addCriterion("agentId in", values, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotIn(List<Integer> values) {
            addCriterion("agentId not in", values, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdBetween(Integer value1, Integer value2) {
            addCriterion("agentId between", value1, value2, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotBetween(Integer value1, Integer value2) {
            addCriterion("agentId not between", value1, value2, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentNameIsNull() {
            addCriterion("agentName is null");
            return (Criteria) this;
        }

        public Criteria andAgentNameIsNotNull() {
            addCriterion("agentName is not null");
            return (Criteria) this;
        }

        public Criteria andAgentNameEqualTo(String value) {
            addCriterion("agentName =", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotEqualTo(String value) {
            addCriterion("agentName <>", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameGreaterThan(String value) {
            addCriterion("agentName >", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameGreaterThanOrEqualTo(String value) {
            addCriterion("agentName >=", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameLessThan(String value) {
            addCriterion("agentName <", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameLessThanOrEqualTo(String value) {
            addCriterion("agentName <=", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameLike(String value) {
            addCriterion("agentName like", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotLike(String value) {
            addCriterion("agentName not like", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameIn(List<String> values) {
            addCriterion("agentName in", values, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotIn(List<String> values) {
            addCriterion("agentName not in", values, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameBetween(String value1, String value2) {
            addCriterion("agentName between", value1, value2, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotBetween(String value1, String value2) {
            addCriterion("agentName not between", value1, value2, "agentName");
            return (Criteria) this;
        }

        public Criteria andWorkingIsNull() {
            addCriterion("working is null");
            return (Criteria) this;
        }

        public Criteria andWorkingIsNotNull() {
            addCriterion("working is not null");
            return (Criteria) this;
        }

        public Criteria andWorkingEqualTo(Integer value) {
            addCriterion("working =", value, "working");
            return (Criteria) this;
        }

        public Criteria andWorkingNotEqualTo(Integer value) {
            addCriterion("working <>", value, "working");
            return (Criteria) this;
        }

        public Criteria andWorkingGreaterThan(Integer value) {
            addCriterion("working >", value, "working");
            return (Criteria) this;
        }

        public Criteria andWorkingGreaterThanOrEqualTo(Integer value) {
            addCriterion("working >=", value, "working");
            return (Criteria) this;
        }

        public Criteria andWorkingLessThan(Integer value) {
            addCriterion("working <", value, "working");
            return (Criteria) this;
        }

        public Criteria andWorkingLessThanOrEqualTo(Integer value) {
            addCriterion("working <=", value, "working");
            return (Criteria) this;
        }

        public Criteria andWorkingIn(List<Integer> values) {
            addCriterion("working in", values, "working");
            return (Criteria) this;
        }

        public Criteria andWorkingNotIn(List<Integer> values) {
            addCriterion("working not in", values, "working");
            return (Criteria) this;
        }

        public Criteria andWorkingBetween(Integer value1, Integer value2) {
            addCriterion("working between", value1, value2, "working");
            return (Criteria) this;
        }

        public Criteria andWorkingNotBetween(Integer value1, Integer value2) {
            addCriterion("working not between", value1, value2, "working");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Integer value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Integer value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Integer value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Integer value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Integer value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Integer> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Integer> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Integer value1, Integer value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andOtherIdIsNull() {
            addCriterion("otherId is null");
            return (Criteria) this;
        }

        public Criteria andOtherIdIsNotNull() {
            addCriterion("otherId is not null");
            return (Criteria) this;
        }

        public Criteria andOtherIdEqualTo(String value) {
            addCriterion("otherId =", value, "otherId");
            return (Criteria) this;
        }

        public Criteria andOtherIdNotEqualTo(String value) {
            addCriterion("otherId <>", value, "otherId");
            return (Criteria) this;
        }

        public Criteria andOtherIdGreaterThan(String value) {
            addCriterion("otherId >", value, "otherId");
            return (Criteria) this;
        }

        public Criteria andOtherIdGreaterThanOrEqualTo(String value) {
            addCriterion("otherId >=", value, "otherId");
            return (Criteria) this;
        }

        public Criteria andOtherIdLessThan(String value) {
            addCriterion("otherId <", value, "otherId");
            return (Criteria) this;
        }

        public Criteria andOtherIdLessThanOrEqualTo(String value) {
            addCriterion("otherId <=", value, "otherId");
            return (Criteria) this;
        }

        public Criteria andOtherIdLike(String value) {
            addCriterion("otherId like", value, "otherId");
            return (Criteria) this;
        }

        public Criteria andOtherIdNotLike(String value) {
            addCriterion("otherId not like", value, "otherId");
            return (Criteria) this;
        }

        public Criteria andOtherIdIn(List<String> values) {
            addCriterion("otherId in", values, "otherId");
            return (Criteria) this;
        }

        public Criteria andOtherIdNotIn(List<String> values) {
            addCriterion("otherId not in", values, "otherId");
            return (Criteria) this;
        }

        public Criteria andOtherIdBetween(String value1, String value2) {
            addCriterion("otherId between", value1, value2, "otherId");
            return (Criteria) this;
        }

        public Criteria andOtherIdNotBetween(String value1, String value2) {
            addCriterion("otherId not between", value1, value2, "otherId");
            return (Criteria) this;
        }

        public Criteria andPushTypeIsNull() {
            addCriterion("pushType is null");
            return (Criteria) this;
        }

        public Criteria andPushTypeIsNotNull() {
            addCriterion("pushType is not null");
            return (Criteria) this;
        }

        public Criteria andPushTypeEqualTo(Integer value) {
            addCriterion("pushType =", value, "pushType");
            return (Criteria) this;
        }

        public Criteria andPushTypeNotEqualTo(Integer value) {
            addCriterion("pushType <>", value, "pushType");
            return (Criteria) this;
        }

        public Criteria andPushTypeGreaterThan(Integer value) {
            addCriterion("pushType >", value, "pushType");
            return (Criteria) this;
        }

        public Criteria andPushTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("pushType >=", value, "pushType");
            return (Criteria) this;
        }

        public Criteria andPushTypeLessThan(Integer value) {
            addCriterion("pushType <", value, "pushType");
            return (Criteria) this;
        }

        public Criteria andPushTypeLessThanOrEqualTo(Integer value) {
            addCriterion("pushType <=", value, "pushType");
            return (Criteria) this;
        }

        public Criteria andPushTypeIn(List<Integer> values) {
            addCriterion("pushType in", values, "pushType");
            return (Criteria) this;
        }

        public Criteria andPushTypeNotIn(List<Integer> values) {
            addCriterion("pushType not in", values, "pushType");
            return (Criteria) this;
        }

        public Criteria andPushTypeBetween(Integer value1, Integer value2) {
            addCriterion("pushType between", value1, value2, "pushType");
            return (Criteria) this;
        }

        public Criteria andPushTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("pushType not between", value1, value2, "pushType");
            return (Criteria) this;
        }

        public Criteria andTeamidIsNull() {
            addCriterion("teamid is null");
            return (Criteria) this;
        }

        public Criteria andTeamidIsNotNull() {
            addCriterion("teamid is not null");
            return (Criteria) this;
        }

        public Criteria andTeamidEqualTo(Integer value) {
            addCriterion("teamid =", value, "teamid");
            return (Criteria) this;
        }

        public Criteria andTeamidNotEqualTo(Integer value) {
            addCriterion("teamid <>", value, "teamid");
            return (Criteria) this;
        }

        public Criteria andTeamidGreaterThan(Integer value) {
            addCriterion("teamid >", value, "teamid");
            return (Criteria) this;
        }

        public Criteria andTeamidGreaterThanOrEqualTo(Integer value) {
            addCriterion("teamid >=", value, "teamid");
            return (Criteria) this;
        }

        public Criteria andTeamidLessThan(Integer value) {
            addCriterion("teamid <", value, "teamid");
            return (Criteria) this;
        }

        public Criteria andTeamidLessThanOrEqualTo(Integer value) {
            addCriterion("teamid <=", value, "teamid");
            return (Criteria) this;
        }

        public Criteria andTeamidIn(List<Integer> values) {
            addCriterion("teamid in", values, "teamid");
            return (Criteria) this;
        }

        public Criteria andTeamidNotIn(List<Integer> values) {
            addCriterion("teamid not in", values, "teamid");
            return (Criteria) this;
        }

        public Criteria andTeamidBetween(Integer value1, Integer value2) {
            addCriterion("teamid between", value1, value2, "teamid");
            return (Criteria) this;
        }

        public Criteria andTeamidNotBetween(Integer value1, Integer value2) {
            addCriterion("teamid not between", value1, value2, "teamid");
            return (Criteria) this;
        }

        public Criteria andTeamnameIsNull() {
            addCriterion("teamname is null");
            return (Criteria) this;
        }

        public Criteria andTeamnameIsNotNull() {
            addCriterion("teamname is not null");
            return (Criteria) this;
        }

        public Criteria andTeamnameEqualTo(String value) {
            addCriterion("teamname =", value, "teamname");
            return (Criteria) this;
        }

        public Criteria andTeamnameNotEqualTo(String value) {
            addCriterion("teamname <>", value, "teamname");
            return (Criteria) this;
        }

        public Criteria andTeamnameGreaterThan(String value) {
            addCriterion("teamname >", value, "teamname");
            return (Criteria) this;
        }

        public Criteria andTeamnameGreaterThanOrEqualTo(String value) {
            addCriterion("teamname >=", value, "teamname");
            return (Criteria) this;
        }

        public Criteria andTeamnameLessThan(String value) {
            addCriterion("teamname <", value, "teamname");
            return (Criteria) this;
        }

        public Criteria andTeamnameLessThanOrEqualTo(String value) {
            addCriterion("teamname <=", value, "teamname");
            return (Criteria) this;
        }

        public Criteria andTeamnameLike(String value) {
            addCriterion("teamname like", value, "teamname");
            return (Criteria) this;
        }

        public Criteria andTeamnameNotLike(String value) {
            addCriterion("teamname not like", value, "teamname");
            return (Criteria) this;
        }

        public Criteria andTeamnameIn(List<String> values) {
            addCriterion("teamname in", values, "teamname");
            return (Criteria) this;
        }

        public Criteria andTeamnameNotIn(List<String> values) {
            addCriterion("teamname not in", values, "teamname");
            return (Criteria) this;
        }

        public Criteria andTeamnameBetween(String value1, String value2) {
            addCriterion("teamname between", value1, value2, "teamname");
            return (Criteria) this;
        }

        public Criteria andTeamnameNotBetween(String value1, String value2) {
            addCriterion("teamname not between", value1, value2, "teamname");
            return (Criteria) this;
        }

        public Criteria andDistanceIsNull() {
            addCriterion("distance is null");
            return (Criteria) this;
        }

        public Criteria andDistanceIsNotNull() {
            addCriterion("distance is not null");
            return (Criteria) this;
        }

        public Criteria andDistanceEqualTo(Double value) {
            addCriterion("distance =", value, "distance");
            return (Criteria) this;
        }

        public Criteria andDistanceNotEqualTo(Double value) {
            addCriterion("distance <>", value, "distance");
            return (Criteria) this;
        }

        public Criteria andDistanceGreaterThan(Double value) {
            addCriterion("distance >", value, "distance");
            return (Criteria) this;
        }

        public Criteria andDistanceGreaterThanOrEqualTo(Double value) {
            addCriterion("distance >=", value, "distance");
            return (Criteria) this;
        }

        public Criteria andDistanceLessThan(Double value) {
            addCriterion("distance <", value, "distance");
            return (Criteria) this;
        }

        public Criteria andDistanceLessThanOrEqualTo(Double value) {
            addCriterion("distance <=", value, "distance");
            return (Criteria) this;
        }

        public Criteria andDistanceIn(List<Double> values) {
            addCriterion("distance in", values, "distance");
            return (Criteria) this;
        }

        public Criteria andDistanceNotIn(List<Double> values) {
            addCriterion("distance not in", values, "distance");
            return (Criteria) this;
        }

        public Criteria andDistanceBetween(Double value1, Double value2) {
            addCriterion("distance between", value1, value2, "distance");
            return (Criteria) this;
        }

        public Criteria andDistanceNotBetween(Double value1, Double value2) {
            addCriterion("distance not between", value1, value2, "distance");
            return (Criteria) this;
        }

        public Criteria andAliasIsNull() {
            addCriterion("alias is null");
            return (Criteria) this;
        }

        public Criteria andAliasIsNotNull() {
            addCriterion("alias is not null");
            return (Criteria) this;
        }

        public Criteria andAliasEqualTo(String value) {
            addCriterion("alias =", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasNotEqualTo(String value) {
            addCriterion("alias <>", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasGreaterThan(String value) {
            addCriterion("alias >", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasGreaterThanOrEqualTo(String value) {
            addCriterion("alias >=", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasLessThan(String value) {
            addCriterion("alias <", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasLessThanOrEqualTo(String value) {
            addCriterion("alias <=", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasLike(String value) {
            addCriterion("alias like", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasNotLike(String value) {
            addCriterion("alias not like", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasIn(List<String> values) {
            addCriterion("alias in", values, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasNotIn(List<String> values) {
            addCriterion("alias not in", values, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasBetween(String value1, String value2) {
            addCriterion("alias between", value1, value2, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasNotBetween(String value1, String value2) {
            addCriterion("alias not between", value1, value2, "alias");
            return (Criteria) this;
        }

        public Criteria andCoefficientIsNull() {
            addCriterion("coefficient is null");
            return (Criteria) this;
        }

        public Criteria andCoefficientIsNotNull() {
            addCriterion("coefficient is not null");
            return (Criteria) this;
        }

        public Criteria andCoefficientEqualTo(BigDecimal value) {
            addCriterion("coefficient =", value, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientNotEqualTo(BigDecimal value) {
            addCriterion("coefficient <>", value, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientGreaterThan(BigDecimal value) {
            addCriterion("coefficient >", value, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("coefficient >=", value, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientLessThan(BigDecimal value) {
            addCriterion("coefficient <", value, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientLessThanOrEqualTo(BigDecimal value) {
            addCriterion("coefficient <=", value, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientIn(List<BigDecimal> values) {
            addCriterion("coefficient in", values, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientNotIn(List<BigDecimal> values) {
            addCriterion("coefficient not in", values, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("coefficient between", value1, value2, "coefficient");
            return (Criteria) this;
        }

        public Criteria andCoefficientNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("coefficient not between", value1, value2, "coefficient");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}