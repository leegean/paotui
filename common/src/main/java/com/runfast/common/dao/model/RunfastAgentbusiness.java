package com.runfast.common.dao.model;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
public class RunfastAgentbusiness implements Serializable {
    private Integer id;

    private String areas;

    private Date createTime;

    private String mobile;

    private String name;

    private Integer agentId;

    private String agentName;

    private String cityId;

    private String cityName;

    private String countyId;

    private String countyName;

    private BigDecimal coefficient;

    private String code;

    private String area;

    private String areaId;

    private BigDecimal coefficient2;

    private String managerCode;

    private Integer managerId;

    private String province;

    private String provinceId;

    private Integer aid;

    private Integer cid;

    private String managerName;

    private Integer pid;

    private Integer tid;

    private Integer ishaveup;

    /**
     * 代理商公司注册名
     */
    private String company_name;

    /**
     * 法人名
     */
    private String legal_name;

    /**
     * 法人手机号
     */
    private String legal_mobile;

    /**
     *  法人身份证号
     */
    private String legal_idNumber;

    /**
     * 法人身份证正面照片路径
     */
    private String legal_id_positive_load;

    /**
     * 法人身份证反面路径
     */
    private String legal_id_negative_load;

    /**
     * 统一社会信用代码图片路径
     */
    private String unified_social_credit_code_load;

    /**
     * 管理员名字
     */
    private String admin_name;

    /**
     * 管理员电话
     */
    private String admin_mobile;

    /**
     * 管理员身份证号
     */
    private String admin_idNumber;

    /**
     * 管理员身份证正面路径
     */
    private String admin_id_positive_load;

    /**
     * 管理员身份证反面路径
     */
    private String admin_id_negative_load;

    /**
     * 对公账户名
     */
    private String public_accounts_name;

    /**
     * 银行账户
     */
    private String bank_account;

    /**
     * 开户行（账户为那个银行）
     */
    private String account_bank;

    /**
     * 开户行所在省市
     */
    private String account_bank_province_city;

    /**
     * 支行名称
     */
    private String branch_name;

    /**
     * 开户行联行号
     */
    private Integer account_bank_link_bank_no;

    /**
     * 应缴保证金
     */
    private BigDecimal payable_deposit;

    /**
     *  已缴保证金
     */
    private BigDecimal deposit_paid;

    /**
     * 合同生效时间
     */
    private Date contract_start_time;

    /**
     * 合同到期时间
     */
    private Date contract_end_time;

    /**
     * 代理商等级
     */
    private Integer level;

    /**
     * 办公地址
     */
    private String office_address;

    /**
     * 合同有效期年
     */
    private Integer validity_period_year;

    /**
     * 1区域代理、2上级区域代理普通代理、3上级为公司代理的普通代理
     */
    private Integer flage;

    /**
     * 骑手接单模式：1抢单，2派送
     */
    private Integer order_mode;

    private String showAreas;

    /**
     * 代理商范围
     */
    private String distRange;

    private static final long serialVersionUID = 1L;


    private List<RunfastAgentbusiness> children;
}