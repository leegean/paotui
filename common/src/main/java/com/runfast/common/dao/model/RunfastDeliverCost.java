package com.runfast.common.dao.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class RunfastDeliverCost implements Serializable {
    private Integer id;

    private Double charge;

    private Date createTime;

    private Double noCharge;

    private Double timeCost1;

    private Double timeCost2;

    private Date endTimeDay1;

    private Date endTimeNight2;

    private Date startTimeDay1;

    private Date startTimeNight2;

    private BigDecimal charge1;

    private BigDecimal charge2;

    private BigDecimal maxCharge;

    private Double maxDistance;

    private String area;

    private String areaName;

    private String cityId;

    private String cityName;

    private String countyId;

    private String countyName;

    private Integer isdefault;

    private Integer updatehour;

    private Integer agentId;

    private String agentName;

    private Double speed;

    private String distRange;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public RunfastDeliverCost withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getCharge() {
        return charge;
    }

    public RunfastDeliverCost withCharge(Double charge) {
        this.setCharge(charge);
        return this;
    }

    public void setCharge(Double charge) {
        this.charge = charge;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public RunfastDeliverCost withCreateTime(Date createTime) {
        this.setCreateTime(createTime);
        return this;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Double getNoCharge() {
        return noCharge;
    }

    public RunfastDeliverCost withNoCharge(Double noCharge) {
        this.setNoCharge(noCharge);
        return this;
    }

    public void setNoCharge(Double noCharge) {
        this.noCharge = noCharge;
    }

    public Double getTimeCost1() {
        return timeCost1;
    }

    public RunfastDeliverCost withTimeCost1(Double timeCost1) {
        this.setTimeCost1(timeCost1);
        return this;
    }

    public void setTimeCost1(Double timeCost1) {
        this.timeCost1 = timeCost1;
    }

    public Double getTimeCost2() {
        return timeCost2;
    }

    public RunfastDeliverCost withTimeCost2(Double timeCost2) {
        this.setTimeCost2(timeCost2);
        return this;
    }

    public void setTimeCost2(Double timeCost2) {
        this.timeCost2 = timeCost2;
    }

    public Date getEndTimeDay1() {
        return endTimeDay1;
    }

    public RunfastDeliverCost withEndTimeDay1(Date endTimeDay1) {
        this.setEndTimeDay1(endTimeDay1);
        return this;
    }

    public void setEndTimeDay1(Date endTimeDay1) {
        this.endTimeDay1 = endTimeDay1;
    }

    public Date getEndTimeNight2() {
        return endTimeNight2;
    }

    public RunfastDeliverCost withEndTimeNight2(Date endTimeNight2) {
        this.setEndTimeNight2(endTimeNight2);
        return this;
    }

    public void setEndTimeNight2(Date endTimeNight2) {
        this.endTimeNight2 = endTimeNight2;
    }

    public Date getStartTimeDay1() {
        return startTimeDay1;
    }

    public RunfastDeliverCost withStartTimeDay1(Date startTimeDay1) {
        this.setStartTimeDay1(startTimeDay1);
        return this;
    }

    public void setStartTimeDay1(Date startTimeDay1) {
        this.startTimeDay1 = startTimeDay1;
    }

    public Date getStartTimeNight2() {
        return startTimeNight2;
    }

    public RunfastDeliverCost withStartTimeNight2(Date startTimeNight2) {
        this.setStartTimeNight2(startTimeNight2);
        return this;
    }

    public void setStartTimeNight2(Date startTimeNight2) {
        this.startTimeNight2 = startTimeNight2;
    }

    public BigDecimal getCharge1() {
        return charge1;
    }

    public RunfastDeliverCost withCharge1(BigDecimal charge1) {
        this.setCharge1(charge1);
        return this;
    }

    public void setCharge1(BigDecimal charge1) {
        this.charge1 = charge1;
    }

    public BigDecimal getCharge2() {
        return charge2;
    }

    public RunfastDeliverCost withCharge2(BigDecimal charge2) {
        this.setCharge2(charge2);
        return this;
    }

    public void setCharge2(BigDecimal charge2) {
        this.charge2 = charge2;
    }

    public BigDecimal getMaxCharge() {
        return maxCharge;
    }

    public RunfastDeliverCost withMaxCharge(BigDecimal maxCharge) {
        this.setMaxCharge(maxCharge);
        return this;
    }

    public void setMaxCharge(BigDecimal maxCharge) {
        this.maxCharge = maxCharge;
    }

    public Double getMaxDistance() {
        return maxDistance;
    }

    public RunfastDeliverCost withMaxDistance(Double maxDistance) {
        this.setMaxDistance(maxDistance);
        return this;
    }

    public void setMaxDistance(Double maxDistance) {
        this.maxDistance = maxDistance;
    }

    public String getArea() {
        return area;
    }

    public RunfastDeliverCost withArea(String area) {
        this.setArea(area);
        return this;
    }

    public void setArea(String area) {
        this.area = area == null ? null : area.trim();
    }

    public String getAreaName() {
        return areaName;
    }

    public RunfastDeliverCost withAreaName(String areaName) {
        this.setAreaName(areaName);
        return this;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName == null ? null : areaName.trim();
    }

    public String getCityId() {
        return cityId;
    }

    public RunfastDeliverCost withCityId(String cityId) {
        this.setCityId(cityId);
        return this;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId == null ? null : cityId.trim();
    }

    public String getCityName() {
        return cityName;
    }

    public RunfastDeliverCost withCityName(String cityName) {
        this.setCityName(cityName);
        return this;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName == null ? null : cityName.trim();
    }

    public String getCountyId() {
        return countyId;
    }

    public RunfastDeliverCost withCountyId(String countyId) {
        this.setCountyId(countyId);
        return this;
    }

    public void setCountyId(String countyId) {
        this.countyId = countyId == null ? null : countyId.trim();
    }

    public String getCountyName() {
        return countyName;
    }

    public RunfastDeliverCost withCountyName(String countyName) {
        this.setCountyName(countyName);
        return this;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName == null ? null : countyName.trim();
    }

    public Integer getIsdefault() {
        return isdefault;
    }

    public RunfastDeliverCost withIsdefault(Integer isdefault) {
        this.setIsdefault(isdefault);
        return this;
    }

    public void setIsdefault(Integer isdefault) {
        this.isdefault = isdefault;
    }

    public Integer getUpdatehour() {
        return updatehour;
    }

    public RunfastDeliverCost withUpdatehour(Integer updatehour) {
        this.setUpdatehour(updatehour);
        return this;
    }

    public void setUpdatehour(Integer updatehour) {
        this.updatehour = updatehour;
    }

    public Integer getAgentId() {
        return agentId;
    }

    public RunfastDeliverCost withAgentId(Integer agentId) {
        this.setAgentId(agentId);
        return this;
    }

    public void setAgentId(Integer agentId) {
        this.agentId = agentId;
    }

    public String getAgentName() {
        return agentName;
    }

    public RunfastDeliverCost withAgentName(String agentName) {
        this.setAgentName(agentName);
        return this;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName == null ? null : agentName.trim();
    }

    public Double getSpeed() {
        return speed;
    }

    public RunfastDeliverCost withSpeed(Double speed) {
        this.setSpeed(speed);
        return this;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public String getDistRange() {
        return distRange;
    }

    public RunfastDeliverCost withDistRange(String distRange) {
        this.setDistRange(distRange);
        return this;
    }

    public void setDistRange(String distRange) {
        this.distRange = distRange == null ? null : distRange.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", charge=").append(charge);
        sb.append(", createTime=").append(createTime);
        sb.append(", noCharge=").append(noCharge);
        sb.append(", timeCost1=").append(timeCost1);
        sb.append(", timeCost2=").append(timeCost2);
        sb.append(", endTimeDay1=").append(endTimeDay1);
        sb.append(", endTimeNight2=").append(endTimeNight2);
        sb.append(", startTimeDay1=").append(startTimeDay1);
        sb.append(", startTimeNight2=").append(startTimeNight2);
        sb.append(", charge1=").append(charge1);
        sb.append(", charge2=").append(charge2);
        sb.append(", maxCharge=").append(maxCharge);
        sb.append(", maxDistance=").append(maxDistance);
        sb.append(", area=").append(area);
        sb.append(", areaName=").append(areaName);
        sb.append(", cityId=").append(cityId);
        sb.append(", cityName=").append(cityName);
        sb.append(", countyId=").append(countyId);
        sb.append(", countyName=").append(countyName);
        sb.append(", isdefault=").append(isdefault);
        sb.append(", updatehour=").append(updatehour);
        sb.append(", agentId=").append(agentId);
        sb.append(", agentName=").append(agentName);
        sb.append(", speed=").append(speed);
        sb.append(", distRange=").append(distRange);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}