package com.runfast.common.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.common.dao.model.RunfastCuser;
import com.runfast.common.dao.model.RunfastCuserExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastCuserMapper extends IMapper<RunfastCuser, Integer, RunfastCuserExample> {
    int updateAlias(String alias);
}