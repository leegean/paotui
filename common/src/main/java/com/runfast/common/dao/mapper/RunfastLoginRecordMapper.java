package com.runfast.common.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.common.dao.model.RunfastLoginRecord;
import com.runfast.common.dao.model.RunfastLoginRecordExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastLoginRecordMapper extends IMapper<RunfastLoginRecord, Integer, RunfastLoginRecordExample> {
}