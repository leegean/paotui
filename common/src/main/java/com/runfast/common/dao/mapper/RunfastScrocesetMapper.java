package com.runfast.common.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.common.dao.model.RunfastScroceset;
import com.runfast.common.dao.model.RunfastScrocesetExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastScrocesetMapper extends IMapper<RunfastScroceset, Integer, RunfastScrocesetExample> {
}