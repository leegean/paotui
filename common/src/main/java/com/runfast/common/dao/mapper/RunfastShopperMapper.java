package com.runfast.common.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.common.dao.model.RunfastShopper;
import com.runfast.common.dao.model.RunfastShopperExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastShopperMapper extends IMapper<RunfastShopper, Integer, RunfastShopperExample> {
}