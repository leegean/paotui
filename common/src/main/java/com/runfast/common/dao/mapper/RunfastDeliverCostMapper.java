package com.runfast.common.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.common.dao.model.RunfastDeliverCost;
import com.runfast.common.dao.model.RunfastDeliverCostExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastDeliverCostMapper extends IMapper<RunfastDeliverCost, Integer, RunfastDeliverCostExample> {
}