package com.runfast.common.dao.model;

import com.runfast.common.entity.validate.group.Create;
import com.runfast.common.entity.validate.group.Update;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Data
public class RunfastCuseraddress implements Serializable {
    @NotNull(groups = Update.class)
    private Integer id;
    @NotBlank(groups = Create.class)
    private String address;

    private Integer cid;

    private Date createTime;

    private String mobile;

    @NotBlank(groups = {Create.class,Update.class})
    private String name;

    private String cname;
    @NotBlank(groups = {Create.class,Update.class})
    private String phone;
    @NotBlank(groups = {Create.class,Update.class})
    private String userAddress;

    private Integer isChoose;
    @NotBlank(groups = {Create.class,Update.class})
    private String latitude;
    @NotBlank(groups = {Create.class,Update.class})
    private String longitude;

    private String cityName;

    private String countyName;

    private String provinceName;

    /**
     * 常用地址标签（1：家，2：公司，3：学校）
     */
    private Integer tag;

    /**
     * 0:女；1：男
     */
    private Integer gender;

    private Boolean outRange;

    private static final long serialVersionUID = 1L;

}