package com.runfast.common.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.common.dao.model.RunfastAgentbusiness;
import com.runfast.common.dao.model.RunfastAgentbusinessExample;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface RunfastAgentbusinessMapper extends IMapper<RunfastAgentbusiness, Integer, RunfastAgentbusinessExample> {
    List<RunfastAgentbusiness> findAllAgent();

}