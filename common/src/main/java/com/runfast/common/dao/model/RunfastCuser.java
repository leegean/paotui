package com.runfast.common.dao.model;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class RunfastCuser implements Serializable {
    private Integer id;

    private String areaName;

    private String byRoles;

    private String card;

    private String cityId;

    private String cityName;

    private String code;

    private String countyId;

    private String countyName;

    private Date createTime;

    private Date logTime;

    private String mobile;

    private String name;

    private String nickname;

    private String openid;

    private String orgType;

    private String password;

    private String pic;

    private BigDecimal remainder;

    private String townId;

    private String townName;

    private String provinceId;

    private String qq;

    private String xinl;

    private String email;

    private Integer gender;

    private Double score;

    private BigDecimal minmonety;

    private BigDecimal consume;

    private Integer rnum;

    private BigDecimal totalremainder;

    private String bdpushChannelId;

    private String bdpushUserId;

    private Integer bptype;

    private String otherId;

    private Integer pushType;

    private String alias;

    private String thirdLoginId;

    private Integer thirdLoginType;

    private String miniOpenId;

    private String qqId;

    private String weixinId;

    private static final long serialVersionUID = 1L;

}