package com.runfast.common.dao.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class RunfastCuserExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public RunfastCuserExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andAreaNameIsNull() {
            addCriterion("areaName is null");
            return (Criteria) this;
        }

        public Criteria andAreaNameIsNotNull() {
            addCriterion("areaName is not null");
            return (Criteria) this;
        }

        public Criteria andAreaNameEqualTo(String value) {
            addCriterion("areaName =", value, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameNotEqualTo(String value) {
            addCriterion("areaName <>", value, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameGreaterThan(String value) {
            addCriterion("areaName >", value, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameGreaterThanOrEqualTo(String value) {
            addCriterion("areaName >=", value, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameLessThan(String value) {
            addCriterion("areaName <", value, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameLessThanOrEqualTo(String value) {
            addCriterion("areaName <=", value, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameLike(String value) {
            addCriterion("areaName like", value, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameNotLike(String value) {
            addCriterion("areaName not like", value, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameIn(List<String> values) {
            addCriterion("areaName in", values, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameNotIn(List<String> values) {
            addCriterion("areaName not in", values, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameBetween(String value1, String value2) {
            addCriterion("areaName between", value1, value2, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameNotBetween(String value1, String value2) {
            addCriterion("areaName not between", value1, value2, "areaName");
            return (Criteria) this;
        }

        public Criteria andByRolesIsNull() {
            addCriterion("byRoles is null");
            return (Criteria) this;
        }

        public Criteria andByRolesIsNotNull() {
            addCriterion("byRoles is not null");
            return (Criteria) this;
        }

        public Criteria andByRolesEqualTo(String value) {
            addCriterion("byRoles =", value, "byRoles");
            return (Criteria) this;
        }

        public Criteria andByRolesNotEqualTo(String value) {
            addCriterion("byRoles <>", value, "byRoles");
            return (Criteria) this;
        }

        public Criteria andByRolesGreaterThan(String value) {
            addCriterion("byRoles >", value, "byRoles");
            return (Criteria) this;
        }

        public Criteria andByRolesGreaterThanOrEqualTo(String value) {
            addCriterion("byRoles >=", value, "byRoles");
            return (Criteria) this;
        }

        public Criteria andByRolesLessThan(String value) {
            addCriterion("byRoles <", value, "byRoles");
            return (Criteria) this;
        }

        public Criteria andByRolesLessThanOrEqualTo(String value) {
            addCriterion("byRoles <=", value, "byRoles");
            return (Criteria) this;
        }

        public Criteria andByRolesLike(String value) {
            addCriterion("byRoles like", value, "byRoles");
            return (Criteria) this;
        }

        public Criteria andByRolesNotLike(String value) {
            addCriterion("byRoles not like", value, "byRoles");
            return (Criteria) this;
        }

        public Criteria andByRolesIn(List<String> values) {
            addCriterion("byRoles in", values, "byRoles");
            return (Criteria) this;
        }

        public Criteria andByRolesNotIn(List<String> values) {
            addCriterion("byRoles not in", values, "byRoles");
            return (Criteria) this;
        }

        public Criteria andByRolesBetween(String value1, String value2) {
            addCriterion("byRoles between", value1, value2, "byRoles");
            return (Criteria) this;
        }

        public Criteria andByRolesNotBetween(String value1, String value2) {
            addCriterion("byRoles not between", value1, value2, "byRoles");
            return (Criteria) this;
        }

        public Criteria andCardIsNull() {
            addCriterion("card is null");
            return (Criteria) this;
        }

        public Criteria andCardIsNotNull() {
            addCriterion("card is not null");
            return (Criteria) this;
        }

        public Criteria andCardEqualTo(String value) {
            addCriterion("card =", value, "card");
            return (Criteria) this;
        }

        public Criteria andCardNotEqualTo(String value) {
            addCriterion("card <>", value, "card");
            return (Criteria) this;
        }

        public Criteria andCardGreaterThan(String value) {
            addCriterion("card >", value, "card");
            return (Criteria) this;
        }

        public Criteria andCardGreaterThanOrEqualTo(String value) {
            addCriterion("card >=", value, "card");
            return (Criteria) this;
        }

        public Criteria andCardLessThan(String value) {
            addCriterion("card <", value, "card");
            return (Criteria) this;
        }

        public Criteria andCardLessThanOrEqualTo(String value) {
            addCriterion("card <=", value, "card");
            return (Criteria) this;
        }

        public Criteria andCardLike(String value) {
            addCriterion("card like", value, "card");
            return (Criteria) this;
        }

        public Criteria andCardNotLike(String value) {
            addCriterion("card not like", value, "card");
            return (Criteria) this;
        }

        public Criteria andCardIn(List<String> values) {
            addCriterion("card in", values, "card");
            return (Criteria) this;
        }

        public Criteria andCardNotIn(List<String> values) {
            addCriterion("card not in", values, "card");
            return (Criteria) this;
        }

        public Criteria andCardBetween(String value1, String value2) {
            addCriterion("card between", value1, value2, "card");
            return (Criteria) this;
        }

        public Criteria andCardNotBetween(String value1, String value2) {
            addCriterion("card not between", value1, value2, "card");
            return (Criteria) this;
        }

        public Criteria andCityIdIsNull() {
            addCriterion("cityId is null");
            return (Criteria) this;
        }

        public Criteria andCityIdIsNotNull() {
            addCriterion("cityId is not null");
            return (Criteria) this;
        }

        public Criteria andCityIdEqualTo(String value) {
            addCriterion("cityId =", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdNotEqualTo(String value) {
            addCriterion("cityId <>", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdGreaterThan(String value) {
            addCriterion("cityId >", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdGreaterThanOrEqualTo(String value) {
            addCriterion("cityId >=", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdLessThan(String value) {
            addCriterion("cityId <", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdLessThanOrEqualTo(String value) {
            addCriterion("cityId <=", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdLike(String value) {
            addCriterion("cityId like", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdNotLike(String value) {
            addCriterion("cityId not like", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdIn(List<String> values) {
            addCriterion("cityId in", values, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdNotIn(List<String> values) {
            addCriterion("cityId not in", values, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdBetween(String value1, String value2) {
            addCriterion("cityId between", value1, value2, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdNotBetween(String value1, String value2) {
            addCriterion("cityId not between", value1, value2, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityNameIsNull() {
            addCriterion("cityName is null");
            return (Criteria) this;
        }

        public Criteria andCityNameIsNotNull() {
            addCriterion("cityName is not null");
            return (Criteria) this;
        }

        public Criteria andCityNameEqualTo(String value) {
            addCriterion("cityName =", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameNotEqualTo(String value) {
            addCriterion("cityName <>", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameGreaterThan(String value) {
            addCriterion("cityName >", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameGreaterThanOrEqualTo(String value) {
            addCriterion("cityName >=", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameLessThan(String value) {
            addCriterion("cityName <", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameLessThanOrEqualTo(String value) {
            addCriterion("cityName <=", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameLike(String value) {
            addCriterion("cityName like", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameNotLike(String value) {
            addCriterion("cityName not like", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameIn(List<String> values) {
            addCriterion("cityName in", values, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameNotIn(List<String> values) {
            addCriterion("cityName not in", values, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameBetween(String value1, String value2) {
            addCriterion("cityName between", value1, value2, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameNotBetween(String value1, String value2) {
            addCriterion("cityName not between", value1, value2, "cityName");
            return (Criteria) this;
        }

        public Criteria andCodeIsNull() {
            addCriterion("code is null");
            return (Criteria) this;
        }

        public Criteria andCodeIsNotNull() {
            addCriterion("code is not null");
            return (Criteria) this;
        }

        public Criteria andCodeEqualTo(String value) {
            addCriterion("code =", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeNotEqualTo(String value) {
            addCriterion("code <>", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeGreaterThan(String value) {
            addCriterion("code >", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeGreaterThanOrEqualTo(String value) {
            addCriterion("code >=", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeLessThan(String value) {
            addCriterion("code <", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeLessThanOrEqualTo(String value) {
            addCriterion("code <=", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeLike(String value) {
            addCriterion("code like", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeNotLike(String value) {
            addCriterion("code not like", value, "code");
            return (Criteria) this;
        }

        public Criteria andCodeIn(List<String> values) {
            addCriterion("code in", values, "code");
            return (Criteria) this;
        }

        public Criteria andCodeNotIn(List<String> values) {
            addCriterion("code not in", values, "code");
            return (Criteria) this;
        }

        public Criteria andCodeBetween(String value1, String value2) {
            addCriterion("code between", value1, value2, "code");
            return (Criteria) this;
        }

        public Criteria andCodeNotBetween(String value1, String value2) {
            addCriterion("code not between", value1, value2, "code");
            return (Criteria) this;
        }

        public Criteria andCountyIdIsNull() {
            addCriterion("countyId is null");
            return (Criteria) this;
        }

        public Criteria andCountyIdIsNotNull() {
            addCriterion("countyId is not null");
            return (Criteria) this;
        }

        public Criteria andCountyIdEqualTo(String value) {
            addCriterion("countyId =", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdNotEqualTo(String value) {
            addCriterion("countyId <>", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdGreaterThan(String value) {
            addCriterion("countyId >", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdGreaterThanOrEqualTo(String value) {
            addCriterion("countyId >=", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdLessThan(String value) {
            addCriterion("countyId <", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdLessThanOrEqualTo(String value) {
            addCriterion("countyId <=", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdLike(String value) {
            addCriterion("countyId like", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdNotLike(String value) {
            addCriterion("countyId not like", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdIn(List<String> values) {
            addCriterion("countyId in", values, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdNotIn(List<String> values) {
            addCriterion("countyId not in", values, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdBetween(String value1, String value2) {
            addCriterion("countyId between", value1, value2, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdNotBetween(String value1, String value2) {
            addCriterion("countyId not between", value1, value2, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyNameIsNull() {
            addCriterion("countyName is null");
            return (Criteria) this;
        }

        public Criteria andCountyNameIsNotNull() {
            addCriterion("countyName is not null");
            return (Criteria) this;
        }

        public Criteria andCountyNameEqualTo(String value) {
            addCriterion("countyName =", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameNotEqualTo(String value) {
            addCriterion("countyName <>", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameGreaterThan(String value) {
            addCriterion("countyName >", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameGreaterThanOrEqualTo(String value) {
            addCriterion("countyName >=", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameLessThan(String value) {
            addCriterion("countyName <", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameLessThanOrEqualTo(String value) {
            addCriterion("countyName <=", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameLike(String value) {
            addCriterion("countyName like", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameNotLike(String value) {
            addCriterion("countyName not like", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameIn(List<String> values) {
            addCriterion("countyName in", values, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameNotIn(List<String> values) {
            addCriterion("countyName not in", values, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameBetween(String value1, String value2) {
            addCriterion("countyName between", value1, value2, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameNotBetween(String value1, String value2) {
            addCriterion("countyName not between", value1, value2, "countyName");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("createTime is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("createTime is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("createTime =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("createTime <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("createTime >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("createTime >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("createTime <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("createTime <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("createTime in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("createTime not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("createTime between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("createTime not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andLogTimeIsNull() {
            addCriterion("logTime is null");
            return (Criteria) this;
        }

        public Criteria andLogTimeIsNotNull() {
            addCriterion("logTime is not null");
            return (Criteria) this;
        }

        public Criteria andLogTimeEqualTo(Date value) {
            addCriterion("logTime =", value, "logTime");
            return (Criteria) this;
        }

        public Criteria andLogTimeNotEqualTo(Date value) {
            addCriterion("logTime <>", value, "logTime");
            return (Criteria) this;
        }

        public Criteria andLogTimeGreaterThan(Date value) {
            addCriterion("logTime >", value, "logTime");
            return (Criteria) this;
        }

        public Criteria andLogTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("logTime >=", value, "logTime");
            return (Criteria) this;
        }

        public Criteria andLogTimeLessThan(Date value) {
            addCriterion("logTime <", value, "logTime");
            return (Criteria) this;
        }

        public Criteria andLogTimeLessThanOrEqualTo(Date value) {
            addCriterion("logTime <=", value, "logTime");
            return (Criteria) this;
        }

        public Criteria andLogTimeIn(List<Date> values) {
            addCriterion("logTime in", values, "logTime");
            return (Criteria) this;
        }

        public Criteria andLogTimeNotIn(List<Date> values) {
            addCriterion("logTime not in", values, "logTime");
            return (Criteria) this;
        }

        public Criteria andLogTimeBetween(Date value1, Date value2) {
            addCriterion("logTime between", value1, value2, "logTime");
            return (Criteria) this;
        }

        public Criteria andLogTimeNotBetween(Date value1, Date value2) {
            addCriterion("logTime not between", value1, value2, "logTime");
            return (Criteria) this;
        }

        public Criteria andMobileIsNull() {
            addCriterion("mobile is null");
            return (Criteria) this;
        }

        public Criteria andMobileIsNotNull() {
            addCriterion("mobile is not null");
            return (Criteria) this;
        }

        public Criteria andMobileEqualTo(String value) {
            addCriterion("mobile =", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotEqualTo(String value) {
            addCriterion("mobile <>", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileGreaterThan(String value) {
            addCriterion("mobile >", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileGreaterThanOrEqualTo(String value) {
            addCriterion("mobile >=", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLessThan(String value) {
            addCriterion("mobile <", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLessThanOrEqualTo(String value) {
            addCriterion("mobile <=", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLike(String value) {
            addCriterion("mobile like", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotLike(String value) {
            addCriterion("mobile not like", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileIn(List<String> values) {
            addCriterion("mobile in", values, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotIn(List<String> values) {
            addCriterion("mobile not in", values, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileBetween(String value1, String value2) {
            addCriterion("mobile between", value1, value2, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotBetween(String value1, String value2) {
            addCriterion("mobile not between", value1, value2, "mobile");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNicknameIsNull() {
            addCriterion("nickname is null");
            return (Criteria) this;
        }

        public Criteria andNicknameIsNotNull() {
            addCriterion("nickname is not null");
            return (Criteria) this;
        }

        public Criteria andNicknameEqualTo(String value) {
            addCriterion("nickname =", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameNotEqualTo(String value) {
            addCriterion("nickname <>", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameGreaterThan(String value) {
            addCriterion("nickname >", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameGreaterThanOrEqualTo(String value) {
            addCriterion("nickname >=", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameLessThan(String value) {
            addCriterion("nickname <", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameLessThanOrEqualTo(String value) {
            addCriterion("nickname <=", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameLike(String value) {
            addCriterion("nickname like", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameNotLike(String value) {
            addCriterion("nickname not like", value, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameIn(List<String> values) {
            addCriterion("nickname in", values, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameNotIn(List<String> values) {
            addCriterion("nickname not in", values, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameBetween(String value1, String value2) {
            addCriterion("nickname between", value1, value2, "nickname");
            return (Criteria) this;
        }

        public Criteria andNicknameNotBetween(String value1, String value2) {
            addCriterion("nickname not between", value1, value2, "nickname");
            return (Criteria) this;
        }

        public Criteria andOpenidIsNull() {
            addCriterion("openid is null");
            return (Criteria) this;
        }

        public Criteria andOpenidIsNotNull() {
            addCriterion("openid is not null");
            return (Criteria) this;
        }

        public Criteria andOpenidEqualTo(String value) {
            addCriterion("openid =", value, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidNotEqualTo(String value) {
            addCriterion("openid <>", value, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidGreaterThan(String value) {
            addCriterion("openid >", value, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidGreaterThanOrEqualTo(String value) {
            addCriterion("openid >=", value, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidLessThan(String value) {
            addCriterion("openid <", value, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidLessThanOrEqualTo(String value) {
            addCriterion("openid <=", value, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidLike(String value) {
            addCriterion("openid like", value, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidNotLike(String value) {
            addCriterion("openid not like", value, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidIn(List<String> values) {
            addCriterion("openid in", values, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidNotIn(List<String> values) {
            addCriterion("openid not in", values, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidBetween(String value1, String value2) {
            addCriterion("openid between", value1, value2, "openid");
            return (Criteria) this;
        }

        public Criteria andOpenidNotBetween(String value1, String value2) {
            addCriterion("openid not between", value1, value2, "openid");
            return (Criteria) this;
        }

        public Criteria andOrgTypeIsNull() {
            addCriterion("orgType is null");
            return (Criteria) this;
        }

        public Criteria andOrgTypeIsNotNull() {
            addCriterion("orgType is not null");
            return (Criteria) this;
        }

        public Criteria andOrgTypeEqualTo(String value) {
            addCriterion("orgType =", value, "orgType");
            return (Criteria) this;
        }

        public Criteria andOrgTypeNotEqualTo(String value) {
            addCriterion("orgType <>", value, "orgType");
            return (Criteria) this;
        }

        public Criteria andOrgTypeGreaterThan(String value) {
            addCriterion("orgType >", value, "orgType");
            return (Criteria) this;
        }

        public Criteria andOrgTypeGreaterThanOrEqualTo(String value) {
            addCriterion("orgType >=", value, "orgType");
            return (Criteria) this;
        }

        public Criteria andOrgTypeLessThan(String value) {
            addCriterion("orgType <", value, "orgType");
            return (Criteria) this;
        }

        public Criteria andOrgTypeLessThanOrEqualTo(String value) {
            addCriterion("orgType <=", value, "orgType");
            return (Criteria) this;
        }

        public Criteria andOrgTypeLike(String value) {
            addCriterion("orgType like", value, "orgType");
            return (Criteria) this;
        }

        public Criteria andOrgTypeNotLike(String value) {
            addCriterion("orgType not like", value, "orgType");
            return (Criteria) this;
        }

        public Criteria andOrgTypeIn(List<String> values) {
            addCriterion("orgType in", values, "orgType");
            return (Criteria) this;
        }

        public Criteria andOrgTypeNotIn(List<String> values) {
            addCriterion("orgType not in", values, "orgType");
            return (Criteria) this;
        }

        public Criteria andOrgTypeBetween(String value1, String value2) {
            addCriterion("orgType between", value1, value2, "orgType");
            return (Criteria) this;
        }

        public Criteria andOrgTypeNotBetween(String value1, String value2) {
            addCriterion("orgType not between", value1, value2, "orgType");
            return (Criteria) this;
        }

        public Criteria andPasswordIsNull() {
            addCriterion("password is null");
            return (Criteria) this;
        }

        public Criteria andPasswordIsNotNull() {
            addCriterion("password is not null");
            return (Criteria) this;
        }

        public Criteria andPasswordEqualTo(String value) {
            addCriterion("password =", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotEqualTo(String value) {
            addCriterion("password <>", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordGreaterThan(String value) {
            addCriterion("password >", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordGreaterThanOrEqualTo(String value) {
            addCriterion("password >=", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLessThan(String value) {
            addCriterion("password <", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLessThanOrEqualTo(String value) {
            addCriterion("password <=", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLike(String value) {
            addCriterion("password like", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotLike(String value) {
            addCriterion("password not like", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordIn(List<String> values) {
            addCriterion("password in", values, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotIn(List<String> values) {
            addCriterion("password not in", values, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordBetween(String value1, String value2) {
            addCriterion("password between", value1, value2, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotBetween(String value1, String value2) {
            addCriterion("password not between", value1, value2, "password");
            return (Criteria) this;
        }

        public Criteria andPicIsNull() {
            addCriterion("pic is null");
            return (Criteria) this;
        }

        public Criteria andPicIsNotNull() {
            addCriterion("pic is not null");
            return (Criteria) this;
        }

        public Criteria andPicEqualTo(String value) {
            addCriterion("pic =", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicNotEqualTo(String value) {
            addCriterion("pic <>", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicGreaterThan(String value) {
            addCriterion("pic >", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicGreaterThanOrEqualTo(String value) {
            addCriterion("pic >=", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicLessThan(String value) {
            addCriterion("pic <", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicLessThanOrEqualTo(String value) {
            addCriterion("pic <=", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicLike(String value) {
            addCriterion("pic like", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicNotLike(String value) {
            addCriterion("pic not like", value, "pic");
            return (Criteria) this;
        }

        public Criteria andPicIn(List<String> values) {
            addCriterion("pic in", values, "pic");
            return (Criteria) this;
        }

        public Criteria andPicNotIn(List<String> values) {
            addCriterion("pic not in", values, "pic");
            return (Criteria) this;
        }

        public Criteria andPicBetween(String value1, String value2) {
            addCriterion("pic between", value1, value2, "pic");
            return (Criteria) this;
        }

        public Criteria andPicNotBetween(String value1, String value2) {
            addCriterion("pic not between", value1, value2, "pic");
            return (Criteria) this;
        }

        public Criteria andRemainderIsNull() {
            addCriterion("remainder is null");
            return (Criteria) this;
        }

        public Criteria andRemainderIsNotNull() {
            addCriterion("remainder is not null");
            return (Criteria) this;
        }

        public Criteria andRemainderEqualTo(BigDecimal value) {
            addCriterion("remainder =", value, "remainder");
            return (Criteria) this;
        }

        public Criteria andRemainderNotEqualTo(BigDecimal value) {
            addCriterion("remainder <>", value, "remainder");
            return (Criteria) this;
        }

        public Criteria andRemainderGreaterThan(BigDecimal value) {
            addCriterion("remainder >", value, "remainder");
            return (Criteria) this;
        }

        public Criteria andRemainderGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("remainder >=", value, "remainder");
            return (Criteria) this;
        }

        public Criteria andRemainderLessThan(BigDecimal value) {
            addCriterion("remainder <", value, "remainder");
            return (Criteria) this;
        }

        public Criteria andRemainderLessThanOrEqualTo(BigDecimal value) {
            addCriterion("remainder <=", value, "remainder");
            return (Criteria) this;
        }

        public Criteria andRemainderIn(List<BigDecimal> values) {
            addCriterion("remainder in", values, "remainder");
            return (Criteria) this;
        }

        public Criteria andRemainderNotIn(List<BigDecimal> values) {
            addCriterion("remainder not in", values, "remainder");
            return (Criteria) this;
        }

        public Criteria andRemainderBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("remainder between", value1, value2, "remainder");
            return (Criteria) this;
        }

        public Criteria andRemainderNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("remainder not between", value1, value2, "remainder");
            return (Criteria) this;
        }

        public Criteria andTownIdIsNull() {
            addCriterion("townId is null");
            return (Criteria) this;
        }

        public Criteria andTownIdIsNotNull() {
            addCriterion("townId is not null");
            return (Criteria) this;
        }

        public Criteria andTownIdEqualTo(String value) {
            addCriterion("townId =", value, "townId");
            return (Criteria) this;
        }

        public Criteria andTownIdNotEqualTo(String value) {
            addCriterion("townId <>", value, "townId");
            return (Criteria) this;
        }

        public Criteria andTownIdGreaterThan(String value) {
            addCriterion("townId >", value, "townId");
            return (Criteria) this;
        }

        public Criteria andTownIdGreaterThanOrEqualTo(String value) {
            addCriterion("townId >=", value, "townId");
            return (Criteria) this;
        }

        public Criteria andTownIdLessThan(String value) {
            addCriterion("townId <", value, "townId");
            return (Criteria) this;
        }

        public Criteria andTownIdLessThanOrEqualTo(String value) {
            addCriterion("townId <=", value, "townId");
            return (Criteria) this;
        }

        public Criteria andTownIdLike(String value) {
            addCriterion("townId like", value, "townId");
            return (Criteria) this;
        }

        public Criteria andTownIdNotLike(String value) {
            addCriterion("townId not like", value, "townId");
            return (Criteria) this;
        }

        public Criteria andTownIdIn(List<String> values) {
            addCriterion("townId in", values, "townId");
            return (Criteria) this;
        }

        public Criteria andTownIdNotIn(List<String> values) {
            addCriterion("townId not in", values, "townId");
            return (Criteria) this;
        }

        public Criteria andTownIdBetween(String value1, String value2) {
            addCriterion("townId between", value1, value2, "townId");
            return (Criteria) this;
        }

        public Criteria andTownIdNotBetween(String value1, String value2) {
            addCriterion("townId not between", value1, value2, "townId");
            return (Criteria) this;
        }

        public Criteria andTownNameIsNull() {
            addCriterion("townName is null");
            return (Criteria) this;
        }

        public Criteria andTownNameIsNotNull() {
            addCriterion("townName is not null");
            return (Criteria) this;
        }

        public Criteria andTownNameEqualTo(String value) {
            addCriterion("townName =", value, "townName");
            return (Criteria) this;
        }

        public Criteria andTownNameNotEqualTo(String value) {
            addCriterion("townName <>", value, "townName");
            return (Criteria) this;
        }

        public Criteria andTownNameGreaterThan(String value) {
            addCriterion("townName >", value, "townName");
            return (Criteria) this;
        }

        public Criteria andTownNameGreaterThanOrEqualTo(String value) {
            addCriterion("townName >=", value, "townName");
            return (Criteria) this;
        }

        public Criteria andTownNameLessThan(String value) {
            addCriterion("townName <", value, "townName");
            return (Criteria) this;
        }

        public Criteria andTownNameLessThanOrEqualTo(String value) {
            addCriterion("townName <=", value, "townName");
            return (Criteria) this;
        }

        public Criteria andTownNameLike(String value) {
            addCriterion("townName like", value, "townName");
            return (Criteria) this;
        }

        public Criteria andTownNameNotLike(String value) {
            addCriterion("townName not like", value, "townName");
            return (Criteria) this;
        }

        public Criteria andTownNameIn(List<String> values) {
            addCriterion("townName in", values, "townName");
            return (Criteria) this;
        }

        public Criteria andTownNameNotIn(List<String> values) {
            addCriterion("townName not in", values, "townName");
            return (Criteria) this;
        }

        public Criteria andTownNameBetween(String value1, String value2) {
            addCriterion("townName between", value1, value2, "townName");
            return (Criteria) this;
        }

        public Criteria andTownNameNotBetween(String value1, String value2) {
            addCriterion("townName not between", value1, value2, "townName");
            return (Criteria) this;
        }

        public Criteria andProvinceIdIsNull() {
            addCriterion("provinceId is null");
            return (Criteria) this;
        }

        public Criteria andProvinceIdIsNotNull() {
            addCriterion("provinceId is not null");
            return (Criteria) this;
        }

        public Criteria andProvinceIdEqualTo(String value) {
            addCriterion("provinceId =", value, "provinceId");
            return (Criteria) this;
        }

        public Criteria andProvinceIdNotEqualTo(String value) {
            addCriterion("provinceId <>", value, "provinceId");
            return (Criteria) this;
        }

        public Criteria andProvinceIdGreaterThan(String value) {
            addCriterion("provinceId >", value, "provinceId");
            return (Criteria) this;
        }

        public Criteria andProvinceIdGreaterThanOrEqualTo(String value) {
            addCriterion("provinceId >=", value, "provinceId");
            return (Criteria) this;
        }

        public Criteria andProvinceIdLessThan(String value) {
            addCriterion("provinceId <", value, "provinceId");
            return (Criteria) this;
        }

        public Criteria andProvinceIdLessThanOrEqualTo(String value) {
            addCriterion("provinceId <=", value, "provinceId");
            return (Criteria) this;
        }

        public Criteria andProvinceIdLike(String value) {
            addCriterion("provinceId like", value, "provinceId");
            return (Criteria) this;
        }

        public Criteria andProvinceIdNotLike(String value) {
            addCriterion("provinceId not like", value, "provinceId");
            return (Criteria) this;
        }

        public Criteria andProvinceIdIn(List<String> values) {
            addCriterion("provinceId in", values, "provinceId");
            return (Criteria) this;
        }

        public Criteria andProvinceIdNotIn(List<String> values) {
            addCriterion("provinceId not in", values, "provinceId");
            return (Criteria) this;
        }

        public Criteria andProvinceIdBetween(String value1, String value2) {
            addCriterion("provinceId between", value1, value2, "provinceId");
            return (Criteria) this;
        }

        public Criteria andProvinceIdNotBetween(String value1, String value2) {
            addCriterion("provinceId not between", value1, value2, "provinceId");
            return (Criteria) this;
        }

        public Criteria andQqIsNull() {
            addCriterion("qq is null");
            return (Criteria) this;
        }

        public Criteria andQqIsNotNull() {
            addCriterion("qq is not null");
            return (Criteria) this;
        }

        public Criteria andQqEqualTo(String value) {
            addCriterion("qq =", value, "qq");
            return (Criteria) this;
        }

        public Criteria andQqNotEqualTo(String value) {
            addCriterion("qq <>", value, "qq");
            return (Criteria) this;
        }

        public Criteria andQqGreaterThan(String value) {
            addCriterion("qq >", value, "qq");
            return (Criteria) this;
        }

        public Criteria andQqGreaterThanOrEqualTo(String value) {
            addCriterion("qq >=", value, "qq");
            return (Criteria) this;
        }

        public Criteria andQqLessThan(String value) {
            addCriterion("qq <", value, "qq");
            return (Criteria) this;
        }

        public Criteria andQqLessThanOrEqualTo(String value) {
            addCriterion("qq <=", value, "qq");
            return (Criteria) this;
        }

        public Criteria andQqLike(String value) {
            addCriterion("qq like", value, "qq");
            return (Criteria) this;
        }

        public Criteria andQqNotLike(String value) {
            addCriterion("qq not like", value, "qq");
            return (Criteria) this;
        }

        public Criteria andQqIn(List<String> values) {
            addCriterion("qq in", values, "qq");
            return (Criteria) this;
        }

        public Criteria andQqNotIn(List<String> values) {
            addCriterion("qq not in", values, "qq");
            return (Criteria) this;
        }

        public Criteria andQqBetween(String value1, String value2) {
            addCriterion("qq between", value1, value2, "qq");
            return (Criteria) this;
        }

        public Criteria andQqNotBetween(String value1, String value2) {
            addCriterion("qq not between", value1, value2, "qq");
            return (Criteria) this;
        }

        public Criteria andXinlIsNull() {
            addCriterion("xinl is null");
            return (Criteria) this;
        }

        public Criteria andXinlIsNotNull() {
            addCriterion("xinl is not null");
            return (Criteria) this;
        }

        public Criteria andXinlEqualTo(String value) {
            addCriterion("xinl =", value, "xinl");
            return (Criteria) this;
        }

        public Criteria andXinlNotEqualTo(String value) {
            addCriterion("xinl <>", value, "xinl");
            return (Criteria) this;
        }

        public Criteria andXinlGreaterThan(String value) {
            addCriterion("xinl >", value, "xinl");
            return (Criteria) this;
        }

        public Criteria andXinlGreaterThanOrEqualTo(String value) {
            addCriterion("xinl >=", value, "xinl");
            return (Criteria) this;
        }

        public Criteria andXinlLessThan(String value) {
            addCriterion("xinl <", value, "xinl");
            return (Criteria) this;
        }

        public Criteria andXinlLessThanOrEqualTo(String value) {
            addCriterion("xinl <=", value, "xinl");
            return (Criteria) this;
        }

        public Criteria andXinlLike(String value) {
            addCriterion("xinl like", value, "xinl");
            return (Criteria) this;
        }

        public Criteria andXinlNotLike(String value) {
            addCriterion("xinl not like", value, "xinl");
            return (Criteria) this;
        }

        public Criteria andXinlIn(List<String> values) {
            addCriterion("xinl in", values, "xinl");
            return (Criteria) this;
        }

        public Criteria andXinlNotIn(List<String> values) {
            addCriterion("xinl not in", values, "xinl");
            return (Criteria) this;
        }

        public Criteria andXinlBetween(String value1, String value2) {
            addCriterion("xinl between", value1, value2, "xinl");
            return (Criteria) this;
        }

        public Criteria andXinlNotBetween(String value1, String value2) {
            addCriterion("xinl not between", value1, value2, "xinl");
            return (Criteria) this;
        }

        public Criteria andEmailIsNull() {
            addCriterion("email is null");
            return (Criteria) this;
        }

        public Criteria andEmailIsNotNull() {
            addCriterion("email is not null");
            return (Criteria) this;
        }

        public Criteria andEmailEqualTo(String value) {
            addCriterion("email =", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotEqualTo(String value) {
            addCriterion("email <>", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThan(String value) {
            addCriterion("email >", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThanOrEqualTo(String value) {
            addCriterion("email >=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThan(String value) {
            addCriterion("email <", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThanOrEqualTo(String value) {
            addCriterion("email <=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLike(String value) {
            addCriterion("email like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotLike(String value) {
            addCriterion("email not like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailIn(List<String> values) {
            addCriterion("email in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotIn(List<String> values) {
            addCriterion("email not in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailBetween(String value1, String value2) {
            addCriterion("email between", value1, value2, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotBetween(String value1, String value2) {
            addCriterion("email not between", value1, value2, "email");
            return (Criteria) this;
        }

        public Criteria andGenderIsNull() {
            addCriterion("gender is null");
            return (Criteria) this;
        }

        public Criteria andGenderIsNotNull() {
            addCriterion("gender is not null");
            return (Criteria) this;
        }

        public Criteria andGenderEqualTo(Integer value) {
            addCriterion("gender =", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderNotEqualTo(Integer value) {
            addCriterion("gender <>", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderGreaterThan(Integer value) {
            addCriterion("gender >", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderGreaterThanOrEqualTo(Integer value) {
            addCriterion("gender >=", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderLessThan(Integer value) {
            addCriterion("gender <", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderLessThanOrEqualTo(Integer value) {
            addCriterion("gender <=", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderIn(List<Integer> values) {
            addCriterion("gender in", values, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderNotIn(List<Integer> values) {
            addCriterion("gender not in", values, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderBetween(Integer value1, Integer value2) {
            addCriterion("gender between", value1, value2, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderNotBetween(Integer value1, Integer value2) {
            addCriterion("gender not between", value1, value2, "gender");
            return (Criteria) this;
        }

        public Criteria andScoreIsNull() {
            addCriterion("score is null");
            return (Criteria) this;
        }

        public Criteria andScoreIsNotNull() {
            addCriterion("score is not null");
            return (Criteria) this;
        }

        public Criteria andScoreEqualTo(Double value) {
            addCriterion("score =", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotEqualTo(Double value) {
            addCriterion("score <>", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreGreaterThan(Double value) {
            addCriterion("score >", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreGreaterThanOrEqualTo(Double value) {
            addCriterion("score >=", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreLessThan(Double value) {
            addCriterion("score <", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreLessThanOrEqualTo(Double value) {
            addCriterion("score <=", value, "score");
            return (Criteria) this;
        }

        public Criteria andScoreIn(List<Double> values) {
            addCriterion("score in", values, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotIn(List<Double> values) {
            addCriterion("score not in", values, "score");
            return (Criteria) this;
        }

        public Criteria andScoreBetween(Double value1, Double value2) {
            addCriterion("score between", value1, value2, "score");
            return (Criteria) this;
        }

        public Criteria andScoreNotBetween(Double value1, Double value2) {
            addCriterion("score not between", value1, value2, "score");
            return (Criteria) this;
        }

        public Criteria andMinmonetyIsNull() {
            addCriterion("minmonety is null");
            return (Criteria) this;
        }

        public Criteria andMinmonetyIsNotNull() {
            addCriterion("minmonety is not null");
            return (Criteria) this;
        }

        public Criteria andMinmonetyEqualTo(BigDecimal value) {
            addCriterion("minmonety =", value, "minmonety");
            return (Criteria) this;
        }

        public Criteria andMinmonetyNotEqualTo(BigDecimal value) {
            addCriterion("minmonety <>", value, "minmonety");
            return (Criteria) this;
        }

        public Criteria andMinmonetyGreaterThan(BigDecimal value) {
            addCriterion("minmonety >", value, "minmonety");
            return (Criteria) this;
        }

        public Criteria andMinmonetyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("minmonety >=", value, "minmonety");
            return (Criteria) this;
        }

        public Criteria andMinmonetyLessThan(BigDecimal value) {
            addCriterion("minmonety <", value, "minmonety");
            return (Criteria) this;
        }

        public Criteria andMinmonetyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("minmonety <=", value, "minmonety");
            return (Criteria) this;
        }

        public Criteria andMinmonetyIn(List<BigDecimal> values) {
            addCriterion("minmonety in", values, "minmonety");
            return (Criteria) this;
        }

        public Criteria andMinmonetyNotIn(List<BigDecimal> values) {
            addCriterion("minmonety not in", values, "minmonety");
            return (Criteria) this;
        }

        public Criteria andMinmonetyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("minmonety between", value1, value2, "minmonety");
            return (Criteria) this;
        }

        public Criteria andMinmonetyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("minmonety not between", value1, value2, "minmonety");
            return (Criteria) this;
        }

        public Criteria andConsumeIsNull() {
            addCriterion("consume is null");
            return (Criteria) this;
        }

        public Criteria andConsumeIsNotNull() {
            addCriterion("consume is not null");
            return (Criteria) this;
        }

        public Criteria andConsumeEqualTo(BigDecimal value) {
            addCriterion("consume =", value, "consume");
            return (Criteria) this;
        }

        public Criteria andConsumeNotEqualTo(BigDecimal value) {
            addCriterion("consume <>", value, "consume");
            return (Criteria) this;
        }

        public Criteria andConsumeGreaterThan(BigDecimal value) {
            addCriterion("consume >", value, "consume");
            return (Criteria) this;
        }

        public Criteria andConsumeGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("consume >=", value, "consume");
            return (Criteria) this;
        }

        public Criteria andConsumeLessThan(BigDecimal value) {
            addCriterion("consume <", value, "consume");
            return (Criteria) this;
        }

        public Criteria andConsumeLessThanOrEqualTo(BigDecimal value) {
            addCriterion("consume <=", value, "consume");
            return (Criteria) this;
        }

        public Criteria andConsumeIn(List<BigDecimal> values) {
            addCriterion("consume in", values, "consume");
            return (Criteria) this;
        }

        public Criteria andConsumeNotIn(List<BigDecimal> values) {
            addCriterion("consume not in", values, "consume");
            return (Criteria) this;
        }

        public Criteria andConsumeBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("consume between", value1, value2, "consume");
            return (Criteria) this;
        }

        public Criteria andConsumeNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("consume not between", value1, value2, "consume");
            return (Criteria) this;
        }

        public Criteria andRnumIsNull() {
            addCriterion("rnum is null");
            return (Criteria) this;
        }

        public Criteria andRnumIsNotNull() {
            addCriterion("rnum is not null");
            return (Criteria) this;
        }

        public Criteria andRnumEqualTo(Integer value) {
            addCriterion("rnum =", value, "rnum");
            return (Criteria) this;
        }

        public Criteria andRnumNotEqualTo(Integer value) {
            addCriterion("rnum <>", value, "rnum");
            return (Criteria) this;
        }

        public Criteria andRnumGreaterThan(Integer value) {
            addCriterion("rnum >", value, "rnum");
            return (Criteria) this;
        }

        public Criteria andRnumGreaterThanOrEqualTo(Integer value) {
            addCriterion("rnum >=", value, "rnum");
            return (Criteria) this;
        }

        public Criteria andRnumLessThan(Integer value) {
            addCriterion("rnum <", value, "rnum");
            return (Criteria) this;
        }

        public Criteria andRnumLessThanOrEqualTo(Integer value) {
            addCriterion("rnum <=", value, "rnum");
            return (Criteria) this;
        }

        public Criteria andRnumIn(List<Integer> values) {
            addCriterion("rnum in", values, "rnum");
            return (Criteria) this;
        }

        public Criteria andRnumNotIn(List<Integer> values) {
            addCriterion("rnum not in", values, "rnum");
            return (Criteria) this;
        }

        public Criteria andRnumBetween(Integer value1, Integer value2) {
            addCriterion("rnum between", value1, value2, "rnum");
            return (Criteria) this;
        }

        public Criteria andRnumNotBetween(Integer value1, Integer value2) {
            addCriterion("rnum not between", value1, value2, "rnum");
            return (Criteria) this;
        }

        public Criteria andTotalremainderIsNull() {
            addCriterion("totalremainder is null");
            return (Criteria) this;
        }

        public Criteria andTotalremainderIsNotNull() {
            addCriterion("totalremainder is not null");
            return (Criteria) this;
        }

        public Criteria andTotalremainderEqualTo(BigDecimal value) {
            addCriterion("totalremainder =", value, "totalremainder");
            return (Criteria) this;
        }

        public Criteria andTotalremainderNotEqualTo(BigDecimal value) {
            addCriterion("totalremainder <>", value, "totalremainder");
            return (Criteria) this;
        }

        public Criteria andTotalremainderGreaterThan(BigDecimal value) {
            addCriterion("totalremainder >", value, "totalremainder");
            return (Criteria) this;
        }

        public Criteria andTotalremainderGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("totalremainder >=", value, "totalremainder");
            return (Criteria) this;
        }

        public Criteria andTotalremainderLessThan(BigDecimal value) {
            addCriterion("totalremainder <", value, "totalremainder");
            return (Criteria) this;
        }

        public Criteria andTotalremainderLessThanOrEqualTo(BigDecimal value) {
            addCriterion("totalremainder <=", value, "totalremainder");
            return (Criteria) this;
        }

        public Criteria andTotalremainderIn(List<BigDecimal> values) {
            addCriterion("totalremainder in", values, "totalremainder");
            return (Criteria) this;
        }

        public Criteria andTotalremainderNotIn(List<BigDecimal> values) {
            addCriterion("totalremainder not in", values, "totalremainder");
            return (Criteria) this;
        }

        public Criteria andTotalremainderBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("totalremainder between", value1, value2, "totalremainder");
            return (Criteria) this;
        }

        public Criteria andTotalremainderNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("totalremainder not between", value1, value2, "totalremainder");
            return (Criteria) this;
        }

        public Criteria andBdpushChannelIdIsNull() {
            addCriterion("bdpushChannelId is null");
            return (Criteria) this;
        }

        public Criteria andBdpushChannelIdIsNotNull() {
            addCriterion("bdpushChannelId is not null");
            return (Criteria) this;
        }

        public Criteria andBdpushChannelIdEqualTo(String value) {
            addCriterion("bdpushChannelId =", value, "bdpushChannelId");
            return (Criteria) this;
        }

        public Criteria andBdpushChannelIdNotEqualTo(String value) {
            addCriterion("bdpushChannelId <>", value, "bdpushChannelId");
            return (Criteria) this;
        }

        public Criteria andBdpushChannelIdGreaterThan(String value) {
            addCriterion("bdpushChannelId >", value, "bdpushChannelId");
            return (Criteria) this;
        }

        public Criteria andBdpushChannelIdGreaterThanOrEqualTo(String value) {
            addCriterion("bdpushChannelId >=", value, "bdpushChannelId");
            return (Criteria) this;
        }

        public Criteria andBdpushChannelIdLessThan(String value) {
            addCriterion("bdpushChannelId <", value, "bdpushChannelId");
            return (Criteria) this;
        }

        public Criteria andBdpushChannelIdLessThanOrEqualTo(String value) {
            addCriterion("bdpushChannelId <=", value, "bdpushChannelId");
            return (Criteria) this;
        }

        public Criteria andBdpushChannelIdLike(String value) {
            addCriterion("bdpushChannelId like", value, "bdpushChannelId");
            return (Criteria) this;
        }

        public Criteria andBdpushChannelIdNotLike(String value) {
            addCriterion("bdpushChannelId not like", value, "bdpushChannelId");
            return (Criteria) this;
        }

        public Criteria andBdpushChannelIdIn(List<String> values) {
            addCriterion("bdpushChannelId in", values, "bdpushChannelId");
            return (Criteria) this;
        }

        public Criteria andBdpushChannelIdNotIn(List<String> values) {
            addCriterion("bdpushChannelId not in", values, "bdpushChannelId");
            return (Criteria) this;
        }

        public Criteria andBdpushChannelIdBetween(String value1, String value2) {
            addCriterion("bdpushChannelId between", value1, value2, "bdpushChannelId");
            return (Criteria) this;
        }

        public Criteria andBdpushChannelIdNotBetween(String value1, String value2) {
            addCriterion("bdpushChannelId not between", value1, value2, "bdpushChannelId");
            return (Criteria) this;
        }

        public Criteria andBdpushUserIdIsNull() {
            addCriterion("bdpushUserId is null");
            return (Criteria) this;
        }

        public Criteria andBdpushUserIdIsNotNull() {
            addCriterion("bdpushUserId is not null");
            return (Criteria) this;
        }

        public Criteria andBdpushUserIdEqualTo(String value) {
            addCriterion("bdpushUserId =", value, "bdpushUserId");
            return (Criteria) this;
        }

        public Criteria andBdpushUserIdNotEqualTo(String value) {
            addCriterion("bdpushUserId <>", value, "bdpushUserId");
            return (Criteria) this;
        }

        public Criteria andBdpushUserIdGreaterThan(String value) {
            addCriterion("bdpushUserId >", value, "bdpushUserId");
            return (Criteria) this;
        }

        public Criteria andBdpushUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("bdpushUserId >=", value, "bdpushUserId");
            return (Criteria) this;
        }

        public Criteria andBdpushUserIdLessThan(String value) {
            addCriterion("bdpushUserId <", value, "bdpushUserId");
            return (Criteria) this;
        }

        public Criteria andBdpushUserIdLessThanOrEqualTo(String value) {
            addCriterion("bdpushUserId <=", value, "bdpushUserId");
            return (Criteria) this;
        }

        public Criteria andBdpushUserIdLike(String value) {
            addCriterion("bdpushUserId like", value, "bdpushUserId");
            return (Criteria) this;
        }

        public Criteria andBdpushUserIdNotLike(String value) {
            addCriterion("bdpushUserId not like", value, "bdpushUserId");
            return (Criteria) this;
        }

        public Criteria andBdpushUserIdIn(List<String> values) {
            addCriterion("bdpushUserId in", values, "bdpushUserId");
            return (Criteria) this;
        }

        public Criteria andBdpushUserIdNotIn(List<String> values) {
            addCriterion("bdpushUserId not in", values, "bdpushUserId");
            return (Criteria) this;
        }

        public Criteria andBdpushUserIdBetween(String value1, String value2) {
            addCriterion("bdpushUserId between", value1, value2, "bdpushUserId");
            return (Criteria) this;
        }

        public Criteria andBdpushUserIdNotBetween(String value1, String value2) {
            addCriterion("bdpushUserId not between", value1, value2, "bdpushUserId");
            return (Criteria) this;
        }

        public Criteria andBptypeIsNull() {
            addCriterion("bptype is null");
            return (Criteria) this;
        }

        public Criteria andBptypeIsNotNull() {
            addCriterion("bptype is not null");
            return (Criteria) this;
        }

        public Criteria andBptypeEqualTo(Integer value) {
            addCriterion("bptype =", value, "bptype");
            return (Criteria) this;
        }

        public Criteria andBptypeNotEqualTo(Integer value) {
            addCriterion("bptype <>", value, "bptype");
            return (Criteria) this;
        }

        public Criteria andBptypeGreaterThan(Integer value) {
            addCriterion("bptype >", value, "bptype");
            return (Criteria) this;
        }

        public Criteria andBptypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("bptype >=", value, "bptype");
            return (Criteria) this;
        }

        public Criteria andBptypeLessThan(Integer value) {
            addCriterion("bptype <", value, "bptype");
            return (Criteria) this;
        }

        public Criteria andBptypeLessThanOrEqualTo(Integer value) {
            addCriterion("bptype <=", value, "bptype");
            return (Criteria) this;
        }

        public Criteria andBptypeIn(List<Integer> values) {
            addCriterion("bptype in", values, "bptype");
            return (Criteria) this;
        }

        public Criteria andBptypeNotIn(List<Integer> values) {
            addCriterion("bptype not in", values, "bptype");
            return (Criteria) this;
        }

        public Criteria andBptypeBetween(Integer value1, Integer value2) {
            addCriterion("bptype between", value1, value2, "bptype");
            return (Criteria) this;
        }

        public Criteria andBptypeNotBetween(Integer value1, Integer value2) {
            addCriterion("bptype not between", value1, value2, "bptype");
            return (Criteria) this;
        }

        public Criteria andOtherIdIsNull() {
            addCriterion("otherId is null");
            return (Criteria) this;
        }

        public Criteria andOtherIdIsNotNull() {
            addCriterion("otherId is not null");
            return (Criteria) this;
        }

        public Criteria andOtherIdEqualTo(String value) {
            addCriterion("otherId =", value, "otherId");
            return (Criteria) this;
        }

        public Criteria andOtherIdNotEqualTo(String value) {
            addCriterion("otherId <>", value, "otherId");
            return (Criteria) this;
        }

        public Criteria andOtherIdGreaterThan(String value) {
            addCriterion("otherId >", value, "otherId");
            return (Criteria) this;
        }

        public Criteria andOtherIdGreaterThanOrEqualTo(String value) {
            addCriterion("otherId >=", value, "otherId");
            return (Criteria) this;
        }

        public Criteria andOtherIdLessThan(String value) {
            addCriterion("otherId <", value, "otherId");
            return (Criteria) this;
        }

        public Criteria andOtherIdLessThanOrEqualTo(String value) {
            addCriterion("otherId <=", value, "otherId");
            return (Criteria) this;
        }

        public Criteria andOtherIdLike(String value) {
            addCriterion("otherId like", value, "otherId");
            return (Criteria) this;
        }

        public Criteria andOtherIdNotLike(String value) {
            addCriterion("otherId not like", value, "otherId");
            return (Criteria) this;
        }

        public Criteria andOtherIdIn(List<String> values) {
            addCriterion("otherId in", values, "otherId");
            return (Criteria) this;
        }

        public Criteria andOtherIdNotIn(List<String> values) {
            addCriterion("otherId not in", values, "otherId");
            return (Criteria) this;
        }

        public Criteria andOtherIdBetween(String value1, String value2) {
            addCriterion("otherId between", value1, value2, "otherId");
            return (Criteria) this;
        }

        public Criteria andOtherIdNotBetween(String value1, String value2) {
            addCriterion("otherId not between", value1, value2, "otherId");
            return (Criteria) this;
        }

        public Criteria andPushTypeIsNull() {
            addCriterion("pushType is null");
            return (Criteria) this;
        }

        public Criteria andPushTypeIsNotNull() {
            addCriterion("pushType is not null");
            return (Criteria) this;
        }

        public Criteria andPushTypeEqualTo(Integer value) {
            addCriterion("pushType =", value, "pushType");
            return (Criteria) this;
        }

        public Criteria andPushTypeNotEqualTo(Integer value) {
            addCriterion("pushType <>", value, "pushType");
            return (Criteria) this;
        }

        public Criteria andPushTypeGreaterThan(Integer value) {
            addCriterion("pushType >", value, "pushType");
            return (Criteria) this;
        }

        public Criteria andPushTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("pushType >=", value, "pushType");
            return (Criteria) this;
        }

        public Criteria andPushTypeLessThan(Integer value) {
            addCriterion("pushType <", value, "pushType");
            return (Criteria) this;
        }

        public Criteria andPushTypeLessThanOrEqualTo(Integer value) {
            addCriterion("pushType <=", value, "pushType");
            return (Criteria) this;
        }

        public Criteria andPushTypeIn(List<Integer> values) {
            addCriterion("pushType in", values, "pushType");
            return (Criteria) this;
        }

        public Criteria andPushTypeNotIn(List<Integer> values) {
            addCriterion("pushType not in", values, "pushType");
            return (Criteria) this;
        }

        public Criteria andPushTypeBetween(Integer value1, Integer value2) {
            addCriterion("pushType between", value1, value2, "pushType");
            return (Criteria) this;
        }

        public Criteria andPushTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("pushType not between", value1, value2, "pushType");
            return (Criteria) this;
        }

        public Criteria andAliasIsNull() {
            addCriterion("alias is null");
            return (Criteria) this;
        }

        public Criteria andAliasIsNotNull() {
            addCriterion("alias is not null");
            return (Criteria) this;
        }

        public Criteria andAliasEqualTo(String value) {
            addCriterion("alias =", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasNotEqualTo(String value) {
            addCriterion("alias <>", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasGreaterThan(String value) {
            addCriterion("alias >", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasGreaterThanOrEqualTo(String value) {
            addCriterion("alias >=", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasLessThan(String value) {
            addCriterion("alias <", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasLessThanOrEqualTo(String value) {
            addCriterion("alias <=", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasLike(String value) {
            addCriterion("alias like", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasNotLike(String value) {
            addCriterion("alias not like", value, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasIn(List<String> values) {
            addCriterion("alias in", values, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasNotIn(List<String> values) {
            addCriterion("alias not in", values, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasBetween(String value1, String value2) {
            addCriterion("alias between", value1, value2, "alias");
            return (Criteria) this;
        }

        public Criteria andAliasNotBetween(String value1, String value2) {
            addCriterion("alias not between", value1, value2, "alias");
            return (Criteria) this;
        }

        public Criteria andThirdLoginIdIsNull() {
            addCriterion("thirdLoginId is null");
            return (Criteria) this;
        }

        public Criteria andThirdLoginIdIsNotNull() {
            addCriterion("thirdLoginId is not null");
            return (Criteria) this;
        }

        public Criteria andThirdLoginIdEqualTo(String value) {
            addCriterion("thirdLoginId =", value, "thirdLoginId");
            return (Criteria) this;
        }

        public Criteria andThirdLoginIdNotEqualTo(String value) {
            addCriterion("thirdLoginId <>", value, "thirdLoginId");
            return (Criteria) this;
        }

        public Criteria andThirdLoginIdGreaterThan(String value) {
            addCriterion("thirdLoginId >", value, "thirdLoginId");
            return (Criteria) this;
        }

        public Criteria andThirdLoginIdGreaterThanOrEqualTo(String value) {
            addCriterion("thirdLoginId >=", value, "thirdLoginId");
            return (Criteria) this;
        }

        public Criteria andThirdLoginIdLessThan(String value) {
            addCriterion("thirdLoginId <", value, "thirdLoginId");
            return (Criteria) this;
        }

        public Criteria andThirdLoginIdLessThanOrEqualTo(String value) {
            addCriterion("thirdLoginId <=", value, "thirdLoginId");
            return (Criteria) this;
        }

        public Criteria andThirdLoginIdLike(String value) {
            addCriterion("thirdLoginId like", value, "thirdLoginId");
            return (Criteria) this;
        }

        public Criteria andThirdLoginIdNotLike(String value) {
            addCriterion("thirdLoginId not like", value, "thirdLoginId");
            return (Criteria) this;
        }

        public Criteria andThirdLoginIdIn(List<String> values) {
            addCriterion("thirdLoginId in", values, "thirdLoginId");
            return (Criteria) this;
        }

        public Criteria andThirdLoginIdNotIn(List<String> values) {
            addCriterion("thirdLoginId not in", values, "thirdLoginId");
            return (Criteria) this;
        }

        public Criteria andThirdLoginIdBetween(String value1, String value2) {
            addCriterion("thirdLoginId between", value1, value2, "thirdLoginId");
            return (Criteria) this;
        }

        public Criteria andThirdLoginIdNotBetween(String value1, String value2) {
            addCriterion("thirdLoginId not between", value1, value2, "thirdLoginId");
            return (Criteria) this;
        }

        public Criteria andThirdLoginTypeIsNull() {
            addCriterion("thirdLoginType is null");
            return (Criteria) this;
        }

        public Criteria andThirdLoginTypeIsNotNull() {
            addCriterion("thirdLoginType is not null");
            return (Criteria) this;
        }

        public Criteria andThirdLoginTypeEqualTo(Integer value) {
            addCriterion("thirdLoginType =", value, "thirdLoginType");
            return (Criteria) this;
        }

        public Criteria andThirdLoginTypeNotEqualTo(Integer value) {
            addCriterion("thirdLoginType <>", value, "thirdLoginType");
            return (Criteria) this;
        }

        public Criteria andThirdLoginTypeGreaterThan(Integer value) {
            addCriterion("thirdLoginType >", value, "thirdLoginType");
            return (Criteria) this;
        }

        public Criteria andThirdLoginTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("thirdLoginType >=", value, "thirdLoginType");
            return (Criteria) this;
        }

        public Criteria andThirdLoginTypeLessThan(Integer value) {
            addCriterion("thirdLoginType <", value, "thirdLoginType");
            return (Criteria) this;
        }

        public Criteria andThirdLoginTypeLessThanOrEqualTo(Integer value) {
            addCriterion("thirdLoginType <=", value, "thirdLoginType");
            return (Criteria) this;
        }

        public Criteria andThirdLoginTypeIn(List<Integer> values) {
            addCriterion("thirdLoginType in", values, "thirdLoginType");
            return (Criteria) this;
        }

        public Criteria andThirdLoginTypeNotIn(List<Integer> values) {
            addCriterion("thirdLoginType not in", values, "thirdLoginType");
            return (Criteria) this;
        }

        public Criteria andThirdLoginTypeBetween(Integer value1, Integer value2) {
            addCriterion("thirdLoginType between", value1, value2, "thirdLoginType");
            return (Criteria) this;
        }

        public Criteria andThirdLoginTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("thirdLoginType not between", value1, value2, "thirdLoginType");
            return (Criteria) this;
        }

        public Criteria andMiniOpenIdIsNull() {
            addCriterion("miniOpenId is null");
            return (Criteria) this;
        }

        public Criteria andMiniOpenIdIsNotNull() {
            addCriterion("miniOpenId is not null");
            return (Criteria) this;
        }

        public Criteria andMiniOpenIdEqualTo(String value) {
            addCriterion("miniOpenId =", value, "miniOpenId");
            return (Criteria) this;
        }

        public Criteria andMiniOpenIdNotEqualTo(String value) {
            addCriterion("miniOpenId <>", value, "miniOpenId");
            return (Criteria) this;
        }

        public Criteria andMiniOpenIdGreaterThan(String value) {
            addCriterion("miniOpenId >", value, "miniOpenId");
            return (Criteria) this;
        }

        public Criteria andMiniOpenIdGreaterThanOrEqualTo(String value) {
            addCriterion("miniOpenId >=", value, "miniOpenId");
            return (Criteria) this;
        }

        public Criteria andMiniOpenIdLessThan(String value) {
            addCriterion("miniOpenId <", value, "miniOpenId");
            return (Criteria) this;
        }

        public Criteria andMiniOpenIdLessThanOrEqualTo(String value) {
            addCriterion("miniOpenId <=", value, "miniOpenId");
            return (Criteria) this;
        }

        public Criteria andMiniOpenIdLike(String value) {
            addCriterion("miniOpenId like", value, "miniOpenId");
            return (Criteria) this;
        }

        public Criteria andMiniOpenIdNotLike(String value) {
            addCriterion("miniOpenId not like", value, "miniOpenId");
            return (Criteria) this;
        }

        public Criteria andMiniOpenIdIn(List<String> values) {
            addCriterion("miniOpenId in", values, "miniOpenId");
            return (Criteria) this;
        }

        public Criteria andMiniOpenIdNotIn(List<String> values) {
            addCriterion("miniOpenId not in", values, "miniOpenId");
            return (Criteria) this;
        }

        public Criteria andMiniOpenIdBetween(String value1, String value2) {
            addCriterion("miniOpenId between", value1, value2, "miniOpenId");
            return (Criteria) this;
        }

        public Criteria andMiniOpenIdNotBetween(String value1, String value2) {
            addCriterion("miniOpenId not between", value1, value2, "miniOpenId");
            return (Criteria) this;
        }

        public Criteria andQqIdIsNull() {
            addCriterion("qqId is null");
            return (Criteria) this;
        }

        public Criteria andQqIdIsNotNull() {
            addCriterion("qqId is not null");
            return (Criteria) this;
        }

        public Criteria andQqIdEqualTo(String value) {
            addCriterion("qqId =", value, "qqId");
            return (Criteria) this;
        }

        public Criteria andQqIdNotEqualTo(String value) {
            addCriterion("qqId <>", value, "qqId");
            return (Criteria) this;
        }

        public Criteria andQqIdGreaterThan(String value) {
            addCriterion("qqId >", value, "qqId");
            return (Criteria) this;
        }

        public Criteria andQqIdGreaterThanOrEqualTo(String value) {
            addCriterion("qqId >=", value, "qqId");
            return (Criteria) this;
        }

        public Criteria andQqIdLessThan(String value) {
            addCriterion("qqId <", value, "qqId");
            return (Criteria) this;
        }

        public Criteria andQqIdLessThanOrEqualTo(String value) {
            addCriterion("qqId <=", value, "qqId");
            return (Criteria) this;
        }

        public Criteria andQqIdLike(String value) {
            addCriterion("qqId like", value, "qqId");
            return (Criteria) this;
        }

        public Criteria andQqIdNotLike(String value) {
            addCriterion("qqId not like", value, "qqId");
            return (Criteria) this;
        }

        public Criteria andQqIdIn(List<String> values) {
            addCriterion("qqId in", values, "qqId");
            return (Criteria) this;
        }

        public Criteria andQqIdNotIn(List<String> values) {
            addCriterion("qqId not in", values, "qqId");
            return (Criteria) this;
        }

        public Criteria andQqIdBetween(String value1, String value2) {
            addCriterion("qqId between", value1, value2, "qqId");
            return (Criteria) this;
        }

        public Criteria andQqIdNotBetween(String value1, String value2) {
            addCriterion("qqId not between", value1, value2, "qqId");
            return (Criteria) this;
        }

        public Criteria andWeixinIdIsNull() {
            addCriterion("weixinId is null");
            return (Criteria) this;
        }

        public Criteria andWeixinIdIsNotNull() {
            addCriterion("weixinId is not null");
            return (Criteria) this;
        }

        public Criteria andWeixinIdEqualTo(String value) {
            addCriterion("weixinId =", value, "weixinId");
            return (Criteria) this;
        }

        public Criteria andWeixinIdNotEqualTo(String value) {
            addCriterion("weixinId <>", value, "weixinId");
            return (Criteria) this;
        }

        public Criteria andWeixinIdGreaterThan(String value) {
            addCriterion("weixinId >", value, "weixinId");
            return (Criteria) this;
        }

        public Criteria andWeixinIdGreaterThanOrEqualTo(String value) {
            addCriterion("weixinId >=", value, "weixinId");
            return (Criteria) this;
        }

        public Criteria andWeixinIdLessThan(String value) {
            addCriterion("weixinId <", value, "weixinId");
            return (Criteria) this;
        }

        public Criteria andWeixinIdLessThanOrEqualTo(String value) {
            addCriterion("weixinId <=", value, "weixinId");
            return (Criteria) this;
        }

        public Criteria andWeixinIdLike(String value) {
            addCriterion("weixinId like", value, "weixinId");
            return (Criteria) this;
        }

        public Criteria andWeixinIdNotLike(String value) {
            addCriterion("weixinId not like", value, "weixinId");
            return (Criteria) this;
        }

        public Criteria andWeixinIdIn(List<String> values) {
            addCriterion("weixinId in", values, "weixinId");
            return (Criteria) this;
        }

        public Criteria andWeixinIdNotIn(List<String> values) {
            addCriterion("weixinId not in", values, "weixinId");
            return (Criteria) this;
        }

        public Criteria andWeixinIdBetween(String value1, String value2) {
            addCriterion("weixinId between", value1, value2, "weixinId");
            return (Criteria) this;
        }

        public Criteria andWeixinIdNotBetween(String value1, String value2) {
            addCriterion("weixinId not between", value1, value2, "weixinId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}