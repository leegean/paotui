package com.runfast.common.dao.mapper;

import com.runfast.common.dao.IMapper;
import com.runfast.common.dao.model.RunfastCuseraddress;
import com.runfast.common.dao.model.RunfastCuseraddressExample;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RunfastCuseraddressMapper extends IMapper<RunfastCuseraddress, Integer, RunfastCuseraddressExample> {

    public Integer deleteByPrimaryKey(Integer id);
}