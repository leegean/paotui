package com.runfast.common.dao.model;

import java.io.Serializable;
import java.util.Date;

public class RunfastScroceset implements Serializable {
    private Integer id;

    private Double advert;

    private Double consumexs;

    private Date createTime;

    private Double enshrine;

    private Integer num;

    private Double recommend;

    private Double register;

    private Double review;

    private Double share;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public RunfastScroceset withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getAdvert() {
        return advert;
    }

    public RunfastScroceset withAdvert(Double advert) {
        this.setAdvert(advert);
        return this;
    }

    public void setAdvert(Double advert) {
        this.advert = advert;
    }

    public Double getConsumexs() {
        return consumexs;
    }

    public RunfastScroceset withConsumexs(Double consumexs) {
        this.setConsumexs(consumexs);
        return this;
    }

    public void setConsumexs(Double consumexs) {
        this.consumexs = consumexs;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public RunfastScroceset withCreateTime(Date createTime) {
        this.setCreateTime(createTime);
        return this;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Double getEnshrine() {
        return enshrine;
    }

    public RunfastScroceset withEnshrine(Double enshrine) {
        this.setEnshrine(enshrine);
        return this;
    }

    public void setEnshrine(Double enshrine) {
        this.enshrine = enshrine;
    }

    public Integer getNum() {
        return num;
    }

    public RunfastScroceset withNum(Integer num) {
        this.setNum(num);
        return this;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Double getRecommend() {
        return recommend;
    }

    public RunfastScroceset withRecommend(Double recommend) {
        this.setRecommend(recommend);
        return this;
    }

    public void setRecommend(Double recommend) {
        this.recommend = recommend;
    }

    public Double getRegister() {
        return register;
    }

    public RunfastScroceset withRegister(Double register) {
        this.setRegister(register);
        return this;
    }

    public void setRegister(Double register) {
        this.register = register;
    }

    public Double getReview() {
        return review;
    }

    public RunfastScroceset withReview(Double review) {
        this.setReview(review);
        return this;
    }

    public void setReview(Double review) {
        this.review = review;
    }

    public Double getShare() {
        return share;
    }

    public RunfastScroceset withShare(Double share) {
        this.setShare(share);
        return this;
    }

    public void setShare(Double share) {
        this.share = share;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", advert=").append(advert);
        sb.append(", consumexs=").append(consumexs);
        sb.append(", createTime=").append(createTime);
        sb.append(", enshrine=").append(enshrine);
        sb.append(", num=").append(num);
        sb.append(", recommend=").append(recommend);
        sb.append(", register=").append(register);
        sb.append(", review=").append(review);
        sb.append(", share=").append(share);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}