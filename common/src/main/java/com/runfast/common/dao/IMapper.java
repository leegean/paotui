package com.runfast.common.dao;

import org.apache.ibatis.annotations.Param;
import org.springframework.data.domain.Pageable;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface IMapper<M, ID extends Serializable, Example> {

    long countByExample(Example example);

    long countByExampleWithPageable(@Param("example") Example example, @Param("pageable") Pageable pageable);

    /*int deleteByExample(Example example);

    int deleteByPrimaryKey(ID id);*/

    int insert(M record);

    int insertSelective(M record);

    List<M> selectByExample(Example example);

    List<M> selectByExampleWithBLOBs(Example example);

    List<M> selectByExampleWithPageable(@Param("example") Example example, @Param("pageable") Pageable pageable);

    List<M> selectByExampleWithBLOBsWithPageable(@Param("example") Example example, @Param("pageable") Pageable pageable);

    M selectByPrimaryKey(ID id);

    int updateByExampleSelective(@Param("record") M record, @Param("example") Example example);

    int updateByExample(@Param("record") M record, @Param("example") Example example);

    int updateByPrimaryKeySelective(M record);

    int updateByPrimaryKey(M record);

    List<Map<String, Integer>> selectIdUserIdByExample(Example example);


}
