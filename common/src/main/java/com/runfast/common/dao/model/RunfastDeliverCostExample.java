package com.runfast.common.dao.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class RunfastDeliverCostExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public RunfastDeliverCostExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCTime(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Time(value.getTime()), property);
        }

        protected void addCriterionForJDBCTime(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Time> timeList = new ArrayList<java.sql.Time>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                timeList.add(new java.sql.Time(iter.next().getTime()));
            }
            addCriterion(condition, timeList, property);
        }

        protected void addCriterionForJDBCTime(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Time(value1.getTime()), new java.sql.Time(value2.getTime()), property);
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andChargeIsNull() {
            addCriterion("charge is null");
            return (Criteria) this;
        }

        public Criteria andChargeIsNotNull() {
            addCriterion("charge is not null");
            return (Criteria) this;
        }

        public Criteria andChargeEqualTo(Double value) {
            addCriterion("charge =", value, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeNotEqualTo(Double value) {
            addCriterion("charge <>", value, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeGreaterThan(Double value) {
            addCriterion("charge >", value, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeGreaterThanOrEqualTo(Double value) {
            addCriterion("charge >=", value, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeLessThan(Double value) {
            addCriterion("charge <", value, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeLessThanOrEqualTo(Double value) {
            addCriterion("charge <=", value, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeIn(List<Double> values) {
            addCriterion("charge in", values, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeNotIn(List<Double> values) {
            addCriterion("charge not in", values, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeBetween(Double value1, Double value2) {
            addCriterion("charge between", value1, value2, "charge");
            return (Criteria) this;
        }

        public Criteria andChargeNotBetween(Double value1, Double value2) {
            addCriterion("charge not between", value1, value2, "charge");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("createTime is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("createTime is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("createTime =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("createTime <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("createTime >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("createTime >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("createTime <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("createTime <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("createTime in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("createTime not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("createTime between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("createTime not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andNoChargeIsNull() {
            addCriterion("noCharge is null");
            return (Criteria) this;
        }

        public Criteria andNoChargeIsNotNull() {
            addCriterion("noCharge is not null");
            return (Criteria) this;
        }

        public Criteria andNoChargeEqualTo(Double value) {
            addCriterion("noCharge =", value, "noCharge");
            return (Criteria) this;
        }

        public Criteria andNoChargeNotEqualTo(Double value) {
            addCriterion("noCharge <>", value, "noCharge");
            return (Criteria) this;
        }

        public Criteria andNoChargeGreaterThan(Double value) {
            addCriterion("noCharge >", value, "noCharge");
            return (Criteria) this;
        }

        public Criteria andNoChargeGreaterThanOrEqualTo(Double value) {
            addCriterion("noCharge >=", value, "noCharge");
            return (Criteria) this;
        }

        public Criteria andNoChargeLessThan(Double value) {
            addCriterion("noCharge <", value, "noCharge");
            return (Criteria) this;
        }

        public Criteria andNoChargeLessThanOrEqualTo(Double value) {
            addCriterion("noCharge <=", value, "noCharge");
            return (Criteria) this;
        }

        public Criteria andNoChargeIn(List<Double> values) {
            addCriterion("noCharge in", values, "noCharge");
            return (Criteria) this;
        }

        public Criteria andNoChargeNotIn(List<Double> values) {
            addCriterion("noCharge not in", values, "noCharge");
            return (Criteria) this;
        }

        public Criteria andNoChargeBetween(Double value1, Double value2) {
            addCriterion("noCharge between", value1, value2, "noCharge");
            return (Criteria) this;
        }

        public Criteria andNoChargeNotBetween(Double value1, Double value2) {
            addCriterion("noCharge not between", value1, value2, "noCharge");
            return (Criteria) this;
        }

        public Criteria andTimeCost1IsNull() {
            addCriterion("timeCost1 is null");
            return (Criteria) this;
        }

        public Criteria andTimeCost1IsNotNull() {
            addCriterion("timeCost1 is not null");
            return (Criteria) this;
        }

        public Criteria andTimeCost1EqualTo(Double value) {
            addCriterion("timeCost1 =", value, "timeCost1");
            return (Criteria) this;
        }

        public Criteria andTimeCost1NotEqualTo(Double value) {
            addCriterion("timeCost1 <>", value, "timeCost1");
            return (Criteria) this;
        }

        public Criteria andTimeCost1GreaterThan(Double value) {
            addCriterion("timeCost1 >", value, "timeCost1");
            return (Criteria) this;
        }

        public Criteria andTimeCost1GreaterThanOrEqualTo(Double value) {
            addCriterion("timeCost1 >=", value, "timeCost1");
            return (Criteria) this;
        }

        public Criteria andTimeCost1LessThan(Double value) {
            addCriterion("timeCost1 <", value, "timeCost1");
            return (Criteria) this;
        }

        public Criteria andTimeCost1LessThanOrEqualTo(Double value) {
            addCriterion("timeCost1 <=", value, "timeCost1");
            return (Criteria) this;
        }

        public Criteria andTimeCost1In(List<Double> values) {
            addCriterion("timeCost1 in", values, "timeCost1");
            return (Criteria) this;
        }

        public Criteria andTimeCost1NotIn(List<Double> values) {
            addCriterion("timeCost1 not in", values, "timeCost1");
            return (Criteria) this;
        }

        public Criteria andTimeCost1Between(Double value1, Double value2) {
            addCriterion("timeCost1 between", value1, value2, "timeCost1");
            return (Criteria) this;
        }

        public Criteria andTimeCost1NotBetween(Double value1, Double value2) {
            addCriterion("timeCost1 not between", value1, value2, "timeCost1");
            return (Criteria) this;
        }

        public Criteria andTimeCost2IsNull() {
            addCriterion("timeCost2 is null");
            return (Criteria) this;
        }

        public Criteria andTimeCost2IsNotNull() {
            addCriterion("timeCost2 is not null");
            return (Criteria) this;
        }

        public Criteria andTimeCost2EqualTo(Double value) {
            addCriterion("timeCost2 =", value, "timeCost2");
            return (Criteria) this;
        }

        public Criteria andTimeCost2NotEqualTo(Double value) {
            addCriterion("timeCost2 <>", value, "timeCost2");
            return (Criteria) this;
        }

        public Criteria andTimeCost2GreaterThan(Double value) {
            addCriterion("timeCost2 >", value, "timeCost2");
            return (Criteria) this;
        }

        public Criteria andTimeCost2GreaterThanOrEqualTo(Double value) {
            addCriterion("timeCost2 >=", value, "timeCost2");
            return (Criteria) this;
        }

        public Criteria andTimeCost2LessThan(Double value) {
            addCriterion("timeCost2 <", value, "timeCost2");
            return (Criteria) this;
        }

        public Criteria andTimeCost2LessThanOrEqualTo(Double value) {
            addCriterion("timeCost2 <=", value, "timeCost2");
            return (Criteria) this;
        }

        public Criteria andTimeCost2In(List<Double> values) {
            addCriterion("timeCost2 in", values, "timeCost2");
            return (Criteria) this;
        }

        public Criteria andTimeCost2NotIn(List<Double> values) {
            addCriterion("timeCost2 not in", values, "timeCost2");
            return (Criteria) this;
        }

        public Criteria andTimeCost2Between(Double value1, Double value2) {
            addCriterion("timeCost2 between", value1, value2, "timeCost2");
            return (Criteria) this;
        }

        public Criteria andTimeCost2NotBetween(Double value1, Double value2) {
            addCriterion("timeCost2 not between", value1, value2, "timeCost2");
            return (Criteria) this;
        }

        public Criteria andEndTimeDay1IsNull() {
            addCriterion("endTimeDay1 is null");
            return (Criteria) this;
        }

        public Criteria andEndTimeDay1IsNotNull() {
            addCriterion("endTimeDay1 is not null");
            return (Criteria) this;
        }

        public Criteria andEndTimeDay1EqualTo(Date value) {
            addCriterionForJDBCTime("endTimeDay1 =", value, "endTimeDay1");
            return (Criteria) this;
        }

        public Criteria andEndTimeDay1NotEqualTo(Date value) {
            addCriterionForJDBCTime("endTimeDay1 <>", value, "endTimeDay1");
            return (Criteria) this;
        }

        public Criteria andEndTimeDay1GreaterThan(Date value) {
            addCriterionForJDBCTime("endTimeDay1 >", value, "endTimeDay1");
            return (Criteria) this;
        }

        public Criteria andEndTimeDay1GreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("endTimeDay1 >=", value, "endTimeDay1");
            return (Criteria) this;
        }

        public Criteria andEndTimeDay1LessThan(Date value) {
            addCriterionForJDBCTime("endTimeDay1 <", value, "endTimeDay1");
            return (Criteria) this;
        }

        public Criteria andEndTimeDay1LessThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("endTimeDay1 <=", value, "endTimeDay1");
            return (Criteria) this;
        }

        public Criteria andEndTimeDay1In(List<Date> values) {
            addCriterionForJDBCTime("endTimeDay1 in", values, "endTimeDay1");
            return (Criteria) this;
        }

        public Criteria andEndTimeDay1NotIn(List<Date> values) {
            addCriterionForJDBCTime("endTimeDay1 not in", values, "endTimeDay1");
            return (Criteria) this;
        }

        public Criteria andEndTimeDay1Between(Date value1, Date value2) {
            addCriterionForJDBCTime("endTimeDay1 between", value1, value2, "endTimeDay1");
            return (Criteria) this;
        }

        public Criteria andEndTimeDay1NotBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("endTimeDay1 not between", value1, value2, "endTimeDay1");
            return (Criteria) this;
        }

        public Criteria andEndTimeNight2IsNull() {
            addCriterion("endTimeNight2 is null");
            return (Criteria) this;
        }

        public Criteria andEndTimeNight2IsNotNull() {
            addCriterion("endTimeNight2 is not null");
            return (Criteria) this;
        }

        public Criteria andEndTimeNight2EqualTo(Date value) {
            addCriterionForJDBCTime("endTimeNight2 =", value, "endTimeNight2");
            return (Criteria) this;
        }

        public Criteria andEndTimeNight2NotEqualTo(Date value) {
            addCriterionForJDBCTime("endTimeNight2 <>", value, "endTimeNight2");
            return (Criteria) this;
        }

        public Criteria andEndTimeNight2GreaterThan(Date value) {
            addCriterionForJDBCTime("endTimeNight2 >", value, "endTimeNight2");
            return (Criteria) this;
        }

        public Criteria andEndTimeNight2GreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("endTimeNight2 >=", value, "endTimeNight2");
            return (Criteria) this;
        }

        public Criteria andEndTimeNight2LessThan(Date value) {
            addCriterionForJDBCTime("endTimeNight2 <", value, "endTimeNight2");
            return (Criteria) this;
        }

        public Criteria andEndTimeNight2LessThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("endTimeNight2 <=", value, "endTimeNight2");
            return (Criteria) this;
        }

        public Criteria andEndTimeNight2In(List<Date> values) {
            addCriterionForJDBCTime("endTimeNight2 in", values, "endTimeNight2");
            return (Criteria) this;
        }

        public Criteria andEndTimeNight2NotIn(List<Date> values) {
            addCriterionForJDBCTime("endTimeNight2 not in", values, "endTimeNight2");
            return (Criteria) this;
        }

        public Criteria andEndTimeNight2Between(Date value1, Date value2) {
            addCriterionForJDBCTime("endTimeNight2 between", value1, value2, "endTimeNight2");
            return (Criteria) this;
        }

        public Criteria andEndTimeNight2NotBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("endTimeNight2 not between", value1, value2, "endTimeNight2");
            return (Criteria) this;
        }

        public Criteria andStartTimeDay1IsNull() {
            addCriterion("startTimeDay1 is null");
            return (Criteria) this;
        }

        public Criteria andStartTimeDay1IsNotNull() {
            addCriterion("startTimeDay1 is not null");
            return (Criteria) this;
        }

        public Criteria andStartTimeDay1EqualTo(Date value) {
            addCriterionForJDBCTime("startTimeDay1 =", value, "startTimeDay1");
            return (Criteria) this;
        }

        public Criteria andStartTimeDay1NotEqualTo(Date value) {
            addCriterionForJDBCTime("startTimeDay1 <>", value, "startTimeDay1");
            return (Criteria) this;
        }

        public Criteria andStartTimeDay1GreaterThan(Date value) {
            addCriterionForJDBCTime("startTimeDay1 >", value, "startTimeDay1");
            return (Criteria) this;
        }

        public Criteria andStartTimeDay1GreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("startTimeDay1 >=", value, "startTimeDay1");
            return (Criteria) this;
        }

        public Criteria andStartTimeDay1LessThan(Date value) {
            addCriterionForJDBCTime("startTimeDay1 <", value, "startTimeDay1");
            return (Criteria) this;
        }

        public Criteria andStartTimeDay1LessThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("startTimeDay1 <=", value, "startTimeDay1");
            return (Criteria) this;
        }

        public Criteria andStartTimeDay1In(List<Date> values) {
            addCriterionForJDBCTime("startTimeDay1 in", values, "startTimeDay1");
            return (Criteria) this;
        }

        public Criteria andStartTimeDay1NotIn(List<Date> values) {
            addCriterionForJDBCTime("startTimeDay1 not in", values, "startTimeDay1");
            return (Criteria) this;
        }

        public Criteria andStartTimeDay1Between(Date value1, Date value2) {
            addCriterionForJDBCTime("startTimeDay1 between", value1, value2, "startTimeDay1");
            return (Criteria) this;
        }

        public Criteria andStartTimeDay1NotBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("startTimeDay1 not between", value1, value2, "startTimeDay1");
            return (Criteria) this;
        }

        public Criteria andStartTimeNight2IsNull() {
            addCriterion("startTimeNight2 is null");
            return (Criteria) this;
        }

        public Criteria andStartTimeNight2IsNotNull() {
            addCriterion("startTimeNight2 is not null");
            return (Criteria) this;
        }

        public Criteria andStartTimeNight2EqualTo(Date value) {
            addCriterionForJDBCTime("startTimeNight2 =", value, "startTimeNight2");
            return (Criteria) this;
        }

        public Criteria andStartTimeNight2NotEqualTo(Date value) {
            addCriterionForJDBCTime("startTimeNight2 <>", value, "startTimeNight2");
            return (Criteria) this;
        }

        public Criteria andStartTimeNight2GreaterThan(Date value) {
            addCriterionForJDBCTime("startTimeNight2 >", value, "startTimeNight2");
            return (Criteria) this;
        }

        public Criteria andStartTimeNight2GreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("startTimeNight2 >=", value, "startTimeNight2");
            return (Criteria) this;
        }

        public Criteria andStartTimeNight2LessThan(Date value) {
            addCriterionForJDBCTime("startTimeNight2 <", value, "startTimeNight2");
            return (Criteria) this;
        }

        public Criteria andStartTimeNight2LessThanOrEqualTo(Date value) {
            addCriterionForJDBCTime("startTimeNight2 <=", value, "startTimeNight2");
            return (Criteria) this;
        }

        public Criteria andStartTimeNight2In(List<Date> values) {
            addCriterionForJDBCTime("startTimeNight2 in", values, "startTimeNight2");
            return (Criteria) this;
        }

        public Criteria andStartTimeNight2NotIn(List<Date> values) {
            addCriterionForJDBCTime("startTimeNight2 not in", values, "startTimeNight2");
            return (Criteria) this;
        }

        public Criteria andStartTimeNight2Between(Date value1, Date value2) {
            addCriterionForJDBCTime("startTimeNight2 between", value1, value2, "startTimeNight2");
            return (Criteria) this;
        }

        public Criteria andStartTimeNight2NotBetween(Date value1, Date value2) {
            addCriterionForJDBCTime("startTimeNight2 not between", value1, value2, "startTimeNight2");
            return (Criteria) this;
        }

        public Criteria andCharge1IsNull() {
            addCriterion("charge1 is null");
            return (Criteria) this;
        }

        public Criteria andCharge1IsNotNull() {
            addCriterion("charge1 is not null");
            return (Criteria) this;
        }

        public Criteria andCharge1EqualTo(BigDecimal value) {
            addCriterion("charge1 =", value, "charge1");
            return (Criteria) this;
        }

        public Criteria andCharge1NotEqualTo(BigDecimal value) {
            addCriterion("charge1 <>", value, "charge1");
            return (Criteria) this;
        }

        public Criteria andCharge1GreaterThan(BigDecimal value) {
            addCriterion("charge1 >", value, "charge1");
            return (Criteria) this;
        }

        public Criteria andCharge1GreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("charge1 >=", value, "charge1");
            return (Criteria) this;
        }

        public Criteria andCharge1LessThan(BigDecimal value) {
            addCriterion("charge1 <", value, "charge1");
            return (Criteria) this;
        }

        public Criteria andCharge1LessThanOrEqualTo(BigDecimal value) {
            addCriterion("charge1 <=", value, "charge1");
            return (Criteria) this;
        }

        public Criteria andCharge1In(List<BigDecimal> values) {
            addCriterion("charge1 in", values, "charge1");
            return (Criteria) this;
        }

        public Criteria andCharge1NotIn(List<BigDecimal> values) {
            addCriterion("charge1 not in", values, "charge1");
            return (Criteria) this;
        }

        public Criteria andCharge1Between(BigDecimal value1, BigDecimal value2) {
            addCriterion("charge1 between", value1, value2, "charge1");
            return (Criteria) this;
        }

        public Criteria andCharge1NotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("charge1 not between", value1, value2, "charge1");
            return (Criteria) this;
        }

        public Criteria andCharge2IsNull() {
            addCriterion("charge2 is null");
            return (Criteria) this;
        }

        public Criteria andCharge2IsNotNull() {
            addCriterion("charge2 is not null");
            return (Criteria) this;
        }

        public Criteria andCharge2EqualTo(BigDecimal value) {
            addCriterion("charge2 =", value, "charge2");
            return (Criteria) this;
        }

        public Criteria andCharge2NotEqualTo(BigDecimal value) {
            addCriterion("charge2 <>", value, "charge2");
            return (Criteria) this;
        }

        public Criteria andCharge2GreaterThan(BigDecimal value) {
            addCriterion("charge2 >", value, "charge2");
            return (Criteria) this;
        }

        public Criteria andCharge2GreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("charge2 >=", value, "charge2");
            return (Criteria) this;
        }

        public Criteria andCharge2LessThan(BigDecimal value) {
            addCriterion("charge2 <", value, "charge2");
            return (Criteria) this;
        }

        public Criteria andCharge2LessThanOrEqualTo(BigDecimal value) {
            addCriterion("charge2 <=", value, "charge2");
            return (Criteria) this;
        }

        public Criteria andCharge2In(List<BigDecimal> values) {
            addCriterion("charge2 in", values, "charge2");
            return (Criteria) this;
        }

        public Criteria andCharge2NotIn(List<BigDecimal> values) {
            addCriterion("charge2 not in", values, "charge2");
            return (Criteria) this;
        }

        public Criteria andCharge2Between(BigDecimal value1, BigDecimal value2) {
            addCriterion("charge2 between", value1, value2, "charge2");
            return (Criteria) this;
        }

        public Criteria andCharge2NotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("charge2 not between", value1, value2, "charge2");
            return (Criteria) this;
        }

        public Criteria andMaxChargeIsNull() {
            addCriterion("maxCharge is null");
            return (Criteria) this;
        }

        public Criteria andMaxChargeIsNotNull() {
            addCriterion("maxCharge is not null");
            return (Criteria) this;
        }

        public Criteria andMaxChargeEqualTo(BigDecimal value) {
            addCriterion("maxCharge =", value, "maxCharge");
            return (Criteria) this;
        }

        public Criteria andMaxChargeNotEqualTo(BigDecimal value) {
            addCriterion("maxCharge <>", value, "maxCharge");
            return (Criteria) this;
        }

        public Criteria andMaxChargeGreaterThan(BigDecimal value) {
            addCriterion("maxCharge >", value, "maxCharge");
            return (Criteria) this;
        }

        public Criteria andMaxChargeGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("maxCharge >=", value, "maxCharge");
            return (Criteria) this;
        }

        public Criteria andMaxChargeLessThan(BigDecimal value) {
            addCriterion("maxCharge <", value, "maxCharge");
            return (Criteria) this;
        }

        public Criteria andMaxChargeLessThanOrEqualTo(BigDecimal value) {
            addCriterion("maxCharge <=", value, "maxCharge");
            return (Criteria) this;
        }

        public Criteria andMaxChargeIn(List<BigDecimal> values) {
            addCriterion("maxCharge in", values, "maxCharge");
            return (Criteria) this;
        }

        public Criteria andMaxChargeNotIn(List<BigDecimal> values) {
            addCriterion("maxCharge not in", values, "maxCharge");
            return (Criteria) this;
        }

        public Criteria andMaxChargeBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("maxCharge between", value1, value2, "maxCharge");
            return (Criteria) this;
        }

        public Criteria andMaxChargeNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("maxCharge not between", value1, value2, "maxCharge");
            return (Criteria) this;
        }

        public Criteria andMaxDistanceIsNull() {
            addCriterion("maxDistance is null");
            return (Criteria) this;
        }

        public Criteria andMaxDistanceIsNotNull() {
            addCriterion("maxDistance is not null");
            return (Criteria) this;
        }

        public Criteria andMaxDistanceEqualTo(Double value) {
            addCriterion("maxDistance =", value, "maxDistance");
            return (Criteria) this;
        }

        public Criteria andMaxDistanceNotEqualTo(Double value) {
            addCriterion("maxDistance <>", value, "maxDistance");
            return (Criteria) this;
        }

        public Criteria andMaxDistanceGreaterThan(Double value) {
            addCriterion("maxDistance >", value, "maxDistance");
            return (Criteria) this;
        }

        public Criteria andMaxDistanceGreaterThanOrEqualTo(Double value) {
            addCriterion("maxDistance >=", value, "maxDistance");
            return (Criteria) this;
        }

        public Criteria andMaxDistanceLessThan(Double value) {
            addCriterion("maxDistance <", value, "maxDistance");
            return (Criteria) this;
        }

        public Criteria andMaxDistanceLessThanOrEqualTo(Double value) {
            addCriterion("maxDistance <=", value, "maxDistance");
            return (Criteria) this;
        }

        public Criteria andMaxDistanceIn(List<Double> values) {
            addCriterion("maxDistance in", values, "maxDistance");
            return (Criteria) this;
        }

        public Criteria andMaxDistanceNotIn(List<Double> values) {
            addCriterion("maxDistance not in", values, "maxDistance");
            return (Criteria) this;
        }

        public Criteria andMaxDistanceBetween(Double value1, Double value2) {
            addCriterion("maxDistance between", value1, value2, "maxDistance");
            return (Criteria) this;
        }

        public Criteria andMaxDistanceNotBetween(Double value1, Double value2) {
            addCriterion("maxDistance not between", value1, value2, "maxDistance");
            return (Criteria) this;
        }

        public Criteria andAreaIsNull() {
            addCriterion("area is null");
            return (Criteria) this;
        }

        public Criteria andAreaIsNotNull() {
            addCriterion("area is not null");
            return (Criteria) this;
        }

        public Criteria andAreaEqualTo(String value) {
            addCriterion("area =", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaNotEqualTo(String value) {
            addCriterion("area <>", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaGreaterThan(String value) {
            addCriterion("area >", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaGreaterThanOrEqualTo(String value) {
            addCriterion("area >=", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaLessThan(String value) {
            addCriterion("area <", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaLessThanOrEqualTo(String value) {
            addCriterion("area <=", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaLike(String value) {
            addCriterion("area like", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaNotLike(String value) {
            addCriterion("area not like", value, "area");
            return (Criteria) this;
        }

        public Criteria andAreaIn(List<String> values) {
            addCriterion("area in", values, "area");
            return (Criteria) this;
        }

        public Criteria andAreaNotIn(List<String> values) {
            addCriterion("area not in", values, "area");
            return (Criteria) this;
        }

        public Criteria andAreaBetween(String value1, String value2) {
            addCriterion("area between", value1, value2, "area");
            return (Criteria) this;
        }

        public Criteria andAreaNotBetween(String value1, String value2) {
            addCriterion("area not between", value1, value2, "area");
            return (Criteria) this;
        }

        public Criteria andAreaNameIsNull() {
            addCriterion("areaName is null");
            return (Criteria) this;
        }

        public Criteria andAreaNameIsNotNull() {
            addCriterion("areaName is not null");
            return (Criteria) this;
        }

        public Criteria andAreaNameEqualTo(String value) {
            addCriterion("areaName =", value, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameNotEqualTo(String value) {
            addCriterion("areaName <>", value, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameGreaterThan(String value) {
            addCriterion("areaName >", value, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameGreaterThanOrEqualTo(String value) {
            addCriterion("areaName >=", value, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameLessThan(String value) {
            addCriterion("areaName <", value, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameLessThanOrEqualTo(String value) {
            addCriterion("areaName <=", value, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameLike(String value) {
            addCriterion("areaName like", value, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameNotLike(String value) {
            addCriterion("areaName not like", value, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameIn(List<String> values) {
            addCriterion("areaName in", values, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameNotIn(List<String> values) {
            addCriterion("areaName not in", values, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameBetween(String value1, String value2) {
            addCriterion("areaName between", value1, value2, "areaName");
            return (Criteria) this;
        }

        public Criteria andAreaNameNotBetween(String value1, String value2) {
            addCriterion("areaName not between", value1, value2, "areaName");
            return (Criteria) this;
        }

        public Criteria andCityIdIsNull() {
            addCriterion("cityId is null");
            return (Criteria) this;
        }

        public Criteria andCityIdIsNotNull() {
            addCriterion("cityId is not null");
            return (Criteria) this;
        }

        public Criteria andCityIdEqualTo(String value) {
            addCriterion("cityId =", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdNotEqualTo(String value) {
            addCriterion("cityId <>", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdGreaterThan(String value) {
            addCriterion("cityId >", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdGreaterThanOrEqualTo(String value) {
            addCriterion("cityId >=", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdLessThan(String value) {
            addCriterion("cityId <", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdLessThanOrEqualTo(String value) {
            addCriterion("cityId <=", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdLike(String value) {
            addCriterion("cityId like", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdNotLike(String value) {
            addCriterion("cityId not like", value, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdIn(List<String> values) {
            addCriterion("cityId in", values, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdNotIn(List<String> values) {
            addCriterion("cityId not in", values, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdBetween(String value1, String value2) {
            addCriterion("cityId between", value1, value2, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityIdNotBetween(String value1, String value2) {
            addCriterion("cityId not between", value1, value2, "cityId");
            return (Criteria) this;
        }

        public Criteria andCityNameIsNull() {
            addCriterion("cityName is null");
            return (Criteria) this;
        }

        public Criteria andCityNameIsNotNull() {
            addCriterion("cityName is not null");
            return (Criteria) this;
        }

        public Criteria andCityNameEqualTo(String value) {
            addCriterion("cityName =", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameNotEqualTo(String value) {
            addCriterion("cityName <>", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameGreaterThan(String value) {
            addCriterion("cityName >", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameGreaterThanOrEqualTo(String value) {
            addCriterion("cityName >=", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameLessThan(String value) {
            addCriterion("cityName <", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameLessThanOrEqualTo(String value) {
            addCriterion("cityName <=", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameLike(String value) {
            addCriterion("cityName like", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameNotLike(String value) {
            addCriterion("cityName not like", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameIn(List<String> values) {
            addCriterion("cityName in", values, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameNotIn(List<String> values) {
            addCriterion("cityName not in", values, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameBetween(String value1, String value2) {
            addCriterion("cityName between", value1, value2, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameNotBetween(String value1, String value2) {
            addCriterion("cityName not between", value1, value2, "cityName");
            return (Criteria) this;
        }

        public Criteria andCountyIdIsNull() {
            addCriterion("countyId is null");
            return (Criteria) this;
        }

        public Criteria andCountyIdIsNotNull() {
            addCriterion("countyId is not null");
            return (Criteria) this;
        }

        public Criteria andCountyIdEqualTo(String value) {
            addCriterion("countyId =", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdNotEqualTo(String value) {
            addCriterion("countyId <>", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdGreaterThan(String value) {
            addCriterion("countyId >", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdGreaterThanOrEqualTo(String value) {
            addCriterion("countyId >=", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdLessThan(String value) {
            addCriterion("countyId <", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdLessThanOrEqualTo(String value) {
            addCriterion("countyId <=", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdLike(String value) {
            addCriterion("countyId like", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdNotLike(String value) {
            addCriterion("countyId not like", value, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdIn(List<String> values) {
            addCriterion("countyId in", values, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdNotIn(List<String> values) {
            addCriterion("countyId not in", values, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdBetween(String value1, String value2) {
            addCriterion("countyId between", value1, value2, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyIdNotBetween(String value1, String value2) {
            addCriterion("countyId not between", value1, value2, "countyId");
            return (Criteria) this;
        }

        public Criteria andCountyNameIsNull() {
            addCriterion("countyName is null");
            return (Criteria) this;
        }

        public Criteria andCountyNameIsNotNull() {
            addCriterion("countyName is not null");
            return (Criteria) this;
        }

        public Criteria andCountyNameEqualTo(String value) {
            addCriterion("countyName =", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameNotEqualTo(String value) {
            addCriterion("countyName <>", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameGreaterThan(String value) {
            addCriterion("countyName >", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameGreaterThanOrEqualTo(String value) {
            addCriterion("countyName >=", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameLessThan(String value) {
            addCriterion("countyName <", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameLessThanOrEqualTo(String value) {
            addCriterion("countyName <=", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameLike(String value) {
            addCriterion("countyName like", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameNotLike(String value) {
            addCriterion("countyName not like", value, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameIn(List<String> values) {
            addCriterion("countyName in", values, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameNotIn(List<String> values) {
            addCriterion("countyName not in", values, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameBetween(String value1, String value2) {
            addCriterion("countyName between", value1, value2, "countyName");
            return (Criteria) this;
        }

        public Criteria andCountyNameNotBetween(String value1, String value2) {
            addCriterion("countyName not between", value1, value2, "countyName");
            return (Criteria) this;
        }

        public Criteria andIsdefaultIsNull() {
            addCriterion("isdefault is null");
            return (Criteria) this;
        }

        public Criteria andIsdefaultIsNotNull() {
            addCriterion("isdefault is not null");
            return (Criteria) this;
        }

        public Criteria andIsdefaultEqualTo(Integer value) {
            addCriterion("isdefault =", value, "isdefault");
            return (Criteria) this;
        }

        public Criteria andIsdefaultNotEqualTo(Integer value) {
            addCriterion("isdefault <>", value, "isdefault");
            return (Criteria) this;
        }

        public Criteria andIsdefaultGreaterThan(Integer value) {
            addCriterion("isdefault >", value, "isdefault");
            return (Criteria) this;
        }

        public Criteria andIsdefaultGreaterThanOrEqualTo(Integer value) {
            addCriterion("isdefault >=", value, "isdefault");
            return (Criteria) this;
        }

        public Criteria andIsdefaultLessThan(Integer value) {
            addCriterion("isdefault <", value, "isdefault");
            return (Criteria) this;
        }

        public Criteria andIsdefaultLessThanOrEqualTo(Integer value) {
            addCriterion("isdefault <=", value, "isdefault");
            return (Criteria) this;
        }

        public Criteria andIsdefaultIn(List<Integer> values) {
            addCriterion("isdefault in", values, "isdefault");
            return (Criteria) this;
        }

        public Criteria andIsdefaultNotIn(List<Integer> values) {
            addCriterion("isdefault not in", values, "isdefault");
            return (Criteria) this;
        }

        public Criteria andIsdefaultBetween(Integer value1, Integer value2) {
            addCriterion("isdefault between", value1, value2, "isdefault");
            return (Criteria) this;
        }

        public Criteria andIsdefaultNotBetween(Integer value1, Integer value2) {
            addCriterion("isdefault not between", value1, value2, "isdefault");
            return (Criteria) this;
        }

        public Criteria andUpdatehourIsNull() {
            addCriterion("updatehour is null");
            return (Criteria) this;
        }

        public Criteria andUpdatehourIsNotNull() {
            addCriterion("updatehour is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatehourEqualTo(Integer value) {
            addCriterion("updatehour =", value, "updatehour");
            return (Criteria) this;
        }

        public Criteria andUpdatehourNotEqualTo(Integer value) {
            addCriterion("updatehour <>", value, "updatehour");
            return (Criteria) this;
        }

        public Criteria andUpdatehourGreaterThan(Integer value) {
            addCriterion("updatehour >", value, "updatehour");
            return (Criteria) this;
        }

        public Criteria andUpdatehourGreaterThanOrEqualTo(Integer value) {
            addCriterion("updatehour >=", value, "updatehour");
            return (Criteria) this;
        }

        public Criteria andUpdatehourLessThan(Integer value) {
            addCriterion("updatehour <", value, "updatehour");
            return (Criteria) this;
        }

        public Criteria andUpdatehourLessThanOrEqualTo(Integer value) {
            addCriterion("updatehour <=", value, "updatehour");
            return (Criteria) this;
        }

        public Criteria andUpdatehourIn(List<Integer> values) {
            addCriterion("updatehour in", values, "updatehour");
            return (Criteria) this;
        }

        public Criteria andUpdatehourNotIn(List<Integer> values) {
            addCriterion("updatehour not in", values, "updatehour");
            return (Criteria) this;
        }

        public Criteria andUpdatehourBetween(Integer value1, Integer value2) {
            addCriterion("updatehour between", value1, value2, "updatehour");
            return (Criteria) this;
        }

        public Criteria andUpdatehourNotBetween(Integer value1, Integer value2) {
            addCriterion("updatehour not between", value1, value2, "updatehour");
            return (Criteria) this;
        }

        public Criteria andAgentIdIsNull() {
            addCriterion("agentId is null");
            return (Criteria) this;
        }

        public Criteria andAgentIdIsNotNull() {
            addCriterion("agentId is not null");
            return (Criteria) this;
        }

        public Criteria andAgentIdEqualTo(Integer value) {
            addCriterion("agentId =", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotEqualTo(Integer value) {
            addCriterion("agentId <>", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdGreaterThan(Integer value) {
            addCriterion("agentId >", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("agentId >=", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdLessThan(Integer value) {
            addCriterion("agentId <", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdLessThanOrEqualTo(Integer value) {
            addCriterion("agentId <=", value, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdIn(List<Integer> values) {
            addCriterion("agentId in", values, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotIn(List<Integer> values) {
            addCriterion("agentId not in", values, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdBetween(Integer value1, Integer value2) {
            addCriterion("agentId between", value1, value2, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentIdNotBetween(Integer value1, Integer value2) {
            addCriterion("agentId not between", value1, value2, "agentId");
            return (Criteria) this;
        }

        public Criteria andAgentNameIsNull() {
            addCriterion("agentName is null");
            return (Criteria) this;
        }

        public Criteria andAgentNameIsNotNull() {
            addCriterion("agentName is not null");
            return (Criteria) this;
        }

        public Criteria andAgentNameEqualTo(String value) {
            addCriterion("agentName =", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotEqualTo(String value) {
            addCriterion("agentName <>", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameGreaterThan(String value) {
            addCriterion("agentName >", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameGreaterThanOrEqualTo(String value) {
            addCriterion("agentName >=", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameLessThan(String value) {
            addCriterion("agentName <", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameLessThanOrEqualTo(String value) {
            addCriterion("agentName <=", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameLike(String value) {
            addCriterion("agentName like", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotLike(String value) {
            addCriterion("agentName not like", value, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameIn(List<String> values) {
            addCriterion("agentName in", values, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotIn(List<String> values) {
            addCriterion("agentName not in", values, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameBetween(String value1, String value2) {
            addCriterion("agentName between", value1, value2, "agentName");
            return (Criteria) this;
        }

        public Criteria andAgentNameNotBetween(String value1, String value2) {
            addCriterion("agentName not between", value1, value2, "agentName");
            return (Criteria) this;
        }

        public Criteria andSpeedIsNull() {
            addCriterion("speed is null");
            return (Criteria) this;
        }

        public Criteria andSpeedIsNotNull() {
            addCriterion("speed is not null");
            return (Criteria) this;
        }

        public Criteria andSpeedEqualTo(Double value) {
            addCriterion("speed =", value, "speed");
            return (Criteria) this;
        }

        public Criteria andSpeedNotEqualTo(Double value) {
            addCriterion("speed <>", value, "speed");
            return (Criteria) this;
        }

        public Criteria andSpeedGreaterThan(Double value) {
            addCriterion("speed >", value, "speed");
            return (Criteria) this;
        }

        public Criteria andSpeedGreaterThanOrEqualTo(Double value) {
            addCriterion("speed >=", value, "speed");
            return (Criteria) this;
        }

        public Criteria andSpeedLessThan(Double value) {
            addCriterion("speed <", value, "speed");
            return (Criteria) this;
        }

        public Criteria andSpeedLessThanOrEqualTo(Double value) {
            addCriterion("speed <=", value, "speed");
            return (Criteria) this;
        }

        public Criteria andSpeedIn(List<Double> values) {
            addCriterion("speed in", values, "speed");
            return (Criteria) this;
        }

        public Criteria andSpeedNotIn(List<Double> values) {
            addCriterion("speed not in", values, "speed");
            return (Criteria) this;
        }

        public Criteria andSpeedBetween(Double value1, Double value2) {
            addCriterion("speed between", value1, value2, "speed");
            return (Criteria) this;
        }

        public Criteria andSpeedNotBetween(Double value1, Double value2) {
            addCriterion("speed not between", value1, value2, "speed");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}