package com.runfast.common.dao.model;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class RunfastShopper implements Serializable {
    private Integer id;

    private Date createTime;

    private Integer creditRating;

    private String distribution;

    private Integer distributionmun;

    private Date endTime;

    private String munber;

    private String name;

    private String password;

    private Date startTime;

    private String userName;

    private String worktoday;

    private String latitude;

    private String longitude;

    private BigDecimal interimremainder;

    private BigDecimal minmonety;

    private BigDecimal remainder;

    private String bdchannelId;

    private String bduserId;

    private Integer bptype;

    private Integer creditLevelid;

    private Integer agentId;

    private String agentName;

    private Integer working;

    private Integer status;

    private String otherId;

    private Integer pushType;

    private Integer teamid;

    private String teamname;

    private Double distance;

    private String alias;

    private BigDecimal coefficient;

    private static final long serialVersionUID = 1L;

}