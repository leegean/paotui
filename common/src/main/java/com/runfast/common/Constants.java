/**
 * Copyright (c) 2005-2012 https://github.com/zhangkaitao
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package com.runfast.common;

public interface Constants {
    /**
     * 操作名称
     */
    String OP_NAME = "operation";


    /**
     * 当前登录的用户
     */
    String CURRENT_ACCOUNT = "CURRENT_ACCOUNT";
    String CURRENT_USERNAME = "username";

    String ENCODING = "UTF-8";
    /**
     * 上次请求地址
     */
    String PREREQUEST_URL = "PREREQUEST_URL";
    /**
     * 上次请求时间
     */
    String PREREQUEST_TIME = "PREREQUEST_TIME";
    /**
     * 非法请求次数
     */
    String MALICIOUS_REQUEST_TIMES = "MALICIOUS_REQUEST_TIMES";
    String CURRENT_USER = "runfast_app_cuser";
    String APP_DRIVER_SESSION = "app_driver";


    public final static String TOKEN = "token";
    public final static String SCRETE = "wdptkc88";
    public final static String ISSUER = "runfast";
    public final static String USER_ID = "userId";


}
