package com.runfast.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.Enumeration;

@Slf4j
public class LogUtils {


    private static final int DEFAULT_MAX_PAYLOAD_LENGTH = Integer.MAX_VALUE;

    private static int maxPayloadLength = DEFAULT_MAX_PAYLOAD_LENGTH;



    public void setMaxPayloadLength(int maxPayloadLength) {
        this.maxPayloadLength = maxPayloadLength;
    }

    public static void loggingRequest(HttpServletRequest request) {
        String uri = request.getRequestURI();
        String queryString = request.getQueryString();
        if (org.apache.commons.lang3.StringUtils.isNotBlank(queryString)) {
            uri = uri + "?" + queryString;
        }

        log.info("{} {}", request.getMethod(), uri);

        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            Enumeration<String> headerValues = request.getHeaders(headerName);
            while (headerValues.hasMoreElements()) {
                String headerValue = headerValues.nextElement();
                log.info("{}:{}", headerName, headerValue);

            }

        }


        String requestPayload = "";
        ContentCachingRequestWrapper wrapper = WebUtils.getNativeRequest(request, ContentCachingRequestWrapper.class);
        if(wrapper!=null){
            byte[] buf = wrapper.getContentAsByteArray();
            if (buf.length > 0) {
                int length = Math.min(buf.length, maxPayloadLength);
                try {
                    requestPayload = new String(buf, 0, length, wrapper.getCharacterEncoding());
                } catch (UnsupportedEncodingException ex) {
                    requestPayload = "[unknown]";
                }
            }
            log.info(requestPayload);
        }




    }

    public static void loggingResponse(HttpServletResponse response, long beginTime) {
        long endTime = System.currentTimeMillis();    //2、结束时间
        int status = response.getStatus();
        log.info("");
        log.info("{} {} {}ms", "HTTP/1.1", status, endTime - beginTime);

        Collection<String> headerNames = response.getHeaderNames();
        for (String headerName : headerNames) {
            Collection<String> headerValues = response.getHeaders(headerName);
            for (String headerValue : headerValues) {
                log.info("{}:{}", headerName, headerValue);
            }
        }
        String requestPayload = "";
        ContentCachingResponseWrapper wrapper = WebUtils.getNativeResponse(response, ContentCachingResponseWrapper.class);
        if(wrapper!=null){
            byte[] buf = wrapper.getContentAsByteArray();
            if (buf.length > 0) {
                int length = Math.min(buf.length, maxPayloadLength);
                try {
                    requestPayload = new String(buf, 0, length, wrapper.getCharacterEncoding());
                } catch (UnsupportedEncodingException ex) {
                    requestPayload = "[unknown]";
                }
            }

            log.info(requestPayload);
        }





    }
}
