package com.runfast.common.utils;

/**
 * @author: lijin
 * @date: 2018年03月21日
 */
public final class DistanceUtil {
    private static final double EARTH_RADIUS = 6371000;

    public static Integer getDistanceBy(double longitude1, double latitude1,
                                        double longitude2, double latitude2) {
        double Lat1 = rad(latitude1);
        double Lat2 = rad(latitude2);
        double a = Lat1 - Lat2;
        double b = rad(longitude1) - rad(longitude2);
        Double s = Math.acos(Math.cos(Lat1)*Math.cos(Lat2)*Math.cos(b)+Math.sin(Lat1)*Math.sin(Lat2));
        s = s * EARTH_RADIUS;
        return s.intValue();
    }

    private static double rad(double d) {
        return d * Math.PI / 180.0;
    }
}
