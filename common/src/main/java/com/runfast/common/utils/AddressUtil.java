package com.runfast.common.utils;

import com.runfast.common.dao.model.RunfastCuseraddress;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author: lijin
 * @date: 2018年06月19日
 */
public abstract class AddressUtil {

    /**
     * 百度转高德
     * @param toAddress
     */
    public static void bd09ToGcj02(RunfastCuseraddress toAddress) {
        double[] to = GPSUtil.bd09_To_Gcj02(Double.valueOf(toAddress.getLatitude()), Double.valueOf(toAddress.getLongitude()));

        toAddress.setLongitude(BigDecimal.valueOf(to[1]).setScale(6, RoundingMode.HALF_UP).doubleValue() + "");
        toAddress.setLatitude(BigDecimal.valueOf(to[0]).setScale(6, RoundingMode.HALF_UP).doubleValue() + "");
    }

    /**
     * 高德转百度
     * @param toAddress
     */
    public static void gcj02ToBd09(RunfastCuseraddress toAddress) {
        double[] to = GPSUtil.gcj02_To_Bd09(Double.valueOf(toAddress.getLatitude()), Double.valueOf(toAddress.getLongitude()));

        toAddress.setLongitude(BigDecimal.valueOf(to[1]).setScale(6, RoundingMode.HALF_UP).doubleValue() + "");
        toAddress.setLatitude(BigDecimal.valueOf(to[0]).setScale(6, RoundingMode.HALF_UP).doubleValue() + "");
    }
}
