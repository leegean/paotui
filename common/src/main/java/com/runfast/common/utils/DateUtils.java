package com.runfast.common.utils;

import com.runfast.waimai.entity.DeliveryRange;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author: lijin
 * @date: 2018年03月01日
 */
public class DateUtils {


    public static boolean nowTimeBetween(Date start, Date end) {

        LocalTime now = LocalTime.now();

        LocalTime startTime;
        if (start == null) {
            startTime = LocalTime.of(0, 0, 0);
        } else {
            Calendar startCalendar = Calendar.getInstance();
            startCalendar.setTime(start);
            startTime = LocalTime.of(startCalendar.get(Calendar.HOUR_OF_DAY), startCalendar.get(Calendar.MINUTE), startCalendar.get(Calendar.SECOND));
        }


        LocalTime endTime;
        if (end == null) {

            endTime = LocalTime.of(23, 59, 59);
        } else {
            Calendar endCalendar = Calendar.getInstance();
            endCalendar.setTime(end);
            endTime = LocalTime.of(endCalendar.get(Calendar.HOUR_OF_DAY), endCalendar.get(Calendar.MINUTE), endCalendar.get(Calendar.SECOND));
        }

        if (now.isAfter(startTime) && now.isBefore(endTime)) return true;
        return false;
    }

    public static LocalTime localTime(Date date) {
        if (date == null) return null;
        Calendar startCalendar = Calendar.getInstance();
        startCalendar.setTime(date);
        return LocalTime.of(startCalendar.get(Calendar.HOUR_OF_DAY), startCalendar.get(Calendar.MINUTE), startCalendar.get(Calendar.SECOND));
    }

    public static boolean isOpen(String weekday,
                                 Date businessStart1, Date businessEnd1,
                                 Date businessStart2, Date businessEnd2,
                                 Date deliveryStart1, Date deliveryEnd1,
                                 Date deliveryStart2, Date deliveryEnd2,
                                 boolean isDelivery, Integer status, Integer statu) {

        if ((status != null && status == 1) || (statu != null && statu == 1)) return false;

        Calendar nowCalendar = Calendar.getInstance();
        int week = nowCalendar.get(Calendar.DAY_OF_WEEK) - 1;
        if (StringUtils.isNotBlank(weekday)) {
            if (!weekday.contains(String.valueOf(week))) return false;
        }


        Date now = new Date();


        Boolean inBusiness1 = (localTime(now).isBefore(localTime(businessEnd1)) || businessEnd1 == null) && (localTime(now).isAfter(localTime(businessStart1)) || businessStart1 == null);
        Boolean inBusiness2 = (localTime(now).isBefore(localTime(businessEnd2)) || businessEnd2 == null) && (localTime(now).isAfter(localTime(businessStart2)) || businessStart2 == null);
        Boolean inDriver1 = (localTime(now).isBefore(localTime(deliveryEnd1)) || deliveryEnd1 == null) && (localTime(now).isAfter(localTime(deliveryStart1)) || deliveryStart1 == null);
        Boolean inDriver2 = (localTime(now).isBefore(localTime(deliveryEnd2)) || deliveryEnd2 == null) && (localTime(now).isAfter(localTime(deliveryStart2)) || deliveryStart2 == null);


        Boolean haveBusinessRange1 = (businessStart1 != null) || (businessEnd1 != null);
        Boolean haveBusinessRange2 = (businessStart2 != null) || (businessEnd2 != null);

        Boolean haveDriverRange1 = (deliveryStart1 != null) || (deliveryEnd1 != null);
        Boolean haveDriverRange2 = (deliveryStart2 != null) || (deliveryEnd2 != null);


        if (isDelivery) {


            if (haveBusinessRange1 && !haveBusinessRange2) {
                return inBusiness1;

            } else if (!haveBusinessRange1 && haveBusinessRange2) {
                return inBusiness2;
            } else if (haveBusinessRange1 && haveBusinessRange2) {
                return inBusiness1 || inBusiness2;
            } else return true;

        } else {
            if (haveBusinessRange1 && !haveBusinessRange2) {
                if (haveDriverRange1 && !haveDriverRange2) {
                    return inBusiness1 && inDriver1;

                } else if (!haveDriverRange1 && haveDriverRange2) {
                    return inBusiness1 && inDriver2;
                } else if (haveDriverRange1 && haveDriverRange2) {
                    return inBusiness1 && (inDriver1 || inDriver2);
                } else return inBusiness1;


            } else if (!haveBusinessRange1 && haveBusinessRange2) {
                if (haveDriverRange1 && !haveDriverRange2) {
                    return inBusiness2 && inDriver1;

                } else if (!haveDriverRange1 && haveDriverRange2) {
                    return inBusiness2 && inDriver2;
                } else if (haveDriverRange1 && haveDriverRange2) {
                    return inBusiness1 && (inDriver1 || inDriver2);
                } else return inBusiness2;
            } else if (haveBusinessRange1 && haveBusinessRange2) {
                if (haveDriverRange1 && !haveDriverRange2) {
                    return (inBusiness1 || inBusiness2) && inDriver1;

                } else if (!haveDriverRange1 && haveDriverRange2) {
                    return (inBusiness1 || inBusiness2) && inDriver2;
                } else if (haveDriverRange1 && haveDriverRange2) {
                    return (inBusiness1 || inBusiness2) && (inDriver1 || inDriver2);
                } else return (inBusiness1 || inBusiness2);
            } else {
                if (haveDriverRange1 && !haveDriverRange2) {
                    return inDriver1;

                } else if (!haveDriverRange1 && haveDriverRange2) {
                    return inDriver2;
                } else if (haveDriverRange1 && haveDriverRange2) {
                    return (inDriver1 || inDriver2);
                } else return true;
            }
        }
    }


    public static List<DeliveryRange> getDeliveryRange(String weekday,
                                                       Date businessStart1, Date businessEnd1,
                                                       Date businessStart2, Date businessEnd2,
                                                       Date deliveryStart1, Date deliveryEnd1,
                                                       Date deliveryStart2, Date deliveryEnd2,
                                                       boolean isDelivery, Integer status, Integer statu, BigDecimal timeCost1, BigDecimal timeCost2) {

        if ((status != null && status == 1) || (statu != null && statu == 1)) return null;

        Calendar nowCalendar = Calendar.getInstance();
        int week = nowCalendar.get(Calendar.DAY_OF_WEEK) - 1;
        if (StringUtils.isNotBlank(weekday)) {
            if (!weekday.contains(String.valueOf(week))) return null;
        }

        LocalTime dayStart = LocalTime.of(0, 0, 0);
        LocalTime dayEnd = LocalTime.of(23, 59, 59);

        LocalTime businessStartTime1 = localTime(businessStart1);
        LocalTime businessEndTime1 = localTime(businessEnd1);

        LocalTime businessStartTime2 = localTime(businessStart2);
        LocalTime businessEndTime2 = localTime(businessEnd2);

        LocalTime deliveryStartTime1 = localTime(deliveryStart1);
        LocalTime deliveryEndTime1 = localTime(deliveryEnd1);

        LocalTime deliveryStartTime2 = localTime(deliveryStart2);
        LocalTime deliveryEndTime2 = localTime(deliveryEnd2);


        Boolean haveBusinessRange1 = (businessStart1 != null) || (businessEnd1 != null);
        Boolean haveBusinessRange2 = (businessStart2 != null) || (businessEnd2 != null);

        Boolean haveDriverRange1 = (deliveryStart1 != null) || (deliveryEnd1 != null);
        Boolean haveDriverRange2 = (deliveryStart2 != null) || (deliveryEnd2 != null);


        List<DeliveryRange> rangeList = new ArrayList<>();
        if (isDelivery) {
            if (haveBusinessRange1 && !haveBusinessRange2) {
                if (businessStart1 == null && businessEnd1 != null) {
                    businessStartTime1 = dayStart;
                } else if (businessStart1 != null && businessEnd1 == null) {
                    businessEndTime1 = dayEnd;
                }

                rangeList.add(new DeliveryRange(businessStartTime1, businessEndTime1, timeCost1));

            } else if (!haveBusinessRange1 && haveBusinessRange2) {
                if (businessStart2 == null && businessEnd2 != null) {
                    businessStartTime2 = dayStart;
                } else if (businessStart2 != null && businessEnd2 == null) {
                    businessEndTime2 = dayEnd;
                }

                rangeList.add(new DeliveryRange(businessStartTime2, businessEndTime2, timeCost1));
            } else if (haveBusinessRange1 && haveBusinessRange2) {
                if (businessStart1 == null && businessEnd1 != null) {
                    businessStartTime1 = dayStart;
                } else if (businessStart1 != null && businessEnd1 == null) {
                    businessEndTime1 = dayEnd;
                }

                rangeList.add(new DeliveryRange(businessStartTime1, businessEndTime1, timeCost1));

                if (businessStart2 == null && businessEnd2 != null) {
                    businessStartTime2 = dayStart;
                } else if (businessStart2 != null && businessEnd2 == null) {
                    businessEndTime2 = dayEnd;
                }

                rangeList.add(new DeliveryRange(businessStartTime2, businessEndTime2, timeCost1));
            } else {
                rangeList.add(new DeliveryRange(dayStart, dayEnd, timeCost1));
            }

        } else {
            if (haveBusinessRange1 && !haveBusinessRange2) {

                if (businessStart1 == null && businessEnd1 != null) {
                    businessStartTime1 = dayStart;
                } else if (businessStart1 != null && businessEnd1 == null) {
                    businessEndTime1 = dayEnd;
                }

                if (haveDriverRange1 && !haveDriverRange2) {
                    if (deliveryStart1 == null && deliveryEnd1 != null) {
                        deliveryStartTime1 = dayStart;
                    } else if (deliveryStart1 != null && deliveryEnd1 == null) {
                        deliveryEndTime1 = dayEnd;
                    }

                    LocalTime rStart = null;
                    LocalTime rEnd = null;
                    if(!businessStartTime1.isAfter(deliveryStartTime1)&&!businessEndTime1.isBefore(deliveryStartTime1)&&!businessEndTime1.isAfter(deliveryEndTime1)){
                        rStart = deliveryStartTime1;
                        rEnd = businessEndTime1;
                        rangeList.add(new DeliveryRange(rStart, rEnd, timeCost1));
                    }else if(!businessStartTime1.isAfter(deliveryStartTime1)&&!businessEndTime1.isBefore(deliveryEndTime1)){
                        rStart = deliveryStartTime1;
                        rEnd = deliveryEndTime1;
                        rangeList.add(new DeliveryRange(rStart, rEnd, timeCost1));
                    }else if(!businessStartTime1.isBefore(deliveryStartTime1)&&!businessEndTime1.isAfter(deliveryEndTime1)){
                        rStart = businessStartTime1;
                        rEnd = businessEndTime1;
                        rangeList.add(new DeliveryRange(rStart, rEnd, timeCost1));
                    }else if(!businessStartTime1.isBefore(deliveryStartTime1)&&!businessStartTime1.isAfter(deliveryEndTime1)&&!businessEndTime1.isBefore(deliveryEndTime1)){
                        rStart = businessStartTime1;
                        rEnd = deliveryEndTime1;
                        rangeList.add(new DeliveryRange(rStart, rEnd, timeCost1));
                    }


                } else if (!haveDriverRange1 && haveDriverRange2) {
                    if (deliveryStart2 == null && deliveryEnd2 != null) {
                        deliveryStartTime2 = dayStart;
                    } else if (deliveryStart2 != null && deliveryEnd2 == null) {
                        deliveryEndTime2 = dayEnd;
                    }

                    LocalTime rStart = null;
                    LocalTime rEnd = null;
                    if(!businessStartTime1.isAfter(deliveryStartTime2)&&!businessEndTime1.isBefore(deliveryStartTime2)&&!businessEndTime1.isAfter(deliveryEndTime2)){
                        rStart = deliveryStartTime2;
                        rEnd = businessEndTime1;
                        rangeList.add(new DeliveryRange(rStart, rEnd, timeCost2));
                    }else if(!businessStartTime1.isAfter(deliveryStartTime2)&&!businessEndTime1.isBefore(deliveryEndTime2)){
                        rStart = deliveryStartTime2;
                        rEnd = deliveryEndTime2;
                        rangeList.add(new DeliveryRange(rStart, rEnd, timeCost2));
                    }else if(!businessStartTime1.isBefore(deliveryStartTime2)&&!businessEndTime1.isAfter(deliveryEndTime2)){
                        rStart = businessStartTime1;
                        rEnd = businessEndTime1;
                        rangeList.add(new DeliveryRange(rStart, rEnd, timeCost2));
                    }else if(!businessStartTime1.isBefore(deliveryStartTime2)&&!businessStartTime1.isAfter(deliveryEndTime2)&&!businessEndTime1.isBefore(deliveryEndTime2)){
                        rStart = businessStartTime1;
                        rEnd = deliveryEndTime2;
                        rangeList.add(new DeliveryRange(rStart, rEnd, timeCost2));
                    }
                } else if (haveDriverRange1 && haveDriverRange2) {
                    if (deliveryStart1 == null && deliveryEnd1 != null) {
                        deliveryStartTime1 = dayStart;
                    } else if (deliveryStart1 != null && deliveryEnd1 == null) {
                        deliveryEndTime1 = dayEnd;
                    }

                    LocalTime rStart1 = null;
                    LocalTime rEnd1 = null;
                    if(!businessStartTime1.isAfter(deliveryStartTime1)&&!businessEndTime1.isBefore(deliveryStartTime1)&&!businessEndTime1.isAfter(deliveryEndTime1)){
                        rStart1 = deliveryStartTime1;
                        rEnd1 = businessEndTime1;
                        rangeList.add(new DeliveryRange(rStart1, rEnd1, timeCost1));
                    }else if(!businessStartTime1.isAfter(deliveryStartTime1)&&!businessEndTime1.isBefore(deliveryEndTime1)){
                        rStart1 = deliveryStartTime1;
                        rEnd1 = deliveryEndTime1;
                        rangeList.add(new DeliveryRange(rStart1, rEnd1, timeCost1));
                    }else if(!businessStartTime1.isBefore(deliveryStartTime1)&&!businessEndTime1.isAfter(deliveryEndTime1)){
                        rStart1 = businessStartTime1;
                        rEnd1 = businessEndTime1;
                        rangeList.add(new DeliveryRange(rStart1, rEnd1, timeCost1));
                    }else if(!businessStartTime1.isBefore(deliveryStartTime1)&&!businessStartTime1.isAfter(deliveryEndTime1)&&!businessEndTime1.isBefore(deliveryEndTime1)){
                        rStart1 = businessStartTime1;
                        rEnd1 = deliveryEndTime1;
                        rangeList.add(new DeliveryRange(rStart1, rEnd1, timeCost1));
                    }

                    if (deliveryStart2 == null && deliveryEnd2 != null) {
                        deliveryStartTime2 = dayStart;
                    } else if (deliveryStart2 != null && deliveryEnd2 == null) {
                        deliveryEndTime2 = dayEnd;
                    }

                    LocalTime rStart2 = null;
                    LocalTime rEnd2 = null;
                    if(!businessStartTime1.isAfter(deliveryStartTime2)&&!businessEndTime1.isBefore(deliveryStartTime2)&&!businessEndTime1.isAfter(deliveryEndTime2)){
                        rStart2 = deliveryStartTime2;
                        rEnd2 = businessEndTime1;
                        rangeList.add(new DeliveryRange(rStart2, rEnd2, timeCost2));
                    }else if(!businessStartTime1.isAfter(deliveryStartTime2)&&!businessEndTime1.isBefore(deliveryEndTime2)){
                        rStart2 = deliveryStartTime2;
                        rEnd2 = deliveryEndTime2;
                        rangeList.add(new DeliveryRange(rStart2, rEnd2, timeCost2));
                    }else if(!businessStartTime1.isBefore(deliveryStartTime2)&&!businessEndTime1.isAfter(deliveryEndTime2)){
                        rStart2 = businessStartTime1;
                        rEnd2 = businessEndTime1;
                        rangeList.add(new DeliveryRange(rStart2, rEnd2, timeCost2));
                    }else if(!businessStartTime1.isBefore(deliveryStartTime2)&&!businessStartTime1.isAfter(deliveryEndTime2)&&!businessEndTime1.isBefore(deliveryEndTime2)){
                        rStart2 = businessStartTime1;
                        rEnd2 = deliveryEndTime2;
                        rangeList.add(new DeliveryRange(rStart2, rEnd2, timeCost2));
                    }
                } else {
                    rangeList.add(new DeliveryRange(businessStartTime1, businessEndTime1, timeCost1));
                }


            } else if (!haveBusinessRange1 && haveBusinessRange2) {
                if (businessStart2 == null && businessEnd2 != null) {
                    businessStartTime2 = dayStart;
                } else if (businessStart2 != null && businessEnd2 == null) {
                    businessEndTime2 = dayEnd;
                }

                if (haveDriverRange1 && !haveDriverRange2) {
                    if (deliveryStart1 == null && deliveryEnd1 != null) {
                        deliveryStartTime1 = dayStart;
                    } else if (deliveryStart1 != null && deliveryEnd1 == null) {
                        deliveryEndTime1 = dayEnd;
                    }

                    LocalTime rStart = null;
                    LocalTime rEnd = null;
                    if(!businessStartTime2.isAfter(deliveryStartTime1)&&!businessEndTime2.isBefore(deliveryStartTime1)&&!businessEndTime2.isAfter(deliveryEndTime1)){
                        rStart = deliveryStartTime1;
                        rEnd = businessEndTime2;
                        rangeList.add(new DeliveryRange(rStart, rEnd, timeCost1));
                    }else if(!businessStartTime2.isAfter(deliveryStartTime1)&&!businessEndTime2.isBefore(deliveryEndTime1)){
                        rStart = deliveryStartTime1;
                        rEnd = deliveryEndTime1;
                        rangeList.add(new DeliveryRange(rStart, rEnd, timeCost1));
                    }else if(!businessStartTime2.isBefore(deliveryStartTime1)&&!businessEndTime2.isAfter(deliveryEndTime1)){
                        rStart = businessStartTime2;
                        rEnd = businessEndTime2;
                        rangeList.add(new DeliveryRange(rStart, rEnd, timeCost1));
                    }else if(!businessStartTime2.isBefore(deliveryStartTime1)&&!businessStartTime2.isAfter(deliveryEndTime1)&&!businessEndTime2.isBefore(deliveryEndTime1)){
                        rStart = businessStartTime2;
                        rEnd = deliveryEndTime1;
                        rangeList.add(new DeliveryRange(rStart, rEnd, timeCost1));
                    }


                } else if (!haveDriverRange1 && haveDriverRange2) {
                    if (deliveryStart2 == null && deliveryEnd2 != null) {
                        deliveryStartTime2 = dayStart;
                    } else if (deliveryStart2 != null && deliveryEnd2 == null) {
                        deliveryEndTime2 = dayEnd;
                    }

                    LocalTime rStart = null;
                    LocalTime rEnd = null;
                    if(!businessStartTime2.isAfter(deliveryStartTime2)&&!businessEndTime2.isBefore(deliveryStartTime2)&&!businessEndTime2.isAfter(deliveryEndTime2)){
                        rStart = deliveryStartTime2;
                        rEnd = businessEndTime2;
                        rangeList.add(new DeliveryRange(rStart, rEnd, timeCost2));
                    }else if(!businessStartTime2.isAfter(deliveryStartTime2)&&!businessEndTime2.isBefore(deliveryEndTime2)){
                        rStart = deliveryStartTime2;
                        rEnd = deliveryEndTime2;
                        rangeList.add(new DeliveryRange(rStart, rEnd, timeCost2));
                    }else if(!businessStartTime2.isBefore(deliveryStartTime2)&&!businessEndTime2.isAfter(deliveryEndTime2)){
                        rStart = businessStartTime2;
                        rEnd = businessEndTime2;
                        rangeList.add(new DeliveryRange(rStart, rEnd, timeCost2));
                    }else if(!businessStartTime2.isBefore(deliveryStartTime2)&&!businessStartTime2.isAfter(deliveryEndTime2)&&!businessEndTime2.isBefore(deliveryEndTime2)){
                        rStart = businessStartTime2;
                        rEnd = deliveryEndTime2;
                        rangeList.add(new DeliveryRange(rStart, rEnd, timeCost2));
                    }
                } else if (haveDriverRange1 && haveDriverRange2) {
                    if (deliveryStart1 == null && deliveryEnd1 != null) {
                        deliveryStartTime1 = dayStart;
                    } else if (deliveryStart1 != null && deliveryEnd1 == null) {
                        deliveryEndTime1 = dayEnd;
                    }

                    LocalTime rStart1 = null;
                    LocalTime rEnd1 = null;
                    if(!businessStartTime2.isAfter(deliveryStartTime1)&&!businessEndTime2.isBefore(deliveryStartTime1)&&!businessEndTime2.isAfter(deliveryEndTime1)){
                        rStart1 = deliveryStartTime1;
                        rEnd1 = businessEndTime2;
                        rangeList.add(new DeliveryRange(rStart1, rEnd1, timeCost1));
                    }else if(!businessStartTime2.isAfter(deliveryStartTime1)&&!businessEndTime2.isBefore(deliveryEndTime1)){
                        rStart1 = deliveryStartTime1;
                        rEnd1 = deliveryEndTime1;
                        rangeList.add(new DeliveryRange(rStart1, rEnd1, timeCost1));
                    }else if(!businessStartTime2.isBefore(deliveryStartTime1)&&!businessEndTime2.isAfter(deliveryEndTime1)){
                        rStart1 = businessStartTime2;
                        rEnd1 = businessEndTime2;
                        rangeList.add(new DeliveryRange(rStart1, rEnd1, timeCost1));
                    }else if(!businessStartTime2.isBefore(deliveryStartTime1)&&!businessStartTime2.isAfter(deliveryEndTime1)&&!businessEndTime2.isBefore(deliveryEndTime1)){
                        rStart1 = businessStartTime2;
                        rEnd1 = deliveryEndTime1;
                        rangeList.add(new DeliveryRange(rStart1, rEnd1, timeCost1));
                    }

                    if (deliveryStart2 == null && deliveryEnd2 != null) {
                        deliveryStartTime2 = dayStart;
                    } else if (deliveryStart2 != null && deliveryEnd2 == null) {
                        deliveryEndTime2 = dayEnd;
                    }

                    LocalTime rStart2 = null;
                    LocalTime rEnd2 = null;
                    if(!businessStartTime2.isAfter(deliveryStartTime2)&&!businessEndTime2.isBefore(deliveryStartTime2)&&!businessEndTime2.isAfter(deliveryEndTime2)){
                        rStart2 = deliveryStartTime2;
                        rEnd2 = businessEndTime2;
                        rangeList.add(new DeliveryRange(rStart2, rEnd2, timeCost2));
                    }else if(!businessStartTime2.isAfter(deliveryStartTime2)&&!businessEndTime2.isBefore(deliveryEndTime2)){
                        rStart2 = deliveryStartTime2;
                        rEnd2 = deliveryEndTime2;
                        rangeList.add(new DeliveryRange(rStart2, rEnd2, timeCost2));
                    }else if(!businessStartTime2.isBefore(deliveryStartTime2)&&!businessEndTime2.isAfter(deliveryEndTime2)){
                        rStart2 = businessStartTime2;
                        rEnd2 = businessEndTime2;
                        rangeList.add(new DeliveryRange(rStart2, rEnd2, timeCost2));
                    }else if(!businessStartTime2.isBefore(deliveryStartTime2)&&!businessStartTime2.isAfter(deliveryEndTime2)&&!businessEndTime2.isBefore(deliveryEndTime2)){
                        rStart2 = businessStartTime2;
                        rEnd2 = deliveryEndTime2;
                        rangeList.add(new DeliveryRange(rStart2, rEnd2, timeCost2));
                    }
                } else {
                    rangeList.add(new DeliveryRange(businessStartTime2, businessEndTime2, timeCost1));
                }


            } else if (haveBusinessRange1 && haveBusinessRange2) {

                    if (businessStart1 == null && businessEnd1 != null) {
                        businessStartTime1 = dayStart;
                    } else if (businessStart1 != null && businessEnd1 == null) {
                        businessEndTime1 = dayEnd;
                    }

                    if (haveDriverRange1 && !haveDriverRange2) {
                        if (deliveryStart1 == null && deliveryEnd1 != null) {
                            deliveryStartTime1 = dayStart;
                        } else if (deliveryStart1 != null && deliveryEnd1 == null) {
                            deliveryEndTime1 = dayEnd;
                        }

                        LocalTime rStart = null;
                        LocalTime rEnd = null;
                        if(!businessStartTime1.isAfter(deliveryStartTime1)&&!businessEndTime1.isBefore(deliveryStartTime1)&&!businessEndTime1.isAfter(deliveryEndTime1)){
                            rStart = deliveryStartTime1;
                            rEnd = businessEndTime1;
                            rangeList.add(new DeliveryRange(rStart, rEnd, timeCost1));
                        }else if(!businessStartTime1.isAfter(deliveryStartTime1)&&!businessEndTime1.isBefore(deliveryEndTime1)){
                            rStart = deliveryStartTime1;
                            rEnd = deliveryEndTime1;
                            rangeList.add(new DeliveryRange(rStart, rEnd, timeCost1));
                        }else if(!businessStartTime1.isBefore(deliveryStartTime1)&&!businessEndTime1.isAfter(deliveryEndTime1)){
                            rStart = businessStartTime1;
                            rEnd = businessEndTime1;
                            rangeList.add(new DeliveryRange(rStart, rEnd, timeCost1));
                        }else if(!businessStartTime1.isBefore(deliveryStartTime1)&&!businessStartTime1.isAfter(deliveryEndTime1)&&!businessEndTime1.isBefore(deliveryEndTime1)){
                            rStart = businessStartTime1;
                            rEnd = deliveryEndTime1;
                            rangeList.add(new DeliveryRange(rStart, rEnd, timeCost1));
                        }


                    } else if (!haveDriverRange1 && haveDriverRange2) {
                        if (deliveryStart2 == null && deliveryEnd2 != null) {
                            deliveryStartTime2 = dayStart;
                        } else if (deliveryStart2 != null && deliveryEnd2 == null) {
                            deliveryEndTime2 = dayEnd;
                        }

                        LocalTime rStart = null;
                        LocalTime rEnd = null;
                        if(!businessStartTime1.isAfter(deliveryStartTime2)&&!businessEndTime1.isBefore(deliveryStartTime2)&&!businessEndTime1.isAfter(deliveryEndTime2)){
                            rStart = deliveryStartTime2;
                            rEnd = businessEndTime1;
                            rangeList.add(new DeliveryRange(rStart, rEnd, timeCost2));
                        }else if(!businessStartTime1.isAfter(deliveryStartTime2)&&!businessEndTime1.isBefore(deliveryEndTime2)){
                            rStart = deliveryStartTime2;
                            rEnd = deliveryEndTime2;
                            rangeList.add(new DeliveryRange(rStart, rEnd, timeCost2));
                        }else if(!businessStartTime1.isBefore(deliveryStartTime2)&&!businessEndTime1.isAfter(deliveryEndTime2)){
                            rStart = businessStartTime1;
                            rEnd = businessEndTime1;
                            rangeList.add(new DeliveryRange(rStart, rEnd, timeCost2));
                        }else if(!businessStartTime1.isBefore(deliveryStartTime2)&&!businessStartTime1.isAfter(deliveryEndTime2)&&!businessEndTime1.isBefore(deliveryEndTime2)){
                            rStart = businessStartTime1;
                            rEnd = deliveryEndTime2;
                            rangeList.add(new DeliveryRange(rStart, rEnd, timeCost2));
                        }
                    } else if (haveDriverRange1 && haveDriverRange2) {
                        if (deliveryStart1 == null && deliveryEnd1 != null) {
                            deliveryStartTime1 = dayStart;
                        } else if (deliveryStart1 != null && deliveryEnd1 == null) {
                            deliveryEndTime1 = dayEnd;
                        }

                        LocalTime rStart1 = null;
                        LocalTime rEnd1 = null;
                        if(!businessStartTime1.isAfter(deliveryStartTime1)&&!businessEndTime1.isBefore(deliveryStartTime1)&&!businessEndTime1.isAfter(deliveryEndTime1)){
                            rStart1 = deliveryStartTime1;
                            rEnd1 = businessEndTime1;
                            rangeList.add(new DeliveryRange(rStart1, rEnd1, timeCost1));
                        }else if(!businessStartTime1.isAfter(deliveryStartTime1)&&!businessEndTime1.isBefore(deliveryEndTime1)){
                            rStart1 = deliveryStartTime1;
                            rEnd1 = deliveryEndTime1;
                            rangeList.add(new DeliveryRange(rStart1, rEnd1, timeCost1));
                        }else if(!businessStartTime1.isBefore(deliveryStartTime1)&&!businessEndTime1.isAfter(deliveryEndTime1)){
                            rStart1 = businessStartTime1;
                            rEnd1 = businessEndTime1;
                            rangeList.add(new DeliveryRange(rStart1, rEnd1, timeCost1));
                        }else if(!businessStartTime1.isBefore(deliveryStartTime1)&&!businessStartTime1.isAfter(deliveryEndTime1)&&!businessEndTime1.isBefore(deliveryEndTime1)){
                            rStart1 = businessStartTime1;
                            rEnd1 = deliveryEndTime1;
                            rangeList.add(new DeliveryRange(rStart1, rEnd1, timeCost1));
                        }

                        if (deliveryStart2 == null && deliveryEnd2 != null) {
                            deliveryStartTime2 = dayStart;
                        } else if (deliveryStart2 != null && deliveryEnd2 == null) {
                            deliveryEndTime2 = dayEnd;
                        }

                        LocalTime rStart2 = null;
                        LocalTime rEnd2 = null;
                        if(!businessStartTime1.isAfter(deliveryStartTime2)&&!businessEndTime1.isBefore(deliveryStartTime2)&&!businessEndTime1.isAfter(deliveryEndTime2)){
                            rStart2 = deliveryStartTime2;
                            rEnd2 = businessEndTime1;
                            rangeList.add(new DeliveryRange(rStart2, rEnd2, timeCost2));
                        }else if(!businessStartTime1.isAfter(deliveryStartTime2)&&!businessEndTime1.isBefore(deliveryEndTime2)){
                            rStart2 = deliveryStartTime2;
                            rEnd2 = deliveryEndTime2;
                            rangeList.add(new DeliveryRange(rStart2, rEnd2, timeCost2));
                        }else if(!businessStartTime1.isBefore(deliveryStartTime2)&&!businessEndTime1.isAfter(deliveryEndTime2)){
                            rStart2 = businessStartTime1;
                            rEnd2 = businessEndTime1;
                            rangeList.add(new DeliveryRange(rStart2, rEnd2, timeCost2));
                        }else if(!businessStartTime1.isBefore(deliveryStartTime2)&&!businessStartTime1.isAfter(deliveryEndTime2)&&!businessEndTime1.isBefore(deliveryEndTime2)){
                            rStart2 = businessStartTime1;
                            rEnd2 = deliveryEndTime2;
                            rangeList.add(new DeliveryRange(rStart2, rEnd2, timeCost2));
                        }
                    } else {
                        rangeList.add(new DeliveryRange(businessStartTime1, businessEndTime1, timeCost1));
                    }


                if (businessStart2 == null && businessEnd2 != null) {
                    businessStartTime2 = dayStart;
                } else if (businessStart2 != null && businessEnd2 == null) {
                    businessEndTime2 = dayEnd;
                }

                if (haveDriverRange1 && !haveDriverRange2) {
                    if (deliveryStart1 == null && deliveryEnd1 != null) {
                        deliveryStartTime1 = dayStart;
                    } else if (deliveryStart1 != null && deliveryEnd1 == null) {
                        deliveryEndTime1 = dayEnd;
                    }

                    LocalTime rStart = null;
                    LocalTime rEnd = null;
                    if(!businessStartTime2.isAfter(deliveryStartTime1)&&!businessEndTime2.isBefore(deliveryStartTime1)&&!businessEndTime2.isAfter(deliveryEndTime1)){
                        rStart = deliveryStartTime1;
                        rEnd = businessEndTime2;
                        rangeList.add(new DeliveryRange(rStart, rEnd, timeCost1));
                    }else if(!businessStartTime2.isAfter(deliveryStartTime1)&&!businessEndTime2.isBefore(deliveryEndTime1)){
                        rStart = deliveryStartTime1;
                        rEnd = deliveryEndTime1;
                        rangeList.add(new DeliveryRange(rStart, rEnd, timeCost1));
                    }else if(!businessStartTime2.isBefore(deliveryStartTime1)&&!businessEndTime2.isAfter(deliveryEndTime1)){
                        rStart = businessStartTime2;
                        rEnd = businessEndTime2;
                        rangeList.add(new DeliveryRange(rStart, rEnd, timeCost1));
                    }else if(!businessStartTime2.isBefore(deliveryStartTime1)&&!businessStartTime2.isAfter(deliveryEndTime1)&&!businessEndTime2.isBefore(deliveryEndTime1)){
                        rStart = businessStartTime2;
                        rEnd = deliveryEndTime1;
                        rangeList.add(new DeliveryRange(rStart, rEnd, timeCost1));
                    }


                } else if (!haveDriverRange1 && haveDriverRange2) {
                    if (deliveryStart2 == null && deliveryEnd2 != null) {
                        deliveryStartTime2 = dayStart;
                    } else if (deliveryStart2 != null && deliveryEnd2 == null) {
                        deliveryEndTime2 = dayEnd;
                    }

                    LocalTime rStart = null;
                    LocalTime rEnd = null;
                    if(!businessStartTime2.isAfter(deliveryStartTime2)&&!businessEndTime2.isBefore(deliveryStartTime2)&&!businessEndTime2.isAfter(deliveryEndTime2)){
                        rStart = deliveryStartTime2;
                        rEnd = businessEndTime2;
                        rangeList.add(new DeliveryRange(rStart, rEnd, timeCost2));
                    }else if(!businessStartTime2.isAfter(deliveryStartTime2)&&!businessEndTime2.isBefore(deliveryEndTime2)){
                        rStart = deliveryStartTime2;
                        rEnd = deliveryEndTime2;
                        rangeList.add(new DeliveryRange(rStart, rEnd, timeCost2));
                    }else if(!businessStartTime2.isBefore(deliveryStartTime2)&&!businessEndTime2.isAfter(deliveryEndTime2)){
                        rStart = businessStartTime2;
                        rEnd = businessEndTime2;
                        rangeList.add(new DeliveryRange(rStart, rEnd, timeCost2));
                    }else if(!businessStartTime2.isBefore(deliveryStartTime2)&&!businessStartTime2.isAfter(deliveryEndTime2)&&!businessEndTime2.isBefore(deliveryEndTime2)){
                        rStart = businessStartTime2;
                        rEnd = deliveryEndTime2;
                        rangeList.add(new DeliveryRange(rStart, rEnd, timeCost2));
                    }
                } else if (haveDriverRange1 && haveDriverRange2) {
                    if (deliveryStart1 == null && deliveryEnd1 != null) {
                        deliveryStartTime1 = dayStart;
                    } else if (deliveryStart1 != null && deliveryEnd1 == null) {
                        deliveryEndTime1 = dayEnd;
                    }

                    LocalTime rStart1 = null;
                    LocalTime rEnd1 = null;
                    if(!businessStartTime2.isAfter(deliveryStartTime1)&&!businessEndTime2.isBefore(deliveryStartTime1)&&!businessEndTime2.isAfter(deliveryEndTime1)){
                        rStart1 = deliveryStartTime1;
                        rEnd1 = businessEndTime2;
                        rangeList.add(new DeliveryRange(rStart1, rEnd1, timeCost1));
                    }else if(!businessStartTime2.isAfter(deliveryStartTime1)&&!businessEndTime2.isBefore(deliveryEndTime1)){
                        rStart1 = deliveryStartTime1;
                        rEnd1 = deliveryEndTime1;
                        rangeList.add(new DeliveryRange(rStart1, rEnd1, timeCost1));
                    }else if(!businessStartTime2.isBefore(deliveryStartTime1)&&!businessEndTime2.isAfter(deliveryEndTime1)){
                        rStart1 = businessStartTime2;
                        rEnd1 = businessEndTime2;
                        rangeList.add(new DeliveryRange(rStart1, rEnd1, timeCost1));
                    }else if(!businessStartTime2.isBefore(deliveryStartTime1)&&!businessStartTime2.isAfter(deliveryEndTime1)&&!businessEndTime2.isBefore(deliveryEndTime1)){
                        rStart1 = businessStartTime2;
                        rEnd1 = deliveryEndTime1;
                        rangeList.add(new DeliveryRange(rStart1, rEnd1, timeCost1));
                    }

                    if (deliveryStart2 == null && deliveryEnd2 != null) {
                        deliveryStartTime2 = dayStart;
                    } else if (deliveryStart2 != null && deliveryEnd2 == null) {
                        deliveryEndTime2 = dayEnd;
                    }

                    LocalTime rStart2 = null;
                    LocalTime rEnd2 = null;
                    if(!businessStartTime2.isAfter(deliveryStartTime2)&&!businessEndTime2.isBefore(deliveryStartTime2)&&!businessEndTime2.isAfter(deliveryEndTime2)){
                        rStart2 = deliveryStartTime2;
                        rEnd2 = businessEndTime2;
                        rangeList.add(new DeliveryRange(rStart2, rEnd2, timeCost2));
                    }else if(!businessStartTime2.isAfter(deliveryStartTime2)&&!businessEndTime2.isBefore(deliveryEndTime2)){
                        rStart2 = deliveryStartTime2;
                        rEnd2 = deliveryEndTime2;
                        rangeList.add(new DeliveryRange(rStart2, rEnd2, timeCost2));
                    }else if(!businessStartTime2.isBefore(deliveryStartTime2)&&!businessEndTime2.isAfter(deliveryEndTime2)){
                        rStart2 = businessStartTime2;
                        rEnd2 = businessEndTime2;
                        rangeList.add(new DeliveryRange(rStart2, rEnd2, timeCost2));
                    }else if(!businessStartTime2.isBefore(deliveryStartTime2)&&!businessStartTime2.isAfter(deliveryEndTime2)&&!businessEndTime2.isBefore(deliveryEndTime2)){
                        rStart2 = businessStartTime2;
                        rEnd2 = deliveryEndTime2;
                        rangeList.add(new DeliveryRange(rStart2, rEnd2, timeCost2));
                    }
                } else {
                    rangeList.add(new DeliveryRange(businessStartTime2, businessEndTime2, timeCost1));
                }

            } else {
                if (haveDriverRange1 && !haveDriverRange2) {
                    if (deliveryStart1 == null && deliveryEnd1 != null) {
                        deliveryStartTime1 = dayStart;
                    } else if (deliveryStart1 != null && deliveryEnd1 == null) {
                        deliveryEndTime1 = dayEnd;
                    }


                    rangeList.add(new DeliveryRange(deliveryStartTime1, deliveryEndTime1, timeCost1));


                } else if (!haveDriverRange1 && haveDriverRange2) {
                    if (deliveryStart2 == null && deliveryEnd2 != null) {
                        deliveryStartTime2 = dayStart;
                    } else if (deliveryStart2 != null && deliveryEnd2 == null) {
                        deliveryEndTime2 = dayEnd;
                    }

                    rangeList.add(new DeliveryRange(deliveryStartTime2, deliveryEndTime2, timeCost2));
                } else if (haveDriverRange1 && haveDriverRange2) {
                    if (deliveryStart1 == null && deliveryEnd1 != null) {
                        deliveryStartTime1 = dayStart;
                    } else if (deliveryStart1 != null && deliveryEnd1 == null) {
                        deliveryEndTime1 = dayEnd;
                    }

                    rangeList.add(new DeliveryRange(deliveryStartTime1, deliveryEndTime1, timeCost1));

                    if (deliveryStart2 == null && deliveryEnd2 != null) {
                        deliveryStartTime2 = dayStart;
                    } else if (deliveryStart2 != null && deliveryEnd2 == null) {
                        deliveryEndTime2 = dayEnd;
                    }
                    rangeList.add(new DeliveryRange(deliveryStartTime2, deliveryEndTime2, timeCost2));
                } else {
                    rangeList.add(new DeliveryRange(dayStart, dayEnd, timeCost1));
                }
            }
        }

        return rangeList;
    }

}
