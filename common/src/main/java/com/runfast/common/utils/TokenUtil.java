package com.runfast.common.utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.Claim;

import static com.auth0.jwt.JWT.decode;

/**
 * @author: lijin
 * @date: 2018年04月24日
 */
public abstract  class TokenUtil {

    public final static String TOKEN="token";
    public final static String SCRETE="wdptkc88";
    public final static String ISSUER = "runfast";
    public final static String USER_ID = "userId";

    public static Integer getUserId(String token) {

        JWT decode = decode(token);
        Claim claim = decode.getClaim(TokenUtil.USER_ID);
        Integer userId = claim.asInt();
        return userId;
    }
}
