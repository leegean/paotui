package com.runfast.common.utils;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author: lijin
 * @date: 2018年03月01日
 */
public class JsonUtils {
    private static final ObjectMapper mapper = new ObjectMapper();

    public static ObjectMapper getMapper() {
        return mapper;
    }
}
