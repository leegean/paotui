package com.runfast.common.utils;

import com.gxuc.runfast.bean.Cuser;
import com.gxuc.runfast.bean.Shopper;
import com.runfast.common.Constants;
import com.runfast.common.exception.BaseException;
import com.runfast.common.web.entity.ResultCode;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

/**
 * @author: lijin
 * @date: 2018年03月21日
 */
public final class SessionContext {
    public static Cuser getCurrentUser() {
        Cuser cuser = (Cuser) RequestContextHolder.currentRequestAttributes().getAttribute(Constants.CURRENT_USER, RequestAttributes.SCOPE_SESSION);
        if (cuser == null) throw new BaseException(ResultCode.ACCOUNT_RELOGIN);
        return cuser;
    }

    public static Shopper getCurrentDriver() {
        Shopper shopper = (Shopper) RequestContextHolder.currentRequestAttributes().getAttribute(Constants.APP_DRIVER_SESSION, RequestAttributes.SCOPE_SESSION);
        if (shopper == null) throw new BaseException(ResultCode.ACCOUNT_RELOGIN);
        return shopper;
    }
}
