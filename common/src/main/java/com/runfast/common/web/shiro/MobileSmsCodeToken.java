package com.runfast.common.web.shiro;

import org.apache.shiro.authc.AuthenticationToken;

/**
 * @author: lijin
 * @date: 2018年05月07日
 */
public class MobileSmsCodeToken implements AuthenticationToken {
    private String mobile;

    private String smsCode;

    public MobileSmsCodeToken(String mobile, String smsCode) {
        this.mobile = mobile;
        this.smsCode = smsCode;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getSmsCode() {
        return smsCode;
    }

    public void setSmsCode(String smsCode) {
        this.smsCode = smsCode;
    }

    @Override
    public Object getPrincipal() {
        return getMobile();
    }

    @Override
    public Object getCredentials() {
        return getSmsCode();
    }
}
