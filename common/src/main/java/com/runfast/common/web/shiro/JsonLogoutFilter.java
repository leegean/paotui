package com.runfast.common.web.shiro;

import org.apache.shiro.web.filter.authc.LogoutFilter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class JsonLogoutFilter extends LogoutFilter {
    public static final String DEFAULT_REDIRECT_URL = "/api/account/logout";

    /**
     * The URL to where the user will be redirected after logout.
     */
    private String redirectUrl = DEFAULT_REDIRECT_URL;

    @Override
    protected void issueRedirect(ServletRequest request, ServletResponse response, String redirectUrl) throws Exception {
        request.getServletContext().getRequestDispatcher(redirectUrl).forward(request, response);
    }
}
