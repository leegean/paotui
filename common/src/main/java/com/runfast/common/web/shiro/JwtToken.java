package com.runfast.common.web.shiro;

import org.apache.shiro.authc.AuthenticationToken;

/**
 * @author: lijin

 */
public interface JwtToken extends AuthenticationToken {

     LoginMode getLoginMode();
}
