package com.runfast.common.web.shiro;

import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.web.util.WebUtils;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * @author: lijin
 * @date: 2018年05月07日
 */
public class JwtPasswordAuthFilter extends JwtAuthenticationFilter{

    private String mobileParam = "mobile";
    private String passwordParam = DEFAULT_PASSWORD_PARAM;
    @Override
    protected AuthenticationToken createToken(ServletRequest request, ServletResponse response) {
        String mobile = WebUtils.getCleanParam(request, mobileParam);
        String password = WebUtils.getCleanParam(request, passwordParam);


        MobilePasswordToken mobilePasswordToken = new MobilePasswordToken(mobile, password);

        return mobilePasswordToken;
    }
}


