package com.runfast.common.web.shiro;

import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.web.util.WebUtils;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * @author: lijin
 * @date: 2018年05月07日
 */
public class JwtThirdAuthFilter extends JwtAuthenticationFilter{

    private String thirdLoginIdParam = "thirdLoginId";
    private String thirdLoginTypeParam = "thirdLoginType";
    @Override
    protected AuthenticationToken createToken(ServletRequest request, ServletResponse response) {
        String thirdLoginId = WebUtils.getCleanParam(request, thirdLoginIdParam);
        String thirdLoginType = WebUtils.getCleanParam(request, thirdLoginTypeParam);


        ThirdToken token = new ThirdToken(thirdLoginId, ThirdLoginType.valueOf(thirdLoginType));
        return token;
    }
}


