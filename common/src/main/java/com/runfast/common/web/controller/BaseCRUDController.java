package com.runfast.common.web.controller;

import com.runfast.common.Constants;
import com.runfast.common.entity.enums.Operation;
import com.runfast.common.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.io.Serializable;
import java.util.List;

public class BaseCRUDController<M, ID extends Serializable, Example> extends BaseController<M, ID> {
    @Autowired
    protected BaseService<M, ID, Example> baseService;

    @GetMapping
    public String list(@PageableDefault Pageable pageable, Model model) {

        List<M> list = baseService.selectByExample(null);
        model.addAttribute("list", list);
        return viewName("list");
    }

    @GetMapping
    public String view(ID id, Operation operation, Model model) {
        model.addAttribute(Constants.OP_NAME, operation);
        M record = baseService.selectByPrimaryKey(id);
        model.addAttribute("record", record);
        return viewName("view");
    }


}
