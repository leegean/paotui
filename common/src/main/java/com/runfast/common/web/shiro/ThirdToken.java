package com.runfast.common.web.shiro;

import org.apache.shiro.authc.AuthenticationToken;

/**
 * @author: lijin
 * @date: 2018年05月07日
 */
public class ThirdToken implements AuthenticationToken {
    private String thirdLoginId;
    private ThirdLoginType loginType;

    public ThirdToken(String thirdLoginId, ThirdLoginType loginType) {
        this.thirdLoginId = thirdLoginId;
        this.loginType = loginType;
    }

    public String getThirdLoginId() {
        return thirdLoginId;
    }

    public void setThirdLoginId(String thirdLoginId) {
        this.thirdLoginId = thirdLoginId;
    }

    public ThirdLoginType getLoginType() {
        return loginType;
    }

    public void setLoginType(ThirdLoginType loginType) {
        this.loginType = loginType;
    }

    @Override
    public Object getPrincipal() {
        return getThirdLoginId();
    }

    @Override
    public Object getCredentials() {
        return getLoginType();
    }
}
