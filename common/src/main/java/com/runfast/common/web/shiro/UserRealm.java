package com.runfast.common.web.shiro;

import com.gxuc.runfast.bean.Cuser;
import com.runfast.common.Constants;
import com.runfast.common.exception.BaseException;
import com.runfast.common.web.entity.ResultCode;
import com.runfast.paotui.dao.model.Account;
import com.runfast.paotui.dao.model.AccountExample;
import com.runfast.common.dao.model.RunfastCuser;
import com.runfast.common.dao.model.RunfastCuserExample;
import com.runfast.paotui.service.AccountService;
import com.runfast.common.service.RunfastCuserService;
import com.runfast.waimai.dao.model.RunfastSmsSendrecord;
import com.runfast.waimai.dao.model.RunfastSmsSendrecordExample;
import com.runfast.waimai.service.RunfastSmsSendrecordService;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import java.util.Date;
import java.util.List;

public class UserRealm extends AuthorizingRealm {
    @Autowired
    @Lazy
    private AccountService accountService;
    @Autowired
    @Lazy
    private RunfastCuserService runfastCuserService;

    @Autowired
    private RunfastSmsSendrecordService smsSendrecordService;

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        String username = (String) principals.getPrimaryPrincipal();

        AccountExample accountExample = new AccountExample();
        accountExample.createCriteria().andUsernameEqualTo(username);
        List<Account> accounts = accountService.selectByExample(accountExample);


        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        authorizationInfo.setStringPermissions(null);

        return authorizationInfo;
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        Cuser cuser =  new Cuser();
        if(token instanceof MobilePasswordToken){

            MobilePasswordToken passwordToken = (MobilePasswordToken) token;
            String mobile = passwordToken.getMobile();
            String password = passwordToken.getPassword();


            try {
                RunfastCuser runfastCuser = runfastCuserService.getUserBy(mobile, password);

                PropertyUtils.copyProperties(cuser,runfastCuser);

            } catch (Exception e) {
                throw new AuthenticationException(e);
            }

        }else if(token instanceof MobileSmsCodeToken){

            MobileSmsCodeToken smsCodeToken = (MobileSmsCodeToken) token;
            String mobile = smsCodeToken.getMobile();
            String smsCode = smsCodeToken.getSmsCode();


            try {


                RunfastSmsSendrecordExample smsSendrecordExample = new RunfastSmsSendrecordExample();
                smsSendrecordExample.createCriteria().andPhoneEqualTo(mobile)
                        .andXcodeEqualTo(smsCode)
                        .andSmstypeEqualTo(1);
                smsSendrecordExample.setOrderByClause("id desc");
                List<RunfastSmsSendrecord> smsSendrecords = smsSendrecordService.selectByExample(smsSendrecordExample);
                if(smsSendrecords.isEmpty())throw new BaseException(ResultCode.FAIL, "短信验证码错误");


                RunfastCuserExample runfastCuserExample = new RunfastCuserExample();
                runfastCuserExample.createCriteria().andMobileEqualTo(mobile);
                List<RunfastCuser> runfastCusers = runfastCuserService.selectByExample(runfastCuserExample);

                RunfastCuser runfastCuser = null;
                if (runfastCusers.isEmpty()){

                    runfastCuser = new RunfastCuser();
                    runfastCuser.setMobile(mobile);
                    runfastCuser.setCreateTime(new Date());

                    runfastCuserService.insertSelective(runfastCuser);


                }else{
                    runfastCuser = runfastCusers.get(0);
                }

                PropertyUtils.copyProperties(cuser,runfastCuser);


            } catch (Exception e) {
                throw new AuthenticationException(e);
            }


        }else if(token instanceof ThirdToken){
            ThirdToken thirdToken = (ThirdToken) token;
            String thirdLoginId = thirdToken.getThirdLoginId();
            ThirdLoginType loginType = thirdToken.getLoginType();



        }



        RequestContextHolder.currentRequestAttributes().setAttribute(Constants.CURRENT_USER, cuser, RequestAttributes.SCOPE_SESSION);
        SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(token.getPrincipal(), token.getCredentials(), getName());
        return info;
    }
}
