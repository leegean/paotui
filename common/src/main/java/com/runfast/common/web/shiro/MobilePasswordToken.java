package com.runfast.common.web.shiro;

import org.apache.shiro.authc.AuthenticationToken;

/**
 * @author: lijin
 * @date: 2018年05月07日
 */
public class MobilePasswordToken implements AuthenticationToken {

    private String mobile;
    private String password;

    public MobilePasswordToken(String mobile, String password) {
        this.mobile = mobile;
        this.password = password;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public Object getPrincipal() {
        return getMobile();
    }

    @Override
    public Object getCredentials() {
        return getPassword();
    }

}
