package com.runfast.common.web.shiro;

/**
 * @author: lijin
 * @date: 2018年05月07日
 */
public enum ThirdLoginType {
    QQ,WEIXIN,MINI
}
