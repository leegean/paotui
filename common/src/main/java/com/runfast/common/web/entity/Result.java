package com.runfast.common.web.entity;

public final class Result<D> {
    private boolean success;
    private String msg;
    private ResultCode errorCode;
    private String errorMsg;
    private D data;

    public Result() {
    }

    private Result(boolean success, String msg, ResultCode errorCode, String errorMsg, D data) {
        this.success = success;
        this.msg = msg;
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
        this.data = data;
    }


    public static Result ok(String msg) {
        return ok(msg, null);
    }


    public static <D> Result<D> ok(String msg, D data) {
        return newInstance(true, msg, null, null, data);
    }

    public static Result fail(ResultCode errorCode) {
        return fail(errorCode, errorCode.getDescription());
    }

    public static Result fail(ResultCode errorCode, String errorMsg) {
        return newInstance(false, null, errorCode, errorMsg, null);
    }
    public static <D> Result<D> fail(ResultCode errorCode, String errorMsg, D data) {
        return newInstance(false, null, errorCode, errorMsg, data);
    }
    private static <D> Result<D> newInstance(boolean success, String msg, ResultCode errorCode, String errorMsg, D data) {
        return new Result(success, msg, errorCode, errorMsg, data);
    }


    public boolean isSuccess() {
        return success;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public String getMsg() {
        return msg;
    }

    public ResultCode getErrorCode() {
        return errorCode;
    }

    public D getData() {
        return data;
    }
}
