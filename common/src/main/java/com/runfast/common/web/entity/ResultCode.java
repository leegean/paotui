package com.runfast.common.web.entity;

public enum ResultCode {
    SUCCESS("成功"),
    FAIL("失败"),

    VALUE_IS_NULL("值为null"),
    VALUE_IS_EMPTY("值为空"),
    VALUE_IS_BLANK("值为空白"),


    PARAMETER_ERROR("参数错误"),
    NO_AUTHORIZED("没有访问取消"),
    SYSTEM_BUSY("系统繁忙，请稍后再试"),
    ACCOUNT_RELOGIN("未登录，请先登录"),
    ACCOUNT_USERNAME_OR_PASSWORD_ERROR("用户名或者密码错误"),
    ACCOUNT_NOT_EXIST("账号不存在"),
    FREQUENT_OPRATION("频繁操作"),
    DELIVER_COST_NOT_EXIST("配送模板不存在"), AGENT_NEAR_BY_NOT_EXIST("附近不存在商家"),
    PAY_REFUND_FAIL("退款失败"), PAY_PREPAY_FAIL("预下单失败"),
    ORDER_NOT_EXIST("订单不存在"), PAY_CHANNEL_NOT_SUPPORT("支付渠道目前不支持"), PAY_ORDER_QUERY_FAIL("订单查询失败"),
    PAY_STATUS_UNPAID("订单未支付"), ILLEGAL_OPERATION("非法操作"), STANDARD_NOT_EXIST("规格不存在"),
    GOODS_NOT_EXIST("商品不存在"), SUB_OPTION_NOT_EXIST("子选项不存在"), BUSINESS_NOT_EXIST("商家不存在"), MINI_ACCOUNT_NOT_BIND("小程序账户没有绑定");
    private String description;

    private ResultCode(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
