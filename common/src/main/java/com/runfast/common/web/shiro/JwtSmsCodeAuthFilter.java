package com.runfast.common.web.shiro;

import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.web.util.WebUtils;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * @author: lijin
 * @date: 2018年05月07日
 */
public class JwtSmsCodeAuthFilter extends JwtAuthenticationFilter{

    private String mobileParam = "mobile";
    private String smsCodeParam = "smsCode";
    @Override
    protected AuthenticationToken createToken(ServletRequest request, ServletResponse response) {
        String mobile = WebUtils.getCleanParam(request, mobileParam);
        String smsCode = WebUtils.getCleanParam(request, smsCodeParam);


        MobileSmsCodeToken token = new MobileSmsCodeToken(mobile, smsCode);
        return token;
    }
}


