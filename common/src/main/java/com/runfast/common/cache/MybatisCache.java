package com.runfast.common.cache;

import com.runfast.common.utils.SpringUtils;
import org.apache.ibatis.cache.Cache;
import org.apache.ibatis.cache.impl.PerpetualCache;
import org.springframework.cache.CacheManager;

import java.util.concurrent.locks.ReadWriteLock;

public class MybatisCache extends PerpetualCache {
    public MybatisCache(String id) {
        super(id);
    }
    /*private static CacheManager INSTANCE;
    private final static String CACHE_NAME = "mybatis";
    private org.springframework.cache.Cache cache;
    private final String id;

    public static CacheManager getInstance() {
        if (INSTANCE == null) {
            synchronized (CacheManager.class) {
                if (INSTANCE == null) {
                    INSTANCE = SpringUtils.getBean(CacheManager.class);
                }
            }
        }
        return INSTANCE;
    }

    public MybatisCache(String id) {
        if (id == null) {
            throw new IllegalArgumentException("Cache instances require an ID");
        }
        this.id = id;
        this.cache = MybatisCache.getInstance().getCache(CACHE_NAME);
    }


    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public void putObject(Object key, Object value) {
        this.cache.put(key, value);
    }

    @Override

    public Object getObject(Object key) {
        org.springframework.cache.Cache.ValueWrapper value = cache.get(key);
        if (value == null) {
            return null;
        }
        return value.get();
    }

    @Override
    public Object removeObject(Object key) {
        Object object = getObject(key);
        cache.evict(key);
        return object;
    }

    @Override
    public void clear() {
        this.cache.clear();
    }

    @Override
    public int getSize() {
        return 0;
    }

    @Override
    public ReadWriteLock getReadWriteLock() {
        return null;
    }*/
}
