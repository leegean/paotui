package com.runfast.common.exception;

import com.runfast.common.web.entity.ResultCode;

public class BaseException extends RuntimeException {


    private ResultCode errorCode;

    public BaseException(ResultCode errorCode) {
        super(errorCode.getDescription());
        this.errorCode = errorCode;
    }

    public BaseException(ResultCode errorCode, String description) {
        super(description);
        this.errorCode = errorCode;
    }


    public ResultCode getErrorCode() {
        return errorCode;
    }

}
