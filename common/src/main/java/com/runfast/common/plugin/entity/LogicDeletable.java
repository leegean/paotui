package com.runfast.common.plugin.entity;

public interface LogicDeletable {

    public Boolean getDeleted();

    public void setDeleted(Boolean deleted);

}
