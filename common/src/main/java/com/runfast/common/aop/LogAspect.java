package com.runfast.common.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import java.time.Clock;

/**
 * @author: lijin
 * @date: 2018年03月01日
 */
@Component
@Aspect
@Slf4j
public class LogAspect {


    /*@Before("execution(* com.runfast..service.*.*(..))")
    public void doBeforeInServiceLayer(JoinPoint joinPoint) {
        log.debug("doBeforeInServiceLayer: " + joinPoint.getSignature().getName());
    }

    @After("execution(* com.runfast..service.*.*(..))")
    public void doAfterInServiceLayer(JoinPoint joinPoint) {
        log.debug("doAfterInServiceLayer: " + joinPoint.getSignature().getName());
    }
*/
    @Around("execution(* com.runfast..service.*.*(..))")
    public Object doAround(ProceedingJoinPoint pjp) throws Throwable {
        long start = Clock.systemDefaultZone().millis();
        Object result = pjp.proceed();
        log.info("doAround: " + pjp.getSignature().getName() + " 耗时 " + (Clock.systemDefaultZone().millis() - start) + " 毫秒");
        return result;
    }
}
