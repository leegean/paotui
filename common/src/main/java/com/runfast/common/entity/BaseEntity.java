package com.runfast.common.entity;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;


public abstract class BaseEntity<ID extends Serializable> {
    public BaseEntity() {
    }

    public BaseEntity(ID id) {
        this.id = id;
    }

    private ID id;

    public ID getId() {
        return id;
    }


    public void setId(final ID id) {
        this.id = id;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BaseEntity that = (BaseEntity) o;

        return null == id ? false : id.equals(that.id);
    }

    @Override
    public int hashCode() {
        int hashCode = 17;

        hashCode += null == getId() ? 0 : getId().hashCode() * 31;

        return hashCode;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }
}
