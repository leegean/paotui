package com.runfast.common.entity.enums;

public enum Operation {
    create, update, delete
}
