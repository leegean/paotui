package com.runfast.common.entity;

import lombok.Data;

/**
 * @author: lijin
 * @date: 2018年07月04日
 */
@Data
public class FeieResult {

    private String msg;

    private Integer ret;

    private Object data;

    private Integer serverExecutedTime;
}
