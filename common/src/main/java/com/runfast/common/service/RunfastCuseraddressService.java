package com.runfast.common.service;

import com.runfast.common.service.IService;
import com.runfast.common.dao.model.RunfastCuseraddress;
import com.runfast.common.dao.model.RunfastCuseraddressExample;

public interface RunfastCuseraddressService extends IService<RunfastCuseraddress, Integer, RunfastCuseraddressExample> {
    public Integer deleteByPrimaryKey(Integer id);
}