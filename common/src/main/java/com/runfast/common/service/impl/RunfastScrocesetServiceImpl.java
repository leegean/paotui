package com.runfast.common.service.impl;

import com.runfast.common.dao.model.RunfastScroceset;
import com.runfast.common.dao.model.RunfastScrocesetExample;
import com.runfast.common.service.BaseService;
import com.runfast.common.service.RunfastScrocesetService;
import org.springframework.stereotype.Service;

@Service
public class RunfastScrocesetServiceImpl extends BaseService<RunfastScroceset, Integer, RunfastScrocesetExample> implements RunfastScrocesetService {
}