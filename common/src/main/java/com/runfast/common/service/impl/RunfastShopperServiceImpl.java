package com.runfast.common.service.impl;

import com.runfast.common.service.BaseService;
import com.runfast.common.dao.model.RunfastShopper;
import com.runfast.common.dao.model.RunfastShopperExample;
import com.runfast.common.service.RunfastShopperService;
import org.springframework.stereotype.Service;

@Service
public class RunfastShopperServiceImpl extends BaseService<RunfastShopper, Integer, RunfastShopperExample> implements RunfastShopperService {
}