package com.runfast.common.service.impl;

import com.runfast.common.dao.model.RunfastLoginRecord;
import com.runfast.common.dao.model.RunfastLoginRecordExample;
import com.runfast.common.service.BaseService;
import com.runfast.common.service.RunfastLoginRecordService;
import org.springframework.stereotype.Service;

@Service
public class RunfastLoginRecordServiceImpl extends BaseService<RunfastLoginRecord, Integer, RunfastLoginRecordExample> implements RunfastLoginRecordService {
}