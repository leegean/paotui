package com.runfast.common.service;

import com.runfast.common.service.IService;
import com.runfast.common.dao.model.RunfastDeliverCost;
import com.runfast.common.dao.model.RunfastDeliverCostExample;
import com.runfast.paotui.entity.DeliveryCost;

public interface RunfastDeliverCostService extends IService<RunfastDeliverCost, Integer, RunfastDeliverCostExample> {
    RunfastDeliverCost getDefaultDeliveryTemplate(Integer agentId);



    DeliveryCost getDeliveryCost(Integer distance, Integer weight, RunfastDeliverCost deliveryTemplate);
}