package com.runfast.common.service;

import com.runfast.common.security.spring.UserDataDetails;
import com.runfast.common.dao.model.RunfastCuser;
import com.runfast.common.dao.model.RunfastCuserExample;

public interface RunfastCuserService extends IService<RunfastCuser, Integer, RunfastCuserExample> {


    RunfastCuser getUserBy(String username, String password);

    public UserDataDetails<RunfastCuser> checkMobielSmsCode(String mobile, String smsCode);

    public UserDataDetails<RunfastCuser> checkThird(String thirdLoginId, String thirdLoginType);

    int updateAlias(String alias);

    UserDataDetails<RunfastCuser> checkMobielPassword(String mobile, String password);
}