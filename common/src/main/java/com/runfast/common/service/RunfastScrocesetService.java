package com.runfast.common.service;


import com.runfast.common.dao.model.RunfastScroceset;
import com.runfast.common.dao.model.RunfastScrocesetExample;

public interface RunfastScrocesetService extends IService<RunfastScroceset, Integer, RunfastScrocesetExample> {
}