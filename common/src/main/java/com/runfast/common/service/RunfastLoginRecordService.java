package com.runfast.common.service;

import com.runfast.common.dao.model.RunfastLoginRecord;
import com.runfast.common.dao.model.RunfastLoginRecordExample;

public interface RunfastLoginRecordService extends IService<RunfastLoginRecord, Integer, RunfastLoginRecordExample> {
}