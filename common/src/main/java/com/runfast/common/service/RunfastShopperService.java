package com.runfast.common.service;

import com.runfast.common.service.IService;
import com.runfast.common.dao.model.RunfastShopper;
import com.runfast.common.dao.model.RunfastShopperExample;

public interface RunfastShopperService extends IService<RunfastShopper, Integer, RunfastShopperExample> {
}