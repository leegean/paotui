package com.runfast.common.service;

import org.springframework.data.domain.Pageable;

import javax.validation.Validator;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2018/1/30.
 */


public interface IService<M, ID extends Serializable, Example> {

    public Validator getValidator();

    public long countByExample(Example example);

    public long countByExampleWithPageable(Example example, Pageable pageable);


    /*public int deleteByExample(Example example);


    public int deleteByPrimaryKey(ID id);*/


    public int insert(M record);


    public int insertSelective(M record);


    public List<M> selectByExample(Example example);

    public List<M> selectByExampleWithBLOBs(Example example);

    public List<M> selectByExampleWithPageable(Example example, Pageable pageable);

    public List<M> selectByExampleWithBLOBsWithPageable(Example example, Pageable pageable);


    public M selectByPrimaryKey(ID id);


    public int updateByExampleSelective(M record, Example example);


    public int updateByExample(M record, Example example);


    public int updateByPrimaryKeySelective(M record);


    public int updateByPrimaryKey(M record);
}
