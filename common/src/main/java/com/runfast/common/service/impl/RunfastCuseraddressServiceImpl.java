package com.runfast.common.service.impl;

import com.runfast.common.dao.IMapper;
import com.runfast.common.dao.mapper.RunfastCuseraddressMapper;
import com.runfast.common.service.BaseService;
import com.runfast.common.dao.model.RunfastCuseraddress;
import com.runfast.common.dao.model.RunfastCuseraddressExample;
import com.runfast.common.service.RunfastCuseraddressService;
import org.springframework.stereotype.Service;

@Service
public class RunfastCuseraddressServiceImpl extends BaseService<RunfastCuseraddress, Integer, RunfastCuseraddressExample> implements RunfastCuseraddressService {
    @Override
    public RunfastCuseraddressMapper getMapper() {
        return (RunfastCuseraddressMapper)super.getMapper();
    }

    @Override
    public Integer deleteByPrimaryKey(Integer id) {
        return getMapper().deleteByPrimaryKey(id);
    }
}