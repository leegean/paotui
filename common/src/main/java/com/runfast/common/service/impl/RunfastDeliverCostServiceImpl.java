package com.runfast.common.service.impl;

import com.runfast.common.exception.BaseException;
import com.runfast.common.service.BaseService;
import com.runfast.common.utils.DateUtils;
import com.runfast.common.web.entity.ResultCode;
import com.runfast.common.dao.model.RunfastDeliverCost;
import com.runfast.common.dao.model.RunfastDeliverCostExample;
import com.runfast.paotui.dao.model.WeightTemplate;
import com.runfast.paotui.dao.model.WeightTemplateExample;
import com.runfast.paotui.entity.DeliveryCost;
import com.runfast.common.service.RunfastDeliverCostService;
import com.runfast.paotui.service.WeightTemplateService;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
public class RunfastDeliverCostServiceImpl extends BaseService<RunfastDeliverCost, Integer, RunfastDeliverCostExample> implements RunfastDeliverCostService {


    @Autowired
    private WeightTemplateService weightTemplateService;




    @Override
    public RunfastDeliverCost getDefaultDeliveryTemplate(Integer agentId) {
        RunfastDeliverCost deliverTemplate = null;

        RunfastDeliverCostExample deliverCostExample = new RunfastDeliverCostExample();
        deliverCostExample.createCriteria().andAgentIdEqualTo(agentId);
        List<RunfastDeliverCost> deliverCosts = this.selectByExampleWithBLOBs(deliverCostExample);
        if (deliverCosts.isEmpty()) throw new BaseException(ResultCode.DELIVER_COST_NOT_EXIST);
        for (RunfastDeliverCost deliverCostItem : deliverCosts) {
            Integer isdefault = deliverCostItem.getIsdefault();
            if (isdefault!=null&&isdefault == 1) {
                deliverTemplate = deliverCostItem;
                break;
            }
        }
        if (deliverTemplate == null) deliverTemplate = deliverCosts.get(0);
        return deliverTemplate;
    }
    @Override
    public DeliveryCost getDeliveryCost(Integer distance, Integer weight, RunfastDeliverCost deliveryTemplate) {

        if (distance != null && distance < 0) throw new BaseException(ResultCode.PARAMETER_ERROR, "distance 不能小于0");
        Validate.notNull(deliveryTemplate);

        Integer deliveryFee = 0;


        Double nocharge = deliveryTemplate.getNoCharge() == null ? 0 : deliveryTemplate.getNoCharge();

        //转换为以米为单位
        int baseDistance = BigDecimal.valueOf(nocharge).multiply(BigDecimal.valueOf(1000)).intValue();

        Date starttimeday1 = deliveryTemplate.getStartTimeDay1();
        Date endtimeday1 = deliveryTemplate.getEndTimeDay1();

        Date now = new Date();
        int baseFee = 0;
        int feePerKM = 0; //没公里超出费用，以分为单位
        if (DateUtils.nowTimeBetween(starttimeday1, endtimeday1)) {
            baseFee = deliveryTemplate.getTimeCost1() == null ? 0 : BigDecimal.valueOf(deliveryTemplate.getTimeCost1()).multiply(BigDecimal.valueOf(100)).intValue();
            feePerKM = deliveryTemplate.getCharge1() == null ? 0 : deliveryTemplate.getCharge1().multiply(BigDecimal.valueOf(100)).intValue();
        } else {
            baseFee = deliveryTemplate.getTimeCost2() == null ? 0 : BigDecimal.valueOf(deliveryTemplate.getTimeCost2()).multiply(BigDecimal.valueOf(100)).intValue();
            feePerKM = deliveryTemplate.getCharge2() == null ? 0 : deliveryTemplate.getCharge2().multiply(BigDecimal.valueOf(100)).intValue();
        }

        Integer exceedFee = null;
        if (distance == null || distance <= baseDistance) {
            deliveryFee = baseFee;
        } else {
            int exceedDistance = distance - baseDistance;
            exceedFee = exceedDistance * feePerKM / 1000;

            deliveryFee = baseFee + exceedFee;
        }

        Integer weightFee = null;
        if (weight != null) {
            WeightTemplateExample weightTemplateExample = new WeightTemplateExample();
            weightTemplateExample.createCriteria().andAgentIdEqualTo(deliveryTemplate.getAgentId())
                    .andDeletedEqualTo(false);
            List<WeightTemplate> weightTemplates = weightTemplateService.selectByExample(weightTemplateExample);


            for (WeightTemplate weightTemplate : weightTemplates) {
                Integer cost = weightTemplate.getCost();
                Integer lower = weightTemplate.getLower();
                Integer upper = weightTemplate.getUpper();


                if (weight >= lower && weight < upper) {
                    weightFee = cost;
                    break;
                }


            }
        }


        BigDecimal maxcharge = deliveryTemplate.getMaxCharge() == null ? BigDecimal.valueOf(0) : deliveryTemplate.getMaxCharge().multiply(BigDecimal.valueOf(100));
        int maxFee = maxcharge.intValue();
        deliveryFee = deliveryFee <= maxFee ? deliveryFee : maxFee;

        DeliveryCost deliveryCost = new DeliveryCost();
        deliveryCost.setBaseFee(baseFee);
        deliveryCost.setExceedFee(exceedFee);
        deliveryCost.setWeightFee(weightFee);
        deliveryCost.setDeliveryFee(deliveryFee);
        return deliveryCost;
    }

}