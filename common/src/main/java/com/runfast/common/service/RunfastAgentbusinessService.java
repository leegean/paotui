package com.runfast.common.service;

import com.runfast.common.service.IService;
import com.runfast.common.dao.model.RunfastAgentbusiness;
import com.runfast.common.dao.model.RunfastAgentbusinessExample;

public interface RunfastAgentbusinessService extends IService<RunfastAgentbusiness, Integer, RunfastAgentbusinessExample> {
    RunfastAgentbusiness getAgentNearByRange(Double lng, Double lat);

}