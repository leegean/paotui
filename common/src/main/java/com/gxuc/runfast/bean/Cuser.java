package com.gxuc.runfast.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Cuser implements Serializable {
    private Integer id;

    private String areaName;

    private String byRoles;

    private String card;

    private String cityId;

    private String cityName;

    private String code;

    private String countyId;

    private String countyName;

    private Date createTime;

    private Date logTime;

    private String mobile;

    private String name;

    private String nickname;

    private String openid;

    private String orgType;

    private String password;

    private String pic;

    private BigDecimal remainder;

    private String townId;

    private String townName;

    private String provinceId;

    private String qq;

    private String xinl;

    private String email;

    private Integer gender;

    private Double score;

    private BigDecimal minmonety;

    private BigDecimal consume;

    private Integer rnum;

    private BigDecimal totalremainder;

    private String bdpushChannelId;

    private String bdpushUserId;

    private Integer bptype;

    private String otherId;

    private Integer pushType;

    private String alias;

//    private String thirdLoginId;
//
//    private Integer thirdLoginType;

    private static final long serialVersionUID = -3556903143935942100L;

    public Integer getId() {
        return id;
    }

    public Cuser withId(Integer id) {
        this.setId(id);
        return this;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAreaName() {
        return areaName;
    }

    public Cuser withAreaName(String areaName) {
        this.setAreaName(areaName);
        return this;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName == null ? null : areaName.trim();
    }

    public String getByRoles() {
        return byRoles;
    }

    public Cuser withByRoles(String byRoles) {
        this.setByRoles(byRoles);
        return this;
    }

    public void setByRoles(String byRoles) {
        this.byRoles = byRoles == null ? null : byRoles.trim();
    }

    public String getCard() {
        return card;
    }

    public Cuser withCard(String card) {
        this.setCard(card);
        return this;
    }

    public void setCard(String card) {
        this.card = card == null ? null : card.trim();
    }

    public String getCityId() {
        return cityId;
    }

    public Cuser withCityId(String cityId) {
        this.setCityId(cityId);
        return this;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId == null ? null : cityId.trim();
    }

    public String getCityName() {
        return cityName;
    }

    public Cuser withCityName(String cityName) {
        this.setCityName(cityName);
        return this;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName == null ? null : cityName.trim();
    }

    public String getCode() {
        return code;
    }

    public Cuser withCode(String code) {
        this.setCode(code);
        return this;
    }

    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    public String getCountyId() {
        return countyId;
    }

    public Cuser withCountyId(String countyId) {
        this.setCountyId(countyId);
        return this;
    }

    public void setCountyId(String countyId) {
        this.countyId = countyId == null ? null : countyId.trim();
    }

    public String getCountyName() {
        return countyName;
    }

    public Cuser withCountyName(String countyName) {
        this.setCountyName(countyName);
        return this;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName == null ? null : countyName.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public Cuser withCreateTime(Date createTime) {
        this.setCreateTime(createTime);
        return this;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getLogTime() {
        return logTime;
    }

    public Cuser withLogTime(Date logTime) {
        this.setLogTime(logTime);
        return this;
    }

    public void setLogTime(Date logTime) {
        this.logTime = logTime;
    }

    public String getMobile() {
        return mobile;
    }

    public Cuser withMobile(String mobile) {
        this.setMobile(mobile);
        return this;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public String getName() {
        return name;
    }

    public Cuser withName(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getNickname() {
        return nickname;
    }

    public Cuser withNickname(String nickname) {
        this.setNickname(nickname);
        return this;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname == null ? null : nickname.trim();
    }

    public String getOpenid() {
        return openid;
    }

    public Cuser withOpenid(String openid) {
        this.setOpenid(openid);
        return this;
    }

    public void setOpenid(String openid) {
        this.openid = openid == null ? null : openid.trim();
    }

    public String getOrgType() {
        return orgType;
    }

    public Cuser withOrgType(String orgType) {
        this.setOrgType(orgType);
        return this;
    }

    public void setOrgType(String orgType) {
        this.orgType = orgType == null ? null : orgType.trim();
    }

    public String getPassword() {
        return password;
    }

    public Cuser withPassword(String password) {
        this.setPassword(password);
        return this;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getPic() {
        return pic;
    }

    public Cuser withPic(String pic) {
        this.setPic(pic);
        return this;
    }

    public void setPic(String pic) {
        this.pic = pic == null ? null : pic.trim();
    }

    public BigDecimal getRemainder() {
        return remainder;
    }

    public Cuser withRemainder(BigDecimal remainder) {
        this.setRemainder(remainder);
        return this;
    }

    public void setRemainder(BigDecimal remainder) {
        this.remainder = remainder;
    }

    public String getTownId() {
        return townId;
    }

    public Cuser withTownId(String townId) {
        this.setTownId(townId);
        return this;
    }

    public void setTownId(String townId) {
        this.townId = townId == null ? null : townId.trim();
    }

    public String getTownName() {
        return townName;
    }

    public Cuser withTownName(String townName) {
        this.setTownName(townName);
        return this;
    }

    public void setTownName(String townName) {
        this.townName = townName == null ? null : townName.trim();
    }

    public String getProvinceId() {
        return provinceId;
    }

    public Cuser withProvinceId(String provinceId) {
        this.setProvinceId(provinceId);
        return this;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId == null ? null : provinceId.trim();
    }

    public String getQq() {
        return qq;
    }

    public Cuser withQq(String qq) {
        this.setQq(qq);
        return this;
    }

    public void setQq(String qq) {
        this.qq = qq == null ? null : qq.trim();
    }

    public String getXinl() {
        return xinl;
    }

    public Cuser withXinl(String xinl) {
        this.setXinl(xinl);
        return this;
    }

    public void setXinl(String xinl) {
        this.xinl = xinl == null ? null : xinl.trim();
    }

    public String getEmail() {
        return email;
    }

    public Cuser withEmail(String email) {
        this.setEmail(email);
        return this;
    }

    public void setEmail(String email) {
        this.email = email == null ? null : email.trim();
    }

    public Integer getGender() {
        return gender;
    }

    public Cuser withGender(Integer gender) {
        this.setGender(gender);
        return this;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public Double getScore() {
        return score;
    }

    public Cuser withScore(Double score) {
        this.setScore(score);
        return this;
    }

    public void setScore(Double score) {
        this.score = score;
    }

    public BigDecimal getMinmonety() {
        return minmonety;
    }

    public Cuser withMinmonety(BigDecimal minmonety) {
        this.setMinmonety(minmonety);
        return this;
    }

    public void setMinmonety(BigDecimal minmonety) {
        this.minmonety = minmonety;
    }

    public BigDecimal getConsume() {
        return consume;
    }

    public Cuser withConsume(BigDecimal consume) {
        this.setConsume(consume);
        return this;
    }

    public void setConsume(BigDecimal consume) {
        this.consume = consume;
    }

    public Integer getRnum() {
        return rnum;
    }

    public Cuser withRnum(Integer rnum) {
        this.setRnum(rnum);
        return this;
    }

    public void setRnum(Integer rnum) {
        this.rnum = rnum;
    }

    public BigDecimal getTotalremainder() {
        return totalremainder;
    }

    public Cuser withTotalremainder(BigDecimal totalremainder) {
        this.setTotalremainder(totalremainder);
        return this;
    }

    public void setTotalremainder(BigDecimal totalremainder) {
        this.totalremainder = totalremainder;
    }

    public String getBdpushChannelId() {
        return bdpushChannelId;
    }

    public Cuser withBdpushChannelId(String bdpushChannelId) {
        this.setBdpushChannelId(bdpushChannelId);
        return this;
    }

    public void setBdpushChannelId(String bdpushChannelId) {
        this.bdpushChannelId = bdpushChannelId == null ? null : bdpushChannelId.trim();
    }

    public String getBdpushUserId() {
        return bdpushUserId;
    }

    public Cuser withBdpushUserId(String bdpushUserId) {
        this.setBdpushUserId(bdpushUserId);
        return this;
    }

    public void setBdpushUserId(String bdpushUserId) {
        this.bdpushUserId = bdpushUserId == null ? null : bdpushUserId.trim();
    }

    public Integer getBptype() {
        return bptype;
    }

    public Cuser withBptype(Integer bptype) {
        this.setBptype(bptype);
        return this;
    }

    public void setBptype(Integer bptype) {
        this.bptype = bptype;
    }

    public String getOtherId() {
        return otherId;
    }

    public Cuser withOtherId(String otherId) {
        this.setOtherId(otherId);
        return this;
    }

    public void setOtherId(String otherId) {
        this.otherId = otherId == null ? null : otherId.trim();
    }

    public Integer getPushType() {
        return pushType;
    }

    public Cuser withPushType(Integer pushType) {
        this.setPushType(pushType);
        return this;
    }

    public void setPushType(Integer pushType) {
        this.pushType = pushType;
    }

    public String getAlias() {
        return alias;
    }

    public Cuser withAlias(String alias) {
        this.setAlias(alias);
        return this;
    }

    public void setAlias(String alias) {
        this.alias = alias == null ? null : alias.trim();
    }

    /*public String getThirdLoginId() {
        return thirdLoginId;
    }

    public Cuser withThirdLoginId(String thirdLoginId) {
        this.setThirdLoginId(thirdLoginId);
        return this;
    }

    public void setThirdLoginId(String thirdLoginId) {
        this.thirdLoginId = thirdLoginId == null ? null : thirdLoginId.trim();
    }

    public Integer getThirdLoginType() {
        return thirdLoginType;
    }

    public Cuser withThirdLoginType(Integer thirdLoginType) {
        this.setThirdLoginType(thirdLoginType);
        return this;
    }

    public void setThirdLoginType(Integer thirdLoginType) {
        this.thirdLoginType = thirdLoginType;
    }*/

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", areaName=").append(areaName);
        sb.append(", byRoles=").append(byRoles);
        sb.append(", card=").append(card);
        sb.append(", cityId=").append(cityId);
        sb.append(", cityName=").append(cityName);
        sb.append(", code=").append(code);
        sb.append(", countyId=").append(countyId);
        sb.append(", countyName=").append(countyName);
        sb.append(", createTime=").append(createTime);
        sb.append(", logTime=").append(logTime);
        sb.append(", mobile=").append(mobile);
        sb.append(", name=").append(name);
        sb.append(", nickname=").append(nickname);
        sb.append(", openid=").append(openid);
        sb.append(", orgType=").append(orgType);
        sb.append(", password=").append(password);
        sb.append(", pic=").append(pic);
        sb.append(", remainder=").append(remainder);
        sb.append(", townId=").append(townId);
        sb.append(", townName=").append(townName);
        sb.append(", provinceId=").append(provinceId);
        sb.append(", qq=").append(qq);
        sb.append(", xinl=").append(xinl);
        sb.append(", email=").append(email);
        sb.append(", gender=").append(gender);
        sb.append(", score=").append(score);
        sb.append(", minmonety=").append(minmonety);
        sb.append(", consume=").append(consume);
        sb.append(", rnum=").append(rnum);
        sb.append(", totalremainder=").append(totalremainder);
        sb.append(", bdpushChannelId=").append(bdpushChannelId);
        sb.append(", bdpushUserId=").append(bdpushUserId);
        sb.append(", bptype=").append(bptype);
        sb.append(", otherId=").append(otherId);
        sb.append(", pushType=").append(pushType);
        sb.append(", alias=").append(alias);
//        sb.append(", thirdLoginId=").append(thirdLoginId);
//        sb.append(", thirdLoginType=").append(thirdLoginType);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}