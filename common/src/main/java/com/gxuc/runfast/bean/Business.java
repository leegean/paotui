package com.gxuc.runfast.bean;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author: lijin
 * @date: 2018年05月04日
 */
public class Business {
    private Integer id;

    private String address;

    private String cityid;

    private String cityname;

    private String content;

    private String countyid;

    private String countyname;

    private Date createtime;

    private String imgpath;

    private String latitude;

    private Integer levelid;

    private String levelname;

    private String longitude;

    private String mobile;

    private String name;

    private String saledaytime;

    private String salerange;

    private String saletime;

    private String townid;

    private String townname;

    private Integer typeid;

    private String typename;

    private Integer salesnum;

    private Integer sort;

    private BigDecimal startpay;

    private Date endwork;

    private Date startwork;

    private String worktoday;

    private BigDecimal packing;

    private Integer isdeliver;

    private Integer recommend;

    private Integer speed;

    private Integer status;

    private Date endtime1;

    private Date endtime2;

    private Date starttime1;

    private Date starttime2;

    private Double coefficient;

    private BigDecimal minmonety;

    private Integer period;

    private String account;

    private BigDecimal showps;

    private BigDecimal busshowps;

    private Date endwork2;

    private Date startwork2;

    private Integer statu;

    private Integer agentid;

    private String agentname;

    private Integer statusx;

    private Integer distributiontime;

    private Integer packtime;

    private Integer bank;

    private String establishbank;

    private String establishname;

    private String miniImgpath;

    private Integer isopen;

    private BigDecimal basecharge;

    private Integer ischarge;

    private String code;

    private String typestr;

    private Integer teamid;

    private String teamname;

    private Integer visitnum;

    private Integer issubsidy;

    private BigDecimal subsidy;

    private Boolean goldbusiness;

    private Boolean autoprint;

    private Boolean autotaking;

    /**
     * 是否优先排名
     */
    private Boolean priority;

    /**
     * 外卖负责人手机号
     */
    private String wmMobile;

    private String faceImage;

    private String innerImage;

    /**
     * 商家获得评论总分
     */
    private Integer score;

    private static final long serialVersionUID = -6006044978031361105L;
}
