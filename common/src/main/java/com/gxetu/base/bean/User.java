package com.gxetu.base.bean;

import java.io.Serializable;
import java.util.Date;

/**
 * @author: lijin
 * @date: 2018年06月05日
 */
public class User implements Serializable {
    private static final long serialVersionUID = -6413645279566445501L;

    private int id;

    private String username;//登陆名
    private String realname;//真实姓名
    private String password;//密码
    private String orgId;//归属组织编号
    private String areaid;//归属地区编号
    private String mobile;//号码
    private String email;//邮件
    private Integer state; //帐号状态
    private Date modifyTime;//状态修改时间
    private Date createtime = new Date();//帐号创建时间
    private String cssStyle;//后台风格
    private Integer flag;//用户级别

//	private String cityId;//城市
//	private String cityName;
//	private String countyId;//县份
//	private String countyName;

    private String agentids;//管理的代理商ID
}
